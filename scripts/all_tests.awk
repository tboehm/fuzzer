BEGIN {
    instructions = ""
    cycles = "2000"
    gen_time = ""
    run_time = ""

    saw_cycle = "0"
    saw_swerv = "0"

    print "Instructions,Cycles,Generation time,Running time"
}

/Cycle PC/ {
    saw_cycle = "1"
}

/SweRV/ && saw_cycle == "1" {
    saw_swerv = "1"
    saw_cycle = "0"
    next
}

/SweRV/ && saw_swerv == "1" {
    cycles = $2
    saw_swerv = ""
    next
}

/Instructions:/ {
    instructions = $2
}

/Generated test in/ {
    gen_time = $5
}

/Ran test in/ {
    run_time = $5
    printf "%s, %s, %s, %s\n", instructions, cycles, gen_time, run_time
    cycles = "2000"
}
