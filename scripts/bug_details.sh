#!/usr/bin/env bash

bug_names=(dec_decode_ctl dec_decode_ctl_1 dec_decode_ctl_2 dec_decode_ctl_3 dec_decode_ctl_4 dec_decode_ctl_5 exu exu_1 exu_alu_ctl)

for bug in ${bug_names[*]}; do
    awk -f scripts/bug_details.awk -v bug_name=$bug top2.log > $bug.csv
done
