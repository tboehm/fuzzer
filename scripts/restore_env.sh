#!/usr/bin/env sh

if ! [ $VERIF_SAVED_UMASK ]; then
    printf %b "No saved umask found!\n"
    return 1
else
    printf %b "Restoring umask to $VERIF_SAVED_UMASK\n"
    umask $VERIF_SAVED_UMASK
    unset VERIF_SAVED_UMASK
fi
