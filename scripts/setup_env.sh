#!/usr/bin/env bash

cwd=$(realpath `dirname "${BASH_SOURCE[0]}"`)

# Save the umask (restore with restore_env.sh)
export VERIF_SAVED_UMASK=`umask`
printf %b "Saved umask: $VERIF_SAVED_UMASK\n"
umask 0007
printf %b "New umask: 0007\n"

# SweRV root directory
export RV_ROOT=`realpath "$cwd/../swerv"`
printf %b "Setting \$RV_ROOT to $RV_ROOT\n"
