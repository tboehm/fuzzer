BEGIN {
    bug_name = ""
    build_time = ""
    test_name = ""
    cycles = ""
    tests_run = ""
    bugs_found = ""

    saw_cycle = ""
    saw_swerv = ""

    printf "Bug name,Build time,Tests run,Tests revealed bugs\n"
}

/Testing bug/ {
    bug_name = $4
}

/Built swerv in/ {
    build_time = $5
}

/^BUG FOUND IN TEST/ {
    test_name = $5
}

/Cycle/ {
    saw_cycle = "1"
}

/SweRV/ && saw_cycle == "1" {
    saw_swerv = "1"
    saw_cycle = "0"
}

/SweRV/ && saw_swerv == "1" {
    cycles = $2
    # printf "%s, %d cycles\n", test_name, cycles
    saw_swerv = "0"
    test_name =""
    cycles = ""
}

/Number of tests run/ {
    tests_run = $5
}

/Found bug in/ {
    bugs_found = $4
    printf "%s, %s, %s, %s\n", bug_name, build_time, tests_run, bugs_found
}
