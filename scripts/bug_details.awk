BEGIN {
    if (bug_name == "") {
        print "Specify a bug name with `-v bug_name=X`"
        exit
    }
    current_bug = ""
    test_name = ""
    cycles = ""

    saw_cycle = ""
    saw_swerv = ""

    printf "Test name,cycles until bug\n"
}

/Testing bug/ {
    current_bug = $4
}

/^BUG FOUND IN TEST/ {
    test_name = $5
}

/Cycle/ && current_bug == bug_name {
    saw_cycle = "1"
}

/SweRV/ && saw_cycle == "1" {
    saw_swerv = "1"
    saw_cycle = "0"
}

/SweRV/ && saw_swerv == "1" {
    cycles = $2
    printf "%s, %d\n", test_name, cycles
    saw_swerv = "0"
    test_name =""
    cycles = ""
}

/Found bug in/ && current_bug == bug_name {
    exit
}
