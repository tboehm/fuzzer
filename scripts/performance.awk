BEGIN {
    saw_cycle = "0"
    saw_swerv = "0"
}

/^BUG FOUND IN TEST/ {
    printf "%s: ", $5
}

/Cycle/ {
    saw_cycle = "1"
}

/SweRV/ && saw_cycle == "1" {
    saw_swerv = "1"
    saw_cycle = "0"
}

/SweRV/ && saw_swerv == "1" {
    printf "%d cycles\n", $2
    saw_swerv = "0"
}
