#!/usr/bin/env python3

import os
import shutil
from multiprocessing import Process, Value
from threading import Thread
import signal
import time
import traceback
import subprocess as sp
import sys

MAX_MISMATCH = 1

cwd = os.getcwd()
ref_dir = os.path.join(cwd, "swerv/tools")
bug_dir = os.path.join(cwd, "buggy-swerv/tools")
test_dir = os.path.join(cwd, "fuzzer/tests")
res_dir = os.path.join(cwd, "results")


def test_process(test, num_pass):
    with open(ref_dir + "/exec.log", "w") as ref, open(
        bug_dir + "/exec.log", "w"
    ) as buggy:
        ref.truncate(0)
        buggy.truncate(0)
    print("Running Test: " + test[: -len(".hex")])
    test_proc = Process(target=run_test, args=(test, ref_dir, bug_dir))
    test_proc.start()
    #run_test(test, ref_dir, bug_dir)
    #os.chdir(cwd)
    test_proc.join()
    find_bug = compare(ref_dir, bug_dir, test)
    if find_bug == False:
        log = "NO BUG FOUND IN TEST: {}\n\n".format(test[: -len(".hex")])
        print(log)
        with open(res_dir + "/summary.log", "a") as sum:
            sum.write(log)
        num_pass.value += 1


def run_test(test, dir1, dir2):
    shutil.copy(test_dir + "/" + test, dir1 + "/fuzzed.hex")
    shutil.copy(test_dir + "/" + test, dir2 + "/fuzzed.hex")
    ref_proc = Process(target=tester, args=(test, dir1))
    bug_proc = Process(target=tester, args=(test, dir2))
    ref_proc.start()
    bug_proc.start()
    ref_proc.join()
    bug_proc.join()


def tester(test, dir):
    shutil.copy(test_dir + "/" + test, dir + "/fuzzed.hex")
    os.chdir(dir)
    #sp.call(['./obj_dir/Vtb_top'], stdout=os.devnull, stderr=os.devnull)
    os.system("./obj_dir/Vtb_top > out.log")


def compare(dir1, dir2, test):
    # Compare Outputs
    ref_prev = ""
    bug_prev = ""
    with open(dir1 + "/exec.log", "r") as ref, open(dir2 + "/exec.log", "r") as buggy:
        reflines = ref.readlines()
        for ref_line in reflines:
            bug_line = buggy.readline()
            if ref_line != bug_line:
                print_err(ref_line, bug_line, test[:-len(".hex")], ref_prev, bug_prev)
                return True
            ref_prev = ref_line
            bug_prev = bug_line
    return False


def print_err(ref_line, bug_line, test, ref_prev, bug_prev):
    ref = parse(ref_line.strip(), test)
    bug = parse(bug_line.strip(), test)
    if ref_prev != "":
        ref_p = parse(ref_prev.strip(), test)
        bug_p = parse(bug_prev.strip(), test)

    table = [ ["","Cycle", "PC", "Instruction", "LSU_ADDR", "LSU_DATA", "REG", "REGVALUE"],
              ["SweRV", ref_p["Cycle"], ref_p["PC"], ref_p["instr"], ref_p["addr"], ref_p["data"], ref_p["reg"], ref_p["regvalue"]],
              ["Buggy-SweRV", bug_p["Cycle"], bug_p["PC"], bug_p["instr"], bug_p["addr"], bug_p["data"], bug_p["reg"], bug_p["regvalue"]],
              ["","","","","","","","",],
              ["SweRV", ref["Cycle"], ref["PC"], ref["instr"], ref["addr"], ref["data"], ref["reg"], ref["regvalue"]],
              ["Buggy-SweRV", bug["Cycle"], bug["PC"], bug["instr"], bug["addr"], bug["data"], bug["reg"], bug["regvalue"]] ]

    log = ("BUG FOUND IN TEST {}\n\n".format(test))
    for cat, cyc, pc, ins, adr, data, reg, val in table:
        log += "{:<12} {:<5} {:<10} {:<30} {:<10} {:<10} {:<3} {:<10}\n".format(cat, cyc, pc, ins, adr, data, reg, val)

    print(log)
    with open(res_dir+"/summary.log", 'a') as sum:
        sum.write(log)

def instr(pc, test):
    addr = "addr0x"+str(pc)
    with open(test_dir+"/"+test+".s", 'r') as read:
        f = read.readlines()
    for line in f:
        if addr in line:
            parse = line.split(":")
            return parse[1][1:].strip()
    print("FAILED TO FIND ASM")


def parse(line, test):
    data = (" ".join(line.split())).split(" ")
    op = instr(data[2], test)
    if len(data) == 7:
        reg_info = data[6].split("=")
        reg = reg_info[0]
        regvalue = reg_info[1]
    else:
        reg = ""
        regvalue = ""
    data_dict = {"Cycle": data[1], "PC": data[2], "instr": op, "addr": data[4], "data": data[5], "reg": reg, "regvalue": regvalue}
    return data_dict


def make(remake):
    if remake == False:
        print("Copying tb_top.sv to testbenches")
        shutil.copy("tb_top.sv", cwd + "/swerv/testbench/tb_top.sv")
        shutil.copy("tb_top.sv", cwd + "/buggy-swerv/testbench/tb_top.sv")
        os.environ["RV_ROOT"] = cwd + "/swerv"
        os.chdir(os.environ["RV_ROOT"] + "/tools")
        print("Building SweRV\n")
        sp.call("make verilator-build".split())
        print("Finished Building SweRV\n")
    
    os.environ["RV_ROOT"] = cwd + "/buggy-swerv"
    os.chdir(os.environ["RV_ROOT"] + "/tools")
    print("Building Buggy-SweRV\n")
    sp.call("make verilator-build".split())
    print("Finished Building Buggy-SweRV")
    os.chdir(cwd)


def main(argv):
    '''
    1. Run Test on Reference and Bugged cores in parallel
    2. Comparison results in summary.log
    '''
    try:
        if argv[1] == "make":
            make(False)
        elif argv[1] == "remake":
            make(True)
    except IndexError:
        pass

    num_pass = Value("i", 0)
    num_tests = 0
    with open(res_dir + "/summary.log", "w") as sum:
        sum.truncate(0)
    for test in sorted(os.listdir(test_dir)):
        if test.endswith(".hex"):
            num_tests += 1
            proc = Process(target=test_process, args=(test, num_pass))
            proc.start()
            proc.join()
    log = "Number of Tests Ran: {}\n".format(num_tests)
    log += "Failed to find bugs in {}/{} tests\n".format(num_pass.value, num_tests)
    print(log)
    with open(res_dir + "/summary.log", "a") as sum:
        sum.write(log)


if __name__ == "__main__":
    main(sys.argv)
