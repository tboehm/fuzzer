#!/usr/bin/env python3

# assembly.py: RISC-V assembly tests
#
# Copyright (c) 2020 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import numpy as np

from instructions import numeric_regs, OpType, Instruction, \
    A_Type, B_Type, J_Type, L_Type, S_Type
from markov import MarkovChain

MAX_BRANCH_OFFSET = (1 << 11) - 1
MAX_JAL_OFFSET = (1 << 19) - 1
MAX_JALR_OFFSET = (1 << 10) - 1


class AssemblyTest:
    '''
    A randomly-generated assembly test.
    '''
    def __init__(self, chain, entry=0x0000_0000, entry_register='x3',
                 base_pointer=0x0040_0000, base_register='x8',
                 reg_pool=list(), skip_ops=list()):
        '''
        Create an assembly test:

        1) Make sure the input looks okay
        2) Run the Markov chain
        3) Insert the base register initialization, if desired
        4) Associate an address with every instruction
        5) Replace operands
            a) Branch targets
            b) Load/store base register
        '''
        # 1) Input validation
        assert(isinstance(chain, MarkovChain))
        self.entry = entry
        self.base_pointer = base_pointer
        for addr in (self.base_pointer, self.entry):
            if addr > 0xffff_ffff or addr < 0:
                raise ValueError(f'Address out of range: {addr}')
        for reg in (entry_register, base_register):
            if reg and reg not in numeric_regs:
                raise ValueError(f'Unknown register: {reg}')
        if entry_register and entry_register == base_register \
                and entry != base_pointer:
            raise ValueError('Entry and base register must be different')
        self.entry_register = entry_register
        self.base_register = base_register
        self.reg_pool = reg_pool

        # 2) Run the Markov chain and generate a sequence of instructions. If
        # the user supplied a base register, don't generate any instructions
        # that modify or read that register (ideally, just exclude it as a
        # destination register). If they give a register pool, use it.
        self.chain = chain
        for reg in (self.entry_register, self.base_register):
            OpType.disallow_register(reg)
            if reg in self.reg_pool:
                self.reg_pool.remove(reg)
        if self.reg_pool:
            OpType.RegPool = self.reg_pool
        self.chain.start_at()
        self.chain.run(skip_nodes=skip_ops)
        self.sequence = self.chain.sequence

        # 3) Initilaize the entry register and base register
        if self.entry_register:
            self.initialize_register(self.entry_register, self.entry)
        if self.base_register:
            self.initialize_register(self.base_register, self.base_pointer)

        # 4) Give each instruction an address
        self.sequence_to_instructions()

        # 5) Replace operands
        self.instruction_count = len(self.sequence)
        self.replace_operands()

    def initialize_register(self, reg, value):
        '''
        Initialize some register `reg` with `value`
        '''
        upper_immediate = (value >> 12) & 0xff_ffff
        lower_immediate = value & 0xfff
        xor = ('xor', (reg, reg, reg))
        lui = ('lui', (reg, upper_immediate))
        addi = ('addi', (reg, reg, lower_immediate))
        self.sequence.insert(0, xor)
        self.sequence.insert(1, lui)
        self.sequence.insert(2, addi)

    def sequence_to_instructions(self):
        '''
        Convert a list to a dictionary of address/instruction pairs

        Note: Assumes there are no compressed instructions.
        '''
        addr = self.entry
        self.instructions = dict()
        for inst in self.sequence:
            self.instructions[addr] = inst
            addr += 4

    def replace_operands(self):
        '''
        Replace operands in certain instructions.

        Branches: Generate targets that stay inside the code.
        Loads/stores: Replace the base register for interesting memory accesses.

        Currently, this assumes there are no compressed instructions.
        '''
        # Assuming no compressed instructions
        self.min_target_addr = self.entry
        self.max_target_addr = self.entry + self.instruction_count * 4
        for addr, inst in self.instructions.items():
            opcode, operands = inst
            i_type = Instruction.get_format(opcode)
            if i_type is B_Type:
                # 5a) Branch targets
                operands[2] = self.random_branch_target(addr)
            elif i_type is J_Type:
                # 5b) JAL targets
                operands[1] = self.random_jal_target(addr)
            elif i_type is A_Type:
                # 5c) JALR targets
                operands[0] = 'x0'
                operands[1] = self.random_jalr_offset()
                operands[2] = self.entry_register
            elif self.base_register and i_type in (L_Type, S_Type):
                # 5b) Load/store base register
                operands[2] = self.base_register
                # We can analyze rv64x source code, but we need to convert the
                # 64-bit opcodes ld, lwu, sd, and swu to lw and sw.
                if opcode in ('ld', 'lwu'):
                    opcode = 'lw'
                elif opcode in ('sd', 'swu'):
                    opcode = 'sw'
            self.instructions[addr] = (opcode, operands)

    def random_branch_target(self, inst_addr):
        '''
        Generate a random branch target for a branch at `instr_addr`
        '''
        forward = min(inst_addr + MAX_BRANCH_OFFSET, self.max_target_addr)
        backward = max(inst_addr - MAX_BRANCH_OFFSET, self.min_target_addr) + 4
        return self.random_target(forward, backward)

    def random_jal_target(self, inst_addr):
        '''
        Generate a random target for a JAL at `instr_addr`
        '''
        forward = min(inst_addr + MAX_JAL_OFFSET, self.max_target_addr)
        backward = max(inst_addr - MAX_JAL_OFFSET, self.min_target_addr) + 4
        return self.random_target(forward, backward)

    def random_jalr_offset(self):
        '''
        Generate a random target for a JALR
        '''
        forward = min(MAX_JALR_OFFSET, self.max_target_addr)
        backward = 0
        return self.random_offset(forward, backward)

    def random_offset(self, forward, backward):
        addr = np.random.randint(backward, forward)
        # Word-align the target
        addr = int(4 * np.floor(addr / 4))
        return addr

    def random_target(self, forward, backward):
        addr = self.random_offset(forward, backward)
        return f'addr0x{addr:08x}'

    def stringify(self):
        '''
        Convert the instruction sequence to an actual assembly file.

        Start off by exporting the _start symbol, then add every instruction
        prefixed with a label, like this:

        .global _start
        _start:
        addr0x80000000: lui x9, .....
        addr0x80000004: addi x9, ....

        We need these labels because the GNU assembly doesn't understand basic
        numeric offsets for branch targets.
        '''
        asm = ['.global _start', '_start:']
        for addr, inst in self.instructions.items():
            asm_inst = Instruction.to_string(inst)
            label = f'addr0x{addr:08x}'
            asm.append(f'{label}: {asm_inst}')
        asm_test = '\n'.join(asm) + '\n'
        return asm_test

    def __str__(self):
        s = '\n'.join([
            f'Entry point: 0x{self.entry:8x}',
            f'Entry register: {self.entry_register}',
            f'Base pointer: 0x{self.base_pointer:8x}',
            f'Base register: {self.base_register}',
            f'Instruction count: {self.instruction_count}',
        ])
        return s
