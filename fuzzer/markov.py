#!/usr/bin/env python3

# markov.py: Convert an object dump to a Markov chain
#
# Copyright (c) 2020 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import numpy as np
import traceback

from instructions import asm_terminus, Instruction


class Node:
    '''
    A single node in a Markov chain.
    '''
    def __init__(self, name, next_freqs):
        self.name = name
        self.payload = None
        self.next_freqs = next_freqs
        self.probabilities = self.compute_probabilities()

    def update(self, next_freqs):
        '''
        Update the frequencies and probabilities with new information.
        '''
        for name, freq in next_freqs.items():
            current = self.next_freqs.get(name, None)
            if current is None:
                self.next_freqs[name] = freq
            else:
                self.next_freqs[name] += freq
        self.probabilities = self.compute_probabilities()

    def compute_probabilities(self):
        '''
        Compute state transition probabilities based on the frequencies.
        '''
        freqs = list(self.next_freqs.values())
        total = sum(freqs)
        probabilities = dict()
        for name, freq in self.next_freqs.items():
            probabilities[name] = freq / total
        return probabilities

    def next(self):
        '''
        Randomly select the name of the next node.
        '''
        n = np.random.choice(list(self.next_freqs.keys()),
                             p=list(self.probabilities.values()))
        return n

    def gen_instance(self):
        '''
        Generate an instance of the node
        '''
        inst_class = Instruction.get_format(self.name)
        # skip node if instruction not handled
        if inst_class is None:
            return ('SKIP', self.name)
        return (self.name, inst_class.gen_instance())

    def __str__(self):
        return " " .join(self.gen_instance())


class MarkovChain:
    '''
    A basic Markov chain, initialized from a dictionary of frequencies.
    '''
    def __init__(self, frequencies, seed=None):
        if seed is not None:
            np.random.seed(seed)
        self.frequencies = frequencies
        self.nodes = self.make_nodes()
        self._current_node = None
        self.n_iterations = 0
        self.sequence = None

    def start_at(self, name=None):
        '''
        Set the initial node.
        '''
        if name is None:
            name = np.random.choice(list(self.frequencies.keys()))
        try:
            self._current_node = self.nodes[name]
        except KeyError:
            print(f'Unknown node name: {name}')

    def make_nodes(self):
        '''
        Generate the nodes from the frequency dictionary.
        '''
        nodes = dict()
        for name, next_freqs in self.frequencies.items():
            nodes[name] = Node(name, next_freqs)
        return nodes

    def update_nodes(self, frequencies):
        '''
        Update the nodes with additional frequency information.
        '''
        for name, next_freqs in frequencies.items():
            if name in self.nodes:
                # Update existing
                self.nodes[name].update(next_freqs)
            else:
                # Create a new node
                self.nodes[name] = Node(name, next_freqs)

    def next(self):
        '''
        Go to the next state.
        '''
        try:
            next_name = self._current_node.next()
            self.n_iterations += 1
            self._current_node = self.nodes[next_name]
            return self._current_node
        except AttributeError:
            if next_name is None:
                print('AttributeError: Have you called start_at?')
            else:
                print(traceback.format_exc())
        except KeyError:
            if next_name == asm_terminus:
                return None
            else:
                print(traceback.format_exc())

    def run(self, skip_nodes=None, verbose=False):
        '''
        Simulate the Markov chain until it reaches a final state.

        Returns the sequence of random nodes.
        '''
        self.sequence = list()

        if skip_nodes is None:
            skip_nodes = list()

        self.n_iterations = 0
        node = self._current_node
        while node is not None:
            # instance is formatted as (opcode, operands)
            instance = node.gen_instance()
            if verbose:
                # Print all nodes
                print(instance)
            if not instance[0].startswith('SKIP'):
                # Only return non-skipped nodes
                self.sequence.append(instance)
            node = self.next()
            while node is not None and node.name in skip_nodes:
                node = self.next()
        if verbose:
            print(asm_terminus)
        print(f'  Instructions: {self.n_iterations}')

    def __str__(self):
        return str(self.frequencies)
