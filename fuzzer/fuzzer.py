#!/usr/bin/env python3

# fuzzer.py: Generate random tests from a corpus of valid assembly
#
# Copyright (c) 2020 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import os
import subprocess as sp

from assembly import AssemblyTest
from analyze import Dump
from markov import MarkovChain

# dumpfiles = [os.path.join('dumps', file) for file in os.listdir('dumps')
#              if file.startswith('rv32ui-p-') and not file.endswith('.dump')]

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--num-tests', type=int, default=5,
                    help='Number of tests')
parser.add_argument('-f', '--files', nargs='+',
                    help='Input files (.s or .dump)')


def generate_chain(files, seed=12):
    '''
    Generate a Markov chain from assembly/dump files.
    '''
    if not isinstance(files, list):
        files = [files]

    # Generate the frequency lists
    print(f'Generating frequency lists from test files: {files}')
    dumps = {}
    for i, test in enumerate(files):
        if not os.path.exists(test):
            raise FileNotFoundError(test)
        dumps[test] = Dump(test)

    # Generate the Markov chain
    chain = MarkovChain(dumps[files[0]].frequencies, seed=seed)

    # Update the Markov chain with frequencies from all other input files
    for test in files[1:]:
        chain.update_nodes(dumps[test].frequencies)
    return chain


def generate_test(chain, test_name):
    '''
    Generate tests from a Markov chain.
    '''
    # Create the fuzzed assembly test from the chain
    asm_test = AssemblyTest(chain)
    code = asm_test.stringify()

    # Assemble into an ELF and dump the hex
    asmfile = test_name + '.s'
    with open(asmfile, 'w') as fp:
        fp.write(code)
    elffile = test_name + '.elf'
    hexfile = test_name + '.hex'
    sp.run(['make', '-C', 'fuzzer', elffile, hexfile])
    return hexfile


def main():
    args = parser.parse_args()
    print(args.files)
    print(args.num_tests)
    if not args.files:
        args.files = ['fuzzer/dumps/rvsim.dump']
    chain = generate_chain(args.files)
    for test_num in range(args.num_tests):
        test_path = f'fuzzer/tests/test_{test_num}'
        generate_test(chain, test_path)


if __name__ == '__main__':
    main()
