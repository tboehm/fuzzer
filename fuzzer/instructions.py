#!/usr/bin/env python3

# instructions.py: Instruction encodings and generation
#
# Copyright (c) 2020 Trey Boehm, Steven Zhu
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from enum import Enum, auto, unique

import numpy as np

# All valid 32-bit RISC-V opcodes
opcodes = "auipc lui jal jalr beq bne blt bge bltu bgeu lb lh lw lbu lhu sb sh sw addi slti sltiu xori ori andi slli srli srai add sub sll slt sltu xor srl sra or and mul mulh mulhsu mulhu div divu rem remu fence fence.i ecall ebreak csrrw csrrs csrrc csrrwi csrrsi csrrci lwu ld sd addiw slliw srliw sraiw addw subw sllw srlw sraw la nop li mv not neg negw sext.w seqz snez sltz sgtz beqz bnez blez bgez bltz bgtz bgtble bgtu bleu j jr ret call tail rdinstret rdinstreth rdcycle rdcycleh rdtime rdtimeh csrr csrw csrs csrc csrwi csrsi csrci".split()

# "opcode" to mark the end of the code segment
asm_terminus = "end"

# Valid ABI register names
abi_regs = ["zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "fp", "s0", "s1",
            "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4",
            "s5", "s6", "s7", "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6"]

numeric_regs = ['x' + str(i) for i in range(32)]


@unique
class OpType(Enum):
    '''
    Operand type
    '''
    UNKNOWN = auto()
    REG = auto()
    IMM5 = auto()
    IMM12 = auto()
    IMM20 = auto()
    LUI20 = auto()
    OFFSET = auto()

    def disallow_register(reg):
        if reg in OpType.AllowedRegs and reg in numeric_regs:
            OpType.AllowedRegs.remove(reg)

    def gen_instance(self):
        if self is OpType.REG:
            reg = np.random.choice(OpType.RegPool)
            if reg is None:
                reg = np.random.choice(OpType.AllowedRegs)
                OpType.RegPool.append(reg)
            return reg
        elif self is OpType.IMM5:
            # 5 bit imm, unsigned
            return np.random.randint(2**5)
        elif self is OpType.IMM12:
            # 12 bit imm, signed
            return np.random.randint(-2**11, 2**11)
        elif self is OpType.IMM20:
            # 20 bit imm, signed
            return np.random.randint(-2**19, 2**19)
        elif self is OpType.LUI20:
            # 20 bit imm, unsigned
            return np.random.randint(2**20)
        elif self is OpType.OFFSET:
            return np.random.randint(-2**11, 2**11)
        else:
            raise Exception("unknown optype")


# Limited pool of registers. [None] -> defer to AllowedRegs
OpType.RegPool = [None]

# Registers we have no forbidden. Used only if RegPool is [None]
OpType.AllowedRegs = numeric_regs.copy()


class Instruction:
    '''
    RISC-V instructions and their representations
    '''
    def __init__(self, format_str, operands):
        self.format_str = format_str
        self.operands = operands

    @classmethod
    def get_format(cls, opcode):
        for inst_format, opcodes in cls.opcode_map.items():
            if opcode in opcodes:
                return inst_format

        # print(f'Unknown opcode name: {opcode}')

    def to_string(instance):
        opcode, operands = instance
        format_str = Instruction.get_format(opcode).format_str
        return ' '.join([opcode, format_str.format(*operands)])

    def gen_instance(self):
        return [op.gen_instance() for op in self.operands]

# hack to associate instruction types with class because no forward decl in python


No_Oper = Instruction("", [])
R_Type = Instruction("{}, {}, {}", [OpType.REG, OpType.REG, OpType.REG])
I_Type = Instruction("{}, {}, {}", [OpType.REG, OpType.REG, OpType.IMM12])
# I_Type load subtype
L_Type = Instruction("{}, {}({})", [OpType.REG, OpType.OFFSET, OpType.REG])
# I_Type shift subtype
H_Type = Instruction("{}, {}, {}", [OpType.REG, OpType.REG, OpType.IMM5])
# I_Type csri subtype
C_Type = Instruction("{}, {}, {}", [OpType.REG, OpType.IMM5, OpType.IMM12])
# I_Type jalr subtype
A_Type = Instruction("{}, {}({})", [OpType.REG, OpType.IMM12, OpType.REG])
S_Type = Instruction("{}, {}({})", [OpType.REG, OpType.OFFSET, OpType.REG])
B_Type = Instruction("{}, {}, {}", [OpType.REG, OpType.REG, OpType.OFFSET])
# LUI requires unsigned
U_Type = Instruction("{}, {}", [OpType.REG, OpType.LUI20])
# AUIPC requires signed
J_Type = Instruction("{}, {}", [OpType.REG, OpType.IMM20])

Instruction.opcode_map = {
    No_Oper: "fence fencei ecall ebreak".split(),
    R_Type: "add sub sll slt sltu xor srl sra or and mul mulh mulhsu mulhu div divu rem remu".split(),
    I_Type: "addi slti sltiu xori ori andi csrrw csrrs csrrc".split(),
    L_Type: "lb lh lw ld lbu lhu lwu".split(),
    H_Type: "slli srli srai".split(),
    C_Type: "csrrwi csrrsi csrrci".split(),
    A_Type: "jalr".split(),
    S_Type: "sb sh sw sd".split(),
    B_Type: "beq bne blt bge bltu bgeu".split(),
    U_Type: "lui auipc".split(),
    J_Type: "jal".split(),
}
