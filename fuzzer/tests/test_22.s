.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: beq x7, x12, addr0x00000298
addr0x0000001c: lw x9, 15(x8)
addr0x00000020: lh x31, -642(x8)
addr0x00000024: slli x31, x30, 28
addr0x00000028: srli x17, x14, 0
addr0x0000002c: srli x13, x20, 6
addr0x00000030: bne x11, x31, addr0x00000038
addr0x00000034: jal x0, addr0x0000068c
addr0x00000038: addi x6, x14, -482
addr0x0000003c: sub x7, x22, x0
addr0x00000040: sw x24, 1815(x8)
addr0x00000044: lui x27, 548653
addr0x00000048: sw x0, -1117(x8)
addr0x0000004c: add x15, x25, x20
addr0x00000050: jal x28, addr0x0000018c
addr0x00000054: jal x29, addr0x000004a0
addr0x00000058: jal x5, addr0x000000fc
addr0x0000005c: jal x5, addr0x000002f8
addr0x00000060: lw x16, 2(x8)
addr0x00000064: jal x19, addr0x000005f0
addr0x00000068: jal x27, addr0x00000714
addr0x0000006c: lw x20, 1400(x8)
addr0x00000070: jal x13, addr0x000002cc
addr0x00000074: lw x27, -1561(x8)
addr0x00000078: sw x14, 1365(x8)
addr0x0000007c: beq x24, x29, addr0x00000788
addr0x00000080: jal x22, addr0x00000618
addr0x00000084: lw x25, 1483(x8)
addr0x00000088: add x30, x26, x28
addr0x0000008c: sw x2, -283(x8)
addr0x00000090: jal x26, addr0x00000700
addr0x00000094: sw x11, 64(x8)
addr0x00000098: jal x0, addr0x00000238
addr0x0000009c: addi x27, x24, -1041
addr0x000000a0: sw x23, -291(x8)
addr0x000000a4: addi x20, x9, 433
addr0x000000a8: sw x9, 975(x8)
addr0x000000ac: jal x23, addr0x00000684
addr0x000000b0: lw x0, -1723(x8)
addr0x000000b4: sw x2, -1012(x8)
addr0x000000b8: sw x0, -1126(x8)
addr0x000000bc: bltu x13, x21, addr0x000000a0
addr0x000000c0: lui x24, 674159
addr0x000000c4: addi x22, x22, 224
addr0x000000c8: beq x27, x1, addr0x0000001c
addr0x000000cc: ori x18, x19, -1372
addr0x000000d0: addi x0, x7, 603
addr0x000000d4: jal x26, addr0x000005e4
addr0x000000d8: addi x27, x23, -1850
addr0x000000dc: sh x15, -2014(x8)
addr0x000000e0: addi x14, x19, -69
addr0x000000e4: sw x12, -201(x8)
addr0x000000e8: lw x20, 1377(x8)
addr0x000000ec: lw x17, 1021(x8)
addr0x000000f0: sw x20, -1603(x8)
addr0x000000f4: sw x13, -203(x8)
addr0x000000f8: jal x10, addr0x00000600
addr0x000000fc: lw x5, -414(x8)
addr0x00000100: lw x5, -1300(x8)
addr0x00000104: bge x30, x7, addr0x00000704
addr0x00000108: addi x17, x23, -789
addr0x0000010c: jal x19, addr0x00000660
addr0x00000110: addi x19, x1, -328
addr0x00000114: addi x24, x13, -1849
addr0x00000118: sw x11, -1372(x8)
addr0x0000011c: sw x28, 456(x8)
addr0x00000120: lw x30, -1168(x8)
addr0x00000124: lw x4, -1000(x8)
addr0x00000128: sw x19, -1708(x8)
addr0x0000012c: sw x29, 949(x8)
addr0x00000130: sw x23, -1087(x8)
addr0x00000134: sw x29, -2043(x8)
addr0x00000138: addi x30, x13, -45
addr0x0000013c: sw x9, -95(x8)
addr0x00000140: sw x27, -2018(x8)
addr0x00000144: jal x19, addr0x0000071c
addr0x00000148: lw x24, 51(x8)
addr0x0000014c: addi x4, x30, -285
addr0x00000150: sub x9, x25, x12
addr0x00000154: add x7, x31, x6
addr0x00000158: lw x2, 1435(x8)
addr0x0000015c: addi x23, x29, 1469
addr0x00000160: lui x28, 131492
addr0x00000164: addi x25, x30, -1395
addr0x00000168: addi x18, x18, -701
addr0x0000016c: sw x29, -31(x8)
addr0x00000170: jal x27, addr0x00000024
addr0x00000174: sw x30, 366(x8)
addr0x00000178: sw x31, -1586(x8)
addr0x0000017c: sw x23, 1725(x8)
addr0x00000180: lw x9, -1059(x8)
addr0x00000184: lw x14, -1786(x8)
addr0x00000188: sb x14, -1288(x8)
addr0x0000018c: lw x7, 1931(x8)
addr0x00000190: lw x12, -473(x8)
addr0x00000194: sw x9, 295(x8)
addr0x00000198: lui x24, 1002206
addr0x0000019c: addi x24, x18, -910
addr0x000001a0: jal x24, addr0x000001fc
addr0x000001a4: sw x18, -1454(x8)
addr0x000001a8: lw x30, 316(x8)
addr0x000001ac: sw x7, 1707(x8)
addr0x000001b0: sub x0, x29, x21
addr0x000001b4: addi x23, x9, -865
addr0x000001b8: lw x20, 1075(x8)
addr0x000001bc: add x30, x11, x19
addr0x000001c0: lw x22, -727(x8)
addr0x000001c4: sw x18, 780(x8)
addr0x000001c8: lui x9, 826974
addr0x000001cc: sh x0, -1814(x8)
addr0x000001d0: lh x0, -91(x8)
addr0x000001d4: lui x23, 1007094
addr0x000001d8: lui x2, 4857
addr0x000001dc: addi x18, x24, -712
addr0x000001e0: jal x4, addr0x00000044
addr0x000001e4: lw x31, -902(x8)
addr0x000001e8: lw x1, -792(x8)
addr0x000001ec: addi x14, x2, -1884
addr0x000001f0: jal x7, addr0x0000047c
addr0x000001f4: lw x7, -875(x8)
addr0x000001f8: jal x23, addr0x000004a8
addr0x000001fc: beq x7, x30, addr0x000006a0
addr0x00000200: addi x19, x6, 1808
addr0x00000204: andi x23, x25, 2037
addr0x00000208: lui x5, 536835
addr0x0000020c: addi x18, x27, 917
addr0x00000210: sh x1, 1630(x8)
addr0x00000214: sw x21, 1557(x8)
addr0x00000218: bge x23, x7, addr0x00000140
addr0x0000021c: sub x0, x2, x17
addr0x00000220: lw x16, -1683(x8)
addr0x00000224: lw x5, 274(x8)
addr0x00000228: lw x10, -1019(x8)
addr0x0000022c: add x1, x27, x0
addr0x00000230: addi x24, x19, -113
addr0x00000234: add x14, x25, x17
addr0x00000238: lw x23, -151(x8)
addr0x0000023c: jal x19, addr0x00000770
addr0x00000240: sw x29, -562(x8)
addr0x00000244: andi x30, x22, 884
addr0x00000248: lw x24, 38(x8)
addr0x0000024c: addi x22, x29, -1859
addr0x00000250: lui x17, 307002
addr0x00000254: addi x13, x17, 1430
addr0x00000258: addi x20, x4, -849
addr0x0000025c: jal x21, addr0x000001e0
addr0x00000260: beq x22, x19, addr0x00000464
addr0x00000264: slli x15, x31, 21
addr0x00000268: addi x22, x1, -1447
addr0x0000026c: addi x19, x5, -1966
addr0x00000270: slli x17, x10, 25
addr0x00000274: lw x19, 1262(x8)
addr0x00000278: lw x17, -693(x8)
addr0x0000027c: lw x18, 879(x8)
addr0x00000280: sw x7, 1124(x8)
addr0x00000284: sw x2, -829(x8)
addr0x00000288: sw x25, 1072(x8)
addr0x0000028c: sw x29, 1064(x8)
addr0x00000290: sw x16, 400(x8)
addr0x00000294: bne x13, x10, addr0x00000758
addr0x00000298: jal x2, addr0x00000428
addr0x0000029c: lw x31, -391(x8)
addr0x000002a0: beq x29, x31, addr0x00000488
addr0x000002a4: jal x17, addr0x00000290
addr0x000002a8: bne x31, x15, addr0x00000540
addr0x000002ac: lw x15, -598(x8)
addr0x000002b0: addi x20, x31, -191
addr0x000002b4: add x0, x21, x29
addr0x000002b8: sub x19, x0, x17
addr0x000002bc: addi x17, x18, -1698
addr0x000002c0: add x30, x19, x5
addr0x000002c4: lui x31, 436740
addr0x000002c8: addi x25, x18, 736
addr0x000002cc: bltu x5, x7, addr0x000001f0
addr0x000002d0: andi x7, x18, -1051
addr0x000002d4: addi x9, x7, 604
addr0x000002d8: sw x17, 1715(x8)
addr0x000002dc: jal x17, addr0x00000354
addr0x000002e0: jal x21, addr0x00000554
addr0x000002e4: lw x7, 1010(x8)
addr0x000002e8: bgeu x1, x17, addr0x0000016c
addr0x000002ec: sw x17, -346(x8)
addr0x000002f0: slli x5, x7, 22
addr0x000002f4: sw x31, 2028(x8)
addr0x000002f8: sw x19, 1919(x8)
addr0x000002fc: jal x27, addr0x000000b4
addr0x00000300: lw x5, -1295(x8)
addr0x00000304: addi x30, x9, 841
addr0x00000308: jal x23, addr0x000005d8
addr0x0000030c: jal x25, addr0x000005b0
addr0x00000310: jal x20, addr0x000003dc
addr0x00000314: sw x12, 1001(x8)
addr0x00000318: jal x6, addr0x00000388
addr0x0000031c: sw x18, -622(x8)
addr0x00000320: sw x0, -378(x8)
addr0x00000324: sw x31, -1207(x8)
addr0x00000328: jal x24, addr0x000006f8
addr0x0000032c: lbu x7, -149(x8)
addr0x00000330: jal x24, addr0x000007bc
addr0x00000334: lw x0, -1712(x8)
addr0x00000338: jal x11, addr0x000005c0
addr0x0000033c: jal x7, addr0x00000430
addr0x00000340: lui x18, 631059
addr0x00000344: and x23, x14, x29
addr0x00000348: slli x22, x30, 6
addr0x0000034c: sub x12, x9, x0
addr0x00000350: sw x27, 1493(x8)
addr0x00000354: jal x27, addr0x0000029c
addr0x00000358: lw x18, 1049(x8)
addr0x0000035c: sw x5, 1740(x8)
addr0x00000360: sw x11, -1100(x8)
addr0x00000364: sw x13, -852(x8)
addr0x00000368: addi x29, x19, 1837
addr0x0000036c: jal x22, addr0x00000750
addr0x00000370: lw x31, -295(x8)
addr0x00000374: lw x16, 936(x8)
addr0x00000378: jal x5, addr0x0000002c
addr0x0000037c: lw x1, -1631(x8)
addr0x00000380: sw x26, 1256(x8)
addr0x00000384: lw x9, -117(x8)
addr0x00000388: bne x27, x18, addr0x0000022c
addr0x0000038c: lbu x19, -810(x8)
addr0x00000390: beq x27, x21, addr0x00000214
addr0x00000394: sw x7, -880(x8)
addr0x00000398: jal x31, addr0x00000478
addr0x0000039c: sw x22, 978(x8)
addr0x000003a0: bltu x0, x14, addr0x00000060
addr0x000003a4: addi x0, x0, 123
addr0x000003a8: jal x1, addr0x000006e8
addr0x000003ac: lw x18, -887(x8)
addr0x000003b0: lw x21, -339(x8)
addr0x000003b4: beq x29, x17, addr0x000007c0
addr0x000003b8: lbu x7, -2028(x8)
addr0x000003bc: lw x19, 511(x8)
addr0x000003c0: addi x7, x27, 1625
addr0x000003c4: sw x0, -332(x8)
addr0x000003c8: jal x5, addr0x0000075c
addr0x000003cc: addi x7, x15, -1986
addr0x000003d0: sw x21, -350(x8)
addr0x000003d4: lw x4, -488(x8)
addr0x000003d8: jal x9, addr0x000001fc
addr0x000003dc: lw x4, -1862(x8)
addr0x000003e0: slli x9, x25, 15
addr0x000003e4: srli x23, x27, 11
addr0x000003e8: addi x17, x12, 589
addr0x000003ec: jal x17, addr0x00000340
addr0x000003f0: sw x9, 851(x8)
addr0x000003f4: sw x5, 231(x8)
addr0x000003f8: sw x22, -761(x8)
addr0x000003fc: lui x5, 655642
addr0x00000400: jal x14, addr0x0000060c
addr0x00000404: addi x20, x20, -572
addr0x00000408: jal x2, addr0x0000023c
addr0x0000040c: lw x27, -1974(x8)
addr0x00000410: sw x29, 597(x8)
addr0x00000414: jal x0, addr0x00000558
addr0x00000418: lui x21, 396251
addr0x0000041c: addi x9, x6, 521
addr0x00000420: addi x13, x5, -1897
addr0x00000424: jal x10, addr0x0000042c
addr0x00000428: lw x23, 328(x8)
addr0x0000042c: lw x13, -155(x8)
addr0x00000430: lbu x7, 1259(x8)
addr0x00000434: jal x21, addr0x00000300
addr0x00000438: lw x22, 832(x8)
addr0x0000043c: jal x29, addr0x00000750
addr0x00000440: addi x7, x14, -1036
addr0x00000444: lw x5, 1174(x8)
addr0x00000448: andi x23, x20, 214
addr0x0000044c: bne x21, x1, addr0x00000398
addr0x00000450: lw x14, -1759(x8)
addr0x00000454: lw x0, -146(x8)
addr0x00000458: addi x11, x19, 303
addr0x0000045c: jal x13, addr0x0000051c
addr0x00000460: sw x4, 1093(x8)
addr0x00000464: bne x22, x17, addr0x000003e4
addr0x00000468: lui x13, 886677
addr0x0000046c: addi x0, x0, 1045
addr0x00000470: lhu x29, 1551(x8)
addr0x00000474: sh x1, -949(x8)
addr0x00000478: bne x29, x29, addr0x0000071c
addr0x0000047c: addi x4, x21, 1200
addr0x00000480: addi x9, x17, 480
addr0x00000484: sw x25, -1718(x8)
addr0x00000488: sw x2, -1909(x8)
addr0x0000048c: sw x29, 779(x8)
addr0x00000490: sw x22, -241(x8)
addr0x00000494: sw x19, -933(x8)
addr0x00000498: sw x20, -1470(x8)
addr0x0000049c: lui x30, 672493
addr0x000004a0: addi x29, x20, -123
addr0x000004a4: jal x4, addr0x000004e8
addr0x000004a8: sw x18, 388(x8)
addr0x000004ac: jal x1, addr0x000005c4
addr0x000004b0: jal x4, addr0x000003fc
addr0x000004b4: lw x29, 750(x8)
addr0x000004b8: lw x31, 1820(x8)
addr0x000004bc: sb x2, -1705(x8)
addr0x000004c0: lw x19, 1500(x8)
addr0x000004c4: addi x0, x19, -88
addr0x000004c8: jal x22, addr0x00000300
addr0x000004cc: lw x0, -165(x8)
addr0x000004d0: lw x29, -858(x8)
addr0x000004d4: bgeu x4, x27, addr0x00000290
addr0x000004d8: add x5, x7, x7
addr0x000004dc: jal x9, addr0x000000a8
addr0x000004e0: addi x29, x12, -743
addr0x000004e4: sw x31, -644(x8)
addr0x000004e8: addi x16, x18, 571
addr0x000004ec: jal x18, addr0x00000274
addr0x000004f0: lw x29, 1795(x8)
addr0x000004f4: lw x0, 266(x8)
addr0x000004f8: beq x25, x24, addr0x00000744
addr0x000004fc: addi x29, x28, 186
addr0x00000500: bge x20, x27, addr0x000001e4
addr0x00000504: addi x29, x23, -419
addr0x00000508: jal x27, addr0x0000007c
addr0x0000050c: sw x0, -1106(x8)
addr0x00000510: lw x9, 1138(x8)
addr0x00000514: addi x27, x4, -63
addr0x00000518: sw x19, -553(x8)
addr0x0000051c: add x27, x24, x23
addr0x00000520: andi x19, x7, 577
addr0x00000524: jal x21, addr0x00000188
addr0x00000528: jal x27, addr0x0000051c
addr0x0000052c: lui x13, 607323
addr0x00000530: addi x29, x17, 1558
addr0x00000534: sw x4, 1246(x8)
addr0x00000538: sw x9, -1921(x8)
addr0x0000053c: lbu x4, 1971(x8)
addr0x00000540: beq x29, x13, addr0x000002c8
addr0x00000544: jal x24, addr0x000006cc
addr0x00000548: jal x22, addr0x000002c8
addr0x0000054c: jal x1, addr0x000005f4
addr0x00000550: jal x29, addr0x000000b8
addr0x00000554: lw x14, 1266(x8)
addr0x00000558: lw x7, -135(x8)
addr0x0000055c: lw x29, 1550(x8)
addr0x00000560: bne x14, x13, addr0x000000ac
addr0x00000564: bltu x18, x28, addr0x00000398
addr0x00000568: sub x30, x9, x30
addr0x0000056c: lbu x18, 1035(x8)
addr0x00000570: sw x20, -1469(x8)
addr0x00000574: jal x7, addr0x00000400
addr0x00000578: lw x7, 142(x8)
addr0x0000057c: jal x19, addr0x00000010
addr0x00000580: lw x20, 478(x8)
addr0x00000584: addi x4, x9, -929
addr0x00000588: lui x17, 261219
addr0x0000058c: sw x29, 1811(x8)
addr0x00000590: jal x30, addr0x00000694
addr0x00000594: lbu x20, -1665(x8)
addr0x00000598: ori x7, x9, 581
addr0x0000059c: lw x0, -497(x8)
addr0x000005a0: srli x31, x0, 12
addr0x000005a4: add x22, x10, x0
addr0x000005a8: bne x17, x28, addr0x00000638
addr0x000005ac: jal x21, addr0x0000005c
addr0x000005b0: jal x13, addr0x000000e4
addr0x000005b4: jal x20, addr0x00000658
addr0x000005b8: lw x0, -1808(x8)
addr0x000005bc: sw x28, 1738(x8)
addr0x000005c0: sw x0, 120(x8)
addr0x000005c4: lw x19, -340(x8)
addr0x000005c8: add x13, x26, x29
addr0x000005cc: jal x4, addr0x000002ac
addr0x000005d0: sw x31, -937(x8)
addr0x000005d4: jal x27, addr0x00000184
addr0x000005d8: addi x28, x0, 1027
addr0x000005dc: lw x12, 725(x8)
addr0x000005e0: lw x29, 402(x8)
addr0x000005e4: sw x24, 1208(x8)
addr0x000005e8: jal x7, addr0x000004ac
addr0x000005ec: sw x19, 744(x8)
addr0x000005f0: jal x28, addr0x00000230
addr0x000005f4: sw x7, -658(x8)
addr0x000005f8: sw x5, 1232(x8)
addr0x000005fc: sw x2, -327(x8)
addr0x00000600: sw x30, 166(x8)
addr0x00000604: sw x29, 95(x8)
addr0x00000608: sw x9, 722(x8)
addr0x0000060c: lui x6, 965334
addr0x00000610: lui x2, 396777
addr0x00000614: addi x14, x27, 1761
addr0x00000618: sw x10, 1(x8)
addr0x0000061c: jal x11, addr0x000000cc
addr0x00000620: lw x1, 945(x8)
addr0x00000624: addi x10, x29, 124
addr0x00000628: sw x20, 1423(x8)
addr0x0000062c: sw x5, -975(x8)
addr0x00000630: jal x30, addr0x000000bc
addr0x00000634: jal x27, addr0x00000328
addr0x00000638: addi x13, x7, 816
addr0x0000063c: bgeu x22, x16, addr0x00000784
addr0x00000640: slli x2, x11, 23
addr0x00000644: jal x1, addr0x0000003c
addr0x00000648: lw x19, 164(x8)
addr0x0000064c: addi x9, x18, 895
addr0x00000650: lbu x20, 250(x8)
addr0x00000654: beq x9, x2, addr0x00000480
addr0x00000658: bltu x5, x18, addr0x000007c4
addr0x0000065c: andi x13, x9, 273
addr0x00000660: addi x29, x15, -1698
addr0x00000664: sw x27, 274(x8)
addr0x00000668: sw x19, 1202(x8)
addr0x0000066c: sw x14, -1184(x8)
addr0x00000670: jal x19, addr0x00000608
addr0x00000674: sb x7, -1671(x8)
addr0x00000678: or x20, x29, x16
addr0x0000067c: beq x23, x6, addr0x0000053c
addr0x00000680: addi x4, x0, -71
addr0x00000684: lw x13, -982(x8)
addr0x00000688: andi x29, x16, 1268
addr0x0000068c: addi x22, x16, 581
addr0x00000690: jal x19, addr0x000001d0
addr0x00000694: jal x1, addr0x0000053c
addr0x00000698: add x17, x7, x19
addr0x0000069c: jal x27, addr0x0000025c
addr0x000006a0: jal x13, addr0x00000378
addr0x000006a4: addi x19, x15, -1680
addr0x000006a8: sw x9, 1795(x8)
addr0x000006ac: bne x17, x12, addr0x00000778
addr0x000006b0: lw x23, -684(x8)
addr0x000006b4: jal x0, addr0x000006d4
addr0x000006b8: sw x10, -1519(x8)
addr0x000006bc: jal x7, addr0x00000404
addr0x000006c0: lw x14, 1740(x8)
addr0x000006c4: add x2, x16, x5
addr0x000006c8: lbu x25, 297(x8)
addr0x000006cc: sw x20, 775(x8)
addr0x000006d0: lw x30, 1115(x8)
addr0x000006d4: sw x0, 1239(x8)
addr0x000006d8: lw x7, -294(x8)
addr0x000006dc: lw x5, 1852(x8)
addr0x000006e0: lw x0, -1649(x8)
addr0x000006e4: add x0, x13, x7
addr0x000006e8: sub x2, x23, x0
addr0x000006ec: srai x1, x24, 14
addr0x000006f0: sw x4, -1020(x8)
addr0x000006f4: sw x17, -1220(x8)
addr0x000006f8: jal x15, addr0x000005d8
addr0x000006fc: ori x20, x30, 219
addr0x00000700: sw x7, -1776(x8)
addr0x00000704: sw x6, -281(x8)
addr0x00000708: bne x18, x19, addr0x00000444
addr0x0000070c: sw x0, 514(x8)
addr0x00000710: jal x19, addr0x00000490
addr0x00000714: addi x9, x13, 1558
addr0x00000718: jal x26, addr0x00000098
addr0x0000071c: addi x13, x30, -1360
addr0x00000720: sub x0, x25, x12
addr0x00000724: sw x20, -800(x8)
addr0x00000728: sw x4, 1414(x8)
addr0x0000072c: sw x0, 112(x8)
addr0x00000730: jal x14, addr0x000007a0
addr0x00000734: lbu x20, -73(x8)
addr0x00000738: lw x14, -1767(x8)
addr0x0000073c: slli x20, x15, 9
addr0x00000740: add x16, x30, x25
addr0x00000744: jal x2, addr0x0000030c
addr0x00000748: addi x1, x27, 1160
addr0x0000074c: add x15, x17, x0
addr0x00000750: lbu x11, -1336(x8)
addr0x00000754: lbu x20, -1463(x8)
addr0x00000758: sw x25, 1733(x8)
addr0x0000075c: jal x22, addr0x00000304
addr0x00000760: addi x1, x19, 1389
addr0x00000764: sw x20, 2026(x8)
addr0x00000768: sw x4, 218(x8)
addr0x0000076c: sw x18, -76(x8)
addr0x00000770: sw x21, 1084(x8)
addr0x00000774: sw x17, -1285(x8)
addr0x00000778: jal x1, addr0x000001a8
addr0x0000077c: lw x0, 1495(x8)
addr0x00000780: addi x6, x31, -1665
addr0x00000784: jal x10, addr0x000003ac
addr0x00000788: jal x11, addr0x0000010c
addr0x0000078c: lw x1, 393(x8)
addr0x00000790: addi x27, x14, -2006
addr0x00000794: lbu x23, 657(x8)
addr0x00000798: andi x31, x24, -408
addr0x0000079c: addi x1, x23, -1759
addr0x000007a0: addi x7, x19, 1295
addr0x000007a4: sw x18, 324(x8)
addr0x000007a8: jal x2, addr0x0000064c
addr0x000007ac: lui x10, 771476
addr0x000007b0: slli x6, x30, 17
addr0x000007b4: addi x9, x27, -775
addr0x000007b8: add x10, x0, x5
addr0x000007bc: lhu x24, 1947(x8)
addr0x000007c0: bne x13, x9, addr0x00000228
addr0x000007c4: addi x23, x5, -184
addr0x000007c8: sw x12, 1812(x8)
addr0x000007cc: sw x15, 996(x8)
addr0x000007d0: lw x0, -134(x8)
addr0x000007d4: lw x22, -1594(x8)
addr0x000007d8: lw x0, 994(x8)
addr0x000007dc: add x5, x1, x18
