.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sb x12, -64(x8)
addr0x0000001c: lbu x15, -1361(x8)
addr0x00000020: sb x7, 1226(x8)
addr0x00000024: lw x19, -805(x8)
addr0x00000028: lw x6, 27(x8)
addr0x0000002c: addi x7, x7, 1486
addr0x00000030: bne x7, x19, addr0x00000184
addr0x00000034: lw x22, -120(x8)
addr0x00000038: sw x11, 1511(x8)
addr0x0000003c: lw x31, 1961(x8)
addr0x00000040: lw x13, 645(x8)
addr0x00000044: lw x20, 780(x8)
addr0x00000048: jal x23, addr0x00000204
addr0x0000004c: lbu x24, 858(x8)
addr0x00000050: andi x19, x31, -479
addr0x00000054: lw x14, 278(x8)
addr0x00000058: sw x27, -85(x8)
addr0x0000005c: jal x14, addr0x000001e0
addr0x00000060: lbu x23, -515(x8)
addr0x00000064: andi x1, x25, -3
addr0x00000068: addi x1, x29, -16
addr0x0000006c: add x27, x20, x29
addr0x00000070: jal x20, addr0x000001bc
addr0x00000074: lw x22, -1759(x8)
addr0x00000078: jal x7, addr0x00000320
addr0x0000007c: jal x5, addr0x00000250
addr0x00000080: lui x24, 247654
addr0x00000084: addi x0, x24, -800
addr0x00000088: lui x22, 642351
addr0x0000008c: sw x17, 870(x8)
addr0x00000090: sw x19, 1560(x8)
addr0x00000094: mul x27, x31, x1
addr0x00000098: lw x1, -1761(x8)
addr0x0000009c: lw x26, -837(x8)
addr0x000000a0: jal x9, addr0x000001b4
addr0x000000a4: sw x5, -865(x8)
addr0x000000a8: addi x24, x19, -765
addr0x000000ac: addi x26, x24, 734
addr0x000000b0: ori x19, x9, 1352
addr0x000000b4: addi x31, x15, -109
addr0x000000b8: addi x15, x31, 1001
addr0x000000bc: jal x5, addr0x00000310
addr0x000000c0: addi x28, x13, 515
addr0x000000c4: addi x10, x29, -837
addr0x000000c8: lw x9, 1972(x8)
addr0x000000cc: bne x13, x0, addr0x00000150
addr0x000000d0: lui x28, 852699
addr0x000000d4: addi x9, x29, 285
addr0x000000d8: sw x20, 961(x8)
addr0x000000dc: sw x13, 1897(x8)
addr0x000000e0: sw x22, 1254(x8)
addr0x000000e4: lw x11, -627(x8)
addr0x000000e8: lw x31, 1698(x8)
addr0x000000ec: lw x11, -2028(x8)
addr0x000000f0: jal x26, addr0x0000008c
addr0x000000f4: lbu x31, 1766(x8)
addr0x000000f8: lw x31, -1735(x8)
addr0x000000fc: lw x0, -1120(x8)
addr0x00000100: add x6, x7, x15
addr0x00000104: sw x23, -1586(x8)
addr0x00000108: lbu x19, -1592(x8)
addr0x0000010c: beq x5, x0, addr0x0000001c
addr0x00000110: sw x25, -1561(x8)
addr0x00000114: sw x13, 1629(x8)
addr0x00000118: lw x13, 1912(x8)
addr0x0000011c: lw x7, 1278(x8)
addr0x00000120: addi x21, x21, 857
addr0x00000124: sh x7, -726(x8)
addr0x00000128: bge x22, x4, addr0x00000294
addr0x0000012c: bltu x6, x25, addr0x000002d4
addr0x00000130: jal x12, addr0x00000064
addr0x00000134: lw x13, -1140(x8)
addr0x00000138: addi x25, x0, -798
addr0x0000013c: sw x13, -1699(x8)
addr0x00000140: lw x7, -7(x8)
addr0x00000144: jal x17, addr0x000002cc
addr0x00000148: lw x27, -399(x8)
addr0x0000014c: add x13, x24, x23
addr0x00000150: add x1, x19, x9
addr0x00000154: add x12, x27, x6
addr0x00000158: addi x11, x28, 391
addr0x0000015c: sw x15, -1417(x8)
addr0x00000160: beq x0, x4, addr0x000002e8
addr0x00000164: lbu x1, 152(x8)
addr0x00000168: sb x22, 1575(x8)
addr0x0000016c: lw x6, -1066(x8)
addr0x00000170: bne x21, x23, addr0x000001c4
addr0x00000174: sw x25, -634(x8)
addr0x00000178: sw x17, -1964(x8)
addr0x0000017c: sw x18, -79(x8)
addr0x00000180: lw x25, -1407(x8)
addr0x00000184: lw x20, 1366(x8)
addr0x00000188: addi x21, x24, -1434
addr0x0000018c: lbu x9, -1434(x8)
addr0x00000190: bne x10, x20, addr0x0000004c
addr0x00000194: jal x23, addr0x0000037c
addr0x00000198: lw x21, 1227(x8)
addr0x0000019c: addi x10, x31, 1007
addr0x000001a0: lw x27, 1664(x8)
addr0x000001a4: jal x22, addr0x00000080
addr0x000001a8: addi x7, x30, 1067
addr0x000001ac: beq x16, x4, addr0x00000070
addr0x000001b0: lw x4, -1113(x8)
addr0x000001b4: add x29, x22, x16
addr0x000001b8: jal x6, addr0x00000328
addr0x000001bc: lw x30, -1346(x8)
addr0x000001c0: add x22, x11, x19
addr0x000001c4: lbu x23, 1456(x8)
addr0x000001c8: lw x25, -1564(x8)
addr0x000001cc: addi x19, x19, 409
addr0x000001d0: sw x13, -83(x8)
addr0x000001d4: sw x31, -391(x8)
addr0x000001d8: sb x4, 801(x8)
addr0x000001dc: lbu x4, -1882(x8)
addr0x000001e0: slli x18, x29, 2
addr0x000001e4: srli x5, x14, 18
addr0x000001e8: addi x2, x19, -1613
addr0x000001ec: addi x23, x20, -647
addr0x000001f0: addi x30, x23, 1251
addr0x000001f4: addi x25, x20, -1254
addr0x000001f8: lw x7, -599(x8)
addr0x000001fc: lw x22, 208(x8)
addr0x00000200: bne x20, x2, addr0x00000378
addr0x00000204: addi x22, x10, -788
addr0x00000208: sw x20, -243(x8)
addr0x0000020c: sw x13, 316(x8)
addr0x00000210: sw x20, 173(x8)
addr0x00000214: sw x2, -1893(x8)
addr0x00000218: addi x0, x20, 652
addr0x0000021c: bge x28, x29, addr0x0000027c
addr0x00000220: addi x5, x22, -318
addr0x00000224: xori x19, x19, -1409
addr0x00000228: slli x7, x21, 31
addr0x0000022c: lbu x0, 187(x8)
addr0x00000230: andi x21, x23, -1193
addr0x00000234: addi x0, x14, 1673
addr0x00000238: jal x10, addr0x00000354
addr0x0000023c: addi x13, x0, 1561
addr0x00000240: sh x7, -1875(x8)
addr0x00000244: lw x17, 81(x8)
addr0x00000248: lw x18, -548(x8)
addr0x0000024c: lw x2, -98(x8)
addr0x00000250: lw x27, 1997(x8)
addr0x00000254: lw x24, -829(x8)
addr0x00000258: sw x6, 1234(x8)
addr0x0000025c: addi x13, x13, 397
addr0x00000260: jal x31, addr0x0000023c
addr0x00000264: jal x0, addr0x00000318
addr0x00000268: addi x5, x20, -584
addr0x0000026c: sw x18, -875(x8)
addr0x00000270: sw x23, 1152(x8)
addr0x00000274: remu x14, x23, x15
addr0x00000278: jal x10, addr0x000000e8
addr0x0000027c: lw x25, -711(x8)
addr0x00000280: bltu x19, x19, addr0x00000284
addr0x00000284: lui x23, 799043
addr0x00000288: lui x29, 602396
addr0x0000028c: sw x20, -1388(x8)
addr0x00000290: sw x23, -1244(x8)
addr0x00000294: lui x16, 372833
addr0x00000298: addi x6, x25, -673
addr0x0000029c: sw x11, -1952(x8)
addr0x000002a0: sw x0, 1491(x8)
addr0x000002a4: jal x16, addr0x000002b8
addr0x000002a8: lw x27, -840(x8)
addr0x000002ac: addi x27, x31, -954
addr0x000002b0: sh x26, 544(x8)
addr0x000002b4: lw x19, -1537(x8)
addr0x000002b8: addi x30, x31, -1217
addr0x000002bc: addi x0, x19, -1380
addr0x000002c0: sw x4, -1618(x8)
addr0x000002c4: lui x26, 1045505
addr0x000002c8: sub x28, x30, x12
addr0x000002cc: srai x22, x0, 11
addr0x000002d0: addi x20, x29, 332
addr0x000002d4: lw x27, 327(x8)
addr0x000002d8: addi x4, x5, 1182
addr0x000002dc: jal x29, addr0x00000248
addr0x000002e0: lw x31, 577(x8)
addr0x000002e4: lui x13, 940346
addr0x000002e8: jal x20, addr0x000001a4
addr0x000002ec: lui x1, 935981
addr0x000002f0: lui x17, 93065
addr0x000002f4: lui x29, 455028
addr0x000002f8: addi x9, x19, -483
addr0x000002fc: sw x0, -1528(x8)
addr0x00000300: sw x20, -1344(x8)
addr0x00000304: sw x5, -172(x8)
addr0x00000308: sw x13, -812(x8)
addr0x0000030c: lw x29, 667(x8)
addr0x00000310: lhu x29, 384(x8)
addr0x00000314: sw x16, -1863(x8)
addr0x00000318: jal x14, addr0x000000a8
addr0x0000031c: lhu x15, -2023(x8)
addr0x00000320: ori x2, x23, -1354
addr0x00000324: lui x20, 686539
addr0x00000328: addi x0, x21, -783
addr0x0000032c: jal x5, addr0x00000144
addr0x00000330: beq x0, x26, addr0x00000114
addr0x00000334: ori x24, x22, -1865
addr0x00000338: addi x18, x27, 133
addr0x0000033c: sw x0, -1524(x8)
addr0x00000340: ori x19, x26, -654
addr0x00000344: sw x4, 1871(x8)
addr0x00000348: lw x26, 1864(x8)
addr0x0000034c: jal x10, addr0x00000028
addr0x00000350: sw x5, 1955(x8)
addr0x00000354: jal x27, addr0x00000344
addr0x00000358: srli x7, x30, 29
addr0x0000035c: addi x9, x9, 1333
addr0x00000360: jal x15, addr0x0000025c
addr0x00000364: blt x19, x23, addr0x00000140
addr0x00000368: sw x5, -1999(x8)
addr0x0000036c: lw x23, 1375(x8)
addr0x00000370: lw x19, 1488(x8)
addr0x00000374: addi x19, x5, 1363
addr0x00000378: bne x29, x19, addr0x0000012c
addr0x0000037c: addi x23, x28, 1333
addr0x00000380: jal x17, addr0x00000198
addr0x00000384: addi x19, x19, 1233
addr0x00000388: jal x17, addr0x00000118
