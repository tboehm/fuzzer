.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: divu x20, x23, x31
addr0x0000001c: beq x7, x6, addr0x00000264
addr0x00000020: lw x7, 1148(x8)
addr0x00000024: addi x5, x0, 133
addr0x00000028: addi x18, x28, 1203
addr0x0000002c: lw x16, 1805(x8)
addr0x00000030: addi x21, x5, 579
addr0x00000034: lui x18, 723097
addr0x00000038: sw x13, -454(x8)
addr0x0000003c: sw x27, 373(x8)
addr0x00000040: sw x18, -120(x8)
addr0x00000044: sw x31, 535(x8)
addr0x00000048: jal x5, addr0x00000d74
addr0x0000004c: lw x4, 483(x8)
addr0x00000050: jal x31, addr0x00000308
addr0x00000054: lw x0, -1945(x8)
addr0x00000058: lw x21, -1455(x8)
addr0x0000005c: lw x5, 1372(x8)
addr0x00000060: sw x14, 61(x8)
addr0x00000064: jal x27, addr0x00000db4
addr0x00000068: lw x26, 858(x8)
addr0x0000006c: lw x29, -956(x8)
addr0x00000070: ori x9, x25, -1363
addr0x00000074: lw x23, -234(x8)
addr0x00000078: lw x26, 993(x8)
addr0x0000007c: lw x6, -582(x8)
addr0x00000080: jal x23, addr0x00000bb8
addr0x00000084: addi x18, x22, 1655
addr0x00000088: sw x17, -462(x8)
addr0x0000008c: lbu x16, -180(x8)
addr0x00000090: sw x27, 734(x8)
addr0x00000094: addi x14, x20, -1392
addr0x00000098: sw x31, -1543(x8)
addr0x0000009c: jal x0, addr0x00000290
addr0x000000a0: lbu x20, -1070(x8)
addr0x000000a4: lui x18, 149392
addr0x000000a8: lui x14, 965446
addr0x000000ac: addi x25, x28, 1094
addr0x000000b0: jal x2, addr0x00000080
addr0x000000b4: lui x28, 242432
addr0x000000b8: sw x24, 1094(x8)
addr0x000000bc: sb x7, 1541(x8)
addr0x000000c0: lw x19, -1582(x8)
addr0x000000c4: add x12, x20, x2
addr0x000000c8: jal x25, addr0x00000f18
addr0x000000cc: lw x15, 946(x8)
addr0x000000d0: lw x29, 1812(x8)
addr0x000000d4: lw x29, 207(x8)
addr0x000000d8: lw x7, 837(x8)
addr0x000000dc: lw x28, 1553(x8)
addr0x000000e0: addi x2, x15, 1726
addr0x000000e4: jal x20, addr0x00000e28
addr0x000000e8: lw x22, 1472(x8)
addr0x000000ec: lw x23, -1980(x8)
addr0x000000f0: lui x18, 817887
addr0x000000f4: lui x19, 657854
addr0x000000f8: addi x14, x19, -527
addr0x000000fc: jal x22, addr0x00000340
addr0x00000100: sw x22, -1096(x8)
addr0x00000104: sw x13, 755(x8)
addr0x00000108: sb x30, -364(x8)
addr0x0000010c: jal x17, addr0x00000f28
addr0x00000110: andi x29, x10, -1825
addr0x00000114: sub x0, x27, x31
addr0x00000118: srai x25, x5, 21
addr0x0000011c: jal x18, addr0x00000cf4
addr0x00000120: lw x30, -204(x8)
addr0x00000124: lw x29, 207(x8)
addr0x00000128: sw x16, -1379(x8)
addr0x0000012c: lw x27, 1243(x8)
addr0x00000130: lw x31, 1574(x8)
addr0x00000134: addi x15, x24, 1218
addr0x00000138: sw x18, 1617(x8)
addr0x0000013c: addi x30, x21, -423
addr0x00000140: sw x23, -285(x8)
addr0x00000144: jalr x0, 1012(x3)
addr0x00000148: jal x24, addr0x00000184
addr0x0000014c: lw x7, -1270(x8)
addr0x00000150: sw x21, -1168(x8)
addr0x00000154: lw x24, -1884(x8)
addr0x00000158: lw x7, -1766(x8)
addr0x0000015c: addi x19, x24, -1896
addr0x00000160: add x17, x18, x30
addr0x00000164: lbu x24, 1902(x8)
addr0x00000168: addi x18, x17, -1755
addr0x0000016c: bne x20, x20, addr0x0000057c
addr0x00000170: lw x16, -1788(x8)
addr0x00000174: sh x29, 1552(x8)
addr0x00000178: addi x15, x30, 821
addr0x0000017c: sw x5, -1391(x8)
addr0x00000180: jal x4, addr0x00000570
addr0x00000184: lw x22, -1291(x8)
addr0x00000188: lw x28, -679(x8)
addr0x0000018c: lbu x20, 5(x8)
addr0x00000190: beq x20, x21, addr0x000003f4
addr0x00000194: lhu x7, 1522(x8)
addr0x00000198: sw x22, -1155(x8)
addr0x0000019c: sw x22, -708(x8)
addr0x000001a0: jal x27, addr0x00000d38
addr0x000001a4: lw x27, 57(x8)
addr0x000001a8: addi x23, x21, 418
addr0x000001ac: lui x22, 969091
addr0x000001b0: addi x31, x25, 1569
addr0x000001b4: addi x7, x14, 970
addr0x000001b8: addi x0, x17, -1569
addr0x000001bc: jal x18, addr0x00000dd4
addr0x000001c0: beq x19, x20, addr0x00000858
addr0x000001c4: jal x26, addr0x00000ea4
addr0x000001c8: sw x0, -1172(x8)
addr0x000001cc: sw x30, 1434(x8)
addr0x000001d0: sw x21, 584(x8)
addr0x000001d4: lw x17, 1825(x8)
addr0x000001d8: bgeu x5, x17, addr0x0000049c
addr0x000001dc: lw x0, -1269(x8)
addr0x000001e0: addi x2, x14, -51
addr0x000001e4: lui x9, 874065
addr0x000001e8: addi x22, x10, 1783
addr0x000001ec: addi x28, x9, 891
addr0x000001f0: jal x11, addr0x00000374
addr0x000001f4: lw x0, -730(x8)
addr0x000001f8: sw x19, -390(x8)
addr0x000001fc: sw x28, -1203(x8)
addr0x00000200: addi x21, x27, -1140
addr0x00000204: jal x9, addr0x000008c8
addr0x00000208: bne x1, x9, addr0x00000540
addr0x0000020c: jal x27, addr0x00000db0
addr0x00000210: lw x21, 1423(x8)
addr0x00000214: jal x21, addr0x00000090
addr0x00000218: lw x24, 1715(x8)
addr0x0000021c: lw x4, -212(x8)
addr0x00000220: sw x23, 1140(x8)
addr0x00000224: jal x28, addr0x000000b8
addr0x00000228: jal x7, addr0x00000538
addr0x0000022c: jal x24, addr0x000002f0
addr0x00000230: addi x7, x5, -761
addr0x00000234: jal x9, addr0x000003f4
addr0x00000238: lui x23, 1005485
addr0x0000023c: lw x19, 1787(x8)
addr0x00000240: jal x25, addr0x00000310
addr0x00000244: lw x21, -1777(x8)
addr0x00000248: lw x23, -1865(x8)
addr0x0000024c: sw x22, -442(x8)
addr0x00000250: sw x7, 789(x8)
addr0x00000254: sw x0, -1811(x8)
addr0x00000258: jal x22, addr0x00000608
addr0x0000025c: bne x5, x7, addr0x00000500
addr0x00000260: lw x0, 670(x8)
addr0x00000264: sw x15, 387(x8)
addr0x00000268: jal x23, addr0x0000073c
addr0x0000026c: sw x19, 1630(x8)
addr0x00000270: addi x22, x26, -932
addr0x00000274: sw x19, 1570(x8)
addr0x00000278: lw x7, 375(x8)
addr0x0000027c: lbu x18, 1169(x8)
addr0x00000280: addi x12, x0, 1616
addr0x00000284: jal x0, addr0x00000294
addr0x00000288: sw x31, 547(x8)
addr0x0000028c: lbu x22, 75(x8)
addr0x00000290: beq x21, x5, addr0x00000804
addr0x00000294: lw x22, 1846(x8)
addr0x00000298: bne x9, x13, addr0x000007e4
addr0x0000029c: lw x13, 1509(x8)
addr0x000002a0: andi x12, x28, 596
addr0x000002a4: jal x6, addr0x00000254
addr0x000002a8: sub x27, x14, x5
addr0x000002ac: srli x19, x18, 2
addr0x000002b0: lw x24, 783(x8)
addr0x000002b4: lw x0, -277(x8)
addr0x000002b8: lw x10, 1233(x8)
addr0x000002bc: lw x27, 1455(x8)
addr0x000002c0: lw x17, 1485(x8)
addr0x000002c4: sw x26, -3(x8)
addr0x000002c8: lw x20, 465(x8)
addr0x000002cc: lw x27, -1530(x8)
addr0x000002d0: jal x28, addr0x00000de0
addr0x000002d4: lw x9, 903(x8)
addr0x000002d8: sw x13, -140(x8)
addr0x000002dc: sw x7, 1249(x8)
addr0x000002e0: slli x25, x9, 23
addr0x000002e4: addi x7, x18, 189
addr0x000002e8: jal x17, addr0x00000464
addr0x000002ec: lui x12, 770793
addr0x000002f0: lui x15, 187539
addr0x000002f4: add x7, x1, x0
addr0x000002f8: lw x9, 937(x8)
addr0x000002fc: lw x29, -1928(x8)
addr0x00000300: lw x16, -292(x8)
addr0x00000304: lw x23, 1333(x8)
addr0x00000308: sw x19, 1698(x8)
addr0x0000030c: addi x21, x24, 1892
addr0x00000310: jal x20, addr0x00000288
addr0x00000314: jal x9, addr0x00000858
addr0x00000318: lw x15, 1143(x8)
addr0x0000031c: lw x19, 1242(x8)
addr0x00000320: lw x16, 857(x8)
addr0x00000324: lw x6, -1306(x8)
addr0x00000328: lbu x20, -1216(x8)
addr0x0000032c: addi x27, x13, 1634
addr0x00000330: lw x20, 1751(x8)
addr0x00000334: jal x20, addr0x00000650
addr0x00000338: lw x20, 898(x8)
addr0x0000033c: bltu x27, x30, addr0x000000d8
addr0x00000340: lw x4, -863(x8)
addr0x00000344: jal x5, addr0x00000e4c
addr0x00000348: addi x5, x16, 522
addr0x0000034c: lw x18, 815(x8)
addr0x00000350: addi x11, x13, -1744
addr0x00000354: bltu x31, x1, addr0x00000564
addr0x00000358: sw x17, -1943(x8)
addr0x0000035c: addi x19, x23, 92
addr0x00000360: jal x31, addr0x00000a6c
addr0x00000364: lw x23, -1555(x8)
addr0x00000368: bne x23, x1, addr0x000009d0
addr0x0000036c: lbu x18, -2047(x8)
addr0x00000370: addi x1, x12, 316
addr0x00000374: bge x26, x20, addr0x00000228
addr0x00000378: lw x29, -646(x8)
addr0x0000037c: sw x19, -758(x8)
addr0x00000380: jal x31, addr0x00000af4
addr0x00000384: bltu x22, x30, addr0x00000ac4
addr0x00000388: addi x29, x14, -1087
addr0x0000038c: addi x20, x23, -88
addr0x00000390: add x15, x18, x27
addr0x00000394: jal x20, addr0x00000e30
addr0x00000398: sw x23, -1200(x8)
addr0x0000039c: sw x22, 432(x8)
addr0x000003a0: lw x2, -238(x8)
addr0x000003a4: addi x22, x22, -1253
addr0x000003a8: sw x31, -1334(x8)
addr0x000003ac: addi x26, x30, 374
addr0x000003b0: lw x9, 541(x8)
addr0x000003b4: lbu x22, -879(x8)
addr0x000003b8: andi x24, x16, 154
addr0x000003bc: lw x23, -579(x8)
addr0x000003c0: jal x20, addr0x00000d18
addr0x000003c4: bge x19, x22, addr0x0000067c
addr0x000003c8: lw x11, -478(x8)
addr0x000003cc: lw x24, 1668(x8)
addr0x000003d0: sb x24, -651(x8)
addr0x000003d4: jalr x0, 788(x3)
addr0x000003d8: beq x17, x23, addr0x000000a0
addr0x000003dc: addi x9, x2, 441
addr0x000003e0: lw x13, 160(x8)
addr0x000003e4: lw x29, -1214(x8)
addr0x000003e8: addi x28, x18, 330
addr0x000003ec: jal x20, addr0x0000050c
addr0x000003f0: jal x19, addr0x000004ec
addr0x000003f4: lw x1, 1573(x8)
addr0x000003f8: lw x18, -294(x8)
addr0x000003fc: jal x26, addr0x00000444
addr0x00000400: lw x23, -876(x8)
addr0x00000404: lw x26, -604(x8)
addr0x00000408: lw x19, -357(x8)
addr0x0000040c: addi x17, x24, 308
addr0x00000410: lw x9, 1340(x8)
addr0x00000414: add x0, x19, x0
addr0x00000418: addi x13, x19, -75
addr0x0000041c: jal x28, addr0x0000013c
addr0x00000420: andi x22, x25, -278
addr0x00000424: addi x7, x29, -884
addr0x00000428: addi x31, x18, -145
addr0x0000042c: jal x31, addr0x000007f0
addr0x00000430: jal x15, addr0x00000ca8
addr0x00000434: lw x23, -152(x8)
addr0x00000438: lw x6, -1351(x8)
addr0x0000043c: sw x0, 1245(x8)
addr0x00000440: lw x28, -695(x8)
addr0x00000444: lbu x9, 1931(x8)
addr0x00000448: jal x26, addr0x00000d60
addr0x0000044c: sw x18, 258(x8)
addr0x00000450: lui x14, 831805
addr0x00000454: sw x10, 1232(x8)
addr0x00000458: sw x10, -415(x8)
addr0x0000045c: sw x25, -88(x8)
addr0x00000460: sw x28, -51(x8)
addr0x00000464: sw x4, 2015(x8)
addr0x00000468: sw x17, -894(x8)
addr0x0000046c: addi x5, x16, -1792
addr0x00000470: addi x13, x2, -1461
addr0x00000474: sw x23, 847(x8)
addr0x00000478: blt x24, x19, addr0x00000a48
addr0x0000047c: beq x19, x10, addr0x0000060c
addr0x00000480: bltu x4, x2, addr0x00000864
addr0x00000484: lui x31, 382995
addr0x00000488: blt x22, x9, addr0x0000056c
addr0x0000048c: lw x16, -1623(x8)
addr0x00000490: lui x20, 865161
addr0x00000494: addi x29, x28, 1046
addr0x00000498: beq x7, x29, addr0x000007fc
addr0x0000049c: auipc x18, 732539
addr0x000004a0: addi x5, x14, -931
addr0x000004a4: lw x17, 737(x8)
addr0x000004a8: slli x5, x29, 18
addr0x000004ac: srli x7, x22, 29
addr0x000004b0: add x23, x22, x20
addr0x000004b4: lw x7, 1802(x8)
addr0x000004b8: lw x22, 408(x8)
addr0x000004bc: add x19, x30, x24
addr0x000004c0: addi x21, x19, 1573
addr0x000004c4: bne x26, x1, addr0x00000c70
addr0x000004c8: addi x26, x25, 370
addr0x000004cc: lw x9, 808(x8)
addr0x000004d0: lw x29, 1573(x8)
addr0x000004d4: andi x0, x13, -1983
addr0x000004d8: jal x24, addr0x00000ddc
addr0x000004dc: beq x31, x18, addr0x000005f4
addr0x000004e0: lw x25, -36(x8)
addr0x000004e4: jal x10, addr0x000001c4
addr0x000004e8: lw x14, -968(x8)
addr0x000004ec: add x10, x22, x15
addr0x000004f0: sub x19, x21, x23
addr0x000004f4: add x7, x14, x28
addr0x000004f8: jal x1, addr0x00000054
addr0x000004fc: addi x0, x31, -1634
addr0x00000500: beq x23, x4, addr0x00000ca4
addr0x00000504: beq x23, x23, addr0x00000b28
addr0x00000508: lbu x20, 1907(x8)
addr0x0000050c: sw x13, 1877(x8)
addr0x00000510: sw x31, -65(x8)
addr0x00000514: sw x14, 1660(x8)
addr0x00000518: bne x22, x20, addr0x00000af8
addr0x0000051c: jal x22, addr0x00000f48
addr0x00000520: jal x21, addr0x000006b0
addr0x00000524: bge x27, x19, addr0x000007f4
addr0x00000528: addi x29, x18, 946
addr0x0000052c: addi x17, x23, 1572
addr0x00000530: jal x18, addr0x00000124
addr0x00000534: addi x9, x4, 1566
addr0x00000538: lw x22, 327(x8)
addr0x0000053c: bgeu x27, x21, addr0x00000050
addr0x00000540: lw x7, -508(x8)
addr0x00000544: bne x17, x20, addr0x0000061c
addr0x00000548: addi x30, x4, 711
addr0x0000054c: sw x9, -1127(x8)
addr0x00000550: sw x7, 1282(x8)
addr0x00000554: sw x17, 749(x8)
addr0x00000558: lw x16, 1028(x8)
addr0x0000055c: addi x19, x30, -294
addr0x00000560: jal x1, addr0x000000e0
addr0x00000564: lui x17, 748704
addr0x00000568: addi x26, x10, 764
addr0x0000056c: bge x24, x1, addr0x00000514
addr0x00000570: slli x24, x4, 20
addr0x00000574: addi x2, x22, 1282
addr0x00000578: lw x20, 468(x8)
addr0x0000057c: lw x12, 995(x8)
addr0x00000580: lw x28, 676(x8)
addr0x00000584: jal x21, addr0x00000710
addr0x00000588: jal x9, addr0x00000cf0
addr0x0000058c: lw x29, -1724(x8)
addr0x00000590: sw x30, 1348(x8)
addr0x00000594: lw x23, 962(x8)
addr0x00000598: add x9, x25, x7
addr0x0000059c: jal x17, addr0x00000bac
addr0x000005a0: jal x10, addr0x00000f28
addr0x000005a4: andi x2, x31, 773
addr0x000005a8: sub x28, x31, x22
addr0x000005ac: addi x25, x20, 468
addr0x000005b0: bne x9, x13, addr0x000003d0
addr0x000005b4: lw x16, 620(x8)
addr0x000005b8: lw x19, 1307(x8)
addr0x000005bc: addi x18, x18, 332
addr0x000005c0: jal x19, addr0x0000026c
addr0x000005c4: jal x31, addr0x00000f20
addr0x000005c8: lw x29, 1059(x8)
addr0x000005cc: or x5, x23, x16
addr0x000005d0: add x17, x20, x23
addr0x000005d4: lw x20, -531(x8)
addr0x000005d8: lw x21, 966(x8)
addr0x000005dc: bne x7, x23, addr0x00000a28
addr0x000005e0: addi x16, x23, 1452
addr0x000005e4: lw x0, -616(x8)
addr0x000005e8: lw x26, -1277(x8)
addr0x000005ec: addi x29, x7, -1156
addr0x000005f0: sw x22, -763(x8)
addr0x000005f4: sw x27, -2044(x8)
addr0x000005f8: jal x10, addr0x00000b04
addr0x000005fc: jal x10, addr0x0000012c
addr0x00000600: beq x1, x7, addr0x00000254
addr0x00000604: bne x22, x7, addr0x00000020
addr0x00000608: addi x20, x18, 665
addr0x0000060c: jal x12, addr0x00000b28
addr0x00000610: lw x16, 693(x8)
addr0x00000614: addi x9, x28, 351
addr0x00000618: sw x7, -1554(x8)
addr0x0000061c: sw x0, 1276(x8)
addr0x00000620: sw x31, 1392(x8)
addr0x00000624: bne x12, x19, addr0x000005c0
addr0x00000628: addi x5, x24, 1396
addr0x0000062c: jal x30, addr0x0000023c
addr0x00000630: addi x22, x24, -186
addr0x00000634: addi x31, x22, 839
addr0x00000638: lhu x26, -230(x8)
addr0x0000063c: lhu x29, -1043(x8)
addr0x00000640: and x30, x0, x4
addr0x00000644: bne x12, x27, addr0x00000a68
addr0x00000648: lbu x0, 571(x8)
addr0x0000064c: auipc x1, 472338
addr0x00000650: lw x19, 1344(x8)
addr0x00000654: addi x12, x27, 644
addr0x00000658: sw x1, 498(x8)
addr0x0000065c: jal x24, addr0x00000fc0
addr0x00000660: lw x16, 1486(x8)
addr0x00000664: lui x9, 280021
addr0x00000668: addi x30, x23, 1451
addr0x0000066c: jal x14, addr0x00000a58
addr0x00000670: bgeu x20, x27, addr0x00000720
addr0x00000674: jal x26, addr0x000006f8
addr0x00000678: jal x17, addr0x000006b8
addr0x0000067c: addi x26, x9, 1080
addr0x00000680: sw x10, -1883(x8)
addr0x00000684: sw x9, 321(x8)
addr0x00000688: lw x29, 1972(x8)
addr0x0000068c: addi x25, x28, 204
addr0x00000690: addi x7, x10, -226
addr0x00000694: sw x5, -453(x8)
addr0x00000698: addi x2, x9, -1374
addr0x0000069c: lw x24, -1051(x8)
addr0x000006a0: lw x13, -1028(x8)
addr0x000006a4: lw x23, -1281(x8)
addr0x000006a8: sw x10, 1194(x8)
addr0x000006ac: sw x5, 1654(x8)
addr0x000006b0: sw x29, -1267(x8)
addr0x000006b4: sw x29, -899(x8)
addr0x000006b8: jal x16, addr0x000009b0
addr0x000006bc: bne x19, x22, addr0x0000072c
addr0x000006c0: addi x6, x28, 638
addr0x000006c4: lw x2, -1851(x8)
addr0x000006c8: jalr x0, 604(x3)
addr0x000006cc: sub x7, x18, x20
addr0x000006d0: sw x31, -103(x8)
addr0x000006d4: addi x18, x11, 808
addr0x000006d8: addi x15, x10, -935
addr0x000006dc: blt x24, x4, addr0x00000944
addr0x000006e0: jal x30, addr0x00000edc
addr0x000006e4: jal x22, addr0x000006d4
addr0x000006e8: addi x27, x18, 1838
addr0x000006ec: jal x10, addr0x00000a80
addr0x000006f0: lw x30, 817(x8)
addr0x000006f4: addi x14, x31, -142
addr0x000006f8: jal x9, addr0x00000198
addr0x000006fc: addi x19, x27, -1967
addr0x00000700: addi x24, x29, -1997
addr0x00000704: beq x13, x7, addr0x00000248
addr0x00000708: add x16, x23, x17
addr0x0000070c: jal x20, addr0x00000efc
addr0x00000710: lw x27, 774(x8)
addr0x00000714: jal x0, addr0x00000c48
addr0x00000718: sw x6, 365(x8)
addr0x0000071c: sw x20, -700(x8)
addr0x00000720: jal x26, addr0x00000ad0
addr0x00000724: sw x31, 820(x8)
addr0x00000728: jal x17, addr0x00000380
addr0x0000072c: lw x4, -1143(x8)
addr0x00000730: bne x21, x31, addr0x00000e08
addr0x00000734: bgeu x22, x2, addr0x000006d0
addr0x00000738: addi x5, x6, -577
addr0x0000073c: sw x22, 892(x8)
addr0x00000740: sw x23, -1660(x8)
addr0x00000744: jal x21, addr0x00000b70
addr0x00000748: addi x23, x19, 1618
addr0x0000074c: addi x14, x31, -501
addr0x00000750: bne x19, x24, addr0x00000590
addr0x00000754: lw x23, 457(x8)
addr0x00000758: lui x31, 337998
addr0x0000075c: sw x5, -311(x8)
addr0x00000760: sw x17, 464(x8)
addr0x00000764: addi x22, x15, 1852
addr0x00000768: bne x19, x5, addr0x00000980
addr0x0000076c: lh x17, -84(x8)
addr0x00000770: lui x17, 813683
addr0x00000774: addi x22, x31, 1471
addr0x00000778: bge x6, x27, addr0x00000eec
addr0x0000077c: addi x15, x17, 542
addr0x00000780: sw x22, -1207(x8)
addr0x00000784: slli x19, x21, 7
addr0x00000788: lw x0, 724(x8)
addr0x0000078c: addi x12, x7, 344
addr0x00000790: sw x19, -1288(x8)
addr0x00000794: lw x6, -1778(x8)
addr0x00000798: lw x22, -1814(x8)
addr0x0000079c: or x13, x20, x9
addr0x000007a0: andi x18, x18, 372
addr0x000007a4: addi x28, x15, -1767
addr0x000007a8: lw x11, -1240(x8)
addr0x000007ac: lw x16, 1251(x8)
addr0x000007b0: jal x0, addr0x000008e8
addr0x000007b4: jal x17, addr0x000007f8
addr0x000007b8: slli x24, x22, 22
addr0x000007bc: addi x27, x28, 2038
addr0x000007c0: jal x27, addr0x00000d28
addr0x000007c4: lw x0, 1352(x8)
addr0x000007c8: lw x10, -811(x8)
addr0x000007cc: bne x9, x31, addr0x00000928
addr0x000007d0: jal x27, addr0x0000085c
addr0x000007d4: sw x27, -231(x8)
addr0x000007d8: slli x31, x30, 19
addr0x000007dc: sw x2, 1072(x8)
addr0x000007e0: lw x23, -843(x8)
addr0x000007e4: lw x19, -1115(x8)
addr0x000007e8: add x5, x10, x27
addr0x000007ec: add x18, x18, x25
addr0x000007f0: jal x17, addr0x00000a94
addr0x000007f4: lw x23, -387(x8)
addr0x000007f8: sll x23, x2, x22
addr0x000007fc: sll x25, x19, x18
addr0x00000800: srl x27, x26, x0
addr0x00000804: sll x27, x10, x13
addr0x00000808: or x17, x13, x10
addr0x0000080c: slli x25, x17, 26
addr0x00000810: lw x25, 1959(x8)
addr0x00000814: addi x13, x0, 1014
addr0x00000818: lui x19, 17895
addr0x0000081c: bltu x22, x5, addr0x00000c60
addr0x00000820: lw x2, 1644(x8)
addr0x00000824: sw x22, 207(x8)
addr0x00000828: addi x20, x26, 1098
addr0x0000082c: addi x22, x27, 667
addr0x00000830: blt x20, x31, addr0x000001d0
addr0x00000834: ori x15, x22, 1033
addr0x00000838: lbu x12, 1736(x8)
addr0x0000083c: lw x29, 1037(x8)
addr0x00000840: andi x25, x1, 1645
addr0x00000844: lw x31, -874(x8)
addr0x00000848: andi x31, x13, 977
addr0x0000084c: auipc x23, 143593
addr0x00000850: addi x30, x5, -541
addr0x00000854: jal x30, addr0x00000e74
addr0x00000858: add x22, x23, x22
addr0x0000085c: jal x28, addr0x00000bb8
addr0x00000860: addi x14, x10, -919
addr0x00000864: lw x29, -337(x8)
addr0x00000868: lw x22, -2045(x8)
addr0x0000086c: lw x5, 632(x8)
addr0x00000870: lw x13, -1779(x8)
addr0x00000874: andi x0, x29, 231
addr0x00000878: addi x28, x26, -1888
addr0x0000087c: sw x5, -1903(x8)
addr0x00000880: lw x9, 1511(x8)
addr0x00000884: jal x10, addr0x00000284
addr0x00000888: addi x13, x18, 1503
addr0x0000088c: sw x19, -1651(x8)
addr0x00000890: sub x27, x13, x31
addr0x00000894: lw x27, -831(x8)
addr0x00000898: addi x14, x19, -1452
addr0x0000089c: addi x14, x17, -32
addr0x000008a0: bge x15, x30, addr0x000001e4
addr0x000008a4: lw x30, -759(x8)
addr0x000008a8: sub x20, x30, x23
addr0x000008ac: jal x15, addr0x00000e58
addr0x000008b0: sw x17, 40(x8)
addr0x000008b4: lbu x0, -1915(x8)
addr0x000008b8: andi x22, x17, -489
addr0x000008bc: jal x31, addr0x00000724
addr0x000008c0: lw x10, -276(x8)
addr0x000008c4: beq x0, x18, addr0x000007e8
addr0x000008c8: sw x19, -1663(x8)
addr0x000008cc: sw x28, -226(x8)
addr0x000008d0: sw x21, -1296(x8)
addr0x000008d4: lw x26, -177(x8)
addr0x000008d8: addi x23, x23, 1196
addr0x000008dc: addi x4, x28, 153
addr0x000008e0: addi x0, x5, 567
addr0x000008e4: addi x31, x7, 1882
addr0x000008e8: jal x30, addr0x00000840
addr0x000008ec: lw x14, -614(x8)
addr0x000008f0: addi x21, x29, 59
addr0x000008f4: sw x17, -1655(x8)
addr0x000008f8: lui x28, 401007
addr0x000008fc: slli x19, x22, 20
addr0x00000900: add x13, x23, x13
addr0x00000904: sw x23, -1906(x8)
addr0x00000908: sw x27, 1063(x8)
addr0x0000090c: lw x5, -480(x8)
addr0x00000910: lw x24, -1461(x8)
addr0x00000914: addi x19, x31, -141
addr0x00000918: lui x9, 481828
addr0x0000091c: lbu x22, -825(x8)
addr0x00000920: bne x29, x19, addr0x00000368
addr0x00000924: jal x29, addr0x00000c20
addr0x00000928: lui x22, 534328
addr0x0000092c: addi x9, x13, -2021
addr0x00000930: beq x0, x4, addr0x00000d70
addr0x00000934: add x23, x13, x24
addr0x00000938: jal x23, addr0x00000224
addr0x0000093c: addi x0, x21, -580
addr0x00000940: lw x15, -993(x8)
addr0x00000944: lw x13, 1845(x8)
addr0x00000948: lw x18, -1594(x8)
addr0x0000094c: lw x31, -459(x8)
addr0x00000950: addi x22, x13, 2010
addr0x00000954: addi x19, x15, 795
addr0x00000958: jal x19, addr0x00000a6c
addr0x0000095c: lw x24, -97(x8)
addr0x00000960: slti x29, x19, 1951
addr0x00000964: lw x19, -1267(x8)
addr0x00000968: lw x19, 1629(x8)
addr0x0000096c: lw x31, -1858(x8)
addr0x00000970: sub x18, x13, x7
addr0x00000974: addi x19, x27, 1498
addr0x00000978: addi x17, x19, -681
addr0x0000097c: jal x27, addr0x00000c30
addr0x00000980: beq x27, x25, addr0x00000e70
addr0x00000984: jal x5, addr0x00000a4c
addr0x00000988: jal x19, addr0x00000dd4
addr0x0000098c: lw x31, 906(x8)
addr0x00000990: addi x17, x17, -931
addr0x00000994: sw x1, -1856(x8)
addr0x00000998: addi x25, x9, 477
addr0x0000099c: sw x27, 161(x8)
addr0x000009a0: sw x31, -490(x8)
addr0x000009a4: jal x17, addr0x00000328
addr0x000009a8: lbu x23, -72(x8)
addr0x000009ac: lw x5, -831(x8)
addr0x000009b0: add x13, x29, x18
addr0x000009b4: jal x22, addr0x00000098
addr0x000009b8: lw x13, 293(x8)
addr0x000009bc: sltu x27, x17, x20
addr0x000009c0: or x5, x17, x15
addr0x000009c4: addi x23, x19, -628
addr0x000009c8: sw x27, 415(x8)
addr0x000009cc: lui x1, 360408
addr0x000009d0: addi x13, x20, 1771
addr0x000009d4: sw x22, -1578(x8)
addr0x000009d8: addi x23, x28, -1617
addr0x000009dc: jal x0, addr0x00000ddc
addr0x000009e0: sw x31, 1318(x8)
addr0x000009e4: sw x16, 1898(x8)
addr0x000009e8: sw x19, 2000(x8)
addr0x000009ec: addi x26, x13, 816
addr0x000009f0: sw x18, 49(x8)
addr0x000009f4: lw x15, 1156(x8)
addr0x000009f8: sw x22, 1128(x8)
addr0x000009fc: lw x21, 784(x8)
addr0x00000a00: lw x5, 1780(x8)
addr0x00000a04: lw x17, -1507(x8)
addr0x00000a08: lbu x10, -1079(x8)
addr0x00000a0c: lw x18, 1936(x8)
addr0x00000a10: lw x13, 1364(x8)
addr0x00000a14: lw x20, -1400(x8)
addr0x00000a18: lw x28, 1530(x8)
addr0x00000a1c: lw x13, -1300(x8)
addr0x00000a20: addi x17, x14, 1972
addr0x00000a24: addi x31, x19, -1174
addr0x00000a28: bltu x11, x30, addr0x00000648
addr0x00000a2c: bltu x16, x22, addr0x00000414
addr0x00000a30: lw x28, 417(x8)
addr0x00000a34: lw x4, -341(x8)
addr0x00000a38: addi x17, x7, -1222
addr0x00000a3c: addi x23, x19, 1143
addr0x00000a40: sw x24, -168(x8)
addr0x00000a44: lw x29, 225(x8)
addr0x00000a48: jal x0, addr0x00000800
addr0x00000a4c: jal x27, addr0x00000f54
addr0x00000a50: lw x18, 893(x8)
addr0x00000a54: lw x30, 1569(x8)
addr0x00000a58: lh x23, -1690(x8)
addr0x00000a5c: lbu x15, 585(x8)
addr0x00000a60: lui x19, 485926
addr0x00000a64: addi x19, x18, 1634
addr0x00000a68: addi x9, x23, 1123
addr0x00000a6c: bne x22, x18, addr0x00000f74
addr0x00000a70: lw x23, -1695(x8)
addr0x00000a74: addi x22, x29, 1304
addr0x00000a78: jal x27, addr0x00000ed8
addr0x00000a7c: jal x13, addr0x00000d24
addr0x00000a80: sw x14, -1095(x8)
addr0x00000a84: lw x13, 1325(x8)
addr0x00000a88: slli x23, x22, 14
addr0x00000a8c: srli x13, x23, 19
addr0x00000a90: sub x11, x23, x7
addr0x00000a94: bgeu x30, x23, addr0x00000d9c
addr0x00000a98: slli x18, x18, 9
addr0x00000a9c: jal x11, addr0x00000a48
addr0x00000aa0: lw x25, 1541(x8)
addr0x00000aa4: lw x19, -297(x8)
addr0x00000aa8: lw x31, 1140(x8)
addr0x00000aac: and x22, x20, x5
addr0x00000ab0: addi x19, x23, 249
addr0x00000ab4: jal x17, addr0x00000714
addr0x00000ab8: sw x18, -233(x8)
addr0x00000abc: sw x26, -164(x8)
addr0x00000ac0: sw x17, -547(x8)
addr0x00000ac4: sw x31, 1226(x8)
addr0x00000ac8: jal x0, addr0x000007cc
addr0x00000acc: jal x19, addr0x00000fa0
addr0x00000ad0: addi x22, x24, -71
addr0x00000ad4: sw x5, 1283(x8)
addr0x00000ad8: addi x11, x27, -1409
addr0x00000adc: jal x7, addr0x00000c2c
addr0x00000ae0: andi x23, x19, -1057
addr0x00000ae4: slli x0, x22, 3
addr0x00000ae8: srli x21, x5, 15
addr0x00000aec: sw x19, -1556(x8)
addr0x00000af0: jal x6, addr0x000002d4
addr0x00000af4: sw x24, -1141(x8)
addr0x00000af8: lw x0, -162(x8)
addr0x00000afc: sw x9, 330(x8)
addr0x00000b00: jal x23, addr0x0000034c
addr0x00000b04: bne x28, x9, addr0x00000b58
addr0x00000b08: andi x17, x17, 1581
addr0x00000b0c: ori x2, x19, -1643
addr0x00000b10: sw x9, -1022(x8)
addr0x00000b14: lw x13, 488(x8)
addr0x00000b18: andi x13, x1, 1108
addr0x00000b1c: lbu x31, 1862(x8)
addr0x00000b20: lh x22, 1350(x8)
addr0x00000b24: jal x29, addr0x00000a9c
addr0x00000b28: jal x9, addr0x00000a00
addr0x00000b2c: addi x0, x31, -197
addr0x00000b30: sw x9, 409(x8)
addr0x00000b34: lw x19, 889(x8)
addr0x00000b38: lw x29, 1246(x8)
addr0x00000b3c: sub x6, x4, x17
addr0x00000b40: sw x27, 788(x8)
addr0x00000b44: sw x22, -1320(x8)
addr0x00000b48: sub x20, x7, x14
addr0x00000b4c: addi x18, x4, 669
addr0x00000b50: sub x14, x18, x29
addr0x00000b54: bgeu x31, x11, addr0x00000eb4
addr0x00000b58: bltu x29, x28, addr0x00000aa0
addr0x00000b5c: sw x14, -1367(x8)
addr0x00000b60: sw x26, 697(x8)
addr0x00000b64: sw x19, -1168(x8)
addr0x00000b68: jal x24, addr0x00000f90
addr0x00000b6c: or x24, x14, x13
addr0x00000b70: andi x27, x6, 1645
addr0x00000b74: lw x20, -1234(x8)
addr0x00000b78: addi x0, x7, -1021
addr0x00000b7c: and x28, x5, x28
addr0x00000b80: or x23, x7, x22
addr0x00000b84: jal x18, addr0x00000e08
addr0x00000b88: addi x28, x23, 1622
addr0x00000b8c: lw x13, 785(x8)
addr0x00000b90: sw x28, 1644(x8)
addr0x00000b94: jal x10, addr0x00000768
addr0x00000b98: beq x24, x27, addr0x000004f4
addr0x00000b9c: sw x17, -639(x8)
addr0x00000ba0: sw x15, 1549(x8)
addr0x00000ba4: lw x22, 1215(x8)
addr0x00000ba8: lw x15, -1342(x8)
addr0x00000bac: addi x25, x17, -1884
addr0x00000bb0: jal x1, addr0x0000008c
addr0x00000bb4: lw x28, -442(x8)
addr0x00000bb8: sw x19, 102(x8)
addr0x00000bbc: addi x21, x23, -1407
addr0x00000bc0: sw x30, 1025(x8)
addr0x00000bc4: jal x23, addr0x000000b8
addr0x00000bc8: sw x19, -989(x8)
addr0x00000bcc: addi x9, x12, 672
addr0x00000bd0: addi x20, x21, -1842
addr0x00000bd4: lw x9, 1443(x8)
addr0x00000bd8: lw x23, -1475(x8)
addr0x00000bdc: sw x17, -996(x8)
addr0x00000be0: sw x18, -1247(x8)
addr0x00000be4: jal x11, addr0x00000a44
addr0x00000be8: addi x31, x20, -459
addr0x00000bec: addi x15, x17, -533
addr0x00000bf0: jal x19, addr0x000007a8
addr0x00000bf4: addi x28, x24, 616
addr0x00000bf8: sw x9, -1696(x8)
addr0x00000bfc: blt x19, x13, addr0x00000fa8
addr0x00000c00: andi x9, x26, -319
addr0x00000c04: bge x4, x2, addr0x00000f44
addr0x00000c08: beq x29, x22, addr0x00000fcc
addr0x00000c0c: lw x31, -1534(x8)
addr0x00000c10: lw x9, 1888(x8)
addr0x00000c14: add x23, x22, x20
addr0x00000c18: jal x23, addr0x00000960
addr0x00000c1c: addi x16, x28, 259
addr0x00000c20: jal x23, addr0x00000d9c
addr0x00000c24: sw x2, -216(x8)
addr0x00000c28: addi x13, x28, -991
addr0x00000c2c: bne x30, x22, addr0x00000540
addr0x00000c30: lw x15, 293(x8)
addr0x00000c34: andi x22, x4, -606
addr0x00000c38: andi x14, x24, 1196
addr0x00000c3c: beq x25, x28, addr0x000007f8
addr0x00000c40: lhu x7, 232(x8)
addr0x00000c44: sltu x21, x19, x18
addr0x00000c48: add x15, x29, x7
addr0x00000c4c: jal x9, addr0x000007f0
addr0x00000c50: lw x13, 1307(x8)
addr0x00000c54: mul x14, x6, x26
addr0x00000c58: srli x27, x13, 26
addr0x00000c5c: sw x22, -1917(x8)
addr0x00000c60: sb x0, -1759(x8)
addr0x00000c64: beq x16, x4, addr0x0000091c
addr0x00000c68: slli x25, x29, 21
addr0x00000c6c: slli x7, x17, 13
addr0x00000c70: add x25, x13, x12
addr0x00000c74: lbu x14, 727(x8)
addr0x00000c78: lw x20, 356(x8)
addr0x00000c7c: addi x23, x12, 1805
addr0x00000c80: jal x15, addr0x00000cb0
addr0x00000c84: lw x11, -584(x8)
addr0x00000c88: addi x2, x22, -687
addr0x00000c8c: sw x18, 2003(x8)
addr0x00000c90: addi x29, x24, 770
addr0x00000c94: lw x0, 875(x8)
addr0x00000c98: andi x30, x13, -942
addr0x00000c9c: lw x19, 1938(x8)
addr0x00000ca0: jal x14, addr0x0000069c
addr0x00000ca4: addi x17, x21, -78
addr0x00000ca8: jal x22, addr0x00000f14
addr0x00000cac: jal x5, addr0x000004e4
addr0x00000cb0: addi x22, x23, 1766
addr0x00000cb4: lui x16, 631855
addr0x00000cb8: addi x19, x4, -637
addr0x00000cbc: jal x9, addr0x000004d8
addr0x00000cc0: lhu x17, -730(x8)
addr0x00000cc4: sw x20, 280(x8)
addr0x00000cc8: lw x22, 889(x8)
addr0x00000ccc: bne x29, x24, addr0x00000684
addr0x00000cd0: addi x14, x19, 84
addr0x00000cd4: addi x18, x21, -1132
addr0x00000cd8: jal x23, addr0x00000bb4
addr0x00000cdc: lw x18, -53(x8)
addr0x00000ce0: sw x18, -349(x8)
addr0x00000ce4: lui x29, 252284
addr0x00000ce8: addi x29, x29, -1745
addr0x00000cec: lw x15, -138(x8)
addr0x00000cf0: sb x2, -864(x8)
addr0x00000cf4: sw x23, -924(x8)
addr0x00000cf8: sw x29, -23(x8)
addr0x00000cfc: lbu x25, -582(x8)
addr0x00000d00: lw x6, -231(x8)
addr0x00000d04: jal x13, addr0x00000d0c
addr0x00000d08: lw x20, -1708(x8)
addr0x00000d0c: lw x15, -284(x8)
addr0x00000d10: sw x16, 1057(x8)
addr0x00000d14: sw x5, 548(x8)
addr0x00000d18: sw x20, -256(x8)
addr0x00000d1c: sw x29, -1993(x8)
addr0x00000d20: jal x9, addr0x0000081c
addr0x00000d24: lw x19, -69(x8)
addr0x00000d28: lw x17, -1182(x8)
addr0x00000d2c: beq x22, x5, addr0x00000df0
addr0x00000d30: bne x9, x14, addr0x00000fbc
addr0x00000d34: jal x22, addr0x00000244
addr0x00000d38: sw x9, -765(x8)
addr0x00000d3c: addi x23, x24, 1991
addr0x00000d40: addi x5, x16, 1434
addr0x00000d44: jal x31, addr0x00000b84
addr0x00000d48: lw x9, 554(x8)
addr0x00000d4c: addi x31, x11, -1953
addr0x00000d50: sw x0, 780(x8)
addr0x00000d54: sw x31, -1221(x8)
addr0x00000d58: lw x5, 316(x8)
addr0x00000d5c: jal x20, addr0x00000978
addr0x00000d60: lw x18, 1720(x8)
addr0x00000d64: sw x20, 1708(x8)
addr0x00000d68: lw x20, -1489(x8)
addr0x00000d6c: sw x7, -1229(x8)
addr0x00000d70: addi x14, x15, -615
addr0x00000d74: jal x27, addr0x00000e98
addr0x00000d78: lw x13, -1149(x8)
addr0x00000d7c: lw x17, 348(x8)
addr0x00000d80: add x20, x9, x9
addr0x00000d84: lw x13, 1277(x8)
addr0x00000d88: jal x9, addr0x00000a08
addr0x00000d8c: bgeu x31, x23, addr0x000006fc
addr0x00000d90: lbu x20, -1573(x8)
addr0x00000d94: lw x17, -605(x8)
addr0x00000d98: bne x7, x12, addr0x000008d0
addr0x00000d9c: srli x4, x19, 14
addr0x00000da0: jal x12, addr0x00000ab4
addr0x00000da4: addi x17, x21, 370
addr0x00000da8: bge x0, x9, addr0x00000954
addr0x00000dac: ori x9, x9, 1691
addr0x00000db0: sw x4, 831(x8)
addr0x00000db4: sw x25, -1055(x8)
addr0x00000db8: addi x30, x16, 1857
addr0x00000dbc: jal x13, addr0x0000039c
addr0x00000dc0: lbu x31, 447(x8)
addr0x00000dc4: beq x23, x29, addr0x00000714
addr0x00000dc8: add x12, x23, x22
addr0x00000dcc: lui x23, 126167
addr0x00000dd0: addi x16, x18, 767
addr0x00000dd4: sw x28, 1207(x8)
addr0x00000dd8: jal x22, addr0x0000056c
addr0x00000ddc: lw x9, -1352(x8)
addr0x00000de0: lw x24, -1771(x8)
addr0x00000de4: bge x11, x0, addr0x00000a34
addr0x00000de8: bltu x7, x20, addr0x00000b24
addr0x00000dec: lw x5, -2008(x8)
addr0x00000df0: lw x25, 56(x8)
addr0x00000df4: ori x14, x9, 781
addr0x00000df8: sw x13, 401(x8)
addr0x00000dfc: lw x14, -612(x8)
addr0x00000e00: jal x18, addr0x00000c5c
addr0x00000e04: lw x0, -1363(x8)
addr0x00000e08: sb x28, -1326(x8)
addr0x00000e0c: add x13, x29, x9
addr0x00000e10: lw x19, -859(x8)
addr0x00000e14: lw x20, 1523(x8)
addr0x00000e18: addi x27, x13, 1383
addr0x00000e1c: jal x9, addr0x00000bd0
addr0x00000e20: andi x10, x25, -292
addr0x00000e24: bgeu x17, x19, addr0x00000dc8
addr0x00000e28: jal x29, addr0x00000a64
addr0x00000e2c: jal x4, addr0x00000564
addr0x00000e30: lw x13, 1361(x8)
addr0x00000e34: lw x28, 1985(x8)
addr0x00000e38: add x28, x17, x16
addr0x00000e3c: jal x23, addr0x00000b4c
addr0x00000e40: lw x28, -1811(x8)
addr0x00000e44: jal x28, addr0x000005b8
addr0x00000e48: addi x16, x31, 900
addr0x00000e4c: or x10, x31, x30
addr0x00000e50: addi x7, x29, -1500
addr0x00000e54: jal x23, addr0x00000374
addr0x00000e58: lw x18, -1559(x8)
addr0x00000e5c: addi x5, x23, 1866
addr0x00000e60: sw x13, 1642(x8)
addr0x00000e64: addi x31, x25, -1979
addr0x00000e68: jal x29, addr0x00000df0
addr0x00000e6c: sw x26, 1133(x8)
addr0x00000e70: addi x7, x18, 98
addr0x00000e74: auipc x13, 556651
addr0x00000e78: lw x23, 749(x8)
addr0x00000e7c: lw x28, -1541(x8)
addr0x00000e80: lw x16, 105(x8)
addr0x00000e84: addi x15, x14, 912
addr0x00000e88: addi x6, x27, -703
addr0x00000e8c: lw x31, 907(x8)
addr0x00000e90: lw x27, -575(x8)
addr0x00000e94: lw x17, -1205(x8)
addr0x00000e98: addi x30, x11, 1661
addr0x00000e9c: add x18, x5, x7
addr0x00000ea0: sub x28, x7, x27
addr0x00000ea4: lw x21, 1537(x8)
addr0x00000ea8: jal x20, addr0x0000017c
addr0x00000eac: lw x6, 296(x8)
addr0x00000eb0: sb x20, -2018(x8)
addr0x00000eb4: lbu x24, -347(x8)
addr0x00000eb8: beq x19, x17, addr0x00000e0c
addr0x00000ebc: lw x9, -1391(x8)
addr0x00000ec0: sw x13, 1753(x8)
addr0x00000ec4: addi x9, x19, 871
addr0x00000ec8: slli x17, x9, 2
addr0x00000ecc: add x13, x13, x23
addr0x00000ed0: addi x21, x30, -1065
addr0x00000ed4: sw x14, -434(x8)
addr0x00000ed8: jal x0, addr0x00000570
addr0x00000edc: lui x27, 767765
addr0x00000ee0: addi x5, x7, -1686
addr0x00000ee4: sw x20, 980(x8)
addr0x00000ee8: sw x27, 1399(x8)
addr0x00000eec: jal x30, addr0x00000530
addr0x00000ef0: jal x19, addr0x00000230
addr0x00000ef4: sub x23, x31, x31
addr0x00000ef8: sh x13, -1960(x8)
addr0x00000efc: lw x22, 282(x8)
addr0x00000f00: lw x1, 1177(x8)
addr0x00000f04: add x29, x6, x9
addr0x00000f08: lw x31, 1585(x8)
addr0x00000f0c: lw x26, 1458(x8)
addr0x00000f10: sw x13, 650(x8)
addr0x00000f14: sw x17, 1195(x8)
addr0x00000f18: lw x13, -1891(x8)
addr0x00000f1c: lw x29, 1203(x8)
addr0x00000f20: addi x14, x27, -1483
addr0x00000f24: jal x0, addr0x00000800
addr0x00000f28: lw x10, -1215(x8)
addr0x00000f2c: sw x22, -77(x8)
addr0x00000f30: lw x29, -1103(x8)
addr0x00000f34: lw x30, -828(x8)
addr0x00000f38: beq x26, x13, addr0x00000e50
addr0x00000f3c: addi x27, x31, -318
addr0x00000f40: sw x24, 889(x8)
addr0x00000f44: jal x25, addr0x00000d84
addr0x00000f48: lw x24, -984(x8)
addr0x00000f4c: addi x20, x0, -1287
addr0x00000f50: sw x10, -296(x8)
addr0x00000f54: sw x17, -1168(x8)
addr0x00000f58: sw x13, 1706(x8)
addr0x00000f5c: sw x23, 116(x8)
addr0x00000f60: addi x7, x28, -1338
addr0x00000f64: andi x7, x27, 822
addr0x00000f68: ori x4, x19, -1172
addr0x00000f6c: lw x9, 481(x8)
addr0x00000f70: lui x17, 401637
addr0x00000f74: addi x30, x1, 422
addr0x00000f78: addi x0, x10, -1814
addr0x00000f7c: jal x31, addr0x00000da0
addr0x00000f80: jal x6, addr0x00000d84
addr0x00000f84: lw x15, 1934(x8)
addr0x00000f88: lw x9, 238(x8)
addr0x00000f8c: lw x7, 723(x8)
addr0x00000f90: addi x28, x7, -2026
addr0x00000f94: addi x23, x17, 1936
addr0x00000f98: jal x9, addr0x00000d44
addr0x00000f9c: sw x0, 665(x8)
addr0x00000fa0: sh x5, 491(x8)
addr0x00000fa4: sw x19, -1885(x8)
addr0x00000fa8: lw x7, 540(x8)
addr0x00000fac: addi x27, x20, -1329
addr0x00000fb0: sw x18, 1934(x8)
addr0x00000fb4: jal x18, addr0x0000078c
addr0x00000fb8: lw x18, 1683(x8)
addr0x00000fbc: lw x10, -2048(x8)
addr0x00000fc0: jal x19, addr0x000002f8
addr0x00000fc4: sb x19, 1748(x8)
addr0x00000fc8: jal x28, addr0x000007c4
addr0x00000fcc: beq x5, x28, addr0x00000c98
addr0x00000fd0: lw x11, 1741(x8)
addr0x00000fd4: lw x25, 1366(x8)
