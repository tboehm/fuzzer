.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sw x29, -920(x8)
addr0x0000001c: sw x7, 820(x8)
addr0x00000020: sw x14, -274(x8)
addr0x00000024: sw x0, 2013(x8)
addr0x00000028: sw x23, 227(x8)
addr0x0000002c: addi x20, x24, -1344
addr0x00000030: addi x29, x30, -1374
addr0x00000034: jal x1, addr0x00000064
addr0x00000038: lbu x31, -528(x8)
addr0x0000003c: xori x23, x18, -1816
addr0x00000040: andi x22, x22, 688
addr0x00000044: addi x19, x0, -432
addr0x00000048: sw x22, -550(x8)
addr0x0000004c: jal x5, addr0x000000cc
addr0x00000050: addi x17, x13, 363
addr0x00000054: sw x0, -56(x8)
addr0x00000058: addi x23, x7, 1220
addr0x0000005c: sw x4, 2014(x8)
addr0x00000060: sw x23, 758(x8)
addr0x00000064: lw x31, 727(x8)
addr0x00000068: lui x20, 628842
addr0x0000006c: lui x9, 3748
addr0x00000070: addi x24, x20, 1459
addr0x00000074: bne x0, x9, addr0x0000007c
addr0x00000078: lw x19, -811(x8)
addr0x0000007c: addi x14, x13, 1312
addr0x00000080: lui x22, 851802
addr0x00000084: addi x13, x7, 1212
addr0x00000088: sw x31, 1234(x8)
addr0x0000008c: lui x6, 220129
addr0x00000090: jal x20, addr0x000000c8
addr0x00000094: addi x22, x0, -1468
addr0x00000098: sw x7, -389(x8)
addr0x0000009c: jal x22, addr0x000000d0
addr0x000000a0: lui x9, 338812
addr0x000000a4: sw x6, 1440(x8)
addr0x000000a8: sw x19, -1538(x8)
addr0x000000ac: sw x21, 309(x8)
addr0x000000b0: sw x6, -1291(x8)
addr0x000000b4: sw x31, -389(x8)
addr0x000000b8: jal x29, addr0x00000100
addr0x000000bc: jal x31, addr0x000000dc
addr0x000000c0: bne x31, x5, addr0x00000084
addr0x000000c4: or x0, x0, x13
addr0x000000c8: lbu x31, -1078(x8)
addr0x000000cc: lw x9, -935(x8)
addr0x000000d0: sw x14, -1812(x8)
addr0x000000d4: sw x24, 1259(x8)
addr0x000000d8: lw x17, -1229(x8)
addr0x000000dc: add x29, x5, x5
addr0x000000e0: jal x19, addr0x00000028
addr0x000000e4: slli x14, x9, 1
addr0x000000e8: lw x27, -1383(x8)
addr0x000000ec: lw x19, 179(x8)
addr0x000000f0: addi x20, x21, -67
addr0x000000f4: addi x19, x19, -1649
addr0x000000f8: blt x31, x2, addr0x000000e0
addr0x000000fc: jal x7, addr0x00000038
addr0x00000100: lw x19, -1863(x8)
addr0x00000104: lw x0, -545(x8)
addr0x00000108: slli x29, x12, 12
addr0x0000010c: lw x4, -723(x8)
addr0x00000110: jal x13, addr0x0000011c
addr0x00000114: lui x21, 713910
addr0x00000118: addi x17, x7, -1775
addr0x0000011c: addi x0, x2, 461
addr0x00000120: lbu x0, -2014(x8)
addr0x00000124: slli x7, x22, 3
addr0x00000128: add x7, x14, x19
addr0x0000012c: jal x0, addr0x000000cc
addr0x00000130: sw x4, 1513(x8)
addr0x00000134: sw x31, -1777(x8)
addr0x00000138: jal x23, addr0x000000ec
addr0x0000013c: sb x7, 529(x8)
addr0x00000140: lbu x16, -630(x8)
addr0x00000144: sw x17, 728(x8)
