.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: slli x10, x17, 11
addr0x0000001c: sh x31, -1748(x8)
addr0x00000020: slli x24, x15, 17
addr0x00000024: add x27, x17, x19
addr0x00000028: sw x7, 1133(x8)
addr0x0000002c: sw x17, 1509(x8)
addr0x00000030: sw x30, 1189(x8)
addr0x00000034: lw x26, 624(x8)
addr0x00000038: add x20, x13, x5
addr0x0000003c: lbu x9, 804(x8)
addr0x00000040: addi x23, x31, -884
addr0x00000044: jal x31, addr0x000000c8
addr0x00000048: lw x0, 925(x8)
addr0x0000004c: sw x30, -1167(x8)
addr0x00000050: sw x4, -2009(x8)
addr0x00000054: slli x29, x15, 2
addr0x00000058: lw x21, -1792(x8)
addr0x0000005c: addi x11, x15, -1257
addr0x00000060: beq x23, x7, addr0x0000005c
addr0x00000064: jal x17, addr0x00000034
addr0x00000068: jal x7, addr0x00000058
addr0x0000006c: lw x4, -1320(x8)
addr0x00000070: lui x30, 747833
addr0x00000074: addi x6, x21, 1842
addr0x00000078: lui x13, 486746
addr0x0000007c: slli x15, x18, 7
addr0x00000080: srli x9, x29, 11
addr0x00000084: slli x22, x0, 26
addr0x00000088: lw x19, 374(x8)
addr0x0000008c: bltu x2, x7, addr0x000000a0
addr0x00000090: lw x0, -992(x8)
addr0x00000094: lw x17, 515(x8)
addr0x00000098: jal x29, addr0x000000a8
addr0x0000009c: addi x13, x27, -995
addr0x000000a0: jal x25, addr0x00000018
addr0x000000a4: sw x18, -1227(x8)
addr0x000000a8: sw x0, 956(x8)
addr0x000000ac: bltu x16, x19, addr0x000000c0
addr0x000000b0: addi x23, x21, 1229
addr0x000000b4: jal x2, addr0x00000028
addr0x000000b8: lw x20, -1936(x8)
addr0x000000bc: lw x29, 1142(x8)
addr0x000000c0: lw x7, -1213(x8)
addr0x000000c4: lw x21, 1067(x8)
addr0x000000c8: jal x17, addr0x00000010
addr0x000000cc: addi x2, x9, 1561
addr0x000000d0: sw x14, 1342(x8)
addr0x000000d4: sw x29, -1394(x8)
addr0x000000d8: jal x25, addr0x00000058
