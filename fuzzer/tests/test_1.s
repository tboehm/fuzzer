.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: ecall 
addr0x0000001c: addi x23, x1, 1408
addr0x00000020: addi x23, x0, -766
addr0x00000024: sw x16, 1861(x8)
addr0x00000028: sw x16, -2016(x8)
addr0x0000002c: sw x22, 158(x8)
addr0x00000030: bge x17, x0, addr0x0000008c
addr0x00000034: lw x5, 1557(x8)
addr0x00000038: add x19, x22, x4
addr0x0000003c: lw x17, -1310(x8)
addr0x00000040: addi x23, x9, 853
addr0x00000044: lw x23, 796(x8)
addr0x00000048: slli x1, x1, 11
addr0x0000004c: lw x9, 778(x8)
addr0x00000050: ori x19, x9, -1612
addr0x00000054: sh x19, -1010(x8)
addr0x00000058: lw x19, -1668(x8)
addr0x0000005c: lui x6, 502090
addr0x00000060: addi x19, x22, 1492
addr0x00000064: lui x0, 708605
addr0x00000068: sw x18, -1170(x8)
addr0x0000006c: sw x23, -867(x8)
addr0x00000070: or x5, x6, x0
addr0x00000074: jal x15, addr0x00000088
addr0x00000078: lbu x16, -117(x8)
addr0x0000007c: lbu x1, 555(x8)
addr0x00000080: lbu x18, -1129(x8)
addr0x00000084: slli x5, x5, 26
addr0x00000088: add x18, x23, x19
addr0x0000008c: lw x29, -908(x8)
addr0x00000090: add x17, x19, x23
