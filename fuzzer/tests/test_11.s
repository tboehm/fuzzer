.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x4, 1815(x8)
addr0x0000001c: beq x5, x7, addr0x00000398
addr0x00000020: beq x29, x9, addr0x00000278
addr0x00000024: lw x19, -1057(x8)
addr0x00000028: sw x29, 1569(x8)
addr0x0000002c: sw x1, 1440(x8)
addr0x00000030: addi x17, x7, 1639
addr0x00000034: lui x29, 1031630
addr0x00000038: addi x22, x23, 1307
addr0x0000003c: sw x29, -34(x8)
addr0x00000040: sw x2, -1644(x8)
addr0x00000044: sub x29, x29, x16
addr0x00000048: bgeu x13, x27, addr0x0000013c
addr0x0000004c: addi x9, x0, -1543
addr0x00000050: sw x1, -1851(x8)
addr0x00000054: addi x13, x24, -1680
addr0x00000058: slli x12, x24, 25
addr0x0000005c: addi x22, x5, -1477
addr0x00000060: add x23, x2, x14
addr0x00000064: and x7, x5, x5
addr0x00000068: lui x0, 865539
addr0x0000006c: addi x22, x1, -560
addr0x00000070: slli x18, x16, 6
addr0x00000074: lw x0, 1334(x8)
addr0x00000078: jal x19, addr0x00000544
addr0x0000007c: jalr x0, 328(x3)
addr0x00000080: lw x22, -1608(x8)
addr0x00000084: sw x22, -1811(x8)
addr0x00000088: sw x22, -1698(x8)
addr0x0000008c: sb x19, -97(x8)
addr0x00000090: sw x1, -921(x8)
addr0x00000094: beq x9, x18, addr0x0000034c
addr0x00000098: sw x9, 1330(x8)
addr0x0000009c: bgeu x17, x7, addr0x000007b8
addr0x000000a0: addi x22, x19, 282
addr0x000000a4: sw x22, 1967(x8)
addr0x000000a8: addi x29, x23, 462
addr0x000000ac: jal x22, addr0x000000c4
addr0x000000b0: addi x18, x0, 486
addr0x000000b4: addi x24, x18, -683
addr0x000000b8: sw x13, 1823(x8)
addr0x000000bc: jal x18, addr0x000002e4
addr0x000000c0: jal x4, addr0x00000768
addr0x000000c4: sw x0, 1642(x8)
addr0x000000c8: sw x31, 1531(x8)
addr0x000000cc: sw x6, 1232(x8)
addr0x000000d0: addi x6, x23, -294
addr0x000000d4: lui x1, 1032755
addr0x000000d8: addi x13, x13, 1539
addr0x000000dc: lui x30, 978451
addr0x000000e0: addi x29, x22, 1638
addr0x000000e4: addi x7, x6, 1472
addr0x000000e8: add x19, x0, x29
addr0x000000ec: bne x25, x19, addr0x00000038
addr0x000000f0: lw x27, 1113(x8)
addr0x000000f4: lw x0, 1753(x8)
addr0x000000f8: lw x30, -1063(x8)
addr0x000000fc: lw x24, -788(x8)
addr0x00000100: slli x9, x30, 0
addr0x00000104: slli x7, x11, 7
addr0x00000108: sub x22, x13, x14
addr0x0000010c: addi x23, x13, -952
addr0x00000110: jal x13, addr0x00000294
addr0x00000114: lw x11, -1129(x8)
addr0x00000118: add x18, x0, x9
addr0x0000011c: jal x29, addr0x000005e8
addr0x00000120: lw x9, 573(x8)
addr0x00000124: lw x30, 94(x8)
addr0x00000128: jal x0, addr0x000007e4
addr0x0000012c: addi x23, x5, -1801
addr0x00000130: sw x15, -1953(x8)
addr0x00000134: jalr x0, 192(x3)
addr0x00000138: blt x13, x29, addr0x00000748
addr0x0000013c: sh x30, -1975(x8)
addr0x00000140: lhu x20, -532(x8)
addr0x00000144: addi x15, x7, -1838
addr0x00000148: jal x13, addr0x0000004c
addr0x0000014c: sub x23, x2, x10
addr0x00000150: bgeu x14, x18, addr0x00000080
addr0x00000154: lw x0, 1510(x8)
addr0x00000158: sw x23, -1075(x8)
addr0x0000015c: andi x2, x22, -1760
addr0x00000160: addi x9, x7, 1546
addr0x00000164: addi x22, x9, 14
addr0x00000168: addi x2, x20, 159
addr0x0000016c: lw x29, -652(x8)
addr0x00000170: sw x23, -283(x8)
addr0x00000174: addi x6, x7, -1399
addr0x00000178: lw x16, 580(x8)
addr0x0000017c: lw x0, -1735(x8)
addr0x00000180: sw x13, 170(x8)
addr0x00000184: addi x19, x9, 907
addr0x00000188: sw x9, -1691(x8)
addr0x0000018c: lw x9, -1312(x8)
addr0x00000190: sw x5, 1994(x8)
addr0x00000194: sw x29, 314(x8)
addr0x00000198: addi x19, x24, 121
addr0x0000019c: bgeu x24, x21, addr0x0000046c
addr0x000001a0: addi x23, x12, 68
addr0x000001a4: srli x19, x22, 11
addr0x000001a8: bltu x19, x23, addr0x000001b8
addr0x000001ac: addi x7, x0, 152
addr0x000001b0: jal x5, addr0x000006f8
addr0x000001b4: lw x17, 8(x8)
addr0x000001b8: lw x14, -1198(x8)
addr0x000001bc: addi x27, x24, 552
addr0x000001c0: jal x20, addr0x00000218
addr0x000001c4: sw x0, 1916(x8)
addr0x000001c8: lw x1, -1848(x8)
addr0x000001cc: lw x19, 1721(x8)
addr0x000001d0: lw x10, 963(x8)
addr0x000001d4: lw x14, -1340(x8)
addr0x000001d8: lw x30, 1829(x8)
addr0x000001dc: addi x0, x25, 758
addr0x000001e0: lui x22, 113828
addr0x000001e4: slli x13, x19, 26
addr0x000001e8: lw x29, 19(x8)
addr0x000001ec: jal x24, addr0x00000644
addr0x000001f0: lbu x20, -688(x8)
addr0x000001f4: slli x9, x31, 3
addr0x000001f8: addi x23, x31, -1700
addr0x000001fc: addi x24, x22, -2020
addr0x00000200: and x20, x9, x31
addr0x00000204: sw x1, 1542(x8)
addr0x00000208: sw x21, 1541(x8)
addr0x0000020c: lw x0, 1847(x8)
addr0x00000210: lw x29, 31(x8)
addr0x00000214: lw x19, -1122(x8)
addr0x00000218: or x5, x13, x21
addr0x0000021c: beq x7, x22, addr0x000007bc
addr0x00000220: jal x22, addr0x00000204
addr0x00000224: lw x24, -1432(x8)
addr0x00000228: lw x19, 207(x8)
addr0x0000022c: lw x23, -635(x8)
addr0x00000230: lw x19, 1598(x8)
addr0x00000234: lw x29, 418(x8)
addr0x00000238: addi x0, x7, 1649
addr0x0000023c: jal x15, addr0x00000094
addr0x00000240: sw x16, -161(x8)
addr0x00000244: lw x23, 1765(x8)
addr0x00000248: beq x19, x13, addr0x00000710
addr0x0000024c: lw x23, -727(x8)
addr0x00000250: sw x2, -1704(x8)
addr0x00000254: sb x20, -613(x8)
addr0x00000258: sw x21, -128(x8)
addr0x0000025c: jalr x0, 816(x3)
addr0x00000260: addi x7, x29, 224
addr0x00000264: lw x27, -1501(x8)
addr0x00000268: lw x19, 1342(x8)
addr0x0000026c: jal x19, addr0x00000184
addr0x00000270: addi x0, x21, 601
addr0x00000274: sw x4, 405(x8)
addr0x00000278: lw x23, 245(x8)
addr0x0000027c: lw x30, -1502(x8)
addr0x00000280: lbu x0, -1672(x8)
addr0x00000284: slli x23, x9, 4
addr0x00000288: lw x24, -1052(x8)
addr0x0000028c: lw x23, -740(x8)
addr0x00000290: sw x15, -1735(x8)
addr0x00000294: sw x24, 1044(x8)
addr0x00000298: lw x31, 1515(x8)
addr0x0000029c: bgeu x30, x2, addr0x0000004c
addr0x000002a0: lw x11, 661(x8)
addr0x000002a4: lw x23, 17(x8)
addr0x000002a8: lw x30, 47(x8)
addr0x000002ac: jal x29, addr0x000001dc
addr0x000002b0: addi x18, x29, 281
addr0x000002b4: sw x23, -528(x8)
addr0x000002b8: lw x24, 1720(x8)
addr0x000002bc: lw x20, -533(x8)
addr0x000002c0: sw x20, 1795(x8)
addr0x000002c4: jal x7, addr0x00000178
addr0x000002c8: lw x19, -689(x8)
addr0x000002cc: addi x14, x24, 1664
addr0x000002d0: jal x30, addr0x00000770
addr0x000002d4: addi x30, x4, -1721
addr0x000002d8: slli x15, x4, 9
addr0x000002dc: addi x23, x19, 832
addr0x000002e0: jal x11, addr0x000004ec
addr0x000002e4: jal x23, addr0x00000308
addr0x000002e8: jal x9, addr0x00000230
addr0x000002ec: lw x31, 567(x8)
addr0x000002f0: sw x23, -1203(x8)
addr0x000002f4: lw x21, 1738(x8)
addr0x000002f8: sw x13, 448(x8)
addr0x000002fc: add x7, x23, x29
addr0x00000300: lw x30, -2042(x8)
addr0x00000304: lbu x23, 897(x8)
addr0x00000308: lw x21, 622(x8)
addr0x0000030c: jal x2, addr0x000007b4
addr0x00000310: sw x14, -1730(x8)
addr0x00000314: addi x30, x18, -779
addr0x00000318: sw x4, -118(x8)
addr0x0000031c: sw x29, 1382(x8)
addr0x00000320: sw x19, -1793(x8)
addr0x00000324: sw x14, 1557(x8)
addr0x00000328: addi x0, x23, -348
addr0x0000032c: lw x22, -1981(x8)
addr0x00000330: lw x7, 1554(x8)
addr0x00000334: addi x22, x7, 956
addr0x00000338: auipc x11, 923122
addr0x0000033c: sw x19, -1198(x8)
addr0x00000340: sw x4, -291(x8)
addr0x00000344: lw x19, 974(x8)
addr0x00000348: lw x14, -1464(x8)
addr0x0000034c: lw x9, 1958(x8)
addr0x00000350: slli x0, x21, 28
addr0x00000354: andi x30, x1, 1615
addr0x00000358: bgeu x29, x14, addr0x0000021c
addr0x0000035c: add x0, x19, x22
addr0x00000360: lw x23, -1747(x8)
addr0x00000364: addi x4, x31, -1309
addr0x00000368: addi x17, x0, -792
addr0x0000036c: auipc x5, 219772
addr0x00000370: addi x9, x18, 611
addr0x00000374: beq x15, x13, addr0x0000044c
addr0x00000378: add x18, x29, x12
addr0x0000037c: lui x2, 231051
addr0x00000380: or x20, x18, x1
addr0x00000384: sw x18, 1579(x8)
addr0x00000388: add x9, x4, x13
addr0x0000038c: jal x29, addr0x00000230
addr0x00000390: sw x1, -1644(x8)
addr0x00000394: sw x9, -1442(x8)
addr0x00000398: sw x0, 513(x8)
addr0x0000039c: lw x13, 897(x8)
addr0x000003a0: lw x0, 145(x8)
addr0x000003a4: addi x12, x2, -1181
addr0x000003a8: addi x18, x1, -1350
addr0x000003ac: lw x20, -879(x8)
addr0x000003b0: sw x29, 1537(x8)
addr0x000003b4: sw x15, 1038(x8)
addr0x000003b8: sw x20, 1525(x8)
addr0x000003bc: jal x19, addr0x0000026c
addr0x000003c0: addi x21, x22, -1100
addr0x000003c4: jal x2, addr0x00000640
addr0x000003c8: sw x14, -1918(x8)
addr0x000003cc: addi x9, x13, 1181
addr0x000003d0: addi x20, x25, -502
addr0x000003d4: addi x5, x29, 1331
addr0x000003d8: jal x23, addr0x00000228
addr0x000003dc: jal x6, addr0x00000014
addr0x000003e0: jal x9, addr0x00000144
addr0x000003e4: lw x0, -1989(x8)
addr0x000003e8: addi x15, x18, -777
addr0x000003ec: sw x24, 350(x8)
addr0x000003f0: lui x29, 246942
addr0x000003f4: sw x24, 293(x8)
addr0x000003f8: jal x13, addr0x000001a8
addr0x000003fc: jal x30, addr0x00000160
addr0x00000400: sw x5, -496(x8)
addr0x00000404: sw x29, 123(x8)
addr0x00000408: bgeu x20, x0, addr0x00000564
addr0x0000040c: lw x18, 262(x8)
addr0x00000410: lw x22, -353(x8)
addr0x00000414: jal x13, addr0x00000170
addr0x00000418: jal x16, addr0x00000730
addr0x0000041c: bgeu x6, x4, addr0x00000388
addr0x00000420: bne x2, x9, addr0x0000029c
addr0x00000424: addi x20, x29, 1304
addr0x00000428: sw x29, -1411(x8)
addr0x0000042c: addi x6, x22, 1244
addr0x00000430: sw x19, 1418(x8)
addr0x00000434: lw x1, -1077(x8)
addr0x00000438: addi x0, x5, 1703
addr0x0000043c: jal x31, addr0x00000324
addr0x00000440: addi x22, x18, -880
addr0x00000444: addi x23, x4, 1970
addr0x00000448: addi x4, x6, -1850
addr0x0000044c: sw x17, -1956(x8)
addr0x00000450: sw x13, 609(x8)
addr0x00000454: jal x30, addr0x00000070
addr0x00000458: sub x20, x22, x23
addr0x0000045c: lbu x25, 550(x8)
addr0x00000460: slli x17, x6, 10
addr0x00000464: lw x23, 762(x8)
addr0x00000468: lui x23, 52287
addr0x0000046c: addi x5, x20, 128
addr0x00000470: jal x17, addr0x000001dc
addr0x00000474: lw x20, -585(x8)
addr0x00000478: jalr x0, 912(x3)
addr0x0000047c: bne x23, x1, addr0x00000018
addr0x00000480: bne x0, x1, addr0x00000394
addr0x00000484: bne x6, x4, addr0x000004c0
addr0x00000488: jal x4, addr0x00000184
addr0x0000048c: lw x23, -73(x8)
addr0x00000490: lw x13, 1880(x8)
addr0x00000494: lw x13, 1346(x8)
addr0x00000498: lw x16, -1602(x8)
addr0x0000049c: addi x13, x5, -1481
addr0x000004a0: bgeu x6, x29, addr0x000002c8
addr0x000004a4: lw x30, -941(x8)
addr0x000004a8: lw x17, -1056(x8)
addr0x000004ac: lw x7, 1868(x8)
addr0x000004b0: jal x23, addr0x0000003c
addr0x000004b4: sw x15, -437(x8)
addr0x000004b8: sw x18, -1838(x8)
addr0x000004bc: addi x2, x15, -1599
addr0x000004c0: beq x24, x29, addr0x00000524
addr0x000004c4: jal x4, addr0x00000460
addr0x000004c8: addi x23, x9, 1604
addr0x000004cc: jal x2, addr0x00000130
addr0x000004d0: lw x30, -56(x8)
addr0x000004d4: addi x24, x13, -996
addr0x000004d8: jal x14, addr0x00000238
addr0x000004dc: jal x29, addr0x0000044c
addr0x000004e0: jal x11, addr0x000001e0
addr0x000004e4: sw x20, 490(x8)
addr0x000004e8: jal x5, addr0x00000790
addr0x000004ec: jal x19, addr0x000002bc
addr0x000004f0: lw x20, 1613(x8)
addr0x000004f4: lw x17, -1117(x8)
addr0x000004f8: addi x21, x4, -874
addr0x000004fc: add x7, x19, x0
addr0x00000500: lw x17, 1906(x8)
addr0x00000504: addi x0, x22, 868
addr0x00000508: lw x13, 920(x8)
addr0x0000050c: lw x11, 329(x8)
addr0x00000510: addi x20, x15, 1690
addr0x00000514: jal x22, addr0x0000052c
addr0x00000518: sw x19, -1548(x8)
addr0x0000051c: addi x11, x13, -2020
addr0x00000520: jal x23, addr0x00000578
addr0x00000524: lw x7, -522(x8)
addr0x00000528: addi x4, x29, -1117
addr0x0000052c: andi x31, x24, -1378
addr0x00000530: jal x11, addr0x00000744
addr0x00000534: lw x22, 470(x8)
addr0x00000538: addi x7, x0, 1185
addr0x0000053c: sw x23, -1492(x8)
addr0x00000540: jal x7, addr0x00000354
addr0x00000544: lw x14, 593(x8)
addr0x00000548: lw x18, -504(x8)
addr0x0000054c: lw x15, 1465(x8)
addr0x00000550: lw x0, -1159(x8)
addr0x00000554: beq x5, x18, addr0x000005a0
addr0x00000558: lbu x19, 1653(x8)
addr0x0000055c: xori x7, x21, 1055
addr0x00000560: andi x19, x13, -1953
addr0x00000564: sw x22, -1171(x8)
addr0x00000568: sw x9, 370(x8)
addr0x0000056c: jal x0, addr0x0000061c
addr0x00000570: jal x19, addr0x00000184
addr0x00000574: jal x24, addr0x000007d8
addr0x00000578: jal x22, addr0x00000154
addr0x0000057c: jal x9, addr0x00000024
addr0x00000580: sw x4, 873(x8)
addr0x00000584: sw x22, -1431(x8)
addr0x00000588: jal x9, addr0x000007c8
addr0x0000058c: lui x29, 533307
addr0x00000590: addi x18, x0, 1804
addr0x00000594: slli x7, x20, 6
addr0x00000598: sw x17, -323(x8)
addr0x0000059c: lw x7, -1768(x8)
addr0x000005a0: addi x24, x23, 1285
addr0x000005a4: jal x24, addr0x00000494
addr0x000005a8: jal x27, addr0x00000724
addr0x000005ac: lw x7, -835(x8)
addr0x000005b0: lw x4, 122(x8)
addr0x000005b4: lw x24, 1559(x8)
addr0x000005b8: addi x25, x7, 1392
addr0x000005bc: add x20, x9, x0
addr0x000005c0: lw x30, -1384(x8)
addr0x000005c4: addi x4, x29, -721
addr0x000005c8: sw x7, -622(x8)
addr0x000005cc: addi x5, x2, -702
addr0x000005d0: jal x20, addr0x0000032c
addr0x000005d4: lw x22, 1453(x8)
addr0x000005d8: lw x2, -1653(x8)
addr0x000005dc: addi x4, x23, -969
addr0x000005e0: sw x22, -955(x8)
addr0x000005e4: jal x27, addr0x000004e0
addr0x000005e8: jal x13, addr0x000006bc
addr0x000005ec: lw x9, 1289(x8)
addr0x000005f0: sw x0, -891(x8)
addr0x000005f4: jal x0, addr0x00000008
addr0x000005f8: lui x7, 292201
addr0x000005fc: and x17, x24, x17
addr0x00000600: bge x29, x30, addr0x000000d0
addr0x00000604: andi x7, x29, 326
addr0x00000608: add x5, x29, x19
addr0x0000060c: lw x15, 1359(x8)
addr0x00000610: lw x15, -949(x8)
addr0x00000614: lw x30, -813(x8)
addr0x00000618: addi x15, x9, 1724
addr0x0000061c: lw x17, 1276(x8)
addr0x00000620: jal x13, addr0x000000a4
addr0x00000624: jal x22, addr0x00000360
addr0x00000628: addi x5, x12, -243
addr0x0000062c: addi x24, x0, 800
addr0x00000630: andi x22, x6, 665
addr0x00000634: lhu x13, -268(x8)
addr0x00000638: addi x29, x23, -346
addr0x0000063c: bgeu x7, x6, addr0x0000035c
addr0x00000640: lw x16, 1246(x8)
addr0x00000644: and x1, x22, x31
addr0x00000648: sw x9, -1163(x8)
addr0x0000064c: jal x29, addr0x00000290
addr0x00000650: lw x4, 1381(x8)
addr0x00000654: add x23, x13, x13
addr0x00000658: lui x19, 247445
addr0x0000065c: lw x9, -1652(x8)
addr0x00000660: add x15, x13, x15
addr0x00000664: sw x24, 256(x8)
addr0x00000668: addi x7, x22, -821
addr0x0000066c: sw x0, 1352(x8)
addr0x00000670: jal x23, addr0x0000068c
addr0x00000674: bne x17, x30, addr0x00000358
addr0x00000678: add x30, x19, x7
addr0x0000067c: lbu x14, -1129(x8)
addr0x00000680: sb x12, 1825(x8)
addr0x00000684: addi x23, x29, 1504
addr0x00000688: add x29, x11, x16
addr0x0000068c: jal x9, addr0x00000204
addr0x00000690: lw x19, 21(x8)
addr0x00000694: sub x22, x23, x22
addr0x00000698: sub x17, x22, x9
addr0x0000069c: jal x6, addr0x000007c8
addr0x000006a0: lw x4, -1866(x8)
addr0x000006a4: addi x9, x13, -1494
addr0x000006a8: beq x18, x4, addr0x000002f4
addr0x000006ac: addi x13, x30, 1724
addr0x000006b0: sw x9, -907(x8)
addr0x000006b4: sw x13, -1769(x8)
addr0x000006b8: addi x18, x20, -1937
addr0x000006bc: jal x19, addr0x0000033c
addr0x000006c0: lw x13, 921(x8)
addr0x000006c4: andi x11, x18, -1993
addr0x000006c8: addi x4, x23, 1064
addr0x000006cc: sw x14, -739(x8)
addr0x000006d0: jal x15, addr0x00000500
addr0x000006d4: lw x5, 1249(x8)
addr0x000006d8: lw x29, -1714(x8)
addr0x000006dc: lw x4, -1284(x8)
addr0x000006e0: lw x7, -1730(x8)
addr0x000006e4: add x0, x6, x2
addr0x000006e8: lw x23, 1614(x8)
addr0x000006ec: jal x23, addr0x00000630
addr0x000006f0: lw x14, -1729(x8)
addr0x000006f4: lbu x6, -2043(x8)
addr0x000006f8: addi x21, x17, 740
addr0x000006fc: lui x29, 676999
addr0x00000700: addi x7, x6, -583
addr0x00000704: lw x27, -530(x8)
addr0x00000708: addi x0, x25, 1012
addr0x0000070c: bne x22, x13, addr0x00000420
addr0x00000710: lw x29, 715(x8)
addr0x00000714: jal x23, addr0x000006c8
addr0x00000718: lw x15, 260(x8)
addr0x0000071c: sb x5, 710(x8)
addr0x00000720: lw x13, -1279(x8)
addr0x00000724: lw x19, -1640(x8)
addr0x00000728: addi x23, x20, -1733
addr0x0000072c: jal x24, addr0x000003a4
addr0x00000730: sw x29, -871(x8)
addr0x00000734: jalr x0, 192(x3)
addr0x00000738: srl x13, x7, x15
addr0x0000073c: andi x22, x30, 47
addr0x00000740: lw x22, 671(x8)
addr0x00000744: lw x13, 1906(x8)
addr0x00000748: add x29, x31, x0
addr0x0000074c: lbu x16, -110(x8)
addr0x00000750: lw x6, -1305(x8)
addr0x00000754: ori x17, x19, 923
addr0x00000758: sw x5, 300(x8)
addr0x0000075c: jal x17, addr0x00000328
addr0x00000760: sw x5, -1407(x8)
addr0x00000764: addi x19, x9, -789
addr0x00000768: sw x22, 1557(x8)
addr0x0000076c: addi x22, x23, -1080
addr0x00000770: jal x31, addr0x00000144
addr0x00000774: lw x0, -2048(x8)
addr0x00000778: lw x17, 583(x8)
addr0x0000077c: lw x18, -592(x8)
addr0x00000780: addi x30, x18, -445
addr0x00000784: addi x7, x6, -696
addr0x00000788: lui x7, 891415
addr0x0000078c: lw x2, -1939(x8)
addr0x00000790: lw x16, 1769(x8)
addr0x00000794: lw x29, 674(x8)
addr0x00000798: addi x9, x22, 419
addr0x0000079c: addi x16, x13, 1421
addr0x000007a0: jal x19, addr0x00000604
addr0x000007a4: addi x18, x13, 698
addr0x000007a8: sw x19, -32(x8)
addr0x000007ac: sw x19, -704(x8)
addr0x000007b0: jal x6, addr0x00000538
addr0x000007b4: addi x29, x20, -801
addr0x000007b8: jal x29, addr0x000000e0
addr0x000007bc: lw x18, -1294(x8)
addr0x000007c0: addi x13, x13, 305
addr0x000007c4: lw x13, 410(x8)
addr0x000007c8: sb x24, 1927(x8)
addr0x000007cc: sw x16, -1952(x8)
addr0x000007d0: auipc x21, 564609
addr0x000007d4: addi x13, x7, 409
addr0x000007d8: jal x7, addr0x00000020
addr0x000007dc: addi x21, x11, 1071
addr0x000007e0: sw x29, -952(x8)
addr0x000007e4: lw x5, 1720(x8)
addr0x000007e8: lw x30, -1073(x8)
addr0x000007ec: lw x29, 1350(x8)
addr0x000007f0: bgeu x17, x29, addr0x00000428
addr0x000007f4: jal x14, addr0x000003d8
