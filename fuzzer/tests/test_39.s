.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: slti x13, x31, -555
addr0x0000001c: addi x5, x4, 1779
addr0x00000020: add x23, x0, x5
addr0x00000024: addi x10, x2, 1333
addr0x00000028: lbu x22, -399(x8)
addr0x0000002c: sw x4, -1403(x8)
addr0x00000030: addi x10, x5, -28
addr0x00000034: sw x30, 152(x8)
addr0x00000038: addi x1, x5, 1871
addr0x0000003c: jal x4, addr0x00000240
addr0x00000040: lw x28, 324(x8)
addr0x00000044: lw x29, 1652(x8)
addr0x00000048: addi x20, x29, -673
addr0x0000004c: beq x13, x21, addr0x00000280
addr0x00000050: lw x9, -447(x8)
addr0x00000054: lhu x9, 595(x8)
addr0x00000058: beq x0, x25, addr0x00000134
addr0x0000005c: andi x16, x0, 509
addr0x00000060: jal x12, addr0x0000027c
addr0x00000064: sw x9, -912(x8)
addr0x00000068: sw x29, 77(x8)
addr0x0000006c: lw x22, -250(x8)
addr0x00000070: lui x31, 330652
addr0x00000074: addi x0, x9, 1638
addr0x00000078: lw x23, -1017(x8)
addr0x0000007c: sw x31, -1631(x8)
addr0x00000080: sw x28, -1906(x8)
addr0x00000084: sw x20, -1691(x8)
addr0x00000088: lbu x0, 138(x8)
addr0x0000008c: jal x0, addr0x0000026c
addr0x00000090: sh x7, -1947(x8)
addr0x00000094: lw x22, 1795(x8)
addr0x00000098: lw x18, 588(x8)
addr0x0000009c: lw x4, 488(x8)
addr0x000000a0: lbu x31, -1642(x8)
addr0x000000a4: jal x9, addr0x000000f4
addr0x000000a8: lw x11, -24(x8)
addr0x000000ac: lw x18, -1591(x8)
addr0x000000b0: lw x22, 340(x8)
addr0x000000b4: sw x5, 411(x8)
addr0x000000b8: lw x15, 222(x8)
addr0x000000bc: sw x10, 1703(x8)
addr0x000000c0: lw x14, -1828(x8)
addr0x000000c4: jal x22, addr0x00000310
addr0x000000c8: lw x29, 553(x8)
addr0x000000cc: jal x17, addr0x000002ec
addr0x000000d0: lw x18, 618(x8)
addr0x000000d4: addi x20, x13, -491
addr0x000000d8: jal x13, addr0x0000031c
addr0x000000dc: sw x21, 382(x8)
addr0x000000e0: lui x29, 745073
addr0x000000e4: addi x18, x12, 898
addr0x000000e8: sw x21, -1110(x8)
addr0x000000ec: sw x0, -1758(x8)
addr0x000000f0: sw x14, 1340(x8)
addr0x000000f4: sw x21, 846(x8)
addr0x000000f8: jal x6, addr0x00000018
addr0x000000fc: lw x14, -744(x8)
addr0x00000100: srli x29, x20, 25
addr0x00000104: lui x19, 645717
addr0x00000108: sw x30, -1312(x8)
addr0x0000010c: addi x20, x18, -300
addr0x00000110: lui x23, 923474
addr0x00000114: addi x4, x19, 678
addr0x00000118: jal x0, addr0x0000021c
addr0x0000011c: sub x18, x14, x18
addr0x00000120: addi x24, x19, 932
addr0x00000124: sw x24, -1291(x8)
addr0x00000128: addi x29, x4, -664
addr0x0000012c: jal x0, addr0x000001b8
addr0x00000130: lui x20, 603750
addr0x00000134: addi x17, x29, -1855
addr0x00000138: jal x4, addr0x000000b4
addr0x0000013c: lw x31, 424(x8)
addr0x00000140: sw x18, 314(x8)
addr0x00000144: jal x2, addr0x000002d4
addr0x00000148: bne x19, x28, addr0x0000023c
addr0x0000014c: lw x31, -1257(x8)
addr0x00000150: lw x27, -1408(x8)
addr0x00000154: lui x13, 982255
addr0x00000158: addi x17, x13, 71
addr0x0000015c: sw x5, -129(x8)
addr0x00000160: jal x5, addr0x000003b0
addr0x00000164: lw x20, 256(x8)
addr0x00000168: bltu x22, x22, addr0x00000064
addr0x0000016c: andi x0, x31, -470
addr0x00000170: bne x0, x27, addr0x00000064
addr0x00000174: lui x25, 217697
addr0x00000178: lbu x7, -663(x8)
addr0x0000017c: lw x17, -1231(x8)
addr0x00000180: sw x9, 1411(x8)
addr0x00000184: addi x20, x22, -1268
addr0x00000188: lui x17, 107326
addr0x0000018c: addi x11, x17, -1528
addr0x00000190: jal x9, addr0x000002dc
addr0x00000194: lw x22, -2033(x8)
addr0x00000198: jal x7, addr0x0000008c
addr0x0000019c: sw x29, -1325(x8)
addr0x000001a0: sw x29, 943(x8)
addr0x000001a4: jal x13, addr0x00000194
addr0x000001a8: sw x30, 1674(x8)
addr0x000001ac: jal x16, addr0x00000070
addr0x000001b0: bne x22, x19, addr0x00000330
addr0x000001b4: sw x29, -475(x8)
addr0x000001b8: sw x20, -1263(x8)
addr0x000001bc: sw x30, 802(x8)
addr0x000001c0: add x14, x13, x27
addr0x000001c4: lw x0, 1293(x8)
addr0x000001c8: lw x23, 1433(x8)
addr0x000001cc: addi x0, x25, -604
addr0x000001d0: addi x22, x14, -668
addr0x000001d4: sw x30, -1594(x8)
addr0x000001d8: ori x18, x16, 1958
addr0x000001dc: sh x5, -305(x8)
addr0x000001e0: lw x0, -1560(x8)
addr0x000001e4: addi x16, x7, -451
addr0x000001e8: sw x31, -410(x8)
addr0x000001ec: sw x25, -1732(x8)
addr0x000001f0: lw x29, 507(x8)
addr0x000001f4: addi x19, x9, 175
addr0x000001f8: sw x4, -958(x8)
addr0x000001fc: sw x21, -1502(x8)
addr0x00000200: lw x16, -1331(x8)
addr0x00000204: lw x10, 1352(x8)
addr0x00000208: lw x5, -1751(x8)
addr0x0000020c: lw x1, -173(x8)
addr0x00000210: lw x17, 801(x8)
addr0x00000214: lw x27, 1826(x8)
addr0x00000218: andi x14, x20, 869
addr0x0000021c: bltu x22, x13, addr0x00000394
addr0x00000220: add x4, x13, x22
addr0x00000224: jal x23, addr0x000001d0
addr0x00000228: addi x20, x7, 347
addr0x0000022c: lui x0, 508784
addr0x00000230: addi x0, x22, 1329
addr0x00000234: sw x28, -998(x8)
addr0x00000238: sw x9, -691(x8)
addr0x0000023c: sw x7, 1683(x8)
addr0x00000240: lui x16, 520052
addr0x00000244: addi x2, x12, 585
addr0x00000248: sw x29, 818(x8)
addr0x0000024c: lw x13, -1431(x8)
addr0x00000250: lw x7, -48(x8)
addr0x00000254: sb x28, 1727(x8)
addr0x00000258: lw x20, -1495(x8)
addr0x0000025c: lw x7, -423(x8)
addr0x00000260: addi x28, x19, 2000
addr0x00000264: jal x30, addr0x000000dc
addr0x00000268: lw x1, -437(x8)
addr0x0000026c: addi x0, x31, 2043
addr0x00000270: sb x12, 855(x8)
addr0x00000274: jal x22, addr0x00000344
addr0x00000278: sw x19, 570(x8)
addr0x0000027c: addi x18, x13, 1411
addr0x00000280: jal x18, addr0x00000378
addr0x00000284: sw x10, 409(x8)
addr0x00000288: jal x26, addr0x00000280
addr0x0000028c: addi x14, x7, 452
addr0x00000290: sw x9, -554(x8)
addr0x00000294: sw x20, -262(x8)
addr0x00000298: jal x17, addr0x000001f0
addr0x0000029c: sw x13, -1078(x8)
addr0x000002a0: lw x26, 215(x8)
addr0x000002a4: lbu x27, 1909(x8)
addr0x000002a8: andi x17, x19, 42
addr0x000002ac: sw x26, 350(x8)
addr0x000002b0: sw x13, 109(x8)
addr0x000002b4: lw x20, 396(x8)
addr0x000002b8: slli x19, x23, 5
addr0x000002bc: lw x25, -247(x8)
addr0x000002c0: sw x22, -1188(x8)
addr0x000002c4: jal x13, addr0x0000035c
addr0x000002c8: addi x19, x12, -1093
addr0x000002cc: addi x24, x18, -1669
addr0x000002d0: addi x26, x31, 1337
addr0x000002d4: lw x15, 1435(x8)
addr0x000002d8: add x4, x23, x16
addr0x000002dc: lw x27, -639(x8)
addr0x000002e0: lw x29, -800(x8)
addr0x000002e4: jal x30, addr0x000002e0
addr0x000002e8: lw x29, 1620(x8)
addr0x000002ec: lw x9, -1549(x8)
addr0x000002f0: beq x31, x29, addr0x00000264
addr0x000002f4: bltu x22, x25, addr0x000002d4
addr0x000002f8: lw x15, -998(x8)
addr0x000002fc: lw x24, 1111(x8)
addr0x00000300: lw x31, 888(x8)
addr0x00000304: sw x17, 958(x8)
addr0x00000308: lw x0, -589(x8)
addr0x0000030c: sub x19, x4, x22
addr0x00000310: lbu x23, -1510(x8)
addr0x00000314: add x0, x17, x18
addr0x00000318: lw x23, -282(x8)
addr0x0000031c: lw x16, 687(x8)
addr0x00000320: lui x30, 730020
addr0x00000324: addi x31, x21, 1292
addr0x00000328: sw x31, -1829(x8)
addr0x0000032c: sw x17, 662(x8)
addr0x00000330: sw x29, 510(x8)
addr0x00000334: lw x18, 1139(x8)
addr0x00000338: addi x7, x27, 399
addr0x0000033c: sw x24, 1429(x8)
addr0x00000340: jal x29, addr0x0000000c
addr0x00000344: jal x6, addr0x00000304
addr0x00000348: sw x5, 419(x8)
addr0x0000034c: lbu x13, -1786(x8)
addr0x00000350: lbu x19, 1777(x8)
addr0x00000354: bne x31, x7, addr0x00000148
addr0x00000358: sw x26, -1993(x8)
addr0x0000035c: addi x4, x22, 2021
addr0x00000360: lw x18, 1704(x8)
addr0x00000364: lw x27, -1831(x8)
addr0x00000368: lw x20, -668(x8)
addr0x0000036c: sw x19, 1740(x8)
addr0x00000370: sw x19, 984(x8)
addr0x00000374: addi x16, x7, 1661
addr0x00000378: sw x19, -128(x8)
addr0x0000037c: jal x5, addr0x00000030
addr0x00000380: lw x31, 1798(x8)
addr0x00000384: addi x27, x5, 1066
addr0x00000388: lhu x18, 423(x8)
addr0x0000038c: jal x16, addr0x000000e4
addr0x00000390: lui x30, 202206
addr0x00000394: lui x28, 31171
addr0x00000398: lui x14, 612171
addr0x0000039c: addi x23, x6, 2011
addr0x000003a0: jal x17, addr0x000001c8
addr0x000003a4: jal x31, addr0x00000340
addr0x000003a8: lw x15, -1146(x8)
addr0x000003ac: lw x17, 668(x8)
addr0x000003b0: jal x20, addr0x000002e8
addr0x000003b4: jal x29, addr0x00000178
addr0x000003b8: lw x2, 814(x8)
addr0x000003bc: lui x14, 182892
