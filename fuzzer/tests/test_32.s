.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x10, -1679(x8)
addr0x0000001c: lw x19, 25(x8)
addr0x00000020: lw x20, -329(x8)
addr0x00000024: add x6, x27, x18
addr0x00000028: sw x17, -489(x8)
addr0x0000002c: addi x7, x13, 942
addr0x00000030: sw x10, -913(x8)
addr0x00000034: sb x31, -1929(x8)
addr0x00000038: slli x21, x9, 17
addr0x0000003c: lw x25, 1408(x8)
addr0x00000040: jal x27, addr0x000006dc
addr0x00000044: lw x30, 1745(x8)
addr0x00000048: sub x0, x29, x13
addr0x0000004c: srai x4, x24, 21
addr0x00000050: lbu x19, 1286(x8)
addr0x00000054: sw x27, 861(x8)
addr0x00000058: jal x30, addr0x000002e8
addr0x0000005c: lw x22, 364(x8)
addr0x00000060: ori x19, x12, 890
addr0x00000064: lw x4, -189(x8)
addr0x00000068: jal x7, addr0x00000044
addr0x0000006c: lw x9, -426(x8)
addr0x00000070: sw x4, 1963(x8)
addr0x00000074: addi x7, x7, 705
addr0x00000078: lw x1, -324(x8)
addr0x0000007c: addi x14, x10, 915
addr0x00000080: sb x6, 1696(x8)
addr0x00000084: sb x25, 1871(x8)
addr0x00000088: divu x29, x4, x7
addr0x0000008c: lbu x21, -131(x8)
addr0x00000090: lw x23, -1782(x8)
addr0x00000094: add x0, x22, x12
addr0x00000098: sw x20, -374(x8)
addr0x0000009c: sltu x21, x31, x25
addr0x000000a0: add x1, x29, x14
addr0x000000a4: lw x6, -1672(x8)
addr0x000000a8: sub x6, x4, x28
addr0x000000ac: sw x11, 663(x8)
addr0x000000b0: jal x14, addr0x00000ae0
addr0x000000b4: addi x23, x5, 1235
addr0x000000b8: jal x5, addr0x000001fc
addr0x000000bc: lui x25, 334812
addr0x000000c0: jal x27, addr0x00000a0c
addr0x000000c4: jal x4, addr0x00000ac0
addr0x000000c8: addi x19, x31, -1918
addr0x000000cc: sw x21, -283(x8)
addr0x000000d0: lw x15, 455(x8)
addr0x000000d4: bne x17, x29, addr0x00000330
addr0x000000d8: sw x9, -1781(x8)
addr0x000000dc: sw x13, -1489(x8)
addr0x000000e0: jal x29, addr0x00000c90
addr0x000000e4: lbu x18, -618(x8)
addr0x000000e8: beq x25, x23, addr0x00000528
addr0x000000ec: jal x7, addr0x00001114
addr0x000000f0: addi x24, x23, 612
addr0x000000f4: add x14, x23, x30
addr0x000000f8: addi x24, x9, -1337
addr0x000000fc: add x9, x1, x7
addr0x00000100: srli x31, x20, 13
addr0x00000104: beq x4, x7, addr0x00000774
addr0x00000108: lw x31, -1243(x8)
addr0x0000010c: bge x7, x26, addr0x000001f8
addr0x00000110: lw x7, 280(x8)
addr0x00000114: lui x29, 918368
addr0x00000118: sw x13, -1420(x8)
addr0x0000011c: sw x26, -1523(x8)
addr0x00000120: lw x13, -123(x8)
addr0x00000124: lw x13, -1166(x8)
addr0x00000128: addi x22, x12, 1773
addr0x0000012c: sw x1, 1424(x8)
addr0x00000130: sw x22, 302(x8)
addr0x00000134: sw x7, -1066(x8)
addr0x00000138: sw x5, -1265(x8)
addr0x0000013c: sw x0, -1641(x8)
addr0x00000140: sw x21, 1743(x8)
addr0x00000144: bge x27, x31, addr0x000000b4
addr0x00000148: lw x5, 732(x8)
addr0x0000014c: addi x0, x18, 1312
addr0x00000150: sw x12, 537(x8)
addr0x00000154: sw x30, -96(x8)
addr0x00000158: jal x19, addr0x00000c20
addr0x0000015c: addi x11, x7, -56
addr0x00000160: sw x28, -476(x8)
addr0x00000164: addi x5, x18, -1309
addr0x00000168: addi x13, x5, 222
addr0x0000016c: sw x27, 1343(x8)
addr0x00000170: lui x17, 760324
addr0x00000174: addi x5, x11, -1304
addr0x00000178: sw x29, 1097(x8)
addr0x0000017c: lw x30, 112(x8)
addr0x00000180: sw x0, -202(x8)
addr0x00000184: jal x4, addr0x0000049c
addr0x00000188: slli x27, x13, 3
addr0x0000018c: addi x9, x9, 632
addr0x00000190: addi x26, x22, 170
addr0x00000194: bne x13, x27, addr0x0000015c
addr0x00000198: sll x27, x23, x23
addr0x0000019c: jal x13, addr0x00000d24
addr0x000001a0: jal x21, addr0x00000ad8
addr0x000001a4: lw x15, -488(x8)
addr0x000001a8: lw x19, -1759(x8)
addr0x000001ac: lw x28, -1065(x8)
addr0x000001b0: lw x18, 573(x8)
addr0x000001b4: sw x29, 1215(x8)
addr0x000001b8: sw x18, 984(x8)
addr0x000001bc: sh x11, 1462(x8)
addr0x000001c0: andi x5, x19, -1969
addr0x000001c4: mul x23, x14, x19
addr0x000001c8: addi x23, x10, -92
addr0x000001cc: lui x29, 390603
addr0x000001d0: slli x5, x10, 24
addr0x000001d4: add x19, x10, x5
addr0x000001d8: jal x13, addr0x00000848
addr0x000001dc: bge x29, x0, addr0x00000290
addr0x000001e0: lw x20, 975(x8)
addr0x000001e4: lw x19, 1902(x8)
addr0x000001e8: addi x0, x19, -1836
addr0x000001ec: sw x11, 1682(x8)
addr0x000001f0: bne x17, x31, addr0x00000464
addr0x000001f4: addi x12, x18, 53
addr0x000001f8: sw x23, 1806(x8)
addr0x000001fc: addi x5, x17, -1846
addr0x00000200: addi x31, x27, 966
addr0x00000204: jal x27, addr0x00000d04
addr0x00000208: jal x21, addr0x00000130
addr0x0000020c: bgeu x0, x26, addr0x00000610
addr0x00000210: jal x9, addr0x00000ad0
addr0x00000214: lw x19, -1633(x8)
addr0x00000218: sub x25, x5, x4
addr0x0000021c: sw x25, 1523(x8)
addr0x00000220: lui x5, 129812
addr0x00000224: sw x31, -608(x8)
addr0x00000228: sw x31, 561(x8)
addr0x0000022c: jal x5, addr0x00000c34
addr0x00000230: lw x2, 1020(x8)
addr0x00000234: addi x21, x6, 681
addr0x00000238: auipc x26, 653897
addr0x0000023c: jalr x0, 860(x3)
addr0x00000240: sw x27, 1866(x8)
addr0x00000244: lw x27, -181(x8)
addr0x00000248: sw x10, -818(x8)
addr0x0000024c: sw x17, -1019(x8)
addr0x00000250: addi x19, x2, 1730
addr0x00000254: jal x23, addr0x000008c0
addr0x00000258: lhu x28, -644(x8)
addr0x0000025c: lhu x11, -471(x8)
addr0x00000260: lui x28, 244405
addr0x00000264: sw x21, 1218(x8)
addr0x00000268: jal x14, addr0x00000600
addr0x0000026c: addi x10, x14, 779
addr0x00000270: bne x14, x31, addr0x000005b4
addr0x00000274: addi x23, x23, 1129
addr0x00000278: beq x28, x9, addr0x000001c8
addr0x0000027c: addi x29, x25, 175
addr0x00000280: sw x6, 1617(x8)
addr0x00000284: lbu x5, -1506(x8)
addr0x00000288: add x6, x17, x21
addr0x0000028c: jal x20, addr0x000004e8
addr0x00000290: blt x20, x0, addr0x00000334
addr0x00000294: beq x19, x16, addr0x00000818
addr0x00000298: lw x25, 672(x8)
addr0x0000029c: lw x16, -1675(x8)
addr0x000002a0: addi x17, x19, 1898
addr0x000002a4: lw x7, -426(x8)
addr0x000002a8: ori x20, x18, 1507
addr0x000002ac: sw x23, 45(x8)
addr0x000002b0: jal x13, addr0x00001118
addr0x000002b4: jal x31, addr0x000005c0
addr0x000002b8: addi x9, x19, -1133
addr0x000002bc: add x30, x28, x25
addr0x000002c0: lbu x22, -18(x8)
addr0x000002c4: addi x2, x7, -1388
addr0x000002c8: addi x23, x19, 798
addr0x000002cc: jal x6, addr0x00000e50
addr0x000002d0: lw x31, 442(x8)
addr0x000002d4: jal x22, addr0x00000374
addr0x000002d8: lw x9, 1963(x8)
addr0x000002dc: lui x0, 466932
addr0x000002e0: addi x22, x10, 1700
addr0x000002e4: jal x18, addr0x00000b78
addr0x000002e8: auipc x23, 488049
addr0x000002ec: lw x24, -1554(x8)
addr0x000002f0: lui x20, 756625
addr0x000002f4: sw x22, 353(x8)
addr0x000002f8: jal x20, addr0x00000f78
addr0x000002fc: slli x5, x29, 24
addr0x00000300: sw x7, 77(x8)
addr0x00000304: addi x22, x24, 956
addr0x00000308: auipc x20, 203212
addr0x0000030c: addi x12, x28, 1848
addr0x00000310: sw x14, 1482(x8)
addr0x00000314: sw x26, -734(x8)
addr0x00000318: sw x31, -1834(x8)
addr0x0000031c: addi x25, x29, 1616
addr0x00000320: jal x18, addr0x000005a4
addr0x00000324: lw x7, -1090(x8)
addr0x00000328: lw x22, -879(x8)
addr0x0000032c: jal x23, addr0x0000054c
addr0x00000330: lw x1, 1049(x8)
addr0x00000334: lw x18, -1366(x8)
addr0x00000338: addi x31, x19, 1035
addr0x0000033c: sw x17, 1099(x8)
addr0x00000340: lw x9, 355(x8)
addr0x00000344: lw x25, -1092(x8)
addr0x00000348: lw x4, 1412(x8)
addr0x0000034c: lw x19, -548(x8)
addr0x00000350: lui x29, 68789
addr0x00000354: addi x19, x14, 622
addr0x00000358: jal x7, addr0x000010bc
addr0x0000035c: lui x18, 474192
addr0x00000360: addi x13, x27, -1991
addr0x00000364: jal x0, addr0x00000310
addr0x00000368: lw x29, -789(x8)
addr0x0000036c: sw x29, 676(x8)
addr0x00000370: jal x19, addr0x00000edc
addr0x00000374: lw x18, -1478(x8)
addr0x00000378: lbu x20, 1633(x8)
addr0x0000037c: lw x30, -1089(x8)
addr0x00000380: slli x17, x28, 27
addr0x00000384: add x6, x5, x29
addr0x00000388: slli x0, x7, 7
addr0x0000038c: lw x30, -1208(x8)
addr0x00000390: add x19, x25, x29
addr0x00000394: lbu x23, 898(x8)
addr0x00000398: bne x23, x0, addr0x000004ac
addr0x0000039c: lw x4, -1693(x8)
addr0x000003a0: beq x12, x9, addr0x0000041c
addr0x000003a4: addi x19, x22, -1654
addr0x000003a8: sw x10, -414(x8)
addr0x000003ac: sw x6, 490(x8)
addr0x000003b0: lw x2, 432(x8)
addr0x000003b4: jal x19, addr0x0000041c
addr0x000003b8: sub x28, x11, x7
addr0x000003bc: sltu x19, x9, x5
addr0x000003c0: or x6, x20, x23
addr0x000003c4: sb x31, 227(x8)
addr0x000003c8: lw x7, -1708(x8)
addr0x000003cc: sw x30, 372(x8)
addr0x000003d0: beq x29, x27, addr0x00000b94
addr0x000003d4: lbu x25, 1616(x8)
addr0x000003d8: bgeu x9, x9, addr0x00000560
addr0x000003dc: lw x13, 1695(x8)
addr0x000003e0: bne x12, x22, addr0x0000096c
addr0x000003e4: addi x0, x19, 1094
addr0x000003e8: jal x7, addr0x0000071c
addr0x000003ec: lw x0, -718(x8)
addr0x000003f0: lw x19, -398(x8)
addr0x000003f4: lw x27, 552(x8)
addr0x000003f8: lw x31, 2044(x8)
addr0x000003fc: lw x7, 1218(x8)
addr0x00000400: sw x9, 159(x8)
addr0x00000404: sw x25, 810(x8)
addr0x00000408: lw x29, 1529(x8)
addr0x0000040c: add x20, x14, x25
addr0x00000410: lbu x19, -1668(x8)
addr0x00000414: lbu x25, -298(x8)
addr0x00000418: lw x29, -1799(x8)
addr0x0000041c: sh x23, 1186(x8)
addr0x00000420: sw x6, -195(x8)
addr0x00000424: jal x9, addr0x0000076c
addr0x00000428: lw x17, 2027(x8)
addr0x0000042c: addi x28, x29, -120
addr0x00000430: sw x20, 1506(x8)
addr0x00000434: jal x31, addr0x00000468
addr0x00000438: jal x25, addr0x00000b68
addr0x0000043c: lw x13, -106(x8)
addr0x00000440: lw x2, -1961(x8)
addr0x00000444: addi x4, x29, -1284
addr0x00000448: sw x12, 348(x8)
addr0x0000044c: addi x7, x7, 914
addr0x00000450: jal x24, addr0x000005b8
addr0x00000454: addi x21, x19, 1490
addr0x00000458: addi x19, x29, 796
addr0x0000045c: sw x2, -858(x8)
addr0x00000460: sw x6, -1017(x8)
addr0x00000464: sw x13, -651(x8)
addr0x00000468: jal x5, addr0x00000db8
addr0x0000046c: ori x31, x19, -858
addr0x00000470: sw x18, -758(x8)
addr0x00000474: lw x18, 149(x8)
addr0x00000478: bltu x17, x1, addr0x00000468
addr0x0000047c: slli x12, x7, 6
addr0x00000480: srli x17, x24, 20
addr0x00000484: jal x31, addr0x000007a0
addr0x00000488: lw x24, 1729(x8)
addr0x0000048c: lw x24, 1098(x8)
addr0x00000490: lbu x21, -1851(x8)
addr0x00000494: lbu x1, 1426(x8)
addr0x00000498: xori x20, x22, -322
addr0x0000049c: andi x14, x0, -1490
addr0x000004a0: addi x13, x2, 1567
addr0x000004a4: addi x20, x17, 596
addr0x000004a8: lw x7, -330(x8)
addr0x000004ac: add x16, x19, x29
addr0x000004b0: jal x23, addr0x000004b8
addr0x000004b4: lui x19, 976569
addr0x000004b8: addi x30, x25, -926
addr0x000004bc: addi x22, x21, 1452
addr0x000004c0: addi x23, x22, 1187
addr0x000004c4: sw x24, -1109(x8)
addr0x000004c8: sw x27, 307(x8)
addr0x000004cc: blt x13, x30, addr0x00000338
addr0x000004d0: jal x5, addr0x000002c8
addr0x000004d4: addi x6, x24, -1064
addr0x000004d8: addi x9, x27, -851
addr0x000004dc: addi x19, x16, -296
addr0x000004e0: sb x19, -694(x8)
addr0x000004e4: sw x14, 837(x8)
addr0x000004e8: sw x19, -1882(x8)
addr0x000004ec: jal x19, addr0x00000b6c
addr0x000004f0: lw x4, 934(x8)
addr0x000004f4: add x20, x30, x7
addr0x000004f8: sw x29, -1339(x8)
addr0x000004fc: addi x5, x30, -242
addr0x00000500: jal x0, addr0x00000c74
addr0x00000504: sw x14, -1222(x8)
addr0x00000508: andi x22, x28, -733
addr0x0000050c: sw x20, -185(x8)
addr0x00000510: addi x16, x0, -1053
addr0x00000514: addi x9, x7, 64
addr0x00000518: sw x27, 592(x8)
addr0x0000051c: sw x5, -1588(x8)
addr0x00000520: sw x29, 982(x8)
addr0x00000524: jal x13, addr0x00000908
addr0x00000528: lw x20, 1524(x8)
addr0x0000052c: ori x19, x29, 1735
addr0x00000530: sw x22, -1296(x8)
addr0x00000534: jal x28, addr0x000002d0
addr0x00000538: sw x19, 37(x8)
addr0x0000053c: jal x17, addr0x00000254
addr0x00000540: add x17, x20, x26
addr0x00000544: sw x14, 510(x8)
addr0x00000548: sw x14, 1675(x8)
addr0x0000054c: lw x6, -1707(x8)
addr0x00000550: sw x7, -1273(x8)
addr0x00000554: bge x29, x13, addr0x00000c80
addr0x00000558: addi x18, x28, -1044
addr0x0000055c: or x7, x15, x22
addr0x00000560: sw x20, -1665(x8)
addr0x00000564: lui x24, 850623
addr0x00000568: addi x13, x31, 650
addr0x0000056c: jal x9, addr0x00000958
addr0x00000570: lw x5, 1733(x8)
addr0x00000574: addi x13, x19, -876
addr0x00000578: bne x6, x17, addr0x00000b84
addr0x0000057c: slli x24, x29, 23
addr0x00000580: lw x7, 146(x8)
addr0x00000584: bne x23, x12, addr0x00000c10
addr0x00000588: addi x2, x24, 493
addr0x0000058c: jal x17, addr0x00000b78
addr0x00000590: lw x4, -224(x8)
addr0x00000594: addi x20, x22, -181
addr0x00000598: jal x10, addr0x00000f4c
addr0x0000059c: lw x29, -1430(x8)
addr0x000005a0: sw x13, 939(x8)
addr0x000005a4: sw x17, -547(x8)
addr0x000005a8: addi x20, x4, -1503
addr0x000005ac: sw x9, -1797(x8)
addr0x000005b0: lw x5, -302(x8)
addr0x000005b4: addi x27, x13, 1989
addr0x000005b8: sw x18, 736(x8)
addr0x000005bc: addi x14, x15, 537
addr0x000005c0: addi x1, x30, -683
addr0x000005c4: sw x23, 598(x8)
addr0x000005c8: sw x31, 1803(x8)
addr0x000005cc: lbu x27, -920(x8)
addr0x000005d0: lbu x20, -426(x8)
addr0x000005d4: jal x19, addr0x000003e0
addr0x000005d8: lbu x0, 805(x8)
addr0x000005dc: addi x21, x29, -524
addr0x000005e0: sub x2, x19, x19
addr0x000005e4: addi x12, x21, -68
addr0x000005e8: jal x19, addr0x00000ad4
addr0x000005ec: jal x31, addr0x00000da4
addr0x000005f0: jal x18, addr0x00000eb4
addr0x000005f4: lui x31, 908006
addr0x000005f8: addi x19, x28, -716
addr0x000005fc: addi x30, x19, 2004
addr0x00000600: sh x12, -1591(x8)
addr0x00000604: sb x19, 1358(x8)
addr0x00000608: lw x25, 842(x8)
addr0x0000060c: lw x29, -1423(x8)
addr0x00000610: addi x28, x19, -1968
addr0x00000614: add x28, x27, x18
addr0x00000618: blt x7, x25, addr0x00000094
addr0x0000061c: lui x22, 487808
addr0x00000620: addi x19, x19, -117
addr0x00000624: sw x18, 543(x8)
addr0x00000628: jal x18, addr0x00000628
addr0x0000062c: addi x9, x29, -1537
addr0x00000630: jal x2, addr0x00000340
addr0x00000634: lw x23, 1623(x8)
addr0x00000638: lw x24, 1100(x8)
addr0x0000063c: lw x19, -1540(x8)
addr0x00000640: lw x24, -1667(x8)
addr0x00000644: srl x4, x1, x18
addr0x00000648: sll x5, x17, x27
addr0x0000064c: bltu x7, x0, addr0x00000c40
addr0x00000650: lui x9, 583313
addr0x00000654: bne x19, x23, addr0x00000860
addr0x00000658: lbu x9, -477(x8)
addr0x0000065c: bne x15, x14, addr0x0000039c
addr0x00000660: lw x17, 830(x8)
addr0x00000664: bge x4, x9, addr0x00000808
addr0x00000668: jal x10, addr0x000005a0
addr0x0000066c: sh x29, 1235(x8)
addr0x00000670: sh x13, 544(x8)
addr0x00000674: lhu x13, -1428(x8)
addr0x00000678: lhu x20, 1360(x8)
addr0x0000067c: sw x5, -1229(x8)
addr0x00000680: lw x23, 1269(x8)
addr0x00000684: lw x27, -1011(x8)
addr0x00000688: sw x9, -2035(x8)
addr0x0000068c: addi x9, x20, 989
addr0x00000690: sw x19, 169(x8)
addr0x00000694: sw x22, -303(x8)
addr0x00000698: sw x20, 1102(x8)
addr0x0000069c: jal x6, addr0x00000500
addr0x000006a0: lw x18, -1658(x8)
addr0x000006a4: lw x15, -1664(x8)
addr0x000006a8: bltu x31, x0, addr0x00000560
addr0x000006ac: bne x20, x30, addr0x000008f4
addr0x000006b0: lw x19, 467(x8)
addr0x000006b4: addi x14, x13, -1233
addr0x000006b8: lw x23, 427(x8)
addr0x000006bc: lw x5, 1708(x8)
addr0x000006c0: ori x14, x14, 453
addr0x000006c4: bltu x1, x7, addr0x00000a58
addr0x000006c8: sw x7, -938(x8)
addr0x000006cc: jal x30, addr0x00000fd4
addr0x000006d0: sb x19, 154(x8)
addr0x000006d4: sub x5, x10, x26
addr0x000006d8: srai x12, x22, 9
addr0x000006dc: jal x7, addr0x00000ab8
addr0x000006e0: lw x5, 1650(x8)
addr0x000006e4: lw x4, -1839(x8)
addr0x000006e8: lw x31, -15(x8)
addr0x000006ec: addi x2, x23, 92
addr0x000006f0: sw x20, -1791(x8)
addr0x000006f4: lw x19, 309(x8)
addr0x000006f8: jalr x0, 268(x3)
addr0x000006fc: lhu x19, -1379(x8)
addr0x00000700: addi x7, x24, 1260
addr0x00000704: lw x20, 754(x8)
addr0x00000708: lw x31, -294(x8)
addr0x0000070c: sw x13, -1219(x8)
addr0x00000710: sw x1, -122(x8)
addr0x00000714: lw x19, 1183(x8)
addr0x00000718: lw x23, 579(x8)
addr0x0000071c: add x7, x28, x22
addr0x00000720: andi x24, x27, -1443
addr0x00000724: lbu x14, -248(x8)
addr0x00000728: slli x5, x18, 9
addr0x0000072c: lw x14, 1185(x8)
addr0x00000730: addi x20, x29, -380
addr0x00000734: addi x10, x26, -1200
addr0x00000738: jal x7, addr0x00001040
addr0x0000073c: lw x5, 97(x8)
addr0x00000740: addi x22, x18, -238
addr0x00000744: slli x19, x6, 7
addr0x00000748: bne x11, x19, addr0x00000ad0
addr0x0000074c: bne x29, x23, addr0x00000254
addr0x00000750: addi x11, x18, -398
addr0x00000754: beq x27, x17, addr0x00000de8
addr0x00000758: lw x20, 1690(x8)
addr0x0000075c: lw x27, -1546(x8)
addr0x00000760: jal x5, addr0x00000aa8
addr0x00000764: sw x0, 938(x8)
addr0x00000768: sw x1, -400(x8)
addr0x0000076c: addi x29, x18, -1378
addr0x00000770: sh x0, -1736(x8)
addr0x00000774: jal x15, addr0x000007a8
addr0x00000778: lw x5, 218(x8)
addr0x0000077c: sw x25, -1302(x8)
addr0x00000780: sb x7, -1532(x8)
addr0x00000784: lbu x22, 703(x8)
addr0x00000788: lw x22, 5(x8)
addr0x0000078c: add x4, x13, x22
addr0x00000790: lw x18, -485(x8)
addr0x00000794: sub x29, x26, x29
addr0x00000798: bgeu x23, x29, addr0x00000248
addr0x0000079c: jal x20, addr0x0000074c
addr0x000007a0: lw x20, 275(x8)
addr0x000007a4: lw x22, -555(x8)
addr0x000007a8: addi x5, x0, 1932
addr0x000007ac: sw x31, 402(x8)
addr0x000007b0: lw x29, -776(x8)
addr0x000007b4: bgeu x4, x6, addr0x00000a00
addr0x000007b8: add x19, x12, x13
addr0x000007bc: sub x24, x22, x16
addr0x000007c0: andi x27, x9, 1395
addr0x000007c4: sw x9, 170(x8)
addr0x000007c8: sw x0, -354(x8)
addr0x000007cc: lw x22, 1108(x8)
addr0x000007d0: jal x22, addr0x00000a00
addr0x000007d4: jal x7, addr0x00000f38
addr0x000007d8: jal x7, addr0x00000e70
addr0x000007dc: lw x9, 369(x8)
addr0x000007e0: lw x24, 287(x8)
addr0x000007e4: add x23, x19, x27
addr0x000007e8: add x0, x15, x13
addr0x000007ec: andi x10, x17, -1677
addr0x000007f0: lw x14, 120(x8)
addr0x000007f4: addi x20, x4, -601
addr0x000007f8: sw x25, 1308(x8)
addr0x000007fc: sw x29, -1087(x8)
addr0x00000800: lw x9, -1103(x8)
addr0x00000804: lw x21, 1076(x8)
addr0x00000808: addi x19, x17, 837
addr0x0000080c: addi x14, x27, -163
addr0x00000810: sw x30, 683(x8)
addr0x00000814: jal x19, addr0x00000500
addr0x00000818: lw x31, -585(x8)
addr0x0000081c: lw x12, 1221(x8)
addr0x00000820: lw x19, 90(x8)
addr0x00000824: lw x5, -10(x8)
addr0x00000828: lw x29, -592(x8)
addr0x0000082c: jalr x0, 828(x3)
addr0x00000830: lw x2, 1107(x8)
addr0x00000834: sw x5, -916(x8)
addr0x00000838: sw x11, -956(x8)
addr0x0000083c: sw x1, 1637(x8)
addr0x00000840: sw x0, -1377(x8)
addr0x00000844: sw x0, -1759(x8)
addr0x00000848: sw x17, 899(x8)
addr0x0000084c: sw x30, -1585(x8)
addr0x00000850: sw x7, -894(x8)
addr0x00000854: sw x22, -674(x8)
addr0x00000858: sw x30, -1917(x8)
addr0x0000085c: lw x23, 1814(x8)
addr0x00000860: lw x7, 1900(x8)
addr0x00000864: addi x17, x16, -1101
addr0x00000868: sw x30, 1453(x8)
addr0x0000086c: lw x26, -1003(x8)
addr0x00000870: addi x23, x20, 600
addr0x00000874: jal x10, addr0x00000094
addr0x00000878: sw x31, -1546(x8)
addr0x0000087c: lw x23, 683(x8)
addr0x00000880: addi x23, x17, -1720
addr0x00000884: lw x25, -1176(x8)
addr0x00000888: addi x24, x4, -280
addr0x0000088c: jal x2, addr0x00000e44
addr0x00000890: lw x15, -1097(x8)
addr0x00000894: sw x6, -1331(x8)
addr0x00000898: lw x25, -2019(x8)
addr0x0000089c: add x28, x4, x13
addr0x000008a0: sw x14, 115(x8)
addr0x000008a4: lbu x7, -1191(x8)
addr0x000008a8: lw x25, 1538(x8)
addr0x000008ac: bgeu x12, x30, addr0x00000aa4
addr0x000008b0: lui x9, 143816
addr0x000008b4: addi x20, x22, 71
addr0x000008b8: lui x12, 768635
addr0x000008bc: sw x10, -635(x8)
addr0x000008c0: lw x9, 664(x8)
addr0x000008c4: lw x1, -445(x8)
addr0x000008c8: addi x28, x13, -1467
addr0x000008cc: sw x0, -537(x8)
addr0x000008d0: lw x18, 1407(x8)
addr0x000008d4: jal x9, addr0x00000e1c
addr0x000008d8: sw x19, -700(x8)
addr0x000008dc: jal x20, addr0x00001114
addr0x000008e0: lw x13, 1664(x8)
addr0x000008e4: bgeu x13, x27, addr0x00000fb4
addr0x000008e8: jal x31, addr0x00000918
addr0x000008ec: addi x23, x23, -1976
addr0x000008f0: sw x11, 1805(x8)
addr0x000008f4: sub x18, x25, x5
addr0x000008f8: sw x31, -1857(x8)
addr0x000008fc: jal x29, addr0x00000bf4
addr0x00000900: jal x1, addr0x000008ec
addr0x00000904: sw x20, -242(x8)
addr0x00000908: sw x29, 543(x8)
addr0x0000090c: addi x6, x22, -1151
addr0x00000910: addi x23, x25, 1968
addr0x00000914: sw x22, 641(x8)
addr0x00000918: sw x29, -1684(x8)
addr0x0000091c: lw x2, -306(x8)
addr0x00000920: jal x19, addr0x000003b0
addr0x00000924: jal x22, addr0x0000004c
addr0x00000928: sb x4, 1192(x8)
addr0x0000092c: sw x31, 763(x8)
addr0x00000930: sw x7, 291(x8)
addr0x00000934: lui x0, 446466
addr0x00000938: addi x21, x14, -882
addr0x0000093c: blt x18, x23, addr0x00000f28
addr0x00000940: sw x20, -719(x8)
addr0x00000944: lw x23, -1549(x8)
addr0x00000948: sw x23, -518(x8)
addr0x0000094c: sw x13, 346(x8)
addr0x00000950: jal x17, addr0x00001050
addr0x00000954: lw x28, -1215(x8)
addr0x00000958: lw x30, 1225(x8)
addr0x0000095c: sw x26, 1453(x8)
addr0x00000960: sw x7, 962(x8)
addr0x00000964: bne x1, x27, addr0x000006b0
addr0x00000968: jal x26, addr0x00000b30
addr0x0000096c: sw x23, 504(x8)
addr0x00000970: jal x23, addr0x000001cc
addr0x00000974: lui x31, 781763
addr0x00000978: addi x10, x23, -127
addr0x0000097c: lw x29, 1084(x8)
addr0x00000980: lw x9, 301(x8)
addr0x00000984: addi x23, x19, 202
addr0x00000988: sw x27, 1790(x8)
addr0x0000098c: sw x0, -716(x8)
addr0x00000990: sw x25, 23(x8)
addr0x00000994: lw x7, -1640(x8)
addr0x00000998: jal x21, addr0x0000048c
addr0x0000099c: addi x9, x15, -1405
addr0x000009a0: jal x1, addr0x00000a78
addr0x000009a4: addi x31, x31, -164
addr0x000009a8: addi x18, x28, -768
addr0x000009ac: lw x17, -978(x8)
addr0x000009b0: lw x17, 1878(x8)
addr0x000009b4: sw x20, -375(x8)
addr0x000009b8: sw x23, -321(x8)
addr0x000009bc: lw x0, 676(x8)
addr0x000009c0: addi x5, x18, 1466
addr0x000009c4: addi x25, x22, -504
addr0x000009c8: lw x11, -1506(x8)
addr0x000009cc: lw x31, -1697(x8)
addr0x000009d0: sw x9, 1471(x8)
addr0x000009d4: jal x19, addr0x00000ac0
addr0x000009d8: lw x0, -1659(x8)
addr0x000009dc: add x29, x5, x23
addr0x000009e0: sb x13, 1285(x8)
addr0x000009e4: beq x31, x28, addr0x00000d68
addr0x000009e8: lw x7, -168(x8)
addr0x000009ec: bne x7, x1, addr0x00000a70
addr0x000009f0: bge x7, x22, addr0x00000610
addr0x000009f4: jal x20, addr0x00000278
addr0x000009f8: andi x7, x10, -423
addr0x000009fc: or x21, x9, x7
addr0x00000a00: lw x22, 1312(x8)
addr0x00000a04: bgeu x2, x27, addr0x00000cc8
addr0x00000a08: lw x24, 1587(x8)
addr0x00000a0c: addi x0, x7, -796
addr0x00000a10: sw x0, -1145(x8)
addr0x00000a14: jal x10, addr0x000008a4
addr0x00000a18: lui x22, 637491
addr0x00000a1c: sw x13, 705(x8)
addr0x00000a20: add x22, x29, x18
addr0x00000a24: sub x13, x17, x7
addr0x00000a28: lw x20, -151(x8)
addr0x00000a2c: lw x23, -441(x8)
addr0x00000a30: sw x12, -1809(x8)
addr0x00000a34: sw x22, -358(x8)
addr0x00000a38: sw x13, -1496(x8)
addr0x00000a3c: sw x2, 1563(x8)
addr0x00000a40: lw x2, -182(x8)
addr0x00000a44: lw x27, 1370(x8)
addr0x00000a48: addi x27, x14, 1567
addr0x00000a4c: addi x20, x6, 593
addr0x00000a50: lui x2, 820508
addr0x00000a54: sb x22, -1680(x8)
addr0x00000a58: lw x11, -1170(x8)
addr0x00000a5c: lbu x2, -1633(x8)
addr0x00000a60: sub x29, x20, x22
addr0x00000a64: sltu x4, x17, x22
addr0x00000a68: lui x7, 610506
addr0x00000a6c: addi x20, x28, 1640
addr0x00000a70: addi x13, x23, 355
addr0x00000a74: beq x5, x6, addr0x00000420
addr0x00000a78: jal x9, addr0x00000c20
addr0x00000a7c: jal x20, addr0x000008b4
addr0x00000a80: sw x0, -336(x8)
addr0x00000a84: jal x17, addr0x00000604
addr0x00000a88: lbu x18, 2000(x8)
addr0x00000a8c: bne x4, x28, addr0x00000868
addr0x00000a90: lw x9, 1931(x8)
addr0x00000a94: lw x27, 1797(x8)
addr0x00000a98: addi x9, x18, -573
addr0x00000a9c: andi x20, x4, 724
addr0x00000aa0: lw x30, 588(x8)
addr0x00000aa4: addi x30, x27, -1628
addr0x00000aa8: lw x9, 499(x8)
addr0x00000aac: jal x19, addr0x0000102c
addr0x00000ab0: jal x20, addr0x00000e1c
addr0x00000ab4: lw x17, -1771(x8)
addr0x00000ab8: lw x27, 919(x8)
addr0x00000abc: lw x27, -1539(x8)
addr0x00000ac0: slli x29, x28, 27
addr0x00000ac4: lui x25, 448861
addr0x00000ac8: addi x20, x31, -380
addr0x00000acc: jal x1, addr0x00000d30
addr0x00000ad0: lui x22, 985850
addr0x00000ad4: lui x19, 995717
addr0x00000ad8: addi x0, x16, 327
addr0x00000adc: jal x20, addr0x00000640
addr0x00000ae0: jalr x0, 912(x3)
addr0x00000ae4: jal x19, addr0x00000ee8
addr0x00000ae8: sw x22, -727(x8)
addr0x00000aec: jal x17, addr0x000007f4
addr0x00000af0: jal x6, addr0x00000904
addr0x00000af4: andi x22, x1, 1396
addr0x00000af8: slli x7, x27, 0
addr0x00000afc: lw x4, 1371(x8)
addr0x00000b00: lw x0, 1674(x8)
addr0x00000b04: lw x24, 1855(x8)
addr0x00000b08: sw x13, 1338(x8)
addr0x00000b0c: bgeu x19, x7, addr0x000007e4
addr0x00000b10: lw x5, 393(x8)
addr0x00000b14: lw x27, -1640(x8)
addr0x00000b18: addi x19, x6, -1169
addr0x00000b1c: sw x6, 818(x8)
addr0x00000b20: jal x24, addr0x000000b0
addr0x00000b24: lw x19, 875(x8)
addr0x00000b28: sw x2, 1482(x8)
addr0x00000b2c: sw x9, -372(x8)
addr0x00000b30: jal x0, addr0x00000fb4
addr0x00000b34: jal x19, addr0x000009ec
addr0x00000b38: lw x27, -1045(x8)
addr0x00000b3c: lw x16, 1298(x8)
addr0x00000b40: bne x21, x19, addr0x00001060
addr0x00000b44: sw x2, 17(x8)
addr0x00000b48: addi x29, x18, 1235
addr0x00000b4c: sw x21, 1430(x8)
addr0x00000b50: lw x17, 1915(x8)
addr0x00000b54: addi x19, x20, 1578
addr0x00000b58: lw x19, -1199(x8)
addr0x00000b5c: lw x4, 156(x8)
addr0x00000b60: lw x25, 174(x8)
addr0x00000b64: lw x21, 1569(x8)
addr0x00000b68: jal x18, addr0x00000cb4
addr0x00000b6c: beq x18, x22, addr0x0000104c
addr0x00000b70: lbu x31, 146(x8)
addr0x00000b74: jal x19, addr0x000010f8
addr0x00000b78: jal x19, addr0x00000c34
addr0x00000b7c: addi x7, x14, 566
addr0x00000b80: lw x31, -592(x8)
addr0x00000b84: lw x0, -227(x8)
addr0x00000b88: lw x14, -44(x8)
addr0x00000b8c: lw x18, -785(x8)
addr0x00000b90: lw x30, -1146(x8)
addr0x00000b94: lw x24, -1872(x8)
addr0x00000b98: lw x7, -1506(x8)
addr0x00000b9c: sh x23, -1908(x8)
addr0x00000ba0: bne x27, x5, addr0x000009c8
addr0x00000ba4: beq x15, x20, addr0x00000bf0
addr0x00000ba8: addi x29, x5, 700
addr0x00000bac: jal x11, addr0x00000e64
addr0x00000bb0: lw x1, 918(x8)
addr0x00000bb4: jal x9, addr0x00000574
addr0x00000bb8: lw x18, -1338(x8)
addr0x00000bbc: addi x23, x23, 1499
addr0x00000bc0: jal x17, addr0x00000684
addr0x00000bc4: lw x17, -1130(x8)
addr0x00000bc8: addi x14, x25, -196
addr0x00000bcc: sw x30, -591(x8)
addr0x00000bd0: jal x31, addr0x00000714
addr0x00000bd4: andi x22, x7, -914
addr0x00000bd8: lw x2, -319(x8)
addr0x00000bdc: lw x28, -620(x8)
addr0x00000be0: addi x19, x30, 1090
addr0x00000be4: bge x26, x9, addr0x0000093c
addr0x00000be8: sw x5, 41(x8)
addr0x00000bec: jal x17, addr0x00000d14
addr0x00000bf0: lw x5, 91(x8)
addr0x00000bf4: sw x27, 1376(x8)
addr0x00000bf8: addi x18, x23, -2012
addr0x00000bfc: add x22, x18, x9
addr0x00000c00: jal x13, addr0x000008c4
addr0x00000c04: lw x10, -144(x8)
addr0x00000c08: lw x1, -552(x8)
addr0x00000c0c: sw x29, -1210(x8)
addr0x00000c10: lw x31, 1023(x8)
addr0x00000c14: sw x9, -70(x8)
addr0x00000c18: sw x26, -462(x8)
addr0x00000c1c: sw x30, 1762(x8)
addr0x00000c20: addi x29, x2, 1476
addr0x00000c24: addi x18, x21, 1680
addr0x00000c28: add x17, x0, x29
addr0x00000c2c: jal x7, addr0x00000c54
addr0x00000c30: jal x0, addr0x0000055c
addr0x00000c34: lw x14, -1027(x8)
addr0x00000c38: lw x9, -1526(x8)
addr0x00000c3c: lw x20, -162(x8)
addr0x00000c40: lui x28, 577337
addr0x00000c44: andi x4, x30, 1690
addr0x00000c48: lui x18, 624630
addr0x00000c4c: slli x21, x7, 17
addr0x00000c50: add x1, x15, x9
addr0x00000c54: srli x0, x17, 7
addr0x00000c58: beq x22, x19, addr0x00000c5c
addr0x00000c5c: addi x22, x2, -1764
addr0x00000c60: add x12, x0, x25
addr0x00000c64: addi x20, x23, 1327
addr0x00000c68: sw x19, 524(x8)
addr0x00000c6c: addi x28, x20, -1957
addr0x00000c70: jal x29, addr0x0000021c
addr0x00000c74: lw x23, 264(x8)
addr0x00000c78: lw x29, 754(x8)
addr0x00000c7c: lw x22, 2042(x8)
addr0x00000c80: addi x25, x4, -883
addr0x00000c84: jal x19, addr0x000003cc
addr0x00000c88: jal x26, addr0x00000250
addr0x00000c8c: sw x30, -173(x8)
addr0x00000c90: sw x30, -1916(x8)
addr0x00000c94: sw x29, 1309(x8)
addr0x00000c98: sw x29, 621(x8)
addr0x00000c9c: sw x7, -1002(x8)
addr0x00000ca0: jal x19, addr0x00001124
addr0x00000ca4: lui x23, 47548
addr0x00000ca8: addi x19, x18, -183
addr0x00000cac: bgeu x22, x26, addr0x00000fa4
addr0x00000cb0: jal x9, addr0x0000060c
addr0x00000cb4: lw x23, 834(x8)
addr0x00000cb8: lw x6, 1948(x8)
addr0x00000cbc: sb x7, -203(x8)
addr0x00000cc0: jal x29, addr0x00000bcc
addr0x00000cc4: lui x28, 875777
addr0x00000cc8: sw x17, 2010(x8)
addr0x00000ccc: jal x31, addr0x0000111c
addr0x00000cd0: jal x17, addr0x000000b4
addr0x00000cd4: lbu x31, -139(x8)
addr0x00000cd8: andi x6, x17, 718
addr0x00000cdc: slli x7, x27, 27
addr0x00000ce0: add x19, x23, x13
addr0x00000ce4: sw x25, -1393(x8)
addr0x00000ce8: sw x28, -831(x8)
addr0x00000cec: sb x7, 1044(x8)
addr0x00000cf0: jal x18, addr0x00000338
addr0x00000cf4: lw x0, -1648(x8)
addr0x00000cf8: lw x24, -502(x8)
addr0x00000cfc: beq x17, x2, addr0x00000bc0
addr0x00000d00: jal x16, addr0x0000016c
addr0x00000d04: sw x14, -1792(x8)
addr0x00000d08: sw x9, -848(x8)
addr0x00000d0c: lw x17, 1843(x8)
addr0x00000d10: addi x19, x29, -718
addr0x00000d14: addi x15, x7, 971
addr0x00000d18: lbu x30, -720(x8)
addr0x00000d1c: blt x27, x28, addr0x00000e64
addr0x00000d20: lw x31, 704(x8)
addr0x00000d24: jal x23, addr0x00000c1c
addr0x00000d28: lw x27, 379(x8)
addr0x00000d2c: jal x7, addr0x00000c54
addr0x00000d30: addi x27, x11, 1860
addr0x00000d34: sb x9, 1026(x8)
addr0x00000d38: sw x20, 225(x8)
addr0x00000d3c: sw x24, 1711(x8)
addr0x00000d40: jal x16, addr0x00000a2c
addr0x00000d44: lw x25, -1113(x8)
addr0x00000d48: addi x5, x7, -1148
addr0x00000d4c: addi x17, x15, 676
addr0x00000d50: sw x17, -983(x8)
addr0x00000d54: beq x6, x28, addr0x00000870
addr0x00000d58: lw x16, -375(x8)
addr0x00000d5c: sb x6, 550(x8)
addr0x00000d60: bne x19, x7, addr0x000006d4
addr0x00000d64: lw x11, 438(x8)
addr0x00000d68: slli x5, x5, 29
addr0x00000d6c: addi x18, x24, -574
addr0x00000d70: addi x19, x5, -1160
addr0x00000d74: and x0, x10, x4
addr0x00000d78: slli x29, x28, 3
addr0x00000d7c: lui x19, 848043
addr0x00000d80: addi x20, x14, 449
addr0x00000d84: jal x2, addr0x00000138
addr0x00000d88: lw x4, 1518(x8)
addr0x00000d8c: lw x20, 1000(x8)
addr0x00000d90: sw x0, 680(x8)
addr0x00000d94: addi x20, x9, -719
addr0x00000d98: lbu x0, 1621(x8)
addr0x00000d9c: lw x30, 566(x8)
addr0x00000da0: addi x29, x6, 544
addr0x00000da4: andi x17, x0, 1705
addr0x00000da8: lw x13, -1619(x8)
addr0x00000dac: lw x7, -244(x8)
addr0x00000db0: lw x29, -1752(x8)
addr0x00000db4: lbu x25, 1385(x8)
addr0x00000db8: addi x2, x13, -1518
addr0x00000dbc: addi x9, x29, -1015
addr0x00000dc0: ori x30, x13, 1230
addr0x00000dc4: sw x18, -1688(x8)
addr0x00000dc8: bne x17, x20, addr0x00000618
addr0x00000dcc: lbu x7, -652(x8)
addr0x00000dd0: bne x29, x20, addr0x00000ad0
addr0x00000dd4: beq x7, x28, addr0x0000073c
addr0x00000dd8: bgeu x20, x10, addr0x00000788
addr0x00000ddc: addi x18, x5, -235
addr0x00000de0: addi x4, x13, -1924
addr0x00000de4: jal x1, addr0x00000188
addr0x00000de8: lui x29, 960882
addr0x00000dec: addi x5, x0, -610
addr0x00000df0: addi x13, x29, 173
addr0x00000df4: sw x17, -623(x8)
addr0x00000df8: lui x0, 707983
addr0x00000dfc: addi x21, x20, -1271
addr0x00000e00: addi x9, x27, 726
addr0x00000e04: addi x28, x1, 406
addr0x00000e08: lw x7, 350(x8)
addr0x00000e0c: addi x16, x30, 666
addr0x00000e10: sw x9, 622(x8)
addr0x00000e14: sw x27, -1368(x8)
addr0x00000e18: sw x20, 1176(x8)
addr0x00000e1c: sw x25, -771(x8)
addr0x00000e20: sw x7, 1762(x8)
addr0x00000e24: addi x5, x0, -1636
addr0x00000e28: jal x27, addr0x00000c20
addr0x00000e2c: beq x20, x20, addr0x000007d0
addr0x00000e30: lw x25, -1525(x8)
addr0x00000e34: lw x25, -2001(x8)
addr0x00000e38: lw x12, 1065(x8)
addr0x00000e3c: lw x23, 441(x8)
addr0x00000e40: addi x19, x10, 858
addr0x00000e44: lw x22, 1176(x8)
addr0x00000e48: jal x21, addr0x000009b8
addr0x00000e4c: jal x30, addr0x00000d70
addr0x00000e50: sw x5, 1689(x8)
addr0x00000e54: addi x29, x9, -366
addr0x00000e58: ori x12, x7, 247
addr0x00000e5c: ori x17, x21, -825
addr0x00000e60: lbu x30, 1988(x8)
addr0x00000e64: lw x16, 67(x8)
addr0x00000e68: addi x19, x7, -1687
addr0x00000e6c: jal x1, addr0x000007c8
addr0x00000e70: slli x11, x13, 25
addr0x00000e74: add x1, x19, x16
addr0x00000e78: lw x15, 1674(x8)
addr0x00000e7c: addi x29, x30, 439
addr0x00000e80: jal x20, addr0x00000e68
addr0x00000e84: lui x9, 960181
addr0x00000e88: andi x23, x9, -665
addr0x00000e8c: lw x11, 1241(x8)
addr0x00000e90: addi x18, x1, -416
addr0x00000e94: sw x13, 759(x8)
addr0x00000e98: jal x27, addr0x00001104
addr0x00000e9c: lw x0, 202(x8)
addr0x00000ea0: addi x23, x31, 1292
addr0x00000ea4: sw x1, 523(x8)
addr0x00000ea8: sw x18, 998(x8)
addr0x00000eac: lw x23, -1069(x8)
addr0x00000eb0: lw x16, 1914(x8)
addr0x00000eb4: sw x22, 289(x8)
addr0x00000eb8: jal x0, addr0x00000fa0
addr0x00000ebc: lw x20, 1461(x8)
addr0x00000ec0: addi x18, x27, -1380
addr0x00000ec4: srli x28, x11, 15
addr0x00000ec8: bltu x22, x14, addr0x00000ef4
addr0x00000ecc: slli x27, x5, 11
addr0x00000ed0: add x13, x7, x5
addr0x00000ed4: add x19, x25, x0
addr0x00000ed8: jal x0, addr0x00000220
addr0x00000edc: lw x9, 937(x8)
addr0x00000ee0: lw x1, 1532(x8)
addr0x00000ee4: jal x31, addr0x000003a0
addr0x00000ee8: lw x19, 276(x8)
addr0x00000eec: bgeu x21, x30, addr0x00000b50
addr0x00000ef0: lw x10, -373(x8)
addr0x00000ef4: addi x19, x18, 667
addr0x00000ef8: jalr x0, 860(x3)
addr0x00000efc: addi x5, x2, 335
addr0x00000f00: sw x4, 676(x8)
addr0x00000f04: sw x11, -1138(x8)
addr0x00000f08: sw x21, -1665(x8)
addr0x00000f0c: jalr x0, 284(x3)
addr0x00000f10: sw x22, 1514(x8)
addr0x00000f14: sw x5, -1609(x8)
addr0x00000f18: bge x2, x10, addr0x00000d84
addr0x00000f1c: addi x7, x31, -976
addr0x00000f20: lw x10, -1583(x8)
addr0x00000f24: sh x18, -1316(x8)
addr0x00000f28: bne x31, x19, addr0x00000b44
addr0x00000f2c: lui x7, 905312
addr0x00000f30: addi x29, x19, 1273
addr0x00000f34: jal x31, addr0x000009b8
addr0x00000f38: addi x13, x31, -928
addr0x00000f3c: jal x19, addr0x000000d0
addr0x00000f40: lw x19, 1382(x8)
addr0x00000f44: lw x31, -1727(x8)
addr0x00000f48: sw x21, 559(x8)
addr0x00000f4c: lw x21, 176(x8)
addr0x00000f50: sw x31, -503(x8)
addr0x00000f54: sb x19, 37(x8)
addr0x00000f58: lw x18, -1842(x8)
addr0x00000f5c: jal x26, addr0x00000188
addr0x00000f60: sw x0, 1700(x8)
addr0x00000f64: lw x30, 1294(x8)
addr0x00000f68: add x25, x6, x13
addr0x00000f6c: addi x20, x15, 1174
addr0x00000f70: addi x31, x12, -173
addr0x00000f74: sb x28, 1603(x8)
addr0x00000f78: lw x7, 964(x8)
addr0x00000f7c: andi x18, x2, 1256
addr0x00000f80: andi x23, x22, -895
addr0x00000f84: add x29, x20, x15
addr0x00000f88: jal x29, addr0x000010c8
addr0x00000f8c: add x24, x1, x25
addr0x00000f90: jal x23, addr0x00000094
addr0x00000f94: sh x4, -1903(x8)
addr0x00000f98: slli x14, x15, 14
addr0x00000f9c: bne x17, x15, addr0x000010f8
addr0x00000fa0: bne x13, x0, addr0x00000850
addr0x00000fa4: jal x31, addr0x0000079c
addr0x00000fa8: sw x27, -1276(x8)
addr0x00000fac: sw x20, 277(x8)
addr0x00000fb0: addi x5, x0, -709
addr0x00000fb4: sw x9, 244(x8)
addr0x00000fb8: sw x23, 1997(x8)
addr0x00000fbc: sw x9, 471(x8)
addr0x00000fc0: sw x5, -1691(x8)
addr0x00000fc4: addi x18, x0, 426
addr0x00000fc8: addi x18, x14, 356
addr0x00000fcc: addi x20, x19, 278
addr0x00000fd0: jal x20, addr0x000006e8
addr0x00000fd4: lw x17, 688(x8)
addr0x00000fd8: sw x19, 1154(x8)
addr0x00000fdc: sw x31, 1440(x8)
addr0x00000fe0: jal x13, addr0x00000300
addr0x00000fe4: lw x20, 1021(x8)
addr0x00000fe8: lbu x16, -1830(x8)
addr0x00000fec: xori x11, x20, -1563
addr0x00000ff0: andi x30, x30, -1752
addr0x00000ff4: lw x17, 1477(x8)
addr0x00000ff8: addi x22, x27, -1523
addr0x00000ffc: lw x19, -779(x8)
addr0x00001000: lbu x15, 624(x8)
addr0x00001004: lw x28, -1763(x8)
addr0x00001008: lw x17, -809(x8)
addr0x0000100c: addi x0, x26, -294
addr0x00001010: beq x25, x0, addr0x00000ec0
addr0x00001014: lw x22, -1011(x8)
addr0x00001018: lw x7, -1194(x8)
addr0x0000101c: jal x10, addr0x00001094
addr0x00001020: beq x19, x27, addr0x00000e90
addr0x00001024: addi x0, x19, 1866
addr0x00001028: sw x22, -1690(x8)
addr0x0000102c: add x22, x25, x18
addr0x00001030: slli x31, x18, 4
addr0x00001034: addi x23, x31, -231
addr0x00001038: add x23, x23, x11
addr0x0000103c: jal x2, addr0x000003b0
addr0x00001040: jal x30, addr0x00000ba8
addr0x00001044: lw x19, -718(x8)
addr0x00001048: addi x20, x24, -983
addr0x0000104c: sw x11, 1159(x8)
addr0x00001050: sw x7, 980(x8)
addr0x00001054: sw x31, 443(x8)
addr0x00001058: addi x24, x13, -1764
addr0x0000105c: sw x31, 1648(x8)
addr0x00001060: sw x18, 1292(x8)
addr0x00001064: jal x20, addr0x000009c4
addr0x00001068: sub x18, x6, x31
addr0x0000106c: jal x6, addr0x000005b4
addr0x00001070: lw x25, 285(x8)
addr0x00001074: addi x2, x5, -161
addr0x00001078: sw x22, 1620(x8)
addr0x0000107c: sw x23, -1753(x8)
addr0x00001080: sw x22, 797(x8)
addr0x00001084: sw x26, 549(x8)
addr0x00001088: jal x20, addr0x00000a10
addr0x0000108c: lui x1, 562870
addr0x00001090: sw x29, -135(x8)
addr0x00001094: sw x10, 136(x8)
addr0x00001098: jal x22, addr0x00000cfc
addr0x0000109c: jal x28, addr0x000003ac
addr0x000010a0: jal x13, addr0x000007cc
addr0x000010a4: addi x7, x11, -68
addr0x000010a8: sw x7, -511(x8)
addr0x000010ac: jal x15, addr0x000006d8
addr0x000010b0: sw x22, 2021(x8)
addr0x000010b4: sw x18, -1077(x8)
addr0x000010b8: sw x27, -1217(x8)
addr0x000010bc: jal x15, addr0x000009a0
addr0x000010c0: slli x6, x4, 18
addr0x000010c4: srli x14, x28, 15
addr0x000010c8: add x0, x23, x22
addr0x000010cc: lw x25, -1162(x8)
addr0x000010d0: lw x1, -862(x8)
addr0x000010d4: lw x14, -1015(x8)
addr0x000010d8: jal x12, addr0x00000cc0
addr0x000010dc: jal x15, addr0x000001fc
addr0x000010e0: sw x6, -1603(x8)
addr0x000010e4: addi x19, x17, 7
addr0x000010e8: lui x7, 135732
addr0x000010ec: addi x14, x23, -1889
addr0x000010f0: jal x9, addr0x000002e0
addr0x000010f4: auipc x19, 675978
addr0x000010f8: jalr x0, 820(x3)
addr0x000010fc: bne x2, x28, addr0x00000b1c
addr0x00001100: sw x15, 1849(x8)
addr0x00001104: addi x15, x13, 111
addr0x00001108: slli x9, x1, 12
addr0x0000110c: srli x7, x9, 14
addr0x00001110: andi x17, x9, 16
addr0x00001114: addi x19, x22, -979
addr0x00001118: addi x0, x29, -1015
addr0x0000111c: jal x28, addr0x00000d0c
addr0x00001120: lw x1, -1838(x8)
addr0x00001124: lbu x5, 435(x8)
addr0x00001128: addi x7, x27, 919
addr0x0000112c: jal x18, addr0x000001d4
addr0x00001130: jal x5, addr0x00000d20
addr0x00001134: sw x31, -872(x8)
addr0x00001138: andi x29, x25, -1958
addr0x0000113c: addi x12, x16, -306
addr0x00001140: andi x7, x25, -1770
addr0x00001144: sub x7, x0, x7
addr0x00001148: sub x19, x25, x18
addr0x0000114c: sw x23, 148(x8)
addr0x00001150: lw x23, 819(x8)
addr0x00001154: lw x7, -1535(x8)
addr0x00001158: jal x16, addr0x0000082c
addr0x0000115c: lw x19, 1017(x8)
addr0x00001160: lw x23, -159(x8)
addr0x00001164: lw x4, 809(x8)
addr0x00001168: jal x14, addr0x00000e48
addr0x0000116c: lw x9, 763(x8)
addr0x00001170: lw x4, 1022(x8)
addr0x00001174: lw x2, -1887(x8)
addr0x00001178: sw x0, 1231(x8)
addr0x0000117c: sw x19, 1430(x8)
addr0x00001180: jal x18, addr0x00000448
addr0x00001184: sw x13, -1123(x8)
addr0x00001188: lw x20, 1795(x8)
addr0x0000118c: addi x27, x29, -557
addr0x00001190: jal x24, addr0x00000e18
addr0x00001194: lbu x7, 788(x8)
addr0x00001198: ori x17, x12, 1383
addr0x0000119c: jal x16, addr0x0000112c
addr0x000011a0: andi x1, x18, -1983
addr0x000011a4: lw x7, 827(x8)
addr0x000011a8: sw x14, 1130(x8)
addr0x000011ac: sw x29, -1097(x8)
addr0x000011b0: lbu x2, -251(x8)
addr0x000011b4: addi x18, x20, 976
addr0x000011b8: jal x27, addr0x00000550
addr0x000011bc: lw x27, 17(x8)
addr0x000011c0: lw x14, 377(x8)
addr0x000011c4: addi x25, x27, 1830
addr0x000011c8: lw x0, -1225(x8)
addr0x000011cc: jal x6, addr0x000005f4
addr0x000011d0: addi x13, x1, 283
addr0x000011d4: bgeu x5, x18, addr0x00000e5c
addr0x000011d8: lw x7, 952(x8)
addr0x000011dc: sw x28, 198(x8)
addr0x000011e0: sw x6, -693(x8)
