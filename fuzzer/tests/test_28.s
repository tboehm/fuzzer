.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x29, -765(x8)
addr0x0000001c: jal x20, addr0x000000b0
addr0x00000020: lw x17, -1928(x8)
addr0x00000024: lw x1, 293(x8)
addr0x00000028: jal x9, addr0x00000210
addr0x0000002c: lw x4, 432(x8)
addr0x00000030: sw x18, 1885(x8)
addr0x00000034: addi x27, x12, 1257
addr0x00000038: jal x12, addr0x000001b0
addr0x0000003c: sw x28, 168(x8)
addr0x00000040: andi x30, x14, 69
addr0x00000044: lw x5, 489(x8)
addr0x00000048: lw x12, 117(x8)
addr0x0000004c: lw x0, -322(x8)
addr0x00000050: bne x25, x7, addr0x0000016c
addr0x00000054: lw x19, 1298(x8)
addr0x00000058: sb x30, 934(x8)
addr0x0000005c: sw x29, 53(x8)
addr0x00000060: jal x12, addr0x00000080
addr0x00000064: jal x0, addr0x00000180
addr0x00000068: lbu x7, 195(x8)
addr0x0000006c: lbu x27, -1120(x8)
addr0x00000070: addi x23, x6, 1977
addr0x00000074: bne x13, x15, addr0x00000228
addr0x00000078: lw x22, -198(x8)
addr0x0000007c: ori x18, x17, -1037
addr0x00000080: sh x29, -1995(x8)
addr0x00000084: sw x7, 1933(x8)
addr0x00000088: jal x21, addr0x00000264
addr0x0000008c: lw x2, 1625(x8)
addr0x00000090: ori x31, x9, -1081
addr0x00000094: sw x20, 923(x8)
addr0x00000098: jal x22, addr0x000000f4
addr0x0000009c: bge x23, x13, addr0x00000224
addr0x000000a0: addi x22, x4, 599
addr0x000000a4: addi x20, x31, 459
addr0x000000a8: sw x0, 779(x8)
addr0x000000ac: lw x0, 922(x8)
addr0x000000b0: sw x4, -32(x8)
addr0x000000b4: jal x5, addr0x0000032c
addr0x000000b8: sub x4, x19, x7
addr0x000000bc: srai x16, x17, 9
addr0x000000c0: lbu x30, -837(x8)
addr0x000000c4: bge x28, x2, addr0x000000a0
addr0x000000c8: jal x30, addr0x000001f0
addr0x000000cc: lw x9, -598(x8)
addr0x000000d0: addi x22, x19, 230
addr0x000000d4: lw x22, 1581(x8)
addr0x000000d8: addi x5, x14, 182
addr0x000000dc: sw x18, -38(x8)
addr0x000000e0: lbu x26, 1460(x8)
addr0x000000e4: jalr x0, 272(x3)
addr0x000000e8: jal x29, addr0x00000028
addr0x000000ec: lw x19, 1187(x8)
addr0x000000f0: lw x1, 1421(x8)
addr0x000000f4: lw x13, 719(x8)
addr0x000000f8: lw x19, -1051(x8)
addr0x000000fc: addi x0, x1, 1435
addr0x00000100: jal x24, addr0x0000004c
addr0x00000104: lui x29, 434629
addr0x00000108: slli x10, x18, 23
addr0x0000010c: slli x20, x22, 11
addr0x00000110: srli x1, x13, 16
addr0x00000114: srli x19, x13, 7
addr0x00000118: addi x19, x4, 404
addr0x0000011c: jal x31, addr0x00000118
addr0x00000120: lw x10, 1194(x8)
addr0x00000124: jal x18, addr0x000000d8
addr0x00000128: lw x29, 899(x8)
addr0x0000012c: addi x19, x2, 146
addr0x00000130: addi x22, x27, 1393
addr0x00000134: sw x9, 956(x8)
addr0x00000138: sw x23, -298(x8)
addr0x0000013c: jal x5, addr0x00000280
addr0x00000140: sw x10, -322(x8)
addr0x00000144: lw x12, -109(x8)
addr0x00000148: addi x31, x0, 79
addr0x0000014c: jal x25, addr0x00000260
addr0x00000150: jal x2, addr0x000002d4
addr0x00000154: jal x23, addr0x000002f0
addr0x00000158: andi x23, x28, 812
addr0x0000015c: addi x19, x7, -1703
addr0x00000160: xor x19, x28, x9
addr0x00000164: bne x24, x10, addr0x000002ec
addr0x00000168: mul x20, x5, x20
addr0x0000016c: sub x13, x10, x0
addr0x00000170: beq x17, x28, addr0x00000064
addr0x00000174: addi x27, x19, -1208
addr0x00000178: sw x22, -446(x8)
addr0x0000017c: jal x27, addr0x00000120
addr0x00000180: sw x0, -1516(x8)
addr0x00000184: sw x5, -945(x8)
addr0x00000188: sw x6, 730(x8)
addr0x0000018c: blt x10, x5, addr0x00000098
addr0x00000190: lhu x17, 761(x8)
addr0x00000194: lhu x4, 1935(x8)
addr0x00000198: jal x0, addr0x0000029c
addr0x0000019c: beq x22, x10, addr0x000001fc
addr0x000001a0: lbu x31, 1408(x8)
addr0x000001a4: beq x14, x10, addr0x000000d4
addr0x000001a8: addi x12, x13, 864
addr0x000001ac: addi x21, x25, 1304
addr0x000001b0: jal x10, addr0x000001a0
addr0x000001b4: lw x11, -517(x8)
addr0x000001b8: sw x22, 534(x8)
addr0x000001bc: addi x24, x4, 51
addr0x000001c0: sw x23, -1061(x8)
addr0x000001c4: sw x9, -393(x8)
addr0x000001c8: jal x0, addr0x00000288
addr0x000001cc: addi x17, x29, 946
addr0x000001d0: jal x26, addr0x000002a8
addr0x000001d4: lw x31, 1567(x8)
addr0x000001d8: jal x14, addr0x000001ac
addr0x000001dc: jal x7, addr0x00000010
addr0x000001e0: lw x28, 1671(x8)
addr0x000001e4: addi x0, x29, -205
addr0x000001e8: addi x20, x15, -980
addr0x000001ec: addi x17, x7, 1654
addr0x000001f0: sw x30, -1047(x8)
addr0x000001f4: sw x20, -1386(x8)
addr0x000001f8: sw x0, 274(x8)
addr0x000001fc: sw x30, 478(x8)
addr0x00000200: lw x25, -1354(x8)
addr0x00000204: lw x29, 984(x8)
addr0x00000208: lbu x13, 1460(x8)
addr0x0000020c: beq x5, x22, addr0x00000348
addr0x00000210: beq x4, x28, addr0x000001bc
addr0x00000214: addi x24, x14, -10
addr0x00000218: sw x22, -1988(x8)
addr0x0000021c: lbu x17, 1023(x8)
addr0x00000220: slli x23, x30, 16
addr0x00000224: or x26, x31, x20
addr0x00000228: lw x29, 597(x8)
addr0x0000022c: lw x27, -1313(x8)
addr0x00000230: sw x17, -187(x8)
addr0x00000234: sw x27, -434(x8)
addr0x00000238: sw x23, 888(x8)
addr0x0000023c: addi x12, x17, 1662
addr0x00000240: sw x20, -125(x8)
addr0x00000244: sw x19, -768(x8)
addr0x00000248: sw x2, 1539(x8)
addr0x0000024c: addi x28, x24, 1652
addr0x00000250: jal x31, addr0x000002e0
addr0x00000254: lw x19, -858(x8)
addr0x00000258: lw x30, 181(x8)
addr0x0000025c: add x27, x24, x7
addr0x00000260: sw x9, -1739(x8)
addr0x00000264: sw x13, -347(x8)
addr0x00000268: sb x19, 1132(x8)
addr0x0000026c: sw x30, -460(x8)
addr0x00000270: beq x9, x0, addr0x000002ec
addr0x00000274: addi x13, x24, -585
addr0x00000278: sw x13, 1769(x8)
addr0x0000027c: sw x6, 1316(x8)
addr0x00000280: addi x1, x18, 213
addr0x00000284: add x18, x20, x24
addr0x00000288: jal x10, addr0x000001f8
addr0x0000028c: addi x7, x4, -226
addr0x00000290: lui x10, 604670
addr0x00000294: addi x0, x20, -1702
addr0x00000298: addi x16, x6, 188
addr0x0000029c: jal x0, addr0x000001f8
addr0x000002a0: lw x30, 1888(x8)
addr0x000002a4: lw x7, 22(x8)
addr0x000002a8: addi x0, x14, 221
addr0x000002ac: addi x21, x27, 385
addr0x000002b0: addi x30, x24, -122
addr0x000002b4: beq x19, x5, addr0x000001d0
addr0x000002b8: lw x7, -546(x8)
addr0x000002bc: addi x31, x17, 1383
addr0x000002c0: lw x29, 729(x8)
addr0x000002c4: slli x13, x28, 0
addr0x000002c8: sw x20, 371(x8)
addr0x000002cc: sw x12, -1169(x8)
addr0x000002d0: jal x4, addr0x0000017c
addr0x000002d4: bgeu x15, x9, addr0x00000064
addr0x000002d8: bgeu x29, x9, addr0x00000338
addr0x000002dc: jal x18, addr0x00000274
addr0x000002e0: addi x28, x2, 241
addr0x000002e4: sh x0, -1972(x8)
addr0x000002e8: addi x29, x17, 257
addr0x000002ec: sw x19, -451(x8)
addr0x000002f0: lw x14, 992(x8)
addr0x000002f4: addi x30, x30, -155
addr0x000002f8: jal x11, addr0x00000174
addr0x000002fc: srli x21, x1, 9
addr0x00000300: jal x0, addr0x0000023c
addr0x00000304: jal x23, addr0x000002d8
addr0x00000308: jal x0, addr0x0000004c
addr0x0000030c: lw x31, 1828(x8)
addr0x00000310: sw x22, 68(x8)
addr0x00000314: sw x6, 17(x8)
addr0x00000318: bgeu x1, x5, addr0x00000338
addr0x0000031c: lw x10, 1815(x8)
addr0x00000320: bne x6, x14, addr0x0000034c
addr0x00000324: lw x19, 761(x8)
addr0x00000328: sw x2, 1202(x8)
addr0x0000032c: jal x31, addr0x00000220
addr0x00000330: addi x19, x18, -832
addr0x00000334: addi x6, x28, -1753
addr0x00000338: jal x19, addr0x000002b8
addr0x0000033c: lw x18, 1968(x8)
addr0x00000340: lw x25, -1087(x8)
addr0x00000344: addi x10, x10, -325
addr0x00000348: jal x31, addr0x00000058
addr0x0000034c: jal x20, addr0x000001d8
addr0x00000350: lw x23, -5(x8)
addr0x00000354: addi x19, x7, -1950
addr0x00000358: sw x31, 471(x8)
addr0x0000035c: addi x23, x9, -1503
addr0x00000360: sw x4, -1395(x8)
addr0x00000364: sw x5, 847(x8)
addr0x00000368: addi x11, x13, 1852
addr0x0000036c: jal x7, addr0x000000ec
addr0x00000370: sh x30, -1611(x8)
addr0x00000374: bne x13, x25, addr0x00000314
addr0x00000378: jal x18, addr0x000000fc
addr0x0000037c: lw x16, -1311(x8)
addr0x00000380: lw x7, 1137(x8)
addr0x00000384: lw x17, 1669(x8)
addr0x00000388: lw x31, -184(x8)
addr0x0000038c: lui x13, 14250
addr0x00000390: addi x4, x7, 588
addr0x00000394: jal x15, addr0x0000006c
