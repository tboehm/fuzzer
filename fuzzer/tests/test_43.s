.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bgeu x22, x7, addr0x00000538
addr0x0000001c: lui x27, 606060
addr0x00000020: addi x10, x28, -2014
addr0x00000024: bne x16, x10, addr0x000001dc
addr0x00000028: lw x0, 2005(x8)
addr0x0000002c: sub x17, x22, x18
addr0x00000030: lw x4, -55(x8)
addr0x00000034: slli x5, x18, 5
addr0x00000038: lw x25, -83(x8)
addr0x0000003c: lw x21, -1728(x8)
addr0x00000040: lw x23, 1716(x8)
addr0x00000044: lw x23, -2019(x8)
addr0x00000048: srli x31, x0, 7
addr0x0000004c: lw x27, -28(x8)
addr0x00000050: lw x21, 1871(x8)
addr0x00000054: lw x11, -1931(x8)
addr0x00000058: jal x18, addr0x00000454
addr0x0000005c: lw x24, 321(x8)
addr0x00000060: lw x28, 1590(x8)
addr0x00000064: lw x24, -1749(x8)
addr0x00000068: addi x29, x19, -1261
addr0x0000006c: lw x5, -409(x8)
addr0x00000070: lw x15, -149(x8)
addr0x00000074: lw x23, 224(x8)
addr0x00000078: jal x25, addr0x000000cc
addr0x0000007c: sw x4, 1136(x8)
addr0x00000080: sw x28, 36(x8)
addr0x00000084: sw x23, -1119(x8)
addr0x00000088: addi x18, x14, -1061
addr0x0000008c: sb x19, 429(x8)
addr0x00000090: sw x23, 1995(x8)
addr0x00000094: sw x27, -1297(x8)
addr0x00000098: sw x7, 1888(x8)
addr0x0000009c: lui x13, 1037090
addr0x000000a0: bltu x21, x22, addr0x000002d0
addr0x000000a4: lw x9, -1263(x8)
addr0x000000a8: bltu x17, x22, addr0x000003fc
addr0x000000ac: bge x19, x0, addr0x00000314
addr0x000000b0: bge x6, x2, addr0x000000c0
addr0x000000b4: jal x25, addr0x00000198
addr0x000000b8: sw x7, -309(x8)
addr0x000000bc: bne x13, x0, addr0x0000041c
addr0x000000c0: sltu x4, x29, x27
addr0x000000c4: or x14, x24, x29
addr0x000000c8: lbu x28, 1356(x8)
addr0x000000cc: lw x12, -1124(x8)
addr0x000000d0: addi x19, x13, 274
addr0x000000d4: lw x14, 1736(x8)
addr0x000000d8: lw x28, -1513(x8)
addr0x000000dc: addi x30, x9, 1475
addr0x000000e0: lw x5, 186(x8)
addr0x000000e4: lw x19, -995(x8)
addr0x000000e8: lw x29, 1932(x8)
addr0x000000ec: sw x23, 561(x8)
addr0x000000f0: auipc x13, 27708
addr0x000000f4: addi x19, x18, -715
addr0x000000f8: sw x13, -1536(x8)
addr0x000000fc: sw x20, -1232(x8)
addr0x00000100: slli x23, x4, 3
addr0x00000104: addi x22, x17, -1946
addr0x00000108: jal x23, addr0x0000002c
addr0x0000010c: sub x11, x31, x14
addr0x00000110: beq x21, x7, addr0x00000098
addr0x00000114: sll x15, x18, x21
addr0x00000118: or x27, x30, x12
addr0x0000011c: lui x24, 550372
addr0x00000120: sw x27, -758(x8)
addr0x00000124: sw x25, 834(x8)
addr0x00000128: sw x13, 256(x8)
addr0x0000012c: sw x7, -316(x8)
addr0x00000130: sw x20, 193(x8)
addr0x00000134: jal x7, addr0x000000a8
addr0x00000138: bne x13, x20, addr0x000003dc
addr0x0000013c: lw x27, -1580(x8)
addr0x00000140: slli x23, x7, 25
addr0x00000144: lui x29, 633928
addr0x00000148: lui x25, 245543
addr0x0000014c: lw x18, 1982(x8)
addr0x00000150: blt x27, x19, addr0x000004c4
addr0x00000154: lw x14, 29(x8)
addr0x00000158: addi x21, x27, -1010
addr0x0000015c: sw x10, -1636(x8)
addr0x00000160: lw x27, -71(x8)
addr0x00000164: lw x17, -1150(x8)
addr0x00000168: sw x28, -230(x8)
addr0x0000016c: lw x24, 470(x8)
addr0x00000170: addi x7, x22, 1950
addr0x00000174: sw x24, -494(x8)
addr0x00000178: jal x19, addr0x00000138
addr0x0000017c: lw x18, -1493(x8)
addr0x00000180: sw x27, -163(x8)
addr0x00000184: sw x0, -197(x8)
addr0x00000188: sw x20, 859(x8)
addr0x0000018c: jal x0, addr0x00000444
addr0x00000190: addi x25, x25, 1460
addr0x00000194: lhu x29, -31(x8)
addr0x00000198: beq x2, x27, addr0x000004e8
addr0x0000019c: lw x5, -1669(x8)
addr0x000001a0: addi x14, x22, 1323
addr0x000001a4: jal x20, addr0x000002c8
addr0x000001a8: addi x14, x22, -504
addr0x000001ac: sw x24, 1462(x8)
addr0x000001b0: beq x18, x9, addr0x000001a0
addr0x000001b4: slti x13, x5, -1356
addr0x000001b8: addi x31, x20, -2038
addr0x000001bc: addi x11, x17, -35
addr0x000001c0: jal x15, addr0x0000015c
addr0x000001c4: sw x22, 1957(x8)
addr0x000001c8: jal x12, addr0x00000598
addr0x000001cc: lw x27, -1594(x8)
addr0x000001d0: addi x31, x9, 1456
addr0x000001d4: jal x18, addr0x000002c4
addr0x000001d8: bne x20, x27, addr0x0000037c
addr0x000001dc: jal x19, addr0x0000044c
addr0x000001e0: addi x26, x17, 2006
addr0x000001e4: sw x27, 1136(x8)
addr0x000001e8: sw x23, 1639(x8)
addr0x000001ec: jal x18, addr0x00000468
addr0x000001f0: addi x22, x6, 1842
addr0x000001f4: sw x5, -2004(x8)
addr0x000001f8: addi x7, x20, -236
addr0x000001fc: lw x30, 1039(x8)
addr0x00000200: sw x7, -1807(x8)
addr0x00000204: jal x7, addr0x00000284
addr0x00000208: sw x18, 198(x8)
addr0x0000020c: lw x4, -1173(x8)
addr0x00000210: jal x11, addr0x0000049c
addr0x00000214: lw x30, -212(x8)
addr0x00000218: lw x22, 1880(x8)
addr0x0000021c: lw x24, -1843(x8)
addr0x00000220: addi x27, x30, 122
addr0x00000224: sw x22, -158(x8)
addr0x00000228: lui x27, 260591
addr0x0000022c: sw x28, 1820(x8)
addr0x00000230: sw x19, 1026(x8)
addr0x00000234: sw x21, 1883(x8)
addr0x00000238: addi x29, x17, 5
addr0x0000023c: jal x30, addr0x000001c8
addr0x00000240: lui x20, 607110
addr0x00000244: addi x6, x27, -1271
addr0x00000248: bge x21, x27, addr0x000000f4
addr0x0000024c: lui x31, 44722
addr0x00000250: sw x1, -665(x8)
addr0x00000254: sw x17, -619(x8)
addr0x00000258: addi x22, x26, -22
addr0x0000025c: bgeu x21, x25, addr0x00000120
addr0x00000260: bgeu x14, x2, addr0x00000008
addr0x00000264: jal x13, addr0x00000008
addr0x00000268: jal x30, addr0x00000384
addr0x0000026c: jal x28, addr0x000004a4
addr0x00000270: lw x28, -62(x8)
addr0x00000274: jal x19, addr0x00000190
addr0x00000278: addi x26, x6, 147
addr0x0000027c: addi x13, x29, 400
addr0x00000280: jal x22, addr0x00000328
addr0x00000284: lbu x26, 154(x8)
addr0x00000288: lbu x9, -2019(x8)
addr0x0000028c: andi x29, x14, 1315
addr0x00000290: jal x0, addr0x00000028
addr0x00000294: add x17, x23, x5
addr0x00000298: lw x22, 1264(x8)
addr0x0000029c: bgeu x20, x20, addr0x0000050c
addr0x000002a0: jal x20, addr0x00000024
addr0x000002a4: addi x27, x18, 1967
addr0x000002a8: jal x27, addr0x00000560
addr0x000002ac: addi x30, x22, 626
addr0x000002b0: jal x0, addr0x000003b0
addr0x000002b4: lw x18, -291(x8)
addr0x000002b8: sw x1, 159(x8)
addr0x000002bc: addi x21, x22, -1405
addr0x000002c0: sh x27, -957(x8)
addr0x000002c4: sw x24, -1508(x8)
addr0x000002c8: sw x10, -1435(x8)
addr0x000002cc: jal x6, addr0x00000574
addr0x000002d0: lw x10, 163(x8)
addr0x000002d4: lw x11, -812(x8)
addr0x000002d8: sw x1, 819(x8)
addr0x000002dc: addi x1, x18, 1333
addr0x000002e0: sw x31, -435(x8)
addr0x000002e4: beq x18, x31, addr0x00000598
addr0x000002e8: lw x5, -281(x8)
addr0x000002ec: bge x5, x9, addr0x000002d0
addr0x000002f0: lui x28, 693926
addr0x000002f4: addi x4, x18, 862
addr0x000002f8: jal x9, addr0x000003ec
addr0x000002fc: sw x16, -141(x8)
addr0x00000300: addi x7, x18, -1665
addr0x00000304: jalr x0, 148(x3)
addr0x00000308: jal x24, addr0x00000048
addr0x0000030c: jal x1, addr0x000002e8
addr0x00000310: lbu x0, -160(x8)
addr0x00000314: jal x19, addr0x0000039c
addr0x00000318: jal x7, addr0x00000230
addr0x0000031c: addi x25, x17, 1022
addr0x00000320: lui x12, 310706
addr0x00000324: addi x20, x16, -193
addr0x00000328: jal x7, addr0x0000005c
addr0x0000032c: lw x13, -1980(x8)
addr0x00000330: addi x4, x13, 1150
addr0x00000334: blt x14, x20, addr0x00000508
addr0x00000338: lw x27, 1807(x8)
addr0x0000033c: lw x18, 608(x8)
addr0x00000340: lw x18, 2009(x8)
addr0x00000344: addi x20, x5, 178
addr0x00000348: jal x27, addr0x00000218
addr0x0000034c: jal x4, addr0x000004a4
addr0x00000350: lw x31, 224(x8)
addr0x00000354: sw x1, -146(x8)
addr0x00000358: sw x25, 820(x8)
addr0x0000035c: sw x26, -1521(x8)
addr0x00000360: lw x29, -1889(x8)
addr0x00000364: lw x19, -1716(x8)
addr0x00000368: lw x9, -1414(x8)
addr0x0000036c: sw x11, -326(x8)
addr0x00000370: addi x13, x30, -1584
addr0x00000374: jal x23, addr0x00000468
addr0x00000378: sw x4, -1270(x8)
addr0x0000037c: sw x2, 1534(x8)
addr0x00000380: lw x14, 590(x8)
addr0x00000384: addi x13, x14, -1315
addr0x00000388: sw x15, 316(x8)
addr0x0000038c: sw x31, 2020(x8)
addr0x00000390: lw x11, -1537(x8)
addr0x00000394: addi x28, x13, 758
addr0x00000398: sw x27, -1230(x8)
addr0x0000039c: addi x2, x22, -1197
addr0x000003a0: jal x15, addr0x000001ac
addr0x000003a4: lw x6, -1424(x8)
addr0x000003a8: sw x26, -999(x8)
addr0x000003ac: sw x17, 1661(x8)
addr0x000003b0: jal x18, addr0x000003f8
addr0x000003b4: lbu x27, 590(x8)
addr0x000003b8: sb x29, 1477(x8)
addr0x000003bc: jal x9, addr0x0000043c
addr0x000003c0: sw x2, 1288(x8)
addr0x000003c4: jal x14, addr0x00000478
addr0x000003c8: jal x5, addr0x0000007c
addr0x000003cc: jal x13, addr0x00000590
addr0x000003d0: beq x21, x18, addr0x00000330
addr0x000003d4: bgeu x29, x2, addr0x00000104
addr0x000003d8: lw x23, -1377(x8)
addr0x000003dc: lw x30, 614(x8)
addr0x000003e0: sw x10, -1647(x8)
addr0x000003e4: addi x31, x12, 1535
addr0x000003e8: bge x29, x12, addr0x00000044
addr0x000003ec: addi x29, x0, -1696
addr0x000003f0: sw x9, 1184(x8)
addr0x000003f4: sw x12, -1078(x8)
addr0x000003f8: lui x18, 793105
addr0x000003fc: addi x9, x23, -673
addr0x00000400: sw x1, -1631(x8)
addr0x00000404: sw x27, 27(x8)
addr0x00000408: sw x29, -1586(x8)
addr0x0000040c: lui x24, 366918
addr0x00000410: addi x20, x9, -1782
addr0x00000414: addi x13, x27, -1134
addr0x00000418: jal x2, addr0x0000004c
addr0x0000041c: addi x12, x24, -1454
addr0x00000420: sw x18, -1741(x8)
addr0x00000424: addi x18, x16, -1587
addr0x00000428: jal x31, addr0x000000f4
addr0x0000042c: lw x20, -1754(x8)
addr0x00000430: addi x7, x6, 160
addr0x00000434: addi x14, x15, 936
addr0x00000438: addi x7, x23, 1045
addr0x0000043c: jal x0, addr0x000002ac
addr0x00000440: jal x28, addr0x000002a8
addr0x00000444: jal x20, addr0x00000314
addr0x00000448: andi x31, x0, -340
addr0x0000044c: lw x22, -233(x8)
addr0x00000450: lw x10, 1571(x8)
addr0x00000454: jal x19, addr0x0000023c
addr0x00000458: jal x12, addr0x00000248
addr0x0000045c: srli x29, x1, 31
addr0x00000460: add x20, x21, x18
addr0x00000464: slli x14, x0, 26
addr0x00000468: sw x23, 292(x8)
addr0x0000046c: jalr x0, 168(x3)
addr0x00000470: lui x22, 681145
addr0x00000474: sw x17, 699(x8)
addr0x00000478: sw x17, 801(x8)
addr0x0000047c: sw x7, -361(x8)
addr0x00000480: jal x29, addr0x00000574
addr0x00000484: or x16, x0, x19
addr0x00000488: lw x13, -1699(x8)
addr0x0000048c: sw x14, -583(x8)
addr0x00000490: sw x29, 689(x8)
addr0x00000494: sw x13, 1137(x8)
addr0x00000498: lui x9, 550517
addr0x0000049c: addi x31, x7, -1272
addr0x000004a0: sw x14, -940(x8)
addr0x000004a4: sw x9, -489(x8)
addr0x000004a8: sw x22, 1649(x8)
addr0x000004ac: sw x11, -1675(x8)
addr0x000004b0: jal x7, addr0x000001b4
addr0x000004b4: lui x20, 316910
addr0x000004b8: sw x22, -1594(x8)
addr0x000004bc: jal x0, addr0x00000010
addr0x000004c0: lw x31, 1456(x8)
addr0x000004c4: lw x20, 604(x8)
addr0x000004c8: addi x27, x20, 232
addr0x000004cc: jalr x0, 1000(x3)
addr0x000004d0: sb x25, 53(x8)
addr0x000004d4: lbu x7, -1108(x8)
addr0x000004d8: beq x0, x28, addr0x00000500
addr0x000004dc: add x31, x0, x25
addr0x000004e0: lui x31, 815925
addr0x000004e4: addi x4, x5, -751
addr0x000004e8: addi x18, x22, 1049
addr0x000004ec: addi x31, x9, -1769
addr0x000004f0: bne x27, x27, addr0x00000468
addr0x000004f4: addi x17, x31, -901
addr0x000004f8: lhu x31, -549(x8)
addr0x000004fc: addi x31, x18, 1989
addr0x00000500: jal x31, addr0x0000059c
addr0x00000504: jal x12, addr0x000001d4
addr0x00000508: lui x31, 773261
addr0x0000050c: bne x7, x21, addr0x000002c8
addr0x00000510: lw x20, 619(x8)
addr0x00000514: sb x5, -2033(x8)
addr0x00000518: jal x29, addr0x0000021c
addr0x0000051c: addi x28, x1, -1342
addr0x00000520: addi x31, x2, -1039
addr0x00000524: sw x19, 1005(x8)
addr0x00000528: jal x5, addr0x00000450
addr0x0000052c: lui x14, 125549
addr0x00000530: addi x31, x23, 1210
addr0x00000534: sw x7, -374(x8)
addr0x00000538: sw x31, -637(x8)
addr0x0000053c: sw x5, 1081(x8)
addr0x00000540: jal x17, addr0x000000d4
addr0x00000544: sw x5, 749(x8)
addr0x00000548: sw x7, 114(x8)
addr0x0000054c: andi x29, x9, 973
addr0x00000550: addi x23, x5, -1512
addr0x00000554: jal x10, addr0x00000514
addr0x00000558: sw x24, -479(x8)
addr0x0000055c: sw x22, -1727(x8)
addr0x00000560: jal x20, addr0x000002e0
addr0x00000564: jal x31, addr0x000004e4
addr0x00000568: addi x7, x22, 1946
addr0x0000056c: sw x7, -99(x8)
addr0x00000570: sw x6, -517(x8)
addr0x00000574: sw x22, -1232(x8)
addr0x00000578: sw x28, 833(x8)
addr0x0000057c: lbu x20, -985(x8)
addr0x00000580: slli x22, x5, 12
addr0x00000584: bgeu x4, x31, addr0x0000018c
addr0x00000588: slli x27, x15, 8
addr0x0000058c: lbu x27, -1350(x8)
addr0x00000590: sb x18, 415(x8)
addr0x00000594: lui x11, 171559
addr0x00000598: addi x13, x4, -1107
addr0x0000059c: bltu x31, x31, addr0x00000560
