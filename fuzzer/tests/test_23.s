.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: jal x19, addr0x000000e0
addr0x0000001c: jal x19, addr0x000002e8
addr0x00000020: beq x0, x14, addr0x0000009c
addr0x00000024: jal x11, addr0x000002e8
addr0x00000028: jal x12, addr0x00000130
addr0x0000002c: slli x30, x0, 15
addr0x00000030: srli x14, x29, 15
addr0x00000034: jal x5, addr0x000001ec
addr0x00000038: jal x22, addr0x0000043c
addr0x0000003c: sw x12, -469(x8)
addr0x00000040: jal x31, addr0x00000458
addr0x00000044: lw x4, -140(x8)
addr0x00000048: lw x31, 1420(x8)
addr0x0000004c: bne x29, x27, addr0x000002f4
addr0x00000050: lw x25, 935(x8)
addr0x00000054: addi x16, x5, -589
addr0x00000058: sw x19, 1953(x8)
addr0x0000005c: lw x17, -1908(x8)
addr0x00000060: addi x17, x19, 262
addr0x00000064: lw x23, -185(x8)
addr0x00000068: lw x15, -1097(x8)
addr0x0000006c: jal x25, addr0x000002a8
addr0x00000070: slli x23, x20, 23
addr0x00000074: sw x2, 828(x8)
addr0x00000078: sw x20, -88(x8)
addr0x0000007c: lbu x6, 1611(x8)
addr0x00000080: addi x17, x11, -1596
addr0x00000084: lw x4, 481(x8)
addr0x00000088: lw x17, -19(x8)
addr0x0000008c: lw x14, 1884(x8)
addr0x00000090: lw x30, 1788(x8)
addr0x00000094: ori x2, x30, -1621
addr0x00000098: sw x13, -720(x8)
addr0x0000009c: addi x7, x17, 1744
addr0x000000a0: sw x4, -1422(x8)
addr0x000000a4: addi x27, x12, -506
addr0x000000a8: lui x15, 85609
addr0x000000ac: addi x25, x13, -1034
addr0x000000b0: jal x15, addr0x00000014
addr0x000000b4: lw x22, 988(x8)
addr0x000000b8: lw x30, -255(x8)
addr0x000000bc: lw x22, -1045(x8)
addr0x000000c0: jal x1, addr0x000003f8
addr0x000000c4: sw x23, -1922(x8)
addr0x000000c8: lw x30, -663(x8)
addr0x000000cc: add x14, x21, x4
addr0x000000d0: sw x25, -2005(x8)
addr0x000000d4: sw x23, -1802(x8)
addr0x000000d8: sw x19, -220(x8)
addr0x000000dc: addi x4, x0, -1797
addr0x000000e0: jal x29, addr0x00000554
addr0x000000e4: sw x17, 1302(x8)
addr0x000000e8: jal x31, addr0x00000394
addr0x000000ec: jal x22, addr0x000003a4
addr0x000000f0: lw x19, -964(x8)
addr0x000000f4: addi x1, x13, 1236
addr0x000000f8: jal x29, addr0x00000324
addr0x000000fc: lw x15, 778(x8)
addr0x00000100: lw x10, -548(x8)
addr0x00000104: bne x18, x29, addr0x00000074
addr0x00000108: lw x19, 1719(x8)
addr0x0000010c: beq x1, x25, addr0x000000c8
addr0x00000110: addi x0, x27, -97
addr0x00000114: lui x22, 314540
addr0x00000118: sw x7, 1886(x8)
addr0x0000011c: jal x17, addr0x00000028
addr0x00000120: sw x30, 164(x8)
addr0x00000124: addi x28, x30, -1878
addr0x00000128: jal x7, addr0x000000d8
addr0x0000012c: lw x2, 1745(x8)
addr0x00000130: srli x0, x19, 8
addr0x00000134: add x13, x19, x24
addr0x00000138: jal x19, addr0x0000055c
addr0x0000013c: jal x17, addr0x000004fc
addr0x00000140: addi x25, x6, -405
addr0x00000144: srli x24, x14, 8
addr0x00000148: slli x19, x0, 18
addr0x0000014c: jal x12, addr0x00000084
addr0x00000150: jal x29, addr0x00000184
addr0x00000154: sb x23, 3(x8)
addr0x00000158: add x14, x12, x7
addr0x0000015c: sw x25, 1137(x8)
addr0x00000160: sw x24, 963(x8)
addr0x00000164: sw x19, 1943(x8)
addr0x00000168: sw x29, -282(x8)
addr0x0000016c: jal x13, addr0x00000240
addr0x00000170: srai x24, x20, 16
addr0x00000174: sw x14, -464(x8)
addr0x00000178: andi x19, x13, 1672
addr0x0000017c: jal x21, addr0x00000318
addr0x00000180: jal x0, addr0x00000010
addr0x00000184: jal x27, addr0x00000084
addr0x00000188: sw x18, -1880(x8)
addr0x0000018c: addi x27, x22, 803
addr0x00000190: sw x24, -1903(x8)
addr0x00000194: sw x15, -1156(x8)
addr0x00000198: slli x22, x25, 31
addr0x0000019c: lui x11, 90632
addr0x000001a0: sw x4, -741(x8)
addr0x000001a4: lw x18, 1371(x8)
addr0x000001a8: lw x30, -832(x8)
addr0x000001ac: sw x12, -1200(x8)
addr0x000001b0: lw x22, 20(x8)
addr0x000001b4: jal x29, addr0x0000059c
addr0x000001b8: addi x22, x19, -1775
addr0x000001bc: lw x25, 1655(x8)
addr0x000001c0: lw x9, -756(x8)
addr0x000001c4: addi x7, x5, 1745
addr0x000001c8: sw x25, 1477(x8)
addr0x000001cc: sw x0, -1797(x8)
addr0x000001d0: lui x20, 746739
addr0x000001d4: addi x19, x13, 1406
addr0x000001d8: addi x24, x25, 583
addr0x000001dc: sw x9, -338(x8)
addr0x000001e0: lw x18, -1464(x8)
addr0x000001e4: jal x17, addr0x000000f0
addr0x000001e8: lw x22, 940(x8)
addr0x000001ec: jal x16, addr0x00000478
addr0x000001f0: lw x6, 629(x8)
addr0x000001f4: sw x28, -334(x8)
addr0x000001f8: jal x23, addr0x000003e4
addr0x000001fc: lw x22, 933(x8)
addr0x00000200: addi x2, x18, -467
addr0x00000204: lui x20, 604799
addr0x00000208: addi x20, x20, 2026
addr0x0000020c: addi x21, x31, -1069
addr0x00000210: sw x27, -496(x8)
addr0x00000214: sw x13, -1548(x8)
addr0x00000218: jal x6, addr0x0000059c
addr0x0000021c: lw x16, -1826(x8)
addr0x00000220: sw x13, 188(x8)
addr0x00000224: sw x14, 478(x8)
addr0x00000228: addi x29, x18, -1854
addr0x0000022c: bne x27, x13, addr0x00000444
addr0x00000230: add x26, x22, x29
addr0x00000234: addi x7, x24, 153
addr0x00000238: lw x14, 1203(x8)
addr0x0000023c: sw x28, -1906(x8)
addr0x00000240: xori x15, x16, 1987
addr0x00000244: andi x0, x0, -195
addr0x00000248: lw x15, -1974(x8)
addr0x0000024c: slli x19, x22, 8
addr0x00000250: jal x27, addr0x000002dc
addr0x00000254: sw x4, -1089(x8)
addr0x00000258: addi x31, x17, -1574
addr0x0000025c: sw x0, -1507(x8)
addr0x00000260: jal x17, addr0x00000050
addr0x00000264: addi x13, x20, -552
addr0x00000268: lui x29, 87427
addr0x0000026c: addi x18, x13, -1282
addr0x00000270: sw x1, -1644(x8)
addr0x00000274: sw x19, 618(x8)
addr0x00000278: sh x25, -1800(x8)
addr0x0000027c: ori x0, x7, 809
addr0x00000280: bne x0, x29, addr0x000000d8
addr0x00000284: addi x5, x0, 253
addr0x00000288: sw x22, 114(x8)
addr0x0000028c: jal x22, addr0x000004c4
addr0x00000290: addi x9, x21, -1465
addr0x00000294: lw x5, -1244(x8)
addr0x00000298: lw x24, -934(x8)
addr0x0000029c: addi x14, x23, -1542
addr0x000002a0: sw x23, 515(x8)
addr0x000002a4: lw x22, -1014(x8)
addr0x000002a8: addi x19, x24, 487
addr0x000002ac: jal x23, addr0x0000026c
addr0x000002b0: lw x23, 599(x8)
addr0x000002b4: andi x22, x30, -500
addr0x000002b8: sw x22, -599(x8)
addr0x000002bc: lw x2, 140(x8)
addr0x000002c0: addi x25, x16, -2026
addr0x000002c4: sw x9, -496(x8)
addr0x000002c8: addi x29, x14, -6
addr0x000002cc: sw x25, 660(x8)
addr0x000002d0: sw x0, -112(x8)
addr0x000002d4: sw x17, 1910(x8)
addr0x000002d8: addi x18, x29, 864
addr0x000002dc: lw x19, -879(x8)
addr0x000002e0: addi x29, x2, -1165
addr0x000002e4: andi x17, x17, -904
addr0x000002e8: sw x5, 1894(x8)
addr0x000002ec: addi x9, x14, -1388
addr0x000002f0: jal x10, addr0x00000388
addr0x000002f4: lw x9, 1528(x8)
addr0x000002f8: ori x23, x22, -1388
addr0x000002fc: sw x0, 1808(x8)
addr0x00000300: jal x15, addr0x000001b4
addr0x00000304: lui x30, 809034
addr0x00000308: addi x4, x27, -1280
addr0x0000030c: addi x10, x15, -2009
addr0x00000310: sw x19, 139(x8)
addr0x00000314: sb x19, 1656(x8)
addr0x00000318: sub x17, x7, x19
addr0x0000031c: beq x13, x13, addr0x000003dc
addr0x00000320: lw x4, 1221(x8)
addr0x00000324: bne x7, x25, addr0x00000338
addr0x00000328: addi x23, x2, -37
addr0x0000032c: addi x0, x5, -83
addr0x00000330: lw x2, -1766(x8)
addr0x00000334: lw x7, -822(x8)
addr0x00000338: jal x30, addr0x000002f8
addr0x0000033c: lw x31, -208(x8)
addr0x00000340: bgeu x19, x7, addr0x00000020
addr0x00000344: sub x29, x28, x25
addr0x00000348: jal x25, addr0x000004a4
addr0x0000034c: lui x13, 385336
addr0x00000350: addi x29, x27, -1008
addr0x00000354: lw x22, 1886(x8)
addr0x00000358: jal x17, addr0x00000280
addr0x0000035c: lw x4, -1124(x8)
addr0x00000360: jal x9, addr0x00000280
addr0x00000364: jal x21, addr0x0000004c
addr0x00000368: lw x13, 1394(x8)
addr0x0000036c: lw x14, 1959(x8)
addr0x00000370: lw x2, -856(x8)
addr0x00000374: jal x9, addr0x00000060
addr0x00000378: lw x4, -1548(x8)
addr0x0000037c: lw x2, 1873(x8)
addr0x00000380: jal x15, addr0x00000340
addr0x00000384: lw x2, -717(x8)
addr0x00000388: sw x17, 1265(x8)
addr0x0000038c: sw x5, 716(x8)
addr0x00000390: addi x14, x31, -579
addr0x00000394: addi x16, x30, -1128
addr0x00000398: jal x27, addr0x000000f8
addr0x0000039c: addi x12, x22, -377
addr0x000003a0: bgeu x19, x30, addr0x000000bc
addr0x000003a4: lbu x27, -925(x8)
addr0x000003a8: lw x7, 1979(x8)
addr0x000003ac: jal x7, addr0x000004c0
addr0x000003b0: lw x14, 498(x8)
addr0x000003b4: add x27, x7, x7
addr0x000003b8: jal x25, addr0x00000230
addr0x000003bc: sw x9, 786(x8)
addr0x000003c0: addi x17, x27, 968
addr0x000003c4: lui x14, 129496
addr0x000003c8: lw x2, -1949(x8)
addr0x000003cc: lw x14, -1830(x8)
addr0x000003d0: lw x7, 1489(x8)
addr0x000003d4: mul x25, x25, x31
addr0x000003d8: lw x1, -1855(x8)
addr0x000003dc: sw x10, 204(x8)
addr0x000003e0: jal x25, addr0x00000078
addr0x000003e4: addi x29, x31, 1277
addr0x000003e8: jal x5, addr0x000005bc
addr0x000003ec: lw x0, -943(x8)
addr0x000003f0: sw x13, -2(x8)
addr0x000003f4: sw x23, -101(x8)
addr0x000003f8: jal x20, addr0x000003c4
addr0x000003fc: sw x13, 394(x8)
addr0x00000400: addi x11, x0, -1490
addr0x00000404: jal x20, addr0x000003e0
addr0x00000408: sw x20, 1711(x8)
addr0x0000040c: sw x5, -2014(x8)
addr0x00000410: lw x6, 1967(x8)
addr0x00000414: lw x13, -70(x8)
addr0x00000418: addi x27, x19, -1571
addr0x0000041c: sw x31, 1185(x8)
addr0x00000420: auipc x19, 779505
addr0x00000424: addi x17, x27, 692
addr0x00000428: sw x15, 894(x8)
addr0x0000042c: lui x26, 501120
addr0x00000430: lw x5, 1112(x8)
addr0x00000434: lw x9, 1345(x8)
addr0x00000438: lw x18, -175(x8)
addr0x0000043c: lw x28, -406(x8)
addr0x00000440: lw x24, 585(x8)
addr0x00000444: jal x20, addr0x00000578
addr0x00000448: sw x2, -1573(x8)
addr0x0000044c: blt x7, x20, addr0x00000234
addr0x00000450: lw x7, -651(x8)
addr0x00000454: addi x1, x21, 641
addr0x00000458: lw x19, -1263(x8)
addr0x0000045c: lw x5, -1898(x8)
addr0x00000460: addi x23, x9, -606
addr0x00000464: jal x31, addr0x00000260
addr0x00000468: lw x19, -535(x8)
addr0x0000046c: add x19, x31, x16
addr0x00000470: lbu x22, 615(x8)
addr0x00000474: beq x30, x30, addr0x000002cc
addr0x00000478: jal x5, addr0x0000003c
addr0x0000047c: lw x1, -74(x8)
addr0x00000480: addi x7, x14, 1091
addr0x00000484: sw x26, -1895(x8)
addr0x00000488: sw x0, -334(x8)
addr0x0000048c: sw x9, 874(x8)
addr0x00000490: lw x20, 773(x8)
addr0x00000494: lw x7, 1657(x8)
addr0x00000498: lw x28, -1439(x8)
addr0x0000049c: lw x27, -1961(x8)
addr0x000004a0: addi x13, x5, 1475
addr0x000004a4: lui x1, 374045
addr0x000004a8: lui x13, 997451
addr0x000004ac: addi x23, x19, -896
addr0x000004b0: jal x30, addr0x0000048c
addr0x000004b4: sw x23, 417(x8)
addr0x000004b8: sw x6, 96(x8)
addr0x000004bc: sw x14, -1119(x8)
addr0x000004c0: sw x29, -1873(x8)
addr0x000004c4: sw x2, 1551(x8)
addr0x000004c8: jal x7, addr0x00000430
addr0x000004cc: lw x15, 252(x8)
addr0x000004d0: andi x30, x9, -1956
addr0x000004d4: lw x9, 1055(x8)
addr0x000004d8: lw x21, 282(x8)
addr0x000004dc: lw x20, 1538(x8)
addr0x000004e0: addi x27, x2, 200
addr0x000004e4: bgeu x23, x29, addr0x000005ac
addr0x000004e8: add x30, x2, x12
addr0x000004ec: sw x27, 1544(x8)
addr0x000004f0: sw x19, -1209(x8)
addr0x000004f4: addi x28, x7, 24
addr0x000004f8: lw x7, -805(x8)
addr0x000004fc: lw x30, 1072(x8)
addr0x00000500: lw x11, 797(x8)
addr0x00000504: lui x20, 3262
addr0x00000508: addi x22, x25, 81
addr0x0000050c: bne x0, x27, addr0x000002cc
addr0x00000510: lw x13, 1396(x8)
addr0x00000514: addi x13, x25, 371
addr0x00000518: sw x25, -1094(x8)
addr0x0000051c: addi x17, x15, 51
addr0x00000520: lw x17, -1710(x8)
addr0x00000524: sw x22, 858(x8)
addr0x00000528: addi x17, x30, -2048
addr0x0000052c: jal x24, addr0x000002f8
addr0x00000530: jal x19, addr0x00000104
addr0x00000534: sw x13, -1206(x8)
addr0x00000538: addi x29, x14, 126
addr0x0000053c: addi x31, x18, 1195
addr0x00000540: sw x0, -1626(x8)
addr0x00000544: sw x27, -997(x8)
addr0x00000548: jal x19, addr0x000002cc
addr0x0000054c: addi x4, x0, 374
addr0x00000550: jal x0, addr0x00000308
addr0x00000554: lw x20, -1044(x8)
addr0x00000558: lw x23, 766(x8)
addr0x0000055c: jal x7, addr0x00000114
addr0x00000560: sw x13, -3(x8)
addr0x00000564: sb x18, -90(x8)
addr0x00000568: sb x24, -1386(x8)
addr0x0000056c: bne x30, x25, addr0x00000404
addr0x00000570: sb x18, 1885(x8)
addr0x00000574: lw x30, 356(x8)
addr0x00000578: lw x2, -1410(x8)
addr0x0000057c: addi x4, x7, -590
addr0x00000580: sb x5, -389(x8)
addr0x00000584: addi x5, x29, -1242
addr0x00000588: addi x29, x25, 1523
addr0x0000058c: addi x9, x23, -2034
addr0x00000590: sw x22, -855(x8)
addr0x00000594: sw x16, -1181(x8)
addr0x00000598: lw x4, 776(x8)
addr0x0000059c: lw x29, -1733(x8)
addr0x000005a0: lw x0, 1918(x8)
addr0x000005a4: sw x12, -113(x8)
addr0x000005a8: sw x10, 740(x8)
addr0x000005ac: lw x23, 1455(x8)
addr0x000005b0: lbu x22, -133(x8)
addr0x000005b4: lbu x19, 1021(x8)
addr0x000005b8: bne x7, x13, addr0x00000448
addr0x000005bc: lw x28, -1400(x8)
