.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: slli x18, x5, 9
addr0x0000001c: sll x26, x29, x0
addr0x00000020: or x15, x5, x24
addr0x00000024: add x13, x4, x19
addr0x00000028: add x13, x19, x27
addr0x0000002c: lw x27, 1474(x8)
addr0x00000030: lw x27, 1028(x8)
addr0x00000034: addi x22, x9, -496
addr0x00000038: beq x30, x21, addr0x00000144
addr0x0000003c: addi x22, x19, -1365
addr0x00000040: addi x17, x13, 1846
addr0x00000044: lw x15, 1226(x8)
addr0x00000048: lw x0, 1287(x8)
addr0x0000004c: addi x17, x31, 253
addr0x00000050: sw x7, -1958(x8)
addr0x00000054: jal x29, addr0x00000028
addr0x00000058: lw x16, 1785(x8)
addr0x0000005c: addi x19, x17, 116
addr0x00000060: sw x2, -157(x8)
addr0x00000064: sw x19, -1568(x8)
addr0x00000068: lw x7, -1758(x8)
addr0x0000006c: add x23, x31, x10
addr0x00000070: sw x22, -1059(x8)
addr0x00000074: jal x17, addr0x00000210
addr0x00000078: lw x12, -1603(x8)
addr0x0000007c: addi x22, x7, 185
addr0x00000080: sw x16, -25(x8)
addr0x00000084: jal x7, addr0x00000190
addr0x00000088: beq x25, x6, addr0x00000190
addr0x0000008c: bne x27, x27, addr0x00000144
addr0x00000090: lw x27, -601(x8)
addr0x00000094: add x20, x1, x10
addr0x00000098: andi x25, x11, 163
addr0x0000009c: andi x31, x23, 1951
addr0x000000a0: addi x25, x29, 1468
addr0x000000a4: sw x7, -347(x8)
addr0x000000a8: jal x31, addr0x000001e0
addr0x000000ac: addi x22, x23, -882
addr0x000000b0: lbu x27, -544(x8)
addr0x000000b4: addi x4, x13, -890
addr0x000000b8: jal x25, addr0x00000178
addr0x000000bc: lw x24, 1675(x8)
addr0x000000c0: lw x7, 1937(x8)
addr0x000000c4: bltu x10, x24, addr0x000000bc
addr0x000000c8: slli x19, x28, 16
addr0x000000cc: addi x27, x10, -1742
addr0x000000d0: bgeu x6, x17, addr0x00000164
addr0x000000d4: lw x10, 746(x8)
addr0x000000d8: lw x23, 822(x8)
addr0x000000dc: lw x20, -375(x8)
addr0x000000e0: lw x10, 447(x8)
addr0x000000e4: sw x19, -403(x8)
addr0x000000e8: sw x2, -417(x8)
addr0x000000ec: lw x11, 1395(x8)
addr0x000000f0: bgeu x10, x13, addr0x00000014
addr0x000000f4: add x17, x6, x4
addr0x000000f8: beq x13, x31, addr0x000000e8
addr0x000000fc: lbu x13, 646(x8)
addr0x00000100: lw x28, -1355(x8)
addr0x00000104: addi x14, x14, 947
addr0x00000108: lhu x0, -86(x8)
addr0x0000010c: addi x9, x20, -882
addr0x00000110: addi x5, x5, 983
addr0x00000114: sw x30, -247(x8)
addr0x00000118: jal x26, addr0x000000d4
addr0x0000011c: beq x5, x0, addr0x00000028
addr0x00000120: lw x22, -891(x8)
addr0x00000124: sub x22, x23, x9
addr0x00000128: lw x6, -1116(x8)
addr0x0000012c: addi x20, x22, 132
addr0x00000130: jal x20, addr0x000001b4
addr0x00000134: jal x29, addr0x00000004
addr0x00000138: sw x10, 1290(x8)
addr0x0000013c: andi x20, x22, -863
addr0x00000140: sw x25, 1172(x8)
addr0x00000144: sw x2, -1286(x8)
addr0x00000148: sw x29, -1280(x8)
addr0x0000014c: sw x22, -983(x8)
addr0x00000150: jal x9, addr0x000000ec
addr0x00000154: lui x16, 1030096
addr0x00000158: addi x17, x29, -1314
addr0x0000015c: jal x17, addr0x00000178
addr0x00000160: lw x10, 1284(x8)
addr0x00000164: addi x13, x27, 1951
addr0x00000168: jal x18, addr0x00000130
addr0x0000016c: lw x23, 676(x8)
addr0x00000170: lw x28, 79(x8)
addr0x00000174: lw x14, -2000(x8)
addr0x00000178: jal x19, addr0x0000007c
addr0x0000017c: sw x17, 1755(x8)
addr0x00000180: jal x0, addr0x00000074
addr0x00000184: addi x4, x24, -1474
addr0x00000188: sw x20, 1034(x8)
addr0x0000018c: jal x20, addr0x00000030
addr0x00000190: lw x21, -307(x8)
addr0x00000194: addi x16, x11, 1364
addr0x00000198: bne x23, x9, addr0x00000108
addr0x0000019c: beq x4, x14, addr0x00000054
addr0x000001a0: jal x29, addr0x000001ec
addr0x000001a4: lw x14, 1368(x8)
addr0x000001a8: bgeu x22, x19, addr0x00000074
addr0x000001ac: addi x13, x26, -1048
addr0x000001b0: lw x14, 449(x8)
addr0x000001b4: jal x24, addr0x00000040
addr0x000001b8: addi x19, x29, -1122
addr0x000001bc: sw x7, -711(x8)
addr0x000001c0: jal x14, addr0x0000000c
addr0x000001c4: jal x28, addr0x00000128
addr0x000001c8: lb x15, 1862(x8)
addr0x000001cc: slli x9, x21, 8
addr0x000001d0: jal x5, addr0x00000030
addr0x000001d4: lw x30, 1991(x8)
addr0x000001d8: sw x23, -190(x8)
addr0x000001dc: jal x19, addr0x00000214
addr0x000001e0: lw x14, -1249(x8)
addr0x000001e4: lw x29, -1741(x8)
addr0x000001e8: lw x26, -860(x8)
addr0x000001ec: addi x0, x27, -611
addr0x000001f0: sw x4, -1202(x8)
addr0x000001f4: lw x25, -432(x8)
addr0x000001f8: lw x31, 635(x8)
addr0x000001fc: lw x29, -2023(x8)
addr0x00000200: lw x17, -1966(x8)
addr0x00000204: lw x7, -613(x8)
addr0x00000208: lw x24, 1876(x8)
addr0x0000020c: lw x12, 1218(x8)
addr0x00000210: lui x20, 779166
addr0x00000214: addi x25, x18, -634
addr0x00000218: add x25, x28, x18
addr0x0000021c: addi x19, x19, 1281
addr0x00000220: jal x31, addr0x0000007c
addr0x00000224: addi x7, x24, 723
addr0x00000228: jal x27, addr0x000001b8
addr0x0000022c: sw x22, -580(x8)
addr0x00000230: addi x22, x14, -1985
addr0x00000234: lui x25, 435061
addr0x00000238: lui x14, 131210
addr0x0000023c: blt x7, x31, addr0x000001a8
