.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x17, 1812(x8)
addr0x0000001c: jal x27, addr0x00000470
addr0x00000020: lui x7, 977329
addr0x00000024: slli x17, x19, 4
addr0x00000028: lw x6, 1159(x8)
addr0x0000002c: sw x23, 1850(x8)
addr0x00000030: andi x1, x24, -923
addr0x00000034: slli x9, x23, 24
addr0x00000038: jal x20, addr0x0000030c
addr0x0000003c: lw x24, -1398(x8)
addr0x00000040: sw x17, -1772(x8)
addr0x00000044: sw x23, 1845(x8)
addr0x00000048: bgeu x21, x29, addr0x000004dc
addr0x0000004c: jal x19, addr0x00000230
addr0x00000050: jal x25, addr0x000004d4
addr0x00000054: addi x4, x5, 1537
addr0x00000058: lw x2, 239(x8)
addr0x0000005c: lw x22, -1341(x8)
addr0x00000060: lw x14, -1876(x8)
addr0x00000064: lw x18, 2044(x8)
addr0x00000068: lw x0, -1342(x8)
addr0x0000006c: lw x23, 219(x8)
addr0x00000070: jal x31, addr0x00000010
addr0x00000074: lw x23, -73(x8)
addr0x00000078: lw x22, 1423(x8)
addr0x0000007c: lw x5, -1286(x8)
addr0x00000080: slli x2, x25, 14
addr0x00000084: srli x7, x29, 25
addr0x00000088: jal x19, addr0x0000044c
addr0x0000008c: add x2, x6, x18
addr0x00000090: jal x18, addr0x00000350
addr0x00000094: jal x19, addr0x000001d0
addr0x00000098: addi x19, x14, 1487
addr0x0000009c: sw x17, 1086(x8)
addr0x000000a0: lw x18, -1197(x8)
addr0x000000a4: lw x13, -237(x8)
addr0x000000a8: lw x27, -775(x8)
addr0x000000ac: lw x29, -1463(x8)
addr0x000000b0: addi x6, x19, 325
addr0x000000b4: lw x27, 292(x8)
addr0x000000b8: beq x22, x7, addr0x00000450
addr0x000000bc: bge x19, x18, addr0x000004ac
addr0x000000c0: addi x19, x0, -1701
addr0x000000c4: addi x19, x25, -665
addr0x000000c8: sw x23, 724(x8)
addr0x000000cc: sw x20, 1984(x8)
addr0x000000d0: sw x12, -704(x8)
addr0x000000d4: sw x19, -934(x8)
addr0x000000d8: sw x7, 1263(x8)
addr0x000000dc: jal x19, addr0x00000310
addr0x000000e0: jal x23, addr0x000004bc
addr0x000000e4: lbu x5, -1596(x8)
addr0x000000e8: addi x7, x17, -1911
addr0x000000ec: bne x11, x9, addr0x00000464
addr0x000000f0: addi x21, x29, 448
addr0x000000f4: sw x21, -1552(x8)
addr0x000000f8: sw x12, -912(x8)
addr0x000000fc: addi x25, x17, 1010
addr0x00000100: jal x1, addr0x000001cc
addr0x00000104: lw x7, -554(x8)
addr0x00000108: addi x30, x24, -786
addr0x0000010c: jal x13, addr0x0000028c
addr0x00000110: lw x12, -573(x8)
addr0x00000114: addi x17, x14, 280
addr0x00000118: sw x7, -1918(x8)
addr0x0000011c: sw x26, -1951(x8)
addr0x00000120: addi x13, x9, -1544
addr0x00000124: add x25, x0, x4
addr0x00000128: jal x27, addr0x00000364
addr0x0000012c: lw x14, 1740(x8)
addr0x00000130: addi x27, x29, -1253
addr0x00000134: sb x18, 1513(x8)
addr0x00000138: sb x31, 1248(x8)
addr0x0000013c: lw x20, 576(x8)
addr0x00000140: lw x27, 508(x8)
addr0x00000144: bgeu x22, x18, addr0x000004a4
addr0x00000148: slli x7, x6, 26
addr0x0000014c: srli x13, x9, 16
addr0x00000150: sh x23, -1817(x8)
addr0x00000154: lw x22, 391(x8)
addr0x00000158: srli x9, x6, 5
addr0x0000015c: jal x0, addr0x00000418
addr0x00000160: addi x28, x1, 1561
addr0x00000164: lw x0, -1677(x8)
addr0x00000168: sw x17, -1585(x8)
addr0x0000016c: lui x25, 706966
addr0x00000170: sw x30, -1350(x8)
addr0x00000174: lw x23, 1397(x8)
addr0x00000178: lw x20, 661(x8)
addr0x0000017c: sw x5, 1054(x8)
addr0x00000180: addi x4, x19, -162
addr0x00000184: slli x17, x5, 26
addr0x00000188: or x11, x22, x30
addr0x0000018c: bne x6, x15, addr0x000004b8
addr0x00000190: jal x22, addr0x000001c0
addr0x00000194: lw x0, -1969(x8)
addr0x00000198: lbu x0, -381(x8)
addr0x0000019c: beq x23, x29, addr0x00000444
addr0x000001a0: jal x13, addr0x00000350
addr0x000001a4: addi x27, x29, -81
addr0x000001a8: jal x31, addr0x000003cc
addr0x000001ac: jal x30, addr0x00000084
addr0x000001b0: jal x17, addr0x00000028
addr0x000001b4: lui x19, 92826
addr0x000001b8: addi x30, x19, -1138
addr0x000001bc: addi x0, x19, -1559
addr0x000001c0: lw x17, -1202(x8)
addr0x000001c4: jal x4, addr0x00000088
addr0x000001c8: addi x0, x17, -1422
addr0x000001cc: jal x31, addr0x00000100
addr0x000001d0: lui x11, 713142
addr0x000001d4: addi x17, x17, 828
addr0x000001d8: auipc x27, 611303
addr0x000001dc: addi x17, x27, 283
addr0x000001e0: sw x0, -374(x8)
addr0x000001e4: sw x18, 1711(x8)
addr0x000001e8: sw x19, -14(x8)
addr0x000001ec: jal x0, addr0x000000dc
addr0x000001f0: lw x27, 602(x8)
addr0x000001f4: lw x22, -1783(x8)
addr0x000001f8: lw x23, 1995(x8)
addr0x000001fc: addi x23, x29, 1118
addr0x00000200: jal x1, addr0x000000e8
addr0x00000204: lw x11, -338(x8)
addr0x00000208: sw x28, -1456(x8)
addr0x0000020c: sw x0, -189(x8)
addr0x00000210: lw x12, 1760(x8)
addr0x00000214: addi x17, x18, 993
addr0x00000218: beq x7, x24, addr0x00000458
addr0x0000021c: lui x28, 775208
addr0x00000220: bge x7, x22, addr0x000002a4
addr0x00000224: bne x9, x13, addr0x00000210
addr0x00000228: jal x19, addr0x0000034c
addr0x0000022c: lw x20, -1716(x8)
addr0x00000230: lw x0, 203(x8)
addr0x00000234: lw x20, 1773(x8)
addr0x00000238: lw x7, 899(x8)
addr0x0000023c: lw x10, 1558(x8)
addr0x00000240: lw x18, 1787(x8)
addr0x00000244: jal x19, addr0x00000310
addr0x00000248: addi x0, x2, -1161
addr0x0000024c: sw x28, -1456(x8)
addr0x00000250: sw x2, -1737(x8)
addr0x00000254: lw x30, 455(x8)
addr0x00000258: jal x17, addr0x00000428
addr0x0000025c: lw x2, -766(x8)
addr0x00000260: addi x11, x22, -238
addr0x00000264: jal x25, addr0x0000024c
addr0x00000268: sw x29, 619(x8)
addr0x0000026c: sw x18, 878(x8)
addr0x00000270: lui x16, 1031794
addr0x00000274: addi x15, x25, 879
addr0x00000278: add x18, x21, x21
addr0x0000027c: lui x22, 355516
addr0x00000280: addi x27, x17, -777
addr0x00000284: lw x12, 1070(x8)
addr0x00000288: sw x14, -1475(x8)
addr0x0000028c: addi x18, x31, -948
addr0x00000290: addi x11, x14, -834
addr0x00000294: addi x0, x20, -787
addr0x00000298: and x30, x24, x9
addr0x0000029c: addi x7, x24, -942
addr0x000002a0: sub x4, x26, x22
addr0x000002a4: sub x18, x18, x20
addr0x000002a8: slli x30, x9, 3
addr0x000002ac: add x22, x26, x23
addr0x000002b0: jal x13, addr0x00000380
addr0x000002b4: sw x30, 454(x8)
addr0x000002b8: sb x23, 1419(x8)
addr0x000002bc: lbu x10, 1339(x8)
addr0x000002c0: lw x0, -1349(x8)
addr0x000002c4: lw x13, 132(x8)
addr0x000002c8: jal x4, addr0x00000468
addr0x000002cc: lui x21, 21883
addr0x000002d0: addi x29, x30, -630
addr0x000002d4: addi x4, x14, 439
addr0x000002d8: sw x5, -1117(x8)
addr0x000002dc: jal x9, addr0x00000358
addr0x000002e0: jal x0, addr0x000001f4
addr0x000002e4: addi x31, x2, 8
addr0x000002e8: sw x27, 1229(x8)
addr0x000002ec: jal x5, addr0x00000158
addr0x000002f0: lui x21, 219829
addr0x000002f4: add x11, x5, x9
addr0x000002f8: bge x23, x25, addr0x000000d8
addr0x000002fc: jal x0, addr0x000004ac
addr0x00000300: lw x22, 1630(x8)
addr0x00000304: lw x27, -483(x8)
addr0x00000308: beq x18, x2, addr0x0000025c
addr0x0000030c: jal x1, addr0x00000398
addr0x00000310: jal x9, addr0x000002d8
addr0x00000314: lw x29, -1340(x8)
addr0x00000318: addi x24, x30, -1645
addr0x0000031c: lw x13, -1426(x8)
addr0x00000320: lw x25, -426(x8)
addr0x00000324: lw x23, -935(x8)
addr0x00000328: addi x13, x19, 920
addr0x0000032c: beq x13, x19, addr0x0000000c
addr0x00000330: addi x29, x1, -1854
addr0x00000334: jal x9, addr0x00000260
addr0x00000338: lw x25, 1976(x8)
addr0x0000033c: addi x29, x11, 1603
addr0x00000340: addi x11, x14, 904
addr0x00000344: addi x0, x21, 1626
addr0x00000348: sb x1, 912(x8)
addr0x0000034c: sub x4, x9, x25
addr0x00000350: sw x6, -1089(x8)
addr0x00000354: sw x28, 555(x8)
addr0x00000358: addi x19, x30, 1103
addr0x0000035c: lui x0, 825063
addr0x00000360: sw x23, 143(x8)
addr0x00000364: addi x9, x22, 689
addr0x00000368: jal x29, addr0x00000310
addr0x0000036c: jal x21, addr0x00000314
addr0x00000370: sw x29, 1582(x8)
addr0x00000374: sw x30, -137(x8)
addr0x00000378: jal x26, addr0x0000021c
addr0x0000037c: lw x14, -2047(x8)
addr0x00000380: addi x1, x30, -504
addr0x00000384: lui x6, 590046
addr0x00000388: addi x7, x18, -1083
addr0x0000038c: sw x2, -785(x8)
addr0x00000390: sw x23, 714(x8)
addr0x00000394: sw x5, 1177(x8)
addr0x00000398: sw x2, -1155(x8)
addr0x0000039c: sw x25, -654(x8)
addr0x000003a0: sw x21, -1180(x8)
addr0x000003a4: jal x29, addr0x000003d8
addr0x000003a8: addi x21, x14, 1920
addr0x000003ac: sw x25, 1430(x8)
addr0x000003b0: sw x21, -1679(x8)
addr0x000003b4: jal x22, addr0x00000424
addr0x000003b8: jal x7, addr0x00000288
addr0x000003bc: lw x23, 695(x8)
addr0x000003c0: jal x20, addr0x00000258
addr0x000003c4: addi x7, x18, -1580
addr0x000003c8: sh x2, -1875(x8)
addr0x000003cc: sw x0, 1827(x8)
addr0x000003d0: sw x18, -568(x8)
addr0x000003d4: sw x9, 1637(x8)
addr0x000003d8: lw x28, 1365(x8)
addr0x000003dc: sw x19, 1521(x8)
addr0x000003e0: sw x29, 1619(x8)
addr0x000003e4: sw x1, 1968(x8)
addr0x000003e8: bne x28, x24, addr0x000000cc
addr0x000003ec: lw x29, -92(x8)
addr0x000003f0: beq x30, x6, addr0x0000022c
addr0x000003f4: lui x7, 933409
addr0x000003f8: addi x20, x18, 2045
addr0x000003fc: addi x19, x7, 64
addr0x00000400: jal x13, addr0x000002a0
addr0x00000404: jal x4, addr0x00000308
addr0x00000408: lw x13, -1530(x8)
addr0x0000040c: addi x0, x14, 1451
addr0x00000410: jal x7, addr0x0000037c
addr0x00000414: lw x19, -275(x8)
addr0x00000418: lw x22, -280(x8)
addr0x0000041c: lw x29, -984(x8)
addr0x00000420: sw x30, 415(x8)
addr0x00000424: sw x2, 986(x8)
addr0x00000428: lw x30, -324(x8)
addr0x0000042c: bne x4, x25, addr0x00000150
addr0x00000430: sw x31, -988(x8)
addr0x00000434: lw x12, -1723(x8)
addr0x00000438: jal x23, addr0x00000324
addr0x0000043c: addi x27, x29, -662
addr0x00000440: slli x30, x13, 6
addr0x00000444: lw x6, 2002(x8)
addr0x00000448: jal x22, addr0x000002f8
addr0x0000044c: bltu x13, x4, addr0x00000128
addr0x00000450: sub x9, x1, x31
addr0x00000454: sw x30, -1934(x8)
addr0x00000458: lui x0, 782093
addr0x0000045c: addi x20, x27, -223
addr0x00000460: jal x22, addr0x00000100
addr0x00000464: sw x22, 1486(x8)
addr0x00000468: sw x17, 247(x8)
addr0x0000046c: sw x2, 166(x8)
addr0x00000470: jal x10, addr0x000000a4
addr0x00000474: addi x9, x13, -451
addr0x00000478: bne x30, x22, addr0x000003a8
addr0x0000047c: addi x27, x30, 248
addr0x00000480: jal x9, addr0x00000318
addr0x00000484: sw x19, -661(x8)
addr0x00000488: lw x21, -1322(x8)
addr0x0000048c: lw x6, 1497(x8)
addr0x00000490: jal x2, addr0x000000ec
addr0x00000494: sw x27, 430(x8)
addr0x00000498: sw x7, 660(x8)
addr0x0000049c: sub x24, x24, x18
addr0x000004a0: beq x5, x27, addr0x00000478
addr0x000004a4: addi x29, x6, 1813
addr0x000004a8: sw x2, -968(x8)
addr0x000004ac: addi x14, x11, -741
addr0x000004b0: lw x20, 1938(x8)
addr0x000004b4: sll x28, x7, x9
addr0x000004b8: or x29, x31, x28
addr0x000004bc: sw x23, 206(x8)
addr0x000004c0: lw x31, -1278(x8)
addr0x000004c4: add x13, x22, x0
addr0x000004c8: bne x31, x10, addr0x00000060
addr0x000004cc: lw x23, 493(x8)
addr0x000004d0: sw x13, 71(x8)
addr0x000004d4: lw x5, -426(x8)
addr0x000004d8: lw x9, 1464(x8)
addr0x000004dc: lw x23, 101(x8)
addr0x000004e0: lw x27, 1283(x8)
