.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: addi x19, x30, -811
addr0x0000001c: jal x0, addr0x00000004
addr0x00000020: lbu x29, -1397(x8)
addr0x00000024: bne x13, x29, addr0x00000134
addr0x00000028: addi x14, x13, -1742
addr0x0000002c: lw x30, 640(x8)
addr0x00000030: addi x9, x23, -1955
addr0x00000034: sw x10, -681(x8)
addr0x00000038: sw x24, -762(x8)
addr0x0000003c: lw x16, 1349(x8)
addr0x00000040: bge x7, x28, addr0x00000070
addr0x00000044: andi x14, x29, -1847
addr0x00000048: sw x28, -940(x8)
addr0x0000004c: jal x0, addr0x00000148
addr0x00000050: lw x9, 2047(x8)
addr0x00000054: addi x31, x10, 1609
addr0x00000058: lui x27, 654162
addr0x0000005c: lw x14, 859(x8)
addr0x00000060: addi x26, x0, -655
addr0x00000064: addi x7, x27, 1755
addr0x00000068: slli x22, x29, 8
addr0x0000006c: ori x19, x23, 145
addr0x00000070: sh x27, -107(x8)
addr0x00000074: lbu x9, 1398(x8)
addr0x00000078: lw x19, -1939(x8)
addr0x0000007c: addi x20, x0, 1745
addr0x00000080: addi x9, x9, 1653
addr0x00000084: addi x20, x27, 1999
addr0x00000088: lw x18, 609(x8)
addr0x0000008c: addi x31, x29, 891
addr0x00000090: beq x1, x1, addr0x000000f4
addr0x00000094: jal x23, addr0x00000010
addr0x00000098: addi x15, x25, -1401
addr0x0000009c: jal x20, addr0x000000b8
addr0x000000a0: sub x10, x18, x9
addr0x000000a4: bgeu x29, x5, addr0x00000018
addr0x000000a8: beq x30, x4, addr0x00000134
addr0x000000ac: slli x28, x23, 13
addr0x000000b0: lw x9, -635(x8)
addr0x000000b4: addi x0, x14, -814
addr0x000000b8: ori x7, x20, -1095
addr0x000000bc: andi x1, x18, -114
addr0x000000c0: lbu x22, 1266(x8)
addr0x000000c4: sub x15, x23, x17
addr0x000000c8: bltu x29, x22, addr0x000000a0
addr0x000000cc: bge x13, x29, addr0x00000074
addr0x000000d0: addi x19, x18, -1164
addr0x000000d4: sw x26, -1096(x8)
addr0x000000d8: jal x28, addr0x000000a0
addr0x000000dc: lui x18, 176247
addr0x000000e0: sw x21, 566(x8)
addr0x000000e4: sw x6, -466(x8)
addr0x000000e8: sw x20, 882(x8)
addr0x000000ec: addi x26, x31, 1501
addr0x000000f0: sw x24, 522(x8)
addr0x000000f4: jal x5, addr0x00000040
addr0x000000f8: lw x7, 105(x8)
addr0x000000fc: lw x30, 387(x8)
addr0x00000100: addi x28, x16, 1574
addr0x00000104: sh x13, 1709(x8)
addr0x00000108: addi x0, x28, 146
addr0x0000010c: bge x4, x27, addr0x00000140
addr0x00000110: lw x18, -199(x8)
addr0x00000114: lw x18, -71(x8)
addr0x00000118: lw x19, -973(x8)
addr0x0000011c: lw x20, 1869(x8)
addr0x00000120: sw x15, -1549(x8)
addr0x00000124: sw x23, 1518(x8)
addr0x00000128: jal x20, addr0x0000001c
addr0x0000012c: lui x22, 365285
addr0x00000130: addi x15, x11, -1880
addr0x00000134: jal x30, addr0x0000016c
addr0x00000138: sb x13, -142(x8)
addr0x0000013c: bge x20, x14, addr0x00000104
addr0x00000140: addi x21, x25, 2042
addr0x00000144: jal x15, addr0x00000138
addr0x00000148: sw x0, -1929(x8)
addr0x0000014c: sb x10, -730(x8)
addr0x00000150: lbu x29, -1795(x8)
addr0x00000154: bgeu x23, x17, addr0x00000154
addr0x00000158: lw x21, -968(x8)
addr0x0000015c: jal x20, addr0x00000048
addr0x00000160: lui x20, 17341
addr0x00000164: addi x19, x17, 1217
addr0x00000168: addi x21, x2, 921
addr0x0000016c: add x13, x30, x18
