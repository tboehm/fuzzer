.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lhu x24, -1262(x8)
addr0x0000001c: lhu x21, -1948(x8)
addr0x00000020: ori x27, x14, 883
addr0x00000024: sw x25, 1650(x8)
addr0x00000028: lw x7, -258(x8)
addr0x0000002c: sw x5, -1877(x8)
addr0x00000030: addi x19, x1, -1754
addr0x00000034: lw x10, -1265(x8)
addr0x00000038: lw x15, 70(x8)
addr0x0000003c: addi x25, x0, -320
addr0x00000040: jal x17, addr0x00000bb8
addr0x00000044: bltu x19, x0, addr0x00000494
addr0x00000048: jal x17, addr0x00000088
addr0x0000004c: addi x5, x24, -1945
addr0x00000050: jal x19, addr0x000002e8
addr0x00000054: lui x20, 479898
addr0x00000058: lw x20, 1255(x8)
addr0x0000005c: addi x5, x29, -368
addr0x00000060: addi x17, x13, 2011
addr0x00000064: sw x0, 159(x8)
addr0x00000068: lw x10, 118(x8)
addr0x0000006c: add x2, x14, x19
addr0x00000070: jal x19, addr0x0000045c
addr0x00000074: jal x2, addr0x0000047c
addr0x00000078: jal x7, addr0x00000b3c
addr0x0000007c: lw x1, -924(x8)
addr0x00000080: addi x13, x19, -497
addr0x00000084: jal x29, addr0x00000074
addr0x00000088: jal x17, addr0x000003f8
addr0x0000008c: bne x19, x1, addr0x00000290
addr0x00000090: or x0, x19, x0
addr0x00000094: lui x4, 668234
addr0x00000098: lui x21, 500699
addr0x0000009c: addi x23, x9, -1988
addr0x000000a0: sw x20, 347(x8)
addr0x000000a4: lw x19, 1558(x8)
addr0x000000a8: addi x7, x28, 487
addr0x000000ac: jal x13, addr0x0000013c
addr0x000000b0: lui x0, 1042527
addr0x000000b4: addi x7, x19, -808
addr0x000000b8: jal x17, addr0x000007a8
addr0x000000bc: bne x23, x28, addr0x0000053c
addr0x000000c0: addi x7, x5, -1135
addr0x000000c4: lui x23, 160998
addr0x000000c8: addi x29, x27, 1748
addr0x000000cc: bgeu x17, x2, addr0x000001e0
addr0x000000d0: lbu x15, 1571(x8)
addr0x000000d4: lbu x15, 425(x8)
addr0x000000d8: sw x5, -1837(x8)
addr0x000000dc: lw x27, 1923(x8)
addr0x000000e0: add x22, x20, x24
addr0x000000e4: sw x22, 1833(x8)
addr0x000000e8: jal x7, addr0x000003fc
addr0x000000ec: lui x29, 531906
addr0x000000f0: lw x7, 1333(x8)
addr0x000000f4: addi x1, x24, 87
addr0x000000f8: jal x24, addr0x0000073c
addr0x000000fc: addi x16, x19, 1400
addr0x00000100: jal x30, addr0x00000380
addr0x00000104: lw x0, -1193(x8)
addr0x00000108: lhu x7, -843(x8)
addr0x0000010c: lhu x7, 1455(x8)
addr0x00000110: addi x2, x20, -70
addr0x00000114: sw x0, 451(x8)
addr0x00000118: lw x9, -1563(x8)
addr0x0000011c: sw x5, -339(x8)
addr0x00000120: sw x17, 1843(x8)
addr0x00000124: addi x18, x18, -160
addr0x00000128: jal x10, addr0x00000558
addr0x0000012c: addi x6, x30, -249
addr0x00000130: or x30, x28, x27
addr0x00000134: lui x30, 212988
addr0x00000138: addi x18, x0, -1044
addr0x0000013c: jal x18, addr0x0000033c
addr0x00000140: lw x6, 61(x8)
addr0x00000144: lw x13, 344(x8)
addr0x00000148: addi x29, x23, -1404
addr0x0000014c: addi x10, x27, -2036
addr0x00000150: sw x7, -1010(x8)
addr0x00000154: sh x31, -764(x8)
addr0x00000158: lw x19, 941(x8)
addr0x0000015c: jal x15, addr0x00000608
addr0x00000160: lw x19, 419(x8)
addr0x00000164: lw x6, -489(x8)
addr0x00000168: sw x0, -1144(x8)
addr0x0000016c: addi x4, x13, -92
addr0x00000170: srai x23, x9, 0
addr0x00000174: lw x9, 1968(x8)
addr0x00000178: lw x27, -632(x8)
addr0x0000017c: lw x7, -86(x8)
addr0x00000180: lw x7, 3(x8)
addr0x00000184: addi x12, x4, 1746
addr0x00000188: sw x0, -1287(x8)
addr0x0000018c: jal x24, addr0x000001cc
addr0x00000190: lw x4, 1974(x8)
addr0x00000194: lw x24, 146(x8)
addr0x00000198: lw x1, 1960(x8)
addr0x0000019c: andi x12, x30, 1859
addr0x000001a0: addi x21, x19, 1866
addr0x000001a4: jal x2, addr0x000004c8
addr0x000001a8: addi x14, x6, 2003
addr0x000001ac: sw x18, -1441(x8)
addr0x000001b0: lw x11, -1249(x8)
addr0x000001b4: lw x27, 1080(x8)
addr0x000001b8: bne x12, x10, addr0x000005f0
addr0x000001bc: lui x13, 343342
addr0x000001c0: addi x19, x10, -465
addr0x000001c4: sw x15, 1242(x8)
addr0x000001c8: sw x25, 746(x8)
addr0x000001cc: jal x29, addr0x000001ec
addr0x000001d0: sw x29, -223(x8)
addr0x000001d4: sw x23, 1401(x8)
addr0x000001d8: lw x19, -1672(x8)
addr0x000001dc: lw x18, 1706(x8)
addr0x000001e0: lw x24, -1348(x8)
addr0x000001e4: sw x29, -401(x8)
addr0x000001e8: sw x1, -1847(x8)
addr0x000001ec: srli x24, x29, 6
addr0x000001f0: add x0, x30, x30
addr0x000001f4: lbu x1, -1034(x8)
addr0x000001f8: lw x10, -80(x8)
addr0x000001fc: lw x31, -302(x8)
addr0x00000200: lw x28, 1823(x8)
addr0x00000204: lw x24, -252(x8)
addr0x00000208: addi x5, x21, -1837
addr0x0000020c: sw x18, 1009(x8)
addr0x00000210: sw x29, 1704(x8)
addr0x00000214: jal x17, addr0x0000089c
addr0x00000218: lw x29, 1050(x8)
addr0x0000021c: lw x11, -626(x8)
addr0x00000220: lw x27, -705(x8)
addr0x00000224: srai x19, x19, 14
addr0x00000228: jal x7, addr0x000001d4
addr0x0000022c: jal x22, addr0x000004f8
addr0x00000230: addi x9, x30, 1595
addr0x00000234: lw x31, -465(x8)
addr0x00000238: lw x19, 560(x8)
addr0x0000023c: lui x7, 17459
addr0x00000240: bltu x27, x4, addr0x00000974
addr0x00000244: lbu x22, -646(x8)
addr0x00000248: bne x7, x10, addr0x000005a0
addr0x0000024c: lbu x14, -1870(x8)
addr0x00000250: slli x4, x28, 24
addr0x00000254: add x29, x31, x7
addr0x00000258: lbu x1, -496(x8)
addr0x0000025c: lw x19, 1825(x8)
addr0x00000260: lw x22, -929(x8)
addr0x00000264: add x0, x17, x17
addr0x00000268: jal x17, addr0x000002f0
addr0x0000026c: lui x20, 901636
addr0x00000270: addi x18, x22, 183
addr0x00000274: lw x29, 1396(x8)
addr0x00000278: sh x17, 1938(x8)
addr0x0000027c: sll x22, x31, x23
addr0x00000280: sb x10, -340(x8)
addr0x00000284: addi x13, x27, 1434
addr0x00000288: sw x25, -1011(x8)
addr0x0000028c: jal x9, addr0x000007e4
addr0x00000290: jal x7, addr0x000004dc
addr0x00000294: lui x23, 553529
addr0x00000298: lui x13, 575156
addr0x0000029c: addi x27, x24, -463
addr0x000002a0: addi x19, x13, -1122
addr0x000002a4: addi x7, x13, -1582
addr0x000002a8: jal x23, addr0x00000658
addr0x000002ac: sw x9, 800(x8)
addr0x000002b0: sw x27, 1016(x8)
addr0x000002b4: sb x17, -2010(x8)
addr0x000002b8: sw x13, 914(x8)
addr0x000002bc: sw x2, 1475(x8)
addr0x000002c0: andi x9, x2, -1845
addr0x000002c4: sw x23, 172(x8)
addr0x000002c8: lw x23, 1200(x8)
addr0x000002cc: sw x29, 1958(x8)
addr0x000002d0: sw x18, -717(x8)
addr0x000002d4: jal x18, addr0x000009d8
addr0x000002d8: lw x6, -365(x8)
addr0x000002dc: jal x28, addr0x0000027c
addr0x000002e0: lw x28, 1433(x8)
addr0x000002e4: lw x2, 195(x8)
addr0x000002e8: lw x24, -337(x8)
addr0x000002ec: lw x21, -1160(x8)
addr0x000002f0: addi x19, x4, -1634
addr0x000002f4: sw x0, -414(x8)
addr0x000002f8: lui x31, 871103
addr0x000002fc: sw x27, 856(x8)
addr0x00000300: jal x0, addr0x00000330
addr0x00000304: addi x15, x19, 298
addr0x00000308: jal x7, addr0x0000006c
addr0x0000030c: lw x29, -1001(x8)
addr0x00000310: lui x24, 879105
addr0x00000314: sw x29, -184(x8)
addr0x00000318: jal x24, addr0x00000600
addr0x0000031c: lw x22, -1245(x8)
addr0x00000320: lw x22, -334(x8)
addr0x00000324: lw x23, -924(x8)
addr0x00000328: lbu x29, 61(x8)
addr0x0000032c: jal x10, addr0x0000087c
addr0x00000330: jal x22, addr0x00000a84
addr0x00000334: addi x17, x10, 1108
addr0x00000338: lui x29, 464207
addr0x0000033c: addi x2, x21, 1768
addr0x00000340: beq x15, x18, addr0x000003ac
addr0x00000344: bge x28, x13, addr0x00000590
addr0x00000348: jal x18, addr0x000005e4
addr0x0000034c: sw x4, -1828(x8)
addr0x00000350: sw x10, 114(x8)
addr0x00000354: sw x19, 1726(x8)
addr0x00000358: lw x15, -990(x8)
addr0x0000035c: add x0, x19, x2
addr0x00000360: lw x22, -1240(x8)
addr0x00000364: lw x10, 1093(x8)
addr0x00000368: lw x11, 404(x8)
addr0x0000036c: beq x22, x9, addr0x00000690
addr0x00000370: lbu x22, -1757(x8)
addr0x00000374: lw x23, 260(x8)
addr0x00000378: lw x18, -763(x8)
addr0x0000037c: jal x22, addr0x000009b8
addr0x00000380: jal x23, addr0x0000093c
addr0x00000384: andi x23, x23, 1634
addr0x00000388: sb x26, 1764(x8)
addr0x0000038c: lbu x19, -1488(x8)
addr0x00000390: addi x25, x15, 1828
addr0x00000394: jal x29, addr0x000009ec
addr0x00000398: jal x11, addr0x00000304
addr0x0000039c: jal x14, addr0x000003a4
addr0x000003a0: beq x0, x9, addr0x000009d8
addr0x000003a4: lbu x28, 1794(x8)
addr0x000003a8: bne x1, x10, addr0x00000950
addr0x000003ac: lw x29, -559(x8)
addr0x000003b0: jal x27, addr0x000005c8
addr0x000003b4: jal x7, addr0x00000004
addr0x000003b8: lbu x13, -1260(x8)
addr0x000003bc: jal x10, addr0x00000698
addr0x000003c0: sw x4, -366(x8)
addr0x000003c4: addi x18, x21, 138
addr0x000003c8: lw x24, 914(x8)
addr0x000003cc: bgeu x13, x25, addr0x000008c0
addr0x000003d0: add x10, x0, x28
addr0x000003d4: lbu x22, -575(x8)
addr0x000003d8: sw x19, 943(x8)
addr0x000003dc: sw x25, 686(x8)
addr0x000003e0: lw x15, 2020(x8)
addr0x000003e4: lw x7, 79(x8)
addr0x000003e8: addi x30, x24, -483
addr0x000003ec: lbu x29, 1241(x8)
addr0x000003f0: bne x30, x2, addr0x00000244
addr0x000003f4: lbu x14, -346(x8)
addr0x000003f8: bgeu x22, x30, addr0x000007e4
addr0x000003fc: lw x2, -471(x8)
addr0x00000400: sw x23, -973(x8)
addr0x00000404: sw x18, -140(x8)
addr0x00000408: sw x1, -1944(x8)
addr0x0000040c: sw x4, -553(x8)
addr0x00000410: addi x24, x28, 1029
addr0x00000414: jal x30, addr0x00000448
addr0x00000418: bne x4, x17, addr0x000006a4
addr0x0000041c: bge x1, x0, addr0x00000468
addr0x00000420: addi x0, x0, 1302
addr0x00000424: lw x24, -2015(x8)
addr0x00000428: ori x17, x31, -1389
addr0x0000042c: beq x14, x0, addr0x0000026c
addr0x00000430: lbu x1, 215(x8)
addr0x00000434: addi x27, x0, 794
addr0x00000438: sw x13, -1534(x8)
addr0x0000043c: lw x12, -760(x8)
addr0x00000440: lw x31, 1631(x8)
addr0x00000444: sw x18, -460(x8)
addr0x00000448: sw x14, 131(x8)
addr0x0000044c: sw x14, -618(x8)
addr0x00000450: jal x7, addr0x00000580
addr0x00000454: lw x7, -1272(x8)
addr0x00000458: sw x24, -1590(x8)
addr0x0000045c: lw x17, -1328(x8)
addr0x00000460: lw x19, -1160(x8)
addr0x00000464: sw x10, 1846(x8)
addr0x00000468: srli x21, x10, 19
addr0x0000046c: blt x5, x0, addr0x00000460
addr0x00000470: addi x13, x16, -715
addr0x00000474: addi x29, x23, -1751
addr0x00000478: jal x14, addr0x00000b58
addr0x0000047c: jal x10, addr0x00000aa0
addr0x00000480: lw x10, 1581(x8)
addr0x00000484: lw x2, 483(x8)
addr0x00000488: jal x30, addr0x00000250
addr0x0000048c: sw x16, 638(x8)
addr0x00000490: addi x31, x22, -383
addr0x00000494: addi x28, x11, -1041
addr0x00000498: jal x0, addr0x00000558
addr0x0000049c: lw x15, -1789(x8)
addr0x000004a0: lw x18, 1026(x8)
addr0x000004a4: jal x7, addr0x0000059c
addr0x000004a8: lui x24, 789660
addr0x000004ac: srli x7, x27, 12
addr0x000004b0: sw x18, -1605(x8)
addr0x000004b4: sw x10, 526(x8)
addr0x000004b8: jal x20, addr0x00000a40
addr0x000004bc: jal x29, addr0x00000a98
addr0x000004c0: jal x7, addr0x00000bc4
addr0x000004c4: sw x18, 1888(x8)
addr0x000004c8: sw x17, 1849(x8)
addr0x000004cc: lw x22, -1558(x8)
addr0x000004d0: lw x13, -949(x8)
addr0x000004d4: addi x31, x1, -875
addr0x000004d8: jal x10, addr0x0000074c
addr0x000004dc: lw x29, 1163(x8)
addr0x000004e0: lw x15, -494(x8)
addr0x000004e4: sw x27, 229(x8)
addr0x000004e8: bge x27, x22, addr0x00000668
addr0x000004ec: jal x27, addr0x00000168
addr0x000004f0: lw x2, 1100(x8)
addr0x000004f4: lw x31, 496(x8)
addr0x000004f8: sw x19, -1920(x8)
addr0x000004fc: sw x15, 442(x8)
addr0x00000500: sw x14, 1997(x8)
addr0x00000504: addi x19, x23, 1862
addr0x00000508: srli x29, x17, 2
addr0x0000050c: lhu x30, 534(x8)
addr0x00000510: addi x20, x21, 1083
addr0x00000514: jal x18, addr0x00000bcc
addr0x00000518: lw x22, -1590(x8)
addr0x0000051c: lw x31, 857(x8)
addr0x00000520: addi x17, x22, -1476
addr0x00000524: sw x14, -1841(x8)
addr0x00000528: lw x23, -1320(x8)
addr0x0000052c: bne x14, x31, addr0x000004ac
addr0x00000530: lw x1, -1549(x8)
addr0x00000534: jal x10, addr0x00000948
addr0x00000538: lw x26, 502(x8)
addr0x0000053c: lui x30, 20953
addr0x00000540: addi x25, x5, -1303
addr0x00000544: sw x29, 1120(x8)
addr0x00000548: lw x7, -564(x8)
addr0x0000054c: jal x9, addr0x0000024c
addr0x00000550: addi x19, x30, 1517
addr0x00000554: jal x9, addr0x00000794
addr0x00000558: sw x22, -320(x8)
addr0x0000055c: addi x9, x29, -660
addr0x00000560: jal x24, addr0x000002d4
addr0x00000564: lw x21, -326(x8)
addr0x00000568: andi x17, x19, 509
addr0x0000056c: sw x27, -1224(x8)
addr0x00000570: lw x0, -1559(x8)
addr0x00000574: lw x19, -87(x8)
addr0x00000578: lw x6, -1987(x8)
addr0x0000057c: lbu x1, -940(x8)
addr0x00000580: andi x20, x5, -916
addr0x00000584: bne x25, x18, addr0x000003dc
addr0x00000588: addi x14, x30, 1379
addr0x0000058c: sw x29, -1449(x8)
addr0x00000590: sw x7, 99(x8)
addr0x00000594: sw x14, -227(x8)
addr0x00000598: bne x2, x22, addr0x000009d0
addr0x0000059c: bltu x23, x7, addr0x00000424
addr0x000005a0: lbu x0, 444(x8)
addr0x000005a4: lbu x5, -3(x8)
addr0x000005a8: beq x2, x16, addr0x0000052c
addr0x000005ac: lbu x7, -1988(x8)
addr0x000005b0: andi x1, x20, -160
addr0x000005b4: sw x4, 869(x8)
addr0x000005b8: sw x29, -1731(x8)
addr0x000005bc: sw x13, 839(x8)
addr0x000005c0: auipc x6, 582260
addr0x000005c4: addi x20, x15, -1610
addr0x000005c8: sw x0, -8(x8)
addr0x000005cc: sw x12, -1749(x8)
addr0x000005d0: sw x27, 1442(x8)
addr0x000005d4: jal x1, addr0x000008fc
addr0x000005d8: jal x4, addr0x0000091c
addr0x000005dc: addi x13, x23, -471
addr0x000005e0: lui x18, 895872
addr0x000005e4: addi x1, x10, 1962
addr0x000005e8: jal x7, addr0x0000046c
addr0x000005ec: lw x29, 159(x8)
addr0x000005f0: lw x9, 728(x8)
addr0x000005f4: lw x0, 1345(x8)
addr0x000005f8: lw x19, -1492(x8)
addr0x000005fc: lw x23, 1460(x8)
addr0x00000600: lw x20, -542(x8)
addr0x00000604: lw x31, 1647(x8)
addr0x00000608: lw x20, -1982(x8)
addr0x0000060c: lw x0, -1301(x8)
addr0x00000610: lw x7, -2008(x8)
addr0x00000614: lw x23, -2042(x8)
addr0x00000618: slli x12, x23, 8
addr0x0000061c: srli x30, x21, 2
addr0x00000620: divu x7, x17, x29
addr0x00000624: lbu x2, -356(x8)
addr0x00000628: xori x29, x25, 450
addr0x0000062c: andi x10, x15, 1593
addr0x00000630: add x17, x23, x30
addr0x00000634: jal x7, addr0x00000a94
addr0x00000638: addi x29, x23, 627
addr0x0000063c: sw x28, -204(x8)
addr0x00000640: sw x20, -72(x8)
addr0x00000644: sw x0, -1532(x8)
addr0x00000648: jal x1, addr0x00000694
addr0x0000064c: sw x20, 1482(x8)
addr0x00000650: jal x14, addr0x000008a4
addr0x00000654: lw x22, -616(x8)
addr0x00000658: sw x30, -105(x8)
addr0x0000065c: lbu x10, 564(x8)
addr0x00000660: slli x19, x31, 6
addr0x00000664: addi x17, x13, 652
addr0x00000668: lw x23, 956(x8)
addr0x0000066c: sw x26, -1412(x8)
addr0x00000670: sw x10, -1321(x8)
addr0x00000674: sw x7, -307(x8)
addr0x00000678: lui x19, 52571
addr0x0000067c: addi x0, x7, 372
addr0x00000680: jal x23, addr0x00000614
addr0x00000684: lui x13, 905076
addr0x00000688: addi x31, x6, -1499
addr0x0000068c: sw x28, -1909(x8)
addr0x00000690: lw x9, -416(x8)
addr0x00000694: lw x7, 712(x8)
addr0x00000698: lw x16, -449(x8)
addr0x0000069c: jal x20, addr0x00000354
addr0x000006a0: lui x6, 521801
addr0x000006a4: bltu x27, x19, addr0x00000848
addr0x000006a8: lbu x2, -531(x8)
addr0x000006ac: andi x22, x29, -1205
addr0x000006b0: bne x14, x14, addr0x00000890
addr0x000006b4: sw x9, 865(x8)
addr0x000006b8: sb x16, -417(x8)
addr0x000006bc: add x10, x2, x21
addr0x000006c0: add x14, x27, x2
addr0x000006c4: sh x24, 1075(x8)
addr0x000006c8: sw x22, 542(x8)
addr0x000006cc: jal x18, addr0x00000624
addr0x000006d0: sw x18, 1144(x8)
addr0x000006d4: addi x1, x19, -904
addr0x000006d8: jal x31, addr0x0000078c
addr0x000006dc: lw x17, -1222(x8)
addr0x000006e0: lw x29, 472(x8)
addr0x000006e4: addi x4, x1, -1087
addr0x000006e8: addi x9, x24, 1508
addr0x000006ec: addi x17, x22, 1767
addr0x000006f0: sw x27, -1861(x8)
addr0x000006f4: sw x6, -1347(x8)
addr0x000006f8: jalr x0, 728(x3)
addr0x000006fc: sw x24, 242(x8)
addr0x00000700: addi x22, x23, -281
addr0x00000704: jal x19, addr0x000004d0
addr0x00000708: lw x20, -1262(x8)
addr0x0000070c: sw x9, -863(x8)
addr0x00000710: jal x10, addr0x00000be4
addr0x00000714: addi x18, x16, -226
addr0x00000718: jal x23, addr0x00000070
addr0x0000071c: beq x10, x16, addr0x00000a5c
addr0x00000720: addi x7, x29, -1689
addr0x00000724: lw x19, -266(x8)
addr0x00000728: lw x15, 1270(x8)
addr0x0000072c: lw x14, -1291(x8)
addr0x00000730: addi x16, x13, 1380
addr0x00000734: jal x28, addr0x0000063c
addr0x00000738: lw x19, 1030(x8)
addr0x0000073c: addi x19, x24, -1797
addr0x00000740: sw x4, -525(x8)
addr0x00000744: jal x9, addr0x00000988
addr0x00000748: add x7, x2, x19
addr0x0000074c: mul x24, x0, x23
addr0x00000750: add x30, x13, x31
addr0x00000754: lw x6, 824(x8)
addr0x00000758: lui x24, 651193
addr0x0000075c: addi x25, x25, 628
addr0x00000760: addi x10, x13, 179
addr0x00000764: sw x7, -605(x8)
addr0x00000768: lui x7, 420138
addr0x0000076c: sw x13, -1316(x8)
addr0x00000770: sw x28, 1029(x8)
addr0x00000774: jal x15, addr0x00000a04
addr0x00000778: addi x29, x28, -793
addr0x0000077c: sw x10, -542(x8)
addr0x00000780: sw x23, -620(x8)
addr0x00000784: lw x12, 603(x8)
addr0x00000788: lw x19, 632(x8)
addr0x0000078c: addi x17, x0, 163
addr0x00000790: sw x4, 1090(x8)
addr0x00000794: lw x25, 522(x8)
addr0x00000798: lw x31, 1471(x8)
addr0x0000079c: bgeu x23, x24, addr0x00000c10
addr0x000007a0: addi x5, x23, -122
addr0x000007a4: jal x5, addr0x000004d4
addr0x000007a8: lw x2, 520(x8)
addr0x000007ac: jal x13, addr0x000004cc
addr0x000007b0: sw x19, 164(x8)
addr0x000007b4: sub x29, x31, x17
addr0x000007b8: bgeu x18, x18, addr0x000002f4
addr0x000007bc: add x31, x22, x22
addr0x000007c0: jal x29, addr0x00000c0c
addr0x000007c4: addi x13, x0, -648
addr0x000007c8: sw x5, -1360(x8)
addr0x000007cc: addi x16, x7, 369
addr0x000007d0: sh x0, 1697(x8)
addr0x000007d4: bltu x0, x1, addr0x000008f8
addr0x000007d8: sw x27, -1682(x8)
addr0x000007dc: sw x0, -213(x8)
addr0x000007e0: sw x0, -443(x8)
addr0x000007e4: sw x0, -1309(x8)
addr0x000007e8: lw x13, 248(x8)
addr0x000007ec: lui x1, 78338
addr0x000007f0: addi x30, x0, -618
addr0x000007f4: add x27, x1, x1
addr0x000007f8: addi x14, x0, -1536
addr0x000007fc: jal x17, addr0x000006fc
addr0x00000800: jal x9, addr0x000003d4
addr0x00000804: jal x14, addr0x00000974
addr0x00000808: jal x30, addr0x00000a74
addr0x0000080c: lw x13, -622(x8)
addr0x00000810: sw x19, 1738(x8)
addr0x00000814: sw x14, 782(x8)
addr0x00000818: lw x24, -234(x8)
addr0x0000081c: lw x18, -676(x8)
addr0x00000820: addi x20, x14, 304
addr0x00000824: sw x12, -163(x8)
addr0x00000828: lbu x14, -1508(x8)
addr0x0000082c: jal x17, addr0x00000ad8
addr0x00000830: lui x17, 883214
addr0x00000834: sw x21, 1254(x8)
addr0x00000838: sw x10, 1597(x8)
addr0x0000083c: lw x0, -346(x8)
addr0x00000840: sw x18, 1891(x8)
addr0x00000844: slli x4, x13, 29
addr0x00000848: lw x23, 870(x8)
addr0x0000084c: addi x10, x0, -385
addr0x00000850: sw x19, -49(x8)
addr0x00000854: sw x17, 529(x8)
addr0x00000858: jal x19, addr0x0000051c
addr0x0000085c: sb x0, 994(x8)
addr0x00000860: lw x29, 1828(x8)
addr0x00000864: lw x30, 1721(x8)
addr0x00000868: add x0, x14, x9
addr0x0000086c: beq x7, x29, addr0x0000015c
addr0x00000870: lw x18, 674(x8)
addr0x00000874: jal x5, addr0x00000b5c
addr0x00000878: addi x19, x2, -1724
addr0x0000087c: slli x25, x18, 10
addr0x00000880: addi x25, x31, 87
addr0x00000884: jal x10, addr0x00000b3c
addr0x00000888: sw x25, 490(x8)
addr0x0000088c: sw x18, 1113(x8)
addr0x00000890: jal x17, addr0x00000a68
addr0x00000894: lw x5, 72(x8)
addr0x00000898: lw x29, -1720(x8)
addr0x0000089c: lw x17, -1056(x8)
addr0x000008a0: addi x9, x19, -56
addr0x000008a4: lui x9, 226287
addr0x000008a8: lw x19, -597(x8)
addr0x000008ac: addi x14, x0, -720
addr0x000008b0: sw x14, 1378(x8)
addr0x000008b4: addi x7, x7, -17
addr0x000008b8: lui x29, 1024234
addr0x000008bc: sh x20, 803(x8)
addr0x000008c0: lw x7, 732(x8)
addr0x000008c4: sw x25, 1082(x8)
addr0x000008c8: sw x20, 76(x8)
addr0x000008cc: sw x10, 1818(x8)
addr0x000008d0: sw x13, 438(x8)
addr0x000008d4: addi x20, x18, -175
addr0x000008d8: jal x7, addr0x00000b30
addr0x000008dc: lw x18, -1455(x8)
addr0x000008e0: lw x22, -1043(x8)
addr0x000008e4: addi x2, x2, 815
addr0x000008e8: sw x29, 968(x8)
addr0x000008ec: lw x19, -1606(x8)
addr0x000008f0: lw x30, 258(x8)
addr0x000008f4: lbu x22, -1527(x8)
addr0x000008f8: add x1, x23, x18
addr0x000008fc: jal x14, addr0x000007ec
addr0x00000900: andi x0, x29, -268
addr0x00000904: lw x4, 530(x8)
addr0x00000908: beq x29, x7, addr0x000006e8
addr0x0000090c: lw x18, 975(x8)
addr0x00000910: lw x31, 241(x8)
addr0x00000914: beq x4, x13, addr0x000007e8
addr0x00000918: jal x29, addr0x00000080
addr0x0000091c: lw x23, 529(x8)
addr0x00000920: sub x29, x20, x29
addr0x00000924: bgeu x23, x25, addr0x0000073c
addr0x00000928: addi x10, x23, 391
addr0x0000092c: addi x24, x0, -239
addr0x00000930: sw x2, 699(x8)
addr0x00000934: lw x29, -2047(x8)
addr0x00000938: addi x9, x9, -1187
addr0x0000093c: sw x23, 1079(x8)
addr0x00000940: addi x26, x31, -1083
addr0x00000944: lui x27, 422687
addr0x00000948: lui x27, 265108
addr0x0000094c: addi x23, x7, -1369
addr0x00000950: bgeu x16, x18, addr0x0000095c
addr0x00000954: beq x24, x17, addr0x00000184
addr0x00000958: ori x7, x22, -582
addr0x0000095c: lw x23, -1410(x8)
addr0x00000960: sb x20, 593(x8)
addr0x00000964: lbu x7, 1719(x8)
addr0x00000968: sw x18, -432(x8)
addr0x0000096c: lw x23, -1721(x8)
addr0x00000970: lw x18, -1526(x8)
addr0x00000974: sw x12, 442(x8)
addr0x00000978: sw x31, 1730(x8)
addr0x0000097c: jal x29, addr0x000003a4
addr0x00000980: andi x0, x13, 330
addr0x00000984: addi x29, x23, -245
addr0x00000988: lbu x20, -1604(x8)
addr0x0000098c: jal x27, addr0x000004e0
addr0x00000990: addi x19, x23, -1890
addr0x00000994: addi x11, x19, 256
addr0x00000998: lw x20, 725(x8)
addr0x0000099c: lw x5, -1644(x8)
addr0x000009a0: add x29, x13, x1
addr0x000009a4: beq x13, x21, addr0x000007b4
addr0x000009a8: lw x2, 899(x8)
addr0x000009ac: jal x0, addr0x00000b14
addr0x000009b0: lw x14, 1394(x8)
addr0x000009b4: lw x9, -951(x8)
addr0x000009b8: lw x10, -337(x8)
addr0x000009bc: addi x29, x20, -1839
addr0x000009c0: lhu x22, -717(x8)
addr0x000009c4: addi x7, x22, 1374
addr0x000009c8: sb x22, 951(x8)
addr0x000009cc: addi x1, x0, 856
addr0x000009d0: jal x16, addr0x00000780
addr0x000009d4: lw x23, -1137(x8)
addr0x000009d8: beq x18, x6, addr0x00000410
addr0x000009dc: beq x24, x25, addr0x00000650
addr0x000009e0: addi x18, x18, 409
addr0x000009e4: jal x30, addr0x00000a18
addr0x000009e8: sw x22, -1061(x8)
addr0x000009ec: sw x4, 643(x8)
addr0x000009f0: sw x0, -1172(x8)
addr0x000009f4: sw x14, -709(x8)
addr0x000009f8: jal x20, addr0x00000540
addr0x000009fc: lw x5, 656(x8)
addr0x00000a00: lw x13, -1931(x8)
addr0x00000a04: bgeu x1, x1, addr0x00000b6c
addr0x00000a08: bgeu x24, x2, addr0x00000b90
addr0x00000a0c: sw x30, 1322(x8)
addr0x00000a10: lw x15, 1441(x8)
addr0x00000a14: add x7, x24, x27
addr0x00000a18: lui x19, 97220
addr0x00000a1c: addi x18, x7, 1773
addr0x00000a20: addi x13, x19, -1736
addr0x00000a24: jal x13, addr0x00000464
addr0x00000a28: sh x21, 1182(x8)
addr0x00000a2c: addi x1, x13, 821
addr0x00000a30: sw x24, -300(x8)
addr0x00000a34: sw x25, -1323(x8)
addr0x00000a38: addi x0, x20, -1386
addr0x00000a3c: jal x19, addr0x00000c00
addr0x00000a40: lw x24, -398(x8)
addr0x00000a44: lbu x19, 640(x8)
addr0x00000a48: jal x25, addr0x00000954
addr0x00000a4c: sll x7, x4, x22
addr0x00000a50: lw x9, 389(x8)
addr0x00000a54: jalr x0, 136(x3)
addr0x00000a58: jal x18, addr0x00000864
addr0x00000a5c: andi x9, x9, 381
addr0x00000a60: jal x17, addr0x00000ab4
addr0x00000a64: lw x19, 871(x8)
addr0x00000a68: lw x9, -1001(x8)
addr0x00000a6c: bne x13, x6, addr0x000006cc
addr0x00000a70: jal x12, addr0x00000418
addr0x00000a74: lw x29, 380(x8)
addr0x00000a78: addi x23, x10, 456
addr0x00000a7c: jal x2, addr0x00000b58
addr0x00000a80: sw x12, 756(x8)
addr0x00000a84: sw x4, 1853(x8)
addr0x00000a88: sw x19, 109(x8)
addr0x00000a8c: lui x6, 570439
addr0x00000a90: addi x7, x29, -725
addr0x00000a94: lw x22, 1553(x8)
addr0x00000a98: sw x31, 797(x8)
addr0x00000a9c: addi x2, x9, 1986
addr0x00000aa0: slli x12, x7, 22
addr0x00000aa4: jal x30, addr0x00000938
addr0x00000aa8: lw x19, 447(x8)
addr0x00000aac: bgeu x0, x0, addr0x000008fc
addr0x00000ab0: add x20, x15, x26
addr0x00000ab4: lw x25, 1407(x8)
addr0x00000ab8: jal x22, addr0x000002c4
addr0x00000abc: lw x12, -568(x8)
addr0x00000ac0: sw x14, -1752(x8)
addr0x00000ac4: lw x28, -1008(x8)
addr0x00000ac8: lbu x22, -1250(x8)
addr0x00000acc: addi x0, x4, -56
addr0x00000ad0: andi x13, x10, 1572
addr0x00000ad4: bgeu x12, x25, addr0x00000ba8
addr0x00000ad8: bgeu x31, x7, addr0x00000a2c
addr0x00000adc: addi x18, x9, -715
addr0x00000ae0: lh x13, -1727(x8)
addr0x00000ae4: sw x0, -1465(x8)
addr0x00000ae8: sw x11, -817(x8)
addr0x00000aec: addi x24, x29, -824
addr0x00000af0: lw x0, -614(x8)
addr0x00000af4: sw x7, -586(x8)
addr0x00000af8: jal x10, addr0x0000033c
addr0x00000afc: addi x27, x0, -1332
addr0x00000b00: slli x14, x1, 4
addr0x00000b04: lbu x15, -1482(x8)
addr0x00000b08: beq x19, x26, addr0x00000a64
addr0x00000b0c: lui x20, 367572
addr0x00000b10: lui x6, 82601
addr0x00000b14: jal x22, addr0x00000490
addr0x00000b18: addi x26, x23, 1053
addr0x00000b1c: lw x25, -19(x8)
addr0x00000b20: bge x1, x31, addr0x00000980
addr0x00000b24: lw x9, -745(x8)
addr0x00000b28: add x18, x26, x7
addr0x00000b2c: lw x14, -833(x8)
addr0x00000b30: addi x17, x14, 1419
addr0x00000b34: lw x19, 573(x8)
addr0x00000b38: lw x24, 866(x8)
addr0x00000b3c: sw x20, -966(x8)
addr0x00000b40: jal x18, addr0x000005a4
addr0x00000b44: lw x30, 1665(x8)
addr0x00000b48: sub x17, x23, x19
addr0x00000b4c: jal x19, addr0x00000bfc
addr0x00000b50: jal x19, addr0x00000b3c
addr0x00000b54: lw x28, 274(x8)
addr0x00000b58: lw x27, 1480(x8)
addr0x00000b5c: sw x14, 781(x8)
addr0x00000b60: sw x23, 146(x8)
addr0x00000b64: sub x10, x14, x17
addr0x00000b68: jal x20, addr0x00000944
addr0x00000b6c: sb x21, 1815(x8)
addr0x00000b70: sb x18, 922(x8)
addr0x00000b74: lbu x19, -1615(x8)
addr0x00000b78: bne x30, x19, addr0x000008c0
addr0x00000b7c: jal x13, addr0x00000c28
addr0x00000b80: jal x0, addr0x00000374
addr0x00000b84: lui x23, 966707
addr0x00000b88: addi x29, x1, 1444
addr0x00000b8c: sub x9, x9, x30
addr0x00000b90: addi x29, x10, 50
addr0x00000b94: jal x29, addr0x00000c2c
addr0x00000b98: addi x0, x16, 689
addr0x00000b9c: sw x30, 1976(x8)
addr0x00000ba0: lw x29, -1703(x8)
addr0x00000ba4: addi x7, x17, 734
addr0x00000ba8: addi x13, x26, 233
addr0x00000bac: jal x19, addr0x00000b50
addr0x00000bb0: bne x1, x23, addr0x000004a8
addr0x00000bb4: addi x24, x27, -485
addr0x00000bb8: jal x14, addr0x00000318
addr0x00000bbc: lw x27, -1990(x8)
addr0x00000bc0: beq x0, x29, addr0x00000c20
addr0x00000bc4: lw x1, -246(x8)
addr0x00000bc8: lw x1, 1225(x8)
addr0x00000bcc: sw x20, -931(x8)
addr0x00000bd0: lw x2, -194(x8)
addr0x00000bd4: lw x9, -70(x8)
addr0x00000bd8: addi x25, x13, -1656
addr0x00000bdc: addi x22, x22, 1897
addr0x00000be0: sw x16, -97(x8)
addr0x00000be4: sw x23, 1193(x8)
addr0x00000be8: lbu x7, -1043(x8)
addr0x00000bec: lui x22, 692578
addr0x00000bf0: addi x23, x29, 1046
addr0x00000bf4: jal x27, addr0x00000bd8
addr0x00000bf8: lbu x22, 324(x8)
addr0x00000bfc: lw x17, -643(x8)
addr0x00000c00: lbu x20, 1692(x8)
addr0x00000c04: xori x4, x12, -1023
addr0x00000c08: andi x22, x27, -531
addr0x00000c0c: lw x9, -654(x8)
addr0x00000c10: addi x19, x17, -749
addr0x00000c14: sw x10, -277(x8)
addr0x00000c18: sb x2, -1738(x8)
addr0x00000c1c: lw x21, -1526(x8)
addr0x00000c20: sw x7, 1726(x8)
addr0x00000c24: jal x0, addr0x00000184
addr0x00000c28: lw x13, 606(x8)
addr0x00000c2c: sw x0, -1437(x8)
addr0x00000c30: sw x31, -220(x8)
