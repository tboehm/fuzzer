.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: jal x28, addr0x00000b54
addr0x0000001c: addi x19, x13, 1723
addr0x00000020: sw x0, 332(x8)
addr0x00000024: addi x22, x18, 809
addr0x00000028: jal x5, addr0x00000b74
addr0x0000002c: lw x19, -152(x8)
addr0x00000030: sw x18, -1047(x8)
addr0x00000034: lw x19, -1589(x8)
addr0x00000038: lw x20, -1992(x8)
addr0x0000003c: lw x30, 1175(x8)
addr0x00000040: lw x24, -300(x8)
addr0x00000044: sw x22, 1814(x8)
addr0x00000048: jal x29, addr0x0000091c
addr0x0000004c: lw x1, -1296(x8)
addr0x00000050: sw x25, -1289(x8)
addr0x00000054: jal x29, addr0x00000bdc
addr0x00000058: lw x18, -682(x8)
addr0x0000005c: lw x29, -570(x8)
addr0x00000060: beq x2, x7, addr0x0000041c
addr0x00000064: lw x5, -1685(x8)
addr0x00000068: bne x29, x6, addr0x0000013c
addr0x0000006c: sw x5, 1360(x8)
addr0x00000070: jal x15, addr0x00000b9c
addr0x00000074: lw x20, 527(x8)
addr0x00000078: jal x19, addr0x0000038c
addr0x0000007c: jal x7, addr0x00000e1c
addr0x00000080: lw x9, -688(x8)
addr0x00000084: lw x6, -161(x8)
addr0x00000088: lw x6, -471(x8)
addr0x0000008c: jal x2, addr0x00000964
addr0x00000090: lw x15, 677(x8)
addr0x00000094: addi x0, x19, 116
addr0x00000098: lui x19, 448018
addr0x0000009c: lui x2, 128070
addr0x000000a0: addi x19, x29, -1832
addr0x000000a4: jal x17, addr0x00000b8c
addr0x000000a8: lw x16, 1761(x8)
addr0x000000ac: addi x23, x14, 610
addr0x000000b0: lw x4, 377(x8)
addr0x000000b4: lw x19, -1673(x8)
addr0x000000b8: addi x9, x13, -1641
addr0x000000bc: add x4, x25, x2
addr0x000000c0: jal x14, addr0x000007fc
addr0x000000c4: addi x27, x7, 355
addr0x000000c8: lbu x0, 2025(x8)
addr0x000000cc: or x15, x20, x30
addr0x000000d0: lbu x4, 314(x8)
addr0x000000d4: sb x29, 678(x8)
addr0x000000d8: lw x1, 1888(x8)
addr0x000000dc: lw x7, -1244(x8)
addr0x000000e0: lw x24, 1311(x8)
addr0x000000e4: addi x29, x23, -1602
addr0x000000e8: sw x7, 18(x8)
addr0x000000ec: lw x18, -1284(x8)
addr0x000000f0: lw x14, 700(x8)
addr0x000000f4: lw x0, 1201(x8)
addr0x000000f8: lw x0, -1850(x8)
addr0x000000fc: jal x20, addr0x00000888
addr0x00000100: lw x0, 1243(x8)
addr0x00000104: lw x20, 376(x8)
addr0x00000108: lw x0, -1994(x8)
addr0x0000010c: addi x30, x22, 1739
addr0x00000110: sw x0, 1008(x8)
addr0x00000114: sw x20, -135(x8)
addr0x00000118: addi x29, x0, -467
addr0x0000011c: jal x20, addr0x00000968
addr0x00000120: jal x14, addr0x00000888
addr0x00000124: lw x13, 1706(x8)
addr0x00000128: lui x4, 594164
addr0x0000012c: addi x24, x19, -889
addr0x00000130: lw x29, 786(x8)
addr0x00000134: addi x20, x23, 1896
addr0x00000138: sw x30, 221(x8)
addr0x0000013c: addi x16, x1, -1623
addr0x00000140: lw x0, 1014(x8)
addr0x00000144: lw x14, 1042(x8)
addr0x00000148: addi x0, x29, -652
addr0x0000014c: sw x19, -929(x8)
addr0x00000150: addi x14, x16, 706
addr0x00000154: andi x18, x29, 166
addr0x00000158: addi x29, x1, 1144
addr0x0000015c: sw x5, -1319(x8)
addr0x00000160: sw x13, -8(x8)
addr0x00000164: sw x11, 738(x8)
addr0x00000168: sw x18, -1030(x8)
addr0x0000016c: sw x30, 1874(x8)
addr0x00000170: lw x0, 668(x8)
addr0x00000174: beq x20, x7, addr0x00000140
addr0x00000178: jal x5, addr0x0000023c
addr0x0000017c: lw x4, -1946(x8)
addr0x00000180: lw x18, -1713(x8)
addr0x00000184: lw x14, -1402(x8)
addr0x00000188: addi x1, x31, 1691
addr0x0000018c: sw x23, 440(x8)
addr0x00000190: bgeu x10, x0, addr0x000008d8
addr0x00000194: slli x22, x4, 26
addr0x00000198: slli x29, x31, 20
addr0x0000019c: sw x2, 357(x8)
addr0x000001a0: addi x18, x30, -400
addr0x000001a4: lw x10, -1271(x8)
addr0x000001a8: lw x28, 1414(x8)
addr0x000001ac: lw x9, 1657(x8)
addr0x000001b0: lw x11, 879(x8)
addr0x000001b4: lw x22, -672(x8)
addr0x000001b8: add x17, x19, x23
addr0x000001bc: lw x14, -708(x8)
addr0x000001c0: lw x0, 988(x8)
addr0x000001c4: lw x23, 260(x8)
addr0x000001c8: lw x1, -1557(x8)
addr0x000001cc: jal x30, addr0x000003f4
addr0x000001d0: lw x22, 1322(x8)
addr0x000001d4: lbu x14, 1144(x8)
addr0x000001d8: sw x24, 102(x8)
addr0x000001dc: lw x28, -196(x8)
addr0x000001e0: lw x29, 1328(x8)
addr0x000001e4: bltu x20, x23, addr0x000001c0
addr0x000001e8: jal x1, addr0x00000ca0
addr0x000001ec: lui x9, 215445
addr0x000001f0: addi x30, x13, 1840
addr0x000001f4: addi x14, x4, -347
addr0x000001f8: bne x9, x12, addr0x000004a4
addr0x000001fc: sw x30, 30(x8)
addr0x00000200: sw x4, -739(x8)
addr0x00000204: sw x23, -1854(x8)
addr0x00000208: lw x10, -1102(x8)
addr0x0000020c: addi x15, x5, 1466
addr0x00000210: jal x29, addr0x00000aa4
addr0x00000214: lw x25, -403(x8)
addr0x00000218: lw x4, 1349(x8)
addr0x0000021c: lw x31, -453(x8)
addr0x00000220: lw x7, -497(x8)
addr0x00000224: lw x13, 495(x8)
addr0x00000228: lw x22, -1654(x8)
addr0x0000022c: lw x29, -1353(x8)
addr0x00000230: lw x17, 511(x8)
addr0x00000234: add x1, x27, x1
addr0x00000238: add x25, x24, x5
addr0x0000023c: sub x20, x15, x16
addr0x00000240: lui x0, 178918
addr0x00000244: addi x24, x18, -1585
addr0x00000248: addi x23, x29, 1736
addr0x0000024c: lbu x18, -538(x8)
addr0x00000250: lw x4, 1538(x8)
addr0x00000254: jal x9, addr0x00000e58
addr0x00000258: jal x2, addr0x000005c8
addr0x0000025c: lw x31, 1613(x8)
addr0x00000260: lw x13, -1015(x8)
addr0x00000264: lw x29, 1637(x8)
addr0x00000268: lw x21, 826(x8)
addr0x0000026c: addi x2, x28, -899
addr0x00000270: add x5, x19, x7
addr0x00000274: jal x14, addr0x000003b8
addr0x00000278: sw x22, -1906(x8)
addr0x0000027c: sw x16, -591(x8)
addr0x00000280: addi x17, x7, 1229
addr0x00000284: sw x19, -1761(x8)
addr0x00000288: sw x30, -557(x8)
addr0x0000028c: sb x13, -271(x8)
addr0x00000290: sb x19, -1929(x8)
addr0x00000294: jal x6, addr0x0000018c
addr0x00000298: lw x25, -1811(x8)
addr0x0000029c: lw x23, 392(x8)
addr0x000002a0: sw x19, -1455(x8)
addr0x000002a4: lw x7, -363(x8)
addr0x000002a8: bge x30, x30, addr0x00000534
addr0x000002ac: bgeu x5, x5, addr0x000004c8
addr0x000002b0: lw x31, -1602(x8)
addr0x000002b4: slli x16, x9, 10
addr0x000002b8: sw x4, -34(x8)
addr0x000002bc: addi x24, x12, -604
addr0x000002c0: sw x4, 1103(x8)
addr0x000002c4: sh x23, 718(x8)
addr0x000002c8: slli x0, x20, 24
addr0x000002cc: add x23, x9, x11
addr0x000002d0: jal x17, addr0x000004f0
addr0x000002d4: addi x1, x7, -1398
addr0x000002d8: addi x7, x13, -761
addr0x000002dc: lui x4, 253927
addr0x000002e0: blt x2, x20, addr0x0000093c
addr0x000002e4: srai x30, x18, 21
addr0x000002e8: jal x19, addr0x0000084c
addr0x000002ec: lw x9, -1264(x8)
addr0x000002f0: add x0, x7, x31
addr0x000002f4: jal x17, addr0x0000069c
addr0x000002f8: bgeu x27, x27, addr0x00000050
addr0x000002fc: andi x29, x18, 1252
addr0x00000300: jal x29, addr0x000003a4
addr0x00000304: addi x19, x27, -142
addr0x00000308: sw x17, 1832(x8)
addr0x0000030c: jal x19, addr0x000004ac
addr0x00000310: lw x14, -1956(x8)
addr0x00000314: sw x23, 2017(x8)
addr0x00000318: addi x30, x5, -864
addr0x0000031c: jal x7, addr0x00000360
addr0x00000320: lw x24, -1588(x8)
addr0x00000324: beq x29, x14, addr0x00000544
addr0x00000328: lui x27, 811652
addr0x0000032c: addi x20, x22, -124
addr0x00000330: sw x29, -780(x8)
addr0x00000334: jal x24, addr0x000001f0
addr0x00000338: jal x22, addr0x00000c04
addr0x0000033c: sub x26, x22, x9
addr0x00000340: srai x7, x18, 15
addr0x00000344: srai x20, x25, 9
addr0x00000348: srai x7, x14, 16
addr0x0000034c: and x24, x18, x0
addr0x00000350: sw x19, -1297(x8)
addr0x00000354: jal x0, addr0x00000600
addr0x00000358: sw x30, -1090(x8)
addr0x0000035c: lw x7, 1207(x8)
addr0x00000360: sw x17, 1096(x8)
addr0x00000364: sw x9, -1137(x8)
addr0x00000368: lw x0, 1592(x8)
addr0x0000036c: lw x23, -1853(x8)
addr0x00000370: lw x22, -1486(x8)
addr0x00000374: lw x17, 767(x8)
addr0x00000378: lw x29, 1299(x8)
addr0x0000037c: jal x20, addr0x00000904
addr0x00000380: lw x28, -1226(x8)
addr0x00000384: lw x26, 559(x8)
addr0x00000388: lw x15, 638(x8)
addr0x0000038c: lw x22, -1747(x8)
addr0x00000390: lui x13, 133594
addr0x00000394: sw x24, 1848(x8)
addr0x00000398: jal x30, addr0x00000808
addr0x0000039c: addi x7, x18, 1945
addr0x000003a0: lw x24, -1447(x8)
addr0x000003a4: bgeu x24, x4, addr0x00000204
addr0x000003a8: sw x26, 1230(x8)
addr0x000003ac: lw x5, 1966(x8)
addr0x000003b0: ori x2, x4, 190
addr0x000003b4: bne x19, x9, addr0x00000738
addr0x000003b8: jal x24, addr0x0000069c
addr0x000003bc: lui x2, 219393
addr0x000003c0: jal x15, addr0x00000df4
addr0x000003c4: lw x30, 1205(x8)
addr0x000003c8: lw x30, 1893(x8)
addr0x000003cc: addi x30, x10, -301
addr0x000003d0: addi x6, x22, -1810
addr0x000003d4: sw x17, 1158(x8)
addr0x000003d8: addi x22, x5, -1760
addr0x000003dc: addi x20, x0, 614
addr0x000003e0: jal x16, addr0x00000124
addr0x000003e4: addi x6, x18, -409
addr0x000003e8: bne x10, x9, addr0x000007e8
addr0x000003ec: lw x23, 1027(x8)
addr0x000003f0: lbu x15, -1085(x8)
addr0x000003f4: addi x2, x11, 698
addr0x000003f8: sw x7, 1800(x8)
addr0x000003fc: auipc x0, 739094
addr0x00000400: slli x13, x23, 19
addr0x00000404: sb x24, -501(x8)
addr0x00000408: addi x11, x27, 1707
addr0x0000040c: sw x25, 1064(x8)
addr0x00000410: bge x20, x29, addr0x000009d0
addr0x00000414: bge x29, x0, addr0x000004bc
addr0x00000418: addi x23, x28, -575
addr0x0000041c: sw x15, 1217(x8)
addr0x00000420: addi x0, x29, -294
addr0x00000424: sw x0, 300(x8)
addr0x00000428: sw x25, 1831(x8)
addr0x0000042c: jal x18, addr0x00000740
addr0x00000430: lw x23, 1308(x8)
addr0x00000434: add x16, x29, x24
addr0x00000438: addi x20, x0, -1653
addr0x0000043c: sw x13, -564(x8)
addr0x00000440: addi x26, x28, -732
addr0x00000444: lw x1, -1749(x8)
addr0x00000448: lw x13, -1649(x8)
addr0x0000044c: sw x12, -967(x8)
addr0x00000450: addi x1, x29, 536
addr0x00000454: sw x13, -420(x8)
addr0x00000458: sw x9, 589(x8)
addr0x0000045c: addi x9, x7, 670
addr0x00000460: jal x20, addr0x00000240
addr0x00000464: sw x9, -189(x8)
addr0x00000468: jal x1, addr0x00000ed8
addr0x0000046c: addi x25, x2, 1202
addr0x00000470: sw x2, -2044(x8)
addr0x00000474: jal x15, addr0x00000da0
addr0x00000478: add x23, x0, x22
addr0x0000047c: sub x19, x28, x16
addr0x00000480: lui x6, 648949
addr0x00000484: addi x13, x18, -1279
addr0x00000488: lbu x21, 593(x8)
addr0x0000048c: lbu x7, -1691(x8)
addr0x00000490: andi x30, x19, -1891
addr0x00000494: jal x31, addr0x00000274
addr0x00000498: addi x13, x20, -1833
addr0x0000049c: auipc x30, 1025554
addr0x000004a0: addi x30, x29, -241
addr0x000004a4: jal x29, addr0x00000178
addr0x000004a8: lw x27, -1031(x8)
addr0x000004ac: ori x7, x19, -757
addr0x000004b0: sw x24, -494(x8)
addr0x000004b4: lbu x5, -1435(x8)
addr0x000004b8: lbu x19, -261(x8)
addr0x000004bc: beq x30, x9, addr0x00000b54
addr0x000004c0: sub x10, x20, x1
addr0x000004c4: sw x17, 1069(x8)
addr0x000004c8: sw x29, -1435(x8)
addr0x000004cc: addi x7, x16, -1049
addr0x000004d0: sw x24, -1822(x8)
addr0x000004d4: jal x17, addr0x00000e64
addr0x000004d8: lw x22, 510(x8)
addr0x000004dc: beq x23, x29, addr0x00000728
addr0x000004e0: bltu x0, x22, addr0x00000ba4
addr0x000004e4: lw x9, -1184(x8)
addr0x000004e8: lw x18, 1456(x8)
addr0x000004ec: sub x13, x20, x27
addr0x000004f0: lw x18, 876(x8)
addr0x000004f4: sw x13, -1072(x8)
addr0x000004f8: bne x14, x10, addr0x00000a50
addr0x000004fc: jal x6, addr0x000008c0
addr0x00000500: lw x19, 92(x8)
addr0x00000504: lw x21, 1933(x8)
addr0x00000508: lw x5, 229(x8)
addr0x0000050c: addi x23, x11, -852
addr0x00000510: lui x19, 578561
addr0x00000514: lui x31, 628037
addr0x00000518: sw x26, 1321(x8)
addr0x0000051c: sw x7, 132(x8)
addr0x00000520: sw x15, -1726(x8)
addr0x00000524: sw x10, 1904(x8)
addr0x00000528: sw x24, -1375(x8)
addr0x0000052c: addi x15, x29, 671
addr0x00000530: sw x23, 1222(x8)
addr0x00000534: addi x29, x4, 214
addr0x00000538: jal x30, addr0x00000518
addr0x0000053c: jal x31, addr0x00000ad4
addr0x00000540: lw x7, 73(x8)
addr0x00000544: lw x0, 1823(x8)
addr0x00000548: lui x4, 573849
addr0x0000054c: beq x0, x17, addr0x00000d44
addr0x00000550: bltu x14, x2, addr0x00000994
addr0x00000554: lui x23, 228667
addr0x00000558: addi x9, x7, 269
addr0x0000055c: sw x14, -1944(x8)
addr0x00000560: addi x24, x23, 736
addr0x00000564: jal x24, addr0x000006dc
addr0x00000568: jal x14, addr0x000001ac
addr0x0000056c: lw x10, -1277(x8)
addr0x00000570: lui x11, 546452
addr0x00000574: jal x21, addr0x00000738
addr0x00000578: jal x17, addr0x00000c2c
addr0x0000057c: jal x19, addr0x00000628
addr0x00000580: bgeu x15, x21, addr0x00000424
addr0x00000584: add x10, x22, x4
addr0x00000588: srli x25, x25, 27
addr0x0000058c: bltu x5, x10, addr0x00000c48
addr0x00000590: lbu x13, -441(x8)
addr0x00000594: lui x30, 206929
addr0x00000598: sw x7, -1661(x8)
addr0x0000059c: jal x24, addr0x00000548
addr0x000005a0: lui x13, 642380
addr0x000005a4: or x29, x29, x16
addr0x000005a8: or x0, x5, x22
addr0x000005ac: lbu x24, -1299(x8)
addr0x000005b0: slli x5, x17, 27
addr0x000005b4: add x13, x4, x29
addr0x000005b8: jal x19, addr0x00000724
addr0x000005bc: jal x7, addr0x000000a8
addr0x000005c0: bgeu x22, x28, addr0x000001f0
addr0x000005c4: addi x21, x13, 597
addr0x000005c8: sw x17, 1928(x8)
addr0x000005cc: lw x17, 1424(x8)
addr0x000005d0: add x9, x16, x22
addr0x000005d4: jal x13, addr0x00000214
addr0x000005d8: blt x10, x19, addr0x0000073c
addr0x000005dc: addi x29, x7, -1341
addr0x000005e0: sh x19, -1928(x8)
addr0x000005e4: lw x6, 1261(x8)
addr0x000005e8: lw x21, 1059(x8)
addr0x000005ec: addi x26, x20, 1867
addr0x000005f0: jal x1, addr0x00000ba4
addr0x000005f4: beq x2, x29, addr0x00000ae0
addr0x000005f8: lw x31, 853(x8)
addr0x000005fc: lw x23, 963(x8)
addr0x00000600: lw x2, -907(x8)
addr0x00000604: lw x30, -920(x8)
addr0x00000608: addi x4, x5, -820
addr0x0000060c: jal x20, addr0x00000600
addr0x00000610: lui x22, 1035350
addr0x00000614: addi x30, x2, 433
addr0x00000618: lw x23, -1629(x8)
addr0x0000061c: addi x19, x17, 1765
addr0x00000620: sw x19, -1148(x8)
addr0x00000624: addi x13, x10, 851
addr0x00000628: addi x29, x13, -278
addr0x0000062c: jal x31, addr0x000001ec
addr0x00000630: jal x14, addr0x0000051c
addr0x00000634: lw x29, -1078(x8)
addr0x00000638: sw x28, 697(x8)
addr0x0000063c: sw x29, -734(x8)
addr0x00000640: jal x24, addr0x00000354
addr0x00000644: lw x17, -1728(x8)
addr0x00000648: bge x9, x28, addr0x00000528
addr0x0000064c: lui x0, 931457
addr0x00000650: addi x14, x17, 1522
addr0x00000654: jal x1, addr0x00000894
addr0x00000658: addi x4, x25, 628
addr0x0000065c: jal x25, addr0x0000049c
addr0x00000660: jal x20, addr0x00000d74
addr0x00000664: lw x24, 844(x8)
addr0x00000668: lw x9, -443(x8)
addr0x0000066c: jal x17, addr0x00000d0c
addr0x00000670: lw x22, -841(x8)
addr0x00000674: addi x22, x24, -350
addr0x00000678: lw x5, 1206(x8)
addr0x0000067c: jal x0, addr0x00000248
addr0x00000680: jal x29, addr0x00000e28
addr0x00000684: blt x17, x17, addr0x000001c4
addr0x00000688: lw x13, 845(x8)
addr0x0000068c: bltu x29, x22, addr0x000009a4
addr0x00000690: slli x23, x16, 28
addr0x00000694: and x19, x23, x7
addr0x00000698: sw x9, 477(x8)
addr0x0000069c: addi x29, x21, 722
addr0x000006a0: beq x7, x13, addr0x00000604
addr0x000006a4: lw x19, -1696(x8)
addr0x000006a8: lw x22, 359(x8)
addr0x000006ac: addi x15, x23, -601
addr0x000006b0: jal x11, addr0x000006f8
addr0x000006b4: lw x29, -1215(x8)
addr0x000006b8: addi x20, x11, 1704
addr0x000006bc: sw x0, -108(x8)
addr0x000006c0: lw x9, 93(x8)
addr0x000006c4: sw x6, -973(x8)
addr0x000006c8: lui x7, 25287
addr0x000006cc: addi x29, x29, 604
addr0x000006d0: lui x0, 992893
addr0x000006d4: addi x23, x7, -1577
addr0x000006d8: jal x7, addr0x0000011c
addr0x000006dc: add x9, x14, x25
addr0x000006e0: lui x19, 257509
addr0x000006e4: addi x24, x14, 1759
addr0x000006e8: sw x14, 855(x8)
addr0x000006ec: sw x13, 961(x8)
addr0x000006f0: jal x0, addr0x00000a98
addr0x000006f4: andi x20, x13, 1632
addr0x000006f8: addi x9, x28, 1055
addr0x000006fc: addi x25, x9, -562
addr0x00000700: jal x24, addr0x00000bf0
addr0x00000704: sw x20, -578(x8)
addr0x00000708: lw x9, 1175(x8)
addr0x0000070c: lw x31, 1749(x8)
addr0x00000710: addi x19, x17, 477
addr0x00000714: jal x27, addr0x00000524
addr0x00000718: lbu x21, -211(x8)
addr0x0000071c: bne x0, x15, addr0x000005a4
addr0x00000720: lw x23, 1325(x8)
addr0x00000724: lw x4, 864(x8)
addr0x00000728: addi x13, x18, 554
addr0x0000072c: lui x19, 160348
addr0x00000730: addi x29, x19, 804
addr0x00000734: addi x24, x25, -1293
addr0x00000738: lw x0, -37(x8)
addr0x0000073c: auipc x28, 1047129
addr0x00000740: addi x18, x22, -436
addr0x00000744: lui x7, 296822
addr0x00000748: addi x30, x13, -584
addr0x0000074c: lui x10, 229508
addr0x00000750: lui x15, 1041427
addr0x00000754: addi x31, x4, -134
addr0x00000758: sw x22, -1228(x8)
addr0x0000075c: lw x7, -910(x8)
addr0x00000760: sw x13, 54(x8)
addr0x00000764: lw x29, -1(x8)
addr0x00000768: jal x28, addr0x00000600
addr0x0000076c: lbu x11, 1281(x8)
addr0x00000770: slli x24, x0, 8
addr0x00000774: sw x14, 1517(x8)
addr0x00000778: sw x7, 1334(x8)
addr0x0000077c: sw x7, 600(x8)
addr0x00000780: sw x4, -849(x8)
addr0x00000784: sw x5, -434(x8)
addr0x00000788: sw x30, -998(x8)
addr0x0000078c: jal x14, addr0x000001a4
addr0x00000790: jal x19, addr0x00000a5c
addr0x00000794: lw x5, 426(x8)
addr0x00000798: add x6, x30, x22
addr0x0000079c: jal x29, addr0x000003dc
addr0x000007a0: lw x14, -789(x8)
addr0x000007a4: sw x19, -1330(x8)
addr0x000007a8: lui x20, 70172
addr0x000007ac: addi x29, x13, 346
addr0x000007b0: jal x18, addr0x00000180
addr0x000007b4: lw x17, 388(x8)
addr0x000007b8: slli x18, x7, 28
addr0x000007bc: lw x4, -564(x8)
addr0x000007c0: lw x11, 385(x8)
addr0x000007c4: addi x17, x20, 1751
addr0x000007c8: jal x1, addr0x000004d8
addr0x000007cc: lw x23, 1356(x8)
addr0x000007d0: sh x6, 1467(x8)
addr0x000007d4: sb x22, -1074(x8)
addr0x000007d8: jal x4, addr0x0000087c
addr0x000007dc: lw x24, -227(x8)
addr0x000007e0: lw x15, 671(x8)
addr0x000007e4: lbu x2, 1147(x8)
addr0x000007e8: lw x0, 590(x8)
addr0x000007ec: addi x9, x0, 1487
addr0x000007f0: bltu x2, x14, addr0x0000066c
addr0x000007f4: divu x0, x13, x19
addr0x000007f8: lbu x15, -1507(x8)
addr0x000007fc: slli x9, x21, 29
addr0x00000800: lui x23, 293089
addr0x00000804: addi x9, x21, -1131
addr0x00000808: sw x9, 104(x8)
addr0x0000080c: sw x1, 1443(x8)
addr0x00000810: sb x0, -895(x8)
addr0x00000814: lw x2, 25(x8)
addr0x00000818: jal x14, addr0x00000380
addr0x0000081c: lw x29, 1781(x8)
addr0x00000820: addi x1, x29, -80
addr0x00000824: sw x14, 144(x8)
addr0x00000828: sw x0, -1915(x8)
addr0x0000082c: addi x31, x17, -1619
addr0x00000830: lw x15, -1055(x8)
addr0x00000834: slli x22, x30, 3
addr0x00000838: add x19, x5, x7
addr0x0000083c: jal x13, addr0x00000198
addr0x00000840: beq x28, x31, addr0x00000b98
addr0x00000844: sb x6, 1165(x8)
addr0x00000848: addi x5, x30, 1890
addr0x0000084c: bltu x14, x27, addr0x000004f8
addr0x00000850: addi x1, x10, -676
addr0x00000854: lui x10, 322068
addr0x00000858: bltu x25, x30, addr0x0000075c
addr0x0000085c: srli x23, x5, 21
addr0x00000860: bltu x19, x27, addr0x00000218
addr0x00000864: lw x29, 865(x8)
addr0x00000868: addi x13, x24, 518
addr0x0000086c: addi x9, x26, 631
addr0x00000870: jal x21, addr0x000005b8
addr0x00000874: lw x17, 1660(x8)
addr0x00000878: addi x4, x25, -307
addr0x0000087c: lbu x20, 490(x8)
addr0x00000880: add x30, x11, x19
addr0x00000884: lw x23, 2023(x8)
addr0x00000888: sw x13, -1555(x8)
addr0x0000088c: andi x2, x25, 157
addr0x00000890: sw x13, 1552(x8)
addr0x00000894: jal x19, addr0x00000844
addr0x00000898: lw x18, -1118(x8)
addr0x0000089c: lw x29, 1425(x8)
addr0x000008a0: andi x4, x30, -350
addr0x000008a4: jal x23, addr0x00000704
addr0x000008a8: lw x18, 1906(x8)
addr0x000008ac: addi x22, x24, -208
addr0x000008b0: bne x2, x23, addr0x000003c4
addr0x000008b4: lbu x20, 1703(x8)
addr0x000008b8: ori x16, x23, -1036
addr0x000008bc: slli x22, x11, 11
addr0x000008c0: addi x10, x30, -1589
addr0x000008c4: jal x29, addr0x000006f8
addr0x000008c8: andi x18, x7, 1752
addr0x000008cc: andi x28, x14, -462
addr0x000008d0: bltu x0, x5, addr0x00000388
addr0x000008d4: jal x24, addr0x00000da4
addr0x000008d8: lw x24, -535(x8)
addr0x000008dc: addi x7, x31, -1388
addr0x000008e0: addi x13, x13, -700
addr0x000008e4: sw x1, 239(x8)
addr0x000008e8: jal x0, addr0x000009d4
addr0x000008ec: lw x19, -1341(x8)
addr0x000008f0: addi x19, x20, -1445
addr0x000008f4: lw x21, -70(x8)
addr0x000008f8: jal x18, addr0x00000254
addr0x000008fc: lui x24, 891397
addr0x00000900: addi x6, x5, 20
addr0x00000904: sw x31, -150(x8)
addr0x00000908: sb x29, -1601(x8)
addr0x0000090c: slli x15, x22, 7
addr0x00000910: addi x27, x22, -572
addr0x00000914: jal x2, addr0x00000bac
addr0x00000918: jal x18, addr0x00000d0c
addr0x0000091c: sw x17, 882(x8)
addr0x00000920: lw x19, -2026(x8)
addr0x00000924: sw x7, -1559(x8)
addr0x00000928: sw x7, -895(x8)
addr0x0000092c: lw x7, -1148(x8)
addr0x00000930: sw x14, -1635(x8)
addr0x00000934: lw x7, -1313(x8)
addr0x00000938: lw x6, -156(x8)
addr0x0000093c: lw x25, 913(x8)
addr0x00000940: lw x31, 649(x8)
addr0x00000944: lui x13, 407245
addr0x00000948: addi x18, x24, -1376
addr0x0000094c: sb x19, -1191(x8)
addr0x00000950: lw x9, -831(x8)
addr0x00000954: addi x2, x5, 849
addr0x00000958: jal x15, addr0x00000c78
addr0x0000095c: jal x22, addr0x0000013c
addr0x00000960: lui x24, 60163
addr0x00000964: addi x4, x7, -1993
addr0x00000968: jal x22, addr0x000000b0
addr0x0000096c: lw x13, 1561(x8)
addr0x00000970: jal x13, addr0x000008cc
addr0x00000974: lui x22, 166004
addr0x00000978: lw x30, -1017(x8)
addr0x0000097c: lui x0, 537919
addr0x00000980: addi x7, x5, -930
addr0x00000984: lw x1, -602(x8)
addr0x00000988: lw x19, -882(x8)
addr0x0000098c: andi x7, x14, 1189
addr0x00000990: lbu x27, 1468(x8)
addr0x00000994: andi x14, x19, -506
addr0x00000998: jal x23, addr0x00000b94
addr0x0000099c: lw x9, 1180(x8)
addr0x000009a0: lw x19, 1514(x8)
addr0x000009a4: jal x15, addr0x00000324
addr0x000009a8: sw x25, 765(x8)
addr0x000009ac: jal x15, addr0x000008f8
addr0x000009b0: sw x9, -952(x8)
addr0x000009b4: lw x4, 1444(x8)
addr0x000009b8: lw x23, 753(x8)
addr0x000009bc: andi x17, x18, 1411
addr0x000009c0: addi x29, x18, 1520
addr0x000009c4: sw x9, -354(x8)
addr0x000009c8: lw x30, -1354(x8)
addr0x000009cc: lw x19, -1461(x8)
addr0x000009d0: jal x14, addr0x00000548
addr0x000009d4: addi x25, x0, 870
addr0x000009d8: sw x4, -416(x8)
addr0x000009dc: sw x5, -1137(x8)
addr0x000009e0: slli x24, x24, 2
addr0x000009e4: sub x0, x27, x9
addr0x000009e8: add x7, x0, x7
addr0x000009ec: addi x14, x25, -540
addr0x000009f0: sw x29, -1623(x8)
addr0x000009f4: lw x13, 837(x8)
addr0x000009f8: lw x0, 567(x8)
addr0x000009fc: lw x23, -173(x8)
addr0x00000a00: addi x6, x2, -1684
addr0x00000a04: sw x29, 1676(x8)
addr0x00000a08: lw x30, -1707(x8)
addr0x00000a0c: addi x4, x19, -876
addr0x00000a10: addi x2, x7, 853
addr0x00000a14: jal x11, addr0x00000848
addr0x00000a18: lw x23, -576(x8)
addr0x00000a1c: addi x13, x24, 1347
addr0x00000a20: jal x24, addr0x0000024c
addr0x00000a24: bltu x28, x20, addr0x00000a08
addr0x00000a28: jal x22, addr0x0000068c
addr0x00000a2c: addi x13, x21, 1211
addr0x00000a30: lw x24, -1568(x8)
addr0x00000a34: sw x9, -426(x8)
addr0x00000a38: jal x0, addr0x00000a54
addr0x00000a3c: lbu x4, -1306(x8)
addr0x00000a40: bne x28, x22, addr0x0000099c
addr0x00000a44: addi x25, x9, -137
addr0x00000a48: sw x18, 2006(x8)
addr0x00000a4c: sw x18, -495(x8)
addr0x00000a50: addi x23, x20, 1721
addr0x00000a54: jal x14, addr0x00000a88
addr0x00000a58: sb x16, -1108(x8)
addr0x00000a5c: lw x20, 1743(x8)
addr0x00000a60: addi x19, x31, -104
addr0x00000a64: sw x27, 1147(x8)
addr0x00000a68: lui x19, 607348
addr0x00000a6c: slli x13, x5, 18
addr0x00000a70: add x2, x30, x19
addr0x00000a74: bge x22, x22, addr0x00000a5c
addr0x00000a78: addi x23, x17, -736
addr0x00000a7c: addi x6, x4, 1590
addr0x00000a80: jal x0, addr0x000003fc
addr0x00000a84: lw x14, 102(x8)
addr0x00000a88: sub x29, x15, x13
addr0x00000a8c: add x7, x19, x20
addr0x00000a90: jal x29, addr0x00000b38
addr0x00000a94: jal x29, addr0x00000e0c
addr0x00000a98: jal x14, addr0x00000448
addr0x00000a9c: jal x19, addr0x000001cc
addr0x00000aa0: jal x18, addr0x00000674
addr0x00000aa4: beq x9, x21, addr0x000004f4
addr0x00000aa8: bne x30, x19, addr0x00000ea8
addr0x00000aac: sw x27, -1096(x8)
addr0x00000ab0: addi x7, x0, -84
addr0x00000ab4: sw x5, 1451(x8)
addr0x00000ab8: lw x13, 869(x8)
addr0x00000abc: lw x13, 1712(x8)
addr0x00000ac0: lw x27, -1015(x8)
addr0x00000ac4: jal x26, addr0x00000d48
addr0x00000ac8: lw x18, 313(x8)
addr0x00000acc: bgeu x5, x19, addr0x00000508
addr0x00000ad0: lbu x20, 156(x8)
addr0x00000ad4: bge x7, x20, addr0x00000bdc
addr0x00000ad8: addi x14, x27, 643
addr0x00000adc: jal x20, addr0x00000b5c
addr0x00000ae0: addi x25, x27, 1937
addr0x00000ae4: sw x17, -1520(x8)
addr0x00000ae8: sw x9, 1957(x8)
addr0x00000aec: sw x24, -1140(x8)
addr0x00000af0: sw x2, -1450(x8)
addr0x00000af4: bltu x21, x2, addr0x00000ed0
addr0x00000af8: slli x31, x7, 15
addr0x00000afc: jal x29, addr0x000000b0
addr0x00000b00: sb x14, -268(x8)
addr0x00000b04: sw x2, 762(x8)
addr0x00000b08: bge x4, x17, addr0x000003fc
addr0x00000b0c: bne x0, x30, addr0x00000970
addr0x00000b10: lw x25, 1935(x8)
addr0x00000b14: jal x10, addr0x000004f8
addr0x00000b18: lw x13, 970(x8)
addr0x00000b1c: addi x0, x31, 1723
addr0x00000b20: lui x28, 680156
addr0x00000b24: bge x18, x29, addr0x00000b18
addr0x00000b28: lbu x2, -359(x8)
addr0x00000b2c: sw x4, 910(x8)
addr0x00000b30: lui x7, 545182
addr0x00000b34: addi x31, x6, 274
addr0x00000b38: sw x22, -1640(x8)
addr0x00000b3c: lui x4, 992140
addr0x00000b40: sb x18, -1263(x8)
addr0x00000b44: lw x27, -311(x8)
addr0x00000b48: sw x7, 1236(x8)
addr0x00000b4c: sw x9, -598(x8)
addr0x00000b50: jal x5, addr0x00000a08
addr0x00000b54: addi x9, x19, 719
addr0x00000b58: jal x28, addr0x00000c10
addr0x00000b5c: addi x23, x22, -416
addr0x00000b60: jal x1, addr0x000000e4
addr0x00000b64: lw x25, -286(x8)
addr0x00000b68: lw x21, 1869(x8)
addr0x00000b6c: sw x23, 1223(x8)
addr0x00000b70: sw x24, 1685(x8)
addr0x00000b74: addi x0, x13, 955
addr0x00000b78: lw x9, -753(x8)
addr0x00000b7c: lw x11, 1831(x8)
addr0x00000b80: jal x17, addr0x000000b8
addr0x00000b84: sw x15, -825(x8)
addr0x00000b88: lbu x29, 1715(x8)
addr0x00000b8c: xori x14, x1, 1214
addr0x00000b90: and x0, x13, x4
addr0x00000b94: sh x29, 272(x8)
addr0x00000b98: beq x13, x28, addr0x00000a60
addr0x00000b9c: lbu x0, -1185(x8)
addr0x00000ba0: lw x28, 971(x8)
addr0x00000ba4: addi x7, x19, -408
addr0x00000ba8: jal x13, addr0x00000d28
addr0x00000bac: addi x29, x4, -580
addr0x00000bb0: add x31, x15, x30
addr0x00000bb4: sh x7, -109(x8)
addr0x00000bb8: lui x20, 484805
addr0x00000bbc: addi x13, x25, -942
addr0x00000bc0: jal x16, addr0x00000a2c
addr0x00000bc4: addi x27, x15, 864
addr0x00000bc8: jal x20, addr0x000006a0
addr0x00000bcc: lw x7, 1882(x8)
addr0x00000bd0: blt x13, x5, addr0x00000440
addr0x00000bd4: lw x7, 293(x8)
addr0x00000bd8: lw x7, 1648(x8)
addr0x00000bdc: lw x9, 977(x8)
addr0x00000be0: add x9, x1, x29
addr0x00000be4: add x20, x2, x29
addr0x00000be8: lbu x17, 2043(x8)
addr0x00000bec: bne x20, x5, addr0x000004f8
addr0x00000bf0: add x13, x15, x21
addr0x00000bf4: jal x18, addr0x00000ddc
addr0x00000bf8: bne x28, x13, addr0x00000c8c
addr0x00000bfc: lbu x2, 682(x8)
addr0x00000c00: andi x18, x13, -682
addr0x00000c04: add x9, x23, x19
addr0x00000c08: sw x20, 980(x8)
addr0x00000c0c: lw x22, -1252(x8)
addr0x00000c10: lbu x24, 890(x8)
addr0x00000c14: addi x30, x22, 1534
addr0x00000c18: sw x11, -478(x8)
addr0x00000c1c: sw x10, 363(x8)
addr0x00000c20: sw x0, 1941(x8)
addr0x00000c24: sw x0, -1891(x8)
addr0x00000c28: jal x13, addr0x000007d0
addr0x00000c2c: sw x0, 1055(x8)
addr0x00000c30: lw x25, -1358(x8)
addr0x00000c34: lw x29, -1449(x8)
addr0x00000c38: jal x4, addr0x00000c20
addr0x00000c3c: lw x4, -949(x8)
addr0x00000c40: lw x23, 1520(x8)
addr0x00000c44: lw x31, -1990(x8)
addr0x00000c48: lw x26, 2038(x8)
addr0x00000c4c: lbu x20, 1595(x8)
addr0x00000c50: andi x28, x0, -357
addr0x00000c54: sw x31, 573(x8)
addr0x00000c58: lw x5, -919(x8)
addr0x00000c5c: sw x14, -560(x8)
addr0x00000c60: lui x0, 450358
addr0x00000c64: sw x20, 330(x8)
addr0x00000c68: lui x7, 778732
addr0x00000c6c: addi x20, x24, -1851
addr0x00000c70: sw x29, 2006(x8)
addr0x00000c74: lw x18, 1463(x8)
addr0x00000c78: lw x2, 1333(x8)
addr0x00000c7c: lw x22, -1732(x8)
addr0x00000c80: lw x0, -1899(x8)
addr0x00000c84: addi x2, x2, -17
addr0x00000c88: jal x7, addr0x00000890
addr0x00000c8c: lw x1, -259(x8)
addr0x00000c90: lw x14, 639(x8)
addr0x00000c94: add x24, x17, x19
addr0x00000c98: beq x4, x5, addr0x00000d04
addr0x00000c9c: sw x29, 1075(x8)
addr0x00000ca0: addi x1, x29, 2038
addr0x00000ca4: sw x10, -1309(x8)
addr0x00000ca8: sw x21, -1833(x8)
addr0x00000cac: jal x19, addr0x00000eb4
addr0x00000cb0: lw x29, 818(x8)
addr0x00000cb4: add x31, x0, x0
addr0x00000cb8: lw x19, 1485(x8)
addr0x00000cbc: lw x0, 2013(x8)
addr0x00000cc0: lw x19, -670(x8)
addr0x00000cc4: addi x5, x20, -1238
addr0x00000cc8: sw x0, 1632(x8)
addr0x00000ccc: sw x7, 2034(x8)
addr0x00000cd0: sw x19, 782(x8)
addr0x00000cd4: jal x23, addr0x00000ee4
addr0x00000cd8: jal x10, addr0x00000960
addr0x00000cdc: sw x7, 1250(x8)
addr0x00000ce0: sw x19, 1706(x8)
addr0x00000ce4: addi x19, x30, -1575
addr0x00000ce8: jal x7, addr0x000008f4
addr0x00000cec: sw x24, 1310(x8)
addr0x00000cf0: lw x14, 1135(x8)
addr0x00000cf4: addi x27, x28, -1161
addr0x00000cf8: jal x0, addr0x000001bc
addr0x00000cfc: jal x28, addr0x000002a4
addr0x00000d00: jal x1, addr0x00000d00
addr0x00000d04: lw x11, -1605(x8)
addr0x00000d08: sw x19, -269(x8)
addr0x00000d0c: addi x23, x0, -479
addr0x00000d10: addi x7, x23, 782
addr0x00000d14: lw x14, -1857(x8)
addr0x00000d18: sw x19, 359(x8)
addr0x00000d1c: lw x28, 442(x8)
addr0x00000d20: sw x0, 1698(x8)
addr0x00000d24: addi x26, x27, 336
addr0x00000d28: jal x20, addr0x00000e2c
addr0x00000d2c: addi x29, x22, -542
addr0x00000d30: addi x7, x23, 2014
addr0x00000d34: jal x17, addr0x0000014c
addr0x00000d38: lw x5, -1519(x8)
addr0x00000d3c: lw x29, 1911(x8)
addr0x00000d40: lui x27, 677056
addr0x00000d44: jal x7, addr0x00000b34
addr0x00000d48: addi x19, x29, -1699
addr0x00000d4c: lui x31, 486000
addr0x00000d50: sw x29, 210(x8)
addr0x00000d54: jal x13, addr0x000008a8
addr0x00000d58: jal x30, addr0x00000130
addr0x00000d5c: blt x29, x6, addr0x000006f0
addr0x00000d60: blt x19, x17, addr0x00000ae8
addr0x00000d64: srai x7, x0, 5
addr0x00000d68: bgeu x14, x2, addr0x00000610
addr0x00000d6c: sw x14, -787(x8)
addr0x00000d70: addi x17, x29, 4
addr0x00000d74: lw x6, 253(x8)
addr0x00000d78: lw x25, -1140(x8)
addr0x00000d7c: add x0, x23, x0
addr0x00000d80: jal x14, addr0x000009ec
addr0x00000d84: jal x22, addr0x00000ec4
addr0x00000d88: sub x30, x17, x13
addr0x00000d8c: beq x24, x29, addr0x000006d0
addr0x00000d90: lw x9, -699(x8)
addr0x00000d94: andi x27, x24, -53
addr0x00000d98: andi x16, x23, 189
addr0x00000d9c: lbu x29, -336(x8)
addr0x00000da0: lui x5, 720242
addr0x00000da4: addi x9, x17, -1369
addr0x00000da8: jal x31, addr0x00000764
addr0x00000dac: lw x24, -2006(x8)
addr0x00000db0: lw x7, -1596(x8)
addr0x00000db4: lw x5, -213(x8)
addr0x00000db8: lw x23, -1243(x8)
addr0x00000dbc: addi x29, x19, -1641
addr0x00000dc0: addi x29, x10, -175
addr0x00000dc4: bne x28, x2, addr0x00000a08
addr0x00000dc8: lbu x27, 124(x8)
addr0x00000dcc: lw x23, 467(x8)
addr0x00000dd0: lw x17, 1817(x8)
addr0x00000dd4: addi x17, x9, -42
addr0x00000dd8: bne x24, x27, addr0x00000ac8
addr0x00000ddc: and x13, x24, x19
addr0x00000de0: xori x29, x29, -812
addr0x00000de4: andi x1, x17, 465
addr0x00000de8: sb x27, 271(x8)
addr0x00000dec: lbu x18, -1186(x8)
addr0x00000df0: lw x18, -1160(x8)
addr0x00000df4: lbu x7, 102(x8)
addr0x00000df8: ecall 
addr0x00000dfc: lw x13, -1390(x8)
addr0x00000e00: jal x0, addr0x00000ed4
addr0x00000e04: bne x18, x22, addr0x00000640
addr0x00000e08: lw x29, -1615(x8)
addr0x00000e0c: bltu x4, x30, addr0x00000640
addr0x00000e10: lbu x18, 269(x8)
addr0x00000e14: lw x24, -641(x8)
addr0x00000e18: addi x17, x23, 286
addr0x00000e1c: sb x0, 1777(x8)
addr0x00000e20: lbu x30, -1930(x8)
addr0x00000e24: jal x16, addr0x00000a24
addr0x00000e28: lw x6, 453(x8)
addr0x00000e2c: lw x29, -845(x8)
addr0x00000e30: sw x21, 690(x8)
addr0x00000e34: sw x30, -1595(x8)
addr0x00000e38: lw x17, -1387(x8)
addr0x00000e3c: addi x22, x15, -849
addr0x00000e40: sw x14, -45(x8)
addr0x00000e44: sw x7, -938(x8)
addr0x00000e48: sw x4, 1432(x8)
addr0x00000e4c: add x28, x5, x0
addr0x00000e50: sw x10, -175(x8)
addr0x00000e54: addi x29, x19, 267
addr0x00000e58: addi x31, x9, -1730
addr0x00000e5c: jal x20, addr0x00000e24
addr0x00000e60: jal x23, addr0x00000664
addr0x00000e64: addi x14, x28, 1675
addr0x00000e68: jal x29, addr0x00000378
addr0x00000e6c: sw x29, -929(x8)
addr0x00000e70: sw x18, -1834(x8)
addr0x00000e74: lw x5, -1217(x8)
addr0x00000e78: lw x22, 1274(x8)
addr0x00000e7c: sub x25, x29, x24
addr0x00000e80: lw x10, 144(x8)
addr0x00000e84: bltu x15, x4, addr0x00000bfc
addr0x00000e88: lw x10, 1300(x8)
addr0x00000e8c: lw x7, 1400(x8)
addr0x00000e90: lw x0, -1391(x8)
addr0x00000e94: lw x1, 76(x8)
addr0x00000e98: lw x19, 1748(x8)
addr0x00000e9c: addi x7, x15, -745
addr0x00000ea0: jal x29, addr0x00000600
addr0x00000ea4: addi x24, x26, -31
addr0x00000ea8: lw x5, -1251(x8)
addr0x00000eac: lw x18, 624(x8)
addr0x00000eb0: sw x23, 764(x8)
addr0x00000eb4: auipc x7, 309356
addr0x00000eb8: jalr x0, 744(x3)
addr0x00000ebc: beq x14, x9, addr0x00000754
addr0x00000ec0: lui x28, 380260
addr0x00000ec4: lw x17, 1880(x8)
addr0x00000ec8: addi x18, x19, -1809
addr0x00000ecc: lw x0, -775(x8)
addr0x00000ed0: lw x1, -556(x8)
addr0x00000ed4: addi x9, x29, 983
addr0x00000ed8: addi x28, x0, 794
addr0x00000edc: lw x22, -267(x8)
addr0x00000ee0: lw x30, 1539(x8)
addr0x00000ee4: addi x0, x0, -17
addr0x00000ee8: jal x31, addr0x0000050c
addr0x00000eec: jal x7, addr0x0000056c
addr0x00000ef0: lw x29, 677(x8)
addr0x00000ef4: lw x14, -1913(x8)
addr0x00000ef8: lui x23, 504173
addr0x00000efc: addi x23, x29, 876
