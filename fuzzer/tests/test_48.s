.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: beq x23, x17, addr0x000000d8
addr0x0000001c: lbu x14, 1101(x8)
addr0x00000020: add x19, x5, x19
addr0x00000024: lbu x25, -992(x8)
addr0x00000028: beq x5, x6, addr0x00000424
addr0x0000002c: addi x11, x1, 504
addr0x00000030: sb x30, 121(x8)
addr0x00000034: sw x15, -1473(x8)
addr0x00000038: lw x13, -1476(x8)
addr0x0000003c: lui x29, 125122
addr0x00000040: lui x24, 305638
addr0x00000044: sw x20, 331(x8)
addr0x00000048: sw x29, -530(x8)
addr0x0000004c: jal x18, addr0x00000b94
addr0x00000050: lw x0, 1354(x8)
addr0x00000054: jal x19, addr0x000018b4
addr0x00000058: lw x19, 584(x8)
addr0x0000005c: lw x15, -1636(x8)
addr0x00000060: lbu x7, 1696(x8)
addr0x00000064: lbu x16, -958(x8)
addr0x00000068: srli x13, x13, 4
addr0x0000006c: lw x17, 172(x8)
addr0x00000070: lw x19, -1221(x8)
addr0x00000074: addi x0, x10, 1087
addr0x00000078: lw x22, 283(x8)
addr0x0000007c: addi x10, x17, 964
addr0x00000080: andi x26, x9, -536
addr0x00000084: andi x9, x27, -45
addr0x00000088: lw x0, -73(x8)
addr0x0000008c: addi x23, x10, 890
addr0x00000090: add x0, x16, x26
addr0x00000094: srli x26, x29, 19
addr0x00000098: lw x19, -1420(x8)
addr0x0000009c: addi x19, x17, -842
addr0x000000a0: lw x19, -1379(x8)
addr0x000000a4: addi x29, x30, 1252
addr0x000000a8: jal x22, addr0x000016c0
addr0x000000ac: lw x29, -65(x8)
addr0x000000b0: jal x20, addr0x00000334
addr0x000000b4: bne x14, x20, addr0x0000066c
addr0x000000b8: jal x25, addr0x00000964
addr0x000000bc: lw x14, -151(x8)
addr0x000000c0: jal x4, addr0x0000033c
addr0x000000c4: lw x23, -296(x8)
addr0x000000c8: jal x9, addr0x00001780
addr0x000000cc: srli x9, x26, 28
addr0x000000d0: addi x14, x30, 1446
addr0x000000d4: lw x11, 2025(x8)
addr0x000000d8: lw x1, 149(x8)
addr0x000000dc: jal x9, addr0x0000057c
addr0x000000e0: jal x28, addr0x000007dc
addr0x000000e4: lw x5, -1428(x8)
addr0x000000e8: lui x13, 803735
addr0x000000ec: addi x22, x1, -1282
addr0x000000f0: jal x29, addr0x0000166c
addr0x000000f4: lw x16, 899(x8)
addr0x000000f8: lw x27, -327(x8)
addr0x000000fc: jal x21, addr0x00000100
addr0x00000100: sw x9, 1062(x8)
addr0x00000104: lui x11, 278684
addr0x00000108: addi x23, x29, 1506
addr0x0000010c: jal x18, addr0x00000438
addr0x00000110: addi x31, x10, -959
addr0x00000114: addi x19, x6, -1679
addr0x00000118: jal x9, addr0x00001788
addr0x0000011c: sw x7, -827(x8)
addr0x00000120: lw x6, 1195(x8)
addr0x00000124: lw x12, -386(x8)
addr0x00000128: lw x19, 849(x8)
addr0x0000012c: lw x21, -885(x8)
addr0x00000130: addi x19, x27, -1957
addr0x00000134: addi x14, x31, -781
addr0x00000138: jal x9, addr0x00000380
addr0x0000013c: lw x12, 963(x8)
addr0x00000140: lw x29, 701(x8)
addr0x00000144: addi x0, x18, 1147
addr0x00000148: sw x28, 2041(x8)
addr0x0000014c: sh x26, 1269(x8)
addr0x00000150: lui x15, 788405
addr0x00000154: addi x1, x11, -502
addr0x00000158: lw x18, -605(x8)
addr0x0000015c: sw x29, 1975(x8)
addr0x00000160: sh x13, 1189(x8)
addr0x00000164: lw x30, -21(x8)
addr0x00000168: lw x0, 1023(x8)
addr0x0000016c: addi x6, x16, 963
addr0x00000170: lw x0, -113(x8)
addr0x00000174: add x13, x11, x23
addr0x00000178: sw x9, 523(x8)
addr0x0000017c: sw x9, -836(x8)
addr0x00000180: sw x30, 561(x8)
addr0x00000184: lw x13, -1090(x8)
addr0x00000188: lw x17, 1063(x8)
addr0x0000018c: lw x27, 678(x8)
addr0x00000190: lw x29, 340(x8)
addr0x00000194: beq x23, x21, addr0x00000240
addr0x00000198: sw x6, -553(x8)
addr0x0000019c: lw x7, 1780(x8)
addr0x000001a0: sw x31, -1345(x8)
addr0x000001a4: lw x11, 1510(x8)
addr0x000001a8: sw x23, -1470(x8)
addr0x000001ac: jal x26, addr0x00000f5c
addr0x000001b0: addi x19, x27, 1238
addr0x000001b4: sw x28, -1771(x8)
addr0x000001b8: lw x19, -800(x8)
addr0x000001bc: lbu x29, 35(x8)
addr0x000001c0: sb x27, -453(x8)
addr0x000001c4: sb x19, -830(x8)
addr0x000001c8: lw x23, -1487(x8)
addr0x000001cc: add x9, x4, x7
addr0x000001d0: lw x15, 792(x8)
addr0x000001d4: addi x18, x13, 1209
addr0x000001d8: lw x9, 796(x8)
addr0x000001dc: lw x26, -852(x8)
addr0x000001e0: lw x9, 1680(x8)
addr0x000001e4: lw x28, 491(x8)
addr0x000001e8: jal x19, addr0x00001428
addr0x000001ec: lui x17, 151309
addr0x000001f0: srai x22, x21, 19
addr0x000001f4: bgeu x28, x17, addr0x000009d8
addr0x000001f8: addi x4, x27, -1612
addr0x000001fc: jal x30, addr0x00000c04
addr0x00000200: bne x31, x20, addr0x00000834
addr0x00000204: jal x24, addr0x000000a4
addr0x00000208: lw x17, 1981(x8)
addr0x0000020c: jal x7, addr0x00000cb8
addr0x00000210: beq x1, x7, addr0x00000240
addr0x00000214: lw x12, 1278(x8)
addr0x00000218: addi x18, x29, -529
addr0x0000021c: sw x17, 1346(x8)
addr0x00000220: lw x1, 629(x8)
addr0x00000224: lw x30, -1342(x8)
addr0x00000228: sw x14, -1123(x8)
addr0x0000022c: jal x4, addr0x00000168
addr0x00000230: lw x27, -708(x8)
addr0x00000234: lw x21, 1567(x8)
addr0x00000238: jal x27, addr0x000006ac
addr0x0000023c: addi x10, x30, 142
addr0x00000240: add x27, x23, x17
addr0x00000244: sw x17, -292(x8)
addr0x00000248: addi x4, x11, -581
addr0x0000024c: sw x7, 1353(x8)
addr0x00000250: lui x12, 861440
addr0x00000254: lui x28, 23700
addr0x00000258: addi x13, x24, -1494
addr0x0000025c: sw x18, 994(x8)
addr0x00000260: lw x29, -932(x8)
addr0x00000264: lw x12, 661(x8)
addr0x00000268: lw x30, -1723(x8)
addr0x0000026c: sw x22, -1266(x8)
addr0x00000270: sw x17, 11(x8)
addr0x00000274: sw x7, 663(x8)
addr0x00000278: sw x15, 179(x8)
addr0x0000027c: lui x9, 14847
addr0x00000280: sw x25, 1438(x8)
addr0x00000284: jal x27, addr0x00000708
addr0x00000288: addi x23, x23, 1257
addr0x0000028c: addi x31, x9, -444
addr0x00000290: jal x1, addr0x00001264
addr0x00000294: lw x19, -434(x8)
addr0x00000298: sw x26, 102(x8)
addr0x0000029c: add x14, x14, x1
addr0x000002a0: lw x18, 1899(x8)
addr0x000002a4: lw x2, 1865(x8)
addr0x000002a8: lw x31, 1352(x8)
addr0x000002ac: addi x11, x9, 1129
addr0x000002b0: ori x2, x27, -123
addr0x000002b4: beq x11, x14, addr0x000009f4
addr0x000002b8: bltu x15, x0, addr0x00000ab0
addr0x000002bc: bne x6, x23, addr0x00000998
addr0x000002c0: lbu x15, -925(x8)
addr0x000002c4: andi x22, x9, -1020
addr0x000002c8: addi x29, x21, 405
addr0x000002cc: jal x10, addr0x00000c04
addr0x000002d0: lw x0, -1559(x8)
addr0x000002d4: lbu x31, -1585(x8)
addr0x000002d8: jal x17, addr0x00000fa8
addr0x000002dc: lbu x7, -805(x8)
addr0x000002e0: auipc x7, 908803
addr0x000002e4: addi x2, x2, -791
addr0x000002e8: jal x19, addr0x000014a0
addr0x000002ec: addi x31, x18, -1622
addr0x000002f0: sw x5, -1699(x8)
addr0x000002f4: bge x7, x15, addr0x00000888
addr0x000002f8: lui x14, 599375
addr0x000002fc: addi x0, x16, -575
addr0x00000300: jal x11, addr0x00000524
addr0x00000304: lw x18, -1114(x8)
addr0x00000308: jal x14, addr0x00000588
addr0x0000030c: jal x14, addr0x00000dec
addr0x00000310: lw x10, -746(x8)
addr0x00000314: jal x6, addr0x00000cb0
addr0x00000318: lw x15, -1230(x8)
addr0x0000031c: add x19, x1, x30
addr0x00000320: jal x5, addr0x00001150
addr0x00000324: lw x9, 277(x8)
addr0x00000328: jal x23, addr0x0000084c
addr0x0000032c: lw x13, 1051(x8)
addr0x00000330: lui x20, 170159
addr0x00000334: addi x17, x14, 1006
addr0x00000338: slli x14, x13, 7
addr0x0000033c: lw x0, 1287(x8)
addr0x00000340: addi x20, x13, -301
addr0x00000344: addi x27, x22, 621
addr0x00000348: lui x10, 871535
addr0x0000034c: addi x27, x19, 273
addr0x00000350: lw x24, 1844(x8)
addr0x00000354: jal x22, addr0x00001904
addr0x00000358: sw x10, 1808(x8)
addr0x0000035c: jal x31, addr0x0000108c
addr0x00000360: lw x10, 693(x8)
addr0x00000364: lw x9, -819(x8)
addr0x00000368: lw x7, 1386(x8)
addr0x0000036c: lw x9, -889(x8)
addr0x00000370: sw x19, -222(x8)
addr0x00000374: sw x22, 1980(x8)
addr0x00000378: lhu x9, -1738(x8)
addr0x0000037c: lw x31, -1309(x8)
addr0x00000380: lw x20, 1015(x8)
addr0x00000384: sw x25, 665(x8)
addr0x00000388: sw x13, 1843(x8)
addr0x0000038c: sw x20, 506(x8)
addr0x00000390: sw x24, 1743(x8)
addr0x00000394: jal x22, addr0x00001990
addr0x00000398: lw x28, 160(x8)
addr0x0000039c: lbu x30, 2044(x8)
addr0x000003a0: andi x10, x14, -1683
addr0x000003a4: bne x2, x15, addr0x00000814
addr0x000003a8: slt x21, x0, x29
addr0x000003ac: addi x27, x30, 1107
addr0x000003b0: jal x30, addr0x00000458
addr0x000003b4: addi x2, x14, 784
addr0x000003b8: addi x4, x7, -386
addr0x000003bc: sw x13, 843(x8)
addr0x000003c0: sw x22, -1262(x8)
addr0x000003c4: jal x22, addr0x00000284
addr0x000003c8: lui x20, 385039
addr0x000003cc: sw x4, -900(x8)
addr0x000003d0: addi x30, x13, -844
addr0x000003d4: sh x26, 0(x8)
addr0x000003d8: lw x11, 1832(x8)
addr0x000003dc: lw x27, -1024(x8)
addr0x000003e0: addi x18, x31, 1506
addr0x000003e4: addi x30, x13, -1070
addr0x000003e8: add x27, x20, x6
addr0x000003ec: andi x24, x7, 613
addr0x000003f0: add x19, x0, x27
addr0x000003f4: addi x11, x9, -45
addr0x000003f8: lw x10, 1655(x8)
addr0x000003fc: lui x16, 215485
addr0x00000400: addi x10, x14, -1575
addr0x00000404: sw x20, -5(x8)
addr0x00000408: sw x1, -456(x8)
addr0x0000040c: addi x29, x30, -1994
addr0x00000410: addi x15, x22, 1062
addr0x00000414: beq x5, x20, addr0x00000a28
addr0x00000418: lw x20, -569(x8)
addr0x0000041c: sw x10, 479(x8)
addr0x00000420: addi x14, x0, -174
addr0x00000424: slli x13, x10, 24
addr0x00000428: add x7, x13, x6
addr0x0000042c: jal x5, addr0x000004d0
addr0x00000430: addi x0, x19, -28
addr0x00000434: lui x17, 545735
addr0x00000438: lbu x30, 263(x8)
addr0x0000043c: lw x18, 679(x8)
addr0x00000440: addi x10, x4, 156
addr0x00000444: lw x0, 1444(x8)
addr0x00000448: lw x29, -509(x8)
addr0x0000044c: lw x18, -987(x8)
addr0x00000450: lw x13, -1622(x8)
addr0x00000454: lw x23, 1939(x8)
addr0x00000458: bgeu x23, x31, addr0x0000097c
addr0x0000045c: lw x1, 861(x8)
addr0x00000460: sw x29, -1659(x8)
addr0x00000464: sw x21, 1692(x8)
addr0x00000468: sw x1, 590(x8)
addr0x0000046c: sw x23, 1047(x8)
addr0x00000470: jal x24, addr0x00001868
addr0x00000474: lw x9, -1938(x8)
addr0x00000478: sw x25, -186(x8)
addr0x0000047c: lbu x25, -598(x8)
addr0x00000480: sb x19, 233(x8)
addr0x00000484: lw x15, 797(x8)
addr0x00000488: lw x15, 64(x8)
addr0x0000048c: sw x20, 531(x8)
addr0x00000490: lw x11, 983(x8)
addr0x00000494: lw x18, -2002(x8)
addr0x00000498: lui x0, 513561
addr0x0000049c: lw x29, -1447(x8)
addr0x000004a0: add x31, x17, x9
addr0x000004a4: sw x7, -1154(x8)
addr0x000004a8: sw x20, -565(x8)
addr0x000004ac: bne x20, x15, addr0x00000534
addr0x000004b0: addi x14, x17, 59
addr0x000004b4: jal x17, addr0x00001060
addr0x000004b8: lw x31, -592(x8)
addr0x000004bc: addi x15, x13, -301
addr0x000004c0: lw x13, 967(x8)
addr0x000004c4: sw x18, 1662(x8)
addr0x000004c8: sw x30, -1561(x8)
addr0x000004cc: addi x13, x19, 898
addr0x000004d0: lui x7, 1023854
addr0x000004d4: addi x15, x17, -726
addr0x000004d8: sw x0, 242(x8)
addr0x000004dc: jal x31, addr0x00000e08
addr0x000004e0: sw x19, 1560(x8)
addr0x000004e4: sw x6, -380(x8)
addr0x000004e8: lw x0, 1213(x8)
addr0x000004ec: sw x27, -950(x8)
addr0x000004f0: sw x2, 1434(x8)
addr0x000004f4: sw x13, -63(x8)
addr0x000004f8: sw x15, -393(x8)
addr0x000004fc: addi x20, x20, -1570
addr0x00000500: beq x20, x22, addr0x00000cec
addr0x00000504: lw x22, -223(x8)
addr0x00000508: jal x23, addr0x00001954
addr0x0000050c: lbu x20, -888(x8)
addr0x00000510: jal x6, addr0x000004f4
addr0x00000514: sub x19, x30, x31
addr0x00000518: jal x22, addr0x00001a1c
addr0x0000051c: lw x10, 1908(x8)
addr0x00000520: lw x12, 1801(x8)
addr0x00000524: sw x13, 985(x8)
addr0x00000528: addi x31, x13, -1518
addr0x0000052c: addi x16, x29, 1563
addr0x00000530: jal x1, addr0x000008f4
addr0x00000534: bgeu x23, x28, addr0x00000424
addr0x00000538: lw x17, 38(x8)
addr0x0000053c: or x25, x31, x31
addr0x00000540: lbu x20, 1950(x8)
addr0x00000544: sub x25, x18, x15
addr0x00000548: lw x6, -1655(x8)
addr0x0000054c: addi x16, x30, 25
addr0x00000550: jal x15, addr0x000008ac
addr0x00000554: lw x31, 352(x8)
addr0x00000558: lw x11, 1807(x8)
addr0x0000055c: beq x19, x22, addr0x00000a54
addr0x00000560: jalr x0, 528(x3)
addr0x00000564: addi x12, x31, 973
addr0x00000568: lw x13, -144(x8)
addr0x0000056c: sb x24, 1032(x8)
addr0x00000570: jal x0, addr0x000007b8
addr0x00000574: lw x26, -1610(x8)
addr0x00000578: bgeu x12, x13, addr0x00000354
addr0x0000057c: lbu x22, 1980(x8)
addr0x00000580: slli x13, x7, 4
addr0x00000584: lw x29, 1971(x8)
addr0x00000588: jal x0, addr0x00001438
addr0x0000058c: lw x26, 149(x8)
addr0x00000590: sw x16, 1653(x8)
addr0x00000594: sw x7, -286(x8)
addr0x00000598: sw x13, -724(x8)
addr0x0000059c: lw x13, 1448(x8)
addr0x000005a0: lw x18, 1170(x8)
addr0x000005a4: lw x18, -966(x8)
addr0x000005a8: addi x29, x16, -53
addr0x000005ac: add x17, x25, x23
addr0x000005b0: sw x15, 879(x8)
addr0x000005b4: slli x26, x30, 11
addr0x000005b8: slli x22, x28, 12
addr0x000005bc: srli x6, x2, 5
addr0x000005c0: lw x20, 551(x8)
addr0x000005c4: lw x24, 1681(x8)
addr0x000005c8: lbu x13, -330(x8)
addr0x000005cc: jal x29, addr0x000008fc
addr0x000005d0: jal x25, addr0x00000778
addr0x000005d4: jal x19, addr0x00000d3c
addr0x000005d8: lw x20, -311(x8)
addr0x000005dc: lw x23, -1406(x8)
addr0x000005e0: addi x27, x28, 348
addr0x000005e4: lw x19, -1675(x8)
addr0x000005e8: lw x23, -985(x8)
addr0x000005ec: sw x13, -703(x8)
addr0x000005f0: lw x27, -100(x8)
addr0x000005f4: sw x6, 899(x8)
addr0x000005f8: lw x9, -329(x8)
addr0x000005fc: lw x19, -1350(x8)
addr0x00000600: addi x5, x22, 839
addr0x00000604: jal x23, addr0x00000ab4
addr0x00000608: lw x7, 1769(x8)
addr0x0000060c: lw x28, -701(x8)
addr0x00000610: lw x22, -733(x8)
addr0x00000614: addi x29, x0, 1587
addr0x00000618: jal x20, addr0x00000fd4
addr0x0000061c: beq x30, x19, addr0x00000d18
addr0x00000620: addi x19, x28, -861
addr0x00000624: sh x13, -226(x8)
addr0x00000628: bne x19, x5, addr0x00000258
addr0x0000062c: lw x30, 434(x8)
addr0x00000630: lw x10, -1123(x8)
addr0x00000634: jal x25, addr0x000017a4
addr0x00000638: srli x18, x14, 7
addr0x0000063c: bltu x22, x10, addr0x000009d8
addr0x00000640: bgeu x19, x28, addr0x00000530
addr0x00000644: lw x30, -175(x8)
addr0x00000648: lw x13, -1753(x8)
addr0x0000064c: lw x18, 1715(x8)
addr0x00000650: lbu x10, -201(x8)
addr0x00000654: beq x11, x21, addr0x000004b8
addr0x00000658: lbu x30, 1833(x8)
addr0x0000065c: addi x29, x25, -1518
addr0x00000660: lw x14, 430(x8)
addr0x00000664: lw x19, -697(x8)
addr0x00000668: addi x7, x21, 210
addr0x0000066c: jal x9, addr0x00000b20
addr0x00000670: jal x20, addr0x00001838
addr0x00000674: lw x13, -516(x8)
addr0x00000678: lw x23, -549(x8)
addr0x0000067c: lw x17, 288(x8)
addr0x00000680: lw x7, 1892(x8)
addr0x00000684: lw x23, 1929(x8)
addr0x00000688: sw x2, -2012(x8)
addr0x0000068c: sw x28, 33(x8)
addr0x00000690: sw x0, 217(x8)
addr0x00000694: lw x19, -574(x8)
addr0x00000698: addi x7, x9, 1182
addr0x0000069c: jal x7, addr0x00000f84
addr0x000006a0: div x18, x31, x12
addr0x000006a4: slli x4, x9, 6
addr0x000006a8: lw x21, -74(x8)
addr0x000006ac: sw x23, 1339(x8)
addr0x000006b0: addi x7, x22, 1547
addr0x000006b4: beq x13, x29, addr0x00000dc0
addr0x000006b8: addi x20, x7, 657
addr0x000006bc: sw x30, 595(x8)
addr0x000006c0: sw x31, 378(x8)
addr0x000006c4: sw x18, -709(x8)
addr0x000006c8: andi x22, x7, -2035
addr0x000006cc: jal x9, addr0x00000ef8
addr0x000006d0: lw x31, -1002(x8)
addr0x000006d4: sw x13, 1279(x8)
addr0x000006d8: sw x26, 1240(x8)
addr0x000006dc: jal x13, addr0x00001000
addr0x000006e0: jal x22, addr0x000003d4
addr0x000006e4: addi x19, x22, 61
addr0x000006e8: bne x2, x14, addr0x000007bc
addr0x000006ec: lw x17, -1240(x8)
addr0x000006f0: lw x27, 546(x8)
addr0x000006f4: sw x23, -957(x8)
addr0x000006f8: sw x22, 1903(x8)
addr0x000006fc: sw x10, 1237(x8)
addr0x00000700: addi x7, x6, 124
addr0x00000704: sw x14, 1346(x8)
addr0x00000708: sw x13, -1053(x8)
addr0x0000070c: sub x6, x0, x14
addr0x00000710: lbu x28, 1560(x8)
addr0x00000714: addi x0, x5, -1255
addr0x00000718: add x23, x23, x17
addr0x0000071c: jal x22, addr0x000015ac
addr0x00000720: lw x14, 904(x8)
addr0x00000724: lw x11, 1650(x8)
addr0x00000728: addi x1, x1, 1401
addr0x0000072c: sb x20, -183(x8)
addr0x00000730: sb x15, 969(x8)
addr0x00000734: bne x21, x27, addr0x00000a9c
addr0x00000738: jal x14, addr0x000015a4
addr0x0000073c: addi x15, x26, -1134
addr0x00000740: addi x17, x11, -491
addr0x00000744: lw x0, -139(x8)
addr0x00000748: addi x31, x24, 773
addr0x0000074c: addi x20, x12, 1828
addr0x00000750: sh x18, -286(x8)
addr0x00000754: bge x28, x18, addr0x00000ef4
addr0x00000758: lw x25, -992(x8)
addr0x0000075c: beq x5, x31, addr0x000006a4
addr0x00000760: lw x22, -1831(x8)
addr0x00000764: addi x14, x17, 1243
addr0x00000768: sw x20, 210(x8)
addr0x0000076c: beq x18, x22, addr0x00000864
addr0x00000770: lw x12, 1829(x8)
addr0x00000774: lbu x20, -1754(x8)
addr0x00000778: add x2, x19, x31
addr0x0000077c: jal x22, addr0x00001344
addr0x00000780: addi x30, x10, 1207
addr0x00000784: lw x14, 1510(x8)
addr0x00000788: addi x27, x17, -500
addr0x0000078c: sw x20, -1192(x8)
addr0x00000790: lw x0, -1571(x8)
addr0x00000794: sw x13, 1452(x8)
addr0x00000798: addi x18, x27, -1129
addr0x0000079c: jal x29, addr0x00001648
addr0x000007a0: sw x9, 576(x8)
addr0x000007a4: jal x9, addr0x00000b3c
addr0x000007a8: lw x19, 1457(x8)
addr0x000007ac: lw x7, 1575(x8)
addr0x000007b0: sw x0, -1238(x8)
addr0x000007b4: jal x28, addr0x00001110
addr0x000007b8: bge x13, x31, addr0x00000390
addr0x000007bc: sw x13, 479(x8)
addr0x000007c0: sb x29, -1247(x8)
addr0x000007c4: addi x14, x13, 1850
addr0x000007c8: sw x22, 1399(x8)
addr0x000007cc: lw x5, -1425(x8)
addr0x000007d0: sw x7, -1959(x8)
addr0x000007d4: sw x22, 1744(x8)
addr0x000007d8: sw x9, -1186(x8)
addr0x000007dc: sw x19, -1437(x8)
addr0x000007e0: jal x26, addr0x00001568
addr0x000007e4: jal x18, addr0x0000173c
addr0x000007e8: lw x0, -37(x8)
addr0x000007ec: addi x11, x27, 1311
addr0x000007f0: beq x9, x13, addr0x000003cc
addr0x000007f4: bgeu x23, x0, addr0x000006bc
addr0x000007f8: lw x7, 978(x8)
addr0x000007fc: lw x14, 1423(x8)
addr0x00000800: beq x23, x22, addr0x00000b30
addr0x00000804: lbu x7, 198(x8)
addr0x00000808: slli x13, x15, 4
addr0x0000080c: bne x23, x31, addr0x000007cc
addr0x00000810: sw x5, -1866(x8)
addr0x00000814: sw x12, -304(x8)
addr0x00000818: lui x5, 114832
addr0x0000081c: addi x10, x18, -1664
addr0x00000820: jal x14, addr0x0000144c
addr0x00000824: slli x1, x9, 1
addr0x00000828: or x20, x22, x11
addr0x0000082c: srli x24, x12, 5
addr0x00000830: sub x16, x1, x29
addr0x00000834: lui x29, 365368
addr0x00000838: addi x6, x2, 1932
addr0x0000083c: addi x17, x31, -1323
addr0x00000840: addi x26, x22, 1676
addr0x00000844: addi x5, x4, -1146
addr0x00000848: add x23, x13, x28
addr0x0000084c: lbu x13, -1153(x8)
addr0x00000850: addi x9, x12, 647
addr0x00000854: sw x0, 541(x8)
addr0x00000858: addi x19, x19, -406
addr0x0000085c: addi x22, x7, -1319
addr0x00000860: bge x23, x23, addr0x000002b0
addr0x00000864: lw x12, -1684(x8)
addr0x00000868: lui x23, 183337
addr0x0000086c: addi x19, x5, -1511
addr0x00000870: jal x18, addr0x0000151c
addr0x00000874: addi x27, x0, -320
addr0x00000878: lw x15, -1565(x8)
addr0x0000087c: lw x23, 1510(x8)
addr0x00000880: lw x7, -163(x8)
addr0x00000884: lui x28, 562390
addr0x00000888: jal x23, addr0x00000f00
addr0x0000088c: lw x28, 715(x8)
addr0x00000890: sw x16, -1741(x8)
addr0x00000894: sw x22, -1659(x8)
addr0x00000898: lw x27, 1020(x8)
addr0x0000089c: lw x30, 1068(x8)
addr0x000008a0: lw x28, -1162(x8)
addr0x000008a4: lw x31, 1394(x8)
addr0x000008a8: lw x5, 658(x8)
addr0x000008ac: bne x21, x11, addr0x0000099c
addr0x000008b0: addi x5, x20, -429
addr0x000008b4: sw x13, -1004(x8)
addr0x000008b8: sw x29, -1103(x8)
addr0x000008bc: addi x2, x29, 871
addr0x000008c0: auipc x15, 227439
addr0x000008c4: addi x29, x27, -279
addr0x000008c8: sb x31, 566(x8)
addr0x000008cc: sw x7, -849(x8)
addr0x000008d0: lw x19, 1550(x8)
addr0x000008d4: lw x31, -1174(x8)
addr0x000008d8: sw x1, -1007(x8)
addr0x000008dc: sw x31, -1562(x8)
addr0x000008e0: sw x19, 1601(x8)
addr0x000008e4: sw x29, -1473(x8)
addr0x000008e8: lw x28, 1565(x8)
addr0x000008ec: lw x23, 559(x8)
addr0x000008f0: bgeu x24, x4, addr0x000008d8
addr0x000008f4: add x29, x22, x18
addr0x000008f8: jal x23, addr0x00001610
addr0x000008fc: jal x28, addr0x00001748
addr0x00000900: sw x7, 894(x8)
addr0x00000904: sw x23, -503(x8)
addr0x00000908: lw x15, 253(x8)
addr0x0000090c: jal x4, addr0x00001484
addr0x00000910: lw x17, -1474(x8)
addr0x00000914: lw x29, 314(x8)
addr0x00000918: lw x21, 1662(x8)
addr0x0000091c: addi x20, x19, 939
addr0x00000920: lui x22, 1022004
addr0x00000924: addi x19, x27, 1640
addr0x00000928: sw x18, 698(x8)
addr0x0000092c: addi x19, x9, -1128
addr0x00000930: addi x31, x12, -855
addr0x00000934: beq x23, x27, addr0x000009a0
addr0x00000938: bltu x13, x7, addr0x00000868
addr0x0000093c: bge x18, x24, addr0x00000f5c
addr0x00000940: blt x29, x27, addr0x00000b7c
addr0x00000944: lui x29, 962329
addr0x00000948: sw x2, -294(x8)
addr0x0000094c: jal x26, addr0x000003ac
addr0x00000950: addi x13, x28, -1800
addr0x00000954: addi x4, x17, -763
addr0x00000958: sw x17, -495(x8)
addr0x0000095c: jal x5, addr0x00001288
addr0x00000960: beq x22, x18, addr0x000009bc
addr0x00000964: lw x23, 1415(x8)
addr0x00000968: lw x19, 442(x8)
addr0x0000096c: addi x4, x22, 601
addr0x00000970: sw x22, -1395(x8)
addr0x00000974: sw x21, 1544(x8)
addr0x00000978: sw x1, 1524(x8)
addr0x0000097c: lw x13, -697(x8)
addr0x00000980: addi x5, x25, -1819
addr0x00000984: lui x29, 312820
addr0x00000988: jal x29, addr0x00000894
addr0x0000098c: ori x27, x1, -2032
addr0x00000990: sw x20, 676(x8)
addr0x00000994: sw x5, 741(x8)
addr0x00000998: sw x0, -825(x8)
addr0x0000099c: addi x29, x2, -48
addr0x000009a0: sw x0, 1800(x8)
addr0x000009a4: lw x27, 1934(x8)
addr0x000009a8: jal x29, addr0x00000a54
addr0x000009ac: lw x21, -1172(x8)
addr0x000009b0: sw x10, -588(x8)
addr0x000009b4: jal x19, addr0x00001278
addr0x000009b8: blt x22, x13, addr0x00000438
addr0x000009bc: lw x13, -608(x8)
addr0x000009c0: lw x21, 1292(x8)
addr0x000009c4: jal x30, addr0x00001160
addr0x000009c8: addi x22, x22, 1415
addr0x000009cc: jal x0, addr0x00001064
addr0x000009d0: lui x18, 988593
addr0x000009d4: addi x19, x10, -1284
addr0x000009d8: jal x20, addr0x0000030c
addr0x000009dc: lw x22, 102(x8)
addr0x000009e0: lw x17, -1660(x8)
addr0x000009e4: jal x29, addr0x000019e4
addr0x000009e8: sw x7, 1287(x8)
addr0x000009ec: addi x19, x19, 286
addr0x000009f0: sw x13, 961(x8)
addr0x000009f4: addi x9, x13, -662
addr0x000009f8: beq x29, x13, addr0x00001024
addr0x000009fc: andi x20, x6, 1926
addr0x00000a00: lui x6, 987309
addr0x00000a04: addi x16, x7, -958
addr0x00000a08: sw x5, -1642(x8)
addr0x00000a0c: blt x23, x12, addr0x00000490
addr0x00000a10: addi x6, x20, 129
addr0x00000a14: jal x22, addr0x00001624
addr0x00000a18: addi x29, x23, -1334
addr0x00000a1c: addi x6, x20, 1735
addr0x00000a20: jal x21, addr0x00000700
addr0x00000a24: lw x19, -895(x8)
addr0x00000a28: lw x9, 521(x8)
addr0x00000a2c: jal x22, addr0x00000a10
addr0x00000a30: lw x29, -1175(x8)
addr0x00000a34: addi x23, x27, 1717
addr0x00000a38: add x1, x11, x16
addr0x00000a3c: jal x15, addr0x0000113c
addr0x00000a40: lui x0, 47888
addr0x00000a44: sh x16, 636(x8)
addr0x00000a48: sw x18, -808(x8)
addr0x00000a4c: addi x23, x29, -1705
addr0x00000a50: lw x21, 111(x8)
addr0x00000a54: andi x2, x13, 1451
addr0x00000a58: beq x5, x10, addr0x0000123c
addr0x00000a5c: lw x13, 1118(x8)
addr0x00000a60: bgeu x0, x19, addr0x00000914
addr0x00000a64: addi x23, x7, 105
addr0x00000a68: jal x30, addr0x0000079c
addr0x00000a6c: addi x22, x1, 1542
addr0x00000a70: lui x11, 921895
addr0x00000a74: addi x17, x18, 1609
addr0x00000a78: addi x20, x0, -587
addr0x00000a7c: add x13, x0, x5
addr0x00000a80: sb x18, -979(x8)
addr0x00000a84: sw x13, 519(x8)
addr0x00000a88: lw x13, -1774(x8)
addr0x00000a8c: sw x7, 473(x8)
addr0x00000a90: sw x19, -922(x8)
addr0x00000a94: lw x31, 837(x8)
addr0x00000a98: jal x24, addr0x0000060c
addr0x00000a9c: lw x13, -1800(x8)
addr0x00000aa0: jal x22, addr0x00000a80
addr0x00000aa4: bltu x29, x20, addr0x00000ae8
addr0x00000aa8: addi x22, x2, 705
addr0x00000aac: lw x22, 913(x8)
addr0x00000ab0: addi x2, x0, -273
addr0x00000ab4: addi x14, x26, -1670
addr0x00000ab8: bge x20, x18, addr0x00001054
addr0x00000abc: beq x28, x20, addr0x0000085c
addr0x00000ac0: addi x7, x20, 1933
addr0x00000ac4: sw x19, -963(x8)
addr0x00000ac8: lw x27, 558(x8)
addr0x00000acc: jal x21, addr0x00000a8c
addr0x00000ad0: jal x17, addr0x00000260
addr0x00000ad4: addi x19, x28, 1229
addr0x00000ad8: sw x13, 460(x8)
addr0x00000adc: sw x18, -1582(x8)
addr0x00000ae0: add x18, x29, x27
addr0x00000ae4: jal x26, addr0x00000f30
addr0x00000ae8: lw x25, 800(x8)
addr0x00000aec: add x18, x7, x12
addr0x00000af0: srli x5, x27, 14
addr0x00000af4: addi x29, x29, -425
addr0x00000af8: addi x24, x25, -1571
addr0x00000afc: sw x27, 131(x8)
addr0x00000b00: jal x29, addr0x00000074
addr0x00000b04: jal x5, addr0x00000f1c
addr0x00000b08: lw x16, 927(x8)
addr0x00000b0c: andi x19, x1, 394
addr0x00000b10: jal x29, addr0x00000f28
addr0x00000b14: lw x11, 817(x8)
addr0x00000b18: lw x25, -685(x8)
addr0x00000b1c: lw x17, -947(x8)
addr0x00000b20: addi x30, x0, 1708
addr0x00000b24: andi x18, x20, -1913
addr0x00000b28: lw x5, -1349(x8)
addr0x00000b2c: lw x13, 1154(x8)
addr0x00000b30: andi x20, x7, -1416
addr0x00000b34: addi x30, x15, -541
addr0x00000b38: lw x7, 1644(x8)
addr0x00000b3c: lbu x2, 1285(x8)
addr0x00000b40: sub x0, x15, x24
addr0x00000b44: sub x26, x12, x2
addr0x00000b48: srai x4, x29, 28
addr0x00000b4c: lbu x5, -720(x8)
addr0x00000b50: jal x13, addr0x0000012c
addr0x00000b54: lw x0, 99(x8)
addr0x00000b58: bltu x28, x22, addr0x00000fb4
addr0x00000b5c: addi x31, x7, 18
addr0x00000b60: jal x4, addr0x000008e4
addr0x00000b64: jal x19, addr0x00001270
addr0x00000b68: sw x20, 220(x8)
addr0x00000b6c: sb x19, -1972(x8)
addr0x00000b70: bltu x1, x7, addr0x000007c0
addr0x00000b74: lbu x22, -555(x8)
addr0x00000b78: sb x15, 1235(x8)
addr0x00000b7c: sb x12, -1488(x8)
addr0x00000b80: lw x27, -64(x8)
addr0x00000b84: lw x22, 1949(x8)
addr0x00000b88: addi x29, x19, -109
addr0x00000b8c: sw x13, -828(x8)
addr0x00000b90: andi x25, x25, -410
addr0x00000b94: lw x11, -1718(x8)
addr0x00000b98: addi x23, x4, 1312
addr0x00000b9c: lw x24, -1568(x8)
addr0x00000ba0: beq x9, x18, addr0x00000a70
addr0x00000ba4: addi x5, x17, 762
addr0x00000ba8: bne x14, x2, addr0x00000c84
addr0x00000bac: addi x12, x5, 1047
addr0x00000bb0: bge x10, x27, addr0x00001358
addr0x00000bb4: addi x20, x27, -531
addr0x00000bb8: sw x12, -1531(x8)
addr0x00000bbc: jal x27, addr0x00000b3c
addr0x00000bc0: sw x25, -748(x8)
addr0x00000bc4: lw x17, -1783(x8)
addr0x00000bc8: addi x5, x27, 1961
addr0x00000bcc: add x0, x21, x29
addr0x00000bd0: bne x14, x26, addr0x00000ba4
addr0x00000bd4: addi x29, x12, 1680
addr0x00000bd8: sw x17, -343(x8)
addr0x00000bdc: sw x10, 82(x8)
addr0x00000be0: sb x15, -237(x8)
addr0x00000be4: lw x23, 2003(x8)
addr0x00000be8: addi x9, x29, -1324
addr0x00000bec: sw x7, 1569(x8)
addr0x00000bf0: sw x22, 919(x8)
addr0x00000bf4: jal x13, addr0x000000a8
addr0x00000bf8: sw x27, 1761(x8)
addr0x00000bfc: lui x20, 313495
addr0x00000c00: addi x20, x23, -459
addr0x00000c04: sw x9, 163(x8)
addr0x00000c08: jal x1, addr0x00001698
addr0x00000c0c: jal x27, addr0x00000c04
addr0x00000c10: lw x15, 1726(x8)
addr0x00000c14: lw x13, -1958(x8)
addr0x00000c18: slli x18, x16, 27
addr0x00000c1c: sw x18, 1938(x8)
addr0x00000c20: addi x23, x26, 1746
addr0x00000c24: addi x28, x30, 1038
addr0x00000c28: beq x13, x17, addr0x00000b58
addr0x00000c2c: jal x9, addr0x00001404
addr0x00000c30: slli x13, x31, 25
addr0x00000c34: auipc x23, 1011355
addr0x00000c38: jalr x0, 500(x3)
addr0x00000c3c: bne x18, x26, addr0x00000b30
addr0x00000c40: jal x13, addr0x00000480
addr0x00000c44: addi x26, x7, 1277
addr0x00000c48: jal x19, addr0x000006a8
addr0x00000c4c: lw x26, 714(x8)
addr0x00000c50: sw x23, -1849(x8)
addr0x00000c54: sw x19, -432(x8)
addr0x00000c58: sw x24, 1984(x8)
addr0x00000c5c: sw x18, 1780(x8)
addr0x00000c60: sb x22, 1804(x8)
addr0x00000c64: addi x19, x10, -1541
addr0x00000c68: addi x4, x27, 1333
addr0x00000c6c: jal x19, addr0x000012e0
addr0x00000c70: lw x12, -1711(x8)
addr0x00000c74: lw x30, -1276(x8)
addr0x00000c78: lw x26, 1131(x8)
addr0x00000c7c: lw x22, 76(x8)
addr0x00000c80: beq x1, x26, addr0x00000bf0
addr0x00000c84: jal x23, addr0x00000d9c
addr0x00000c88: jal x19, addr0x00000d68
addr0x00000c8c: beq x18, x9, addr0x000013c0
addr0x00000c90: lw x31, -814(x8)
addr0x00000c94: addi x2, x0, -1460
addr0x00000c98: lbu x0, 1613(x8)
addr0x00000c9c: lw x21, 1949(x8)
addr0x00000ca0: lw x24, 57(x8)
addr0x00000ca4: addi x20, x28, 285
addr0x00000ca8: jal x24, addr0x00000cb4
addr0x00000cac: lw x31, 540(x8)
addr0x00000cb0: bltu x25, x20, addr0x00000a6c
addr0x00000cb4: addi x30, x18, 646
addr0x00000cb8: jal x20, addr0x00001668
addr0x00000cbc: addi x19, x29, -889
addr0x00000cc0: lw x28, 1755(x8)
addr0x00000cc4: lw x0, -138(x8)
addr0x00000cc8: lw x14, 118(x8)
addr0x00000ccc: lw x30, 1316(x8)
addr0x00000cd0: bltu x29, x24, addr0x000007dc
addr0x00000cd4: mul x17, x2, x18
addr0x00000cd8: and x24, x17, x23
addr0x00000cdc: or x11, x12, x28
addr0x00000ce0: sw x20, -980(x8)
addr0x00000ce4: srli x29, x7, 12
addr0x00000ce8: slli x23, x7, 22
addr0x00000cec: and x7, x7, x9
addr0x00000cf0: slli x9, x23, 3
addr0x00000cf4: sw x9, 391(x8)
addr0x00000cf8: addi x25, x26, 989
addr0x00000cfc: sw x22, -315(x8)
addr0x00000d00: jal x9, addr0x000012f8
addr0x00000d04: lw x30, 2029(x8)
addr0x00000d08: lw x31, 1608(x8)
addr0x00000d0c: jal x16, addr0x00000c5c
addr0x00000d10: bltu x14, x22, addr0x00000674
addr0x00000d14: lw x20, -611(x8)
addr0x00000d18: lw x21, -498(x8)
addr0x00000d1c: lw x31, 630(x8)
addr0x00000d20: lw x9, 1251(x8)
addr0x00000d24: lw x29, -787(x8)
addr0x00000d28: addi x16, x29, -1461
addr0x00000d2c: lui x22, 395421
addr0x00000d30: addi x0, x0, 930
addr0x00000d34: addi x23, x2, 119
addr0x00000d38: jal x28, addr0x00001550
addr0x00000d3c: jal x19, addr0x00001304
addr0x00000d40: sw x13, 378(x8)
addr0x00000d44: sw x0, -557(x8)
addr0x00000d48: lbu x5, 488(x8)
addr0x00000d4c: bne x23, x16, addr0x00000b70
addr0x00000d50: lw x30, 251(x8)
addr0x00000d54: jal x18, addr0x0000197c
addr0x00000d58: jal x7, addr0x00000ab4
addr0x00000d5c: jal x27, addr0x00001578
addr0x00000d60: jal x15, addr0x000003f8
addr0x00000d64: addi x27, x17, -1513
addr0x00000d68: jal x31, addr0x0000148c
addr0x00000d6c: lw x0, 1838(x8)
addr0x00000d70: addi x20, x9, -415
addr0x00000d74: sw x27, -687(x8)
addr0x00000d78: lui x10, 466919
addr0x00000d7c: addi x0, x29, -584
addr0x00000d80: lbu x19, -1251(x8)
addr0x00000d84: sb x10, 503(x8)
addr0x00000d88: jal x23, addr0x00000a44
addr0x00000d8c: lbu x18, -833(x8)
addr0x00000d90: bne x0, x5, addr0x00000cf0
addr0x00000d94: lw x22, -285(x8)
addr0x00000d98: lw x31, -506(x8)
addr0x00000d9c: sw x24, -1567(x8)
addr0x00000da0: jal x12, addr0x00000604
addr0x00000da4: lw x7, 1682(x8)
addr0x00000da8: lw x17, 1844(x8)
addr0x00000dac: lw x31, 457(x8)
addr0x00000db0: lw x21, 1253(x8)
addr0x00000db4: sw x23, -779(x8)
addr0x00000db8: lw x22, 611(x8)
addr0x00000dbc: lw x5, 1756(x8)
addr0x00000dc0: sw x7, -177(x8)
addr0x00000dc4: jal x4, addr0x00000b48
addr0x00000dc8: bgeu x25, x21, addr0x00000680
addr0x00000dcc: lw x25, -586(x8)
addr0x00000dd0: jal x22, addr0x000011a0
addr0x00000dd4: sw x1, -41(x8)
addr0x00000dd8: lw x19, -138(x8)
addr0x00000ddc: lw x7, -1002(x8)
addr0x00000de0: bge x7, x9, addr0x00000614
addr0x00000de4: addi x22, x29, -1877
addr0x00000de8: slli x11, x15, 28
addr0x00000dec: srli x24, x18, 19
addr0x00000df0: lui x22, 850370
addr0x00000df4: lw x28, -1858(x8)
addr0x00000df8: addi x23, x22, -354
addr0x00000dfc: addi x29, x7, -899
addr0x00000e00: lw x13, -115(x8)
addr0x00000e04: lw x19, -918(x8)
addr0x00000e08: lw x29, 413(x8)
addr0x00000e0c: lw x6, 1637(x8)
addr0x00000e10: lh x19, 437(x8)
addr0x00000e14: slli x25, x1, 30
addr0x00000e18: addi x2, x13, 1587
addr0x00000e1c: sw x23, -1272(x8)
addr0x00000e20: lw x22, 1627(x8)
addr0x00000e24: lw x23, -846(x8)
addr0x00000e28: bge x25, x13, addr0x00000e58
addr0x00000e2c: slli x6, x22, 1
addr0x00000e30: lw x27, -1497(x8)
addr0x00000e34: bne x23, x18, addr0x000011f8
addr0x00000e38: lhu x9, -978(x8)
addr0x00000e3c: addi x24, x20, -1196
addr0x00000e40: addi x9, x14, 1333
addr0x00000e44: lw x5, -657(x8)
addr0x00000e48: addi x18, x11, 799
addr0x00000e4c: bne x12, x9, addr0x000012ec
addr0x00000e50: addi x27, x4, 1169
addr0x00000e54: sw x19, -1379(x8)
addr0x00000e58: sw x22, -1535(x8)
addr0x00000e5c: sw x27, 1620(x8)
addr0x00000e60: sw x23, -207(x8)
addr0x00000e64: lw x7, -1471(x8)
addr0x00000e68: jal x21, addr0x00000de4
addr0x00000e6c: sw x23, 1741(x8)
addr0x00000e70: sw x5, 176(x8)
addr0x00000e74: jal x20, addr0x000011dc
addr0x00000e78: lw x29, 642(x8)
addr0x00000e7c: lbu x26, -1457(x8)
addr0x00000e80: lw x19, -1193(x8)
addr0x00000e84: sw x15, -650(x8)
addr0x00000e88: addi x25, x18, 86
addr0x00000e8c: jal x30, addr0x00000964
addr0x00000e90: jal x23, addr0x0000055c
addr0x00000e94: lui x7, 75502
addr0x00000e98: addi x9, x22, -512
addr0x00000e9c: lw x27, 571(x8)
addr0x00000ea0: lw x12, 1107(x8)
addr0x00000ea4: lw x6, -1147(x8)
addr0x00000ea8: lui x13, 943687
addr0x00000eac: addi x12, x24, -1230
addr0x00000eb0: jal x17, addr0x0000139c
addr0x00000eb4: jal x22, addr0x0000131c
addr0x00000eb8: andi x27, x13, 162
addr0x00000ebc: sw x27, -1959(x8)
addr0x00000ec0: addi x27, x19, -1347
addr0x00000ec4: sw x24, -483(x8)
addr0x00000ec8: sh x22, 594(x8)
addr0x00000ecc: bne x18, x12, addr0x00000da8
addr0x00000ed0: lw x30, 604(x8)
addr0x00000ed4: lbu x25, 754(x8)
addr0x00000ed8: addi x23, x31, 1460
addr0x00000edc: addi x18, x16, 2042
addr0x00000ee0: addi x9, x17, -89
addr0x00000ee4: sub x26, x22, x21
addr0x00000ee8: andi x4, x17, -770
addr0x00000eec: bne x14, x16, addr0x000011fc
addr0x00000ef0: bltu x19, x4, addr0x000010f8
addr0x00000ef4: slli x28, x7, 8
addr0x00000ef8: lw x23, -741(x8)
addr0x00000efc: lw x11, 1404(x8)
addr0x00000f00: sub x11, x0, x18
addr0x00000f04: addi x6, x19, -1815
addr0x00000f08: jal x20, addr0x00000cb4
addr0x00000f0c: ori x14, x27, 1011
addr0x00000f10: sw x7, -378(x8)
addr0x00000f14: jal x29, addr0x000018c8
addr0x00000f18: addi x20, x25, 1666
addr0x00000f1c: jal x11, addr0x00001724
addr0x00000f20: sw x10, -1410(x8)
addr0x00000f24: bne x26, x13, addr0x00001488
addr0x00000f28: sw x14, -809(x8)
addr0x00000f2c: sw x22, -1171(x8)
addr0x00000f30: lbu x16, 131(x8)
addr0x00000f34: jal x13, addr0x000013ac
addr0x00000f38: addi x16, x16, -486
addr0x00000f3c: addi x5, x9, -1228
addr0x00000f40: sw x18, -1174(x8)
addr0x00000f44: jal x30, addr0x00001968
addr0x00000f48: or x18, x19, x13
addr0x00000f4c: addi x20, x2, 572
addr0x00000f50: jal x9, addr0x00000694
addr0x00000f54: lw x14, 778(x8)
addr0x00000f58: lw x6, -1111(x8)
addr0x00000f5c: sub x18, x28, x31
addr0x00000f60: jal x23, addr0x00001820
addr0x00000f64: lui x22, 883878
addr0x00000f68: sw x18, -956(x8)
addr0x00000f6c: sw x30, 834(x8)
addr0x00000f70: sw x2, -413(x8)
addr0x00000f74: sw x19, -963(x8)
addr0x00000f78: addi x10, x24, -317
addr0x00000f7c: sh x4, 1300(x8)
addr0x00000f80: sw x28, -29(x8)
addr0x00000f84: lw x15, -895(x8)
addr0x00000f88: lw x5, -1476(x8)
addr0x00000f8c: jal x17, addr0x00000208
addr0x00000f90: sw x30, -1340(x8)
addr0x00000f94: lw x2, 1593(x8)
addr0x00000f98: add x7, x21, x31
addr0x00000f9c: addi x29, x18, -7
addr0x00000fa0: addi x13, x19, -212
addr0x00000fa4: lui x19, 593700
addr0x00000fa8: addi x12, x31, 1364
addr0x00000fac: bgeu x11, x17, addr0x0000097c
addr0x00000fb0: jal x31, addr0x000007e4
addr0x00000fb4: lw x28, 380(x8)
addr0x00000fb8: sw x2, 988(x8)
addr0x00000fbc: jal x4, addr0x0000099c
addr0x00000fc0: lw x18, -228(x8)
addr0x00000fc4: lw x5, -1934(x8)
addr0x00000fc8: addi x7, x12, -2
addr0x00000fcc: addi x17, x19, 1111
addr0x00000fd0: lui x17, 610205
addr0x00000fd4: beq x0, x4, addr0x00001164
addr0x00000fd8: sw x17, 804(x8)
addr0x00000fdc: lbu x5, 1585(x8)
addr0x00000fe0: addi x18, x14, -1124
addr0x00000fe4: addi x17, x13, -1290
addr0x00000fe8: addi x10, x19, -628
addr0x00000fec: jal x4, addr0x00001794
addr0x00000ff0: lw x26, -1751(x8)
addr0x00000ff4: jal x20, addr0x0000074c
addr0x00000ff8: lw x12, 1474(x8)
addr0x00000ffc: add x12, x17, x30
addr0x00001000: beq x23, x25, addr0x00000a5c
addr0x00001004: jal x13, addr0x000009ec
addr0x00001008: addi x21, x13, 1303
addr0x0000100c: and x24, x31, x17
addr0x00001010: bltu x0, x14, addr0x00000844
addr0x00001014: addi x19, x26, 972
addr0x00001018: lw x10, 1504(x8)
addr0x0000101c: jal x24, addr0x00001994
addr0x00001020: sw x13, 1888(x8)
addr0x00001024: jal x10, addr0x00000408
addr0x00001028: sw x29, 1667(x8)
addr0x0000102c: jal x31, addr0x000006f8
addr0x00001030: lh x1, 1995(x8)
addr0x00001034: lw x10, 1260(x8)
addr0x00001038: bge x0, x14, addr0x00000b98
addr0x0000103c: lbu x21, -1303(x8)
addr0x00001040: addi x13, x12, 1177
addr0x00001044: lw x22, 991(x8)
addr0x00001048: jal x28, addr0x00000080
addr0x0000104c: jal x9, addr0x000013cc
addr0x00001050: lw x22, -217(x8)
addr0x00001054: add x22, x9, x29
addr0x00001058: jal x31, addr0x000017ec
addr0x0000105c: addi x9, x29, 928
addr0x00001060: sw x7, 577(x8)
addr0x00001064: lw x22, -896(x8)
addr0x00001068: addi x0, x19, -1605
addr0x0000106c: bne x0, x2, addr0x00001468
addr0x00001070: lw x30, 919(x8)
addr0x00001074: lw x0, 1406(x8)
addr0x00001078: lw x25, -1404(x8)
addr0x0000107c: lw x9, -1849(x8)
addr0x00001080: add x28, x2, x10
addr0x00001084: lw x24, -1548(x8)
addr0x00001088: lw x19, -1733(x8)
addr0x0000108c: addi x29, x0, -523
addr0x00001090: lw x13, 1387(x8)
addr0x00001094: addi x12, x13, -542
addr0x00001098: jal x7, addr0x0000018c
addr0x0000109c: addi x1, x22, -1076
addr0x000010a0: sw x23, 961(x8)
addr0x000010a4: addi x15, x0, -1353
addr0x000010a8: jal x0, addr0x00001378
addr0x000010ac: lw x5, -262(x8)
addr0x000010b0: addi x22, x20, 65
addr0x000010b4: addi x30, x17, 87
addr0x000010b8: sw x16, 280(x8)
addr0x000010bc: addi x4, x30, 931
addr0x000010c0: lw x22, -395(x8)
addr0x000010c4: lbu x26, -2037(x8)
addr0x000010c8: jal x6, addr0x00000c34
addr0x000010cc: sw x19, -1730(x8)
addr0x000010d0: sw x0, -1056(x8)
addr0x000010d4: add x22, x16, x30
addr0x000010d8: lw x25, 449(x8)
addr0x000010dc: sw x26, 15(x8)
addr0x000010e0: sw x22, 1956(x8)
addr0x000010e4: jal x9, addr0x000011d0
addr0x000010e8: jal x29, addr0x00000d98
addr0x000010ec: lw x14, -1408(x8)
addr0x000010f0: lw x14, 425(x8)
addr0x000010f4: bge x4, x0, addr0x00001000
addr0x000010f8: sh x13, 1859(x8)
addr0x000010fc: bne x12, x7, addr0x00001194
addr0x00001100: sw x15, 633(x8)
addr0x00001104: sw x5, 1190(x8)
addr0x00001108: sw x17, -439(x8)
addr0x0000110c: sw x7, -1487(x8)
addr0x00001110: andi x30, x7, -236
addr0x00001114: sb x19, 312(x8)
addr0x00001118: lw x29, 786(x8)
addr0x0000111c: addi x7, x28, -540
addr0x00001120: sw x0, -525(x8)
addr0x00001124: jal x9, addr0x000012bc
addr0x00001128: jal x31, addr0x000015d8
addr0x0000112c: lw x13, -295(x8)
addr0x00001130: beq x28, x12, addr0x000009a0
addr0x00001134: lw x17, 1780(x8)
addr0x00001138: sw x7, 47(x8)
addr0x0000113c: jal x10, addr0x00000a84
addr0x00001140: lw x4, 793(x8)
addr0x00001144: addi x25, x22, -1393
addr0x00001148: sw x0, 1996(x8)
addr0x0000114c: jal x5, addr0x00000010
addr0x00001150: lw x22, -702(x8)
addr0x00001154: addi x6, x22, -1109
addr0x00001158: bgeu x0, x31, addr0x000012b8
addr0x0000115c: sw x27, 756(x8)
addr0x00001160: jal x27, addr0x00001004
addr0x00001164: lw x5, 1129(x8)
addr0x00001168: lw x18, 34(x8)
addr0x0000116c: jal x12, addr0x00000b00
addr0x00001170: jal x11, addr0x00001164
addr0x00001174: lw x17, -314(x8)
addr0x00001178: jal x0, addr0x00000314
addr0x0000117c: sb x20, 1090(x8)
addr0x00001180: sw x19, -655(x8)
addr0x00001184: jal x25, addr0x000003c0
addr0x00001188: lui x30, 391830
addr0x0000118c: lw x31, 2033(x8)
addr0x00001190: jalr x0, 248(x3)
addr0x00001194: lw x9, 277(x8)
addr0x00001198: sw x13, -1152(x8)
addr0x0000119c: lw x5, -1859(x8)
addr0x000011a0: addi x14, x6, -1714
addr0x000011a4: addi x5, x23, 229
addr0x000011a8: addi x25, x11, 1341
addr0x000011ac: beq x17, x5, addr0x00001840
addr0x000011b0: sw x0, -1379(x8)
addr0x000011b4: lw x22, 1540(x8)
addr0x000011b8: bne x24, x5, addr0x00000f8c
addr0x000011bc: addi x18, x9, 1300
addr0x000011c0: sw x4, 10(x8)
addr0x000011c4: lw x2, 560(x8)
addr0x000011c8: add x27, x13, x31
addr0x000011cc: sltu x20, x23, x7
addr0x000011d0: and x0, x19, x27
addr0x000011d4: jal x19, addr0x00001694
addr0x000011d8: lw x5, -61(x8)
addr0x000011dc: lw x19, 580(x8)
addr0x000011e0: lw x19, 872(x8)
addr0x000011e4: lw x23, 1459(x8)
addr0x000011e8: add x4, x20, x0
addr0x000011ec: jal x13, addr0x00001708
addr0x000011f0: lw x7, -802(x8)
addr0x000011f4: sw x17, -1257(x8)
addr0x000011f8: sw x5, 1753(x8)
addr0x000011fc: addi x22, x24, 1366
addr0x00001200: jal x22, addr0x000001b4
addr0x00001204: lw x20, -1912(x8)
addr0x00001208: lw x5, 98(x8)
addr0x0000120c: addi x24, x28, 458
addr0x00001210: blt x5, x12, addr0x000018d8
addr0x00001214: sw x30, 1809(x8)
addr0x00001218: or x10, x26, x17
addr0x0000121c: lbu x5, -704(x8)
addr0x00001220: jal x26, addr0x00000d6c
addr0x00001224: beq x25, x23, addr0x000010fc
addr0x00001228: lw x11, -207(x8)
addr0x0000122c: lw x27, -1699(x8)
addr0x00001230: lbu x22, -1278(x8)
addr0x00001234: lw x18, -39(x8)
addr0x00001238: lw x0, 390(x8)
addr0x0000123c: addi x23, x23, 1027
addr0x00001240: sw x17, 1840(x8)
addr0x00001244: addi x29, x19, 325
addr0x00001248: sw x20, 693(x8)
addr0x0000124c: beq x13, x31, addr0x00000e7c
addr0x00001250: bge x14, x30, addr0x00000ae8
addr0x00001254: sub x13, x26, x0
addr0x00001258: addi x13, x13, 1809
addr0x0000125c: sb x23, -1042(x8)
addr0x00001260: sw x0, 1802(x8)
addr0x00001264: bne x9, x7, addr0x0000113c
addr0x00001268: sb x19, -1733(x8)
addr0x0000126c: lbu x14, 860(x8)
addr0x00001270: lbu x20, 1535(x8)
addr0x00001274: slli x27, x18, 5
addr0x00001278: bgeu x0, x2, addr0x000014a8
addr0x0000127c: jal x1, addr0x000006e0
addr0x00001280: lw x30, -876(x8)
addr0x00001284: lw x31, -1165(x8)
addr0x00001288: sw x9, -875(x8)
addr0x0000128c: addi x19, x7, -1473
addr0x00001290: slli x31, x26, 21
addr0x00001294: and x7, x28, x27
addr0x00001298: addi x31, x27, 302
addr0x0000129c: sw x22, 557(x8)
addr0x000012a0: add x7, x4, x14
addr0x000012a4: bltu x20, x19, addr0x0000115c
addr0x000012a8: addi x11, x17, -1473
addr0x000012ac: addi x31, x23, -788
addr0x000012b0: sb x11, -565(x8)
addr0x000012b4: addi x28, x19, -948
addr0x000012b8: sw x1, 648(x8)
addr0x000012bc: addi x4, x29, -121
addr0x000012c0: addi x21, x23, -644
addr0x000012c4: lw x30, -1655(x8)
addr0x000012c8: sw x27, -1748(x8)
addr0x000012cc: bgeu x14, x19, addr0x00000e10
addr0x000012d0: addi x27, x31, 1347
addr0x000012d4: sw x26, 1922(x8)
addr0x000012d8: sw x29, -66(x8)
addr0x000012dc: bne x22, x30, addr0x000019d0
addr0x000012e0: lbu x19, 1819(x8)
addr0x000012e4: bne x5, x6, addr0x00000f00
addr0x000012e8: lw x7, -309(x8)
addr0x000012ec: sw x2, -1068(x8)
addr0x000012f0: addi x15, x16, 1426
addr0x000012f4: add x7, x16, x31
addr0x000012f8: sw x7, -416(x8)
addr0x000012fc: sw x10, -1807(x8)
addr0x00001300: sw x9, 1153(x8)
addr0x00001304: sw x19, -1205(x8)
addr0x00001308: addi x30, x25, -1308
addr0x0000130c: jal x10, addr0x00000d74
addr0x00001310: jal x10, addr0x00000df0
addr0x00001314: lw x22, -304(x8)
addr0x00001318: add x17, x14, x5
addr0x0000131c: blt x23, x18, addr0x00001404
addr0x00001320: bgeu x1, x23, addr0x0000179c
addr0x00001324: jal x31, addr0x00000650
addr0x00001328: addi x24, x6, -1254
addr0x0000132c: addi x20, x18, -983
addr0x00001330: sw x27, -520(x8)
addr0x00001334: sw x20, -84(x8)
addr0x00001338: sw x19, -1801(x8)
addr0x0000133c: addi x9, x26, -1310
addr0x00001340: lw x14, 2018(x8)
addr0x00001344: lw x27, -969(x8)
addr0x00001348: addi x0, x28, -1089
addr0x0000134c: jal x15, addr0x000006d4
addr0x00001350: lw x17, 1458(x8)
addr0x00001354: jal x20, addr0x0000016c
addr0x00001358: jal x13, addr0x00000dd8
addr0x0000135c: lw x20, -1688(x8)
addr0x00001360: sw x15, 1125(x8)
addr0x00001364: sw x22, 1894(x8)
addr0x00001368: sw x1, 899(x8)
addr0x0000136c: sw x7, -1370(x8)
addr0x00001370: sw x5, -854(x8)
addr0x00001374: jal x22, addr0x000006c4
addr0x00001378: sw x10, -1166(x8)
addr0x0000137c: lw x20, 622(x8)
addr0x00001380: beq x19, x31, addr0x00000cec
addr0x00001384: lw x17, 314(x8)
addr0x00001388: lw x27, -2042(x8)
addr0x0000138c: addi x2, x7, 1746
addr0x00001390: addi x22, x21, -1943
addr0x00001394: jal x27, addr0x000015c0
addr0x00001398: lw x15, -642(x8)
addr0x0000139c: lw x28, -327(x8)
addr0x000013a0: sw x19, 735(x8)
addr0x000013a4: lw x17, 1775(x8)
addr0x000013a8: lw x10, -1561(x8)
addr0x000013ac: lw x14, 911(x8)
addr0x000013b0: addi x26, x14, 1977
addr0x000013b4: sw x20, 261(x8)
addr0x000013b8: sw x1, -778(x8)
addr0x000013bc: jal x13, addr0x00000abc
addr0x000013c0: andi x4, x0, 696
addr0x000013c4: jal x22, addr0x00000dd8
addr0x000013c8: lw x29, 882(x8)
addr0x000013cc: addi x25, x23, 1975
addr0x000013d0: lw x23, 1594(x8)
addr0x000013d4: jal x0, addr0x00001788
addr0x000013d8: lw x4, 316(x8)
addr0x000013dc: sh x0, -741(x8)
addr0x000013e0: lw x9, 500(x8)
addr0x000013e4: addi x21, x27, 1462
addr0x000013e8: sw x1, -348(x8)
addr0x000013ec: sw x23, -1042(x8)
addr0x000013f0: sw x28, -1448(x8)
addr0x000013f4: addi x27, x31, -1066
addr0x000013f8: addi x5, x19, 167
addr0x000013fc: bne x20, x29, addr0x000016c8
addr0x00001400: andi x13, x31, -1984
addr0x00001404: bltu x9, x2, addr0x00001a24
addr0x00001408: addi x23, x29, 1903
addr0x0000140c: sw x10, 1368(x8)
addr0x00001410: addi x31, x22, 1960
addr0x00001414: jal x21, addr0x000004ac
addr0x00001418: jal x17, addr0x000017ec
addr0x0000141c: lw x0, -390(x8)
addr0x00001420: sw x26, -1284(x8)
addr0x00001424: lw x19, 1689(x8)
addr0x00001428: andi x5, x11, 1843
addr0x0000142c: bne x15, x10, addr0x000016fc
addr0x00001430: lw x19, 342(x8)
addr0x00001434: lw x24, -347(x8)
addr0x00001438: jal x4, addr0x0000021c
addr0x0000143c: lbu x0, -1266(x8)
addr0x00001440: add x24, x21, x14
addr0x00001444: lw x9, -1396(x8)
addr0x00001448: lw x22, 1292(x8)
addr0x0000144c: addi x23, x23, 1612
addr0x00001450: beq x9, x13, addr0x00000ff0
addr0x00001454: lw x14, -1902(x8)
addr0x00001458: sw x27, -986(x8)
addr0x0000145c: sw x2, -280(x8)
addr0x00001460: andi x21, x17, -565
addr0x00001464: addi x9, x21, -505
addr0x00001468: blt x5, x19, addr0x00001160
addr0x0000146c: lw x19, -877(x8)
addr0x00001470: bgeu x25, x30, addr0x00001744
addr0x00001474: lw x31, -1665(x8)
addr0x00001478: lw x9, 2025(x8)
addr0x0000147c: lw x19, -532(x8)
addr0x00001480: sw x1, -1495(x8)
addr0x00001484: sw x24, -681(x8)
addr0x00001488: sw x19, 1708(x8)
addr0x0000148c: addi x0, x0, 864
addr0x00001490: lw x29, -1668(x8)
addr0x00001494: sw x9, -519(x8)
addr0x00001498: sw x15, 427(x8)
addr0x0000149c: sw x22, 1426(x8)
addr0x000014a0: lw x16, 1757(x8)
addr0x000014a4: beq x7, x1, addr0x00000ffc
addr0x000014a8: addi x14, x28, -888
addr0x000014ac: addi x19, x25, 1461
addr0x000014b0: lw x23, 37(x8)
addr0x000014b4: lw x15, -67(x8)
addr0x000014b8: addi x20, x16, 541
addr0x000014bc: sh x25, -825(x8)
addr0x000014c0: sb x19, 2028(x8)
addr0x000014c4: lw x18, -1006(x8)
addr0x000014c8: beq x1, x19, addr0x000017c4
addr0x000014cc: addi x27, x30, 221
addr0x000014d0: addi x24, x28, 109
addr0x000014d4: lw x2, -1235(x8)
addr0x000014d8: jal x18, addr0x000017ec
addr0x000014dc: lw x31, 754(x8)
addr0x000014e0: lw x29, -1907(x8)
addr0x000014e4: add x19, x5, x19
addr0x000014e8: lui x31, 817286
addr0x000014ec: addi x13, x5, 207
addr0x000014f0: jal x19, addr0x00000c38
addr0x000014f4: remu x0, x28, x29
addr0x000014f8: addi x16, x13, 1781
addr0x000014fc: lw x31, 1259(x8)
addr0x00001500: sw x24, -1443(x8)
addr0x00001504: lui x20, 58058
addr0x00001508: addi x6, x17, -706
addr0x0000150c: jal x2, addr0x000013d4
addr0x00001510: lui x22, 755077
addr0x00001514: addi x17, x25, 1333
addr0x00001518: jal x19, addr0x000013cc
addr0x0000151c: sw x23, -935(x8)
addr0x00001520: jal x23, addr0x00001754
addr0x00001524: lw x23, 1509(x8)
addr0x00001528: lw x20, -276(x8)
addr0x0000152c: lw x0, 178(x8)
addr0x00001530: addi x18, x23, -581
addr0x00001534: bne x13, x22, addr0x00000fac
addr0x00001538: jal x19, addr0x00000904
addr0x0000153c: lw x7, -1843(x8)
addr0x00001540: jal x12, addr0x00001664
addr0x00001544: sw x7, 1926(x8)
addr0x00001548: addi x28, x12, -595
addr0x0000154c: bne x5, x23, addr0x000019ec
addr0x00001550: lui x19, 276019
addr0x00001554: addi x29, x7, 568
addr0x00001558: auipc x25, 999220
addr0x0000155c: jalr x0, 528(x3)
addr0x00001560: beq x0, x14, addr0x00001424
addr0x00001564: lw x27, -277(x8)
addr0x00001568: divu x29, x12, x19
addr0x0000156c: slli x7, x14, 23
addr0x00001570: add x29, x27, x2
addr0x00001574: bne x0, x6, addr0x000018a0
addr0x00001578: lbu x12, -341(x8)
addr0x0000157c: jal x23, addr0x00000a4c
addr0x00001580: lw x13, 1011(x8)
addr0x00001584: lw x16, 511(x8)
addr0x00001588: lhu x0, -1721(x8)
addr0x0000158c: addi x18, x23, 242
addr0x00001590: lw x1, 1398(x8)
addr0x00001594: lw x21, 1094(x8)
addr0x00001598: jal x13, addr0x00001350
addr0x0000159c: addi x19, x31, -1538
addr0x000015a0: sub x31, x19, x21
addr0x000015a4: sub x31, x0, x12
addr0x000015a8: lw x14, 1813(x8)
addr0x000015ac: addi x15, x20, -1059
addr0x000015b0: lhu x22, -1796(x8)
addr0x000015b4: lui x15, 601082
addr0x000015b8: sw x12, 1242(x8)
addr0x000015bc: sw x23, 676(x8)
addr0x000015c0: bltu x6, x26, addr0x00001548
addr0x000015c4: bge x6, x23, addr0x00000e18
addr0x000015c8: sw x0, 1184(x8)
addr0x000015cc: sw x5, 1445(x8)
addr0x000015d0: sw x30, 897(x8)
addr0x000015d4: lw x5, -435(x8)
addr0x000015d8: jal x5, addr0x000017a8
addr0x000015dc: lw x1, -208(x8)
addr0x000015e0: sb x4, 1294(x8)
addr0x000015e4: sw x20, 1834(x8)
addr0x000015e8: add x15, x1, x24
addr0x000015ec: addi x20, x1, 334
addr0x000015f0: lw x14, -1961(x8)
addr0x000015f4: lw x20, 149(x8)
addr0x000015f8: addi x0, x24, -268
addr0x000015fc: addi x20, x0, 1263
addr0x00001600: jal x0, addr0x000017b4
addr0x00001604: sw x2, 848(x8)
addr0x00001608: lw x26, 609(x8)
addr0x0000160c: jal x30, addr0x00000bec
addr0x00001610: lw x6, 54(x8)
addr0x00001614: lh x24, -1147(x8)
addr0x00001618: sb x4, 106(x8)
addr0x0000161c: andi x5, x20, 492
addr0x00001620: add x25, x19, x11
addr0x00001624: jal x31, addr0x000000e0
addr0x00001628: addi x13, x19, -994
addr0x0000162c: bge x25, x23, addr0x00000e48
addr0x00001630: sb x19, -1904(x8)
addr0x00001634: lbu x19, -200(x8)
addr0x00001638: sw x17, -444(x8)
addr0x0000163c: addi x7, x9, 1117
addr0x00001640: sw x31, -1532(x8)
addr0x00001644: jal x27, addr0x0000016c
addr0x00001648: sw x23, 608(x8)
addr0x0000164c: sw x23, 77(x8)
addr0x00001650: jal x27, addr0x00000e6c
addr0x00001654: lw x9, 1950(x8)
addr0x00001658: addi x13, x27, 441
addr0x0000165c: sub x4, x29, x28
addr0x00001660: lbu x24, -1340(x8)
addr0x00001664: lbu x26, 1856(x8)
addr0x00001668: sw x17, 87(x8)
addr0x0000166c: sw x7, -205(x8)
addr0x00001670: lui x12, 953364
addr0x00001674: addi x1, x17, -468
addr0x00001678: beq x16, x17, addr0x0000121c
addr0x0000167c: bne x2, x10, addr0x00001010
addr0x00001680: lw x13, 851(x8)
addr0x00001684: lw x18, 2045(x8)
addr0x00001688: bgeu x13, x1, addr0x000017f4
addr0x0000168c: sub x22, x19, x13
addr0x00001690: sub x0, x2, x29
addr0x00001694: sub x31, x17, x11
addr0x00001698: bgeu x17, x7, addr0x00001100
addr0x0000169c: slli x21, x29, 14
addr0x000016a0: bne x7, x17, addr0x0000120c
addr0x000016a4: lw x4, 1469(x8)
addr0x000016a8: addi x14, x29, -353
addr0x000016ac: sb x1, 1629(x8)
addr0x000016b0: lhu x30, 481(x8)
addr0x000016b4: addi x4, x1, -1533
addr0x000016b8: jal x2, addr0x00000204
addr0x000016bc: lui x7, 71969
addr0x000016c0: andi x26, x23, 1851
addr0x000016c4: lbu x28, 52(x8)
addr0x000016c8: beq x0, x11, addr0x00001370
addr0x000016cc: sw x17, -646(x8)
addr0x000016d0: sw x9, 1534(x8)
addr0x000016d4: sw x7, 374(x8)
addr0x000016d8: sw x18, 913(x8)
addr0x000016dc: sw x19, -29(x8)
addr0x000016e0: lw x16, 984(x8)
addr0x000016e4: lw x9, 373(x8)
addr0x000016e8: addi x4, x28, -324
addr0x000016ec: sub x15, x29, x6
addr0x000016f0: lui x16, 877778
addr0x000016f4: sw x9, 1530(x8)
addr0x000016f8: jal x22, addr0x000010bc
addr0x000016fc: lw x7, -1274(x8)
addr0x00001700: lw x10, -1434(x8)
addr0x00001704: lw x20, 1505(x8)
addr0x00001708: lw x31, -438(x8)
addr0x0000170c: lw x20, -368(x8)
addr0x00001710: lw x19, 1609(x8)
addr0x00001714: add x25, x7, x7
addr0x00001718: jal x29, addr0x00000650
addr0x0000171c: lw x9, 411(x8)
addr0x00001720: sw x15, -520(x8)
addr0x00001724: lw x11, 925(x8)
addr0x00001728: lui x22, 57263
addr0x0000172c: addi x24, x18, 554
addr0x00001730: sw x5, 971(x8)
addr0x00001734: sw x0, 810(x8)
addr0x00001738: sw x2, 1276(x8)
addr0x0000173c: andi x21, x13, 806
addr0x00001740: lw x31, 1563(x8)
addr0x00001744: lw x25, -1235(x8)
addr0x00001748: jal x30, addr0x00000374
addr0x0000174c: lw x27, 1495(x8)
addr0x00001750: sh x17, 1173(x8)
addr0x00001754: jal x20, addr0x000000f4
addr0x00001758: lw x22, -951(x8)
addr0x0000175c: or x19, x30, x25
addr0x00001760: sh x7, 824(x8)
addr0x00001764: sh x9, -1292(x8)
addr0x00001768: addi x5, x27, -873
addr0x0000176c: lui x1, 973288
addr0x00001770: andi x15, x14, -591
addr0x00001774: addi x30, x14, -1226
addr0x00001778: auipc x13, 16011
addr0x0000177c: addi x31, x9, 1957
addr0x00001780: bge x13, x9, addr0x0000146c
addr0x00001784: lui x14, 132184
addr0x00001788: addi x17, x25, -574
addr0x0000178c: sw x9, -18(x8)
addr0x00001790: addi x5, x29, -798
addr0x00001794: sw x28, 1860(x8)
addr0x00001798: sw x31, -668(x8)
addr0x0000179c: jal x26, addr0x00000254
addr0x000017a0: jal x17, addr0x00000798
addr0x000017a4: lw x13, 1767(x8)
addr0x000017a8: sub x6, x1, x19
addr0x000017ac: lui x13, 42694
addr0x000017b0: sb x22, 102(x8)
addr0x000017b4: sb x5, 1955(x8)
addr0x000017b8: sw x2, -664(x8)
addr0x000017bc: addi x7, x30, 319
addr0x000017c0: lw x19, -1784(x8)
addr0x000017c4: lw x25, 1313(x8)
addr0x000017c8: lw x6, 533(x8)
addr0x000017cc: addi x22, x18, -1938
addr0x000017d0: lw x18, 142(x8)
addr0x000017d4: lw x17, 400(x8)
addr0x000017d8: lw x23, -191(x8)
addr0x000017dc: addi x27, x29, -530
addr0x000017e0: lui x29, 1008251
addr0x000017e4: sw x20, 1114(x8)
addr0x000017e8: jal x6, addr0x000010f8
addr0x000017ec: lw x0, -177(x8)
addr0x000017f0: lw x21, 1262(x8)
addr0x000017f4: addi x30, x13, -38
addr0x000017f8: addi x2, x19, -1273
addr0x000017fc: lw x6, -1562(x8)
addr0x00001800: lw x6, 1088(x8)
addr0x00001804: addi x19, x19, 1003
addr0x00001808: jal x23, addr0x00001614
addr0x0000180c: lw x21, -1072(x8)
addr0x00001810: beq x23, x19, addr0x000017d8
addr0x00001814: sw x23, -282(x8)
addr0x00001818: sw x28, -457(x8)
addr0x0000181c: sw x13, 1002(x8)
addr0x00001820: sw x29, -161(x8)
addr0x00001824: lw x27, 416(x8)
addr0x00001828: lw x23, 669(x8)
addr0x0000182c: addi x1, x30, -1435
addr0x00001830: jal x31, addr0x000004ec
addr0x00001834: lbu x28, 1609(x8)
addr0x00001838: lbu x13, 1328(x8)
addr0x0000183c: bltu x19, x0, addr0x00001680
addr0x00001840: beq x6, x22, addr0x000013a4
addr0x00001844: addi x12, x22, -1325
addr0x00001848: sw x15, 1015(x8)
addr0x0000184c: sw x28, 105(x8)
addr0x00001850: lw x14, 1779(x8)
addr0x00001854: lw x7, 1463(x8)
addr0x00001858: lw x12, 929(x8)
addr0x0000185c: lw x27, -322(x8)
addr0x00001860: lw x22, -1332(x8)
addr0x00001864: jal x11, addr0x00001980
addr0x00001868: sw x27, 929(x8)
addr0x0000186c: sw x9, -1672(x8)
addr0x00001870: lw x24, 1669(x8)
addr0x00001874: sltu x17, x31, x25
addr0x00001878: or x14, x10, x27
addr0x0000187c: lw x22, -124(x8)
addr0x00001880: sw x27, -882(x8)
addr0x00001884: lw x20, -1956(x8)
addr0x00001888: lbu x7, 774(x8)
addr0x0000188c: add x18, x24, x21
addr0x00001890: sub x20, x0, x17
addr0x00001894: andi x13, x23, -1345
addr0x00001898: jal x13, addr0x00001040
addr0x0000189c: lw x9, 1696(x8)
addr0x000018a0: jal x10, addr0x000006f8
addr0x000018a4: lw x19, 1243(x8)
addr0x000018a8: add x26, x21, x0
addr0x000018ac: jal x22, addr0x00001a24
addr0x000018b0: lhu x15, 833(x8)
addr0x000018b4: sw x13, 660(x8)
addr0x000018b8: sw x19, 1865(x8)
addr0x000018bc: addi x1, x9, -1525
addr0x000018c0: sw x27, 1676(x8)
addr0x000018c4: lw x23, 1755(x8)
addr0x000018c8: lw x13, 1713(x8)
addr0x000018cc: lw x17, 1473(x8)
addr0x000018d0: lw x2, 660(x8)
addr0x000018d4: addi x22, x1, -98
addr0x000018d8: sw x29, 790(x8)
addr0x000018dc: jal x30, addr0x000011ec
addr0x000018e0: jal x14, addr0x00001520
addr0x000018e4: lw x12, 1058(x8)
addr0x000018e8: lw x25, -1679(x8)
addr0x000018ec: add x19, x7, x0
addr0x000018f0: addi x17, x18, 752
addr0x000018f4: sw x10, -365(x8)
addr0x000018f8: lbu x4, 258(x8)
addr0x000018fc: andi x19, x20, 121
addr0x00001900: jal x30, addr0x00001130
addr0x00001904: jal x5, addr0x000003b8
addr0x00001908: lw x5, 1925(x8)
addr0x0000190c: bltu x9, x26, addr0x00001418
addr0x00001910: jal x0, addr0x00000a24
addr0x00001914: jal x12, addr0x000009f8
addr0x00001918: addi x22, x27, 1427
addr0x0000191c: addi x2, x22, 1901
addr0x00001920: jal x1, addr0x000016ec
addr0x00001924: addi x1, x7, -762
addr0x00001928: lui x23, 526
addr0x0000192c: addi x9, x29, 15
addr0x00001930: lw x27, 1717(x8)
addr0x00001934: lw x15, -1659(x8)
addr0x00001938: slli x9, x19, 0
addr0x0000193c: bne x4, x19, addr0x00001740
addr0x00001940: addi x24, x1, -1969
addr0x00001944: sw x9, -572(x8)
addr0x00001948: sw x27, -1994(x8)
addr0x0000194c: sw x15, 1172(x8)
addr0x00001950: ori x10, x10, -1647
addr0x00001954: sw x18, -1430(x8)
addr0x00001958: bne x1, x24, addr0x000012d8
addr0x0000195c: lbu x13, 434(x8)
addr0x00001960: sub x17, x22, x23
addr0x00001964: lui x30, 885074
addr0x00001968: addi x13, x31, 1685
addr0x0000196c: sw x13, 1445(x8)
addr0x00001970: sw x18, 529(x8)
addr0x00001974: addi x1, x7, 150
addr0x00001978: jal x23, addr0x000010a0
addr0x0000197c: jal x16, addr0x00000974
addr0x00001980: addi x25, x19, -1512
addr0x00001984: sw x18, -1704(x8)
addr0x00001988: addi x7, x23, 749
addr0x0000198c: slli x18, x18, 26
addr0x00001990: lw x17, 1219(x8)
addr0x00001994: addi x20, x30, 1039
addr0x00001998: addi x19, x0, 718
addr0x0000199c: jal x19, addr0x00000008
addr0x000019a0: sw x7, -2015(x8)
addr0x000019a4: sw x29, -442(x8)
addr0x000019a8: sw x20, 688(x8)
addr0x000019ac: jal x11, addr0x00000394
addr0x000019b0: lw x29, 241(x8)
addr0x000019b4: beq x0, x10, addr0x00001578
addr0x000019b8: bgeu x17, x2, addr0x00001870
addr0x000019bc: lw x0, -1239(x8)
addr0x000019c0: lw x25, 1750(x8)
addr0x000019c4: sw x22, -733(x8)
addr0x000019c8: jal x12, addr0x00001848
addr0x000019cc: lw x4, -452(x8)
addr0x000019d0: sw x12, -1471(x8)
addr0x000019d4: lw x1, -729(x8)
addr0x000019d8: lw x13, 1903(x8)
addr0x000019dc: sw x20, -318(x8)
addr0x000019e0: sw x13, 939(x8)
addr0x000019e4: addi x15, x9, -537
addr0x000019e8: sw x2, -1814(x8)
addr0x000019ec: addi x11, x9, 500
addr0x000019f0: add x6, x20, x19
addr0x000019f4: sw x18, 2008(x8)
addr0x000019f8: lw x0, 1205(x8)
addr0x000019fc: addi x0, x7, 297
addr0x00001a00: addi x13, x28, -980
addr0x00001a04: jal x7, addr0x000019cc
addr0x00001a08: addi x11, x13, 921
addr0x00001a0c: jal x7, addr0x00000b48
addr0x00001a10: lw x22, 1136(x8)
addr0x00001a14: jal x27, addr0x00000eac
addr0x00001a18: lw x9, 156(x8)
addr0x00001a1c: lw x0, 1342(x8)
addr0x00001a20: jal x22, addr0x000006cc
addr0x00001a24: jalr x0, 976(x3)
addr0x00001a28: sw x10, 586(x8)
addr0x00001a2c: lw x18, -48(x8)
addr0x00001a30: lw x14, -2040(x8)
addr0x00001a34: lw x31, 1195(x8)
addr0x00001a38: sb x11, 816(x8)
addr0x00001a3c: lw x19, -1402(x8)
addr0x00001a40: lw x12, 1689(x8)
addr0x00001a44: andi x5, x14, -372
