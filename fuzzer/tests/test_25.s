.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bne x4, x27, addr0x0000058c
addr0x0000001c: lw x27, -1884(x8)
addr0x00000020: sw x14, -1526(x8)
addr0x00000024: lw x30, 1796(x8)
addr0x00000028: jal x10, addr0x00000380
addr0x0000002c: lw x19, -1176(x8)
addr0x00000030: bne x22, x22, addr0x000005c8
addr0x00000034: addi x24, x17, 1545
addr0x00000038: sw x31, 440(x8)
addr0x0000003c: lw x20, -1923(x8)
addr0x00000040: sub x5, x19, x13
addr0x00000044: ori x19, x6, 1271
addr0x00000048: lbu x23, 407(x8)
addr0x0000004c: sw x25, -924(x8)
addr0x00000050: lui x24, 159568
addr0x00000054: lbu x30, 603(x8)
addr0x00000058: add x9, x15, x27
addr0x0000005c: ori x22, x0, 1702
addr0x00000060: add x0, x29, x18
addr0x00000064: lw x7, 36(x8)
addr0x00000068: sw x1, -2031(x8)
addr0x0000006c: beq x27, x29, addr0x00000638
addr0x00000070: sub x30, x27, x17
addr0x00000074: srai x27, x18, 8
addr0x00000078: sb x1, -374(x8)
addr0x0000007c: sb x19, 672(x8)
addr0x00000080: addi x30, x15, 898
addr0x00000084: jal x21, addr0x00000510
addr0x00000088: addi x13, x27, -194
addr0x0000008c: jal x29, addr0x00000514
addr0x00000090: lbu x30, 1550(x8)
addr0x00000094: lw x19, 426(x8)
addr0x00000098: lw x0, 478(x8)
addr0x0000009c: lw x25, 895(x8)
addr0x000000a0: lw x22, 1006(x8)
addr0x000000a4: addi x29, x14, -790
addr0x000000a8: bne x16, x17, addr0x000001f0
addr0x000000ac: bgeu x19, x19, addr0x00000674
addr0x000000b0: ori x11, x15, -672
addr0x000000b4: lw x22, 1117(x8)
addr0x000000b8: addi x7, x17, -376
addr0x000000bc: lw x21, 971(x8)
addr0x000000c0: jal x4, addr0x00000124
addr0x000000c4: lw x7, -1899(x8)
addr0x000000c8: lw x21, -1761(x8)
addr0x000000cc: lw x19, 1193(x8)
addr0x000000d0: add x6, x13, x30
addr0x000000d4: srli x6, x23, 26
addr0x000000d8: slli x1, x27, 21
addr0x000000dc: lw x9, 378(x8)
addr0x000000e0: sw x2, 637(x8)
addr0x000000e4: addi x23, x31, -1684
addr0x000000e8: jal x19, addr0x0000003c
addr0x000000ec: jal x30, addr0x00000538
addr0x000000f0: lw x17, 896(x8)
addr0x000000f4: addi x5, x18, -1361
addr0x000000f8: sw x2, 1773(x8)
addr0x000000fc: jal x18, addr0x000000d0
addr0x00000100: lw x25, -1974(x8)
addr0x00000104: lw x11, -377(x8)
addr0x00000108: jal x5, addr0x00000480
addr0x0000010c: lw x17, 464(x8)
addr0x00000110: lw x30, 916(x8)
addr0x00000114: sw x24, -507(x8)
addr0x00000118: jal x31, addr0x000004b4
addr0x0000011c: sll x9, x11, x22
addr0x00000120: or x5, x7, x14
addr0x00000124: lhu x6, 145(x8)
addr0x00000128: andi x21, x5, 1970
addr0x0000012c: lw x2, 1646(x8)
addr0x00000130: jal x13, addr0x000001b8
addr0x00000134: addi x17, x11, -345
addr0x00000138: lw x24, 788(x8)
addr0x0000013c: sw x23, 1508(x8)
addr0x00000140: addi x5, x2, -1623
addr0x00000144: sw x13, -1790(x8)
addr0x00000148: sw x7, 941(x8)
addr0x0000014c: jal x0, addr0x00000460
addr0x00000150: lui x0, 97277
addr0x00000154: addi x0, x9, 1194
addr0x00000158: jal x20, addr0x0000040c
addr0x0000015c: addi x22, x22, -1092
addr0x00000160: jalr x0, 772(x3)
addr0x00000164: jal x17, addr0x000002f4
addr0x00000168: lw x7, -788(x8)
addr0x0000016c: addi x21, x18, 1754
addr0x00000170: lw x6, -1331(x8)
addr0x00000174: or x13, x0, x0
addr0x00000178: sub x19, x27, x21
addr0x0000017c: lw x17, 1787(x8)
addr0x00000180: addi x23, x11, -408
addr0x00000184: addi x29, x31, 1524
addr0x00000188: jal x13, addr0x00000030
addr0x0000018c: bgeu x22, x20, addr0x000003c0
addr0x00000190: addi x20, x19, -131
addr0x00000194: jal x27, addr0x00000308
addr0x00000198: lw x5, -1291(x8)
addr0x0000019c: jal x22, addr0x00000444
addr0x000001a0: lw x31, -50(x8)
addr0x000001a4: jal x27, addr0x0000003c
addr0x000001a8: jal x12, addr0x00000460
addr0x000001ac: bne x10, x20, addr0x000004f4
addr0x000001b0: addi x23, x18, -1319
addr0x000001b4: jal x1, addr0x0000040c
addr0x000001b8: lw x13, 327(x8)
addr0x000001bc: lw x23, -985(x8)
addr0x000001c0: lw x27, -1778(x8)
addr0x000001c4: addi x30, x0, -1550
addr0x000001c8: andi x17, x19, -99
addr0x000001cc: sub x19, x22, x7
addr0x000001d0: beq x27, x25, addr0x000001dc
addr0x000001d4: lw x18, -1169(x8)
addr0x000001d8: lw x9, 1043(x8)
addr0x000001dc: lw x17, -1802(x8)
addr0x000001e0: lw x16, -205(x8)
addr0x000001e4: lw x4, 1503(x8)
addr0x000001e8: lw x7, 2003(x8)
addr0x000001ec: lw x19, 242(x8)
addr0x000001f0: bne x19, x19, addr0x000000dc
addr0x000001f4: lw x22, -54(x8)
addr0x000001f8: add x6, x14, x14
addr0x000001fc: jal x20, addr0x00000068
addr0x00000200: lw x9, 382(x8)
addr0x00000204: remu x14, x13, x6
addr0x00000208: addi x23, x22, 264
addr0x0000020c: sw x23, -1659(x8)
addr0x00000210: sub x23, x7, x31
addr0x00000214: addi x5, x23, 389
addr0x00000218: sw x2, -299(x8)
addr0x0000021c: sh x20, -1806(x8)
addr0x00000220: sw x20, -199(x8)
addr0x00000224: jal x20, addr0x000002a4
addr0x00000228: lw x23, 813(x8)
addr0x0000022c: add x14, x17, x19
addr0x00000230: add x6, x19, x13
addr0x00000234: jal x25, addr0x000004c4
addr0x00000238: jal x10, addr0x000004f0
addr0x0000023c: sw x12, -252(x8)
addr0x00000240: lw x1, -234(x8)
addr0x00000244: lw x7, 398(x8)
addr0x00000248: slli x28, x7, 30
addr0x0000024c: or x26, x28, x17
addr0x00000250: sw x6, 544(x8)
addr0x00000254: jal x18, addr0x000002f8
addr0x00000258: jal x9, addr0x00000148
addr0x0000025c: lw x15, -1771(x8)
addr0x00000260: lw x19, 2014(x8)
addr0x00000264: beq x22, x28, addr0x00000254
addr0x00000268: jal x25, addr0x00000270
addr0x0000026c: lw x22, 781(x8)
addr0x00000270: jal x25, addr0x00000648
addr0x00000274: lw x13, -565(x8)
addr0x00000278: and x29, x14, x30
addr0x0000027c: sw x31, 260(x8)
addr0x00000280: jal x25, addr0x00000068
addr0x00000284: lw x4, -1668(x8)
addr0x00000288: lw x27, 941(x8)
addr0x0000028c: add x27, x20, x24
addr0x00000290: sb x22, -1994(x8)
addr0x00000294: lw x29, -1950(x8)
addr0x00000298: lbu x16, 1074(x8)
addr0x0000029c: addi x17, x11, -588
addr0x000002a0: add x14, x18, x9
addr0x000002a4: jal x5, addr0x000003fc
addr0x000002a8: sw x13, -200(x8)
addr0x000002ac: sw x29, 1703(x8)
addr0x000002b0: lw x25, 1846(x8)
addr0x000002b4: sw x22, 472(x8)
addr0x000002b8: addi x24, x12, -429
addr0x000002bc: sw x27, 1391(x8)
addr0x000002c0: andi x16, x18, 1395
addr0x000002c4: lbu x25, -1835(x8)
addr0x000002c8: lw x19, 348(x8)
addr0x000002cc: lw x23, 1804(x8)
addr0x000002d0: lw x29, -344(x8)
addr0x000002d4: addi x27, x19, -1915
addr0x000002d8: lw x7, -177(x8)
addr0x000002dc: sub x23, x24, x13
addr0x000002e0: jal x16, addr0x00000134
addr0x000002e4: lw x28, -593(x8)
addr0x000002e8: addi x20, x24, 1467
addr0x000002ec: lw x15, -248(x8)
addr0x000002f0: lw x31, 387(x8)
addr0x000002f4: addi x27, x31, -1052
addr0x000002f8: sw x0, 223(x8)
addr0x000002fc: sw x20, 982(x8)
addr0x00000300: sw x19, -1570(x8)
addr0x00000304: sw x20, 1506(x8)
addr0x00000308: lw x25, -227(x8)
addr0x0000030c: sw x30, 1226(x8)
addr0x00000310: jal x5, addr0x00000630
addr0x00000314: jal x12, addr0x000001d4
addr0x00000318: slli x19, x4, 22
addr0x0000031c: lui x2, 260962
addr0x00000320: addi x5, x14, -949
addr0x00000324: lw x19, -2037(x8)
addr0x00000328: lw x0, -401(x8)
addr0x0000032c: lui x29, 286327
addr0x00000330: addi x2, x7, -1943
addr0x00000334: lw x13, 101(x8)
addr0x00000338: lw x18, 500(x8)
addr0x0000033c: jal x22, addr0x00000204
addr0x00000340: sb x22, 1218(x8)
addr0x00000344: sub x19, x14, x1
addr0x00000348: lui x27, 1005500
addr0x0000034c: addi x9, x30, 352
addr0x00000350: jal x5, addr0x00000634
addr0x00000354: jal x4, addr0x00000358
addr0x00000358: lw x17, 144(x8)
addr0x0000035c: jal x30, addr0x00000134
addr0x00000360: bgeu x16, x19, addr0x0000026c
addr0x00000364: addi x2, x29, -24
addr0x00000368: addi x1, x19, 1686
addr0x0000036c: addi x30, x2, 940
addr0x00000370: sw x30, -125(x8)
addr0x00000374: lw x7, -84(x8)
addr0x00000378: lw x6, -938(x8)
addr0x0000037c: lw x19, -878(x8)
addr0x00000380: add x0, x28, x7
addr0x00000384: lbu x29, -1481(x8)
addr0x00000388: slli x9, x25, 19
addr0x0000038c: lui x24, 59948
addr0x00000390: lui x6, 885153
addr0x00000394: addi x10, x4, -1582
addr0x00000398: jal x7, addr0x00000050
addr0x0000039c: andi x17, x1, -1541
addr0x000003a0: lui x7, 952556
addr0x000003a4: addi x0, x5, -1984
addr0x000003a8: jal x14, addr0x00000310
addr0x000003ac: sw x20, -695(x8)
addr0x000003b0: lw x0, 610(x8)
addr0x000003b4: lw x9, 182(x8)
addr0x000003b8: lw x19, 1229(x8)
addr0x000003bc: add x22, x23, x2
addr0x000003c0: srli x13, x14, 14
addr0x000003c4: lw x24, 1127(x8)
addr0x000003c8: add x23, x1, x22
addr0x000003cc: jal x27, addr0x0000020c
addr0x000003d0: addi x5, x11, 117
addr0x000003d4: lw x12, 851(x8)
addr0x000003d8: bne x18, x16, addr0x00000534
addr0x000003dc: sub x1, x13, x4
addr0x000003e0: bgeu x19, x5, addr0x00000304
addr0x000003e4: addi x18, x23, -536
addr0x000003e8: slli x27, x15, 27
addr0x000003ec: beq x0, x5, addr0x000006ac
addr0x000003f0: lw x31, 174(x8)
addr0x000003f4: jal x24, addr0x00000348
addr0x000003f8: lw x14, -1477(x8)
addr0x000003fc: jal x18, addr0x0000032c
addr0x00000400: lw x1, -522(x8)
addr0x00000404: lw x22, -1770(x8)
addr0x00000408: bne x18, x13, addr0x00000324
addr0x0000040c: lw x22, 1343(x8)
addr0x00000410: sw x10, -1152(x8)
addr0x00000414: sub x24, x19, x13
addr0x00000418: sub x22, x12, x20
addr0x0000041c: lbu x7, -204(x8)
addr0x00000420: addi x29, x14, 686
addr0x00000424: jal x16, addr0x00000624
addr0x00000428: sw x29, -526(x8)
addr0x0000042c: addi x28, x17, 1514
addr0x00000430: sw x27, -1338(x8)
addr0x00000434: lw x19, 1521(x8)
addr0x00000438: sw x22, 1009(x8)
addr0x0000043c: lbu x29, 1065(x8)
addr0x00000440: jal x27, addr0x000003dc
addr0x00000444: sw x20, 1353(x8)
addr0x00000448: sw x13, 83(x8)
addr0x0000044c: lw x28, 95(x8)
addr0x00000450: lw x12, -1723(x8)
addr0x00000454: ori x23, x12, -1518
addr0x00000458: sw x15, 113(x8)
addr0x0000045c: sw x2, -892(x8)
addr0x00000460: sw x7, 1946(x8)
addr0x00000464: lw x0, 1706(x8)
addr0x00000468: lw x31, -921(x8)
addr0x0000046c: lw x4, -1317(x8)
addr0x00000470: jal x28, addr0x00000650
addr0x00000474: sw x11, -998(x8)
addr0x00000478: addi x23, x17, -1416
addr0x0000047c: jal x22, addr0x000003a8
addr0x00000480: lw x19, -1599(x8)
addr0x00000484: andi x1, x17, 1009
addr0x00000488: beq x22, x0, addr0x00000418
addr0x0000048c: addi x29, x24, 2040
addr0x00000490: lw x31, -166(x8)
addr0x00000494: jal x29, addr0x000000fc
addr0x00000498: sw x29, -2017(x8)
addr0x0000049c: sw x21, 1507(x8)
addr0x000004a0: sw x18, 966(x8)
addr0x000004a4: sub x29, x4, x24
addr0x000004a8: sw x5, -382(x8)
addr0x000004ac: sw x28, 52(x8)
addr0x000004b0: sb x22, -1187(x8)
addr0x000004b4: lhu x5, -22(x8)
addr0x000004b8: bge x21, x13, addr0x00000038
addr0x000004bc: addi x7, x22, -588
addr0x000004c0: addi x1, x25, 1998
addr0x000004c4: sub x23, x18, x19
addr0x000004c8: lw x13, 299(x8)
addr0x000004cc: lw x18, -1831(x8)
addr0x000004d0: addi x24, x4, -1786
addr0x000004d4: jal x21, addr0x00000640
addr0x000004d8: addi x5, x19, 459
addr0x000004dc: sw x4, -1854(x8)
addr0x000004e0: beq x2, x23, addr0x00000664
addr0x000004e4: lhu x31, 1380(x8)
addr0x000004e8: or x13, x20, x0
addr0x000004ec: andi x0, x0, 1207
addr0x000004f0: bge x29, x17, addr0x00000580
addr0x000004f4: add x29, x21, x25
addr0x000004f8: lui x27, 280674
addr0x000004fc: sw x0, 1263(x8)
addr0x00000500: sw x19, -1335(x8)
addr0x00000504: sw x5, -555(x8)
addr0x00000508: addi x1, x7, -2016
addr0x0000050c: sub x21, x4, x16
addr0x00000510: sub x27, x16, x21
addr0x00000514: bltu x11, x5, addr0x00000150
addr0x00000518: bne x19, x20, addr0x000003e4
addr0x0000051c: addi x4, x12, -1351
addr0x00000520: lw x7, 1813(x8)
addr0x00000524: jal x13, addr0x00000024
addr0x00000528: jal x24, addr0x00000340
addr0x0000052c: lw x4, -1187(x8)
addr0x00000530: lui x23, 940247
addr0x00000534: addi x17, x29, 658
addr0x00000538: blt x16, x29, addr0x00000218
addr0x0000053c: lw x0, -854(x8)
addr0x00000540: addi x23, x22, -1399
addr0x00000544: addi x4, x17, -391
addr0x00000548: jal x4, addr0x000003b4
addr0x0000054c: jal x11, addr0x00000194
addr0x00000550: addi x21, x27, -818
addr0x00000554: sw x19, 1783(x8)
addr0x00000558: sw x18, -24(x8)
addr0x0000055c: slli x30, x29, 30
addr0x00000560: add x27, x22, x5
addr0x00000564: sub x18, x23, x19
addr0x00000568: bgeu x7, x13, addr0x000006ac
addr0x0000056c: jal x27, addr0x00000340
addr0x00000570: sub x14, x29, x24
addr0x00000574: bgeu x9, x5, addr0x000002b8
addr0x00000578: lw x23, -910(x8)
addr0x0000057c: lbu x10, -1636(x8)
addr0x00000580: sw x5, -1226(x8)
addr0x00000584: sw x22, 874(x8)
addr0x00000588: lbu x27, 1926(x8)
addr0x0000058c: sw x18, -566(x8)
addr0x00000590: jal x23, addr0x0000010c
addr0x00000594: lw x21, -519(x8)
addr0x00000598: lw x27, 2013(x8)
addr0x0000059c: add x22, x2, x19
addr0x000005a0: sw x9, -264(x8)
addr0x000005a4: lw x22, -416(x8)
addr0x000005a8: sw x2, -1404(x8)
addr0x000005ac: sw x21, 291(x8)
addr0x000005b0: jal x7, addr0x0000007c
addr0x000005b4: lw x13, -425(x8)
addr0x000005b8: lw x24, 1793(x8)
addr0x000005bc: lw x25, -1231(x8)
addr0x000005c0: jal x23, addr0x0000037c
addr0x000005c4: sw x2, 1629(x8)
addr0x000005c8: addi x30, x5, -982
addr0x000005cc: jal x27, addr0x000005a8
addr0x000005d0: lw x14, 899(x8)
addr0x000005d4: sw x0, 223(x8)
addr0x000005d8: jal x20, addr0x000005d0
addr0x000005dc: lui x17, 212591
addr0x000005e0: addi x14, x19, -1192
addr0x000005e4: sw x6, -1059(x8)
addr0x000005e8: sw x27, 657(x8)
addr0x000005ec: blt x21, x19, addr0x00000614
addr0x000005f0: lw x13, 1222(x8)
addr0x000005f4: addi x11, x13, -1704
addr0x000005f8: addi x19, x19, 1655
addr0x000005fc: addi x0, x27, 1654
addr0x00000600: addi x17, x19, -1281
addr0x00000604: jal x5, addr0x0000032c
addr0x00000608: addi x6, x27, -1196
addr0x0000060c: andi x24, x28, 812
addr0x00000610: add x15, x27, x17
addr0x00000614: add x22, x9, x23
addr0x00000618: addi x0, x27, 632
addr0x0000061c: sw x20, 1993(x8)
addr0x00000620: addi x4, x24, 1476
addr0x00000624: addi x22, x20, 1198
addr0x00000628: andi x0, x15, -281
addr0x0000062c: lw x17, 457(x8)
addr0x00000630: lw x29, 1460(x8)
addr0x00000634: lw x10, -589(x8)
addr0x00000638: lw x10, 920(x8)
addr0x0000063c: lw x24, -485(x8)
addr0x00000640: andi x17, x5, -964
addr0x00000644: addi x11, x31, 1037
addr0x00000648: sh x0, -1236(x8)
addr0x0000064c: addi x13, x12, 799
addr0x00000650: sw x9, 2037(x8)
addr0x00000654: jal x20, addr0x000002c4
addr0x00000658: sw x18, -2044(x8)
addr0x0000065c: sb x4, -1099(x8)
addr0x00000660: lbu x5, 829(x8)
addr0x00000664: lw x19, -1350(x8)
addr0x00000668: lw x4, 1384(x8)
addr0x0000066c: lw x25, -438(x8)
addr0x00000670: lw x5, 1206(x8)
addr0x00000674: lw x14, 547(x8)
addr0x00000678: addi x29, x20, 1189
addr0x0000067c: jal x9, addr0x00000294
addr0x00000680: sw x20, 961(x8)
addr0x00000684: addi x15, x11, 1531
addr0x00000688: jal x21, addr0x000005e4
addr0x0000068c: addi x24, x13, -1324
addr0x00000690: addi x30, x28, 2020
addr0x00000694: addi x2, x4, 1726
addr0x00000698: lw x30, 486(x8)
addr0x0000069c: jal x29, addr0x000005b4
addr0x000006a0: jal x22, addr0x000001bc
addr0x000006a4: jal x23, addr0x000003a8
addr0x000006a8: jal x4, addr0x00000214
addr0x000006ac: bltu x27, x20, addr0x00000564
addr0x000006b0: slli x14, x23, 31
addr0x000006b4: lui x28, 786387
addr0x000006b8: sub x31, x18, x5
addr0x000006bc: srai x12, x1, 6
addr0x000006c0: and x4, x20, x22
