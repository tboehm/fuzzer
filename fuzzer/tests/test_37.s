.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bltu x28, x25, addr0x000007d4
addr0x0000001c: addi x16, x25, -872
addr0x00000020: blt x12, x17, addr0x00000440
addr0x00000024: bne x14, x18, addr0x00000520
addr0x00000028: lw x18, 2000(x8)
addr0x0000002c: sw x13, 1614(x8)
addr0x00000030: jal x24, addr0x00000188
addr0x00000034: lw x25, -708(x8)
addr0x00000038: addi x27, x16, -814
addr0x0000003c: lw x13, -1330(x8)
addr0x00000040: lbu x28, 229(x8)
addr0x00000044: lw x31, -1293(x8)
addr0x00000048: addi x18, x22, -659
addr0x0000004c: sw x19, -1127(x8)
addr0x00000050: lw x0, 1192(x8)
addr0x00000054: addi x20, x7, -907
addr0x00000058: jal x30, addr0x0000065c
addr0x0000005c: lw x9, -1036(x8)
addr0x00000060: addi x10, x25, -1958
addr0x00000064: jal x20, addr0x00000520
addr0x00000068: jal x0, addr0x00000320
addr0x0000006c: addi x10, x19, 1673
addr0x00000070: andi x0, x25, 2046
addr0x00000074: addi x15, x0, 1751
addr0x00000078: lw x10, -315(x8)
addr0x0000007c: addi x16, x19, 1306
addr0x00000080: addi x12, x27, 1179
addr0x00000084: jal x30, addr0x00000d34
addr0x00000088: sw x5, 940(x8)
addr0x0000008c: lw x26, 1974(x8)
addr0x00000090: lw x31, 357(x8)
addr0x00000094: jal x29, addr0x00000690
addr0x00000098: bltu x1, x7, addr0x0000030c
addr0x0000009c: sub x21, x16, x28
addr0x000000a0: sub x31, x13, x18
addr0x000000a4: srai x19, x26, 8
addr0x000000a8: bne x10, x1, addr0x000006b8
addr0x000000ac: or x19, x5, x12
addr0x000000b0: addi x0, x25, 407
addr0x000000b4: lw x25, 231(x8)
addr0x000000b8: add x22, x9, x30
addr0x000000bc: sw x26, -39(x8)
addr0x000000c0: lw x24, -413(x8)
addr0x000000c4: lw x23, -522(x8)
addr0x000000c8: beq x28, x1, addr0x0000001c
addr0x000000cc: lhu x29, 1825(x8)
addr0x000000d0: jal x29, addr0x00000474
addr0x000000d4: lw x2, -616(x8)
addr0x000000d8: lw x31, 1193(x8)
addr0x000000dc: addi x17, x24, 1327
addr0x000000e0: jal x22, addr0x000008e8
addr0x000000e4: lw x18, 39(x8)
addr0x000000e8: addi x0, x17, -1730
addr0x000000ec: jal x28, addr0x000008e4
addr0x000000f0: jal x13, addr0x0000037c
addr0x000000f4: lw x29, 1200(x8)
addr0x000000f8: addi x24, x30, 1093
addr0x000000fc: sh x7, -1207(x8)
addr0x00000100: sw x20, -369(x8)
addr0x00000104: addi x20, x19, -599
addr0x00000108: sw x28, 1078(x8)
addr0x0000010c: jal x23, addr0x00000360
addr0x00000110: lw x20, -950(x8)
addr0x00000114: addi x29, x18, -867
addr0x00000118: jal x4, addr0x00000c54
addr0x0000011c: lw x18, -1796(x8)
addr0x00000120: lw x21, 1249(x8)
addr0x00000124: sw x10, 590(x8)
addr0x00000128: sw x19, -1048(x8)
addr0x0000012c: bne x15, x5, addr0x000003ec
addr0x00000130: sw x28, -1472(x8)
addr0x00000134: addi x20, x12, -1206
addr0x00000138: beq x17, x23, addr0x000007a0
addr0x0000013c: lw x10, -1195(x8)
addr0x00000140: addi x14, x26, 869
addr0x00000144: addi x23, x4, -1551
addr0x00000148: lw x5, 1116(x8)
addr0x0000014c: sw x17, -1476(x8)
addr0x00000150: lw x18, 326(x8)
addr0x00000154: addi x1, x29, 595
addr0x00000158: sw x25, 668(x8)
addr0x0000015c: addi x29, x13, 232
addr0x00000160: sw x21, -136(x8)
addr0x00000164: lui x18, 611095
addr0x00000168: lui x11, 702538
addr0x0000016c: addi x17, x20, -1341
addr0x00000170: beq x31, x7, addr0x000005f0
addr0x00000174: lw x28, -1931(x8)
addr0x00000178: addi x31, x28, 983
addr0x0000017c: sub x18, x28, x13
addr0x00000180: addi x17, x20, -618
addr0x00000184: lw x26, 1484(x8)
addr0x00000188: andi x7, x14, -986
addr0x0000018c: addi x26, x30, 1880
addr0x00000190: sw x4, -1917(x8)
addr0x00000194: sw x28, -871(x8)
addr0x00000198: sw x4, -107(x8)
addr0x0000019c: lbu x10, 1847(x8)
addr0x000001a0: bne x29, x18, addr0x00000084
addr0x000001a4: jal x25, addr0x00000c10
addr0x000001a8: addi x9, x18, -1005
addr0x000001ac: addi x17, x21, -267
addr0x000001b0: beq x31, x12, addr0x00000090
addr0x000001b4: beq x31, x0, addr0x00000200
addr0x000001b8: lw x20, 1938(x8)
addr0x000001bc: lw x15, -492(x8)
addr0x000001c0: add x5, x17, x13
addr0x000001c4: jal x31, addr0x00000af4
addr0x000001c8: lw x5, 743(x8)
addr0x000001cc: sw x17, 542(x8)
addr0x000001d0: sw x7, 1412(x8)
addr0x000001d4: jal x10, addr0x00000410
addr0x000001d8: lw x14, 1504(x8)
addr0x000001dc: lw x26, -679(x8)
addr0x000001e0: lw x27, -1811(x8)
addr0x000001e4: lw x9, -720(x8)
addr0x000001e8: lw x31, -1273(x8)
addr0x000001ec: add x22, x10, x28
addr0x000001f0: lbu x19, -1930(x8)
addr0x000001f4: andi x16, x23, -1794
addr0x000001f8: andi x21, x7, -631
addr0x000001fc: lw x30, -1428(x8)
addr0x00000200: lw x29, 196(x8)
addr0x00000204: addi x19, x13, -834
addr0x00000208: jal x12, addr0x00000c78
addr0x0000020c: lw x14, -985(x8)
addr0x00000210: srai x11, x20, 26
addr0x00000214: srai x10, x23, 19
addr0x00000218: srai x25, x9, 4
addr0x0000021c: sw x18, -1039(x8)
addr0x00000220: jal x19, addr0x0000079c
addr0x00000224: add x13, x30, x24
addr0x00000228: jal x28, addr0x000004f8
addr0x0000022c: lw x5, 887(x8)
addr0x00000230: jal x18, addr0x00000c3c
addr0x00000234: lw x4, -1625(x8)
addr0x00000238: lw x26, 1615(x8)
addr0x0000023c: addi x30, x24, 764
addr0x00000240: jal x19, addr0x00000340
addr0x00000244: andi x27, x29, 182
addr0x00000248: ori x25, x23, 1551
addr0x0000024c: addi x7, x19, 994
addr0x00000250: bltu x15, x30, addr0x000009d0
addr0x00000254: lbu x19, 1979(x8)
addr0x00000258: lw x28, -999(x8)
addr0x0000025c: andi x29, x20, -794
addr0x00000260: lw x22, -312(x8)
addr0x00000264: lw x21, 1961(x8)
addr0x00000268: lw x23, -1905(x8)
addr0x0000026c: lw x22, 541(x8)
addr0x00000270: addi x27, x30, -1010
addr0x00000274: sw x11, -498(x8)
addr0x00000278: sb x13, 1317(x8)
addr0x0000027c: lw x21, -972(x8)
addr0x00000280: lw x31, -1088(x8)
addr0x00000284: lw x19, -1807(x8)
addr0x00000288: addi x29, x23, 1653
addr0x0000028c: bne x16, x10, addr0x000005b8
addr0x00000290: lw x4, -1829(x8)
addr0x00000294: jal x24, addr0x00000984
addr0x00000298: lbu x12, 1560(x8)
addr0x0000029c: beq x17, x0, addr0x00000440
addr0x000002a0: lw x13, -1236(x8)
addr0x000002a4: addi x9, x17, -778
addr0x000002a8: jal x7, addr0x00000458
addr0x000002ac: jal x7, addr0x00000648
addr0x000002b0: lui x26, 136296
addr0x000002b4: addi x27, x16, -1174
addr0x000002b8: xor x21, x26, x23
addr0x000002bc: sw x12, 135(x8)
addr0x000002c0: addi x18, x23, 264
addr0x000002c4: sw x29, -1513(x8)
addr0x000002c8: addi x15, x9, 439
addr0x000002cc: addi x19, x11, 1534
addr0x000002d0: sw x31, -1517(x8)
addr0x000002d4: lhu x25, 1841(x8)
addr0x000002d8: sub x24, x24, x7
addr0x000002dc: addi x0, x16, 1071
addr0x000002e0: lui x19, 1046647
addr0x000002e4: addi x19, x29, -1023
addr0x000002e8: add x31, x20, x17
addr0x000002ec: and x14, x17, x0
addr0x000002f0: or x24, x26, x17
addr0x000002f4: sltu x9, x23, x29
addr0x000002f8: or x30, x15, x0
addr0x000002fc: bne x9, x27, addr0x0000020c
addr0x00000300: lbu x5, -1724(x8)
addr0x00000304: lw x9, -1056(x8)
addr0x00000308: lw x14, -370(x8)
addr0x0000030c: lw x7, -1910(x8)
addr0x00000310: jal x27, addr0x00000654
addr0x00000314: jal x29, addr0x00000158
addr0x00000318: addi x12, x22, -128
addr0x0000031c: andi x6, x7, -212
addr0x00000320: sw x10, -660(x8)
addr0x00000324: sb x9, 1198(x8)
addr0x00000328: jal x19, addr0x00000510
addr0x0000032c: lw x17, 1695(x8)
addr0x00000330: lw x9, 1575(x8)
addr0x00000334: jal x29, addr0x00000b40
addr0x00000338: addi x17, x24, 1131
addr0x0000033c: jal x1, addr0x00000d78
addr0x00000340: lw x22, -1320(x8)
addr0x00000344: jal x24, addr0x00000730
addr0x00000348: lbu x7, 86(x8)
addr0x0000034c: bne x22, x22, addr0x00000740
addr0x00000350: jal x23, addr0x00000304
addr0x00000354: lw x23, 1150(x8)
addr0x00000358: jal x7, addr0x00000150
addr0x0000035c: ori x31, x31, -1770
addr0x00000360: lw x13, 1476(x8)
addr0x00000364: bgeu x6, x22, addr0x0000078c
addr0x00000368: addi x23, x16, -1091
addr0x0000036c: jal x31, addr0x00000518
addr0x00000370: sw x15, -2032(x8)
addr0x00000374: jal x18, addr0x000009dc
addr0x00000378: addi x17, x28, 1746
addr0x0000037c: sw x14, 948(x8)
addr0x00000380: addi x2, x9, 1746
addr0x00000384: jal x14, addr0x00000838
addr0x00000388: jal x28, addr0x00000b60
addr0x0000038c: lw x27, 716(x8)
addr0x00000390: addi x22, x23, 1624
addr0x00000394: jal x0, addr0x00000784
addr0x00000398: lw x24, 897(x8)
addr0x0000039c: lw x15, 1604(x8)
addr0x000003a0: lw x16, -38(x8)
addr0x000003a4: addi x14, x25, 100
addr0x000003a8: jal x11, addr0x000000f4
addr0x000003ac: jal x20, addr0x00000ac8
addr0x000003b0: addi x18, x28, 1940
addr0x000003b4: lhu x28, 791(x8)
addr0x000003b8: addi x17, x7, 900
addr0x000003bc: lw x20, 134(x8)
addr0x000003c0: sw x22, 1617(x8)
addr0x000003c4: lw x28, -1140(x8)
addr0x000003c8: lw x19, 936(x8)
addr0x000003cc: bltu x17, x4, addr0x00000538
addr0x000003d0: bgeu x6, x16, addr0x000000ec
addr0x000003d4: lw x27, -260(x8)
addr0x000003d8: lw x29, 6(x8)
addr0x000003dc: addi x7, x21, 261
addr0x000003e0: bne x26, x26, addr0x000005e0
addr0x000003e4: lw x19, 1292(x8)
addr0x000003e8: lw x7, 1683(x8)
addr0x000003ec: sb x19, -1661(x8)
addr0x000003f0: sw x24, 1655(x8)
addr0x000003f4: addi x29, x17, -832
addr0x000003f8: jal x19, addr0x00000680
addr0x000003fc: addi x5, x0, 729
addr0x00000400: lui x23, 586759
addr0x00000404: addi x21, x17, 1581
addr0x00000408: beq x20, x1, addr0x0000090c
addr0x0000040c: slli x10, x0, 6
addr0x00000410: srli x22, x5, 15
addr0x00000414: addi x31, x25, -855
addr0x00000418: jal x23, addr0x00000774
addr0x0000041c: lw x13, 1279(x8)
addr0x00000420: lbu x14, -1111(x8)
addr0x00000424: bne x20, x28, addr0x0000033c
addr0x00000428: addi x15, x0, -877
addr0x0000042c: lw x19, -718(x8)
addr0x00000430: lw x0, -1038(x8)
addr0x00000434: lw x13, -1799(x8)
addr0x00000438: lw x7, 2039(x8)
addr0x0000043c: bge x12, x13, addr0x00000284
addr0x00000440: addi x29, x13, -493
addr0x00000444: sw x18, -160(x8)
addr0x00000448: lbu x14, 1530(x8)
addr0x0000044c: andi x19, x1, -1042
addr0x00000450: sw x14, -1961(x8)
addr0x00000454: sw x29, 297(x8)
addr0x00000458: slli x20, x2, 24
addr0x0000045c: andi x2, x27, -704
addr0x00000460: sub x1, x13, x16
addr0x00000464: sw x22, 1037(x8)
addr0x00000468: addi x22, x5, -418
addr0x0000046c: jal x11, addr0x00000958
addr0x00000470: sw x20, -1341(x8)
addr0x00000474: addi x19, x0, -647
addr0x00000478: bne x17, x0, addr0x00000458
addr0x0000047c: addi x18, x17, -405
addr0x00000480: jal x13, addr0x000008f0
addr0x00000484: jal x11, addr0x00000bdc
addr0x00000488: bgeu x27, x29, addr0x000000fc
addr0x0000048c: lw x0, 1523(x8)
addr0x00000490: add x20, x5, x30
addr0x00000494: sw x7, -65(x8)
addr0x00000498: lw x11, 1652(x8)
addr0x0000049c: add x20, x18, x9
addr0x000004a0: lw x14, -177(x8)
addr0x000004a4: lw x22, 1381(x8)
addr0x000004a8: beq x6, x23, addr0x00000620
addr0x000004ac: jalr x0, 24(x3)
addr0x000004b0: jal x20, addr0x00000748
addr0x000004b4: lw x22, -920(x8)
addr0x000004b8: addi x19, x23, -1914
addr0x000004bc: addi x30, x20, 977
addr0x000004c0: addi x25, x11, -1863
addr0x000004c4: jal x9, addr0x00000328
addr0x000004c8: addi x19, x28, 1947
addr0x000004cc: sw x20, 975(x8)
addr0x000004d0: addi x2, x27, -614
addr0x000004d4: bge x23, x22, addr0x00000c78
addr0x000004d8: addi x25, x11, 1664
addr0x000004dc: lw x23, -950(x8)
addr0x000004e0: addi x0, x11, -1237
addr0x000004e4: sw x13, 1817(x8)
addr0x000004e8: lw x22, 996(x8)
addr0x000004ec: lw x24, -245(x8)
addr0x000004f0: srai x18, x30, 5
addr0x000004f4: srai x19, x22, 14
addr0x000004f8: bltu x18, x22, addr0x000008dc
addr0x000004fc: slli x15, x25, 7
addr0x00000500: bltu x4, x9, addr0x00000b18
addr0x00000504: addi x14, x22, -705
addr0x00000508: sw x23, -465(x8)
addr0x0000050c: sw x14, -982(x8)
addr0x00000510: sw x22, -2036(x8)
addr0x00000514: lw x20, -1042(x8)
addr0x00000518: bgeu x31, x24, addr0x00000980
addr0x0000051c: addi x19, x19, 819
addr0x00000520: addi x30, x30, -1781
addr0x00000524: blt x0, x0, addr0x00000b74
addr0x00000528: lui x20, 388827
addr0x0000052c: lui x0, 2062
addr0x00000530: slli x30, x9, 11
addr0x00000534: addi x5, x13, 553
addr0x00000538: jal x22, addr0x000007c4
addr0x0000053c: lw x13, -1893(x8)
addr0x00000540: sw x19, 1704(x8)
addr0x00000544: sw x5, -336(x8)
addr0x00000548: addi x7, x22, 1558
addr0x0000054c: bge x19, x30, addr0x00000644
addr0x00000550: sw x31, 1163(x8)
addr0x00000554: sw x0, -1595(x8)
addr0x00000558: sw x4, 1244(x8)
addr0x0000055c: lhu x31, -32(x8)
addr0x00000560: sw x5, 615(x8)
addr0x00000564: addi x27, x19, -298
addr0x00000568: jal x19, addr0x00000cfc
addr0x0000056c: lw x18, -437(x8)
addr0x00000570: jal x7, addr0x00000a30
addr0x00000574: addi x7, x23, 1315
addr0x00000578: sw x23, 669(x8)
addr0x0000057c: sw x16, -843(x8)
addr0x00000580: addi x30, x2, -455
addr0x00000584: jal x11, addr0x000001e8
addr0x00000588: addi x2, x13, -532
addr0x0000058c: jal x0, addr0x000007ec
addr0x00000590: lw x28, -1129(x8)
addr0x00000594: sw x2, 109(x8)
addr0x00000598: bge x24, x14, addr0x000006a0
addr0x0000059c: lw x19, 1721(x8)
addr0x000005a0: sw x13, -320(x8)
addr0x000005a4: sw x7, -790(x8)
addr0x000005a8: jal x16, addr0x00000ca0
addr0x000005ac: addi x30, x31, -14
addr0x000005b0: lw x18, 907(x8)
addr0x000005b4: ori x17, x16, 1290
addr0x000005b8: addi x16, x12, 814
addr0x000005bc: lui x31, 408478
addr0x000005c0: addi x20, x4, -1462
addr0x000005c4: lui x26, 87917
addr0x000005c8: addi x25, x23, 2036
addr0x000005cc: srai x22, x12, 26
addr0x000005d0: jal x0, addr0x0000019c
addr0x000005d4: lw x27, -1339(x8)
addr0x000005d8: add x18, x17, x21
addr0x000005dc: jal x19, addr0x000005d4
addr0x000005e0: div x23, x17, x0
addr0x000005e4: jal x1, addr0x00000258
addr0x000005e8: bgeu x7, x0, addr0x00000d74
addr0x000005ec: lw x28, -364(x8)
addr0x000005f0: sw x23, -314(x8)
addr0x000005f4: lbu x14, 1755(x8)
addr0x000005f8: lw x20, 1558(x8)
addr0x000005fc: addi x17, x4, -868
addr0x00000600: sw x15, 95(x8)
addr0x00000604: sw x5, -1749(x8)
addr0x00000608: andi x18, x7, 408
addr0x0000060c: lw x0, 1731(x8)
addr0x00000610: jal x14, addr0x00000278
addr0x00000614: lw x22, 1517(x8)
addr0x00000618: lw x5, 1759(x8)
addr0x0000061c: addi x0, x9, -492
addr0x00000620: bne x19, x22, addr0x00000360
addr0x00000624: lw x19, -453(x8)
addr0x00000628: lw x20, 448(x8)
addr0x0000062c: addi x19, x31, -613
addr0x00000630: jal x14, addr0x00000c6c
addr0x00000634: jal x0, addr0x000000dc
addr0x00000638: jal x18, addr0x00000c68
addr0x0000063c: lw x17, 1497(x8)
addr0x00000640: addi x18, x23, 590
addr0x00000644: lw x19, 1716(x8)
addr0x00000648: lw x18, -427(x8)
addr0x0000064c: lw x5, 1935(x8)
addr0x00000650: bgeu x29, x10, addr0x00000a14
addr0x00000654: jal x11, addr0x0000028c
addr0x00000658: addi x7, x30, 1523
addr0x0000065c: sw x13, -1910(x8)
addr0x00000660: sw x19, -803(x8)
addr0x00000664: addi x17, x20, -2042
addr0x00000668: sw x29, 1120(x8)
addr0x0000066c: sw x15, -1317(x8)
addr0x00000670: sw x4, -1433(x8)
addr0x00000674: addi x18, x7, -840
addr0x00000678: addi x5, x19, 1386
addr0x0000067c: bne x23, x13, addr0x000004c8
addr0x00000680: sh x14, -511(x8)
addr0x00000684: slli x1, x20, 5
addr0x00000688: srli x5, x18, 14
addr0x0000068c: add x30, x30, x29
addr0x00000690: sw x13, 2035(x8)
addr0x00000694: sw x23, 422(x8)
addr0x00000698: addi x31, x7, 176
addr0x0000069c: bgeu x0, x9, addr0x0000008c
addr0x000006a0: lw x19, -1275(x8)
addr0x000006a4: slli x28, x12, 22
addr0x000006a8: lw x7, -114(x8)
addr0x000006ac: jal x27, addr0x00000020
addr0x000006b0: lw x23, -940(x8)
addr0x000006b4: sw x29, -384(x8)
addr0x000006b8: jal x19, addr0x00000578
addr0x000006bc: sw x30, 1567(x8)
addr0x000006c0: sub x16, x7, x9
addr0x000006c4: srai x18, x18, 20
addr0x000006c8: jal x28, addr0x0000007c
addr0x000006cc: lbu x15, -1777(x8)
addr0x000006d0: addi x0, x16, -44
addr0x000006d4: lw x19, 1327(x8)
addr0x000006d8: add x7, x10, x19
addr0x000006dc: lhu x7, 1791(x8)
addr0x000006e0: addi x9, x0, -1498
addr0x000006e4: lw x11, -1274(x8)
addr0x000006e8: andi x7, x30, -744
addr0x000006ec: ori x5, x24, 1192
addr0x000006f0: sw x16, 644(x8)
addr0x000006f4: lui x27, 490376
addr0x000006f8: sw x31, -1183(x8)
addr0x000006fc: jal x2, addr0x0000082c
addr0x00000700: lw x0, -403(x8)
addr0x00000704: addi x4, x17, -1026
addr0x00000708: addi x0, x15, 258
addr0x0000070c: sw x5, 1475(x8)
addr0x00000710: jal x7, addr0x000008d8
addr0x00000714: addi x12, x17, 1993
addr0x00000718: addi x29, x14, -1282
addr0x0000071c: sw x17, -976(x8)
addr0x00000720: sw x28, 35(x8)
addr0x00000724: lw x6, -2006(x8)
addr0x00000728: slli x19, x7, 5
addr0x0000072c: bgeu x18, x18, addr0x00000044
addr0x00000730: slli x13, x15, 30
addr0x00000734: lw x9, -1795(x8)
addr0x00000738: bgeu x18, x27, addr0x0000040c
addr0x0000073c: lw x16, 651(x8)
addr0x00000740: addi x31, x17, 190
addr0x00000744: jal x0, addr0x00000b8c
addr0x00000748: jal x0, addr0x000002f8
addr0x0000074c: lw x24, -398(x8)
addr0x00000750: lw x20, -1383(x8)
addr0x00000754: lw x31, 630(x8)
addr0x00000758: lw x19, 994(x8)
addr0x0000075c: sub x7, x0, x29
addr0x00000760: srai x10, x19, 13
addr0x00000764: and x16, x31, x18
addr0x00000768: or x28, x23, x9
addr0x0000076c: lbu x20, 1829(x8)
addr0x00000770: addi x0, x5, -564
addr0x00000774: bne x14, x31, addr0x00000a34
addr0x00000778: addi x27, x4, 1451
addr0x0000077c: lui x31, 532507
addr0x00000780: lui x7, 965850
addr0x00000784: sw x7, -392(x8)
addr0x00000788: sw x23, 1981(x8)
addr0x0000078c: lui x31, 307207
addr0x00000790: lw x7, 182(x8)
addr0x00000794: sw x31, 1164(x8)
addr0x00000798: lw x20, 556(x8)
addr0x0000079c: sw x14, 1539(x8)
addr0x000007a0: lw x9, -583(x8)
addr0x000007a4: jal x1, addr0x000001a4
addr0x000007a8: lui x14, 337801
addr0x000007ac: addi x25, x19, 1051
addr0x000007b0: lbu x6, -1592(x8)
addr0x000007b4: sw x10, -1636(x8)
addr0x000007b8: jal x19, addr0x00000dd4
addr0x000007bc: lw x22, 1908(x8)
addr0x000007c0: auipc x18, 243933
addr0x000007c4: lw x9, -304(x8)
addr0x000007c8: lw x24, 274(x8)
addr0x000007cc: addi x23, x2, -1813
addr0x000007d0: sw x20, -1932(x8)
addr0x000007d4: sw x31, 148(x8)
addr0x000007d8: addi x21, x18, -827
addr0x000007dc: sw x13, -1515(x8)
addr0x000007e0: lw x11, -834(x8)
addr0x000007e4: lw x9, -1740(x8)
addr0x000007e8: lw x9, -962(x8)
addr0x000007ec: lw x13, 712(x8)
addr0x000007f0: lw x7, 1618(x8)
addr0x000007f4: lw x26, 364(x8)
addr0x000007f8: lw x4, -1200(x8)
addr0x000007fc: lw x18, 1160(x8)
addr0x00000800: lw x5, -644(x8)
addr0x00000804: sw x18, -641(x8)
addr0x00000808: lw x29, -2020(x8)
addr0x0000080c: sb x22, -1884(x8)
addr0x00000810: addi x7, x23, 582
addr0x00000814: sw x5, -1041(x8)
addr0x00000818: sw x10, -1655(x8)
addr0x0000081c: sw x27, 611(x8)
addr0x00000820: addi x16, x4, 1795
addr0x00000824: bgeu x14, x26, addr0x00000af8
addr0x00000828: slli x27, x31, 24
addr0x0000082c: jal x19, addr0x00000074
addr0x00000830: jal x19, addr0x00000c64
addr0x00000834: addi x19, x7, 1730
addr0x00000838: jal x20, addr0x000001a8
addr0x0000083c: lw x17, 384(x8)
addr0x00000840: addi x25, x9, -672
addr0x00000844: jal x15, addr0x000003f8
addr0x00000848: lw x23, 1405(x8)
addr0x0000084c: addi x29, x30, -1050
addr0x00000850: addi x24, x18, 254
addr0x00000854: addi x7, x25, 474
addr0x00000858: sub x12, x4, x23
addr0x0000085c: add x10, x2, x19
addr0x00000860: sw x22, 406(x8)
addr0x00000864: sw x20, -929(x8)
addr0x00000868: lw x29, -177(x8)
addr0x0000086c: lw x14, 1623(x8)
addr0x00000870: lw x5, 468(x8)
addr0x00000874: sw x12, -870(x8)
addr0x00000878: sw x9, 849(x8)
addr0x0000087c: sw x23, -1261(x8)
addr0x00000880: sw x18, -456(x8)
addr0x00000884: jal x18, addr0x000004b4
addr0x00000888: addi x17, x0, 256
addr0x0000088c: addi x18, x11, -1675
addr0x00000890: lw x7, 1291(x8)
addr0x00000894: lw x26, 1319(x8)
addr0x00000898: lw x24, 1695(x8)
addr0x0000089c: lw x0, -1633(x8)
addr0x000008a0: lw x20, 695(x8)
addr0x000008a4: addi x28, x9, -1153
addr0x000008a8: sw x9, -24(x8)
addr0x000008ac: sw x31, 1585(x8)
addr0x000008b0: jal x28, addr0x000001b4
addr0x000008b4: addi x23, x26, 1372
addr0x000008b8: sw x10, -1315(x8)
addr0x000008bc: sw x24, -1505(x8)
addr0x000008c0: addi x28, x4, 69
addr0x000008c4: addi x5, x20, -1350
addr0x000008c8: lbu x0, -620(x8)
addr0x000008cc: sw x28, -1133(x8)
addr0x000008d0: sb x29, -221(x8)
addr0x000008d4: jal x23, addr0x000006ec
addr0x000008d8: lui x17, 883694
addr0x000008dc: sw x18, -919(x8)
addr0x000008e0: sw x30, 1142(x8)
addr0x000008e4: lw x20, -321(x8)
addr0x000008e8: sw x1, 230(x8)
addr0x000008ec: jal x25, addr0x00000db4
addr0x000008f0: lw x0, -300(x8)
addr0x000008f4: bge x18, x4, addr0x00000d54
addr0x000008f8: beq x24, x5, addr0x000004dc
addr0x000008fc: lbu x29, -1286(x8)
addr0x00000900: bne x27, x14, addr0x000006dc
addr0x00000904: lw x5, -231(x8)
addr0x00000908: lw x22, -1122(x8)
addr0x0000090c: addi x19, x19, -154
addr0x00000910: lw x28, -143(x8)
addr0x00000914: addi x5, x17, -1225
addr0x00000918: sw x9, -2029(x8)
addr0x0000091c: jal x18, addr0x000006b8
addr0x00000920: lw x7, -1199(x8)
addr0x00000924: lw x15, -1307(x8)
addr0x00000928: lw x10, 1150(x8)
addr0x0000092c: sw x17, -231(x8)
addr0x00000930: lw x18, 283(x8)
addr0x00000934: sw x7, 1636(x8)
addr0x00000938: jal x22, addr0x00000be4
addr0x0000093c: lbu x26, 1800(x8)
addr0x00000940: beq x16, x20, addr0x00000cfc
addr0x00000944: sw x30, -903(x8)
addr0x00000948: addi x0, x16, 1447
addr0x0000094c: sw x26, -1519(x8)
addr0x00000950: sw x26, 1390(x8)
addr0x00000954: lw x25, -1444(x8)
addr0x00000958: addi x5, x18, 1682
addr0x0000095c: sw x9, -1131(x8)
addr0x00000960: bne x13, x20, addr0x00000584
addr0x00000964: sh x28, 478(x8)
addr0x00000968: bne x5, x0, addr0x000002c8
addr0x0000096c: lw x23, -1215(x8)
addr0x00000970: lw x24, -1572(x8)
addr0x00000974: lw x19, -1334(x8)
addr0x00000978: addi x19, x5, -1065
addr0x0000097c: jal x31, addr0x000003f8
addr0x00000980: sw x19, -1137(x8)
addr0x00000984: jal x29, addr0x0000070c
addr0x00000988: lw x24, -1044(x8)
addr0x0000098c: lw x19, 1560(x8)
addr0x00000990: sw x22, -658(x8)
addr0x00000994: sw x21, 170(x8)
addr0x00000998: lw x20, -1171(x8)
addr0x0000099c: addi x14, x20, -1339
addr0x000009a0: lw x2, 302(x8)
addr0x000009a4: sw x10, 677(x8)
addr0x000009a8: lw x31, 1283(x8)
addr0x000009ac: lw x23, 1185(x8)
addr0x000009b0: lw x24, 1977(x8)
addr0x000009b4: bgeu x9, x25, addr0x00000c1c
addr0x000009b8: lw x13, -1980(x8)
addr0x000009bc: lw x25, -111(x8)
addr0x000009c0: lhu x5, -975(x8)
addr0x000009c4: lhu x5, -712(x8)
addr0x000009c8: lhu x17, -608(x8)
addr0x000009cc: bgeu x4, x17, addr0x00000274
addr0x000009d0: lbu x13, -1156(x8)
addr0x000009d4: andi x17, x0, -527
addr0x000009d8: lw x14, -1569(x8)
addr0x000009dc: srai x18, x0, 1
addr0x000009e0: lw x7, 1644(x8)
addr0x000009e4: sw x29, 1632(x8)
addr0x000009e8: addi x24, x18, -2015
addr0x000009ec: sw x20, -235(x8)
addr0x000009f0: slli x30, x17, 4
addr0x000009f4: andi x29, x7, -1631
addr0x000009f8: andi x18, x14, 171
addr0x000009fc: sub x13, x30, x25
addr0x00000a00: slli x26, x11, 20
addr0x00000a04: lw x4, 706(x8)
addr0x00000a08: jal x25, addr0x00000820
addr0x00000a0c: jal x25, addr0x00000b28
addr0x00000a10: addi x22, x20, 194
addr0x00000a14: jal x22, addr0x00000df0
addr0x00000a18: jal x17, addr0x00000c84
addr0x00000a1c: sb x23, -977(x8)
addr0x00000a20: jal x29, addr0x00000afc
addr0x00000a24: addi x23, x27, -848
addr0x00000a28: jal x24, addr0x000008b8
addr0x00000a2c: lui x0, 626812
addr0x00000a30: lw x16, -684(x8)
addr0x00000a34: sw x18, 215(x8)
addr0x00000a38: lw x12, -309(x8)
addr0x00000a3c: lw x22, -57(x8)
addr0x00000a40: lw x7, 265(x8)
addr0x00000a44: sw x7, -1575(x8)
addr0x00000a48: sw x5, -1500(x8)
addr0x00000a4c: lbu x25, 684(x8)
addr0x00000a50: add x20, x1, x4
addr0x00000a54: jal x12, addr0x00000de8
addr0x00000a58: jal x2, addr0x00000458
addr0x00000a5c: sw x20, 1978(x8)
addr0x00000a60: sb x4, -3(x8)
addr0x00000a64: beq x13, x23, addr0x00000598
addr0x00000a68: addi x27, x18, 701
addr0x00000a6c: addi x22, x27, 1608
addr0x00000a70: add x24, x13, x27
addr0x00000a74: jal x17, addr0x00000b4c
addr0x00000a78: andi x26, x20, 1254
addr0x00000a7c: addi x7, x20, 1883
addr0x00000a80: andi x7, x20, 1317
addr0x00000a84: add x22, x27, x25
addr0x00000a88: addi x19, x19, 1401
addr0x00000a8c: lui x4, 703060
addr0x00000a90: lui x24, 937574
addr0x00000a94: slli x14, x7, 23
addr0x00000a98: jal x23, addr0x00000488
addr0x00000a9c: jal x7, addr0x000009f4
addr0x00000aa0: lw x21, 949(x8)
addr0x00000aa4: lw x26, -1959(x8)
addr0x00000aa8: lw x7, -1216(x8)
addr0x00000aac: lw x15, 1482(x8)
addr0x00000ab0: addi x21, x31, 633
addr0x00000ab4: bne x14, x30, addr0x0000048c
addr0x00000ab8: bne x26, x17, addr0x00000380
addr0x00000abc: addi x21, x31, 1264
addr0x00000ac0: lw x13, -497(x8)
addr0x00000ac4: lw x29, 1254(x8)
addr0x00000ac8: slli x0, x19, 31
addr0x00000acc: lw x27, 420(x8)
addr0x00000ad0: lbu x20, 1066(x8)
addr0x00000ad4: beq x0, x22, addr0x00000930
addr0x00000ad8: lw x31, -1744(x8)
addr0x00000adc: jal x28, addr0x000001e8
addr0x00000ae0: blt x9, x9, addr0x000003cc
addr0x00000ae4: sw x20, 1780(x8)
addr0x00000ae8: sw x23, 1546(x8)
addr0x00000aec: lw x20, -333(x8)
addr0x00000af0: addi x4, x9, -43
addr0x00000af4: sw x18, 109(x8)
addr0x00000af8: sw x1, 1107(x8)
addr0x00000afc: beq x7, x17, addr0x00000920
addr0x00000b00: bgeu x20, x19, addr0x00000560
addr0x00000b04: add x1, x31, x11
addr0x00000b08: addi x7, x10, -328
addr0x00000b0c: addi x16, x19, 778
addr0x00000b10: lw x31, 5(x8)
addr0x00000b14: slli x13, x10, 19
addr0x00000b18: add x29, x22, x21
addr0x00000b1c: beq x18, x29, addr0x00000b78
addr0x00000b20: lui x1, 598367
addr0x00000b24: sw x31, -129(x8)
addr0x00000b28: sw x25, 481(x8)
addr0x00000b2c: jal x14, addr0x0000054c
addr0x00000b30: beq x14, x16, addr0x000004fc
addr0x00000b34: jal x5, addr0x000003f0
addr0x00000b38: sw x11, -1737(x8)
addr0x00000b3c: sw x13, -1802(x8)
addr0x00000b40: sw x31, -253(x8)
addr0x00000b44: sub x13, x7, x27
addr0x00000b48: lw x12, -1231(x8)
addr0x00000b4c: addi x0, x29, 406
addr0x00000b50: lw x7, 1599(x8)
addr0x00000b54: bgeu x23, x19, addr0x00000750
addr0x00000b58: blt x6, x2, addr0x000006a4
addr0x00000b5c: addi x22, x10, 1657
addr0x00000b60: sw x13, 1891(x8)
addr0x00000b64: lw x11, 1426(x8)
addr0x00000b68: lw x31, -1210(x8)
addr0x00000b6c: jal x2, addr0x00000264
addr0x00000b70: lw x0, 753(x8)
addr0x00000b74: andi x2, x9, -2009
addr0x00000b78: jal x13, addr0x000002b4
addr0x00000b7c: lbu x18, -475(x8)
addr0x00000b80: lbu x22, 1170(x8)
addr0x00000b84: andi x19, x4, -249
addr0x00000b88: add x24, x9, x0
addr0x00000b8c: sb x14, 68(x8)
addr0x00000b90: sw x7, 1128(x8)
addr0x00000b94: sw x7, -588(x8)
addr0x00000b98: sw x9, -1369(x8)
addr0x00000b9c: lw x9, -1255(x8)
addr0x00000ba0: lw x2, 1410(x8)
addr0x00000ba4: lw x1, 1374(x8)
addr0x00000ba8: lw x27, -628(x8)
addr0x00000bac: addi x23, x28, -425
addr0x00000bb0: addi x7, x0, 1859
addr0x00000bb4: lw x29, -1344(x8)
addr0x00000bb8: sw x7, -1528(x8)
addr0x00000bbc: sw x29, -1488(x8)
addr0x00000bc0: add x5, x27, x26
addr0x00000bc4: jal x5, addr0x000002ac
addr0x00000bc8: lw x25, -403(x8)
addr0x00000bcc: add x9, x27, x7
addr0x00000bd0: sub x18, x7, x0
addr0x00000bd4: sub x18, x20, x5
addr0x00000bd8: srai x21, x25, 26
addr0x00000bdc: bge x13, x26, addr0x00000590
addr0x00000be0: addi x1, x7, 654
addr0x00000be4: sw x13, -2026(x8)
addr0x00000be8: sw x20, 1142(x8)
addr0x00000bec: sw x18, -267(x8)
addr0x00000bf0: sw x9, 406(x8)
addr0x00000bf4: slli x23, x22, 12
addr0x00000bf8: lw x2, 1338(x8)
addr0x00000bfc: addi x19, x15, -277
addr0x00000c00: jal x18, addr0x00000204
addr0x00000c04: beq x17, x19, addr0x00000e24
addr0x00000c08: jal x13, addr0x000001fc
addr0x00000c0c: sw x1, -800(x8)
addr0x00000c10: jal x0, addr0x000003b8
addr0x00000c14: lw x13, -1047(x8)
addr0x00000c18: lw x23, -1628(x8)
addr0x00000c1c: andi x30, x26, 1101
addr0x00000c20: sb x22, -1817(x8)
addr0x00000c24: lw x11, -501(x8)
addr0x00000c28: lw x2, -1051(x8)
addr0x00000c2c: lw x12, -568(x8)
addr0x00000c30: addi x24, x2, -862
addr0x00000c34: jal x7, addr0x00000840
addr0x00000c38: lw x15, 1532(x8)
addr0x00000c3c: lbu x9, -761(x8)
addr0x00000c40: addi x19, x28, 1061
addr0x00000c44: jal x5, addr0x0000002c
addr0x00000c48: sw x4, 20(x8)
addr0x00000c4c: jal x18, addr0x00000cb0
addr0x00000c50: lw x27, -540(x8)
addr0x00000c54: addi x14, x9, -1525
addr0x00000c58: addi x27, x9, 800
addr0x00000c5c: jal x21, addr0x00000014
addr0x00000c60: jal x7, addr0x00000b80
addr0x00000c64: beq x14, x18, addr0x00000e14
addr0x00000c68: jal x28, addr0x000000f0
addr0x00000c6c: bne x10, x20, addr0x00000e7c
addr0x00000c70: addi x0, x14, 1718
addr0x00000c74: lw x10, -357(x8)
addr0x00000c78: blt x26, x29, addr0x00000cb0
addr0x00000c7c: sw x11, 381(x8)
addr0x00000c80: jal x15, addr0x00000d38
addr0x00000c84: addi x24, x28, -1655
addr0x00000c88: jal x17, addr0x00000750
addr0x00000c8c: jal x23, addr0x0000033c
addr0x00000c90: lw x9, -1450(x8)
addr0x00000c94: beq x17, x23, addr0x00000e1c
addr0x00000c98: addi x17, x20, 1643
addr0x00000c9c: addi x18, x18, -1063
addr0x00000ca0: sw x22, -1395(x8)
addr0x00000ca4: sw x26, -798(x8)
addr0x00000ca8: jal x12, addr0x00000710
addr0x00000cac: auipc x22, 320996
addr0x00000cb0: addi x23, x10, 928
addr0x00000cb4: jal x7, addr0x00000658
addr0x00000cb8: lw x15, 628(x8)
addr0x00000cbc: lw x22, -446(x8)
addr0x00000cc0: bltu x4, x25, addr0x000008b0
addr0x00000cc4: jal x13, addr0x00000384
addr0x00000cc8: lw x0, 10(x8)
addr0x00000ccc: sw x18, 45(x8)
addr0x00000cd0: sw x0, 1148(x8)
addr0x00000cd4: addi x29, x17, 1363
addr0x00000cd8: jal x20, addr0x0000079c
addr0x00000cdc: lw x23, -1751(x8)
addr0x00000ce0: sub x31, x13, x7
addr0x00000ce4: bltu x0, x0, addr0x0000067c
addr0x00000ce8: lw x4, -2015(x8)
addr0x00000cec: lw x20, -1293(x8)
addr0x00000cf0: sw x31, 733(x8)
addr0x00000cf4: lbu x20, 1968(x8)
addr0x00000cf8: addi x18, x23, 907
addr0x00000cfc: jal x31, addr0x00000dbc
addr0x00000d00: jal x25, addr0x00000494
addr0x00000d04: sw x0, -1513(x8)
addr0x00000d08: sw x28, 483(x8)
addr0x00000d0c: addi x9, x19, -1833
addr0x00000d10: sw x9, -590(x8)
addr0x00000d14: jal x16, addr0x00000c14
addr0x00000d18: lw x20, 1811(x8)
addr0x00000d1c: addi x13, x29, 915
addr0x00000d20: jal x19, addr0x00000bfc
addr0x00000d24: lw x30, 1081(x8)
addr0x00000d28: jal x20, addr0x00000dd4
addr0x00000d2c: lw x13, 1914(x8)
addr0x00000d30: lw x13, 1581(x8)
addr0x00000d34: lw x23, -1645(x8)
addr0x00000d38: lw x20, -82(x8)
addr0x00000d3c: lui x5, 210008
addr0x00000d40: addi x23, x20, -1201
addr0x00000d44: sw x0, -839(x8)
addr0x00000d48: sw x27, -603(x8)
addr0x00000d4c: jal x13, addr0x00000608
addr0x00000d50: lw x13, 122(x8)
addr0x00000d54: jal x2, addr0x000000d8
addr0x00000d58: lw x4, -1943(x8)
addr0x00000d5c: addi x9, x20, 1564
addr0x00000d60: sw x5, -1969(x8)
addr0x00000d64: jal x6, addr0x00000898
addr0x00000d68: lui x29, 389397
addr0x00000d6c: lh x18, -1698(x8)
addr0x00000d70: sw x7, -1234(x8)
addr0x00000d74: addi x29, x23, 256
addr0x00000d78: sw x5, -1227(x8)
addr0x00000d7c: addi x31, x16, 237
addr0x00000d80: bne x2, x22, addr0x00000918
addr0x00000d84: addi x23, x27, 412
addr0x00000d88: jal x21, addr0x00000490
addr0x00000d8c: bge x22, x0, addr0x00000a08
addr0x00000d90: addi x6, x21, -1641
addr0x00000d94: jal x24, addr0x00000ad8
addr0x00000d98: lw x6, 1767(x8)
addr0x00000d9c: lw x28, -1211(x8)
addr0x00000da0: addi x23, x7, 1308
addr0x00000da4: addi x1, x11, -1508
addr0x00000da8: lbu x25, -1772(x8)
addr0x00000dac: beq x25, x5, addr0x00000648
addr0x00000db0: sw x9, 1394(x8)
addr0x00000db4: jal x7, addr0x00000714
addr0x00000db8: beq x17, x4, addr0x000006fc
addr0x00000dbc: andi x18, x7, -331
addr0x00000dc0: jal x22, addr0x000002c8
addr0x00000dc4: sw x19, -384(x8)
addr0x00000dc8: lw x16, 1808(x8)
addr0x00000dcc: jal x13, addr0x00000198
addr0x00000dd0: lw x19, 533(x8)
addr0x00000dd4: addi x10, x31, 524
addr0x00000dd8: lw x7, 1372(x8)
addr0x00000ddc: lw x22, -1081(x8)
addr0x00000de0: lw x17, -179(x8)
addr0x00000de4: lw x20, 1520(x8)
addr0x00000de8: addi x26, x0, -1695
addr0x00000dec: jal x23, addr0x00000b00
addr0x00000df0: lw x13, -283(x8)
addr0x00000df4: lw x15, -297(x8)
addr0x00000df8: sb x18, -804(x8)
addr0x00000dfc: addi x17, x14, 444
addr0x00000e00: lw x22, -233(x8)
addr0x00000e04: lw x27, 1472(x8)
addr0x00000e08: lw x0, 627(x8)
addr0x00000e0c: lui x23, 663050
addr0x00000e10: addi x24, x20, -1606
addr0x00000e14: sw x29, -516(x8)
addr0x00000e18: addi x5, x29, 463
addr0x00000e1c: lui x13, 994820
addr0x00000e20: addi x20, x9, 1599
addr0x00000e24: jal x29, addr0x00000308
addr0x00000e28: andi x18, x6, 2036
addr0x00000e2c: lw x23, -1981(x8)
addr0x00000e30: sw x23, -380(x8)
addr0x00000e34: sw x10, 141(x8)
addr0x00000e38: jal x22, addr0x00000d30
addr0x00000e3c: addi x0, x25, -1206
addr0x00000e40: addi x31, x21, -481
addr0x00000e44: sw x13, 392(x8)
addr0x00000e48: lw x31, 669(x8)
addr0x00000e4c: sw x28, 860(x8)
addr0x00000e50: jal x14, addr0x00000364
addr0x00000e54: lw x11, 1646(x8)
addr0x00000e58: bge x18, x22, addr0x00000bdc
addr0x00000e5c: andi x17, x29, 1280
addr0x00000e60: addi x20, x25, 1312
addr0x00000e64: lui x19, 243779
addr0x00000e68: slli x20, x4, 21
addr0x00000e6c: srli x1, x17, 17
addr0x00000e70: srli x13, x12, 21
addr0x00000e74: andi x31, x0, 709
addr0x00000e78: sb x12, 273(x8)
addr0x00000e7c: addi x5, x24, -1167
addr0x00000e80: add x1, x5, x17
addr0x00000e84: addi x19, x12, -651
addr0x00000e88: lui x4, 40382
addr0x00000e8c: addi x27, x0, 486
addr0x00000e90: jal x13, addr0x000004ec
addr0x00000e94: lw x31, -422(x8)
addr0x00000e98: jal x25, addr0x00000350
addr0x00000e9c: lw x20, -1484(x8)
addr0x00000ea0: lw x9, -983(x8)
addr0x00000ea4: addi x17, x4, 1369
