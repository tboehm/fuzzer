.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: addi x9, x21, -43
addr0x0000001c: bne x9, x9, addr0x00000024
addr0x00000020: sw x29, 869(x8)
addr0x00000024: sw x17, 580(x8)
addr0x00000028: sw x22, -1576(x8)
addr0x0000002c: sw x1, -505(x8)
addr0x00000030: jal x19, addr0x000000c4
addr0x00000034: addi x4, x22, 1890
addr0x00000038: lw x20, 416(x8)
addr0x0000003c: sw x29, 1105(x8)
addr0x00000040: lw x2, 1681(x8)
addr0x00000044: lw x21, 1794(x8)
addr0x00000048: add x17, x17, x23
addr0x0000004c: bgeu x22, x0, addr0x000001a8
addr0x00000050: bne x26, x17, addr0x00000148
addr0x00000054: lw x29, 1764(x8)
addr0x00000058: lw x22, -618(x8)
addr0x0000005c: addi x19, x23, 1341
addr0x00000060: lw x28, 1554(x8)
addr0x00000064: lw x13, -1824(x8)
addr0x00000068: sub x20, x7, x0
addr0x0000006c: sw x5, -1769(x8)
addr0x00000070: jal x6, addr0x00000134
addr0x00000074: sb x24, -1575(x8)
addr0x00000078: jal x29, addr0x00000178
addr0x0000007c: jal x25, addr0x000000ac
addr0x00000080: lw x25, -1830(x8)
addr0x00000084: jal x29, addr0x000000e8
addr0x00000088: lw x9, 2029(x8)
addr0x0000008c: jal x19, addr0x0000013c
addr0x00000090: sub x21, x2, x22
addr0x00000094: divu x19, x26, x9
addr0x00000098: addi x25, x22, -410
addr0x0000009c: sw x6, 1967(x8)
addr0x000000a0: lbu x21, -1291(x8)
addr0x000000a4: bne x13, x9, addr0x0000003c
addr0x000000a8: lbu x13, -2035(x8)
addr0x000000ac: bne x2, x30, addr0x000000d4
addr0x000000b0: lw x25, -1036(x8)
addr0x000000b4: bne x5, x7, addr0x00000144
addr0x000000b8: lw x25, -1649(x8)
addr0x000000bc: lw x6, 1812(x8)
addr0x000000c0: addi x13, x27, 1619
addr0x000000c4: sw x23, 333(x8)
addr0x000000c8: sw x18, 1544(x8)
addr0x000000cc: jal x10, addr0x00000084
addr0x000000d0: addi x11, x21, 1150
addr0x000000d4: addi x18, x30, 1677
addr0x000000d8: sw x6, 1641(x8)
addr0x000000dc: sb x0, -1(x8)
addr0x000000e0: jal x11, addr0x0000004c
addr0x000000e4: lw x0, 821(x8)
addr0x000000e8: sub x18, x22, x31
addr0x000000ec: add x22, x19, x23
addr0x000000f0: lbu x22, 625(x8)
addr0x000000f4: addi x11, x9, 970
addr0x000000f8: jal x20, addr0x00000128
addr0x000000fc: jal x11, addr0x000001c0
addr0x00000100: lw x15, 1540(x8)
addr0x00000104: lw x15, -810(x8)
addr0x00000108: sw x20, -1233(x8)
addr0x0000010c: sw x17, -1476(x8)
addr0x00000110: addi x17, x7, -739
addr0x00000114: jalr x0, 432(x3)
addr0x00000118: lw x23, -1574(x8)
addr0x0000011c: sw x22, -2042(x8)
addr0x00000120: sw x0, 671(x8)
addr0x00000124: jal x23, addr0x00000044
addr0x00000128: addi x23, x27, 1352
addr0x0000012c: jal x16, addr0x000000bc
addr0x00000130: lw x24, -1523(x8)
addr0x00000134: lbu x19, -891(x8)
addr0x00000138: addi x29, x7, 5
addr0x0000013c: jal x24, addr0x00000098
addr0x00000140: sw x1, 1513(x8)
addr0x00000144: add x29, x24, x7
addr0x00000148: sb x5, 1996(x8)
addr0x0000014c: srli x13, x24, 30
addr0x00000150: slli x23, x5, 16
addr0x00000154: lw x2, -1306(x8)
addr0x00000158: lw x19, -663(x8)
addr0x0000015c: addi x2, x1, 1039
addr0x00000160: jal x29, addr0x0000004c
addr0x00000164: bge x24, x4, addr0x000000c8
addr0x00000168: sub x13, x4, x22
addr0x0000016c: bge x23, x29, addr0x00000190
addr0x00000170: addi x23, x29, -1360
addr0x00000174: sw x30, 311(x8)
addr0x00000178: sw x29, 1656(x8)
addr0x0000017c: addi x16, x31, 422
addr0x00000180: ori x16, x13, -853
addr0x00000184: sw x17, 1536(x8)
addr0x00000188: lbu x7, -1016(x8)
addr0x0000018c: beq x17, x15, addr0x0000011c
addr0x00000190: lw x20, 1972(x8)
addr0x00000194: jal x2, addr0x0000008c
addr0x00000198: addi x5, x1, -182
addr0x0000019c: sw x0, 1482(x8)
addr0x000001a0: jal x22, addr0x00000170
addr0x000001a4: jal x0, addr0x0000012c
addr0x000001a8: addi x2, x23, 1636
addr0x000001ac: bgeu x4, x17, addr0x00000024
addr0x000001b0: lw x20, -775(x8)
addr0x000001b4: beq x17, x15, addr0x00000108
addr0x000001b8: jal x4, addr0x0000007c
addr0x000001bc: sw x23, -1012(x8)
addr0x000001c0: jal x23, addr0x00000004
