.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lbu x31, 439(x8)
addr0x0000001c: slli x1, x22, 6
addr0x00000020: sw x13, -1320(x8)
addr0x00000024: sh x23, 743(x8)
addr0x00000028: addi x17, x27, 585
addr0x0000002c: addi x19, x22, 310
addr0x00000030: jal x7, addr0x00000398
addr0x00000034: addi x6, x28, 1041
addr0x00000038: sw x2, 553(x8)
addr0x0000003c: sw x11, 1838(x8)
addr0x00000040: addi x22, x10, -1499
addr0x00000044: addi x17, x31, 1394
addr0x00000048: jal x11, addr0x000001c8
addr0x0000004c: lw x28, -377(x8)
addr0x00000050: sw x9, 476(x8)
addr0x00000054: blt x2, x17, addr0x00000278
addr0x00000058: sw x18, -1332(x8)
addr0x0000005c: sw x14, -1121(x8)
addr0x00000060: sw x31, 987(x8)
addr0x00000064: addi x15, x2, -4
addr0x00000068: jal x25, addr0x00000214
addr0x0000006c: lbu x20, 1314(x8)
addr0x00000070: bne x23, x19, addr0x00000268
addr0x00000074: jal x30, addr0x00000218
addr0x00000078: jal x19, addr0x00000054
addr0x0000007c: addi x1, x0, -133
addr0x00000080: sh x21, 1229(x8)
addr0x00000084: bne x24, x0, addr0x0000036c
addr0x00000088: lw x1, -92(x8)
addr0x0000008c: srl x20, x14, x5
addr0x00000090: or x14, x10, x14
addr0x00000094: lbu x2, -966(x8)
addr0x00000098: bltu x17, x18, addr0x00000130
addr0x0000009c: bne x21, x4, addr0x000000d4
addr0x000000a0: jal x13, addr0x000001a8
addr0x000000a4: addi x19, x31, 745
addr0x000000a8: jal x19, addr0x00000008
addr0x000000ac: sw x16, -6(x8)
addr0x000000b0: sub x25, x6, x7
addr0x000000b4: jal x9, addr0x00000278
addr0x000000b8: jal x21, addr0x00000310
addr0x000000bc: lw x1, 2013(x8)
addr0x000000c0: addi x6, x0, -593
addr0x000000c4: andi x13, x7, -1666
addr0x000000c8: addi x12, x7, -431
addr0x000000cc: jal x0, addr0x00000338
addr0x000000d0: lw x23, 1136(x8)
addr0x000000d4: addi x25, x16, -1697
addr0x000000d8: lhu x19, -997(x8)
addr0x000000dc: bne x31, x30, addr0x00000098
addr0x000000e0: lw x18, 542(x8)
addr0x000000e4: addi x30, x20, 127
addr0x000000e8: sw x20, -69(x8)
addr0x000000ec: jal x4, addr0x00000358
addr0x000000f0: jal x21, addr0x00000030
addr0x000000f4: bltu x18, x30, addr0x00000278
addr0x000000f8: lw x1, 99(x8)
addr0x000000fc: jal x17, addr0x00000184
addr0x00000100: lw x29, 1099(x8)
addr0x00000104: bne x29, x20, addr0x000001c8
addr0x00000108: lw x9, 1208(x8)
addr0x0000010c: sw x5, -1819(x8)
addr0x00000110: sw x22, 1552(x8)
addr0x00000114: bgeu x18, x0, addr0x0000016c
addr0x00000118: lw x14, -1843(x8)
addr0x0000011c: ori x5, x30, -167
addr0x00000120: jal x27, addr0x00000130
addr0x00000124: lw x24, 757(x8)
addr0x00000128: jal x13, addr0x000001d8
addr0x0000012c: lw x21, 632(x8)
addr0x00000130: andi x4, x29, -1905
addr0x00000134: addi x7, x6, 533
addr0x00000138: jal x19, addr0x0000008c
addr0x0000013c: jal x4, addr0x00000248
addr0x00000140: sw x10, 1272(x8)
addr0x00000144: addi x25, x31, 296
addr0x00000148: sh x22, 549(x8)
addr0x0000014c: sw x28, -1166(x8)
addr0x00000150: sll x7, x17, x13
addr0x00000154: bgeu x18, x10, addr0x00000100
addr0x00000158: lw x21, 900(x8)
addr0x0000015c: bne x7, x5, addr0x00000080
addr0x00000160: lw x25, 646(x8)
addr0x00000164: addi x28, x6, 1241
addr0x00000168: jal x20, addr0x000002cc
addr0x0000016c: jal x25, addr0x00000184
addr0x00000170: bne x2, x19, addr0x000000e0
addr0x00000174: lw x20, -205(x8)
addr0x00000178: bge x19, x13, addr0x000002cc
addr0x0000017c: sw x1, -1533(x8)
addr0x00000180: addi x21, x12, 277
addr0x00000184: jal x29, addr0x00000350
addr0x00000188: jal x13, addr0x00000190
addr0x0000018c: jal x6, addr0x00000338
addr0x00000190: addi x27, x4, -1284
addr0x00000194: jal x22, addr0x000002d8
addr0x00000198: jal x25, addr0x0000032c
addr0x0000019c: jal x13, addr0x00000370
addr0x000001a0: lw x0, -142(x8)
addr0x000001a4: addi x0, x4, 1714
addr0x000001a8: sw x27, 2043(x8)
addr0x000001ac: jal x17, addr0x0000002c
addr0x000001b0: addi x14, x22, -1832
addr0x000001b4: lw x1, 1635(x8)
addr0x000001b8: beq x14, x28, addr0x00000330
addr0x000001bc: lbu x13, 896(x8)
addr0x000001c0: lw x2, -1543(x8)
addr0x000001c4: addi x1, x25, -1850
addr0x000001c8: addi x14, x25, 1855
addr0x000001cc: add x30, x27, x5
addr0x000001d0: sw x9, -193(x8)
addr0x000001d4: addi x23, x22, -120
addr0x000001d8: sw x15, 1696(x8)
addr0x000001dc: addi x9, x26, 939
addr0x000001e0: lbu x7, 789(x8)
addr0x000001e4: lw x14, -1050(x8)
addr0x000001e8: jal x13, addr0x00000130
addr0x000001ec: lw x14, 184(x8)
addr0x000001f0: lw x9, 702(x8)
addr0x000001f4: lw x30, -1265(x8)
addr0x000001f8: lw x2, -267(x8)
addr0x000001fc: sub x7, x24, x12
addr0x00000200: sw x13, -137(x8)
addr0x00000204: lw x5, 1918(x8)
addr0x00000208: jal x17, addr0x0000011c
addr0x0000020c: lw x29, 622(x8)
addr0x00000210: addi x20, x23, 1870
addr0x00000214: jal x4, addr0x00000180
addr0x00000218: lw x0, -506(x8)
addr0x0000021c: slli x25, x20, 17
addr0x00000220: addi x30, x0, 1546
addr0x00000224: lui x0, 808120
addr0x00000228: addi x24, x25, -254
addr0x0000022c: addi x12, x18, 1963
addr0x00000230: addi x15, x6, -1514
addr0x00000234: bne x9, x23, addr0x00000290
addr0x00000238: addi x27, x29, -1390
addr0x0000023c: sw x17, 948(x8)
addr0x00000240: lui x18, 914326
addr0x00000244: addi x4, x7, -1471
addr0x00000248: lw x5, -1227(x8)
addr0x0000024c: lw x4, 821(x8)
addr0x00000250: lw x13, -1664(x8)
addr0x00000254: addi x31, x27, -1753
addr0x00000258: sw x30, 1087(x8)
addr0x0000025c: lw x1, 1497(x8)
addr0x00000260: bgeu x4, x20, addr0x0000019c
addr0x00000264: sw x7, -552(x8)
addr0x00000268: addi x23, x27, -420
addr0x0000026c: beq x19, x9, addr0x000002b4
addr0x00000270: sw x30, 720(x8)
addr0x00000274: sw x7, -1601(x8)
addr0x00000278: jal x29, addr0x00000220
addr0x0000027c: jal x23, addr0x0000013c
addr0x00000280: addi x21, x0, -1898
addr0x00000284: lw x11, 316(x8)
addr0x00000288: srli x10, x31, 31
addr0x0000028c: sw x13, 1469(x8)
addr0x00000290: addi x15, x29, -537
addr0x00000294: jal x4, addr0x00000004
addr0x00000298: lw x19, 259(x8)
addr0x0000029c: lw x0, 1687(x8)
addr0x000002a0: lui x11, 750986
addr0x000002a4: addi x9, x5, 902
addr0x000002a8: or x30, x22, x9
addr0x000002ac: slli x16, x27, 28
addr0x000002b0: lw x6, -1753(x8)
addr0x000002b4: lui x7, 62992
addr0x000002b8: addi x18, x22, -1363
addr0x000002bc: lhu x27, 788(x8)
addr0x000002c0: beq x25, x13, addr0x0000031c
addr0x000002c4: lw x19, 272(x8)
addr0x000002c8: lui x30, 776635
addr0x000002cc: addi x7, x10, -702
addr0x000002d0: lui x9, 421384
addr0x000002d4: addi x27, x14, -476
addr0x000002d8: andi x14, x0, 787
addr0x000002dc: slli x30, x12, 4
addr0x000002e0: xor x11, x18, x7
addr0x000002e4: lbu x24, -1138(x8)
addr0x000002e8: slli x15, x20, 27
addr0x000002ec: addi x22, x24, -813
addr0x000002f0: jal x11, addr0x0000038c
addr0x000002f4: lw x23, -1732(x8)
addr0x000002f8: lw x0, 1019(x8)
addr0x000002fc: mul x17, x9, x16
addr0x00000300: lw x23, 425(x8)
addr0x00000304: addi x6, x20, -1771
addr0x00000308: sw x19, -671(x8)
addr0x0000030c: sw x14, 477(x8)
addr0x00000310: lw x7, 167(x8)
addr0x00000314: jal x24, addr0x0000002c
addr0x00000318: jal x31, addr0x00000138
addr0x0000031c: sw x4, 349(x8)
addr0x00000320: sw x20, 467(x8)
addr0x00000324: sw x25, 1354(x8)
addr0x00000328: addi x10, x19, -560
addr0x0000032c: lw x19, 1278(x8)
addr0x00000330: sw x29, -1303(x8)
addr0x00000334: lw x29, -701(x8)
addr0x00000338: sw x7, 1661(x8)
addr0x0000033c: sw x26, 1049(x8)
addr0x00000340: lbu x19, -969(x8)
addr0x00000344: andi x25, x13, -873
addr0x00000348: lui x22, 568606
addr0x0000034c: addi x7, x7, 1738
addr0x00000350: jal x27, addr0x000000e4
addr0x00000354: lw x16, -930(x8)
addr0x00000358: sub x27, x6, x16
addr0x0000035c: addi x22, x27, 1580
addr0x00000360: addi x13, x0, -995
addr0x00000364: sw x7, 570(x8)
addr0x00000368: sw x30, -1080(x8)
addr0x0000036c: sb x14, 1488(x8)
addr0x00000370: sb x4, 356(x8)
addr0x00000374: sw x5, 407(x8)
addr0x00000378: sw x28, -298(x8)
addr0x0000037c: lui x4, 43970
addr0x00000380: addi x9, x15, -681
addr0x00000384: add x31, x10, x29
addr0x00000388: lw x5, -1505(x8)
addr0x0000038c: sub x13, x13, x5
addr0x00000390: blt x4, x9, addr0x000002fc
addr0x00000394: bge x18, x7, addr0x0000021c
addr0x00000398: lw x27, -484(x8)
addr0x0000039c: lw x30, -919(x8)
addr0x000003a0: srli x4, x29, 2
addr0x000003a4: lw x28, -2016(x8)
addr0x000003a8: jal x30, addr0x000002ac
addr0x000003ac: lw x28, -117(x8)
addr0x000003b0: addi x11, x4, 331
addr0x000003b4: addi x18, x2, -277
addr0x000003b8: jal x29, addr0x000000a0
addr0x000003bc: jal x29, addr0x00000148
addr0x000003c0: lui x19, 150601
addr0x000003c4: addi x24, x4, 725
addr0x000003c8: jal x30, addr0x000002d0
addr0x000003cc: bne x4, x12, addr0x00000028
addr0x000003d0: jal x20, addr0x00000314
addr0x000003d4: bne x14, x24, addr0x000000dc
