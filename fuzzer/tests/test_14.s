.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x24, -1770(x8)
addr0x0000001c: bgeu x6, x17, addr0x00000290
addr0x00000020: slli x19, x5, 22
addr0x00000024: addi x5, x15, -1610
addr0x00000028: lbu x20, 1768(x8)
addr0x0000002c: addi x23, x23, -957
addr0x00000030: lui x21, 450710
addr0x00000034: addi x2, x14, -1082
addr0x00000038: lbu x29, -930(x8)
addr0x0000003c: addi x4, x23, 557
addr0x00000040: lw x24, -480(x8)
addr0x00000044: sw x31, 2010(x8)
addr0x00000048: lw x20, -705(x8)
addr0x0000004c: addi x0, x23, -1930
addr0x00000050: slli x31, x18, 5
addr0x00000054: slli x29, x9, 27
addr0x00000058: srai x23, x2, 1
addr0x0000005c: sw x5, 285(x8)
addr0x00000060: addi x22, x4, 1563
addr0x00000064: sw x24, -957(x8)
addr0x00000068: lui x9, 475589
addr0x0000006c: jal x14, addr0x00000428
addr0x00000070: add x5, x6, x9
addr0x00000074: sw x5, 1651(x8)
addr0x00000078: sw x31, 1401(x8)
addr0x0000007c: lw x29, 528(x8)
addr0x00000080: lw x9, -1550(x8)
addr0x00000084: jal x13, addr0x000003b0
addr0x00000088: lbu x30, -729(x8)
addr0x0000008c: addi x6, x24, 1282
addr0x00000090: lw x30, 886(x8)
addr0x00000094: addi x21, x6, 194
addr0x00000098: jal x22, addr0x000001c0
addr0x0000009c: addi x7, x5, -215
addr0x000000a0: sw x0, -2044(x8)
addr0x000000a4: addi x18, x4, -2011
addr0x000000a8: lw x9, 1242(x8)
addr0x000000ac: lw x13, -1613(x8)
addr0x000000b0: lw x4, -283(x8)
addr0x000000b4: lw x16, -1551(x8)
addr0x000000b8: sw x22, 1245(x8)
addr0x000000bc: sw x0, -1844(x8)
addr0x000000c0: sw x29, 1472(x8)
addr0x000000c4: jal x1, addr0x000001bc
addr0x000000c8: jal x5, addr0x000004b4
addr0x000000cc: jal x19, addr0x00000198
addr0x000000d0: jal x29, addr0x000004dc
addr0x000000d4: andi x30, x20, -602
addr0x000000d8: lw x1, -1951(x8)
addr0x000000dc: jal x22, addr0x00000248
addr0x000000e0: lui x4, 89915
addr0x000000e4: sw x9, 456(x8)
addr0x000000e8: jal x25, addr0x000004d0
addr0x000000ec: lw x22, -436(x8)
addr0x000000f0: slli x22, x4, 5
addr0x000000f4: or x4, x22, x30
addr0x000000f8: slli x22, x19, 0
addr0x000000fc: sub x0, x24, x24
addr0x00000100: sw x19, -1278(x8)
addr0x00000104: sw x13, -1262(x8)
addr0x00000108: lui x27, 260058
addr0x0000010c: beq x29, x2, addr0x000000f0
addr0x00000110: addi x22, x29, 450
addr0x00000114: sw x20, 561(x8)
addr0x00000118: andi x21, x4, -950
addr0x0000011c: bgeu x13, x5, addr0x000000d8
addr0x00000120: lbu x13, -1782(x8)
addr0x00000124: lbu x15, -1652(x8)
addr0x00000128: bne x2, x23, addr0x000003bc
addr0x0000012c: jal x13, addr0x0000047c
addr0x00000130: lw x29, -195(x8)
addr0x00000134: lw x29, -1909(x8)
addr0x00000138: sw x18, -761(x8)
addr0x0000013c: lw x7, -1393(x8)
addr0x00000140: lw x23, -1916(x8)
addr0x00000144: lw x0, 1094(x8)
addr0x00000148: jal x23, addr0x00000224
addr0x0000014c: addi x13, x9, -699
addr0x00000150: bltu x23, x19, addr0x00000390
addr0x00000154: slli x19, x9, 31
addr0x00000158: lw x6, -1446(x8)
addr0x0000015c: jal x5, addr0x000001cc
addr0x00000160: bne x1, x30, addr0x00000204
addr0x00000164: lw x29, 632(x8)
addr0x00000168: addi x22, x29, -1465
addr0x0000016c: addi x7, x9, -553
addr0x00000170: add x29, x31, x29
addr0x00000174: addi x22, x1, 24
addr0x00000178: lbu x29, -2003(x8)
addr0x0000017c: sw x20, 1591(x8)
addr0x00000180: jal x0, addr0x000002fc
addr0x00000184: sw x0, -795(x8)
addr0x00000188: lw x25, 560(x8)
addr0x0000018c: sw x7, 835(x8)
addr0x00000190: lw x15, -564(x8)
addr0x00000194: jal x1, addr0x0000019c
addr0x00000198: bltu x12, x6, addr0x00000404
addr0x0000019c: lui x27, 514957
addr0x000001a0: jal x21, addr0x0000014c
addr0x000001a4: sw x18, -1187(x8)
addr0x000001a8: sw x14, -1147(x8)
addr0x000001ac: jal x31, addr0x00000044
addr0x000001b0: lw x0, -1781(x8)
addr0x000001b4: lw x18, -128(x8)
addr0x000001b8: addi x13, x27, 409
addr0x000001bc: slli x14, x23, 8
addr0x000001c0: jal x21, addr0x00000274
addr0x000001c4: lw x0, -676(x8)
addr0x000001c8: bgeu x7, x15, addr0x00000178
addr0x000001cc: slli x24, x13, 21
addr0x000001d0: srli x0, x30, 24
addr0x000001d4: add x19, x22, x18
addr0x000001d8: jal x1, addr0x00000304
addr0x000001dc: jal x5, addr0x000000bc
addr0x000001e0: lw x19, -1769(x8)
addr0x000001e4: andi x7, x22, 1940
addr0x000001e8: lbu x0, 1686(x8)
addr0x000001ec: slli x0, x28, 15
addr0x000001f0: lw x30, 499(x8)
addr0x000001f4: jal x27, addr0x0000043c
addr0x000001f8: sw x29, -1613(x8)
addr0x000001fc: addi x23, x19, -1448
addr0x00000200: sw x1, -447(x8)
addr0x00000204: jal x4, addr0x00000330
addr0x00000208: bne x15, x19, addr0x00000280
addr0x0000020c: sw x0, 1603(x8)
addr0x00000210: sw x4, -770(x8)
addr0x00000214: sw x19, -866(x8)
addr0x00000218: lw x7, 130(x8)
addr0x0000021c: addi x9, x18, 1038
addr0x00000220: addi x2, x27, -1476
addr0x00000224: addi x9, x4, -1461
addr0x00000228: sw x13, -679(x8)
addr0x0000022c: lui x28, 918612
addr0x00000230: lw x19, 826(x8)
addr0x00000234: lw x14, 574(x8)
addr0x00000238: lw x0, -357(x8)
addr0x0000023c: sw x16, 376(x8)
addr0x00000240: beq x19, x7, addr0x00000160
addr0x00000244: lw x19, 1892(x8)
addr0x00000248: jal x14, addr0x00000008
addr0x0000024c: jal x9, addr0x0000049c
addr0x00000250: bne x9, x26, addr0x00000424
addr0x00000254: sb x27, 300(x8)
addr0x00000258: sub x18, x9, x2
addr0x0000025c: add x20, x29, x21
addr0x00000260: jal x18, addr0x000002d4
addr0x00000264: sb x27, 1440(x8)
addr0x00000268: addi x23, x13, -1657
addr0x0000026c: addi x21, x0, 1478
addr0x00000270: addi x19, x20, -762
addr0x00000274: sw x24, 1205(x8)
addr0x00000278: jal x29, addr0x000003fc
addr0x0000027c: blt x6, x27, addr0x00000330
addr0x00000280: sh x2, 1330(x8)
addr0x00000284: sb x21, -1120(x8)
addr0x00000288: sw x19, -1616(x8)
addr0x0000028c: addi x30, x30, -1199
addr0x00000290: sw x14, 991(x8)
addr0x00000294: addi x22, x20, 2040
addr0x00000298: sub x5, x21, x13
addr0x0000029c: add x29, x30, x19
addr0x000002a0: lbu x7, 1110(x8)
addr0x000002a4: add x13, x0, x19
addr0x000002a8: add x5, x31, x9
addr0x000002ac: lbu x14, 75(x8)
addr0x000002b0: beq x29, x18, addr0x00000108
addr0x000002b4: lw x29, 687(x8)
addr0x000002b8: lw x19, -889(x8)
addr0x000002bc: lw x29, -908(x8)
addr0x000002c0: sub x18, x14, x5
addr0x000002c4: addi x24, x17, -213
addr0x000002c8: jal x7, addr0x0000031c
addr0x000002cc: sw x25, -1824(x8)
addr0x000002d0: sw x13, -1206(x8)
addr0x000002d4: lbu x14, -489(x8)
addr0x000002d8: slli x31, x22, 21
addr0x000002dc: andi x23, x17, -745
addr0x000002e0: bltu x7, x6, addr0x00000394
addr0x000002e4: blt x9, x7, addr0x000004a0
addr0x000002e8: jal x21, addr0x000003f0
addr0x000002ec: lw x7, 593(x8)
addr0x000002f0: sw x0, -1809(x8)
addr0x000002f4: addi x31, x27, 1199
addr0x000002f8: jal x21, addr0x00000480
addr0x000002fc: lw x19, 476(x8)
addr0x00000300: lw x21, 743(x8)
addr0x00000304: bltu x23, x4, addr0x0000026c
addr0x00000308: lw x22, 1649(x8)
addr0x0000030c: add x25, x31, x2
addr0x00000310: slli x11, x19, 8
addr0x00000314: slli x29, x22, 12
addr0x00000318: lw x22, -1003(x8)
addr0x0000031c: lw x17, 629(x8)
addr0x00000320: addi x25, x1, -1533
addr0x00000324: sw x19, 522(x8)
addr0x00000328: sw x19, -820(x8)
addr0x0000032c: sw x22, -751(x8)
addr0x00000330: jal x19, addr0x000002e8
addr0x00000334: sh x30, -688(x8)
addr0x00000338: sw x18, 1193(x8)
addr0x0000033c: sw x0, -854(x8)
addr0x00000340: sw x6, 976(x8)
addr0x00000344: sw x12, 1708(x8)
addr0x00000348: addi x11, x25, 744
addr0x0000034c: sw x30, -359(x8)
addr0x00000350: bge x24, x7, addr0x000004a0
addr0x00000354: lbu x4, -1349(x8)
addr0x00000358: bne x17, x18, addr0x000001c4
addr0x0000035c: lw x24, 1734(x8)
addr0x00000360: bgeu x0, x2, addr0x00000140
addr0x00000364: lw x18, 1147(x8)
addr0x00000368: lw x23, 1482(x8)
addr0x0000036c: sw x19, -1317(x8)
addr0x00000370: lw x16, 1479(x8)
addr0x00000374: sw x14, 482(x8)
addr0x00000378: addi x19, x17, -1261
addr0x0000037c: sw x24, -171(x8)
addr0x00000380: addi x1, x23, -389
addr0x00000384: jal x18, addr0x0000012c
addr0x00000388: lui x19, 933363
addr0x0000038c: addi x24, x9, 181
addr0x00000390: addi x22, x27, 1041
addr0x00000394: lui x17, 624462
addr0x00000398: addi x7, x22, -756
addr0x0000039c: addi x23, x11, -1915
addr0x000003a0: addi x19, x27, 275
addr0x000003a4: jal x22, addr0x000003f0
addr0x000003a8: jal x30, addr0x00000214
addr0x000003ac: lw x24, 452(x8)
addr0x000003b0: add x1, x21, x27
addr0x000003b4: sw x19, 241(x8)
addr0x000003b8: addi x10, x31, -690
addr0x000003bc: sw x0, 1143(x8)
addr0x000003c0: jal x2, addr0x00000378
addr0x000003c4: addi x9, x27, 1750
addr0x000003c8: sw x7, -1533(x8)
addr0x000003cc: sw x30, -296(x8)
addr0x000003d0: lw x4, 1909(x8)
addr0x000003d4: lw x10, -1003(x8)
addr0x000003d8: addi x30, x16, 568
addr0x000003dc: lui x5, 208826
addr0x000003e0: addi x12, x23, -542
addr0x000003e4: jal x7, addr0x000004b0
addr0x000003e8: addi x29, x21, 1498
addr0x000003ec: lw x14, 1428(x8)
addr0x000003f0: jal x29, addr0x0000045c
addr0x000003f4: lw x27, 1552(x8)
addr0x000003f8: sb x9, -1992(x8)
addr0x000003fc: jal x7, addr0x0000020c
addr0x00000400: jal x2, addr0x0000005c
addr0x00000404: addi x17, x27, -1115
addr0x00000408: sw x0, 1544(x8)
addr0x0000040c: sw x2, 276(x8)
addr0x00000410: jal x4, addr0x00000184
addr0x00000414: addi x9, x7, -1452
addr0x00000418: add x31, x0, x19
addr0x0000041c: lbu x19, 2014(x8)
addr0x00000420: beq x4, x29, addr0x000001e4
addr0x00000424: sw x9, 146(x8)
addr0x00000428: bne x25, x6, addr0x00000418
addr0x0000042c: beq x6, x0, addr0x00000420
addr0x00000430: lui x22, 81009
addr0x00000434: addi x13, x18, -107
addr0x00000438: sw x25, -696(x8)
addr0x0000043c: sw x16, -578(x8)
addr0x00000440: sw x5, -1612(x8)
addr0x00000444: lw x22, 546(x8)
addr0x00000448: sub x16, x30, x29
addr0x0000044c: sub x29, x31, x23
addr0x00000450: bne x24, x6, addr0x000000f0
addr0x00000454: lw x23, -711(x8)
addr0x00000458: lui x30, 305381
addr0x0000045c: addi x31, x15, 370
addr0x00000460: jal x6, addr0x0000027c
addr0x00000464: lw x9, 290(x8)
addr0x00000468: beq x27, x20, addr0x00000404
addr0x0000046c: lw x7, 389(x8)
addr0x00000470: bltu x28, x30, addr0x00000224
addr0x00000474: lw x20, -380(x8)
addr0x00000478: lbu x11, -742(x8)
addr0x0000047c: lbu x20, -1735(x8)
addr0x00000480: slli x5, x31, 6
addr0x00000484: add x14, x7, x20
addr0x00000488: jal x17, addr0x00000238
addr0x0000048c: sb x4, -1656(x8)
addr0x00000490: addi x30, x7, 1482
addr0x00000494: lw x29, -1949(x8)
addr0x00000498: beq x30, x21, addr0x00000338
addr0x0000049c: beq x11, x29, addr0x0000029c
addr0x000004a0: bne x2, x31, addr0x00000408
addr0x000004a4: bne x31, x24, addr0x000002f0
addr0x000004a8: sw x7, 475(x8)
addr0x000004ac: sw x1, 1728(x8)
addr0x000004b0: lw x23, -1542(x8)
addr0x000004b4: addi x30, x31, 831
addr0x000004b8: sw x27, -1919(x8)
addr0x000004bc: sw x30, 223(x8)
addr0x000004c0: lw x0, 119(x8)
addr0x000004c4: lw x23, 1074(x8)
addr0x000004c8: lw x22, -425(x8)
addr0x000004cc: addi x9, x29, 1324
addr0x000004d0: jal x5, addr0x00000188
addr0x000004d4: lw x2, -1231(x8)
addr0x000004d8: lw x23, 1552(x8)
addr0x000004dc: andi x2, x15, -521
addr0x000004e0: bne x18, x17, addr0x000004d8
addr0x000004e4: addi x4, x17, -1478
addr0x000004e8: beq x29, x13, addr0x000001d4
addr0x000004ec: sw x19, 473(x8)
addr0x000004f0: jal x31, addr0x000000f8
