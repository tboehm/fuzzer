.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bgeu x1, x5, addr0x00000108
addr0x0000001c: andi x9, x7, -658
addr0x00000020: lw x19, 446(x8)
addr0x00000024: addi x0, x13, 1161
addr0x00000028: sb x0, -1894(x8)
addr0x0000002c: jal x22, addr0x000001e8
addr0x00000030: lw x9, 28(x8)
addr0x00000034: lw x31, -227(x8)
addr0x00000038: lw x14, 382(x8)
addr0x0000003c: lw x23, -1931(x8)
addr0x00000040: jal x19, addr0x00000308
addr0x00000044: lw x2, -1502(x8)
addr0x00000048: sw x19, 1742(x8)
addr0x0000004c: beq x22, x17, addr0x000002a0
addr0x00000050: andi x9, x19, 1568
addr0x00000054: addi x0, x18, 867
addr0x00000058: lw x0, 720(x8)
addr0x0000005c: lw x30, 1452(x8)
addr0x00000060: lw x9, -1835(x8)
addr0x00000064: addi x14, x18, 1517
addr0x00000068: jal x0, addr0x00000278
addr0x0000006c: lw x27, 327(x8)
addr0x00000070: jal x22, addr0x0000016c
addr0x00000074: jal x23, addr0x00000088
addr0x00000078: addi x6, x12, 404
addr0x0000007c: bge x1, x31, addr0x000002f0
addr0x00000080: beq x11, x20, addr0x000001cc
addr0x00000084: beq x15, x22, addr0x00000260
addr0x00000088: addi x7, x30, -1969
addr0x0000008c: sw x6, 93(x8)
addr0x00000090: sw x5, -1954(x8)
addr0x00000094: addi x0, x29, 1486
addr0x00000098: bne x19, x6, addr0x00000268
addr0x0000009c: lw x29, 409(x8)
addr0x000000a0: lw x22, 1508(x8)
addr0x000000a4: lw x4, -67(x8)
addr0x000000a8: lbu x0, 1436(x8)
addr0x000000ac: lbu x20, 883(x8)
addr0x000000b0: jal x24, addr0x00000204
addr0x000000b4: sub x29, x9, x4
addr0x000000b8: srai x18, x11, 18
addr0x000000bc: addi x22, x29, -1830
addr0x000000c0: lui x15, 1045105
addr0x000000c4: sw x14, 958(x8)
addr0x000000c8: sw x30, 133(x8)
addr0x000000cc: sw x15, 445(x8)
addr0x000000d0: sb x12, 190(x8)
addr0x000000d4: jal x18, addr0x00000180
addr0x000000d8: addi x30, x0, 818
addr0x000000dc: sw x18, -1486(x8)
addr0x000000e0: jal x23, addr0x000002c4
addr0x000000e4: lw x17, -18(x8)
addr0x000000e8: addi x19, x20, 1523
addr0x000000ec: addi x21, x23, -1678
addr0x000000f0: sub x17, x15, x24
addr0x000000f4: srai x16, x6, 17
addr0x000000f8: jal x7, addr0x00000064
addr0x000000fc: lw x31, 367(x8)
addr0x00000100: lw x1, 1315(x8)
addr0x00000104: addi x19, x23, -1763
addr0x00000108: jal x19, addr0x00000170
addr0x0000010c: jal x21, addr0x0000002c
addr0x00000110: sw x9, 1330(x8)
addr0x00000114: sw x27, -1262(x8)
addr0x00000118: jal x2, addr0x00000044
addr0x0000011c: lui x13, 190702
addr0x00000120: addi x9, x2, -703
addr0x00000124: beq x22, x13, addr0x00000178
addr0x00000128: jal x11, addr0x0000028c
addr0x0000012c: lw x5, 1701(x8)
addr0x00000130: addi x2, x5, -1945
addr0x00000134: lui x29, 497246
addr0x00000138: lui x20, 232270
addr0x0000013c: addi x5, x30, -1897
addr0x00000140: lw x13, 1518(x8)
addr0x00000144: andi x0, x13, -602
addr0x00000148: lw x29, -1023(x8)
addr0x0000014c: sw x24, -1434(x8)
addr0x00000150: lbu x24, -1041(x8)
addr0x00000154: lbu x18, 468(x8)
addr0x00000158: andi x0, x22, 1673
addr0x0000015c: addi x18, x4, 1607
addr0x00000160: bge x1, x24, addr0x00000014
addr0x00000164: lui x31, 179171
addr0x00000168: addi x16, x9, 1912
addr0x0000016c: lbu x11, 797(x8)
addr0x00000170: beq x5, x22, addr0x0000001c
addr0x00000174: lbu x2, 616(x8)
addr0x00000178: beq x24, x22, addr0x00000174
addr0x0000017c: lw x22, -799(x8)
addr0x00000180: lw x7, -1469(x8)
addr0x00000184: lw x31, 1287(x8)
addr0x00000188: lw x29, 604(x8)
addr0x0000018c: jal x30, addr0x000001dc
addr0x00000190: jal x16, addr0x00000248
addr0x00000194: lw x13, -582(x8)
addr0x00000198: addi x19, x29, -451
addr0x0000019c: lw x0, -1499(x8)
addr0x000001a0: lw x9, -291(x8)
addr0x000001a4: jal x18, addr0x00000120
addr0x000001a8: lw x14, 179(x8)
addr0x000001ac: lw x21, 1540(x8)
addr0x000001b0: lw x22, -784(x8)
addr0x000001b4: lw x5, 327(x8)
addr0x000001b8: sw x19, -1362(x8)
addr0x000001bc: sb x13, -1351(x8)
addr0x000001c0: lw x16, 1682(x8)
addr0x000001c4: andi x19, x1, -720
addr0x000001c8: bne x18, x13, addr0x000002d8
addr0x000001cc: andi x29, x20, -1576
addr0x000001d0: or x0, x30, x9
addr0x000001d4: or x19, x31, x29
addr0x000001d8: sw x31, 238(x8)
addr0x000001dc: addi x0, x4, -1551
addr0x000001e0: jal x1, addr0x000000c8
addr0x000001e4: lw x19, -70(x8)
addr0x000001e8: lbu x31, -1439(x8)
addr0x000001ec: addi x21, x2, -1667
addr0x000001f0: jal x1, addr0x000002d8
addr0x000001f4: sw x4, 531(x8)
addr0x000001f8: sw x6, 880(x8)
addr0x000001fc: lw x31, -333(x8)
addr0x00000200: lw x18, -1585(x8)
addr0x00000204: lw x1, -133(x8)
addr0x00000208: lw x17, -1600(x8)
addr0x0000020c: add x11, x13, x23
addr0x00000210: sub x0, x23, x15
addr0x00000214: beq x13, x21, addr0x00000154
addr0x00000218: jal x21, addr0x00000154
addr0x0000021c: jal x31, addr0x000001c4
addr0x00000220: auipc x0, 623569
addr0x00000224: addi x4, x12, -1450
addr0x00000228: sw x21, 1979(x8)
addr0x0000022c: addi x9, x19, -652
addr0x00000230: sh x0, -1747(x8)
addr0x00000234: beq x21, x29, addr0x000000f8
addr0x00000238: jal x17, addr0x000001f4
addr0x0000023c: lui x12, 305175
addr0x00000240: addi x16, x14, 374
addr0x00000244: jal x21, addr0x000001c0
addr0x00000248: sw x22, -1014(x8)
addr0x0000024c: lw x29, -991(x8)
addr0x00000250: lhu x1, 1972(x8)
addr0x00000254: addi x13, x13, -1490
addr0x00000258: lui x13, 1039555
addr0x0000025c: addi x20, x0, -393
addr0x00000260: lui x9, 787962
addr0x00000264: addi x19, x22, 1662
addr0x00000268: jal x30, addr0x00000260
addr0x0000026c: lw x22, -1774(x8)
addr0x00000270: sw x18, 1310(x8)
addr0x00000274: bgeu x11, x19, addr0x00000104
addr0x00000278: lw x15, -629(x8)
addr0x0000027c: addi x13, x19, 1700
addr0x00000280: blt x14, x18, addr0x000002c4
addr0x00000284: lw x5, -1675(x8)
addr0x00000288: addi x30, x17, -299
addr0x0000028c: addi x7, x4, -2005
addr0x00000290: addi x23, x5, -1995
addr0x00000294: andi x0, x1, -25
addr0x00000298: lw x19, -1161(x8)
addr0x0000029c: lw x29, -279(x8)
addr0x000002a0: lw x16, 1110(x8)
addr0x000002a4: lw x19, 341(x8)
addr0x000002a8: add x6, x7, x22
addr0x000002ac: bgeu x29, x13, addr0x00000004
addr0x000002b0: lw x14, -1712(x8)
addr0x000002b4: andi x6, x22, -198
addr0x000002b8: sw x4, 1719(x8)
addr0x000002bc: lui x11, 659525
addr0x000002c0: lw x0, 1092(x8)
addr0x000002c4: jal x29, addr0x000000f4
addr0x000002c8: lw x1, 1117(x8)
addr0x000002cc: lui x22, 534781
addr0x000002d0: andi x24, x21, 1150
addr0x000002d4: lw x18, -93(x8)
addr0x000002d8: addi x4, x5, 697
addr0x000002dc: sw x30, -884(x8)
addr0x000002e0: lw x4, -335(x8)
addr0x000002e4: beq x20, x31, addr0x00000074
addr0x000002e8: lw x23, -899(x8)
addr0x000002ec: lw x15, 1978(x8)
addr0x000002f0: addi x0, x29, -2015
addr0x000002f4: lw x13, -202(x8)
addr0x000002f8: lw x24, -419(x8)
addr0x000002fc: lw x6, 418(x8)
addr0x00000300: lw x0, -1503(x8)
addr0x00000304: divu x9, x5, x31
addr0x00000308: lbu x29, -94(x8)
addr0x0000030c: addi x11, x1, -919
addr0x00000310: auipc x2, 1024028
addr0x00000314: addi x14, x4, 747
