.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x15, 446(x8)
addr0x0000001c: lw x9, 1544(x8)
addr0x00000020: jal x11, addr0x00000204
addr0x00000024: sw x22, -1978(x8)
addr0x00000028: jal x0, addr0x000000d8
addr0x0000002c: lw x24, 229(x8)
addr0x00000030: jal x21, addr0x000000fc
addr0x00000034: lw x4, 1588(x8)
addr0x00000038: sw x31, 987(x8)
addr0x0000003c: lw x4, -1795(x8)
addr0x00000040: lw x30, -1524(x8)
addr0x00000044: lw x4, -1471(x8)
addr0x00000048: lui x5, 900976
addr0x0000004c: andi x19, x18, -555
addr0x00000050: jal x30, addr0x000002e0
addr0x00000054: jal x22, addr0x000008d8
addr0x00000058: lbu x20, -1104(x8)
addr0x0000005c: lbu x2, 888(x8)
addr0x00000060: sw x20, -1522(x8)
addr0x00000064: lw x20, 1743(x8)
addr0x00000068: lw x0, -993(x8)
addr0x0000006c: sw x10, 1087(x8)
addr0x00000070: sw x14, 1737(x8)
addr0x00000074: sw x29, 1513(x8)
addr0x00000078: addi x11, x14, 205
addr0x0000007c: addi x27, x17, 1555
addr0x00000080: jal x13, addr0x00000998
addr0x00000084: lw x28, -364(x8)
addr0x00000088: lw x4, -1980(x8)
addr0x0000008c: lw x18, -915(x8)
addr0x00000090: addi x28, x17, -973
addr0x00000094: sw x14, -1289(x8)
addr0x00000098: sw x30, 1317(x8)
addr0x0000009c: addi x24, x28, -957
addr0x000000a0: jal x29, addr0x00000604
addr0x000000a4: addi x19, x20, -694
addr0x000000a8: sw x18, 639(x8)
addr0x000000ac: jal x29, addr0x000001cc
addr0x000000b0: sw x13, 571(x8)
addr0x000000b4: lw x22, -1399(x8)
addr0x000000b8: lw x13, 979(x8)
addr0x000000bc: addi x9, x7, -1143
addr0x000000c0: sw x15, -141(x8)
addr0x000000c4: addi x13, x27, -778
addr0x000000c8: addi x20, x25, 4
addr0x000000cc: jal x27, addr0x000001ec
addr0x000000d0: addi x9, x5, 1212
addr0x000000d4: addi x11, x22, 998
addr0x000000d8: add x30, x19, x29
addr0x000000dc: sub x20, x0, x30
addr0x000000e0: sub x1, x13, x24
addr0x000000e4: bgeu x0, x14, addr0x0000035c
addr0x000000e8: beq x23, x17, addr0x0000008c
addr0x000000ec: slli x20, x22, 18
addr0x000000f0: srli x15, x31, 18
addr0x000000f4: add x30, x27, x0
addr0x000000f8: slli x17, x22, 31
addr0x000000fc: lw x20, -401(x8)
addr0x00000100: bltu x26, x19, addr0x000002a4
addr0x00000104: andi x1, x26, 551
addr0x00000108: lw x19, -271(x8)
addr0x0000010c: addi x7, x7, 622
addr0x00000110: sw x7, -251(x8)
addr0x00000114: jal x20, addr0x000009c0
addr0x00000118: lw x28, -870(x8)
addr0x0000011c: lw x10, 403(x8)
addr0x00000120: addi x11, x15, 1802
addr0x00000124: jal x23, addr0x00000190
addr0x00000128: jal x18, addr0x0000093c
addr0x0000012c: lw x10, -1339(x8)
addr0x00000130: lw x7, -390(x8)
addr0x00000134: lw x26, 1588(x8)
addr0x00000138: jalr x0, 656(x3)
addr0x0000013c: beq x19, x29, addr0x00000474
addr0x00000140: lw x24, -1091(x8)
addr0x00000144: lw x19, -418(x8)
addr0x00000148: addi x19, x7, 640
addr0x0000014c: jal x20, addr0x0000015c
addr0x00000150: jal x0, addr0x000008e0
addr0x00000154: slli x30, x17, 11
addr0x00000158: add x23, x1, x23
addr0x0000015c: lw x18, 1143(x8)
addr0x00000160: sub x24, x11, x19
addr0x00000164: lw x0, 963(x8)
addr0x00000168: addi x24, x13, 1853
addr0x0000016c: jal x17, addr0x00000684
addr0x00000170: jal x23, addr0x00000948
addr0x00000174: jal x31, addr0x00000468
addr0x00000178: jal x30, addr0x00000540
addr0x0000017c: jal x24, addr0x000006c8
addr0x00000180: ori x6, x14, -528
addr0x00000184: sw x30, -617(x8)
addr0x00000188: jal x19, addr0x000004b8
addr0x0000018c: lw x7, -1019(x8)
addr0x00000190: lw x20, -1301(x8)
addr0x00000194: jalr x0, 380(x3)
addr0x00000198: beq x23, x19, addr0x000003b0
addr0x0000019c: lbu x4, 983(x8)
addr0x000001a0: sb x5, -812(x8)
addr0x000001a4: lbu x28, 360(x8)
addr0x000001a8: sub x9, x14, x13
addr0x000001ac: sub x14, x31, x30
addr0x000001b0: bgeu x12, x20, addr0x00000760
addr0x000001b4: slli x18, x29, 21
addr0x000001b8: lbu x9, -826(x8)
addr0x000001bc: addi x28, x23, 1807
addr0x000001c0: jal x13, addr0x00000680
addr0x000001c4: jal x18, addr0x0000014c
addr0x000001c8: jal x10, addr0x000009b8
addr0x000001cc: bltu x15, x27, addr0x0000066c
addr0x000001d0: lw x6, -365(x8)
addr0x000001d4: ori x1, x21, -1659
addr0x000001d8: sw x14, -1450(x8)
addr0x000001dc: lw x22, -1761(x8)
addr0x000001e0: lui x27, 645499
addr0x000001e4: addi x18, x14, -448
addr0x000001e8: jal x23, addr0x000000b8
addr0x000001ec: sw x23, 1913(x8)
addr0x000001f0: lbu x28, 1223(x8)
addr0x000001f4: lw x19, -749(x8)
addr0x000001f8: addi x4, x23, -1670
addr0x000001fc: lhu x19, 1079(x8)
addr0x00000200: lhu x13, 392(x8)
addr0x00000204: lhu x7, -2047(x8)
addr0x00000208: lw x5, -1971(x8)
addr0x0000020c: sw x19, 627(x8)
addr0x00000210: sw x2, 1521(x8)
addr0x00000214: jal x21, addr0x000004f0
addr0x00000218: beq x28, x22, addr0x0000074c
addr0x0000021c: bne x28, x30, addr0x00000054
addr0x00000220: lui x19, 256494
addr0x00000224: addi x16, x22, 899
addr0x00000228: lw x7, 1424(x8)
addr0x0000022c: bne x22, x18, addr0x000000e4
addr0x00000230: lw x0, 605(x8)
addr0x00000234: sub x18, x7, x5
addr0x00000238: beq x11, x15, addr0x0000037c
addr0x0000023c: beq x9, x27, addr0x00000430
addr0x00000240: jal x27, addr0x00000330
addr0x00000244: lw x31, -1752(x8)
addr0x00000248: addi x16, x31, 1760
addr0x0000024c: sw x11, -426(x8)
addr0x00000250: jal x20, addr0x000005c4
addr0x00000254: lui x0, 322346
addr0x00000258: addi x13, x22, -1219
addr0x0000025c: lw x22, -1738(x8)
addr0x00000260: jal x13, addr0x00000674
addr0x00000264: lw x22, 957(x8)
addr0x00000268: addi x23, x2, 1599
addr0x0000026c: jal x26, addr0x000000e8
addr0x00000270: sw x12, -1557(x8)
addr0x00000274: jal x9, addr0x00000814
addr0x00000278: sw x27, 1692(x8)
addr0x0000027c: bgeu x19, x27, addr0x00000328
addr0x00000280: slli x5, x29, 17
addr0x00000284: lw x27, 2009(x8)
addr0x00000288: sltu x23, x4, x17
addr0x0000028c: or x10, x4, x6
addr0x00000290: addi x17, x27, -1480
addr0x00000294: lw x18, 1911(x8)
addr0x00000298: lw x21, 56(x8)
addr0x0000029c: addi x21, x5, 175
addr0x000002a0: sw x22, 654(x8)
addr0x000002a4: sw x5, -1440(x8)
addr0x000002a8: jalr x0, 36(x3)
addr0x000002ac: add x24, x30, x18
addr0x000002b0: sw x27, -516(x8)
addr0x000002b4: sw x11, -1570(x8)
addr0x000002b8: jal x10, addr0x000003c8
addr0x000002bc: lw x27, -326(x8)
addr0x000002c0: addi x14, x7, 922
addr0x000002c4: lw x21, 834(x8)
addr0x000002c8: addi x4, x14, -224
addr0x000002cc: addi x16, x18, 1427
addr0x000002d0: sw x15, 643(x8)
addr0x000002d4: jal x25, addr0x000000fc
addr0x000002d8: lw x13, -998(x8)
addr0x000002dc: addi x16, x21, -1605
addr0x000002e0: jal x28, addr0x00000534
addr0x000002e4: lw x4, -652(x8)
addr0x000002e8: add x22, x4, x20
addr0x000002ec: bltu x23, x25, addr0x000001fc
addr0x000002f0: addi x9, x7, 1183
addr0x000002f4: jal x23, addr0x000002a0
addr0x000002f8: lui x20, 288017
addr0x000002fc: sw x23, -1787(x8)
addr0x00000300: lw x24, -738(x8)
addr0x00000304: lw x10, -1550(x8)
addr0x00000308: sw x5, 1816(x8)
addr0x0000030c: sw x17, -829(x8)
addr0x00000310: addi x13, x22, -319
addr0x00000314: lui x25, 588266
addr0x00000318: lw x29, -811(x8)
addr0x0000031c: lbu x31, -1042(x8)
addr0x00000320: andi x12, x14, 1345
addr0x00000324: lw x12, -755(x8)
addr0x00000328: addi x31, x27, -533
addr0x0000032c: addi x27, x7, 1483
addr0x00000330: jal x13, addr0x000003a8
addr0x00000334: lui x29, 321118
addr0x00000338: lw x31, -472(x8)
addr0x0000033c: srli x27, x23, 30
addr0x00000340: bgeu x28, x19, addr0x000004b8
addr0x00000344: andi x19, x4, 1941
addr0x00000348: jal x13, addr0x00000430
addr0x0000034c: sw x10, -33(x8)
addr0x00000350: jal x29, addr0x000004c8
addr0x00000354: lw x29, 600(x8)
addr0x00000358: lw x27, 1287(x8)
addr0x0000035c: addi x9, x29, -806
addr0x00000360: sw x18, -820(x8)
addr0x00000364: addi x21, x0, 491
addr0x00000368: jal x26, addr0x0000042c
addr0x0000036c: lw x4, -2034(x8)
addr0x00000370: addi x24, x7, -16
addr0x00000374: sh x21, 710(x8)
addr0x00000378: lhu x31, 1569(x8)
addr0x0000037c: andi x0, x25, -535
addr0x00000380: addi x9, x28, 1885
addr0x00000384: beq x22, x29, addr0x0000092c
addr0x00000388: lbu x0, -1819(x8)
addr0x0000038c: slli x28, x27, 7
addr0x00000390: jal x18, addr0x000007ec
addr0x00000394: lw x18, 1226(x8)
addr0x00000398: lw x15, -1676(x8)
addr0x0000039c: sw x16, 1684(x8)
addr0x000003a0: sw x26, -901(x8)
addr0x000003a4: lw x9, 1884(x8)
addr0x000003a8: addi x0, x27, -1673
addr0x000003ac: bne x10, x12, addr0x00000490
addr0x000003b0: lw x31, -1496(x8)
addr0x000003b4: lw x22, 44(x8)
addr0x000003b8: lw x28, -43(x8)
addr0x000003bc: sw x21, -1605(x8)
addr0x000003c0: sw x21, -1901(x8)
addr0x000003c4: jal x13, addr0x00000790
addr0x000003c8: lw x17, 816(x8)
addr0x000003cc: lw x27, -2026(x8)
addr0x000003d0: lw x18, -1812(x8)
addr0x000003d4: lw x29, -1360(x8)
addr0x000003d8: bne x13, x29, addr0x000001e0
addr0x000003dc: lw x2, -1798(x8)
addr0x000003e0: jal x12, addr0x000000fc
addr0x000003e4: lw x0, -528(x8)
addr0x000003e8: bgeu x19, x23, addr0x00000704
addr0x000003ec: slli x5, x2, 27
addr0x000003f0: addi x28, x22, -1457
addr0x000003f4: sw x22, -1558(x8)
addr0x000003f8: lw x28, -1632(x8)
addr0x000003fc: addi x20, x1, -819
addr0x00000400: bne x0, x5, addr0x000008d4
addr0x00000404: sb x27, -166(x8)
addr0x00000408: sw x22, 1893(x8)
addr0x0000040c: jal x27, addr0x000004fc
addr0x00000410: lw x21, 1063(x8)
addr0x00000414: addi x22, x2, 1108
addr0x00000418: bne x27, x22, addr0x00000198
addr0x0000041c: sw x13, 863(x8)
addr0x00000420: jal x19, addr0x00000860
addr0x00000424: lw x11, 335(x8)
addr0x00000428: lw x29, -924(x8)
addr0x0000042c: beq x22, x9, addr0x000007e0
addr0x00000430: addi x20, x0, 1180
addr0x00000434: lw x6, -1419(x8)
addr0x00000438: lw x19, 1337(x8)
addr0x0000043c: sub x23, x31, x5
addr0x00000440: bgeu x29, x4, addr0x0000034c
addr0x00000444: jal x15, addr0x00000024
addr0x00000448: lh x26, -733(x8)
addr0x0000044c: addi x31, x31, -1334
addr0x00000450: addi x20, x19, -190
addr0x00000454: addi x7, x31, -979
addr0x00000458: lw x13, -473(x8)
addr0x0000045c: lw x1, 510(x8)
addr0x00000460: lw x13, -1290(x8)
addr0x00000464: lw x28, -2031(x8)
addr0x00000468: addi x24, x20, -682
addr0x0000046c: sb x19, -1129(x8)
addr0x00000470: sw x5, 807(x8)
addr0x00000474: sw x27, 603(x8)
addr0x00000478: lw x19, 163(x8)
addr0x0000047c: addi x12, x24, 811
addr0x00000480: addi x29, x13, -614
addr0x00000484: bne x7, x27, addr0x00000368
addr0x00000488: addi x19, x4, 1335
addr0x0000048c: addi x18, x27, 167
addr0x00000490: addi x27, x27, -79
addr0x00000494: sw x7, -1121(x8)
addr0x00000498: lbu x18, -813(x8)
addr0x0000049c: sw x31, 822(x8)
addr0x000004a0: addi x7, x0, -228
addr0x000004a4: addi x14, x27, -488
addr0x000004a8: jal x10, addr0x0000073c
addr0x000004ac: lbu x27, 1699(x8)
addr0x000004b0: jal x13, addr0x000008d4
addr0x000004b4: sw x29, -1976(x8)
addr0x000004b8: jal x10, addr0x00000118
addr0x000004bc: addi x0, x19, 174
addr0x000004c0: jal x15, addr0x000000cc
addr0x000004c4: addi x13, x13, 64
addr0x000004c8: addi x27, x19, 679
addr0x000004cc: jal x19, addr0x00000994
addr0x000004d0: lw x6, 1228(x8)
addr0x000004d4: jal x10, addr0x00000080
addr0x000004d8: lw x9, 1851(x8)
addr0x000004dc: jal x9, addr0x00000888
addr0x000004e0: addi x22, x10, 212
addr0x000004e4: sw x0, 2040(x8)
addr0x000004e8: addi x19, x14, 534
addr0x000004ec: addi x25, x29, 1978
addr0x000004f0: sw x16, 1933(x8)
addr0x000004f4: addi x9, x26, 1273
addr0x000004f8: sw x19, -857(x8)
addr0x000004fc: sw x9, 1479(x8)
addr0x00000500: addi x28, x18, 983
addr0x00000504: sw x23, 192(x8)
addr0x00000508: lw x25, -1563(x8)
addr0x0000050c: lw x31, -1881(x8)
addr0x00000510: lw x17, -1842(x8)
addr0x00000514: lw x1, -1949(x8)
addr0x00000518: addi x20, x20, 1089
addr0x0000051c: sh x0, -318(x8)
addr0x00000520: lbu x2, -424(x8)
addr0x00000524: bne x25, x26, addr0x000003b8
addr0x00000528: lw x24, 696(x8)
addr0x0000052c: jal x14, addr0x00000490
addr0x00000530: addi x14, x7, -1406
addr0x00000534: add x23, x6, x19
addr0x00000538: bne x7, x22, addr0x00000114
addr0x0000053c: lui x23, 737010
addr0x00000540: addi x17, x24, 1524
addr0x00000544: sw x25, -1393(x8)
addr0x00000548: sw x7, 628(x8)
addr0x0000054c: jal x30, addr0x00000560
addr0x00000550: jal x9, addr0x000008b8
addr0x00000554: lw x13, 417(x8)
addr0x00000558: lh x20, -1689(x8)
addr0x0000055c: lui x5, 27898
addr0x00000560: addi x27, x23, 1547
addr0x00000564: jal x20, addr0x0000022c
addr0x00000568: jal x13, addr0x000008e0
addr0x0000056c: lw x4, 418(x8)
addr0x00000570: jal x6, addr0x00000328
addr0x00000574: lui x1, 21289
addr0x00000578: slli x23, x9, 4
addr0x0000057c: add x7, x27, x22
addr0x00000580: jal x24, addr0x0000014c
addr0x00000584: jal x28, addr0x0000099c
addr0x00000588: sw x17, -570(x8)
addr0x0000058c: addi x17, x5, 111
addr0x00000590: addi x1, x13, 947
addr0x00000594: sw x28, 845(x8)
addr0x00000598: lw x1, 810(x8)
addr0x0000059c: addi x20, x23, -1234
addr0x000005a0: lw x14, 988(x8)
addr0x000005a4: bgeu x13, x30, addr0x00000608
addr0x000005a8: beq x27, x25, addr0x00000550
addr0x000005ac: bne x31, x20, addr0x00000618
addr0x000005b0: lhu x23, -1528(x8)
addr0x000005b4: lhu x28, 230(x8)
addr0x000005b8: lhu x18, 1827(x8)
addr0x000005bc: andi x5, x20, 1192
addr0x000005c0: lbu x9, -1285(x8)
addr0x000005c4: bne x31, x7, addr0x00000274
addr0x000005c8: lw x6, 597(x8)
addr0x000005cc: lw x14, 1833(x8)
addr0x000005d0: slli x19, x0, 26
addr0x000005d4: lui x13, 126551
addr0x000005d8: sw x31, -971(x8)
addr0x000005dc: sw x1, 362(x8)
addr0x000005e0: addi x24, x28, 59
addr0x000005e4: sw x5, -1562(x8)
addr0x000005e8: addi x2, x15, 1399
addr0x000005ec: sw x7, -500(x8)
addr0x000005f0: addi x0, x15, -241
addr0x000005f4: sw x2, 1673(x8)
addr0x000005f8: sw x1, 511(x8)
addr0x000005fc: sw x13, 361(x8)
addr0x00000600: add x31, x14, x24
addr0x00000604: jal x22, addr0x00000884
addr0x00000608: sw x21, 1639(x8)
addr0x0000060c: sw x7, 1530(x8)
addr0x00000610: sw x19, -210(x8)
addr0x00000614: jal x15, addr0x00000098
addr0x00000618: lw x20, 1456(x8)
addr0x0000061c: jal x23, addr0x000001c8
addr0x00000620: beq x5, x23, addr0x0000023c
addr0x00000624: lbu x5, 314(x8)
addr0x00000628: beq x17, x0, addr0x00000354
addr0x0000062c: lbu x27, 887(x8)
addr0x00000630: sb x29, -1372(x8)
addr0x00000634: jal x9, addr0x00000544
addr0x00000638: addi x4, x24, -1670
addr0x0000063c: sw x23, 943(x8)
addr0x00000640: lbu x15, 320(x8)
addr0x00000644: bne x22, x25, addr0x00000238
addr0x00000648: sw x4, 1838(x8)
addr0x0000064c: addi x30, x16, -718
addr0x00000650: lw x22, -1740(x8)
addr0x00000654: sw x7, 1664(x8)
addr0x00000658: jal x25, addr0x000005f0
addr0x0000065c: jal x20, addr0x00000364
addr0x00000660: lui x27, 601806
addr0x00000664: sw x10, -361(x8)
addr0x00000668: lbu x9, 216(x8)
addr0x0000066c: lw x7, -446(x8)
addr0x00000670: jal x29, addr0x00000614
addr0x00000674: lw x26, 1629(x8)
addr0x00000678: beq x29, x27, addr0x000009bc
addr0x0000067c: beq x22, x12, addr0x00000248
addr0x00000680: addi x13, x7, -667
addr0x00000684: sw x23, 985(x8)
addr0x00000688: addi x14, x22, 1154
addr0x0000068c: jal x9, addr0x00000548
addr0x00000690: lw x22, 1409(x8)
addr0x00000694: addi x27, x4, -1302
addr0x00000698: sub x10, x23, x27
addr0x0000069c: beq x10, x20, addr0x0000089c
addr0x000006a0: beq x12, x6, addr0x00000048
addr0x000006a4: lw x14, 97(x8)
addr0x000006a8: jal x30, addr0x000003d8
addr0x000006ac: jal x28, addr0x0000043c
addr0x000006b0: bge x5, x1, addr0x00000990
addr0x000006b4: srai x15, x12, 31
addr0x000006b8: srai x5, x6, 6
addr0x000006bc: lw x0, -1198(x8)
addr0x000006c0: sw x25, -608(x8)
addr0x000006c4: addi x19, x21, 1857
addr0x000006c8: sw x18, -143(x8)
addr0x000006cc: bne x4, x27, addr0x00000264
addr0x000006d0: beq x27, x17, addr0x000008d8
addr0x000006d4: sw x31, -698(x8)
addr0x000006d8: jal x14, addr0x000004e0
addr0x000006dc: lw x0, 998(x8)
addr0x000006e0: lw x15, 1029(x8)
addr0x000006e4: sw x27, -741(x8)
addr0x000006e8: addi x7, x1, 1899
addr0x000006ec: lw x17, 1404(x8)
addr0x000006f0: add x0, x4, x19
addr0x000006f4: lbu x28, 1978(x8)
addr0x000006f8: lw x15, -1867(x8)
addr0x000006fc: lw x28, -1161(x8)
addr0x00000700: lw x18, 1831(x8)
addr0x00000704: lw x31, -413(x8)
addr0x00000708: lw x7, -885(x8)
addr0x0000070c: sw x20, 1700(x8)
addr0x00000710: bne x19, x22, addr0x00000380
addr0x00000714: addi x20, x18, 1372
addr0x00000718: sw x29, -1456(x8)
addr0x0000071c: sw x14, -757(x8)
addr0x00000720: jal x25, addr0x000005e8
addr0x00000724: lw x14, -538(x8)
addr0x00000728: lw x17, 1257(x8)
addr0x0000072c: lw x23, 1801(x8)
addr0x00000730: lw x7, -1571(x8)
addr0x00000734: lw x28, 655(x8)
addr0x00000738: lw x30, 420(x8)
addr0x0000073c: lw x22, -107(x8)
addr0x00000740: lui x13, 1003485
addr0x00000744: bne x18, x0, addr0x000002f4
addr0x00000748: lui x4, 367757
addr0x0000074c: blt x13, x24, addr0x00000634
addr0x00000750: lw x21, -799(x8)
addr0x00000754: jal x31, addr0x000002ec
addr0x00000758: addi x10, x19, -1786
addr0x0000075c: sw x14, -475(x8)
addr0x00000760: lw x13, 267(x8)
addr0x00000764: lbu x13, -207(x8)
addr0x00000768: sb x12, 1471(x8)
addr0x0000076c: addi x4, x20, -63
addr0x00000770: addi x13, x9, 516
addr0x00000774: sw x4, 1976(x8)
addr0x00000778: sw x10, 1056(x8)
addr0x0000077c: bltu x24, x25, addr0x00000224
addr0x00000780: mul x31, x28, x28
addr0x00000784: jal x19, addr0x00000954
addr0x00000788: sw x20, 1860(x8)
addr0x0000078c: jal x6, addr0x00000430
addr0x00000790: sw x7, -1499(x8)
addr0x00000794: sw x27, 707(x8)
addr0x00000798: sw x28, 714(x8)
addr0x0000079c: lw x20, -616(x8)
addr0x000007a0: lw x20, 2041(x8)
addr0x000007a4: jal x23, addr0x000001fc
addr0x000007a8: lw x9, -815(x8)
addr0x000007ac: addi x27, x23, -1990
addr0x000007b0: jal x29, addr0x000007f8
addr0x000007b4: addi x22, x19, -752
addr0x000007b8: lw x0, -1483(x8)
addr0x000007bc: lw x19, -208(x8)
addr0x000007c0: addi x24, x28, 1083
addr0x000007c4: sw x26, 214(x8)
addr0x000007c8: addi x25, x29, 993
addr0x000007cc: sw x2, 1007(x8)
addr0x000007d0: jal x29, addr0x000001a4
addr0x000007d4: lw x19, -1326(x8)
addr0x000007d8: lw x7, -127(x8)
addr0x000007dc: beq x20, x24, addr0x000002e4
addr0x000007e0: lbu x20, 1368(x8)
addr0x000007e4: beq x16, x0, addr0x000007d0
addr0x000007e8: bgeu x29, x28, addr0x00000190
addr0x000007ec: lw x28, 1171(x8)
addr0x000007f0: beq x11, x11, addr0x00000824
addr0x000007f4: lbu x24, 680(x8)
addr0x000007f8: bne x9, x13, addr0x000004f0
addr0x000007fc: lbu x0, -416(x8)
addr0x00000800: andi x23, x16, 732
addr0x00000804: addi x29, x26, -940
addr0x00000808: jal x12, addr0x000002e0
addr0x0000080c: lw x31, -677(x8)
addr0x00000810: lui x18, 983963
addr0x00000814: sw x28, 1230(x8)
addr0x00000818: sw x28, 2014(x8)
addr0x0000081c: lw x19, 861(x8)
addr0x00000820: lw x24, -1890(x8)
addr0x00000824: lw x5, 746(x8)
addr0x00000828: lw x18, -760(x8)
addr0x0000082c: lui x27, 788814
addr0x00000830: addi x24, x27, 153
addr0x00000834: sw x23, -17(x8)
addr0x00000838: sw x19, 1417(x8)
addr0x0000083c: addi x16, x20, -297
addr0x00000840: remu x16, x30, x23
addr0x00000844: lw x27, -1434(x8)
addr0x00000848: lh x31, -1377(x8)
addr0x0000084c: jal x7, addr0x00000230
addr0x00000850: bgeu x20, x22, addr0x0000065c
addr0x00000854: bne x30, x18, addr0x00000300
addr0x00000858: lui x5, 961148
addr0x0000085c: addi x27, x20, 1805
addr0x00000860: sw x31, -1008(x8)
addr0x00000864: jal x18, addr0x00000038
addr0x00000868: lw x26, 1049(x8)
addr0x0000086c: lw x7, 54(x8)
addr0x00000870: add x31, x7, x1
addr0x00000874: sw x0, -1109(x8)
addr0x00000878: lbu x14, -436(x8)
addr0x0000087c: lui x13, 469962
addr0x00000880: addi x17, x0, 1798
addr0x00000884: sw x27, 239(x8)
addr0x00000888: lw x28, 903(x8)
addr0x0000088c: addi x31, x20, 327
addr0x00000890: lui x19, 940354
addr0x00000894: addi x13, x29, -1039
addr0x00000898: sw x23, -660(x8)
addr0x0000089c: lbu x19, 686(x8)
addr0x000008a0: bne x27, x22, addr0x0000067c
addr0x000008a4: lw x10, 967(x8)
addr0x000008a8: addi x7, x2, 156
addr0x000008ac: sub x31, x13, x19
addr0x000008b0: addi x19, x7, -2013
addr0x000008b4: sw x26, -800(x8)
addr0x000008b8: jal x1, addr0x00000074
addr0x000008bc: lui x0, 544100
addr0x000008c0: lui x7, 450931
addr0x000008c4: lui x5, 194930
addr0x000008c8: addi x17, x23, 1466
addr0x000008cc: beq x15, x23, addr0x00000280
addr0x000008d0: bne x0, x13, addr0x00000968
addr0x000008d4: sw x0, 2037(x8)
addr0x000008d8: sw x24, -665(x8)
addr0x000008dc: sw x13, 253(x8)
addr0x000008e0: addi x23, x10, -59
addr0x000008e4: sw x7, 135(x8)
addr0x000008e8: addi x27, x18, -1665
addr0x000008ec: jal x20, addr0x000002b8
addr0x000008f0: bge x7, x2, addr0x00000880
addr0x000008f4: jalr x0, 508(x3)
addr0x000008f8: lw x29, -78(x8)
addr0x000008fc: addi x29, x14, 1297
addr0x00000900: jal x27, addr0x0000073c
addr0x00000904: lw x13, 416(x8)
addr0x00000908: jal x13, addr0x00000734
addr0x0000090c: lw x26, -1070(x8)
addr0x00000910: lw x22, 947(x8)
addr0x00000914: lw x29, 420(x8)
addr0x00000918: sub x31, x14, x0
addr0x0000091c: sw x9, 684(x8)
addr0x00000920: lw x9, 2021(x8)
addr0x00000924: sw x30, -36(x8)
addr0x00000928: jal x7, addr0x00000864
addr0x0000092c: addi x22, x25, 405
addr0x00000930: jal x18, addr0x000000a8
addr0x00000934: sw x6, -1846(x8)
addr0x00000938: addi x24, x9, -1414
addr0x0000093c: addi x22, x7, 410
addr0x00000940: bltu x22, x9, addr0x00000714
addr0x00000944: lui x22, 330041
addr0x00000948: lui x26, 241999
addr0x0000094c: sh x1, 301(x8)
addr0x00000950: bne x15, x0, addr0x0000056c
addr0x00000954: lbu x29, -1340(x8)
addr0x00000958: beq x18, x19, addr0x000005d4
addr0x0000095c: jal x7, addr0x00000964
addr0x00000960: addi x29, x7, 554
addr0x00000964: sll x7, x25, x27
addr0x00000968: sll x13, x13, x19
addr0x0000096c: lui x14, 934930
addr0x00000970: addi x28, x30, -1258
addr0x00000974: sw x20, 1167(x8)
addr0x00000978: sw x27, 180(x8)
addr0x0000097c: jal x25, addr0x00000904
addr0x00000980: addi x2, x20, 717
addr0x00000984: addi x4, x27, -599
addr0x00000988: sw x13, -498(x8)
addr0x0000098c: sb x24, 1418(x8)
addr0x00000990: addi x9, x29, -374
addr0x00000994: jal x22, addr0x0000035c
addr0x00000998: lw x5, 639(x8)
addr0x0000099c: lw x15, 1650(x8)
addr0x000009a0: lw x17, -1178(x8)
addr0x000009a4: bltu x29, x19, addr0x00000788
addr0x000009a8: slli x0, x17, 4
addr0x000009ac: jal x0, addr0x00000164
addr0x000009b0: lw x13, -427(x8)
addr0x000009b4: lw x5, -319(x8)
addr0x000009b8: lw x21, -1944(x8)
addr0x000009bc: addi x6, x16, 1446
addr0x000009c0: sw x21, 728(x8)
