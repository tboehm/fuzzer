.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bltu x27, x13, addr0x00000554
addr0x0000001c: lw x10, 23(x8)
addr0x00000020: addi x5, x18, 1563
addr0x00000024: jal x21, addr0x00000054
addr0x00000028: sub x4, x10, x4
addr0x0000002c: lw x13, -419(x8)
addr0x00000030: lw x5, -1335(x8)
addr0x00000034: lw x13, -1539(x8)
addr0x00000038: addi x30, x22, 1598
addr0x0000003c: bge x2, x15, addr0x000005d8
addr0x00000040: bltu x13, x30, addr0x00000104
addr0x00000044: mul x14, x9, x19
addr0x00000048: mul x0, x22, x30
addr0x0000004c: add x21, x19, x22
addr0x00000050: addi x4, x17, 1667
addr0x00000054: jal x17, addr0x000001f0
addr0x00000058: lw x31, 1882(x8)
addr0x0000005c: lw x9, 333(x8)
addr0x00000060: lw x29, -1367(x8)
addr0x00000064: lbu x5, -1088(x8)
addr0x00000068: add x23, x23, x20
addr0x0000006c: jal x30, addr0x0000014c
addr0x00000070: lw x23, -725(x8)
addr0x00000074: jal x11, addr0x000005e8
addr0x00000078: addi x11, x9, -693
addr0x0000007c: sw x13, -454(x8)
addr0x00000080: addi x19, x31, 1243
addr0x00000084: sw x9, 880(x8)
addr0x00000088: lw x22, 1695(x8)
addr0x0000008c: lw x23, 1508(x8)
addr0x00000090: lbu x28, -1637(x8)
addr0x00000094: andi x4, x2, 1591
addr0x00000098: lbu x15, 116(x8)
addr0x0000009c: add x4, x23, x27
addr0x000000a0: lw x24, 1666(x8)
addr0x000000a4: andi x17, x7, -1387
addr0x000000a8: beq x0, x14, addr0x00000120
addr0x000000ac: lw x18, 1271(x8)
addr0x000000b0: jal x23, addr0x000006b4
addr0x000000b4: addi x24, x9, -1972
addr0x000000b8: lui x16, 535644
addr0x000000bc: addi x1, x0, 1302
addr0x000000c0: sw x22, -89(x8)
addr0x000000c4: lw x14, 2044(x8)
addr0x000000c8: addi x22, x13, -1388
addr0x000000cc: lw x5, -652(x8)
addr0x000000d0: lw x6, 448(x8)
addr0x000000d4: addi x29, x31, -730
addr0x000000d8: sw x9, -188(x8)
addr0x000000dc: lw x23, -370(x8)
addr0x000000e0: addi x10, x14, 1168
addr0x000000e4: addi x9, x13, 795
addr0x000000e8: sw x13, -177(x8)
addr0x000000ec: sb x30, -370(x8)
addr0x000000f0: sub x22, x13, x23
addr0x000000f4: bgeu x9, x30, addr0x000002cc
addr0x000000f8: addi x0, x14, -1616
addr0x000000fc: sw x14, -2030(x8)
addr0x00000100: lw x20, 1498(x8)
addr0x00000104: jal x7, addr0x000004cc
addr0x00000108: lw x17, -27(x8)
addr0x0000010c: addi x19, x20, 221
addr0x00000110: sub x13, x17, x9
addr0x00000114: lw x9, 446(x8)
addr0x00000118: sw x18, 670(x8)
addr0x0000011c: lw x19, -166(x8)
addr0x00000120: lw x19, -882(x8)
addr0x00000124: lw x4, -2015(x8)
addr0x00000128: lw x0, 1756(x8)
addr0x0000012c: addi x30, x13, 220
addr0x00000130: sw x15, 1415(x8)
addr0x00000134: sw x7, 1544(x8)
addr0x00000138: sw x22, -1472(x8)
addr0x0000013c: jal x0, addr0x00000280
addr0x00000140: jal x9, addr0x00000318
addr0x00000144: addi x7, x21, -1004
addr0x00000148: sw x16, 1278(x8)
addr0x0000014c: sw x30, 1464(x8)
addr0x00000150: addi x30, x13, -1956
addr0x00000154: bne x24, x24, addr0x0000060c
addr0x00000158: addi x20, x1, -287
addr0x0000015c: beq x27, x12, addr0x000006b0
addr0x00000160: slli x11, x21, 3
addr0x00000164: sh x6, -732(x8)
addr0x00000168: bne x13, x18, addr0x00000414
addr0x0000016c: lbu x18, 353(x8)
addr0x00000170: addi x29, x30, 909
addr0x00000174: sw x27, -1094(x8)
addr0x00000178: sw x2, -1096(x8)
addr0x0000017c: sub x25, x13, x4
addr0x00000180: bgeu x29, x28, addr0x000001ac
addr0x00000184: lw x7, 425(x8)
addr0x00000188: lw x19, 1420(x8)
addr0x0000018c: lbu x25, 1658(x8)
addr0x00000190: bne x30, x2, addr0x00000298
addr0x00000194: addi x0, x5, 288
addr0x00000198: slli x28, x0, 1
addr0x0000019c: sh x22, 1755(x8)
addr0x000001a0: lw x9, -134(x8)
addr0x000001a4: jal x22, addr0x00000364
addr0x000001a8: lui x28, 260813
addr0x000001ac: addi x22, x7, 158
addr0x000001b0: jal x5, addr0x00000458
addr0x000001b4: jal x5, addr0x0000004c
addr0x000001b8: lui x0, 7032
addr0x000001bc: addi x20, x23, -2044
addr0x000001c0: add x19, x9, x18
addr0x000001c4: slli x22, x20, 1
addr0x000001c8: lw x21, 1071(x8)
addr0x000001cc: add x20, x15, x13
addr0x000001d0: lui x0, 84461
addr0x000001d4: sh x19, 539(x8)
addr0x000001d8: addi x7, x7, 1640
addr0x000001dc: sw x19, 1595(x8)
addr0x000001e0: addi x16, x7, 874
addr0x000001e4: addi x0, x9, -323
addr0x000001e8: jalr x0, 344(x3)
addr0x000001ec: bne x7, x22, addr0x00000014
addr0x000001f0: andi x24, x20, -882
addr0x000001f4: andi x17, x10, 869
addr0x000001f8: lw x0, -595(x8)
addr0x000001fc: lw x19, -1543(x8)
addr0x00000200: and x21, x18, x21
addr0x00000204: or x20, x5, x22
addr0x00000208: lui x30, 168851
addr0x0000020c: sw x29, -1594(x8)
addr0x00000210: sw x18, 619(x8)
addr0x00000214: jal x19, addr0x000003fc
addr0x00000218: addi x17, x16, -1985
addr0x0000021c: lhu x2, 528(x8)
addr0x00000220: lhu x17, -700(x8)
addr0x00000224: addi x13, x17, -599
addr0x00000228: bne x5, x19, addr0x00000618
addr0x0000022c: lbu x9, 1359(x8)
addr0x00000230: addi x21, x19, -660
addr0x00000234: addi x27, x27, -1175
addr0x00000238: slli x17, x0, 7
addr0x0000023c: sub x29, x12, x5
addr0x00000240: srai x22, x29, 13
addr0x00000244: xor x9, x9, x22
addr0x00000248: sh x24, 732(x8)
addr0x0000024c: slli x16, x22, 9
addr0x00000250: andi x4, x17, 1768
addr0x00000254: jal x19, addr0x000000b4
addr0x00000258: beq x0, x23, addr0x00000150
addr0x0000025c: addi x12, x5, 1402
addr0x00000260: sb x29, -128(x8)
addr0x00000264: addi x30, x23, 34
addr0x00000268: jal x29, addr0x00000264
addr0x0000026c: addi x29, x4, 1331
addr0x00000270: jal x27, addr0x000005e0
addr0x00000274: jal x24, addr0x00000140
addr0x00000278: sw x24, -733(x8)
addr0x0000027c: sw x23, 553(x8)
addr0x00000280: divu x31, x23, x29
addr0x00000284: lw x0, 249(x8)
addr0x00000288: lw x2, 1296(x8)
addr0x0000028c: lw x19, -203(x8)
addr0x00000290: jal x30, addr0x00000308
addr0x00000294: sw x19, -1824(x8)
addr0x00000298: sw x0, 69(x8)
addr0x0000029c: jal x18, addr0x000003ac
addr0x000002a0: lw x23, 1329(x8)
addr0x000002a4: lw x29, -964(x8)
addr0x000002a8: lw x13, 1081(x8)
addr0x000002ac: lw x31, -154(x8)
addr0x000002b0: lw x31, 421(x8)
addr0x000002b4: addi x0, x22, 1432
addr0x000002b8: addi x23, x9, 239
addr0x000002bc: jal x18, addr0x00000634
addr0x000002c0: addi x19, x28, 1191
addr0x000002c4: lui x7, 570353
addr0x000002c8: sw x23, 1326(x8)
addr0x000002cc: sw x25, 1251(x8)
addr0x000002d0: jal x4, addr0x00000170
addr0x000002d4: jal x11, addr0x000002fc
addr0x000002d8: jal x11, addr0x00000330
addr0x000002dc: lw x19, 1556(x8)
addr0x000002e0: sw x5, -1224(x8)
addr0x000002e4: sw x9, -435(x8)
addr0x000002e8: sw x20, -881(x8)
addr0x000002ec: sw x31, 414(x8)
addr0x000002f0: jal x14, addr0x00000634
addr0x000002f4: addi x18, x16, 180
addr0x000002f8: blt x23, x13, addr0x00000284
addr0x000002fc: lw x29, -1393(x8)
addr0x00000300: lw x15, -889(x8)
addr0x00000304: add x25, x31, x18
addr0x00000308: jal x14, addr0x00000394
addr0x0000030c: beq x23, x15, addr0x00000434
addr0x00000310: beq x19, x12, addr0x00000154
addr0x00000314: lui x22, 846059
addr0x00000318: lui x21, 277284
addr0x0000031c: sw x17, -732(x8)
addr0x00000320: sw x4, -851(x8)
addr0x00000324: jal x20, addr0x000003a0
addr0x00000328: lw x14, -701(x8)
addr0x0000032c: add x31, x24, x0
addr0x00000330: jal x24, addr0x00000368
addr0x00000334: lw x22, 592(x8)
addr0x00000338: sw x31, -1580(x8)
addr0x0000033c: addi x15, x23, -144
addr0x00000340: sh x22, -633(x8)
addr0x00000344: sw x7, 1751(x8)
addr0x00000348: addi x2, x14, -612
addr0x0000034c: lw x14, -838(x8)
addr0x00000350: lw x24, 0(x8)
addr0x00000354: sw x18, -1595(x8)
addr0x00000358: jal x30, addr0x000002b8
addr0x0000035c: lw x28, 316(x8)
addr0x00000360: sw x17, 1138(x8)
addr0x00000364: sw x22, 1068(x8)
addr0x00000368: sw x13, 1868(x8)
addr0x0000036c: jal x20, addr0x00000144
addr0x00000370: sw x17, 1126(x8)
addr0x00000374: jal x5, addr0x00000584
addr0x00000378: lw x23, -1801(x8)
addr0x0000037c: lw x29, -1848(x8)
addr0x00000380: lw x4, 599(x8)
addr0x00000384: sw x17, -104(x8)
addr0x00000388: addi x13, x17, 96
addr0x0000038c: jal x0, addr0x00000590
addr0x00000390: addi x27, x9, -102
addr0x00000394: lw x23, 1580(x8)
addr0x00000398: jal x22, addr0x00000194
addr0x0000039c: lw x19, 1787(x8)
addr0x000003a0: lw x19, 559(x8)
addr0x000003a4: jal x21, addr0x00000690
addr0x000003a8: lw x29, -687(x8)
addr0x000003ac: sltu x2, x0, x13
addr0x000003b0: or x13, x23, x31
addr0x000003b4: sw x4, 1947(x8)
addr0x000003b8: sw x7, -132(x8)
addr0x000003bc: lw x5, 960(x8)
addr0x000003c0: sw x21, -26(x8)
addr0x000003c4: jal x29, addr0x000002f8
addr0x000003c8: addi x5, x18, 883
addr0x000003cc: lui x1, 744361
addr0x000003d0: addi x21, x22, -1147
addr0x000003d4: sw x19, -920(x8)
addr0x000003d8: lw x4, -1113(x8)
addr0x000003dc: addi x9, x23, 603
addr0x000003e0: addi x22, x22, -737
addr0x000003e4: andi x20, x19, -1788
addr0x000003e8: andi x9, x12, 504
addr0x000003ec: addi x28, x21, -1577
addr0x000003f0: jal x28, addr0x00000534
addr0x000003f4: addi x1, x2, 1665
addr0x000003f8: beq x20, x18, addr0x0000020c
addr0x000003fc: sw x16, -545(x8)
addr0x00000400: jal x17, addr0x000000a4
addr0x00000404: lw x13, 751(x8)
addr0x00000408: addi x29, x15, -482
addr0x0000040c: addi x22, x4, 792
addr0x00000410: addi x23, x0, -271
addr0x00000414: jal x7, addr0x00000148
addr0x00000418: lw x7, 1893(x8)
addr0x0000041c: lw x29, 318(x8)
addr0x00000420: addi x22, x29, 1923
addr0x00000424: bltu x22, x5, addr0x00000580
addr0x00000428: slli x30, x5, 25
addr0x0000042c: jal x29, addr0x00000260
addr0x00000430: jal x24, addr0x000006e0
addr0x00000434: addi x19, x12, 133
addr0x00000438: lui x19, 1036698
addr0x0000043c: addi x5, x22, -1459
addr0x00000440: addi x24, x18, 951
addr0x00000444: add x19, x11, x20
addr0x00000448: bgeu x17, x29, addr0x00000580
addr0x0000044c: sw x9, -1569(x8)
addr0x00000450: sw x29, -2025(x8)
addr0x00000454: addi x2, x18, -1778
addr0x00000458: jal x13, addr0x000001b0
addr0x0000045c: addi x6, x7, 822
addr0x00000460: add x27, x25, x29
addr0x00000464: jal x5, addr0x00000178
addr0x00000468: lw x2, 1067(x8)
addr0x0000046c: lw x5, 332(x8)
addr0x00000470: add x18, x0, x27
addr0x00000474: jal x25, addr0x00000674
addr0x00000478: addi x20, x28, 1053
addr0x0000047c: jal x29, addr0x000000bc
addr0x00000480: addi x17, x1, -1170
addr0x00000484: beq x1, x13, addr0x00000274
addr0x00000488: ori x25, x14, -1231
addr0x0000048c: ori x7, x25, -354
addr0x00000490: sw x6, 104(x8)
addr0x00000494: addi x9, x27, 1702
addr0x00000498: lbu x19, -248(x8)
addr0x0000049c: sb x2, -1227(x8)
addr0x000004a0: andi x9, x14, -322
addr0x000004a4: sub x17, x1, x5
addr0x000004a8: srli x6, x0, 31
addr0x000004ac: lui x18, 792639
addr0x000004b0: addi x11, x12, -1190
addr0x000004b4: bne x14, x31, addr0x0000009c
addr0x000004b8: lw x15, -504(x8)
addr0x000004bc: lw x25, -1053(x8)
addr0x000004c0: jal x5, addr0x00000184
addr0x000004c4: sw x30, 1207(x8)
addr0x000004c8: lw x9, -1455(x8)
addr0x000004cc: lw x2, 1446(x8)
addr0x000004d0: lw x21, -1555(x8)
addr0x000004d4: lw x17, 431(x8)
addr0x000004d8: addi x20, x4, 1903
addr0x000004dc: bgeu x0, x6, addr0x00000070
addr0x000004e0: sw x14, -1637(x8)
addr0x000004e4: sw x4, -1587(x8)
addr0x000004e8: sw x9, 193(x8)
addr0x000004ec: sw x20, 1655(x8)
addr0x000004f0: sh x29, 1858(x8)
addr0x000004f4: bne x30, x20, addr0x00000370
addr0x000004f8: auipc x18, 1004851
addr0x000004fc: lw x30, 1402(x8)
addr0x00000500: sw x0, 271(x8)
addr0x00000504: jal x30, addr0x0000019c
addr0x00000508: lw x20, -1763(x8)
addr0x0000050c: lw x0, 649(x8)
addr0x00000510: sw x5, 1691(x8)
addr0x00000514: sw x13, -586(x8)
addr0x00000518: lw x23, -1557(x8)
addr0x0000051c: lw x22, 567(x8)
addr0x00000520: lw x30, 389(x8)
addr0x00000524: add x27, x9, x0
addr0x00000528: jal x9, addr0x0000062c
addr0x0000052c: lw x21, -72(x8)
addr0x00000530: addi x24, x11, 1455
addr0x00000534: blt x31, x13, addr0x00000058
addr0x00000538: lw x21, 209(x8)
addr0x0000053c: addi x24, x2, 724
addr0x00000540: sw x4, 622(x8)
addr0x00000544: addi x1, x19, -688
addr0x00000548: addi x21, x27, -1919
addr0x0000054c: lw x1, -1095(x8)
addr0x00000550: addi x15, x9, 1755
addr0x00000554: lw x6, 240(x8)
addr0x00000558: lw x0, -1465(x8)
addr0x0000055c: beq x11, x28, addr0x000005b8
addr0x00000560: sb x17, 31(x8)
addr0x00000564: sb x19, 34(x8)
addr0x00000568: lw x13, 1434(x8)
addr0x0000056c: lw x9, -846(x8)
addr0x00000570: sw x19, -140(x8)
addr0x00000574: sw x7, -1622(x8)
addr0x00000578: lw x29, 940(x8)
addr0x0000057c: sw x28, 1306(x8)
addr0x00000580: jal x6, addr0x00000598
addr0x00000584: addi x5, x0, -1425
addr0x00000588: lw x2, -1096(x8)
addr0x0000058c: lbu x29, 1983(x8)
addr0x00000590: bltu x29, x22, addr0x000002b8
addr0x00000594: lbu x2, -38(x8)
addr0x00000598: beq x29, x22, addr0x0000001c
addr0x0000059c: sll x6, x16, x29
addr0x000005a0: lui x30, 941056
addr0x000005a4: addi x29, x29, -1031
addr0x000005a8: sw x17, -831(x8)
addr0x000005ac: jal x4, addr0x00000354
addr0x000005b0: sw x29, -1642(x8)
addr0x000005b4: addi x20, x29, 1047
addr0x000005b8: sw x18, -750(x8)
addr0x000005bc: jal x5, addr0x000003a8
addr0x000005c0: lw x1, -1728(x8)
addr0x000005c4: addi x31, x5, -485
addr0x000005c8: jal x7, addr0x000003f0
addr0x000005cc: lbu x22, 46(x8)
addr0x000005d0: add x31, x25, x25
addr0x000005d4: jal x29, addr0x0000065c
addr0x000005d8: addi x0, x9, -1694
addr0x000005dc: sw x21, -1194(x8)
addr0x000005e0: sw x14, 1572(x8)
addr0x000005e4: addi x22, x5, 1828
addr0x000005e8: lw x14, -2041(x8)
addr0x000005ec: sb x1, 64(x8)
addr0x000005f0: lw x16, 1097(x8)
addr0x000005f4: addi x29, x28, 788
addr0x000005f8: addi x5, x14, -1046
addr0x000005fc: sw x31, 295(x8)
addr0x00000600: addi x0, x4, 269
addr0x00000604: lui x9, 1037679
addr0x00000608: addi x18, x11, 2016
addr0x0000060c: andi x9, x29, -775
addr0x00000610: lw x11, 514(x8)
addr0x00000614: lw x20, -139(x8)
addr0x00000618: lw x13, 819(x8)
addr0x0000061c: add x30, x21, x20
addr0x00000620: lw x9, -1667(x8)
addr0x00000624: lw x0, -1602(x8)
addr0x00000628: jal x6, addr0x00000108
addr0x0000062c: jal x4, addr0x00000508
addr0x00000630: lui x23, 907768
addr0x00000634: addi x13, x19, -1323
addr0x00000638: bne x30, x5, addr0x00000654
addr0x0000063c: sw x19, -1171(x8)
addr0x00000640: jal x30, addr0x00000248
addr0x00000644: sw x13, 1546(x8)
addr0x00000648: bltu x20, x18, addr0x00000138
addr0x0000064c: addi x9, x21, -629
addr0x00000650: jal x12, addr0x0000051c
addr0x00000654: lbu x18, -808(x8)
addr0x00000658: jal x1, addr0x000000d0
addr0x0000065c: jal x0, addr0x000006b4
addr0x00000660: lw x22, -620(x8)
addr0x00000664: lw x30, 138(x8)
addr0x00000668: lw x28, 1864(x8)
addr0x0000066c: bgeu x5, x0, addr0x00000540
addr0x00000670: lw x17, 1878(x8)
addr0x00000674: beq x23, x2, addr0x000004e8
addr0x00000678: lhu x30, -1236(x8)
addr0x0000067c: addi x28, x27, -1089
addr0x00000680: addi x30, x21, 1842
addr0x00000684: jal x24, addr0x00000694
addr0x00000688: lw x0, 1255(x8)
addr0x0000068c: lw x4, 1192(x8)
addr0x00000690: lw x27, 947(x8)
addr0x00000694: lw x19, 1038(x8)
addr0x00000698: lw x31, 1661(x8)
addr0x0000069c: lw x2, 1139(x8)
addr0x000006a0: lw x15, -1180(x8)
addr0x000006a4: addi x20, x19, 642
addr0x000006a8: jal x30, addr0x000005a0
addr0x000006ac: lw x13, -388(x8)
addr0x000006b0: addi x22, x20, -230
addr0x000006b4: jal x7, addr0x00000540
addr0x000006b8: jal x5, addr0x00000140
addr0x000006bc: lw x19, -1292(x8)
addr0x000006c0: beq x23, x13, addr0x00000218
addr0x000006c4: beq x0, x15, addr0x00000580
addr0x000006c8: lw x18, 1090(x8)
addr0x000006cc: addi x9, x25, -1790
addr0x000006d0: jal x7, addr0x00000558
addr0x000006d4: jal x18, addr0x000005d0
addr0x000006d8: lbu x26, 1271(x8)
addr0x000006dc: bltu x31, x30, addr0x00000700
addr0x000006e0: lw x4, -314(x8)
addr0x000006e4: ori x9, x25, 1289
addr0x000006e8: sw x4, 1723(x8)
addr0x000006ec: lbu x22, 158(x8)
addr0x000006f0: lbu x30, 1413(x8)
addr0x000006f4: andi x30, x27, -958
addr0x000006f8: lbu x23, -541(x8)
addr0x000006fc: jal x31, addr0x000005ac
addr0x00000700: add x23, x24, x19
addr0x00000704: bltu x11, x14, addr0x00000274
