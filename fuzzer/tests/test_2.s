.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x5, -211(x8)
addr0x0000001c: addi x29, x17, -1356
addr0x00000020: lw x19, 1181(x8)
addr0x00000024: lw x25, 475(x8)
addr0x00000028: lw x15, 1782(x8)
addr0x0000002c: add x22, x19, x4
addr0x00000030: lw x25, -3(x8)
addr0x00000034: lw x1, -857(x8)
addr0x00000038: lw x29, -65(x8)
addr0x0000003c: lw x22, 94(x8)
addr0x00000040: lw x29, -1917(x8)
addr0x00000044: sub x23, x6, x19
addr0x00000048: bne x23, x29, addr0x00000434
addr0x0000004c: lw x25, -763(x8)
addr0x00000050: lw x5, -975(x8)
addr0x00000054: bne x7, x23, addr0x00000580
addr0x00000058: lw x16, -1667(x8)
addr0x0000005c: lw x5, 1076(x8)
addr0x00000060: sw x22, 1136(x8)
addr0x00000064: lui x19, 23775
addr0x00000068: addi x17, x9, -1674
addr0x0000006c: beq x18, x18, addr0x00000388
addr0x00000070: addi x23, x6, -1102
addr0x00000074: beq x19, x29, addr0x0000057c
addr0x00000078: lw x0, 1533(x8)
addr0x0000007c: beq x4, x22, addr0x000002b8
addr0x00000080: add x19, x0, x23
addr0x00000084: sltu x19, x17, x22
addr0x00000088: addi x5, x29, 982
addr0x0000008c: sw x5, 1826(x8)
addr0x00000090: jalr x0, 580(x3)
addr0x00000094: jal x22, addr0x00000888
addr0x00000098: addi x29, x23, -989
addr0x0000009c: jal x16, addr0x000007e0
addr0x000000a0: andi x23, x29, 547
addr0x000000a4: sw x7, 105(x8)
addr0x000000a8: jal x25, addr0x00000580
addr0x000000ac: lw x4, -30(x8)
addr0x000000b0: addi x22, x22, 814
addr0x000000b4: sw x19, -1414(x8)
addr0x000000b8: lbu x23, -1273(x8)
addr0x000000bc: beq x1, x4, addr0x00000220
addr0x000000c0: bltu x6, x29, addr0x00000468
addr0x000000c4: sw x18, -834(x8)
addr0x000000c8: sw x4, 841(x8)
addr0x000000cc: lw x19, 652(x8)
addr0x000000d0: add x29, x19, x23
addr0x000000d4: slli x23, x17, 2
addr0x000000d8: lui x22, 114427
addr0x000000dc: sw x23, 209(x8)
addr0x000000e0: sw x22, -387(x8)
addr0x000000e4: add x7, x0, x23
addr0x000000e8: andi x23, x29, 1528
addr0x000000ec: srai x16, x19, 28
addr0x000000f0: sw x17, 1699(x8)
addr0x000000f4: jal x9, addr0x0000086c
addr0x000000f8: lw x23, -341(x8)
addr0x000000fc: addi x17, x17, 81
addr0x00000100: addi x6, x19, -1824
addr0x00000104: lbu x17, -575(x8)
addr0x00000108: addi x24, x23, 864
addr0x0000010c: addi x16, x15, 1258
addr0x00000110: andi x22, x29, 364
addr0x00000114: sll x19, x23, x24
addr0x00000118: lw x22, -464(x8)
addr0x0000011c: sub x7, x19, x23
addr0x00000120: lw x0, -1154(x8)
addr0x00000124: jal x16, addr0x00000040
addr0x00000128: slli x15, x17, 9
addr0x0000012c: srli x19, x29, 22
addr0x00000130: lui x19, 91400
addr0x00000134: addi x16, x29, 1336
addr0x00000138: sw x15, 1081(x8)
addr0x0000013c: sw x22, 451(x8)
addr0x00000140: jal x19, addr0x00000270
addr0x00000144: lw x0, -1282(x8)
addr0x00000148: lw x22, -1726(x8)
addr0x0000014c: lw x7, 1573(x8)
addr0x00000150: andi x23, x15, -303
addr0x00000154: jal x17, addr0x000003d0
addr0x00000158: bne x23, x4, addr0x0000072c
addr0x0000015c: lui x19, 451589
addr0x00000160: sw x19, -1969(x8)
addr0x00000164: jal x5, addr0x00000484
addr0x00000168: lw x18, 1055(x8)
addr0x0000016c: addi x22, x4, 291
addr0x00000170: lw x15, 550(x8)
addr0x00000174: beq x22, x15, addr0x0000023c
addr0x00000178: sw x1, -1046(x8)
addr0x0000017c: add x4, x29, x5
addr0x00000180: sb x6, -1867(x8)
addr0x00000184: bne x9, x7, addr0x00000208
addr0x00000188: lw x29, 803(x8)
addr0x0000018c: lw x24, -1285(x8)
addr0x00000190: jal x23, addr0x0000082c
addr0x00000194: slli x19, x23, 28
addr0x00000198: addi x4, x23, 1763
addr0x0000019c: jal x29, addr0x00000544
addr0x000001a0: lw x1, -1340(x8)
addr0x000001a4: srli x22, x23, 29
addr0x000001a8: bge x4, x29, addr0x00000608
addr0x000001ac: sw x1, -12(x8)
addr0x000001b0: sw x19, -966(x8)
addr0x000001b4: lw x6, -579(x8)
addr0x000001b8: lw x22, -101(x8)
addr0x000001bc: addi x29, x29, 1166
addr0x000001c0: sb x16, -997(x8)
addr0x000001c4: sb x15, 1780(x8)
addr0x000001c8: lw x0, 1767(x8)
addr0x000001cc: sw x6, 99(x8)
addr0x000001d0: addi x19, x29, -254
addr0x000001d4: lw x22, -1894(x8)
addr0x000001d8: addi x9, x29, 899
addr0x000001dc: sw x29, -1688(x8)
addr0x000001e0: lw x25, -394(x8)
addr0x000001e4: sw x22, 632(x8)
addr0x000001e8: lw x22, -1296(x8)
addr0x000001ec: lw x25, 54(x8)
addr0x000001f0: lw x22, -136(x8)
addr0x000001f4: sw x19, -1462(x8)
addr0x000001f8: sw x22, 238(x8)
addr0x000001fc: lw x22, 917(x8)
addr0x00000200: lw x22, -1318(x8)
addr0x00000204: addi x29, x23, -693
addr0x00000208: lw x15, 589(x8)
addr0x0000020c: lw x25, 1153(x8)
addr0x00000210: bge x1, x16, addr0x000004c0
addr0x00000214: lw x24, -1015(x8)
addr0x00000218: lw x22, -1333(x8)
addr0x0000021c: addi x22, x23, -1928
addr0x00000220: sw x4, 1914(x8)
addr0x00000224: lw x19, -1627(x8)
addr0x00000228: lw x19, 1362(x8)
addr0x0000022c: jal x22, addr0x00000618
addr0x00000230: bne x23, x22, addr0x000006f0
addr0x00000234: lw x0, 1083(x8)
addr0x00000238: add x6, x24, x29
addr0x0000023c: sw x21, -1030(x8)
addr0x00000240: addi x19, x23, -1258
addr0x00000244: jal x4, addr0x000008d4
addr0x00000248: lw x23, 1143(x8)
addr0x0000024c: addi x25, x6, 1740
addr0x00000250: jal x25, addr0x00000384
addr0x00000254: bne x19, x19, addr0x0000009c
addr0x00000258: bltu x16, x6, addr0x00000494
addr0x0000025c: lw x19, -855(x8)
addr0x00000260: lw x22, 1020(x8)
addr0x00000264: addi x29, x9, 1773
addr0x00000268: lui x18, 442186
addr0x0000026c: lw x23, 1090(x8)
addr0x00000270: lui x23, 235645
addr0x00000274: sw x6, 435(x8)
addr0x00000278: jal x0, addr0x000002e4
addr0x0000027c: lui x4, 524277
addr0x00000280: addi x25, x16, -1944
addr0x00000284: jal x17, addr0x00000220
addr0x00000288: addi x15, x18, 461
addr0x0000028c: sub x29, x23, x25
addr0x00000290: srai x19, x9, 29
addr0x00000294: and x4, x6, x15
addr0x00000298: slli x25, x22, 19
addr0x0000029c: add x15, x15, x15
addr0x000002a0: and x16, x23, x7
addr0x000002a4: srli x19, x22, 31
addr0x000002a8: jal x29, addr0x000008b8
addr0x000002ac: jal x16, addr0x000008b0
addr0x000002b0: lw x23, 308(x8)
addr0x000002b4: lw x29, 53(x8)
addr0x000002b8: lw x23, -1941(x8)
addr0x000002bc: lw x19, -1738(x8)
addr0x000002c0: addi x5, x25, 1404
addr0x000002c4: lw x25, -1905(x8)
addr0x000002c8: addi x4, x25, -1814
addr0x000002cc: blt x19, x19, addr0x0000011c
addr0x000002d0: addi x24, x29, 596
addr0x000002d4: jal x7, addr0x00000714
addr0x000002d8: sb x17, 296(x8)
addr0x000002dc: lw x15, 1125(x8)
addr0x000002e0: lw x17, -1487(x8)
addr0x000002e4: jal x16, addr0x00000394
addr0x000002e8: lw x27, -1200(x8)
addr0x000002ec: jal x29, addr0x00000128
addr0x000002f0: sw x29, -768(x8)
addr0x000002f4: bne x23, x5, addr0x000006ac
addr0x000002f8: sw x23, 222(x8)
addr0x000002fc: sw x23, -1969(x8)
addr0x00000300: jalr x0, 564(x3)
addr0x00000304: sw x29, 1211(x8)
addr0x00000308: sw x4, -1696(x8)
addr0x0000030c: sw x23, -974(x8)
addr0x00000310: lui x29, 310557
addr0x00000314: addi x19, x24, 2029
addr0x00000318: lw x23, -626(x8)
addr0x0000031c: lw x27, 260(x8)
addr0x00000320: lw x27, -33(x8)
addr0x00000324: lw x21, 594(x8)
addr0x00000328: bgeu x9, x9, addr0x00000178
addr0x0000032c: addi x17, x15, -1622
addr0x00000330: lw x25, 1267(x8)
addr0x00000334: add x9, x0, x15
addr0x00000338: srli x23, x22, 26
addr0x0000033c: slt x22, x19, x18
addr0x00000340: sw x24, 1397(x8)
addr0x00000344: sw x4, -1346(x8)
addr0x00000348: sw x23, -547(x8)
addr0x0000034c: sw x24, -1234(x8)
addr0x00000350: lw x6, 595(x8)
addr0x00000354: andi x18, x1, 1350
addr0x00000358: bne x19, x17, addr0x00000830
addr0x0000035c: lw x23, -675(x8)
addr0x00000360: bne x23, x23, addr0x000007dc
addr0x00000364: lw x24, -817(x8)
addr0x00000368: lw x23, 1524(x8)
addr0x0000036c: beq x29, x22, addr0x000001c0
addr0x00000370: bne x23, x29, addr0x00000368
addr0x00000374: add x19, x15, x19
addr0x00000378: sw x9, 742(x8)
addr0x0000037c: jal x5, addr0x00000224
addr0x00000380: lui x29, 970378
addr0x00000384: lui x22, 70667
addr0x00000388: lw x22, -1522(x8)
addr0x0000038c: bgeu x4, x4, addr0x00000350
addr0x00000390: lw x19, -502(x8)
addr0x00000394: lw x22, 1571(x8)
addr0x00000398: lw x9, 423(x8)
addr0x0000039c: lw x5, 1934(x8)
addr0x000003a0: lw x5, 923(x8)
addr0x000003a4: addi x15, x15, -1549
addr0x000003a8: jal x19, addr0x00000230
addr0x000003ac: lw x29, 1246(x8)
addr0x000003b0: lw x0, -21(x8)
addr0x000003b4: sw x17, 1676(x8)
addr0x000003b8: sw x22, 826(x8)
addr0x000003bc: addi x15, x19, 767
addr0x000003c0: sw x22, -1893(x8)
addr0x000003c4: sw x22, -1756(x8)
addr0x000003c8: jal x5, addr0x000005ac
addr0x000003cc: jal x29, addr0x00000234
addr0x000003d0: jal x29, addr0x000002f4
addr0x000003d4: sw x18, 1997(x8)
addr0x000003d8: lui x22, 127565
addr0x000003dc: addi x23, x23, 354
addr0x000003e0: jal x27, addr0x0000088c
addr0x000003e4: jal x19, addr0x00000160
addr0x000003e8: lw x19, 1710(x8)
addr0x000003ec: addi x5, x15, 352
addr0x000003f0: lui x27, 541726
addr0x000003f4: addi x25, x22, -1924
addr0x000003f8: lw x15, 35(x8)
addr0x000003fc: jal x4, addr0x00000008
addr0x00000400: lw x19, 823(x8)
addr0x00000404: addi x6, x4, -551
addr0x00000408: sw x1, 306(x8)
addr0x0000040c: sw x6, -885(x8)
addr0x00000410: jal x22, addr0x00000308
addr0x00000414: lw x29, 774(x8)
addr0x00000418: addi x21, x5, 1052
addr0x0000041c: lw x23, 1229(x8)
addr0x00000420: lw x19, -1468(x8)
addr0x00000424: lw x29, 1330(x8)
addr0x00000428: addi x22, x24, 1765
addr0x0000042c: jal x7, addr0x00000310
addr0x00000430: addi x17, x5, 1015
addr0x00000434: addi x6, x16, 1252
addr0x00000438: sw x21, 1424(x8)
addr0x0000043c: or x29, x22, x4
addr0x00000440: srli x21, x17, 27
addr0x00000444: beq x20, x15, addr0x00000560
addr0x00000448: lw x4, 1358(x8)
addr0x0000044c: lw x19, -634(x8)
addr0x00000450: sw x21, 1576(x8)
addr0x00000454: jal x23, addr0x000004c4
addr0x00000458: sw x19, 1361(x8)
addr0x0000045c: sw x17, 262(x8)
addr0x00000460: sw x21, -228(x8)
addr0x00000464: jal x23, addr0x00000828
addr0x00000468: lui x19, 762352
addr0x0000046c: sw x17, 1057(x8)
addr0x00000470: sw x20, -1929(x8)
addr0x00000474: jal x22, addr0x000000bc
addr0x00000478: lw x22, -653(x8)
addr0x0000047c: lw x22, -1149(x8)
addr0x00000480: jal x23, addr0x000006c0
addr0x00000484: jal x23, addr0x00000544
addr0x00000488: jal x23, addr0x0000010c
addr0x0000048c: add x5, x0, x19
addr0x00000490: sw x25, 1258(x8)
addr0x00000494: lui x24, 1038777
addr0x00000498: addi x7, x7, -916
addr0x0000049c: sw x29, 1642(x8)
addr0x000004a0: sw x4, -383(x8)
addr0x000004a4: sub x15, x21, x7
addr0x000004a8: bge x23, x19, addr0x000005b4
addr0x000004ac: lw x15, -97(x8)
addr0x000004b0: jal x19, addr0x000000bc
addr0x000004b4: andi x9, x17, 1323
addr0x000004b8: jal x30, addr0x000001e8
addr0x000004bc: lbu x19, 515(x8)
addr0x000004c0: andi x4, x4, -2039
addr0x000004c4: addi x5, x9, 103
addr0x000004c8: sw x24, -388(x8)
addr0x000004cc: jal x19, addr0x00000668
addr0x000004d0: jal x6, addr0x00000160
addr0x000004d4: lw x30, 520(x8)
addr0x000004d8: sw x24, -553(x8)
addr0x000004dc: lui x22, 990112
addr0x000004e0: lui x0, 442077
addr0x000004e4: addi x20, x19, 798
addr0x000004e8: jal x29, addr0x0000067c
addr0x000004ec: lw x21, 1944(x8)
addr0x000004f0: lw x17, 1087(x8)
addr0x000004f4: addi x4, x23, -685
addr0x000004f8: sw x6, 1379(x8)
addr0x000004fc: addi x7, x20, 1557
addr0x00000500: lw x19, 1587(x8)
addr0x00000504: sw x0, -922(x8)
addr0x00000508: sw x25, 1656(x8)
addr0x0000050c: sw x17, 72(x8)
addr0x00000510: lbu x5, -895(x8)
addr0x00000514: jal x19, addr0x00000240
addr0x00000518: lw x18, 1610(x8)
addr0x0000051c: bne x18, x29, addr0x00000860
addr0x00000520: lui x24, 866297
addr0x00000524: addi x22, x17, -1016
addr0x00000528: addi x16, x22, -491
addr0x0000052c: lw x19, 1265(x8)
addr0x00000530: slli x29, x29, 28
addr0x00000534: lw x19, 1160(x8)
addr0x00000538: lui x23, 80234
addr0x0000053c: addi x17, x19, -11
addr0x00000540: lw x15, 1505(x8)
addr0x00000544: bne x23, x22, addr0x000007b8
addr0x00000548: bne x29, x17, addr0x0000065c
addr0x0000054c: lw x23, 1517(x8)
addr0x00000550: addi x30, x29, -25
addr0x00000554: beq x1, x18, addr0x00000678
addr0x00000558: jal x17, addr0x000001ec
addr0x0000055c: bge x6, x30, addr0x000002b8
addr0x00000560: lw x24, -1626(x8)
addr0x00000564: lw x29, -1510(x8)
addr0x00000568: sw x2, -1439(x8)
addr0x0000056c: lui x20, 978486
addr0x00000570: div x22, x30, x5
addr0x00000574: addi x9, x22, -612
addr0x00000578: sw x29, -749(x8)
addr0x0000057c: jal x29, addr0x00000668
addr0x00000580: beq x19, x22, addr0x0000055c
addr0x00000584: sw x9, -819(x8)
addr0x00000588: blt x29, x9, addr0x00000890
addr0x0000058c: lw x2, 2002(x8)
addr0x00000590: addi x30, x15, 379
addr0x00000594: addi x9, x23, -1989
addr0x00000598: lw x18, -619(x8)
addr0x0000059c: lw x15, 1129(x8)
addr0x000005a0: lw x24, 715(x8)
addr0x000005a4: srli x25, x17, 5
addr0x000005a8: sh x2, -831(x8)
addr0x000005ac: addi x19, x1, 1700
addr0x000005b0: bne x15, x29, addr0x00000478
addr0x000005b4: addi x19, x23, 56
addr0x000005b8: sw x24, 492(x8)
addr0x000005bc: sw x29, 355(x8)
addr0x000005c0: bne x27, x9, addr0x0000061c
addr0x000005c4: lw x29, 1086(x8)
addr0x000005c8: jal x1, addr0x000006d0
addr0x000005cc: sw x18, -894(x8)
addr0x000005d0: lw x6, 1072(x8)
addr0x000005d4: addi x24, x30, 597
addr0x000005d8: bne x1, x4, addr0x000006a8
addr0x000005dc: lui x24, 955588
addr0x000005e0: addi x24, x7, 1084
addr0x000005e4: lw x9, 482(x8)
addr0x000005e8: lw x29, 1269(x8)
addr0x000005ec: jal x17, addr0x00000340
addr0x000005f0: lw x16, 1755(x8)
addr0x000005f4: lhu x19, -439(x8)
addr0x000005f8: lui x23, 16016
addr0x000005fc: lui x29, 1007229
addr0x00000600: addi x19, x29, -1440
addr0x00000604: lui x5, 424700
addr0x00000608: addi x5, x30, -739
addr0x0000060c: lw x19, -1664(x8)
addr0x00000610: addi x29, x19, 1770
addr0x00000614: jal x7, addr0x000008a4
addr0x00000618: andi x23, x19, 1059
addr0x0000061c: srli x9, x18, 31
addr0x00000620: srli x30, x23, 28
addr0x00000624: beq x9, x17, addr0x00000530
addr0x00000628: addi x5, x6, -1056
addr0x0000062c: lbu x9, -1751(x8)
addr0x00000630: beq x27, x17, addr0x0000069c
addr0x00000634: addi x6, x29, -1753
addr0x00000638: jal x22, addr0x000004c8
addr0x0000063c: lbu x18, 1269(x8)
addr0x00000640: sb x22, -1516(x8)
addr0x00000644: lw x21, 1952(x8)
addr0x00000648: addi x29, x9, 1177
addr0x0000064c: lui x17, 296218
addr0x00000650: addi x4, x19, 334
addr0x00000654: sw x25, -698(x8)
addr0x00000658: sw x16, -1781(x8)
addr0x0000065c: sw x15, -625(x8)
addr0x00000660: bne x10, x6, addr0x000003ec
addr0x00000664: sw x2, 1301(x8)
addr0x00000668: bne x16, x1, addr0x0000052c
addr0x0000066c: addi x29, x19, 1162
addr0x00000670: blt x29, x22, addr0x000004fc
addr0x00000674: addi x15, x18, -1523
addr0x00000678: sw x23, -740(x8)
addr0x0000067c: lw x5, 1871(x8)
addr0x00000680: jal x29, addr0x00000240
addr0x00000684: sw x2, 849(x8)
addr0x00000688: sw x22, 1187(x8)
addr0x0000068c: sw x23, -479(x8)
addr0x00000690: sw x29, -827(x8)
addr0x00000694: sw x7, 683(x8)
addr0x00000698: bne x23, x1, addr0x00000770
addr0x0000069c: addi x19, x17, 485
addr0x000006a0: add x4, x27, x4
addr0x000006a4: lbu x16, 636(x8)
addr0x000006a8: addi x25, x4, 1254
addr0x000006ac: addi x24, x23, 1575
addr0x000006b0: jal x20, addr0x000008b8
addr0x000006b4: sw x22, 2027(x8)
addr0x000006b8: lw x2, -1509(x8)
addr0x000006bc: lw x21, -763(x8)
addr0x000006c0: lw x29, -856(x8)
addr0x000006c4: lw x4, -1042(x8)
addr0x000006c8: jal x19, addr0x0000020c
addr0x000006cc: lw x9, -1497(x8)
addr0x000006d0: sw x18, -221(x8)
addr0x000006d4: sw x15, 627(x8)
addr0x000006d8: jal x23, addr0x00000548
addr0x000006dc: lw x1, 124(x8)
addr0x000006e0: lw x5, -350(x8)
addr0x000006e4: lbu x29, -120(x8)
addr0x000006e8: slli x30, x25, 13
addr0x000006ec: add x16, x23, x30
addr0x000006f0: jal x24, addr0x000004c0
addr0x000006f4: lw x29, -1924(x8)
addr0x000006f8: lbu x23, 614(x8)
addr0x000006fc: sb x21, 509(x8)
addr0x00000700: lw x0, -482(x8)
addr0x00000704: jal x25, addr0x000004d0
addr0x00000708: lw x15, -245(x8)
addr0x0000070c: lw x18, 1898(x8)
addr0x00000710: jal x5, addr0x000002dc
addr0x00000714: lui x7, 703422
addr0x00000718: sw x4, -1400(x8)
addr0x0000071c: jal x0, addr0x0000055c
addr0x00000720: srai x24, x10, 10
addr0x00000724: sw x5, -1063(x8)
addr0x00000728: lw x22, -228(x8)
addr0x0000072c: lui x30, 808528
addr0x00000730: sw x29, -383(x8)
addr0x00000734: addi x9, x29, 684
addr0x00000738: sub x17, x19, x23
addr0x0000073c: addi x17, x22, 810
addr0x00000740: sw x7, -573(x8)
addr0x00000744: sw x17, -394(x8)
addr0x00000748: jal x1, addr0x00000460
addr0x0000074c: addi x17, x5, -1608
addr0x00000750: and x23, x1, x22
addr0x00000754: or x19, x16, x17
addr0x00000758: addi x9, x29, 431
addr0x0000075c: sw x10, -2015(x8)
addr0x00000760: sw x24, 605(x8)
addr0x00000764: jal x19, addr0x00000844
addr0x00000768: lw x29, -1115(x8)
addr0x0000076c: lw x19, -803(x8)
addr0x00000770: jal x24, addr0x00000814
addr0x00000774: lui x29, 305287
addr0x00000778: jal x29, addr0x00000064
addr0x0000077c: jal x5, addr0x0000084c
addr0x00000780: lw x19, -1068(x8)
addr0x00000784: sll x23, x4, x20
addr0x00000788: bgeu x30, x23, addr0x000002cc
addr0x0000078c: sw x5, -1277(x8)
addr0x00000790: sw x23, 322(x8)
addr0x00000794: lbu x21, 1343(x8)
addr0x00000798: bne x25, x27, addr0x00000314
addr0x0000079c: lui x22, 358895
addr0x000007a0: and x16, x19, x19
addr0x000007a4: sub x21, x7, x29
addr0x000007a8: addi x27, x29, -832
addr0x000007ac: addi x19, x21, 751
addr0x000007b0: jal x10, addr0x000003f8
addr0x000007b4: lui x19, 540170
addr0x000007b8: addi x2, x20, -150
addr0x000007bc: andi x10, x22, -1117
addr0x000007c0: ori x19, x6, -1178
addr0x000007c4: sh x29, -740(x8)
addr0x000007c8: lbu x4, 669(x8)
addr0x000007cc: lw x1, 371(x8)
addr0x000007d0: jal x19, addr0x00000378
addr0x000007d4: lbu x9, -890(x8)
addr0x000007d8: addi x23, x19, 156
addr0x000007dc: sw x4, -1409(x8)
addr0x000007e0: sb x9, -1282(x8)
addr0x000007e4: lw x6, 638(x8)
addr0x000007e8: addi x19, x23, 317
addr0x000007ec: jal x17, addr0x0000076c
addr0x000007f0: jal x29, addr0x00000898
addr0x000007f4: lw x23, 1737(x8)
addr0x000007f8: lw x17, 1393(x8)
addr0x000007fc: lw x27, -1608(x8)
addr0x00000800: addi x30, x5, -303
addr0x00000804: bne x23, x19, addr0x00000268
addr0x00000808: lw x23, -203(x8)
addr0x0000080c: lw x30, -1313(x8)
addr0x00000810: jal x9, addr0x000006d4
addr0x00000814: sw x10, 241(x8)
addr0x00000818: sw x17, -207(x8)
addr0x0000081c: sw x0, -386(x8)
addr0x00000820: beq x29, x22, addr0x00000670
addr0x00000824: lbu x18, 208(x8)
addr0x00000828: addi x30, x2, -296
addr0x0000082c: sw x22, 1727(x8)
addr0x00000830: sw x18, -1847(x8)
addr0x00000834: sw x2, -1959(x8)
addr0x00000838: jal x24, addr0x000001d8
addr0x0000083c: lw x5, -18(x8)
addr0x00000840: jal x29, addr0x000004ac
addr0x00000844: sw x15, 901(x8)
addr0x00000848: sw x19, 149(x8)
addr0x0000084c: sw x29, 1258(x8)
addr0x00000850: addi x1, x6, 1058
addr0x00000854: addi x23, x19, -911
addr0x00000858: sub x1, x4, x9
addr0x0000085c: sb x29, 1715(x8)
addr0x00000860: lw x9, 422(x8)
addr0x00000864: addi x29, x9, 1644
addr0x00000868: sw x23, 308(x8)
addr0x0000086c: lw x22, 534(x8)
addr0x00000870: addi x22, x10, -2015
addr0x00000874: sw x19, -1570(x8)
addr0x00000878: sw x4, 247(x8)
addr0x0000087c: jal x23, addr0x000001ec
addr0x00000880: lw x9, 881(x8)
addr0x00000884: sw x22, -1045(x8)
addr0x00000888: sw x19, -777(x8)
addr0x0000088c: addi x23, x20, -1452
addr0x00000890: lw x17, 1864(x8)
addr0x00000894: addi x9, x23, -747
addr0x00000898: sw x18, 702(x8)
addr0x0000089c: addi x17, x24, 2022
addr0x000008a0: lw x16, 1210(x8)
addr0x000008a4: lw x22, 709(x8)
addr0x000008a8: sw x15, 1947(x8)
addr0x000008ac: sw x22, 554(x8)
addr0x000008b0: lbu x15, 185(x8)
addr0x000008b4: beq x22, x15, addr0x00000674
addr0x000008b8: lw x22, -172(x8)
addr0x000008bc: lw x6, -28(x8)
addr0x000008c0: add x16, x23, x23
addr0x000008c4: jal x24, addr0x000000d8
addr0x000008c8: addi x23, x29, 1952
addr0x000008cc: lui x20, 421459
addr0x000008d0: addi x19, x23, 170
addr0x000008d4: beq x17, x4, addr0x000005b8
addr0x000008d8: lw x15, -963(x8)
addr0x000008dc: lbu x20, -1956(x8)
