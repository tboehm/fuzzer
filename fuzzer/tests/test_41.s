.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lh x10, 68(x8)
addr0x0000001c: slli x31, x13, 31
addr0x00000020: lw x19, -645(x8)
addr0x00000024: lbu x13, -1666(x8)
addr0x00000028: beq x24, x10, addr0x0000015c
addr0x0000002c: lw x10, -679(x8)
addr0x00000030: jal x5, addr0x000000cc
addr0x00000034: lui x25, 32732
addr0x00000038: addi x13, x19, 953
addr0x0000003c: addi x23, x0, 530
addr0x00000040: sw x18, 926(x8)
addr0x00000044: sw x5, 1930(x8)
addr0x00000048: sw x27, -1266(x8)
addr0x0000004c: addi x4, x9, -1710
addr0x00000050: sw x18, -1293(x8)
addr0x00000054: addi x30, x18, -1022
addr0x00000058: sw x27, -686(x8)
addr0x0000005c: sw x10, -1220(x8)
addr0x00000060: sw x26, -1156(x8)
addr0x00000064: lw x10, 1608(x8)
addr0x00000068: addi x20, x17, 1385
addr0x0000006c: sw x7, -988(x8)
addr0x00000070: addi x23, x16, -500
addr0x00000074: addi x30, x9, 834
addr0x00000078: jal x12, addr0x000000c0
addr0x0000007c: addi x28, x30, -734
addr0x00000080: addi x22, x28, 1561
addr0x00000084: jal x23, addr0x00000174
addr0x00000088: lw x7, 52(x8)
addr0x0000008c: lw x22, 1719(x8)
addr0x00000090: addi x22, x18, 1365
addr0x00000094: sw x7, 589(x8)
addr0x00000098: slli x19, x19, 29
addr0x0000009c: srli x20, x2, 25
addr0x000000a0: bge x0, x22, addr0x00000028
addr0x000000a4: add x5, x18, x18
addr0x000000a8: jal x14, addr0x00000204
addr0x000000ac: jal x15, addr0x00000030
addr0x000000b0: bne x21, x28, addr0x00000068
addr0x000000b4: slli x22, x2, 27
addr0x000000b8: add x18, x19, x28
addr0x000000bc: beq x22, x31, addr0x00000018
addr0x000000c0: beq x19, x0, addr0x000001a4
addr0x000000c4: jal x22, addr0x00000120
addr0x000000c8: lw x5, -324(x8)
addr0x000000cc: lw x14, 1397(x8)
addr0x000000d0: lw x18, -770(x8)
addr0x000000d4: lw x31, 1657(x8)
addr0x000000d8: addi x9, x1, 1629
addr0x000000dc: jal x30, addr0x0000022c
addr0x000000e0: addi x20, x27, -809
addr0x000000e4: jal x14, addr0x000000f0
addr0x000000e8: addi x12, x18, -1393
addr0x000000ec: lw x29, 539(x8)
addr0x000000f0: addi x18, x11, 907
addr0x000000f4: jal x27, addr0x000000c8
addr0x000000f8: sw x4, -1394(x8)
addr0x000000fc: sw x18, 22(x8)
addr0x00000100: sw x19, -418(x8)
addr0x00000104: sw x24, -159(x8)
addr0x00000108: lbu x30, -567(x8)
addr0x0000010c: andi x18, x13, -99
addr0x00000110: auipc x20, 856322
addr0x00000114: addi x20, x24, 1149
addr0x00000118: lw x7, 1497(x8)
addr0x0000011c: lw x31, 123(x8)
addr0x00000120: lw x31, -868(x8)
addr0x00000124: lw x9, -857(x8)
addr0x00000128: sw x19, -188(x8)
addr0x0000012c: jal x6, addr0x00000034
addr0x00000130: lw x10, 1886(x8)
addr0x00000134: beq x21, x30, addr0x00000204
addr0x00000138: lw x27, -1442(x8)
addr0x0000013c: lw x26, 699(x8)
addr0x00000140: lbu x26, 626(x8)
addr0x00000144: lw x6, 1016(x8)
addr0x00000148: bne x26, x9, addr0x00000148
addr0x0000014c: addi x17, x7, 2007
addr0x00000150: beq x18, x30, addr0x000000f8
addr0x00000154: addi x29, x6, 1317
addr0x00000158: andi x21, x16, 689
addr0x0000015c: sub x1, x16, x9
addr0x00000160: bge x17, x0, addr0x00000118
addr0x00000164: lui x19, 98552
addr0x00000168: addi x18, x5, 61
addr0x0000016c: jal x5, addr0x000001bc
addr0x00000170: jal x7, addr0x000001f4
addr0x00000174: addi x7, x26, -798
addr0x00000178: sh x13, -2021(x8)
addr0x0000017c: lw x24, 1777(x8)
addr0x00000180: beq x28, x29, addr0x00000250
addr0x00000184: addi x31, x13, 705
addr0x00000188: jal x0, addr0x00000010
addr0x0000018c: sw x24, -309(x8)
addr0x00000190: sw x31, 1555(x8)
addr0x00000194: jal x17, addr0x000000e0
addr0x00000198: lbu x15, 1602(x8)
addr0x0000019c: addi x27, x31, 73
addr0x000001a0: addi x23, x26, 2024
addr0x000001a4: jal x10, addr0x000001ac
addr0x000001a8: lw x25, 414(x8)
addr0x000001ac: addi x30, x18, 1840
addr0x000001b0: lw x0, -323(x8)
addr0x000001b4: jal x7, addr0x00000244
addr0x000001b8: lw x2, -787(x8)
addr0x000001bc: bgeu x28, x4, addr0x00000134
addr0x000001c0: bne x25, x14, addr0x00000070
addr0x000001c4: lw x15, 578(x8)
addr0x000001c8: lw x2, -1197(x8)
addr0x000001cc: lbu x18, 1469(x8)
addr0x000001d0: addi x22, x0, 1818
addr0x000001d4: addi x18, x19, 899
addr0x000001d8: lbu x13, -664(x8)
addr0x000001dc: sw x5, -926(x8)
addr0x000001e0: jal x22, addr0x000001c4
addr0x000001e4: jal x14, addr0x000000ac
addr0x000001e8: addi x17, x16, 412
addr0x000001ec: sw x27, -1260(x8)
addr0x000001f0: sw x2, 609(x8)
addr0x000001f4: sw x22, 1418(x8)
addr0x000001f8: lw x20, -127(x8)
addr0x000001fc: addi x7, x29, -46
addr0x00000200: sw x9, -453(x8)
addr0x00000204: addi x10, x30, 1693
addr0x00000208: sw x14, -481(x8)
addr0x0000020c: jal x19, addr0x000000e4
addr0x00000210: addi x5, x16, -1693
addr0x00000214: slli x13, x18, 24
addr0x00000218: andi x7, x5, 2
addr0x0000021c: srli x5, x0, 15
addr0x00000220: lw x13, 589(x8)
addr0x00000224: sw x27, 397(x8)
addr0x00000228: lui x0, 531281
addr0x0000022c: sw x1, -1989(x8)
addr0x00000230: sw x9, 1068(x8)
addr0x00000234: sw x31, 1553(x8)
addr0x00000238: jal x28, addr0x00000088
addr0x0000023c: addi x23, x7, -797
addr0x00000240: lui x18, 536988
addr0x00000244: srli x29, x22, 31
addr0x00000248: srai x13, x18, 28
addr0x0000024c: lw x2, 1203(x8)
addr0x00000250: sw x19, 1084(x8)
addr0x00000254: sw x17, 27(x8)
addr0x00000258: lbu x9, -378(x8)
addr0x0000025c: sb x19, 95(x8)
addr0x00000260: lw x18, -869(x8)
addr0x00000264: jal x5, addr0x00000158
addr0x00000268: lw x23, -735(x8)
addr0x0000026c: addi x7, x16, 196
addr0x00000270: addi x9, x15, 1847
