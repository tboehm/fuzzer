.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sb x23, -924(x8)
addr0x0000001c: lw x9, 1088(x8)
addr0x00000020: lw x24, 1535(x8)
addr0x00000024: jal x30, addr0x000000e0
addr0x00000028: lui x22, 415079
addr0x0000002c: sw x10, -571(x8)
addr0x00000030: lw x7, 380(x8)
addr0x00000034: srli x14, x26, 11
addr0x00000038: andi x5, x13, 1500
addr0x0000003c: sub x6, x16, x25
addr0x00000040: lw x9, -348(x8)
addr0x00000044: lw x18, 2033(x8)
addr0x00000048: lw x29, 1501(x8)
addr0x0000004c: lw x29, 1641(x8)
addr0x00000050: lw x19, -565(x8)
addr0x00000054: lw x18, -1941(x8)
addr0x00000058: sub x2, x19, x29
addr0x0000005c: bgeu x29, x18, addr0x000001cc
addr0x00000060: lw x25, 1556(x8)
addr0x00000064: lw x22, 913(x8)
addr0x00000068: sw x20, -2001(x8)
addr0x0000006c: sw x13, -1722(x8)
addr0x00000070: sw x12, -311(x8)
addr0x00000074: jal x7, addr0x0000024c
addr0x00000078: addi x5, x9, 1724
addr0x0000007c: sw x9, 2035(x8)
addr0x00000080: lw x18, -915(x8)
addr0x00000084: lw x1, 944(x8)
addr0x00000088: lw x20, -1274(x8)
addr0x0000008c: addi x28, x29, 1073
addr0x00000090: blt x26, x21, addr0x00000234
addr0x00000094: lw x29, -1561(x8)
addr0x00000098: lbu x30, 294(x8)
addr0x0000009c: lbu x17, -847(x8)
addr0x000000a0: lw x21, -1998(x8)
addr0x000000a4: jal x27, addr0x0000023c
addr0x000000a8: lw x27, -1613(x8)
addr0x000000ac: lw x18, -197(x8)
addr0x000000b0: lw x7, 589(x8)
addr0x000000b4: beq x26, x24, addr0x0000000c
addr0x000000b8: lbu x17, -786(x8)
addr0x000000bc: sw x7, -1684(x8)
addr0x000000c0: sw x18, 200(x8)
addr0x000000c4: addi x5, x17, 872
addr0x000000c8: sw x1, 68(x8)
addr0x000000cc: jal x23, addr0x00000250
addr0x000000d0: lw x5, 172(x8)
addr0x000000d4: addi x29, x28, 128
addr0x000000d8: addi x19, x25, -268
addr0x000000dc: jal x30, addr0x00000160
addr0x000000e0: lw x21, 1280(x8)
addr0x000000e4: addi x28, x18, -412
addr0x000000e8: jal x11, addr0x0000000c
addr0x000000ec: lw x0, 214(x8)
addr0x000000f0: addi x24, x21, 185
addr0x000000f4: addi x24, x13, -1176
addr0x000000f8: sw x18, -593(x8)
addr0x000000fc: addi x4, x2, 1821
addr0x00000100: sw x11, -439(x8)
addr0x00000104: beq x21, x23, addr0x00000060
addr0x00000108: sw x2, 1245(x8)
addr0x0000010c: jal x31, addr0x00000004
addr0x00000110: sub x7, x25, x22
addr0x00000114: beq x12, x17, addr0x000002b0
addr0x00000118: beq x7, x13, addr0x0000014c
addr0x0000011c: lw x13, -633(x8)
addr0x00000120: sb x19, -968(x8)
addr0x00000124: sub x9, x7, x14
addr0x00000128: lui x29, 463476
addr0x0000012c: addi x17, x30, -1862
addr0x00000130: lui x7, 302545
addr0x00000134: addi x12, x18, 2044
addr0x00000138: bne x24, x31, addr0x000001a8
addr0x0000013c: lbu x17, 1238(x8)
addr0x00000140: jal x21, addr0x00000240
addr0x00000144: lui x23, 943467
addr0x00000148: lw x21, 1025(x8)
addr0x0000014c: jalr x0, 708(x3)
addr0x00000150: lw x11, -796(x8)
addr0x00000154: jal x27, addr0x0000012c
addr0x00000158: lw x29, 1727(x8)
addr0x0000015c: jal x24, addr0x0000006c
addr0x00000160: addi x5, x12, 1236
addr0x00000164: sw x15, -101(x8)
addr0x00000168: addi x14, x11, 952
addr0x0000016c: sub x5, x5, x5
addr0x00000170: srai x1, x23, 23
addr0x00000174: bgeu x18, x28, addr0x00000080
addr0x00000178: andi x18, x14, 1885
addr0x0000017c: jal x4, addr0x00000244
addr0x00000180: lw x13, 1350(x8)
addr0x00000184: sub x29, x6, x20
addr0x00000188: srai x19, x12, 26
addr0x0000018c: addi x15, x30, 1450
addr0x00000190: lw x23, -1591(x8)
addr0x00000194: addi x0, x18, 1023
addr0x00000198: lw x28, -258(x8)
addr0x0000019c: lw x14, 1100(x8)
addr0x000001a0: lw x13, 1553(x8)
addr0x000001a4: lw x10, 1908(x8)
addr0x000001a8: jal x19, addr0x0000000c
addr0x000001ac: addi x5, x29, 1382
addr0x000001b0: and x23, x18, x20
addr0x000001b4: slli x1, x24, 21
addr0x000001b8: addi x30, x9, 377
addr0x000001bc: bne x13, x10, addr0x00000314
addr0x000001c0: lhu x19, 1484(x8)
addr0x000001c4: sh x7, -742(x8)
addr0x000001c8: sh x19, -913(x8)
addr0x000001cc: add x25, x20, x5
addr0x000001d0: sw x26, 1550(x8)
addr0x000001d4: sw x19, 1098(x8)
addr0x000001d8: jal x20, addr0x00000004
addr0x000001dc: lw x15, 1243(x8)
addr0x000001e0: lw x20, -679(x8)
addr0x000001e4: lw x0, -893(x8)
addr0x000001e8: addi x18, x4, -505
addr0x000001ec: addi x18, x19, 6
addr0x000001f0: add x5, x30, x18
addr0x000001f4: lbu x22, 1594(x8)
addr0x000001f8: divu x31, x20, x16
addr0x000001fc: slli x18, x22, 24
addr0x00000200: lw x18, 1011(x8)
addr0x00000204: lw x29, -1367(x8)
addr0x00000208: lw x9, 529(x8)
addr0x0000020c: beq x14, x2, addr0x000000e4
addr0x00000210: jal x28, addr0x000000e0
addr0x00000214: lw x14, 1860(x8)
addr0x00000218: addi x29, x23, 1940
addr0x0000021c: bne x29, x23, addr0x000002ec
addr0x00000220: jal x27, addr0x0000017c
addr0x00000224: sw x12, -1778(x8)
addr0x00000228: jal x7, addr0x000002d8
addr0x0000022c: sw x15, 30(x8)
addr0x00000230: lw x4, 1052(x8)
addr0x00000234: addi x16, x19, -957
addr0x00000238: lw x29, 501(x8)
addr0x0000023c: addi x20, x14, -1981
addr0x00000240: jal x9, addr0x000002ac
addr0x00000244: sw x7, -1277(x8)
addr0x00000248: addi x19, x2, 1435
addr0x0000024c: sw x0, 1946(x8)
addr0x00000250: jal x17, addr0x00000200
addr0x00000254: lw x22, 315(x8)
addr0x00000258: addi x27, x13, 1026
addr0x0000025c: lui x23, 89168
addr0x00000260: addi x5, x26, -213
addr0x00000264: sw x13, 1196(x8)
addr0x00000268: addi x27, x5, 1339
addr0x0000026c: jal x25, addr0x00000154
addr0x00000270: addi x29, x17, -902
addr0x00000274: addi x27, x14, -1447
addr0x00000278: jal x16, addr0x000000c4
addr0x0000027c: lw x10, -1281(x8)
addr0x00000280: lw x13, -747(x8)
addr0x00000284: add x17, x24, x9
addr0x00000288: lbu x0, 1605(x8)
addr0x0000028c: bge x29, x17, addr0x0000021c
addr0x00000290: lbu x19, -520(x8)
addr0x00000294: addi x22, x0, 1736
addr0x00000298: sw x0, 1221(x8)
addr0x0000029c: addi x7, x24, 35
addr0x000002a0: sw x20, 1079(x8)
addr0x000002a4: lw x29, -179(x8)
addr0x000002a8: jal x18, addr0x0000027c
addr0x000002ac: lw x1, 1942(x8)
addr0x000002b0: addi x22, x31, -1052
addr0x000002b4: addi x22, x30, 2023
addr0x000002b8: sw x14, -1206(x8)
addr0x000002bc: lw x21, -416(x8)
addr0x000002c0: addi x9, x7, 838
addr0x000002c4: sw x27, 1044(x8)
addr0x000002c8: sw x7, -435(x8)
addr0x000002cc: sw x21, 557(x8)
addr0x000002d0: jal x18, addr0x00000250
addr0x000002d4: lw x22, -728(x8)
addr0x000002d8: add x15, x0, x22
addr0x000002dc: lw x18, -148(x8)
addr0x000002e0: jal x27, addr0x00000128
addr0x000002e4: bgeu x22, x22, addr0x00000234
addr0x000002e8: jal x29, addr0x000000e4
addr0x000002ec: srai x22, x18, 30
addr0x000002f0: and x13, x10, x17
addr0x000002f4: srli x2, x23, 1
addr0x000002f8: beq x19, x21, addr0x000001e0
addr0x000002fc: beq x17, x1, addr0x00000274
addr0x00000300: addi x7, x18, -936
addr0x00000304: addi x22, x13, -23
addr0x00000308: addi x9, x19, -2041
addr0x0000030c: sw x23, -1613(x8)
addr0x00000310: addi x15, x17, -1435
addr0x00000314: beq x13, x2, addr0x000002f0
addr0x00000318: beq x13, x22, addr0x000001f8
addr0x0000031c: addi x7, x12, 1944
addr0x00000320: lw x23, -863(x8)
