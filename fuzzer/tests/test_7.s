.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: ori x29, x6, -293
addr0x0000001c: jal x14, addr0x0000001c
addr0x00000020: addi x20, x22, -985
addr0x00000024: lw x20, -272(x8)
addr0x00000028: lw x15, 579(x8)
addr0x0000002c: add x24, x29, x9
addr0x00000030: sw x30, -655(x8)
addr0x00000034: addi x5, x0, -738
addr0x00000038: lui x22, 410454
addr0x0000003c: lui x18, 777908
addr0x00000040: sw x29, 1517(x8)
addr0x00000044: jal x14, addr0x00000048
addr0x00000048: jal x30, addr0x00000040
addr0x0000004c: jal x22, addr0x00000004
addr0x00000050: jal x29, addr0x00000024
addr0x00000054: lw x21, 1577(x8)
addr0x00000058: lw x14, 1825(x8)
addr0x0000005c: lw x5, 457(x8)
addr0x00000060: slli x19, x9, 6
addr0x00000064: srli x23, x20, 17
