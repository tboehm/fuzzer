.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sb x6, 218(x8)
addr0x0000001c: sw x7, 620(x8)
addr0x00000020: sw x9, -185(x8)
addr0x00000024: sw x23, -1199(x8)
addr0x00000028: sw x23, 215(x8)
addr0x0000002c: jal x0, addr0x000000f0
addr0x00000030: lw x22, -566(x8)
addr0x00000034: sw x19, -1083(x8)
addr0x00000038: addi x24, x19, -1179
addr0x0000003c: jal x31, addr0x00000028
addr0x00000040: jal x24, addr0x000006a4
addr0x00000044: jal x24, addr0x00000418
addr0x00000048: sw x9, -1762(x8)
addr0x0000004c: jal x29, addr0x000004b8
addr0x00000050: sw x30, -1540(x8)
addr0x00000054: sw x21, -1666(x8)
addr0x00000058: lbu x15, 110(x8)
addr0x0000005c: addi x23, x24, -843
addr0x00000060: lw x2, -851(x8)
addr0x00000064: lw x22, -1033(x8)
addr0x00000068: sw x19, -2007(x8)
addr0x0000006c: sw x29, -741(x8)
addr0x00000070: sw x24, -563(x8)
addr0x00000074: jal x24, addr0x0000041c
addr0x00000078: jal x29, addr0x00000304
addr0x0000007c: jal x1, addr0x000002d4
addr0x00000080: lw x18, 1961(x8)
addr0x00000084: jal x29, addr0x00000190
addr0x00000088: addi x29, x13, -695
addr0x0000008c: sw x23, -1671(x8)
addr0x00000090: sw x23, 1792(x8)
addr0x00000094: sb x30, -241(x8)
addr0x00000098: sb x20, -1863(x8)
addr0x0000009c: sll x20, x19, x19
addr0x000000a0: or x30, x29, x27
addr0x000000a4: add x0, x22, x10
addr0x000000a8: addi x19, x18, 1192
addr0x000000ac: sw x23, -370(x8)
addr0x000000b0: sw x30, -693(x8)
addr0x000000b4: bne x20, x15, addr0x000006cc
addr0x000000b8: lw x19, 974(x8)
addr0x000000bc: sub x0, x18, x22
addr0x000000c0: addi x22, x0, 565
addr0x000000c4: bne x19, x29, addr0x000005f4
addr0x000000c8: lw x23, 613(x8)
addr0x000000cc: lw x19, 1088(x8)
addr0x000000d0: sw x30, -138(x8)
addr0x000000d4: addi x24, x2, -71
addr0x000000d8: andi x4, x31, -1469
addr0x000000dc: addi x0, x10, -1340
addr0x000000e0: bne x25, x29, addr0x000002cc
addr0x000000e4: lbu x7, 1456(x8)
addr0x000000e8: addi x13, x24, -1919
addr0x000000ec: addi x0, x2, 1590
addr0x000000f0: lw x29, 573(x8)
addr0x000000f4: jal x11, addr0x00000430
addr0x000000f8: jal x16, addr0x000006a8
addr0x000000fc: lw x6, 1467(x8)
addr0x00000100: lw x22, -556(x8)
addr0x00000104: addi x11, x29, -667
addr0x00000108: bne x6, x7, addr0x00000288
addr0x0000010c: jal x29, addr0x00000538
addr0x00000110: lw x19, -1978(x8)
addr0x00000114: add x11, x0, x4
addr0x00000118: jal x13, addr0x00000114
addr0x0000011c: lw x18, -675(x8)
addr0x00000120: jal x15, addr0x00000274
addr0x00000124: sw x11, -1000(x8)
addr0x00000128: lui x0, 949943
addr0x0000012c: jal x9, addr0x000003b0
addr0x00000130: lw x23, -1341(x8)
addr0x00000134: andi x0, x4, -1802
addr0x00000138: bgeu x0, x0, addr0x000002bc
addr0x0000013c: lw x16, -1459(x8)
addr0x00000140: addi x9, x9, 918
addr0x00000144: lui x6, 550538
addr0x00000148: lui x23, 570596
addr0x0000014c: addi x29, x15, 1472
addr0x00000150: sw x22, -1500(x8)
addr0x00000154: addi x19, x0, -1678
addr0x00000158: jal x19, addr0x00000058
addr0x0000015c: sb x29, -1765(x8)
addr0x00000160: sb x19, -1214(x8)
addr0x00000164: jal x6, addr0x000006e8
addr0x00000168: addi x10, x24, -468
addr0x0000016c: addi x22, x20, 569
addr0x00000170: lw x9, 1707(x8)
addr0x00000174: lw x0, -1520(x8)
addr0x00000178: lw x15, 240(x8)
addr0x0000017c: slli x31, x22, 4
addr0x00000180: sw x2, -1961(x8)
addr0x00000184: lw x12, -1044(x8)
addr0x00000188: jal x1, addr0x0000041c
addr0x0000018c: jal x22, addr0x00000440
addr0x00000190: bltu x31, x22, addr0x000002e8
addr0x00000194: beq x2, x17, addr0x0000003c
addr0x00000198: lbu x23, -2039(x8)
addr0x0000019c: andi x4, x4, -386
addr0x000001a0: jal x6, addr0x000006b4
addr0x000001a4: lw x19, 1163(x8)
addr0x000001a8: lw x30, 551(x8)
addr0x000001ac: lw x0, 1591(x8)
addr0x000001b0: jal x0, addr0x00000588
addr0x000001b4: lw x29, -629(x8)
addr0x000001b8: add x27, x6, x29
addr0x000001bc: jal x7, addr0x00000270
addr0x000001c0: lw x4, 1738(x8)
addr0x000001c4: addi x23, x23, 830
addr0x000001c8: jal x1, addr0x00000018
addr0x000001cc: lw x15, -1827(x8)
addr0x000001d0: jal x21, addr0x00000650
addr0x000001d4: addi x7, x7, -1384
addr0x000001d8: add x10, x20, x19
addr0x000001dc: sltu x19, x2, x31
addr0x000001e0: lw x7, 1730(x8)
addr0x000001e4: lbu x4, -1270(x8)
addr0x000001e8: lw x7, -772(x8)
addr0x000001ec: sw x2, -986(x8)
addr0x000001f0: lw x18, 563(x8)
addr0x000001f4: bltu x0, x10, addr0x000004e0
addr0x000001f8: lw x23, 1109(x8)
addr0x000001fc: lw x9, 1410(x8)
addr0x00000200: lw x30, 1978(x8)
addr0x00000204: lhu x0, 1068(x8)
addr0x00000208: lw x23, 580(x8)
addr0x0000020c: jal x21, addr0x000004ac
addr0x00000210: lw x0, 1635(x8)
addr0x00000214: ori x20, x29, -1481
addr0x00000218: slli x12, x0, 10
addr0x0000021c: addi x9, x19, -390
addr0x00000220: jal x20, addr0x00000444
addr0x00000224: jal x23, addr0x000006d0
addr0x00000228: jal x0, addr0x000000ec
addr0x0000022c: lw x7, 1179(x8)
addr0x00000230: lw x18, -137(x8)
addr0x00000234: jal x22, addr0x00000478
addr0x00000238: lui x15, 31583
addr0x0000023c: addi x29, x19, 1021
addr0x00000240: jal x17, addr0x00000490
addr0x00000244: andi x29, x23, -501
addr0x00000248: sub x7, x2, x21
addr0x0000024c: srai x4, x24, 18
addr0x00000250: jal x23, addr0x000005ac
addr0x00000254: bne x17, x10, addr0x00000254
addr0x00000258: addi x13, x30, 1524
addr0x0000025c: bge x21, x30, addr0x00000384
addr0x00000260: bne x23, x22, addr0x00000110
addr0x00000264: bgeu x19, x22, addr0x000004ac
addr0x00000268: slli x15, x18, 22
addr0x0000026c: sw x2, -1667(x8)
addr0x00000270: bne x19, x1, addr0x00000574
addr0x00000274: lw x30, 138(x8)
addr0x00000278: ori x0, x22, 1382
addr0x0000027c: sw x7, -1176(x8)
addr0x00000280: addi x6, x9, -1802
addr0x00000284: jal x29, addr0x0000014c
addr0x00000288: lui x24, 841723
addr0x0000028c: addi x10, x6, 636
addr0x00000290: jal x21, addr0x00000004
addr0x00000294: lw x20, -1803(x8)
addr0x00000298: lw x9, 143(x8)
addr0x0000029c: bne x0, x12, addr0x0000017c
addr0x000002a0: beq x11, x0, addr0x00000658
addr0x000002a4: lbu x29, -402(x8)
addr0x000002a8: blt x1, x27, addr0x00000040
addr0x000002ac: lw x23, 465(x8)
addr0x000002b0: lw x24, 1870(x8)
addr0x000002b4: lw x18, 1638(x8)
addr0x000002b8: addi x30, x11, 417
addr0x000002bc: sw x5, -1978(x8)
addr0x000002c0: sw x4, -1049(x8)
addr0x000002c4: sw x21, -749(x8)
addr0x000002c8: jal x9, addr0x000003c0
addr0x000002cc: bge x20, x15, addr0x00000618
addr0x000002d0: addi x17, x0, -1070
addr0x000002d4: lbu x10, 761(x8)
addr0x000002d8: lw x29, -1998(x8)
addr0x000002dc: sw x6, -142(x8)
addr0x000002e0: bne x19, x22, addr0x00000624
addr0x000002e4: lbu x10, 419(x8)
addr0x000002e8: addi x6, x16, -2028
addr0x000002ec: sw x6, -64(x8)
addr0x000002f0: jal x23, addr0x00000410
addr0x000002f4: lbu x0, -1879(x8)
addr0x000002f8: andi x21, x22, 323
addr0x000002fc: jal x19, addr0x0000022c
addr0x00000300: sw x22, 268(x8)
addr0x00000304: jal x4, addr0x0000021c
addr0x00000308: lw x29, 1476(x8)
addr0x0000030c: addi x5, x15, -2040
addr0x00000310: lw x22, -1239(x8)
addr0x00000314: bgeu x29, x18, addr0x0000014c
addr0x00000318: bne x9, x4, addr0x00000550
addr0x0000031c: lw x5, 1094(x8)
addr0x00000320: lw x24, 1245(x8)
addr0x00000324: addi x23, x29, -1769
addr0x00000328: sw x29, 1976(x8)
addr0x0000032c: sw x24, 1231(x8)
addr0x00000330: addi x5, x29, 866
addr0x00000334: lw x19, 507(x8)
addr0x00000338: jal x18, addr0x00000104
addr0x0000033c: lw x22, 1477(x8)
addr0x00000340: addi x2, x18, 2038
addr0x00000344: jal x17, addr0x00000468
addr0x00000348: addi x29, x21, -452
addr0x0000034c: sw x23, -1647(x8)
addr0x00000350: jal x25, addr0x00000600
addr0x00000354: lw x4, 1198(x8)
addr0x00000358: lw x7, -1692(x8)
addr0x0000035c: lw x24, -952(x8)
addr0x00000360: jalr x0, 332(x3)
addr0x00000364: sb x5, -1604(x8)
addr0x00000368: lw x13, 1950(x8)
addr0x0000036c: sub x31, x6, x0
addr0x00000370: sw x0, 164(x8)
addr0x00000374: addi x14, x5, 1033
addr0x00000378: addi x24, x15, -602
addr0x0000037c: lw x0, 39(x8)
addr0x00000380: lw x25, -191(x8)
addr0x00000384: sw x16, 2013(x8)
addr0x00000388: sw x15, -424(x8)
addr0x0000038c: addi x2, x18, 1361
addr0x00000390: srli x1, x7, 0
addr0x00000394: bltu x0, x5, addr0x0000040c
addr0x00000398: add x19, x0, x19
addr0x0000039c: jal x29, addr0x0000019c
addr0x000003a0: sw x7, -1303(x8)
addr0x000003a4: sw x23, -1252(x8)
addr0x000003a8: sw x21, 83(x8)
addr0x000003ac: jal x0, addr0x000005ec
addr0x000003b0: addi x9, x1, 1972
addr0x000003b4: lui x1, 345388
addr0x000003b8: addi x0, x0, 269
addr0x000003bc: addi x0, x20, -1268
addr0x000003c0: blt x18, x16, addr0x000002ac
addr0x000003c4: jal x19, addr0x000006d0
addr0x000003c8: lw x31, -400(x8)
addr0x000003cc: sw x5, 667(x8)
addr0x000003d0: lbu x22, -664(x8)
addr0x000003d4: beq x0, x19, addr0x000001ec
addr0x000003d8: addi x30, x1, 211
addr0x000003dc: lui x7, 837597
addr0x000003e0: addi x9, x18, 1010
addr0x000003e4: addi x0, x9, -276
addr0x000003e8: auipc x17, 312302
addr0x000003ec: jalr x0, 340(x3)
addr0x000003f0: beq x18, x24, addr0x00000594
addr0x000003f4: addi x4, x21, 703
addr0x000003f8: jal x4, addr0x00000594
addr0x000003fc: lw x9, -1158(x8)
addr0x00000400: lw x2, 1093(x8)
addr0x00000404: bne x7, x0, addr0x000001ac
addr0x00000408: sw x7, -1658(x8)
addr0x0000040c: addi x15, x4, -1705
addr0x00000410: sw x18, 889(x8)
addr0x00000414: addi x23, x24, 933
addr0x00000418: lw x15, 1103(x8)
addr0x0000041c: addi x31, x24, -97
addr0x00000420: lhu x20, -1599(x8)
addr0x00000424: addi x23, x19, 885
addr0x00000428: sw x23, -949(x8)
addr0x0000042c: sw x11, 814(x8)
addr0x00000430: addi x4, x19, 1504
addr0x00000434: addi x29, x9, -1654
addr0x00000438: bne x29, x30, addr0x0000022c
addr0x0000043c: lw x9, -905(x8)
addr0x00000440: jal x18, addr0x00000074
addr0x00000444: slli x7, x21, 0
addr0x00000448: sw x18, 1763(x8)
addr0x0000044c: jal x0, addr0x00000260
addr0x00000450: lw x4, 1789(x8)
addr0x00000454: lui x31, 236899
addr0x00000458: addi x0, x30, 1475
addr0x0000045c: addi x22, x29, 933
addr0x00000460: jal x20, addr0x000006dc
addr0x00000464: slli x1, x7, 11
addr0x00000468: lui x23, 286485
addr0x0000046c: andi x30, x24, 971
addr0x00000470: bne x17, x11, addr0x000002f8
addr0x00000474: srli x18, x22, 8
addr0x00000478: addi x9, x11, 447
addr0x0000047c: sw x21, -1564(x8)
addr0x00000480: jal x17, addr0x000003c4
addr0x00000484: andi x18, x19, -1600
addr0x00000488: addi x23, x20, 1783
addr0x0000048c: sw x4, 1782(x8)
addr0x00000490: jal x15, addr0x00000098
addr0x00000494: lw x23, 1680(x8)
addr0x00000498: lw x7, -1655(x8)
addr0x0000049c: sw x30, 698(x8)
addr0x000004a0: sw x23, 1704(x8)
addr0x000004a4: lw x15, -1083(x8)
addr0x000004a8: jal x20, addr0x00000620
addr0x000004ac: lui x0, 452519
addr0x000004b0: addi x23, x30, -873
addr0x000004b4: sw x10, 537(x8)
addr0x000004b8: jal x18, addr0x00000018
addr0x000004bc: jal x0, addr0x00000540
addr0x000004c0: lw x9, 306(x8)
addr0x000004c4: jal x30, addr0x000001c0
addr0x000004c8: lhu x18, 1161(x8)
addr0x000004cc: sub x19, x23, x1
addr0x000004d0: bgeu x15, x16, addr0x000001d0
addr0x000004d4: jal x0, addr0x000006f8
addr0x000004d8: jal x4, addr0x000004b4
addr0x000004dc: lw x27, 1940(x8)
addr0x000004e0: add x30, x29, x29
addr0x000004e4: jal x29, addr0x00000354
addr0x000004e8: jal x9, addr0x000005fc
addr0x000004ec: lw x20, -1970(x8)
addr0x000004f0: beq x5, x29, addr0x00000280
addr0x000004f4: addi x23, x4, -1622
addr0x000004f8: sw x13, -784(x8)
addr0x000004fc: jal x13, addr0x00000144
addr0x00000500: jal x22, addr0x0000038c
addr0x00000504: lw x6, -936(x8)
addr0x00000508: lw x0, -269(x8)
addr0x0000050c: lw x18, 1498(x8)
addr0x00000510: sw x4, -1091(x8)
addr0x00000514: sw x19, -1353(x8)
addr0x00000518: slli x15, x19, 9
addr0x0000051c: add x9, x22, x20
addr0x00000520: jal x23, addr0x00000074
addr0x00000524: lw x0, -363(x8)
addr0x00000528: lw x4, -950(x8)
addr0x0000052c: addi x19, x11, 803
addr0x00000530: lw x19, 1882(x8)
addr0x00000534: addi x0, x24, -651
addr0x00000538: jal x17, addr0x000001cc
addr0x0000053c: lui x22, 286294
addr0x00000540: sub x27, x9, x31
addr0x00000544: lw x13, -1360(x8)
addr0x00000548: sh x15, 1817(x8)
addr0x0000054c: srli x9, x21, 17
addr0x00000550: bltu x5, x9, addr0x00000188
addr0x00000554: lw x17, 1353(x8)
addr0x00000558: sw x24, -453(x8)
addr0x0000055c: addi x0, x5, -1332
addr0x00000560: sw x12, -852(x8)
addr0x00000564: lw x7, -657(x8)
addr0x00000568: lw x16, -919(x8)
addr0x0000056c: addi x1, x30, -1937
addr0x00000570: sw x23, -594(x8)
addr0x00000574: sw x30, -206(x8)
addr0x00000578: sw x31, -964(x8)
addr0x0000057c: sw x22, 265(x8)
addr0x00000580: jal x23, addr0x00000064
addr0x00000584: sw x14, -2031(x8)
addr0x00000588: lw x17, -1232(x8)
addr0x0000058c: addi x23, x27, 97
addr0x00000590: addi x0, x31, 890
addr0x00000594: jal x29, addr0x0000063c
addr0x00000598: jal x11, addr0x00000610
addr0x0000059c: jal x19, addr0x000002ac
addr0x000005a0: lw x0, -59(x8)
addr0x000005a4: addi x29, x1, -2024
addr0x000005a8: jal x11, addr0x00000564
addr0x000005ac: lw x22, 813(x8)
addr0x000005b0: addi x21, x30, 1087
addr0x000005b4: sw x15, -1403(x8)
addr0x000005b8: sw x13, -85(x8)
addr0x000005bc: jal x20, addr0x00000654
addr0x000005c0: addi x15, x2, 1897
addr0x000005c4: sw x30, 1795(x8)
addr0x000005c8: sw x24, -889(x8)
addr0x000005cc: sw x21, -1665(x8)
addr0x000005d0: lw x7, -383(x8)
addr0x000005d4: add x9, x22, x7
addr0x000005d8: lui x6, 819788
addr0x000005dc: addi x23, x13, -1876
addr0x000005e0: sw x13, 1824(x8)
addr0x000005e4: sw x5, 1449(x8)
addr0x000005e8: jal x22, addr0x000003b4
addr0x000005ec: addi x6, x4, 1691
addr0x000005f0: lbu x19, -845(x8)
addr0x000005f4: slli x17, x9, 28
addr0x000005f8: lw x29, 151(x8)
addr0x000005fc: lw x16, 92(x8)
addr0x00000600: sw x4, 1774(x8)
addr0x00000604: jal x22, addr0x0000071c
addr0x00000608: sw x0, -1260(x8)
addr0x0000060c: jal x0, addr0x00000488
addr0x00000610: lw x29, -1472(x8)
addr0x00000614: addi x23, x9, -458
addr0x00000618: add x5, x21, x23
addr0x0000061c: addi x23, x18, -95
addr0x00000620: jal x30, addr0x00000204
addr0x00000624: sw x0, -1052(x8)
addr0x00000628: jal x29, addr0x00000618
addr0x0000062c: lw x24, -911(x8)
addr0x00000630: lw x23, 112(x8)
addr0x00000634: lw x6, -843(x8)
addr0x00000638: lw x14, 796(x8)
addr0x0000063c: addi x1, x29, 1250
addr0x00000640: sw x22, -1209(x8)
addr0x00000644: sw x30, -1266(x8)
addr0x00000648: lbu x5, 1153(x8)
addr0x0000064c: slli x4, x14, 3
addr0x00000650: lw x19, -1575(x8)
addr0x00000654: lw x23, -1066(x8)
addr0x00000658: addi x23, x13, 1649
addr0x0000065c: addi x0, x21, -396
addr0x00000660: srai x23, x23, 3
addr0x00000664: and x6, x1, x13
addr0x00000668: sw x7, -190(x8)
addr0x0000066c: jal x24, addr0x000001b0
addr0x00000670: sw x14, 1592(x8)
addr0x00000674: sw x15, -1747(x8)
addr0x00000678: jal x5, addr0x0000071c
addr0x0000067c: lw x7, 727(x8)
addr0x00000680: jal x10, addr0x0000067c
addr0x00000684: lbu x22, 2012(x8)
addr0x00000688: beq x30, x31, addr0x00000298
addr0x0000068c: lw x19, -505(x8)
addr0x00000690: addi x0, x6, 367
addr0x00000694: sw x7, 1497(x8)
addr0x00000698: sw x4, 1860(x8)
addr0x0000069c: lw x21, 1447(x8)
addr0x000006a0: addi x29, x0, -1886
addr0x000006a4: lui x11, 286633
addr0x000006a8: jal x24, addr0x0000012c
addr0x000006ac: jal x23, addr0x00000110
addr0x000006b0: auipc x30, 710681
addr0x000006b4: addi x24, x21, -393
addr0x000006b8: jal x19, addr0x000006e4
addr0x000006bc: lw x31, 981(x8)
addr0x000006c0: lw x23, -1033(x8)
addr0x000006c4: sw x11, -1192(x8)
addr0x000006c8: sw x29, -1758(x8)
addr0x000006cc: sw x21, -667(x8)
addr0x000006d0: jal x2, addr0x000006e8
addr0x000006d4: lw x4, -358(x8)
addr0x000006d8: lw x0, 930(x8)
addr0x000006dc: jal x12, addr0x000000f0
addr0x000006e0: lui x0, 838417
addr0x000006e4: sub x23, x5, x31
addr0x000006e8: bgeu x9, x7, addr0x00000460
addr0x000006ec: addi x19, x1, -1030
addr0x000006f0: addi x22, x10, 1250
addr0x000006f4: addi x5, x4, -23
addr0x000006f8: sw x0, -138(x8)
addr0x000006fc: bltu x2, x21, addr0x000002f4
addr0x00000700: lw x7, 1154(x8)
addr0x00000704: lw x6, -1616(x8)
addr0x00000708: ori x22, x0, 1483
addr0x0000070c: sw x2, 1103(x8)
addr0x00000710: sw x0, -563(x8)
addr0x00000714: lw x23, -1427(x8)
addr0x00000718: sw x23, 1639(x8)
addr0x0000071c: sw x23, 1920(x8)
addr0x00000720: jal x14, addr0x000006b0
addr0x00000724: andi x19, x5, 1701
addr0x00000728: add x4, x5, x30
