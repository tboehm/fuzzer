.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: xori x29, x19, 162
addr0x0000001c: andi x9, x7, -1295
addr0x00000020: addi x22, x9, -1129
addr0x00000024: addi x1, x24, -1841
addr0x00000028: addi x0, x23, -1729
addr0x0000002c: sw x19, -1372(x8)
addr0x00000030: addi x18, x18, -1254
addr0x00000034: sw x4, -19(x8)
addr0x00000038: jal x31, addr0x00000460
addr0x0000003c: jal x9, addr0x00000448
addr0x00000040: addi x15, x14, 2034
addr0x00000044: jal x31, addr0x0000043c
addr0x00000048: addi x19, x30, -625
addr0x0000004c: addi x17, x31, -1905
addr0x00000050: sb x28, -492(x8)
addr0x00000054: sw x13, -42(x8)
addr0x00000058: addi x28, x1, 278
addr0x0000005c: sw x6, 18(x8)
addr0x00000060: lw x12, -15(x8)
addr0x00000064: addi x16, x17, 953
addr0x00000068: sw x15, -253(x8)
addr0x0000006c: lw x25, 1934(x8)
addr0x00000070: sw x29, 1235(x8)
addr0x00000074: sw x17, -1262(x8)
addr0x00000078: sw x12, -1833(x8)
addr0x0000007c: jalr x0, 220(x3)
addr0x00000080: sb x18, -473(x8)
addr0x00000084: lw x10, -11(x8)
addr0x00000088: jal x16, addr0x00000064
addr0x0000008c: lhu x28, -366(x8)
addr0x00000090: lui x9, 774740
addr0x00000094: addi x14, x24, -695
addr0x00000098: jal x14, addr0x00000690
addr0x0000009c: addi x30, x18, -886
addr0x000000a0: andi x23, x28, -582
addr0x000000a4: andi x27, x20, 240
addr0x000000a8: addi x16, x2, -385
addr0x000000ac: sw x7, -342(x8)
addr0x000000b0: jal x23, addr0x000000f8
addr0x000000b4: lw x19, -1683(x8)
addr0x000000b8: lw x23, 1029(x8)
addr0x000000bc: lw x20, 1159(x8)
addr0x000000c0: addi x28, x13, 1796
addr0x000000c4: jal x18, addr0x0000033c
addr0x000000c8: sltu x23, x18, x14
addr0x000000cc: or x5, x26, x16
addr0x000000d0: add x14, x7, x12
addr0x000000d4: sub x20, x20, x26
addr0x000000d8: sw x0, -37(x8)
addr0x000000dc: jal x4, addr0x00000730
addr0x000000e0: lw x6, 1746(x8)
addr0x000000e4: lui x17, 133660
addr0x000000e8: lui x19, 366907
addr0x000000ec: addi x14, x29, -574
addr0x000000f0: sw x11, 1786(x8)
addr0x000000f4: lbu x25, 1417(x8)
addr0x000000f8: sw x1, -597(x8)
addr0x000000fc: beq x27, x19, addr0x000001b8
addr0x00000100: andi x23, x30, -242
addr0x00000104: bgeu x10, x18, addr0x00000358
addr0x00000108: lw x18, -9(x8)
addr0x0000010c: or x19, x20, x13
addr0x00000110: addi x18, x14, 686
addr0x00000114: lb x0, -249(x8)
addr0x00000118: slli x5, x1, 19
addr0x0000011c: bne x31, x9, addr0x000000d4
addr0x00000120: sb x23, 760(x8)
addr0x00000124: lw x18, -1295(x8)
addr0x00000128: sw x19, 512(x8)
addr0x0000012c: lw x13, 1724(x8)
addr0x00000130: lw x11, 966(x8)
addr0x00000134: lw x4, -1406(x8)
addr0x00000138: addi x7, x11, 1968
addr0x0000013c: sw x13, -336(x8)
addr0x00000140: sw x16, 1608(x8)
addr0x00000144: lui x20, 717965
addr0x00000148: addi x0, x19, 866
addr0x0000014c: lw x13, -773(x8)
addr0x00000150: addi x5, x20, -201
addr0x00000154: jal x24, addr0x00000794
addr0x00000158: lui x20, 1017634
addr0x0000015c: sw x5, -383(x8)
addr0x00000160: sw x25, 393(x8)
addr0x00000164: lw x29, 1963(x8)
addr0x00000168: addi x5, x24, -1527
addr0x0000016c: sw x19, -1256(x8)
addr0x00000170: lw x20, -710(x8)
addr0x00000174: addi x9, x7, 1753
addr0x00000178: lw x11, -1043(x8)
addr0x0000017c: bgeu x26, x15, addr0x00000268
addr0x00000180: jal x19, addr0x00000284
addr0x00000184: sw x20, 1036(x8)
addr0x00000188: sw x17, 501(x8)
addr0x0000018c: sw x25, -1439(x8)
addr0x00000190: lbu x27, 127(x8)
addr0x00000194: lw x20, -1280(x8)
addr0x00000198: lui x25, 710562
addr0x0000019c: addi x7, x22, -827
addr0x000001a0: andi x18, x0, 109
addr0x000001a4: bne x21, x0, addr0x0000044c
addr0x000001a8: jal x12, addr0x000001fc
addr0x000001ac: andi x4, x13, -921
addr0x000001b0: lw x2, 1407(x8)
addr0x000001b4: lw x0, 1800(x8)
addr0x000001b8: lbu x25, -1771(x8)
addr0x000001bc: slli x9, x9, 28
addr0x000001c0: or x18, x6, x15
addr0x000001c4: lbu x0, -775(x8)
addr0x000001c8: sb x18, 783(x8)
addr0x000001cc: sub x0, x20, x14
addr0x000001d0: ori x26, x18, 1439
addr0x000001d4: sw x5, -1263(x8)
addr0x000001d8: lw x4, 1310(x8)
addr0x000001dc: lw x27, 1316(x8)
addr0x000001e0: lw x28, 1037(x8)
addr0x000001e4: lw x0, 896(x8)
addr0x000001e8: ori x23, x28, -1697
addr0x000001ec: srai x22, x9, 28
addr0x000001f0: bgeu x19, x9, addr0x00000328
addr0x000001f4: lw x18, -579(x8)
addr0x000001f8: sw x25, -1839(x8)
addr0x000001fc: jal x18, addr0x00000090
addr0x00000200: lw x25, 105(x8)
addr0x00000204: lw x9, 397(x8)
addr0x00000208: lw x18, 2045(x8)
addr0x0000020c: lw x2, -1652(x8)
addr0x00000210: sw x9, 61(x8)
addr0x00000214: lui x27, 803137
addr0x00000218: addi x15, x18, 1089
addr0x0000021c: add x15, x15, x31
addr0x00000220: lw x15, -823(x8)
addr0x00000224: lw x7, 1939(x8)
addr0x00000228: lw x13, -1506(x8)
addr0x0000022c: bne x13, x19, addr0x00000700
addr0x00000230: andi x5, x24, 1486
addr0x00000234: jal x14, addr0x000006d8
addr0x00000238: jal x4, addr0x00000168
addr0x0000023c: lw x0, 1421(x8)
addr0x00000240: addi x10, x18, 1581
addr0x00000244: jal x7, addr0x00000604
addr0x00000248: jal x18, addr0x00000408
addr0x0000024c: lui x27, 738835
addr0x00000250: addi x24, x29, -1404
addr0x00000254: addi x23, x7, 1777
addr0x00000258: sw x29, 517(x8)
addr0x0000025c: beq x15, x30, addr0x000006d8
addr0x00000260: addi x12, x9, 1906
addr0x00000264: add x7, x27, x18
addr0x00000268: lw x6, 1678(x8)
addr0x0000026c: jal x9, addr0x00000358
addr0x00000270: sw x29, 1306(x8)
addr0x00000274: lw x21, -741(x8)
addr0x00000278: lw x31, -77(x8)
addr0x0000027c: lw x29, -884(x8)
addr0x00000280: sw x7, -949(x8)
addr0x00000284: jal x0, addr0x00000790
addr0x00000288: jal x16, addr0x00000164
addr0x0000028c: jal x13, addr0x00000478
addr0x00000290: sw x24, -674(x8)
addr0x00000294: lw x7, -1373(x8)
addr0x00000298: addi x5, x13, -384
addr0x0000029c: addi x24, x29, 804
addr0x000002a0: jal x5, addr0x00000020
addr0x000002a4: lw x20, -1559(x8)
addr0x000002a8: lw x22, -2037(x8)
addr0x000002ac: jal x19, addr0x0000020c
addr0x000002b0: jal x27, addr0x00000040
addr0x000002b4: lw x7, -1540(x8)
addr0x000002b8: addi x15, x10, 629
addr0x000002bc: jal x25, addr0x000003e8
addr0x000002c0: lw x22, 795(x8)
addr0x000002c4: lw x29, 1153(x8)
addr0x000002c8: addi x14, x31, 249
addr0x000002cc: jal x18, addr0x000001f8
addr0x000002d0: lbu x4, -1115(x8)
addr0x000002d4: addi x20, x5, -37
addr0x000002d8: lw x22, 1335(x8)
addr0x000002dc: ori x9, x5, 32
addr0x000002e0: sw x19, 1127(x8)
addr0x000002e4: sb x2, -158(x8)
addr0x000002e8: sb x7, 23(x8)
addr0x000002ec: lbu x2, 1779(x8)
addr0x000002f0: bne x5, x24, addr0x000007a0
addr0x000002f4: sub x19, x9, x15
addr0x000002f8: srai x13, x29, 22
addr0x000002fc: lw x19, 810(x8)
addr0x00000300: jal x12, addr0x000000dc
addr0x00000304: lw x31, -651(x8)
addr0x00000308: addi x13, x30, -764
addr0x0000030c: bne x22, x0, addr0x000003c8
addr0x00000310: addi x20, x31, 267
addr0x00000314: sw x9, -1790(x8)
addr0x00000318: jal x15, addr0x00000190
addr0x0000031c: jal x18, addr0x00000790
addr0x00000320: jal x9, addr0x0000064c
addr0x00000324: jal x19, addr0x0000003c
addr0x00000328: addi x30, x14, 1174
addr0x0000032c: add x18, x23, x14
addr0x00000330: lw x23, -538(x8)
addr0x00000334: sw x13, -1616(x8)
addr0x00000338: lw x4, -1176(x8)
addr0x0000033c: lw x17, 64(x8)
addr0x00000340: addi x4, x10, -1431
addr0x00000344: lw x25, -1384(x8)
addr0x00000348: lw x16, -441(x8)
addr0x0000034c: add x10, x23, x31
addr0x00000350: add x19, x6, x6
addr0x00000354: bgeu x19, x13, addr0x000002d0
addr0x00000358: sw x30, -306(x8)
addr0x0000035c: sub x9, x4, x22
addr0x00000360: sw x13, -1565(x8)
addr0x00000364: sub x10, x22, x7
addr0x00000368: beq x28, x29, addr0x000003e0
addr0x0000036c: lbu x9, 1336(x8)
addr0x00000370: slli x27, x18, 25
addr0x00000374: add x21, x23, x12
addr0x00000378: jal x7, addr0x0000030c
addr0x0000037c: jal x27, addr0x00000660
addr0x00000380: lw x5, 790(x8)
addr0x00000384: bne x31, x17, addr0x00000380
addr0x00000388: addi x13, x14, 1611
addr0x0000038c: sh x4, -808(x8)
addr0x00000390: bne x18, x4, addr0x00000128
addr0x00000394: slli x15, x26, 17
addr0x00000398: lbu x0, 923(x8)
addr0x0000039c: addi x10, x31, 20
addr0x000003a0: sh x5, 1602(x8)
addr0x000003a4: lw x11, -1676(x8)
addr0x000003a8: lw x19, 807(x8)
addr0x000003ac: lbu x18, 1764(x8)
addr0x000003b0: sb x20, -1271(x8)
addr0x000003b4: sub x13, x23, x20
addr0x000003b8: bge x16, x30, addr0x0000002c
addr0x000003bc: addi x18, x0, -1098
addr0x000003c0: lhu x9, 460(x8)
addr0x000003c4: addi x29, x7, -1826
addr0x000003c8: sw x0, -311(x8)
addr0x000003cc: lw x5, -1832(x8)
addr0x000003d0: sw x25, -336(x8)
addr0x000003d4: lw x14, -893(x8)
addr0x000003d8: sw x13, -1095(x8)
addr0x000003dc: sw x21, -1009(x8)
addr0x000003e0: sw x4, -1881(x8)
addr0x000003e4: jal x14, addr0x0000036c
addr0x000003e8: lw x20, 1184(x8)
addr0x000003ec: jal x24, addr0x000005a8
addr0x000003f0: addi x9, x18, -936
addr0x000003f4: sw x17, -104(x8)
addr0x000003f8: lui x6, 971489
addr0x000003fc: addi x23, x29, -1501
addr0x00000400: addi x27, x29, -8
addr0x00000404: jal x5, addr0x0000017c
addr0x00000408: jal x29, addr0x000003c4
addr0x0000040c: addi x18, x7, -1802
addr0x00000410: lw x18, -212(x8)
addr0x00000414: lw x20, -1214(x8)
addr0x00000418: lw x25, -1044(x8)
addr0x0000041c: addi x2, x27, 1632
addr0x00000420: jal x19, addr0x00000508
addr0x00000424: slti x18, x7, -433
addr0x00000428: addi x20, x30, -1038
addr0x0000042c: lhu x29, -1624(x8)
addr0x00000430: jal x20, addr0x00000620
addr0x00000434: lw x27, -855(x8)
addr0x00000438: lw x14, 1124(x8)
addr0x0000043c: sw x29, -329(x8)
addr0x00000440: jal x30, addr0x00000098
addr0x00000444: lw x19, -393(x8)
addr0x00000448: jal x26, addr0x0000009c
addr0x0000044c: lw x15, 394(x8)
addr0x00000450: bgeu x20, x12, addr0x00000500
addr0x00000454: lw x14, 1057(x8)
addr0x00000458: lw x7, 662(x8)
addr0x0000045c: slli x5, x4, 28
addr0x00000460: lw x1, -1031(x8)
addr0x00000464: lw x18, 1288(x8)
addr0x00000468: addi x18, x27, -222
addr0x0000046c: jal x10, addr0x00000734
addr0x00000470: addi x24, x25, -1878
addr0x00000474: lw x20, -1549(x8)
addr0x00000478: addi x0, x4, -430
addr0x0000047c: sw x18, 1649(x8)
addr0x00000480: sw x16, 754(x8)
addr0x00000484: lw x29, 207(x8)
addr0x00000488: jal x1, addr0x00000484
addr0x0000048c: sw x31, -318(x8)
addr0x00000490: sw x16, 663(x8)
addr0x00000494: lw x23, 1567(x8)
addr0x00000498: addi x13, x5, -1762
addr0x0000049c: lbu x22, 262(x8)
addr0x000004a0: lw x18, 428(x8)
addr0x000004a4: addi x27, x22, 192
addr0x000004a8: lw x16, -1754(x8)
addr0x000004ac: lbu x14, 159(x8)
addr0x000004b0: beq x6, x16, addr0x0000020c
addr0x000004b4: andi x20, x23, -573
addr0x000004b8: andi x27, x0, 441
addr0x000004bc: jal x14, addr0x00000588
addr0x000004c0: lw x17, 930(x8)
addr0x000004c4: lw x27, -390(x8)
addr0x000004c8: lw x25, -1457(x8)
addr0x000004cc: lw x14, -336(x8)
addr0x000004d0: slli x30, x22, 27
addr0x000004d4: srli x22, x1, 11
addr0x000004d8: addi x1, x5, -508
addr0x000004dc: lhu x27, 1479(x8)
addr0x000004e0: addi x19, x21, 1958
addr0x000004e4: sw x24, -1682(x8)
addr0x000004e8: sw x18, 146(x8)
addr0x000004ec: jalr x0, 296(x3)
addr0x000004f0: jal x17, addr0x00000758
addr0x000004f4: addi x9, x20, 941
addr0x000004f8: addi x23, x15, 516
addr0x000004fc: sw x22, -684(x8)
addr0x00000500: addi x10, x16, -374
addr0x00000504: add x17, x9, x20
addr0x00000508: addi x4, x20, 822
addr0x0000050c: lw x7, -338(x8)
addr0x00000510: lw x27, 1054(x8)
addr0x00000514: sw x14, -296(x8)
addr0x00000518: andi x19, x19, 539
addr0x0000051c: addi x14, x18, -1706
addr0x00000520: sw x15, -970(x8)
addr0x00000524: andi x0, x7, -1912
addr0x00000528: andi x29, x9, -1340
addr0x0000052c: sw x17, -629(x8)
addr0x00000530: jal x16, addr0x00000598
addr0x00000534: lw x0, 764(x8)
addr0x00000538: jal x22, addr0x00000694
addr0x0000053c: lw x5, -1385(x8)
addr0x00000540: lw x21, -109(x8)
addr0x00000544: lw x30, 1845(x8)
addr0x00000548: jal x18, addr0x00000108
addr0x0000054c: beq x9, x26, addr0x00000334
addr0x00000550: jal x17, addr0x00000324
addr0x00000554: lhu x25, 656(x8)
addr0x00000558: sw x31, -1185(x8)
addr0x0000055c: jal x18, addr0x00000474
addr0x00000560: lw x10, -1279(x8)
addr0x00000564: beq x9, x15, addr0x000003bc
addr0x00000568: lhu x20, 995(x8)
addr0x0000056c: addi x4, x5, 917
addr0x00000570: sw x30, -1566(x8)
addr0x00000574: sw x22, 1107(x8)
addr0x00000578: add x5, x19, x15
addr0x0000057c: lui x13, 200704
addr0x00000580: addi x18, x23, 173
addr0x00000584: jal x11, addr0x00000434
addr0x00000588: lw x29, 1941(x8)
addr0x0000058c: jal x15, addr0x000002ec
addr0x00000590: addi x22, x19, 494
addr0x00000594: addi x7, x11, 1393
addr0x00000598: addi x7, x15, 1456
addr0x0000059c: jal x24, addr0x0000066c
addr0x000005a0: jal x4, addr0x000006ec
addr0x000005a4: addi x21, x22, -1285
addr0x000005a8: lw x13, 1362(x8)
addr0x000005ac: bge x19, x13, addr0x00000004
addr0x000005b0: addi x13, x4, 764
addr0x000005b4: jal x23, addr0x00000514
addr0x000005b8: lw x5, -782(x8)
addr0x000005bc: addi x15, x14, -1570
addr0x000005c0: jal x20, addr0x000003a8
addr0x000005c4: addi x31, x4, -185
addr0x000005c8: jal x13, addr0x00000680
addr0x000005cc: jal x15, addr0x000000e8
addr0x000005d0: lui x31, 60055
addr0x000005d4: addi x23, x21, -839
addr0x000005d8: jal x5, addr0x000002c8
addr0x000005dc: lw x0, -102(x8)
addr0x000005e0: addi x23, x17, 856
addr0x000005e4: sw x28, -1994(x8)
addr0x000005e8: lw x26, 396(x8)
addr0x000005ec: jal x0, addr0x000000d4
addr0x000005f0: addi x7, x20, 944
addr0x000005f4: jal x25, addr0x00000174
addr0x000005f8: lw x30, 932(x8)
addr0x000005fc: lw x10, 1548(x8)
addr0x00000600: slli x19, x23, 23
addr0x00000604: jal x12, addr0x000002e4
addr0x00000608: lw x22, 376(x8)
addr0x0000060c: lw x30, 2(x8)
addr0x00000610: jal x27, addr0x00000390
addr0x00000614: lbu x29, 1640(x8)
addr0x00000618: addi x0, x7, 1846
addr0x0000061c: addi x13, x31, 910
addr0x00000620: jal x5, addr0x00000130
addr0x00000624: andi x29, x28, 1524
addr0x00000628: add x0, x7, x7
addr0x0000062c: jal x14, addr0x00000340
addr0x00000630: lw x29, -909(x8)
addr0x00000634: addi x29, x11, 738
addr0x00000638: add x29, x15, x0
addr0x0000063c: sw x22, 248(x8)
addr0x00000640: addi x30, x19, 1968
addr0x00000644: jal x13, addr0x0000064c
addr0x00000648: jal x16, addr0x000000cc
addr0x0000064c: sw x23, -1735(x8)
addr0x00000650: lw x18, -827(x8)
addr0x00000654: addi x13, x27, -235
addr0x00000658: addi x5, x29, -1564
addr0x0000065c: addi x22, x29, 338
addr0x00000660: sw x5, -1239(x8)
addr0x00000664: lw x5, -387(x8)
addr0x00000668: add x22, x22, x0
addr0x0000066c: jal x1, addr0x000005e0
addr0x00000670: sw x23, 556(x8)
addr0x00000674: lui x6, 425788
addr0x00000678: and x27, x27, x19
addr0x0000067c: add x26, x28, x22
addr0x00000680: addi x13, x24, 1190
addr0x00000684: lw x6, 1273(x8)
addr0x00000688: lw x0, 1997(x8)
addr0x0000068c: addi x7, x28, 526
addr0x00000690: sw x29, 558(x8)
addr0x00000694: sw x6, 23(x8)
addr0x00000698: sw x14, 36(x8)
addr0x0000069c: sw x18, -376(x8)
addr0x000006a0: jal x25, addr0x0000035c
addr0x000006a4: addi x22, x7, 1674
addr0x000006a8: jal x5, addr0x00000124
addr0x000006ac: sw x6, -460(x8)
addr0x000006b0: sw x0, -1686(x8)
addr0x000006b4: jal x0, addr0x000006e8
addr0x000006b8: jal x30, addr0x000001f4
addr0x000006bc: lw x25, 1093(x8)
addr0x000006c0: addi x9, x18, -1229
addr0x000006c4: addi x12, x2, -221
addr0x000006c8: sw x13, -1260(x8)
addr0x000006cc: lui x2, 354106
addr0x000006d0: addi x26, x23, 906
addr0x000006d4: lui x16, 949296
addr0x000006d8: sw x4, -1326(x8)
addr0x000006dc: addi x21, x22, 986
addr0x000006e0: jal x12, addr0x00000198
addr0x000006e4: lw x9, 1468(x8)
addr0x000006e8: lw x6, -990(x8)
addr0x000006ec: lw x17, -1095(x8)
addr0x000006f0: lw x16, 266(x8)
addr0x000006f4: lui x0, 1016161
addr0x000006f8: addi x21, x4, -682
addr0x000006fc: sw x1, -1670(x8)
addr0x00000700: sw x6, -1872(x8)
addr0x00000704: addi x22, x1, 579
addr0x00000708: sw x31, 1074(x8)
addr0x0000070c: sw x27, -1790(x8)
addr0x00000710: jal x14, addr0x0000031c
addr0x00000714: addi x20, x24, 457
addr0x00000718: ori x27, x25, 1273
addr0x0000071c: addi x28, x27, -539
addr0x00000720: lw x22, -1432(x8)
addr0x00000724: slli x19, x5, 9
addr0x00000728: andi x25, x9, 1924
addr0x0000072c: addi x13, x19, 751
addr0x00000730: lw x17, 1427(x8)
addr0x00000734: add x25, x18, x22
addr0x00000738: sw x11, 1494(x8)
addr0x0000073c: sw x9, -1347(x8)
addr0x00000740: sw x7, -1537(x8)
addr0x00000744: lw x16, -147(x8)
addr0x00000748: sw x17, -1067(x8)
addr0x0000074c: sw x0, -720(x8)
addr0x00000750: addi x9, x23, -107
addr0x00000754: bltu x23, x7, addr0x00000504
addr0x00000758: lw x24, -1696(x8)
addr0x0000075c: addi x0, x29, -1841
addr0x00000760: lw x0, -873(x8)
addr0x00000764: addi x31, x23, -1638
addr0x00000768: sw x0, 454(x8)
addr0x0000076c: sw x30, -520(x8)
addr0x00000770: sw x28, -1907(x8)
addr0x00000774: sw x1, 1859(x8)
addr0x00000778: jal x4, addr0x000002f8
addr0x0000077c: lbu x26, 1814(x8)
addr0x00000780: lw x5, -347(x8)
addr0x00000784: addi x9, x19, 1720
addr0x00000788: bge x27, x17, addr0x000005b4
addr0x0000078c: jal x20, addr0x000006f0
addr0x00000790: jal x27, addr0x0000076c
addr0x00000794: lbu x23, 546(x8)
addr0x00000798: sw x17, -603(x8)
addr0x0000079c: addi x31, x1, -307
addr0x000007a0: jal x10, addr0x00000344
addr0x000007a4: jal x23, addr0x000000c8
addr0x000007a8: add x31, x17, x29
addr0x000007ac: bltu x20, x20, addr0x000004b8
