.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lbu x2, 828(x8)
addr0x0000001c: bne x18, x25, addr0x0000016c
addr0x00000020: beq x9, x23, addr0x00000194
addr0x00000024: lw x19, -980(x8)
addr0x00000028: addi x7, x19, -1335
addr0x0000002c: slli x23, x30, 30
addr0x00000030: lw x7, -666(x8)
addr0x00000034: addi x19, x12, 906
addr0x00000038: addi x16, x22, 1570
addr0x0000003c: beq x4, x29, addr0x000002bc
addr0x00000040: bltu x6, x31, addr0x000001ac
addr0x00000044: lw x7, 1583(x8)
addr0x00000048: addi x7, x30, -1379
addr0x0000004c: lw x4, 249(x8)
addr0x00000050: addi x7, x27, 1382
addr0x00000054: lw x29, -1771(x8)
addr0x00000058: lw x29, -1791(x8)
addr0x0000005c: addi x29, x4, -700
addr0x00000060: bne x19, x12, addr0x00000020
addr0x00000064: lw x4, -1282(x8)
addr0x00000068: addi x27, x4, -1214
addr0x0000006c: sw x20, -1546(x8)
addr0x00000070: sw x6, 449(x8)
addr0x00000074: jal x29, addr0x00000088
addr0x00000078: lw x29, 807(x8)
addr0x0000007c: lw x29, -1135(x8)
addr0x00000080: jalr x0, 564(x3)
addr0x00000084: addi x19, x18, 1985
addr0x00000088: jal x27, addr0x000000b4
addr0x0000008c: addi x9, x22, -1929
addr0x00000090: sw x7, -1461(x8)
addr0x00000094: lui x6, 10731
addr0x00000098: lbu x30, -633(x8)
addr0x0000009c: bgeu x20, x30, addr0x000000cc
addr0x000000a0: sw x0, 162(x8)
addr0x000000a4: lw x6, -775(x8)
addr0x000000a8: ori x1, x29, 1553
addr0x000000ac: jal x29, addr0x00000164
addr0x000000b0: lui x22, 89206
addr0x000000b4: addi x9, x1, -1943
addr0x000000b8: lw x4, -1638(x8)
addr0x000000bc: lw x15, -570(x8)
addr0x000000c0: lw x22, 171(x8)
addr0x000000c4: lw x17, 1454(x8)
addr0x000000c8: lw x22, -1318(x8)
addr0x000000cc: sw x5, 217(x8)
addr0x000000d0: lw x0, -26(x8)
addr0x000000d4: lw x23, -825(x8)
addr0x000000d8: lw x20, 710(x8)
addr0x000000dc: add x7, x19, x18
addr0x000000e0: add x23, x7, x29
addr0x000000e4: sw x30, 112(x8)
addr0x000000e8: jal x29, addr0x00000294
addr0x000000ec: jal x22, addr0x000000a0
addr0x000000f0: lhu x30, 1748(x8)
addr0x000000f4: andi x29, x23, -1176
addr0x000000f8: beq x27, x7, addr0x0000023c
addr0x000000fc: jal x11, addr0x0000029c
addr0x00000100: addi x21, x18, 949
addr0x00000104: jal x18, addr0x000000a0
addr0x00000108: lw x5, -364(x8)
addr0x0000010c: sltu x18, x0, x19
addr0x00000110: add x22, x11, x21
addr0x00000114: lw x0, 1538(x8)
addr0x00000118: addi x29, x27, -670
addr0x0000011c: jal x7, addr0x00000190
addr0x00000120: lw x11, 607(x8)
addr0x00000124: addi x12, x19, -1493
addr0x00000128: lw x22, -1715(x8)
addr0x0000012c: lw x23, 398(x8)
addr0x00000130: lw x7, 2014(x8)
addr0x00000134: bltu x18, x21, addr0x000000ec
addr0x00000138: bge x19, x17, addr0x00000034
addr0x0000013c: slli x22, x0, 15
addr0x00000140: srli x31, x10, 22
addr0x00000144: ori x7, x23, 1399
addr0x00000148: lw x5, -1867(x8)
addr0x0000014c: jal x24, addr0x000000f4
addr0x00000150: sw x6, -134(x8)
addr0x00000154: sw x15, 1485(x8)
addr0x00000158: sw x17, -483(x8)
addr0x0000015c: addi x22, x18, -1235
addr0x00000160: sw x4, 185(x8)
addr0x00000164: jal x18, addr0x00000108
addr0x00000168: jal x24, addr0x00000030
addr0x0000016c: addi x29, x4, 1608
addr0x00000170: addi x23, x7, -52
addr0x00000174: sh x22, -1622(x8)
addr0x00000178: addi x22, x16, 845
addr0x0000017c: sw x5, -1149(x8)
addr0x00000180: sw x23, 1390(x8)
addr0x00000184: sw x4, 1304(x8)
addr0x00000188: lw x30, 1877(x8)
addr0x0000018c: lw x19, -485(x8)
addr0x00000190: slli x23, x22, 24
addr0x00000194: sw x15, 123(x8)
addr0x00000198: jal x24, addr0x00000120
addr0x0000019c: jal x30, addr0x00000138
addr0x000001a0: sw x6, -1298(x8)
addr0x000001a4: sw x11, -1471(x8)
addr0x000001a8: lbu x23, -310(x8)
addr0x000001ac: blt x22, x0, addr0x00000280
addr0x000001b0: beq x18, x30, addr0x0000002c
addr0x000001b4: lui x11, 356517
addr0x000001b8: addi x4, x19, 1724
addr0x000001bc: lw x9, -751(x8)
addr0x000001c0: lw x1, 976(x8)
addr0x000001c4: andi x22, x11, 1735
addr0x000001c8: bge x20, x7, addr0x00000060
addr0x000001cc: addi x9, x0, 1339
addr0x000001d0: addi x19, x25, 1634
addr0x000001d4: jal x9, addr0x00000190
addr0x000001d8: lw x31, 359(x8)
addr0x000001dc: jal x22, addr0x00000290
addr0x000001e0: lbu x25, -252(x8)
addr0x000001e4: lw x20, 583(x8)
addr0x000001e8: addi x22, x5, 518
addr0x000001ec: addi x23, x29, -1276
addr0x000001f0: blt x31, x31, addr0x00000114
addr0x000001f4: slli x15, x21, 8
addr0x000001f8: srli x20, x4, 14
addr0x000001fc: addi x23, x25, -1521
addr0x00000200: jal x29, addr0x000002bc
addr0x00000204: andi x15, x2, -1970
addr0x00000208: sub x5, x29, x10
addr0x0000020c: jal x19, addr0x00000178
addr0x00000210: sw x19, -111(x8)
addr0x00000214: sw x23, -1956(x8)
addr0x00000218: sw x21, 453(x8)
addr0x0000021c: sw x29, -964(x8)
addr0x00000220: beq x11, x11, addr0x000001e0
addr0x00000224: lw x29, 1106(x8)
addr0x00000228: jal x21, addr0x0000001c
addr0x0000022c: jal x10, addr0x000001dc
addr0x00000230: ori x18, x9, 707
addr0x00000234: sw x0, 1804(x8)
addr0x00000238: addi x4, x9, 33
addr0x0000023c: sw x6, -588(x8)
addr0x00000240: lbu x7, -1258(x8)
addr0x00000244: addi x0, x31, -397
addr0x00000248: addi x29, x21, 103
addr0x0000024c: bne x23, x18, addr0x0000014c
addr0x00000250: jal x23, addr0x000002d0
addr0x00000254: jal x15, addr0x000000e0
addr0x00000258: lw x23, -1717(x8)
addr0x0000025c: add x4, x7, x7
addr0x00000260: sw x20, 1320(x8)
addr0x00000264: addi x30, x31, 956
addr0x00000268: lui x9, 539515
addr0x0000026c: addi x23, x10, -1321
addr0x00000270: lw x18, -692(x8)
addr0x00000274: sw x9, -1443(x8)
addr0x00000278: addi x29, x15, -46
addr0x0000027c: bne x29, x15, addr0x000001c8
addr0x00000280: sh x15, 1373(x8)
addr0x00000284: lw x19, -893(x8)
addr0x00000288: sw x4, -748(x8)
addr0x0000028c: addi x23, x4, 799
addr0x00000290: sw x23, -1672(x8)
addr0x00000294: sw x4, 1052(x8)
addr0x00000298: addi x22, x19, -1347
addr0x0000029c: jal x29, addr0x00000174
addr0x000002a0: addi x20, x0, -1137
addr0x000002a4: jal x21, addr0x000002a4
addr0x000002a8: addi x0, x30, -830
addr0x000002ac: jal x19, addr0x00000110
addr0x000002b0: lw x30, -1611(x8)
addr0x000002b4: lw x20, -1704(x8)
addr0x000002b8: lw x29, 155(x8)
addr0x000002bc: jal x19, addr0x00000184
addr0x000002c0: addi x2, x30, -1213
addr0x000002c4: add x29, x9, x4
addr0x000002c8: addi x29, x24, 767
addr0x000002cc: sw x0, -1626(x8)
addr0x000002d0: sw x19, -112(x8)
addr0x000002d4: addi x20, x0, 528
addr0x000002d8: jal x31, addr0x00000198
addr0x000002dc: lui x22, 912275
addr0x000002e0: addi x11, x21, 129
addr0x000002e4: and x2, x24, x22
