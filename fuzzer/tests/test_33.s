.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x7, -1695(x8)
addr0x0000001c: addi x23, x6, -721
addr0x00000020: sh x13, 639(x8)
addr0x00000024: lbu x10, -68(x8)
addr0x00000028: jal x9, addr0x000000b8
addr0x0000002c: lhu x23, -19(x8)
addr0x00000030: andi x25, x29, -582
addr0x00000034: slli x0, x0, 26
addr0x00000038: addi x16, x12, -613
addr0x0000003c: jal x23, addr0x00000138
addr0x00000040: lw x15, -1952(x8)
addr0x00000044: sb x7, 1657(x8)
addr0x00000048: lw x7, 533(x8)
addr0x0000004c: bge x19, x7, addr0x0000017c
addr0x00000050: lw x20, 356(x8)
addr0x00000054: lw x20, -1864(x8)
addr0x00000058: lw x17, 118(x8)
addr0x0000005c: sw x0, -223(x8)
addr0x00000060: addi x29, x20, -9
addr0x00000064: lui x16, 1963
addr0x00000068: lw x7, 802(x8)
addr0x0000006c: jal x19, addr0x000000d4
addr0x00000070: jal x27, addr0x00000094
addr0x00000074: lw x28, -976(x8)
addr0x00000078: sw x0, 146(x8)
addr0x0000007c: sw x27, 1219(x8)
addr0x00000080: slli x10, x19, 31
addr0x00000084: and x19, x17, x11
addr0x00000088: srli x29, x21, 12
addr0x0000008c: andi x11, x1, 95
addr0x00000090: lw x17, 819(x8)
addr0x00000094: beq x12, x30, addr0x00000108
addr0x00000098: lw x24, 753(x8)
addr0x0000009c: lw x27, -1229(x8)
addr0x000000a0: add x21, x23, x29
addr0x000000a4: sw x23, -2020(x8)
addr0x000000a8: jal x5, addr0x00000274
addr0x000000ac: lw x2, 209(x8)
addr0x000000b0: lbu x20, -1176(x8)
addr0x000000b4: andi x15, x5, -1193
addr0x000000b8: addi x20, x1, -1882
addr0x000000bc: sw x23, -587(x8)
addr0x000000c0: sw x29, 863(x8)
addr0x000000c4: sw x7, 81(x8)
addr0x000000c8: addi x21, x20, 603
addr0x000000cc: sw x7, 939(x8)
addr0x000000d0: lw x11, 71(x8)
addr0x000000d4: jal x2, addr0x0000011c
addr0x000000d8: sw x18, 1191(x8)
addr0x000000dc: sw x24, -1441(x8)
addr0x000000e0: jal x27, addr0x00000238
addr0x000000e4: lw x25, 1688(x8)
addr0x000000e8: bgeu x10, x17, addr0x00000230
addr0x000000ec: lw x31, -901(x8)
addr0x000000f0: sw x28, 1848(x8)
addr0x000000f4: lw x0, 2012(x8)
addr0x000000f8: jal x9, addr0x00000038
addr0x000000fc: lw x22, 1270(x8)
addr0x00000100: addi x7, x28, -1362
addr0x00000104: addi x29, x19, -1240
addr0x00000108: sw x31, -210(x8)
addr0x0000010c: addi x6, x14, 327
addr0x00000110: sw x25, -850(x8)
addr0x00000114: jal x25, addr0x00000288
addr0x00000118: sw x13, 1944(x8)
addr0x0000011c: addi x1, x22, 1979
addr0x00000120: sw x24, 1564(x8)
addr0x00000124: addi x24, x29, -1865
addr0x00000128: addi x9, x7, -170
addr0x0000012c: lw x5, 885(x8)
addr0x00000130: lw x10, 306(x8)
addr0x00000134: lw x2, -1180(x8)
addr0x00000138: addi x23, x18, 854
addr0x0000013c: jal x4, addr0x000002a0
addr0x00000140: lw x20, -1343(x8)
addr0x00000144: beq x20, x0, addr0x0000016c
addr0x00000148: jal x18, addr0x00000048
addr0x0000014c: sb x2, 530(x8)
addr0x00000150: jal x19, addr0x00000020
addr0x00000154: lw x17, 218(x8)
addr0x00000158: jal x5, addr0x0000011c
addr0x0000015c: jal x31, addr0x000000c8
addr0x00000160: jal x18, addr0x000000b0
addr0x00000164: sw x19, 1544(x8)
addr0x00000168: sw x19, 124(x8)
addr0x0000016c: lw x0, 554(x8)
addr0x00000170: jal x15, addr0x00000228
addr0x00000174: lw x1, -1943(x8)
addr0x00000178: lw x18, 540(x8)
addr0x0000017c: addi x13, x5, -932
addr0x00000180: jal x0, addr0x000001f8
addr0x00000184: sw x18, -1098(x8)
addr0x00000188: jal x29, addr0x00000038
addr0x0000018c: lui x7, 1013554
addr0x00000190: addi x7, x22, -163
addr0x00000194: sw x20, -1979(x8)
addr0x00000198: mul x30, x1, x28
addr0x0000019c: lw x19, 436(x8)
addr0x000001a0: sw x23, -1277(x8)
addr0x000001a4: sw x20, 907(x8)
addr0x000001a8: add x27, x22, x17
addr0x000001ac: jal x2, addr0x00000128
addr0x000001b0: lw x28, -750(x8)
addr0x000001b4: lw x9, 1884(x8)
addr0x000001b8: addi x21, x2, -937
addr0x000001bc: jal x10, addr0x00000224
addr0x000001c0: lw x28, -716(x8)
addr0x000001c4: lw x19, -123(x8)
addr0x000001c8: sw x20, -324(x8)
addr0x000001cc: ori x23, x20, -1719
addr0x000001d0: sw x7, -1346(x8)
addr0x000001d4: jal x12, addr0x000000d4
addr0x000001d8: lw x14, 547(x8)
addr0x000001dc: sw x2, -1368(x8)
addr0x000001e0: lw x25, 1934(x8)
addr0x000001e4: lw x27, 940(x8)
addr0x000001e8: lw x28, 1057(x8)
addr0x000001ec: lw x19, 1783(x8)
addr0x000001f0: lw x27, -1796(x8)
addr0x000001f4: lw x30, -85(x8)
addr0x000001f8: lw x22, -1479(x8)
addr0x000001fc: sw x19, -408(x8)
addr0x00000200: sb x29, 1382(x8)
addr0x00000204: addi x7, x18, 1839
addr0x00000208: slli x0, x25, 8
addr0x0000020c: addi x22, x20, -327
addr0x00000210: slti x30, x29, 1285
addr0x00000214: addi x19, x13, 1933
addr0x00000218: sw x31, -169(x8)
addr0x0000021c: blt x27, x1, addr0x0000013c
addr0x00000220: lw x9, 399(x8)
addr0x00000224: add x19, x30, x4
addr0x00000228: andi x16, x31, 1783
addr0x0000022c: sw x10, -1160(x8)
addr0x00000230: addi x18, x27, 222
addr0x00000234: lw x9, 1127(x8)
addr0x00000238: srai x10, x1, 29
addr0x0000023c: addi x5, x7, -1401
addr0x00000240: addi x17, x20, -1402
addr0x00000244: sw x29, 711(x8)
addr0x00000248: addi x19, x10, -11
addr0x0000024c: lw x20, -1440(x8)
addr0x00000250: sw x0, -124(x8)
addr0x00000254: addi x21, x10, 652
addr0x00000258: sw x24, 1614(x8)
addr0x0000025c: ori x31, x20, -1826
addr0x00000260: lw x30, -805(x8)
addr0x00000264: jal x7, addr0x000001cc
addr0x00000268: lw x27, -874(x8)
addr0x0000026c: lui x31, 209791
addr0x00000270: lui x23, 389536
addr0x00000274: lui x12, 949898
addr0x00000278: lui x17, 849556
addr0x0000027c: sw x19, 1814(x8)
addr0x00000280: sw x25, -1576(x8)
addr0x00000284: lw x1, -1274(x8)
addr0x00000288: lw x19, 1172(x8)
addr0x0000028c: addi x9, x9, 893
addr0x00000290: addi x29, x22, -634
addr0x00000294: sw x21, -196(x8)
addr0x00000298: lw x26, 1473(x8)
addr0x0000029c: addi x0, x6, -1628
addr0x000002a0: lw x22, -1657(x8)
addr0x000002a4: bge x25, x27, addr0x00000210
