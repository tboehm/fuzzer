.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sw x29, 1361(x8)
addr0x0000001c: addi x29, x29, -1198
addr0x00000020: addi x18, x29, 1440
addr0x00000024: sw x29, -1148(x8)
addr0x00000028: jal x18, addr0x000000b8
addr0x0000002c: lw x19, 1603(x8)
addr0x00000030: lw x18, 1728(x8)
addr0x00000034: slli x19, x19, 6
addr0x00000038: srli x18, x29, 27
addr0x0000003c: andi x18, x19, 439
addr0x00000040: lbu x19, 248(x8)
addr0x00000044: ori x19, x19, 1
addr0x00000048: slli x25, x29, 4
addr0x0000004c: lw x19, -103(x8)
addr0x00000050: sw x29, -981(x8)
addr0x00000054: jal x18, addr0x00000214
addr0x00000058: sw x25, 1064(x8)
addr0x0000005c: lw x29, 1404(x8)
addr0x00000060: lw x19, -1824(x8)
addr0x00000064: sw x19, -1555(x8)
addr0x00000068: sw x7, 1823(x8)
addr0x0000006c: sw x5, 185(x8)
addr0x00000070: addi x5, x7, 1684
addr0x00000074: andi x7, x25, 842
addr0x00000078: addi x19, x5, -1085
addr0x0000007c: jal x7, addr0x00000098
addr0x00000080: addi x25, x6, -1243
addr0x00000084: addi x5, x6, -43
addr0x00000088: jal x19, addr0x000000d0
addr0x0000008c: sw x25, -1542(x8)
addr0x00000090: bge x29, x29, addr0x000000c8
addr0x00000094: sw x18, 1683(x8)
addr0x00000098: addi x18, x19, -2033
addr0x0000009c: addi x6, x18, -1463
addr0x000000a0: sw x18, 76(x8)
addr0x000000a4: jal x29, addr0x00000168
addr0x000000a8: sw x5, 811(x8)
addr0x000000ac: lui x19, 683686
addr0x000000b0: addi x7, x29, 1950
addr0x000000b4: addi x19, x25, 313
addr0x000000b8: sw x29, 1450(x8)
addr0x000000bc: sw x5, 226(x8)
addr0x000000c0: sw x16, 1713(x8)
addr0x000000c4: addi x16, x7, -477
addr0x000000c8: bne x0, x29, addr0x0000020c
addr0x000000cc: jal x6, addr0x0000003c
addr0x000000d0: addi x25, x1, -247
addr0x000000d4: jal x6, addr0x000000fc
addr0x000000d8: sub x9, x16, x19
addr0x000000dc: addi x0, x19, -233
addr0x000000e0: bgeu x19, x16, addr0x0000018c
addr0x000000e4: lbu x6, 1904(x8)
addr0x000000e8: bne x1, x4, addr0x000000ec
addr0x000000ec: sb x19, -358(x8)
addr0x000000f0: lw x1, 639(x8)
addr0x000000f4: sw x6, 1175(x8)
addr0x000000f8: lbu x25, -1521(x8)
addr0x000000fc: slli x29, x5, 3
addr0x00000100: srli x9, x16, 30
addr0x00000104: add x25, x5, x0
addr0x00000108: lw x9, 1475(x8)
addr0x0000010c: addi x19, x7, -1866
addr0x00000110: sw x4, -796(x8)
addr0x00000114: jal x19, addr0x0000013c
addr0x00000118: jal x25, addr0x00000208
addr0x0000011c: sw x19, 2023(x8)
addr0x00000120: jal x25, addr0x00000090
addr0x00000124: lw x16, -670(x8)
addr0x00000128: lw x25, -1121(x8)
addr0x0000012c: lw x29, 1794(x8)
addr0x00000130: lw x29, 1052(x8)
addr0x00000134: lw x18, -834(x8)
addr0x00000138: addi x23, x6, 1706
addr0x0000013c: bne x1, x7, addr0x000001d4
addr0x00000140: lw x0, 1747(x8)
addr0x00000144: sw x16, -879(x8)
addr0x00000148: addi x29, x25, 1670
addr0x0000014c: addi x4, x0, 879
addr0x00000150: or x9, x25, x5
addr0x00000154: lw x23, -1295(x8)
addr0x00000158: addi x6, x1, -277
addr0x0000015c: sw x5, 890(x8)
addr0x00000160: add x5, x0, x16
addr0x00000164: jal x5, addr0x00000170
addr0x00000168: slli x16, x4, 10
addr0x0000016c: lw x19, -414(x8)
addr0x00000170: jal x5, addr0x000000f0
addr0x00000174: lw x17, 1954(x8)
addr0x00000178: addi x4, x5, -824
addr0x0000017c: lw x16, -802(x8)
addr0x00000180: lw x23, -155(x8)
addr0x00000184: lbu x16, 276(x8)
addr0x00000188: lw x23, 666(x8)
addr0x0000018c: lw x25, 1840(x8)
addr0x00000190: lw x17, -868(x8)
addr0x00000194: jal x25, addr0x000001d0
addr0x00000198: lw x25, 1830(x8)
addr0x0000019c: lw x6, -911(x8)
addr0x000001a0: addi x19, x16, 1666
addr0x000001a4: sw x6, 1692(x8)
addr0x000001a8: jal x19, addr0x00000044
addr0x000001ac: addi x5, x7, 1699
addr0x000001b0: sw x23, -250(x8)
addr0x000001b4: sw x9, -1333(x8)
addr0x000001b8: addi x23, x18, -1580
addr0x000001bc: jal x0, addr0x0000007c
addr0x000001c0: lw x4, 1710(x8)
addr0x000001c4: addi x19, x6, -1029
addr0x000001c8: srai x1, x7, 27
addr0x000001cc: bge x16, x29, addr0x000000a8
addr0x000001d0: addi x29, x4, 1589
addr0x000001d4: jal x9, addr0x0000007c
addr0x000001d8: sw x19, 838(x8)
addr0x000001dc: jal x6, addr0x0000009c
addr0x000001e0: lw x17, 554(x8)
addr0x000001e4: lw x29, -1738(x8)
addr0x000001e8: addi x0, x5, -2038
addr0x000001ec: add x18, x4, x6
addr0x000001f0: lw x22, 1148(x8)
addr0x000001f4: sw x23, 1384(x8)
addr0x000001f8: addi x0, x25, 1770
addr0x000001fc: jal x4, addr0x00000154
addr0x00000200: lw x0, -1825(x8)
addr0x00000204: bltu x1, x5, addr0x00000010
addr0x00000208: andi x6, x23, 1598
addr0x0000020c: bltu x29, x4, addr0x00000010
addr0x00000210: slli x18, x1, 9
addr0x00000214: sll x17, x25, x29
addr0x00000218: srl x29, x19, x29
addr0x0000021c: andi x23, x23, -1763
