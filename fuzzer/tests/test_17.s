.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lhu x16, 694(x8)
addr0x0000001c: lui x15, 365332
addr0x00000020: bne x31, x12, addr0x0000069c
addr0x00000024: sltu x26, x25, x2
addr0x00000028: or x20, x0, x14
addr0x0000002c: addi x20, x27, -474
addr0x00000030: jal x31, addr0x000002e4
addr0x00000034: add x13, x11, x21
addr0x00000038: jal x29, addr0x00000328
addr0x0000003c: addi x7, x19, -1254
addr0x00000040: sw x14, -952(x8)
addr0x00000044: sw x5, 2040(x8)
addr0x00000048: lhu x15, -1083(x8)
addr0x0000004c: slli x5, x24, 26
addr0x00000050: add x20, x1, x6
addr0x00000054: sw x28, -321(x8)
addr0x00000058: sw x19, -339(x8)
addr0x0000005c: sw x24, 1881(x8)
addr0x00000060: sw x31, -789(x8)
addr0x00000064: addi x13, x30, -393
addr0x00000068: jal x21, addr0x000004cc
addr0x0000006c: lui x29, 361474
addr0x00000070: addi x0, x17, 102
addr0x00000074: jal x0, addr0x0000030c
addr0x00000078: sb x26, -1814(x8)
addr0x0000007c: sltu x27, x2, x14
addr0x00000080: sub x21, x6, x18
addr0x00000084: sw x9, 754(x8)
addr0x00000088: jal x23, addr0x000007ec
addr0x0000008c: lw x1, -875(x8)
addr0x00000090: andi x13, x9, -1549
addr0x00000094: addi x10, x16, 1248
addr0x00000098: lui x0, 165808
addr0x0000009c: jal x14, addr0x00000610
addr0x000000a0: lw x0, 1451(x8)
addr0x000000a4: sw x1, -1429(x8)
addr0x000000a8: addi x19, x27, 224
addr0x000000ac: addi x19, x21, 297
addr0x000000b0: sw x24, -1575(x8)
addr0x000000b4: jal x13, addr0x00000034
addr0x000000b8: jal x2, addr0x000005c4
addr0x000000bc: lw x25, -40(x8)
addr0x000000c0: lw x29, -1999(x8)
addr0x000000c4: lw x27, 1802(x8)
addr0x000000c8: lw x16, 532(x8)
addr0x000000cc: jal x30, addr0x0000056c
addr0x000000d0: lw x29, -1796(x8)
addr0x000000d4: bne x20, x21, addr0x000002d4
addr0x000000d8: jal x4, addr0x000006f0
addr0x000000dc: jal x7, addr0x000004a8
addr0x000000e0: lw x22, -74(x8)
addr0x000000e4: lw x19, -362(x8)
addr0x000000e8: addi x30, x13, 710
addr0x000000ec: add x19, x31, x10
addr0x000000f0: lw x31, 1117(x8)
addr0x000000f4: addi x27, x21, -585
addr0x000000f8: sw x1, 1538(x8)
addr0x000000fc: sw x2, 1786(x8)
addr0x00000100: lui x7, 571527
addr0x00000104: addi x31, x0, 604
addr0x00000108: lw x13, 564(x8)
addr0x0000010c: lw x2, 15(x8)
addr0x00000110: lw x17, -1764(x8)
addr0x00000114: lw x13, -1128(x8)
addr0x00000118: jal x14, addr0x000007ac
addr0x0000011c: lw x18, 747(x8)
addr0x00000120: sw x5, 1030(x8)
addr0x00000124: sw x15, 825(x8)
addr0x00000128: lw x15, 1985(x8)
addr0x0000012c: addi x17, x27, 1763
addr0x00000130: sw x29, 1440(x8)
addr0x00000134: sw x30, -1331(x8)
addr0x00000138: sw x27, 1580(x8)
addr0x0000013c: lw x23, -1698(x8)
addr0x00000140: addi x14, x18, -278
addr0x00000144: lbu x13, 1122(x8)
addr0x00000148: andi x17, x23, 83
addr0x0000014c: addi x6, x17, 750
addr0x00000150: sw x29, 441(x8)
addr0x00000154: sw x7, -233(x8)
addr0x00000158: sw x13, 1681(x8)
addr0x0000015c: sw x9, -849(x8)
addr0x00000160: sw x19, -1259(x8)
addr0x00000164: jal x0, addr0x00000724
addr0x00000168: sw x14, 730(x8)
addr0x0000016c: jal x24, addr0x00000654
addr0x00000170: addi x0, x1, -317
addr0x00000174: sw x7, 1342(x8)
addr0x00000178: sw x24, -1247(x8)
addr0x0000017c: sw x9, 850(x8)
addr0x00000180: sw x25, 1216(x8)
addr0x00000184: jal x29, addr0x00000348
addr0x00000188: lw x29, -656(x8)
addr0x0000018c: lw x22, 1106(x8)
addr0x00000190: jal x1, addr0x0000018c
addr0x00000194: lw x0, 60(x8)
addr0x00000198: lw x24, -539(x8)
addr0x0000019c: slli x0, x13, 1
addr0x000001a0: jal x21, addr0x00000130
addr0x000001a4: sw x31, -1103(x8)
addr0x000001a8: jal x0, addr0x00000230
addr0x000001ac: lui x24, 826293
addr0x000001b0: addi x20, x23, 116
addr0x000001b4: sw x1, -500(x8)
addr0x000001b8: addi x13, x13, 1901
addr0x000001bc: sw x24, -192(x8)
addr0x000001c0: jal x16, addr0x00000334
addr0x000001c4: lbu x12, 1967(x8)
addr0x000001c8: add x9, x18, x20
addr0x000001cc: sw x4, 1599(x8)
addr0x000001d0: jal x23, addr0x00000740
addr0x000001d4: addi x27, x31, -489
addr0x000001d8: andi x21, x24, -1809
addr0x000001dc: lw x2, 880(x8)
addr0x000001e0: sw x22, 493(x8)
addr0x000001e4: lui x22, 491476
addr0x000001e8: sw x28, 1662(x8)
addr0x000001ec: lw x7, 275(x8)
addr0x000001f0: addi x23, x17, -1463
addr0x000001f4: addi x4, x5, 329
addr0x000001f8: addi x21, x0, -266
addr0x000001fc: sw x18, 622(x8)
addr0x00000200: jal x1, addr0x00000028
addr0x00000204: sh x19, -1837(x8)
addr0x00000208: bne x13, x25, addr0x000001f0
addr0x0000020c: andi x13, x17, -941
addr0x00000210: sub x28, x29, x24
addr0x00000214: sw x21, -32(x8)
addr0x00000218: sw x18, -488(x8)
addr0x0000021c: sw x25, 704(x8)
addr0x00000220: sw x24, 1445(x8)
addr0x00000224: bge x14, x23, addr0x000000e8
addr0x00000228: lw x31, -394(x8)
addr0x0000022c: jal x9, addr0x00000184
addr0x00000230: lui x4, 840095
addr0x00000234: sw x14, 1222(x8)
addr0x00000238: addi x19, x6, 505
addr0x0000023c: addi x17, x0, -1404
addr0x00000240: sw x1, 1780(x8)
addr0x00000244: lui x29, 945725
addr0x00000248: sw x12, -241(x8)
addr0x0000024c: beq x6, x7, addr0x00000508
addr0x00000250: lui x27, 783243
addr0x00000254: addi x22, x29, -310
addr0x00000258: sw x29, 1873(x8)
addr0x0000025c: auipc x10, 59527
addr0x00000260: addi x0, x25, -1712
addr0x00000264: addi x6, x31, -189
addr0x00000268: jal x0, addr0x0000012c
addr0x0000026c: sw x9, 1929(x8)
addr0x00000270: lui x11, 125838
addr0x00000274: and x15, x0, x22
addr0x00000278: sw x4, -865(x8)
addr0x0000027c: lui x2, 433046
addr0x00000280: and x14, x29, x12
addr0x00000284: slli x29, x13, 18
addr0x00000288: add x29, x15, x29
addr0x0000028c: jal x29, addr0x000007ec
addr0x00000290: lw x23, -233(x8)
addr0x00000294: sw x20, 792(x8)
addr0x00000298: jal x27, addr0x0000044c
addr0x0000029c: lw x19, 847(x8)
addr0x000002a0: lbu x19, -603(x8)
addr0x000002a4: addi x19, x23, 913
addr0x000002a8: jal x14, addr0x000000c8
addr0x000002ac: lw x16, -1423(x8)
addr0x000002b0: addi x19, x29, 1737
addr0x000002b4: jal x14, addr0x000004ac
addr0x000002b8: jal x25, addr0x00000338
addr0x000002bc: jal x30, addr0x000002c0
addr0x000002c0: addi x29, x7, -1205
addr0x000002c4: jal x13, addr0x0000070c
addr0x000002c8: lw x28, 352(x8)
addr0x000002cc: addi x22, x27, 1352
addr0x000002d0: jal x13, addr0x00000518
addr0x000002d4: addi x25, x24, 2006
addr0x000002d8: sw x7, 1111(x8)
addr0x000002dc: lw x15, 1889(x8)
addr0x000002e0: jal x21, addr0x00000614
addr0x000002e4: addi x19, x18, -874
addr0x000002e8: addi x24, x0, -191
addr0x000002ec: jal x15, addr0x0000035c
addr0x000002f0: lw x23, 161(x8)
addr0x000002f4: lw x14, 987(x8)
addr0x000002f8: lw x31, -1410(x8)
addr0x000002fc: jal x15, addr0x00000754
addr0x00000300: addi x13, x0, 1320
addr0x00000304: sw x19, 929(x8)
addr0x00000308: lbu x13, -348(x8)
addr0x0000030c: xori x15, x29, 1910
addr0x00000310: andi x18, x16, -423
addr0x00000314: sw x10, -1429(x8)
addr0x00000318: sw x16, -1926(x8)
addr0x0000031c: sw x23, -379(x8)
addr0x00000320: sw x19, 1758(x8)
addr0x00000324: addi x28, x24, 391
addr0x00000328: sw x22, 672(x8)
addr0x0000032c: addi x18, x13, -1530
addr0x00000330: sw x31, 466(x8)
addr0x00000334: sw x27, -1089(x8)
addr0x00000338: lui x23, 725988
addr0x0000033c: lui x20, 102925
addr0x00000340: addi x31, x30, -1308
addr0x00000344: sw x25, 1099(x8)
addr0x00000348: lw x23, -310(x8)
addr0x0000034c: lw x25, 514(x8)
addr0x00000350: sw x27, -1137(x8)
addr0x00000354: sw x7, -1706(x8)
addr0x00000358: jal x16, addr0x00000228
addr0x0000035c: addi x14, x2, 1687
addr0x00000360: addi x18, x22, 1327
addr0x00000364: lw x17, -1202(x8)
addr0x00000368: lw x14, 287(x8)
addr0x0000036c: slli x7, x22, 21
addr0x00000370: addi x19, x29, 899
addr0x00000374: lbu x4, -1622(x8)
addr0x00000378: bne x24, x18, addr0x000007f4
addr0x0000037c: lw x9, 1996(x8)
addr0x00000380: lw x15, 1422(x8)
addr0x00000384: and x13, x17, x14
addr0x00000388: lui x24, 342005
addr0x0000038c: add x22, x9, x19
addr0x00000390: sw x0, -1674(x8)
addr0x00000394: sw x14, 605(x8)
addr0x00000398: sw x22, -308(x8)
addr0x0000039c: andi x9, x24, 1955
addr0x000003a0: jal x28, addr0x00000124
addr0x000003a4: lw x30, 728(x8)
addr0x000003a8: addi x18, x13, -1904
addr0x000003ac: lbu x13, 1627(x8)
addr0x000003b0: bne x21, x7, addr0x000001cc
addr0x000003b4: sw x14, 1609(x8)
addr0x000003b8: sw x16, -863(x8)
addr0x000003bc: sb x1, -1197(x8)
addr0x000003c0: jal x29, addr0x0000059c
addr0x000003c4: jal x0, addr0x000000d8
addr0x000003c8: sw x4, -1831(x8)
addr0x000003cc: bne x12, x23, addr0x00000234
addr0x000003d0: lbu x21, 153(x8)
addr0x000003d4: lw x14, -1534(x8)
addr0x000003d8: lw x19, -1536(x8)
addr0x000003dc: add x14, x13, x2
addr0x000003e0: lw x13, 67(x8)
addr0x000003e4: slli x13, x4, 1
addr0x000003e8: add x21, x0, x23
addr0x000003ec: jal x9, addr0x000000d8
addr0x000003f0: lui x10, 173650
addr0x000003f4: jal x18, addr0x000005b4
addr0x000003f8: slli x9, x23, 23
addr0x000003fc: add x19, x31, x23
addr0x00000400: add x4, x14, x20
addr0x00000404: jal x21, addr0x000005a8
addr0x00000408: lw x24, -1126(x8)
addr0x0000040c: addi x19, x22, 1999
addr0x00000410: sw x31, 1407(x8)
addr0x00000414: jal x30, addr0x0000045c
addr0x00000418: sw x2, 1611(x8)
addr0x0000041c: lui x0, 67395
addr0x00000420: addi x9, x31, -1069
addr0x00000424: jal x26, addr0x00000620
addr0x00000428: lui x6, 295902
addr0x0000042c: addi x9, x31, -143
addr0x00000430: jal x23, addr0x00000514
addr0x00000434: lui x26, 1029589
addr0x00000438: addi x5, x13, -553
addr0x0000043c: lw x2, 1723(x8)
addr0x00000440: sw x16, -1826(x8)
addr0x00000444: sw x16, 1588(x8)
addr0x00000448: lw x17, 1898(x8)
addr0x0000044c: lw x5, 341(x8)
addr0x00000450: addi x14, x11, -526
addr0x00000454: jal x19, addr0x0000065c
addr0x00000458: sw x0, -1402(x8)
addr0x0000045c: sw x7, -978(x8)
addr0x00000460: beq x16, x9, addr0x00000738
addr0x00000464: lbu x17, 1510(x8)
addr0x00000468: bne x24, x24, addr0x00000330
addr0x0000046c: addi x23, x15, -442
addr0x00000470: addi x11, x2, 627
addr0x00000474: lw x20, 684(x8)
addr0x00000478: addi x22, x23, -827
addr0x0000047c: jal x30, addr0x000007d0
addr0x00000480: sw x17, -1365(x8)
addr0x00000484: sw x7, 1926(x8)
addr0x00000488: addi x0, x29, -816
addr0x0000048c: addi x4, x6, 1279
addr0x00000490: sw x9, 1245(x8)
addr0x00000494: sw x7, 761(x8)
addr0x00000498: sw x10, 709(x8)
addr0x0000049c: lw x7, -903(x8)
addr0x000004a0: jal x20, addr0x00000250
addr0x000004a4: addi x20, x4, 84
addr0x000004a8: sw x14, 1505(x8)
addr0x000004ac: jal x0, addr0x00000384
addr0x000004b0: lhu x10, 409(x8)
addr0x000004b4: andi x23, x19, 531
addr0x000004b8: lw x13, -224(x8)
addr0x000004bc: lw x0, -1094(x8)
addr0x000004c0: lw x21, -1389(x8)
addr0x000004c4: lw x0, 1556(x8)
addr0x000004c8: lw x6, 1908(x8)
addr0x000004cc: jal x18, addr0x00000748
addr0x000004d0: addi x4, x13, 1198
addr0x000004d4: sw x14, 950(x8)
addr0x000004d8: addi x25, x7, 547
addr0x000004dc: addi x12, x29, -851
addr0x000004e0: sw x1, -1946(x8)
addr0x000004e4: jal x24, addr0x00000274
addr0x000004e8: lw x18, 213(x8)
addr0x000004ec: lw x28, 1122(x8)
addr0x000004f0: jal x5, addr0x00000694
addr0x000004f4: lw x29, 11(x8)
addr0x000004f8: sw x27, -448(x8)
addr0x000004fc: sw x6, -1400(x8)
addr0x00000500: sltu x19, x22, x20
addr0x00000504: andi x24, x14, 509
addr0x00000508: beq x19, x24, addr0x000002e4
addr0x0000050c: lbu x22, -1698(x8)
addr0x00000510: sw x5, 1116(x8)
addr0x00000514: sw x17, 1605(x8)
addr0x00000518: jal x2, addr0x0000064c
addr0x0000051c: beq x12, x7, addr0x000006ec
addr0x00000520: bltu x12, x30, addr0x000006d0
addr0x00000524: jal x4, addr0x00000254
addr0x00000528: lw x18, -643(x8)
addr0x0000052c: lw x17, 33(x8)
addr0x00000530: sb x29, -500(x8)
addr0x00000534: lw x20, 31(x8)
addr0x00000538: addi x10, x10, 2031
addr0x0000053c: add x20, x13, x25
addr0x00000540: lw x5, 1898(x8)
addr0x00000544: lw x4, -1978(x8)
addr0x00000548: lui x13, 1004380
addr0x0000054c: lw x29, -4(x8)
addr0x00000550: sw x4, 2010(x8)
addr0x00000554: xori x9, x17, -1364
addr0x00000558: jal x30, addr0x00000664
addr0x0000055c: lw x13, -1544(x8)
addr0x00000560: lw x16, 181(x8)
addr0x00000564: lw x31, -2020(x8)
addr0x00000568: lw x13, 312(x8)
addr0x0000056c: sw x14, 1042(x8)
addr0x00000570: lw x20, -480(x8)
addr0x00000574: addi x23, x16, 1236
addr0x00000578: lw x7, 521(x8)
addr0x0000057c: bne x24, x5, addr0x000007a4
addr0x00000580: lw x18, 241(x8)
addr0x00000584: lw x14, 1476(x8)
addr0x00000588: addi x31, x21, 13
addr0x0000058c: lw x14, 1537(x8)
addr0x00000590: lw x29, 30(x8)
addr0x00000594: lw x2, 1450(x8)
addr0x00000598: add x2, x31, x19
addr0x0000059c: addi x5, x16, -1471
addr0x000005a0: sw x30, -1672(x8)
addr0x000005a4: jal x30, addr0x000007c8
addr0x000005a8: sw x20, 468(x8)
addr0x000005ac: add x14, x13, x9
addr0x000005b0: slli x4, x15, 7
addr0x000005b4: and x9, x22, x2
addr0x000005b8: jal x30, addr0x0000059c
addr0x000005bc: sw x23, 1144(x8)
addr0x000005c0: jal x22, addr0x00000148
addr0x000005c4: lw x29, -440(x8)
addr0x000005c8: addi x28, x10, 164
addr0x000005cc: slli x24, x10, 25
addr0x000005d0: lw x29, 326(x8)
addr0x000005d4: lw x22, 608(x8)
addr0x000005d8: lw x23, 1449(x8)
addr0x000005dc: bltu x10, x0, addr0x00000800
addr0x000005e0: srli x11, x9, 30
addr0x000005e4: addi x0, x6, 1117
addr0x000005e8: jal x30, addr0x00000590
addr0x000005ec: lw x17, 1677(x8)
addr0x000005f0: jal x16, addr0x000004ec
addr0x000005f4: lw x13, 1363(x8)
addr0x000005f8: lw x17, -482(x8)
addr0x000005fc: lw x27, 1402(x8)
addr0x00000600: lw x29, 537(x8)
addr0x00000604: jal x24, addr0x000005fc
addr0x00000608: addi x23, x29, -294
addr0x0000060c: add x23, x29, x29
addr0x00000610: lhu x5, -856(x8)
addr0x00000614: sw x0, -627(x8)
addr0x00000618: jal x17, addr0x000005d8
addr0x0000061c: addi x14, x29, 73
addr0x00000620: slli x20, x30, 18
addr0x00000624: add x1, x28, x22
addr0x00000628: addi x7, x19, -230
addr0x0000062c: jal x11, addr0x000006d8
addr0x00000630: jal x15, addr0x000004a0
addr0x00000634: lw x2, -1033(x8)
addr0x00000638: addi x18, x5, 1733
addr0x0000063c: sw x0, -167(x8)
addr0x00000640: bne x9, x5, addr0x0000077c
addr0x00000644: lw x13, 538(x8)
addr0x00000648: lw x30, -1077(x8)
addr0x0000064c: lw x17, -1339(x8)
addr0x00000650: lui x20, 870359
addr0x00000654: addi x9, x18, -398
addr0x00000658: sb x20, 14(x8)
addr0x0000065c: lbu x13, 139(x8)
addr0x00000660: lw x4, 925(x8)
addr0x00000664: andi x14, x20, -207
addr0x00000668: lw x25, 1189(x8)
addr0x0000066c: lw x2, 1054(x8)
addr0x00000670: lw x27, -1695(x8)
addr0x00000674: lbu x4, 502(x8)
addr0x00000678: sw x15, -288(x8)
addr0x0000067c: lw x25, -1168(x8)
addr0x00000680: sw x9, 1635(x8)
addr0x00000684: lw x5, -861(x8)
addr0x00000688: lw x6, 760(x8)
addr0x0000068c: jalr x0, 304(x3)
addr0x00000690: sb x12, -1061(x8)
addr0x00000694: sb x6, 1092(x8)
addr0x00000698: sb x19, 1362(x8)
addr0x0000069c: sb x21, 996(x8)
addr0x000006a0: sw x26, -357(x8)
addr0x000006a4: bge x19, x20, addr0x00000190
addr0x000006a8: lw x1, -135(x8)
addr0x000006ac: bgeu x0, x26, addr0x00000094
addr0x000006b0: jal x18, addr0x000005b4
addr0x000006b4: jal x5, addr0x0000076c
addr0x000006b8: lbu x7, 1728(x8)
addr0x000006bc: sw x31, -223(x8)
addr0x000006c0: sw x2, -518(x8)
addr0x000006c4: lw x19, 522(x8)
addr0x000006c8: lw x24, -317(x8)
addr0x000006cc: addi x7, x15, 545
addr0x000006d0: sw x12, -1642(x8)
addr0x000006d4: lbu x6, 1784(x8)
addr0x000006d8: addi x18, x18, -591
addr0x000006dc: jal x0, addr0x00000204
addr0x000006e0: lw x0, -806(x8)
addr0x000006e4: sw x13, -1559(x8)
addr0x000006e8: addi x9, x30, -1508
addr0x000006ec: jal x12, addr0x000004ec
addr0x000006f0: sw x27, 1929(x8)
addr0x000006f4: andi x4, x28, -895
addr0x000006f8: lw x31, -1244(x8)
addr0x000006fc: lw x2, -1601(x8)
addr0x00000700: lw x25, -1623(x8)
addr0x00000704: sw x23, 1249(x8)
addr0x00000708: sub x7, x9, x0
addr0x0000070c: lhu x18, -842(x8)
addr0x00000710: addi x25, x20, 328
addr0x00000714: jal x10, addr0x00000388
addr0x00000718: slli x15, x22, 7
addr0x0000071c: or x25, x24, x23
addr0x00000720: lui x10, 496376
addr0x00000724: addi x24, x30, -1768
addr0x00000728: bne x21, x24, addr0x000006f8
addr0x0000072c: addi x17, x7, 227
addr0x00000730: lui x0, 543989
addr0x00000734: jal x23, addr0x000007ac
addr0x00000738: lw x7, 1805(x8)
addr0x0000073c: lw x29, -1489(x8)
addr0x00000740: sw x18, -1080(x8)
addr0x00000744: sw x24, -1412(x8)
addr0x00000748: sw x9, -1817(x8)
addr0x0000074c: beq x2, x26, addr0x00000014
addr0x00000750: addi x9, x15, 1246
addr0x00000754: sw x0, -141(x8)
addr0x00000758: jal x20, addr0x00000088
addr0x0000075c: lw x14, -1308(x8)
addr0x00000760: lw x13, 1625(x8)
addr0x00000764: lw x1, 1916(x8)
addr0x00000768: lw x31, 696(x8)
addr0x0000076c: jal x31, addr0x000006f4
addr0x00000770: jal x14, addr0x0000066c
addr0x00000774: sw x29, 1441(x8)
addr0x00000778: lw x4, 365(x8)
addr0x0000077c: lw x12, 1409(x8)
addr0x00000780: sw x11, -1027(x8)
addr0x00000784: sw x23, -1307(x8)
addr0x00000788: sw x27, 43(x8)
addr0x0000078c: lw x30, -730(x8)
addr0x00000790: xori x23, x28, 989
addr0x00000794: and x19, x13, x18
addr0x00000798: and x28, x17, x13
addr0x0000079c: sw x18, -783(x8)
addr0x000007a0: sw x23, -805(x8)
addr0x000007a4: addi x9, x29, -956
addr0x000007a8: lw x19, 307(x8)
addr0x000007ac: lw x16, -473(x8)
addr0x000007b0: lw x13, 23(x8)
addr0x000007b4: lw x19, 975(x8)
addr0x000007b8: lw x19, 1765(x8)
addr0x000007bc: lbu x4, -1417(x8)
addr0x000007c0: lw x17, 492(x8)
addr0x000007c4: slli x27, x29, 9
addr0x000007c8: and x19, x24, x17
addr0x000007cc: sw x13, 1165(x8)
addr0x000007d0: jal x20, addr0x000002f4
addr0x000007d4: sb x0, 407(x8)
addr0x000007d8: lw x15, 643(x8)
addr0x000007dc: lui x4, 767622
addr0x000007e0: addi x7, x17, 775
addr0x000007e4: jal x7, addr0x00000644
addr0x000007e8: lw x24, 1448(x8)
addr0x000007ec: sw x0, -1839(x8)
addr0x000007f0: sw x31, -1799(x8)
addr0x000007f4: sw x18, -1422(x8)
addr0x000007f8: sw x16, 1360(x8)
addr0x000007fc: sw x19, 1820(x8)
addr0x00000800: sw x21, 1450(x8)
addr0x00000804: jal x27, addr0x00000534
