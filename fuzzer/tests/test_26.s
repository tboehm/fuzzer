.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sra x7, x5, x24
addr0x0000001c: ori x5, x27, -934
addr0x00000020: sw x31, -39(x8)
addr0x00000024: sw x13, -2047(x8)
addr0x00000028: jal x19, addr0x00000014
addr0x0000002c: sw x4, -1350(x8)
addr0x00000030: addi x5, x18, 509
addr0x00000034: sw x5, -1155(x8)
addr0x00000038: lw x23, -840(x8)
addr0x0000003c: addi x30, x9, -130
addr0x00000040: sw x13, 1161(x8)
addr0x00000044: sw x14, -2021(x8)
addr0x00000048: sw x14, 280(x8)
addr0x0000004c: lbu x16, 1497(x8)
