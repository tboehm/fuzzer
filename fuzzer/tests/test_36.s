.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bltu x26, x0, addr0x000001e0
addr0x0000001c: slli x16, x13, 15
addr0x00000020: lw x7, 2016(x8)
addr0x00000024: lw x31, -1045(x8)
addr0x00000028: lw x22, 1149(x8)
addr0x0000002c: addi x22, x17, 1315
addr0x00000030: sw x13, 1246(x8)
addr0x00000034: sw x23, -1958(x8)
addr0x00000038: lw x28, 733(x8)
addr0x0000003c: lw x12, 883(x8)
addr0x00000040: addi x18, x19, -679
addr0x00000044: jal x11, addr0x000001a4
addr0x00000048: lw x27, -426(x8)
addr0x0000004c: addi x23, x16, -1939
addr0x00000050: lui x19, 468507
addr0x00000054: addi x29, x20, -1625
addr0x00000058: lw x21, -1442(x8)
addr0x0000005c: lw x23, -187(x8)
addr0x00000060: sw x27, -480(x8)
addr0x00000064: sw x13, 1538(x8)
addr0x00000068: addi x22, x2, 159
addr0x0000006c: sh x17, -1066(x8)
addr0x00000070: jal x14, addr0x00000038
addr0x00000074: sb x25, 12(x8)
addr0x00000078: addi x6, x21, 1428
addr0x0000007c: beq x7, x9, addr0x00000060
addr0x00000080: sw x20, 628(x8)
addr0x00000084: sw x10, -301(x8)
addr0x00000088: jal x14, addr0x000000c0
addr0x0000008c: beq x0, x24, addr0x00000080
addr0x00000090: sw x23, -823(x8)
addr0x00000094: lw x12, -514(x8)
addr0x00000098: lw x26, 288(x8)
addr0x0000009c: sub x9, x26, x26
addr0x000000a0: sub x17, x27, x18
addr0x000000a4: sub x0, x19, x23
addr0x000000a8: addi x28, x10, -1260
addr0x000000ac: jal x18, addr0x0000000c
addr0x000000b0: addi x26, x0, -702
addr0x000000b4: addi x23, x4, 452
addr0x000000b8: sb x22, -676(x8)
addr0x000000bc: jal x17, addr0x00000044
addr0x000000c0: lw x30, -1156(x8)
addr0x000000c4: sw x4, -1351(x8)
addr0x000000c8: auipc x9, 781737
addr0x000000cc: addi x31, x29, 215
addr0x000000d0: sw x27, 1358(x8)
addr0x000000d4: sw x28, -193(x8)
addr0x000000d8: addi x26, x7, 545
addr0x000000dc: addi x17, x15, -304
addr0x000000e0: addi x22, x15, -1223
addr0x000000e4: lui x7, 272540
addr0x000000e8: sw x7, -1815(x8)
addr0x000000ec: jal x1, addr0x0000003c
addr0x000000f0: sb x7, 437(x8)
addr0x000000f4: lui x13, 142289
addr0x000000f8: addi x31, x17, -1172
addr0x000000fc: addi x24, x28, -666
addr0x00000100: sw x18, 818(x8)
addr0x00000104: jal x13, addr0x00000060
addr0x00000108: lw x22, 919(x8)
addr0x0000010c: lw x22, -9(x8)
addr0x00000110: lw x29, -243(x8)
addr0x00000114: add x17, x28, x4
addr0x00000118: jal x19, addr0x000000e0
addr0x0000011c: lw x17, -2039(x8)
addr0x00000120: bge x27, x26, addr0x00000108
addr0x00000124: lbu x4, 643(x8)
addr0x00000128: lw x11, 975(x8)
addr0x0000012c: lw x18, 484(x8)
addr0x00000130: lw x30, 392(x8)
addr0x00000134: addi x14, x29, 1384
addr0x00000138: sw x7, 1460(x8)
addr0x0000013c: sw x0, -176(x8)
addr0x00000140: sw x23, 1252(x8)
addr0x00000144: jal x5, addr0x0000011c
addr0x00000148: bltu x2, x5, addr0x00000108
addr0x0000014c: addi x7, x20, -1057
addr0x00000150: addi x22, x19, 1697
addr0x00000154: sw x19, -562(x8)
addr0x00000158: lw x31, 1770(x8)
addr0x0000015c: bne x28, x22, addr0x000000b8
addr0x00000160: sw x30, -644(x8)
addr0x00000164: sw x20, 1863(x8)
addr0x00000168: lw x17, 401(x8)
addr0x0000016c: lw x19, 318(x8)
addr0x00000170: lw x2, 1255(x8)
addr0x00000174: addi x25, x29, 1447
addr0x00000178: jal x29, addr0x00000114
addr0x0000017c: lw x20, -1798(x8)
addr0x00000180: addi x27, x6, -672
addr0x00000184: jal x17, addr0x0000000c
addr0x00000188: jal x10, addr0x0000000c
addr0x0000018c: lw x12, -396(x8)
addr0x00000190: lw x17, -1952(x8)
addr0x00000194: lw x22, 1344(x8)
addr0x00000198: sw x19, -1410(x8)
addr0x0000019c: lw x7, 1913(x8)
addr0x000001a0: lw x14, 998(x8)
addr0x000001a4: lw x19, -1321(x8)
addr0x000001a8: addi x10, x19, 750
addr0x000001ac: lhu x7, -1586(x8)
addr0x000001b0: lui x29, 955381
addr0x000001b4: addi x26, x6, 1643
addr0x000001b8: jal x17, addr0x000001f8
addr0x000001bc: beq x29, x9, addr0x00000208
addr0x000001c0: lw x0, -198(x8)
addr0x000001c4: sw x18, -905(x8)
addr0x000001c8: sw x7, 2044(x8)
addr0x000001cc: sltu x22, x29, x27
addr0x000001d0: or x26, x11, x20
addr0x000001d4: lw x29, 1491(x8)
addr0x000001d8: bltu x7, x14, addr0x00000160
addr0x000001dc: addi x1, x14, 536
addr0x000001e0: addi x17, x0, -1819
addr0x000001e4: lui x22, 798708
addr0x000001e8: addi x20, x15, 352
addr0x000001ec: jal x16, addr0x00000214
addr0x000001f0: lw x16, -270(x8)
addr0x000001f4: jal x23, addr0x000001c8
addr0x000001f8: jal x13, addr0x00000084
addr0x000001fc: beq x22, x4, addr0x000001f4
addr0x00000200: lw x22, 1552(x8)
addr0x00000204: andi x26, x16, -1576
addr0x00000208: addi x9, x5, -572
addr0x0000020c: sw x19, 795(x8)
addr0x00000210: sw x27, -602(x8)
addr0x00000214: jal x27, addr0x000001b8
addr0x00000218: sw x25, 174(x8)
addr0x0000021c: jal x19, addr0x000001dc
