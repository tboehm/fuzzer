.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sw x10, -435(x8)
addr0x0000001c: andi x18, x0, -1530
addr0x00000020: blt x9, x29, addr0x0000021c
addr0x00000024: addi x23, x19, -1615
addr0x00000028: lui x29, 574925
addr0x0000002c: sw x11, 362(x8)
addr0x00000030: sw x18, -1673(x8)
addr0x00000034: jal x0, addr0x000004e0
addr0x00000038: lw x27, -830(x8)
addr0x0000003c: lw x0, 846(x8)
addr0x00000040: addi x24, x29, 1456
addr0x00000044: beq x6, x23, addr0x00000440
addr0x00000048: jal x2, addr0x00000398
addr0x0000004c: lw x5, 1364(x8)
addr0x00000050: add x13, x6, x6
addr0x00000054: bgeu x0, x11, addr0x000005b0
addr0x00000058: lw x6, 946(x8)
addr0x0000005c: lw x31, -1(x8)
addr0x00000060: beq x7, x23, addr0x00000520
addr0x00000064: blt x0, x7, addr0x00000520
addr0x00000068: sw x29, -991(x8)
addr0x0000006c: sw x23, -451(x8)
addr0x00000070: lw x23, -533(x8)
addr0x00000074: lbu x0, 1848(x8)
addr0x00000078: bne x4, x4, addr0x00000164
addr0x0000007c: addi x18, x29, 1717
addr0x00000080: sw x2, -1178(x8)
addr0x00000084: jal x19, addr0x00000534
addr0x00000088: lw x19, -950(x8)
addr0x0000008c: sw x22, 796(x8)
addr0x00000090: jal x30, addr0x000004f8
addr0x00000094: bne x29, x27, addr0x00000518
addr0x00000098: lw x31, -723(x8)
addr0x0000009c: bgeu x0, x9, addr0x00000588
addr0x000000a0: sw x13, -401(x8)
addr0x000000a4: jal x13, addr0x00000300
addr0x000000a8: lw x15, 471(x8)
addr0x000000ac: addi x9, x24, -1760
addr0x000000b0: beq x19, x7, addr0x000005d4
addr0x000000b4: lw x4, 552(x8)
addr0x000000b8: lw x19, -1059(x8)
addr0x000000bc: addi x29, x6, 1900
addr0x000000c0: jal x9, addr0x000001b0
addr0x000000c4: beq x11, x0, addr0x00000020
addr0x000000c8: addi x30, x23, 781
addr0x000000cc: sw x25, 771(x8)
addr0x000000d0: addi x0, x24, 1862
addr0x000000d4: addi x12, x0, -2032
addr0x000000d8: addi x4, x0, -529
addr0x000000dc: lw x12, -1208(x8)
addr0x000000e0: addi x9, x11, -1430
addr0x000000e4: add x29, x13, x19
addr0x000000e8: jal x31, addr0x00000030
addr0x000000ec: addi x19, x4, -51
addr0x000000f0: jal x18, addr0x00000354
addr0x000000f4: sw x24, 1313(x8)
addr0x000000f8: sw x5, 1839(x8)
addr0x000000fc: jal x23, addr0x00000364
addr0x00000100: bne x0, x13, addr0x00000118
addr0x00000104: sw x20, 1025(x8)
addr0x00000108: lw x19, 57(x8)
addr0x0000010c: lw x2, -1948(x8)
addr0x00000110: ori x1, x22, 1542
addr0x00000114: sw x7, -1692(x8)
addr0x00000118: lui x18, 418742
addr0x0000011c: andi x4, x4, -1014
addr0x00000120: addi x31, x17, 272
addr0x00000124: sw x30, -866(x8)
addr0x00000128: sw x21, -583(x8)
addr0x0000012c: sw x13, 170(x8)
addr0x00000130: lui x29, 463707
addr0x00000134: sw x9, -1597(x8)
addr0x00000138: andi x1, x15, 288
addr0x0000013c: addi x29, x15, 1594
addr0x00000140: beq x0, x10, addr0x000000c8
addr0x00000144: lw x30, 712(x8)
addr0x00000148: lw x24, -866(x8)
addr0x0000014c: jal x30, addr0x000003e0
addr0x00000150: lw x29, -514(x8)
addr0x00000154: lw x20, -201(x8)
addr0x00000158: lw x18, 213(x8)
addr0x0000015c: lw x23, -1120(x8)
addr0x00000160: addi x11, x0, -704
addr0x00000164: sw x23, 1447(x8)
addr0x00000168: lw x20, -916(x8)
addr0x0000016c: jal x17, addr0x000003c0
addr0x00000170: lw x23, 43(x8)
addr0x00000174: lw x23, -1024(x8)
addr0x00000178: lw x23, -428(x8)
addr0x0000017c: lw x27, -631(x8)
addr0x00000180: lw x6, -309(x8)
addr0x00000184: sw x30, 802(x8)
addr0x00000188: sh x19, -372(x8)
addr0x0000018c: addi x19, x0, -1051
addr0x00000190: sw x31, -808(x8)
addr0x00000194: addi x29, x23, -482
addr0x00000198: addi x20, x29, -58
addr0x0000019c: sw x6, -1287(x8)
addr0x000001a0: lw x5, -1677(x8)
addr0x000001a4: lw x24, -709(x8)
addr0x000001a8: bge x1, x17, addr0x00000150
addr0x000001ac: addi x1, x4, 229
addr0x000001b0: sw x29, 283(x8)
addr0x000001b4: jal x23, addr0x000002a0
addr0x000001b8: lw x20, -193(x8)
addr0x000001bc: lw x13, -1418(x8)
addr0x000001c0: bne x20, x23, addr0x0000048c
addr0x000001c4: bne x7, x13, addr0x000005f0
addr0x000001c8: jal x19, addr0x000005a0
addr0x000001cc: lbu x17, -1785(x8)
addr0x000001d0: addi x31, x30, -1102
addr0x000001d4: sw x18, -1411(x8)
addr0x000001d8: jal x22, addr0x0000015c
addr0x000001dc: addi x23, x19, 373
addr0x000001e0: sw x31, -1562(x8)
addr0x000001e4: jal x29, addr0x00000468
addr0x000001e8: lw x15, 1890(x8)
addr0x000001ec: jal x4, addr0x00000324
addr0x000001f0: lui x29, 829453
addr0x000001f4: addi x9, x22, -179
addr0x000001f8: addi x0, x20, 1736
addr0x000001fc: jal x31, addr0x00000270
addr0x00000200: lw x11, -360(x8)
addr0x00000204: addi x24, x31, 1325
addr0x00000208: sw x19, -643(x8)
addr0x0000020c: sw x23, 1145(x8)
addr0x00000210: sw x20, 1763(x8)
addr0x00000214: lw x0, -1221(x8)
addr0x00000218: lw x31, 1106(x8)
addr0x0000021c: lw x9, -1967(x8)
addr0x00000220: jal x21, addr0x00000268
addr0x00000224: addi x20, x20, -284
addr0x00000228: addi x21, x12, 1931
addr0x0000022c: sw x30, -1567(x8)
addr0x00000230: sw x4, 1923(x8)
addr0x00000234: lw x13, -1775(x8)
addr0x00000238: sw x6, 1422(x8)
addr0x0000023c: addi x2, x0, -1094
addr0x00000240: lui x19, 120490
addr0x00000244: sw x0, 1723(x8)
addr0x00000248: sw x18, -558(x8)
addr0x0000024c: sw x13, -1201(x8)
addr0x00000250: addi x14, x9, 179
addr0x00000254: slli x24, x2, 22
addr0x00000258: add x1, x0, x6
addr0x0000025c: jal x6, addr0x000005f4
addr0x00000260: jal x23, addr0x00000104
addr0x00000264: jal x14, addr0x00000678
addr0x00000268: lw x29, 1477(x8)
addr0x0000026c: lw x21, 721(x8)
addr0x00000270: lw x0, 864(x8)
addr0x00000274: sw x21, 438(x8)
addr0x00000278: lw x14, 556(x8)
addr0x0000027c: sub x24, x4, x15
addr0x00000280: slli x29, x7, 11
addr0x00000284: bne x23, x29, addr0x00000260
addr0x00000288: lbu x22, -1222(x8)
addr0x0000028c: jal x5, addr0x0000005c
addr0x00000290: sw x20, -175(x8)
addr0x00000294: jal x30, addr0x000000f8
addr0x00000298: sw x30, -1901(x8)
addr0x0000029c: sb x11, -1923(x8)
addr0x000002a0: jal x14, addr0x00000170
addr0x000002a4: sb x19, 87(x8)
addr0x000002a8: sw x2, -302(x8)
addr0x000002ac: sw x1, 1374(x8)
addr0x000002b0: lw x29, 368(x8)
addr0x000002b4: jal x2, addr0x000003a0
addr0x000002b8: jal x0, addr0x000005cc
addr0x000002bc: jal x1, addr0x00000168
addr0x000002c0: jal x29, addr0x00000220
addr0x000002c4: jal x0, addr0x00000368
addr0x000002c8: lui x11, 1044650
addr0x000002cc: addi x6, x9, 921
addr0x000002d0: addi x0, x0, 832
addr0x000002d4: lw x29, -2006(x8)
addr0x000002d8: lw x22, -1358(x8)
addr0x000002dc: lw x21, 1214(x8)
addr0x000002e0: jal x22, addr0x00000288
addr0x000002e4: bne x22, x7, addr0x00000268
addr0x000002e8: jal x2, addr0x000003dc
addr0x000002ec: addi x19, x22, 330
addr0x000002f0: jal x6, addr0x0000042c
addr0x000002f4: lw x29, 1210(x8)
addr0x000002f8: bne x17, x18, addr0x00000388
addr0x000002fc: jal x0, addr0x00000270
addr0x00000300: jal x2, addr0x00000424
addr0x00000304: addi x29, x13, -503
addr0x00000308: jal x30, addr0x000003d8
addr0x0000030c: lui x7, 187930
addr0x00000310: addi x24, x4, 1552
addr0x00000314: jal x20, addr0x000004f8
addr0x00000318: addi x30, x4, -884
addr0x0000031c: addi x0, x23, 807
addr0x00000320: addi x29, x29, -1206
addr0x00000324: addi x29, x31, 1698
addr0x00000328: sub x11, x4, x30
addr0x0000032c: srai x17, x9, 18
addr0x00000330: srai x29, x0, 18
addr0x00000334: lw x18, -1667(x8)
addr0x00000338: lw x5, 1969(x8)
addr0x0000033c: addi x29, x22, -950
addr0x00000340: jal x24, addr0x00000194
addr0x00000344: jal x17, addr0x000002e4
addr0x00000348: jal x30, addr0x000003fc
addr0x0000034c: lw x15, -1675(x8)
addr0x00000350: lw x2, 1169(x8)
addr0x00000354: beq x5, x19, addr0x00000460
addr0x00000358: andi x15, x4, -755
addr0x0000035c: lw x9, 583(x8)
addr0x00000360: lw x22, 1444(x8)
addr0x00000364: lw x29, -1079(x8)
addr0x00000368: addi x14, x21, -343
addr0x0000036c: jal x22, addr0x0000000c
addr0x00000370: lhu x4, -1841(x8)
addr0x00000374: addi x29, x7, -1889
addr0x00000378: sw x9, 121(x8)
addr0x0000037c: sw x27, -1811(x8)
addr0x00000380: addi x6, x31, -1870
addr0x00000384: addi x6, x9, -1489
addr0x00000388: sw x20, -1580(x8)
addr0x0000038c: sw x21, 915(x8)
addr0x00000390: jal x30, addr0x00000490
addr0x00000394: lw x0, -1071(x8)
addr0x00000398: addi x19, x23, -223
addr0x0000039c: sw x17, -1642(x8)
addr0x000003a0: sw x30, 1758(x8)
addr0x000003a4: sw x0, -699(x8)
addr0x000003a8: sw x29, 380(x8)
addr0x000003ac: addi x15, x13, 1725
addr0x000003b0: sw x19, -320(x8)
addr0x000003b4: sw x0, 1310(x8)
addr0x000003b8: addi x23, x17, 998
addr0x000003bc: jal x25, addr0x000000f8
addr0x000003c0: lw x9, -645(x8)
addr0x000003c4: addi x12, x20, 427
addr0x000003c8: lbu x14, -1756(x8)
addr0x000003cc: andi x11, x9, 1216
addr0x000003d0: add x17, x13, x22
addr0x000003d4: jal x23, addr0x0000066c
addr0x000003d8: beq x9, x16, addr0x00000660
addr0x000003dc: andi x11, x20, 311
addr0x000003e0: lw x9, -1158(x8)
addr0x000003e4: addi x31, x25, -1898
addr0x000003e8: sw x22, -495(x8)
addr0x000003ec: sw x22, 1620(x8)
addr0x000003f0: jal x11, addr0x00000238
addr0x000003f4: bne x29, x11, addr0x000005b8
addr0x000003f8: lbu x23, 476(x8)
addr0x000003fc: beq x7, x31, addr0x00000584
addr0x00000400: lw x19, -396(x8)
addr0x00000404: sw x21, -391(x8)
addr0x00000408: lw x23, -1126(x8)
addr0x0000040c: lw x18, 938(x8)
addr0x00000410: jal x23, addr0x00000388
addr0x00000414: lui x11, 52767
addr0x00000418: addi x4, x0, -1708
addr0x0000041c: jal x21, addr0x00000410
addr0x00000420: sw x4, -1851(x8)
addr0x00000424: jal x23, addr0x00000278
addr0x00000428: jal x23, addr0x00000138
addr0x0000042c: lw x13, -1529(x8)
addr0x00000430: lw x19, 1015(x8)
addr0x00000434: sw x15, -1734(x8)
addr0x00000438: sw x19, 1903(x8)
addr0x0000043c: addi x1, x31, -1285
addr0x00000440: addi x22, x4, 422
addr0x00000444: lw x29, 687(x8)
addr0x00000448: addi x13, x12, 1031
addr0x0000044c: addi x19, x13, -1319
addr0x00000450: lw x7, 1377(x8)
addr0x00000454: addi x23, x6, -1227
addr0x00000458: addi x13, x24, 159
addr0x0000045c: lw x4, 424(x8)
addr0x00000460: lui x30, 937068
addr0x00000464: sw x18, -260(x8)
addr0x00000468: lw x23, -581(x8)
addr0x0000046c: jal x30, addr0x0000066c
addr0x00000470: lw x2, -1950(x8)
addr0x00000474: addi x5, x13, 1231
addr0x00000478: sw x18, 376(x8)
addr0x0000047c: lw x17, -46(x8)
addr0x00000480: add x15, x21, x22
addr0x00000484: sw x14, -382(x8)
addr0x00000488: sw x22, 535(x8)
addr0x0000048c: sb x12, 276(x8)
addr0x00000490: sw x19, 40(x8)
addr0x00000494: sw x21, -1385(x8)
addr0x00000498: addi x5, x17, -1678
addr0x0000049c: lui x2, 315507
addr0x000004a0: addi x22, x0, 508
addr0x000004a4: sw x29, 141(x8)
addr0x000004a8: jal x13, addr0x00000668
addr0x000004ac: lw x1, -216(x8)
addr0x000004b0: sub x19, x22, x22
addr0x000004b4: sw x5, 837(x8)
addr0x000004b8: addi x30, x30, 236
addr0x000004bc: lw x18, 592(x8)
addr0x000004c0: lw x12, 1036(x8)
addr0x000004c4: addi x6, x23, -1579
addr0x000004c8: mul x27, x5, x2
addr0x000004cc: sub x7, x15, x5
addr0x000004d0: bgeu x7, x9, addr0x000005c4
addr0x000004d4: sh x30, -1295(x8)
addr0x000004d8: jal x11, addr0x00000524
addr0x000004dc: bgeu x12, x19, addr0x0000065c
addr0x000004e0: jal x0, addr0x00000100
addr0x000004e4: jal x13, addr0x0000013c
addr0x000004e8: addi x7, x7, -980
addr0x000004ec: lui x19, 59046
addr0x000004f0: addi x20, x27, -1197
addr0x000004f4: lui x29, 906282
addr0x000004f8: addi x16, x29, 1271
addr0x000004fc: lui x23, 627729
addr0x00000500: addi x23, x13, -306
addr0x00000504: jal x4, addr0x00000174
addr0x00000508: lw x19, 1659(x8)
addr0x0000050c: jal x9, addr0x000001a8
addr0x00000510: sw x10, 190(x8)
addr0x00000514: sw x19, -1567(x8)
addr0x00000518: addi x0, x7, -417
addr0x0000051c: sw x0, 49(x8)
addr0x00000520: addi x17, x5, 1223
addr0x00000524: sw x0, 1014(x8)
addr0x00000528: sw x0, -1810(x8)
addr0x0000052c: sw x13, 428(x8)
addr0x00000530: sw x29, 1634(x8)
addr0x00000534: sb x1, 1513(x8)
addr0x00000538: lw x20, 782(x8)
addr0x0000053c: lui x29, 234619
addr0x00000540: addi x13, x21, -312
addr0x00000544: sw x11, 225(x8)
addr0x00000548: sw x19, -300(x8)
addr0x0000054c: beq x15, x14, addr0x000002d0
addr0x00000550: lw x17, 150(x8)
addr0x00000554: sw x22, 1350(x8)
addr0x00000558: add x2, x30, x19
addr0x0000055c: lbu x29, -1602(x8)
addr0x00000560: lw x21, -1359(x8)
addr0x00000564: lw x2, -1283(x8)
addr0x00000568: bge x30, x19, addr0x00000394
addr0x0000056c: sub x24, x15, x30
addr0x00000570: lbu x19, 513(x8)
addr0x00000574: lbu x6, -183(x8)
addr0x00000578: addi x29, x17, -890
addr0x0000057c: lw x0, -65(x8)
addr0x00000580: sh x24, -762(x8)
addr0x00000584: addi x0, x9, -819
addr0x00000588: bltu x25, x22, addr0x000003c8
addr0x0000058c: lbu x7, -178(x8)
addr0x00000590: lw x15, 1107(x8)
addr0x00000594: addi x9, x15, -1202
addr0x00000598: jal x15, addr0x00000668
addr0x0000059c: sw x5, 319(x8)
addr0x000005a0: jal x20, addr0x00000020
addr0x000005a4: mul x30, x7, x21
addr0x000005a8: sw x0, -610(x8)
addr0x000005ac: addi x0, x4, -337
addr0x000005b0: lw x19, 931(x8)
addr0x000005b4: add x29, x2, x18
addr0x000005b8: lw x1, -1072(x8)
addr0x000005bc: jal x22, addr0x000002a0
addr0x000005c0: jal x22, addr0x000003b0
addr0x000005c4: addi x18, x15, -673
addr0x000005c8: sw x31, 1434(x8)
addr0x000005cc: lw x5, 2044(x8)
addr0x000005d0: lw x15, -276(x8)
addr0x000005d4: add x7, x20, x22
addr0x000005d8: jal x4, addr0x00000220
addr0x000005dc: lw x10, -689(x8)
addr0x000005e0: lw x4, 478(x8)
addr0x000005e4: lui x27, 346477
addr0x000005e8: addi x12, x0, -802
addr0x000005ec: sw x19, -970(x8)
addr0x000005f0: sh x13, 787(x8)
addr0x000005f4: sw x0, -1075(x8)
addr0x000005f8: sw x0, -1703(x8)
addr0x000005fc: sw x22, -505(x8)
addr0x00000600: sw x31, 947(x8)
addr0x00000604: sw x22, 1129(x8)
addr0x00000608: sw x20, -1247(x8)
addr0x0000060c: jal x23, addr0x00000114
addr0x00000610: jal x5, addr0x000001dc
addr0x00000614: jal x2, addr0x00000544
addr0x00000618: jal x13, addr0x00000558
addr0x0000061c: sw x22, -989(x8)
addr0x00000620: sw x4, -1601(x8)
addr0x00000624: jal x11, addr0x00000428
addr0x00000628: addi x29, x23, -671
addr0x0000062c: jal x31, addr0x000000fc
addr0x00000630: sw x16, -1851(x8)
addr0x00000634: jal x9, addr0x00000494
addr0x00000638: sw x21, 1983(x8)
addr0x0000063c: sw x9, 826(x8)
addr0x00000640: lw x4, -1120(x8)
addr0x00000644: lw x9, -1071(x8)
addr0x00000648: lw x2, -1362(x8)
addr0x0000064c: beq x5, x2, addr0x00000538
addr0x00000650: lw x9, 815(x8)
addr0x00000654: ori x13, x24, 1896
addr0x00000658: sw x11, -608(x8)
addr0x0000065c: sw x21, 1260(x8)
addr0x00000660: lui x11, 993434
addr0x00000664: addi x21, x19, -526
addr0x00000668: add x5, x27, x10
addr0x0000066c: jal x15, addr0x000001e8
addr0x00000670: bne x23, x29, addr0x0000061c
addr0x00000674: lw x17, -1337(x8)
addr0x00000678: bne x22, x4, addr0x00000380
addr0x0000067c: jal x9, addr0x000000b0
addr0x00000680: bge x5, x17, addr0x000001b0
addr0x00000684: jal x1, addr0x000005a0
