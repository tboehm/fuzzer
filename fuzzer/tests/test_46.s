.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: divu x27, x22, x23
addr0x0000001c: bltu x25, x16, addr0x000002f0
addr0x00000020: srli x11, x23, 3
addr0x00000024: jal x11, addr0x00000034
addr0x00000028: andi x27, x22, -1801
addr0x0000002c: bltu x7, x5, addr0x000007a4
addr0x00000030: addi x6, x6, 268
addr0x00000034: auipc x20, 84586
addr0x00000038: addi x30, x0, 1337
addr0x0000003c: andi x25, x29, -1498
addr0x00000040: auipc x19, 333967
addr0x00000044: addi x25, x18, -1160
addr0x00000048: beq x7, x13, addr0x00000578
addr0x0000004c: lw x27, 180(x8)
addr0x00000050: sub x7, x4, x7
addr0x00000054: add x5, x1, x18
addr0x00000058: sub x5, x20, x31
addr0x0000005c: srai x13, x27, 26
addr0x00000060: addi x20, x24, -891
addr0x00000064: sw x16, 1932(x8)
addr0x00000068: sw x23, -1794(x8)
addr0x0000006c: sw x12, -667(x8)
addr0x00000070: sw x16, 830(x8)
addr0x00000074: jal x25, addr0x00000174
addr0x00000078: lui x13, 685985
addr0x0000007c: addi x20, x7, 512
addr0x00000080: lui x22, 244879
addr0x00000084: addi x13, x7, -1183
addr0x00000088: lw x4, -662(x8)
addr0x0000008c: lw x23, 1378(x8)
addr0x00000090: addi x13, x16, -1696
addr0x00000094: addi x20, x14, -369
addr0x00000098: jal x19, addr0x00000730
addr0x0000009c: sw x9, 1709(x8)
addr0x000000a0: bne x20, x28, addr0x00000598
addr0x000000a4: lw x19, 1091(x8)
addr0x000000a8: lw x9, -680(x8)
addr0x000000ac: jal x9, addr0x00000678
addr0x000000b0: lw x23, -316(x8)
addr0x000000b4: jal x23, addr0x000000e0
addr0x000000b8: beq x23, x5, addr0x00000340
addr0x000000bc: lw x25, -330(x8)
addr0x000000c0: add x28, x13, x27
addr0x000000c4: sw x4, 1969(x8)
addr0x000000c8: jal x22, addr0x00000a78
addr0x000000cc: lw x7, 1482(x8)
addr0x000000d0: lw x16, 679(x8)
addr0x000000d4: lw x16, -687(x8)
addr0x000000d8: addi x27, x14, -1161
addr0x000000dc: jal x29, addr0x0000021c
addr0x000000e0: lw x13, 93(x8)
addr0x000000e4: lw x1, -2032(x8)
addr0x000000e8: lw x23, 190(x8)
addr0x000000ec: lw x0, 1717(x8)
addr0x000000f0: jal x15, addr0x00000494
addr0x000000f4: sw x17, 1746(x8)
addr0x000000f8: jal x16, addr0x00000a94
addr0x000000fc: lw x15, 67(x8)
addr0x00000100: jal x31, addr0x000006d4
addr0x00000104: jal x14, addr0x0000039c
addr0x00000108: sw x14, -515(x8)
addr0x0000010c: jal x13, addr0x00000630
addr0x00000110: sw x22, -1674(x8)
addr0x00000114: addi x31, x14, 600
addr0x00000118: lw x0, 572(x8)
addr0x0000011c: lbu x7, 1447(x8)
addr0x00000120: andi x18, x16, -1667
addr0x00000124: bne x20, x12, addr0x00000174
addr0x00000128: lw x17, -319(x8)
addr0x0000012c: sub x14, x28, x29
addr0x00000130: beq x24, x5, addr0x00000158
addr0x00000134: lui x0, 103095
addr0x00000138: addi x27, x23, -1214
addr0x0000013c: jal x5, addr0x00000a20
addr0x00000140: lw x18, -1648(x8)
addr0x00000144: addi x18, x2, -515
addr0x00000148: jal x0, addr0x00000468
addr0x0000014c: jal x25, addr0x000008ac
addr0x00000150: lw x10, 1093(x8)
addr0x00000154: addi x27, x30, -1695
addr0x00000158: sw x25, 1100(x8)
addr0x0000015c: sw x18, 1720(x8)
addr0x00000160: lbu x14, -1733(x8)
addr0x00000164: jal x17, addr0x00000948
addr0x00000168: jal x15, addr0x00000190
addr0x0000016c: lui x0, 395216
addr0x00000170: lui x5, 743866
addr0x00000174: addi x6, x0, -100
addr0x00000178: sw x13, 1334(x8)
addr0x0000017c: sw x2, -1152(x8)
addr0x00000180: jal x5, addr0x000002dc
addr0x00000184: sw x7, -1554(x8)
addr0x00000188: add x22, x29, x7
addr0x0000018c: sw x21, 1161(x8)
addr0x00000190: lbu x2, -1718(x8)
addr0x00000194: lw x4, -1946(x8)
addr0x00000198: lw x5, -1176(x8)
addr0x0000019c: addi x4, x27, 447
addr0x000001a0: bgeu x19, x13, addr0x000006d4
addr0x000001a4: addi x29, x22, 445
addr0x000001a8: addi x1, x4, -1560
addr0x000001ac: sw x18, 547(x8)
addr0x000001b0: lw x27, -314(x8)
addr0x000001b4: lw x9, -1185(x8)
addr0x000001b8: addi x17, x31, 308
addr0x000001bc: lw x25, -1531(x8)
addr0x000001c0: lw x12, 1318(x8)
addr0x000001c4: addi x4, x17, 1769
addr0x000001c8: sw x22, -447(x8)
addr0x000001cc: lui x20, 867714
addr0x000001d0: sw x19, -2020(x8)
addr0x000001d4: lw x27, 57(x8)
addr0x000001d8: sw x17, -1018(x8)
addr0x000001dc: sw x26, 15(x8)
addr0x000001e0: addi x9, x27, 2034
addr0x000001e4: sw x2, -410(x8)
addr0x000001e8: jal x29, addr0x000002f8
addr0x000001ec: sw x23, 612(x8)
addr0x000001f0: sw x14, -45(x8)
addr0x000001f4: lw x17, -1702(x8)
addr0x000001f8: lw x25, -2019(x8)
addr0x000001fc: lw x7, -478(x8)
addr0x00000200: sw x18, 164(x8)
addr0x00000204: sw x18, 749(x8)
addr0x00000208: lbu x23, -349(x8)
addr0x0000020c: lw x23, 1854(x8)
addr0x00000210: lw x19, 36(x8)
addr0x00000214: sw x30, 1043(x8)
addr0x00000218: addi x22, x30, -1519
addr0x0000021c: srli x28, x21, 17
addr0x00000220: sw x27, 1592(x8)
addr0x00000224: sw x7, 1770(x8)
addr0x00000228: sw x23, -1333(x8)
addr0x0000022c: bne x1, x15, addr0x00000878
addr0x00000230: bne x20, x18, addr0x000002f0
addr0x00000234: addi x26, x7, -1231
addr0x00000238: jal x0, addr0x000003f4
addr0x0000023c: lui x20, 772146
addr0x00000240: addi x0, x22, -159
addr0x00000244: sw x19, -1559(x8)
addr0x00000248: addi x9, x18, 1027
addr0x0000024c: bne x19, x14, addr0x000009a0
addr0x00000250: jal x18, addr0x00000b44
addr0x00000254: addi x5, x14, -1872
addr0x00000258: lw x1, 1420(x8)
addr0x0000025c: lw x22, 1868(x8)
addr0x00000260: sw x22, -1001(x8)
addr0x00000264: lw x13, 1492(x8)
addr0x00000268: lui x31, 999703
addr0x0000026c: addi x11, x17, 440
addr0x00000270: sw x16, -652(x8)
addr0x00000274: lw x27, -1246(x8)
addr0x00000278: bgeu x15, x18, addr0x00000230
addr0x0000027c: sb x23, 1002(x8)
addr0x00000280: sw x23, 762(x8)
addr0x00000284: sw x29, 919(x8)
addr0x00000288: beq x9, x1, addr0x00000234
addr0x0000028c: lw x23, -34(x8)
addr0x00000290: lw x5, 1769(x8)
addr0x00000294: sw x10, 442(x8)
addr0x00000298: sw x0, 785(x8)
addr0x0000029c: sw x30, 585(x8)
addr0x000002a0: lbu x29, -287(x8)
addr0x000002a4: beq x30, x7, addr0x00000614
addr0x000002a8: lw x9, -1283(x8)
addr0x000002ac: addi x7, x18, 678
addr0x000002b0: lw x29, -125(x8)
addr0x000002b4: andi x23, x25, 880
addr0x000002b8: jal x27, addr0x0000062c
addr0x000002bc: lw x13, -1320(x8)
addr0x000002c0: lw x29, 1247(x8)
addr0x000002c4: sw x10, 1503(x8)
addr0x000002c8: lhu x31, 1123(x8)
addr0x000002cc: lhu x1, 8(x8)
addr0x000002d0: addi x17, x1, 1394
addr0x000002d4: sh x25, -1535(x8)
addr0x000002d8: andi x5, x24, -1822
addr0x000002dc: addi x7, x18, 323
addr0x000002e0: addi x7, x17, 1017
addr0x000002e4: addi x22, x13, -1572
addr0x000002e8: sw x22, -159(x8)
addr0x000002ec: addi x29, x29, 831
addr0x000002f0: beq x17, x9, addr0x00000198
addr0x000002f4: bltu x27, x22, addr0x00000734
addr0x000002f8: lw x29, 1525(x8)
addr0x000002fc: sw x11, -1031(x8)
addr0x00000300: lw x14, 1243(x8)
addr0x00000304: sw x25, 1331(x8)
addr0x00000308: lw x29, 803(x8)
addr0x0000030c: lw x18, -880(x8)
addr0x00000310: sw x13, -130(x8)
addr0x00000314: jal x22, addr0x00000868
addr0x00000318: lbu x30, 1869(x8)
addr0x0000031c: andi x10, x14, 1004
addr0x00000320: sb x9, 925(x8)
addr0x00000324: jalr x0, 712(x3)
addr0x00000328: beq x27, x14, addr0x000000b8
addr0x0000032c: lw x2, 667(x8)
addr0x00000330: bne x16, x22, addr0x000003e4
addr0x00000334: lw x10, 883(x8)
addr0x00000338: lw x26, 694(x8)
addr0x0000033c: lw x16, -316(x8)
addr0x00000340: jal x18, addr0x000009d4
addr0x00000344: sw x7, -541(x8)
addr0x00000348: sw x29, 145(x8)
addr0x0000034c: lw x23, 1426(x8)
addr0x00000350: sw x1, -1744(x8)
addr0x00000354: sw x6, 468(x8)
addr0x00000358: addi x22, x1, -389
addr0x0000035c: sw x1, 489(x8)
addr0x00000360: sw x19, 167(x8)
addr0x00000364: sw x31, -1323(x8)
addr0x00000368: addi x23, x23, 1842
addr0x0000036c: lui x17, 401375
addr0x00000370: addi x18, x14, 1248
addr0x00000374: jal x14, addr0x00000290
addr0x00000378: addi x15, x23, -1260
addr0x0000037c: lw x29, -484(x8)
addr0x00000380: lw x4, 120(x8)
addr0x00000384: jal x18, addr0x00000420
addr0x00000388: ori x20, x23, -1114
addr0x0000038c: sw x17, -1057(x8)
addr0x00000390: sw x14, -1979(x8)
addr0x00000394: sw x1, 1153(x8)
addr0x00000398: addi x0, x19, 794
addr0x0000039c: jal x18, addr0x000003b4
addr0x000003a0: lw x16, 212(x8)
addr0x000003a4: ori x0, x31, 1328
addr0x000003a8: sw x18, -590(x8)
addr0x000003ac: sw x29, 748(x8)
addr0x000003b0: lw x25, 1922(x8)
addr0x000003b4: beq x0, x27, addr0x0000077c
addr0x000003b8: sw x17, 384(x8)
addr0x000003bc: lw x27, -1945(x8)
addr0x000003c0: jal x22, addr0x0000041c
addr0x000003c4: bne x23, x22, addr0x00000278
addr0x000003c8: jal x19, addr0x00000790
addr0x000003cc: lw x28, -2025(x8)
addr0x000003d0: add x30, x7, x19
addr0x000003d4: sw x30, -1432(x8)
addr0x000003d8: lw x23, 1435(x8)
addr0x000003dc: lw x13, -1390(x8)
addr0x000003e0: bgeu x7, x28, addr0x00000778
addr0x000003e4: lw x23, -140(x8)
addr0x000003e8: lw x12, 730(x8)
addr0x000003ec: add x17, x28, x18
addr0x000003f0: jal x4, addr0x000004d8
addr0x000003f4: lw x20, 1291(x8)
addr0x000003f8: addi x30, x1, -1733
addr0x000003fc: sw x29, 1247(x8)
addr0x00000400: sw x14, -440(x8)
addr0x00000404: sw x18, -1956(x8)
addr0x00000408: sw x14, -1059(x8)
addr0x0000040c: jal x4, addr0x000007c0
addr0x00000410: sw x17, 229(x8)
addr0x00000414: sw x9, -810(x8)
addr0x00000418: addi x1, x30, 1687
addr0x0000041c: jal x29, addr0x00000908
addr0x00000420: sw x7, 1381(x8)
addr0x00000424: sw x18, -314(x8)
addr0x00000428: addi x25, x17, 384
addr0x0000042c: lui x10, 324400
addr0x00000430: addi x25, x29, 1643
addr0x00000434: addi x17, x26, 870
addr0x00000438: bne x26, x31, addr0x00000968
addr0x0000043c: slli x5, x13, 30
addr0x00000440: lw x19, -836(x8)
addr0x00000444: lw x14, -2023(x8)
addr0x00000448: sw x27, 2013(x8)
addr0x0000044c: sw x21, 1063(x8)
addr0x00000450: lui x19, 773718
addr0x00000454: addi x2, x23, 1521
addr0x00000458: jal x31, addr0x00000150
addr0x0000045c: addi x2, x22, -1584
addr0x00000460: jal x12, addr0x00000774
addr0x00000464: lw x9, -766(x8)
addr0x00000468: addi x6, x30, -910
addr0x0000046c: lw x9, -1885(x8)
addr0x00000470: sw x7, 1238(x8)
addr0x00000474: jal x21, addr0x000003d4
addr0x00000478: lw x0, 389(x8)
addr0x0000047c: add x25, x13, x29
addr0x00000480: jal x23, addr0x00000b5c
addr0x00000484: lui x17, 978963
addr0x00000488: addi x15, x17, 1986
addr0x0000048c: jal x18, addr0x00000760
addr0x00000490: lw x14, 1304(x8)
addr0x00000494: sub x16, x28, x22
addr0x00000498: slli x7, x9, 20
addr0x0000049c: add x13, x26, x22
addr0x000004a0: sub x22, x7, x23
addr0x000004a4: addi x23, x20, 50
addr0x000004a8: lw x21, 1658(x8)
addr0x000004ac: addi x19, x18, -669
addr0x000004b0: sw x20, 1240(x8)
addr0x000004b4: sw x18, 281(x8)
addr0x000004b8: addi x25, x28, 281
addr0x000004bc: jal x23, addr0x0000096c
addr0x000004c0: lw x31, -1429(x8)
addr0x000004c4: lw x27, -1657(x8)
addr0x000004c8: addi x29, x28, 1079
addr0x000004cc: jal x4, addr0x000006c4
addr0x000004d0: addi x31, x26, -279
addr0x000004d4: jal x19, addr0x000008d4
addr0x000004d8: lw x4, -713(x8)
addr0x000004dc: sw x25, 325(x8)
addr0x000004e0: sw x4, -426(x8)
addr0x000004e4: bne x30, x19, addr0x0000032c
addr0x000004e8: lui x22, 1009289
addr0x000004ec: sw x17, 707(x8)
addr0x000004f0: xori x21, x11, 277
addr0x000004f4: andi x31, x20, -1036
addr0x000004f8: beq x4, x26, addr0x00000260
addr0x000004fc: slli x31, x23, 23
addr0x00000500: sw x23, -1947(x8)
addr0x00000504: lw x16, -1600(x8)
addr0x00000508: lbu x19, -1197(x8)
addr0x0000050c: lw x5, -1635(x8)
addr0x00000510: or x22, x14, x20
addr0x00000514: lbu x19, 1110(x8)
addr0x00000518: lw x15, 188(x8)
addr0x0000051c: andi x24, x22, -570
addr0x00000520: addi x4, x16, -1456
addr0x00000524: sw x2, -1640(x8)
addr0x00000528: sw x2, -642(x8)
addr0x0000052c: jal x17, addr0x00000520
addr0x00000530: lw x24, 1796(x8)
addr0x00000534: lw x20, 757(x8)
addr0x00000538: sub x13, x18, x14
addr0x0000053c: add x27, x28, x23
addr0x00000540: beq x12, x0, addr0x00000784
addr0x00000544: slli x30, x23, 26
addr0x00000548: or x9, x26, x23
addr0x0000054c: jal x17, addr0x00000348
addr0x00000550: lw x7, 613(x8)
addr0x00000554: lw x31, 1661(x8)
addr0x00000558: addi x10, x24, 1186
addr0x0000055c: lw x27, 826(x8)
addr0x00000560: lw x18, 930(x8)
addr0x00000564: lw x9, -1372(x8)
addr0x00000568: addi x13, x11, -963
addr0x0000056c: jal x27, addr0x0000003c
addr0x00000570: lw x19, -346(x8)
addr0x00000574: lw x31, -748(x8)
addr0x00000578: lw x29, -1548(x8)
addr0x0000057c: lw x18, 1120(x8)
addr0x00000580: sw x2, -616(x8)
addr0x00000584: jal x5, addr0x000008cc
addr0x00000588: sw x18, 60(x8)
addr0x0000058c: sw x11, 1055(x8)
addr0x00000590: jal x7, addr0x00000990
addr0x00000594: addi x25, x12, 966
addr0x00000598: lui x5, 458456
addr0x0000059c: blt x27, x22, addr0x000000d4
addr0x000005a0: jal x30, addr0x00000748
addr0x000005a4: jal x15, addr0x00000a88
addr0x000005a8: jal x29, addr0x00000084
addr0x000005ac: sw x20, -1043(x8)
addr0x000005b0: sw x31, -1835(x8)
addr0x000005b4: sw x1, -974(x8)
addr0x000005b8: addi x31, x12, -1193
addr0x000005bc: jal x29, addr0x00000378
addr0x000005c0: jal x15, addr0x000003dc
addr0x000005c4: slli x27, x12, 19
addr0x000005c8: add x6, x22, x14
addr0x000005cc: lw x30, 520(x8)
addr0x000005d0: lw x23, 66(x8)
addr0x000005d4: lw x5, -715(x8)
addr0x000005d8: bgeu x26, x13, addr0x00000834
addr0x000005dc: sw x27, 1887(x8)
addr0x000005e0: jal x16, addr0x00000b04
addr0x000005e4: sw x29, 277(x8)
addr0x000005e8: sw x15, -1634(x8)
addr0x000005ec: jal x5, addr0x0000087c
addr0x000005f0: lw x28, 39(x8)
addr0x000005f4: addi x1, x27, 114
addr0x000005f8: auipc x30, 478859
addr0x000005fc: jalr x0, 952(x3)
addr0x00000600: jal x12, addr0x00000038
addr0x00000604: addi x28, x19, -1335
addr0x00000608: jal x28, addr0x0000088c
addr0x0000060c: sw x31, -441(x8)
addr0x00000610: sw x29, -632(x8)
addr0x00000614: sw x23, -1105(x8)
addr0x00000618: sw x22, -657(x8)
addr0x0000061c: addi x4, x24, 796
addr0x00000620: sw x26, 1141(x8)
addr0x00000624: sw x23, -49(x8)
addr0x00000628: addi x9, x20, -1858
addr0x0000062c: jal x18, addr0x000007d8
addr0x00000630: lw x17, -1027(x8)
addr0x00000634: lw x29, -707(x8)
addr0x00000638: sub x28, x31, x18
addr0x0000063c: lw x2, -1611(x8)
addr0x00000640: lbu x7, -331(x8)
addr0x00000644: addi x7, x30, -237
addr0x00000648: sw x25, 121(x8)
addr0x0000064c: lbu x24, 1540(x8)
addr0x00000650: andi x14, x23, 753
addr0x00000654: addi x20, x13, -1433
addr0x00000658: lw x5, 1463(x8)
addr0x0000065c: lbu x7, -1691(x8)
addr0x00000660: sb x2, 1858(x8)
addr0x00000664: sw x22, -1481(x8)
addr0x00000668: sw x10, -189(x8)
addr0x0000066c: sw x29, -1535(x8)
addr0x00000670: jal x18, addr0x00000064
addr0x00000674: sw x9, 1179(x8)
addr0x00000678: addi x7, x14, -1103
addr0x0000067c: add x28, x9, x22
addr0x00000680: sub x29, x2, x22
addr0x00000684: sub x14, x2, x17
addr0x00000688: lui x11, 32526
addr0x0000068c: sw x19, -1563(x8)
addr0x00000690: sw x9, -973(x8)
addr0x00000694: sw x27, 1617(x8)
addr0x00000698: addi x7, x5, 881
addr0x0000069c: addi x24, x17, 807
addr0x000006a0: jal x25, addr0x0000093c
addr0x000006a4: beq x20, x17, addr0x00000784
addr0x000006a8: lw x31, -132(x8)
addr0x000006ac: jal x19, addr0x00000034
addr0x000006b0: slli x29, x20, 24
addr0x000006b4: add x10, x18, x18
addr0x000006b8: sw x27, 784(x8)
addr0x000006bc: sb x17, -637(x8)
addr0x000006c0: addi x29, x6, 335
addr0x000006c4: add x21, x27, x17
addr0x000006c8: beq x13, x0, addr0x000004b4
addr0x000006cc: lw x21, 1739(x8)
addr0x000006d0: andi x31, x17, -876
addr0x000006d4: jal x20, addr0x0000037c
addr0x000006d8: lui x16, 244990
addr0x000006dc: addi x24, x19, 768
addr0x000006e0: add x17, x27, x27
addr0x000006e4: lw x5, -651(x8)
addr0x000006e8: lw x9, -206(x8)
addr0x000006ec: lw x23, -1397(x8)
addr0x000006f0: lw x20, -393(x8)
addr0x000006f4: lw x27, -892(x8)
addr0x000006f8: lw x27, -2048(x8)
addr0x000006fc: jal x22, addr0x00000ab0
addr0x00000700: lw x17, 1463(x8)
addr0x00000704: lw x28, -182(x8)
addr0x00000708: bne x9, x20, addr0x000005ac
addr0x0000070c: lbu x6, 500(x8)
addr0x00000710: srli x12, x29, 2
addr0x00000714: beq x19, x10, addr0x00000194
addr0x00000718: beq x13, x22, addr0x00000738
addr0x0000071c: lw x17, -827(x8)
addr0x00000720: addi x16, x19, 449
addr0x00000724: sw x27, -1959(x8)
addr0x00000728: bltu x18, x22, addr0x00000840
addr0x0000072c: sb x22, -1659(x8)
addr0x00000730: jal x29, addr0x000003cc
addr0x00000734: sw x29, 519(x8)
addr0x00000738: sw x23, 1503(x8)
addr0x0000073c: sw x6, -1953(x8)
addr0x00000740: jal x1, addr0x000004cc
addr0x00000744: bne x0, x7, addr0x000000bc
addr0x00000748: addi x22, x14, -549
addr0x0000074c: lw x9, 936(x8)
addr0x00000750: lui x15, 967386
addr0x00000754: addi x7, x7, -896
addr0x00000758: sw x14, -1413(x8)
addr0x0000075c: sw x0, 765(x8)
addr0x00000760: jal x17, addr0x00000980
addr0x00000764: addi x18, x20, -1826
addr0x00000768: andi x19, x19, 33
addr0x0000076c: addi x23, x16, 1140
addr0x00000770: jal x17, addr0x00000124
addr0x00000774: lw x18, 1471(x8)
addr0x00000778: lw x22, -1534(x8)
addr0x0000077c: bgeu x29, x12, addr0x00000a0c
addr0x00000780: slli x20, x13, 15
addr0x00000784: ori x31, x15, 1532
addr0x00000788: sw x22, 137(x8)
addr0x0000078c: sh x24, -1395(x8)
addr0x00000790: lw x18, 951(x8)
addr0x00000794: sw x11, 893(x8)
addr0x00000798: lw x27, -3(x8)
addr0x0000079c: bltu x22, x23, addr0x00000774
addr0x000007a0: mul x17, x22, x16
addr0x000007a4: bltu x15, x13, addr0x00000804
addr0x000007a8: slli x12, x23, 20
addr0x000007ac: lw x28, 1676(x8)
addr0x000007b0: lw x17, 1672(x8)
addr0x000007b4: sw x7, 1655(x8)
addr0x000007b8: sw x7, 33(x8)
addr0x000007bc: lw x22, -1995(x8)
addr0x000007c0: lbu x9, -696(x8)
addr0x000007c4: lui x20, 230572
addr0x000007c8: addi x20, x29, -405
addr0x000007cc: slli x26, x6, 11
addr0x000007d0: sw x1, -1446(x8)
addr0x000007d4: sw x28, 978(x8)
addr0x000007d8: jal x5, addr0x00000378
addr0x000007dc: jal x18, addr0x000007d4
addr0x000007e0: jal x17, addr0x00000070
addr0x000007e4: lw x20, 1539(x8)
addr0x000007e8: lw x21, 1415(x8)
addr0x000007ec: jal x14, addr0x000003ec
addr0x000007f0: jal x25, addr0x00000730
addr0x000007f4: addi x10, x25, -929
addr0x000007f8: lw x20, -814(x8)
addr0x000007fc: bne x0, x5, addr0x000003ac
addr0x00000800: addi x18, x29, -348
addr0x00000804: bge x23, x6, addr0x00000a98
addr0x00000808: andi x1, x18, 1252
addr0x0000080c: beq x13, x20, addr0x000005b4
addr0x00000810: addi x25, x17, -1302
addr0x00000814: addi x23, x29, -1816
addr0x00000818: lui x11, 976650
addr0x0000081c: addi x9, x14, 1903
addr0x00000820: lw x6, 1938(x8)
addr0x00000824: jal x2, addr0x000002f4
addr0x00000828: blt x13, x19, addr0x000008c4
addr0x0000082c: addi x0, x4, 1463
addr0x00000830: addi x16, x11, 1106
addr0x00000834: lw x0, 1990(x8)
addr0x00000838: lw x18, -1142(x8)
addr0x0000083c: slli x22, x21, 14
addr0x00000840: jal x17, addr0x0000092c
addr0x00000844: lw x18, 1249(x8)
addr0x00000848: jal x21, addr0x00000540
addr0x0000084c: lw x5, 549(x8)
addr0x00000850: addi x1, x28, 945
addr0x00000854: sw x7, 1007(x8)
addr0x00000858: addi x31, x15, 1468
addr0x0000085c: bltu x18, x9, addr0x000004dc
addr0x00000860: srli x29, x13, 19
addr0x00000864: slli x5, x2, 23
addr0x00000868: sb x17, -1389(x8)
addr0x0000086c: sw x20, -1206(x8)
addr0x00000870: jal x17, addr0x0000095c
addr0x00000874: sw x25, 1359(x8)
addr0x00000878: sw x4, -1903(x8)
addr0x0000087c: andi x26, x19, 452
addr0x00000880: jal x20, addr0x00000750
addr0x00000884: jal x1, addr0x00000978
addr0x00000888: lw x7, 1304(x8)
addr0x0000088c: addi x23, x30, -1955
addr0x00000890: addi x23, x22, 1253
addr0x00000894: addi x9, x14, -1961
addr0x00000898: lw x0, -983(x8)
addr0x0000089c: beq x2, x18, addr0x0000027c
addr0x000008a0: addi x0, x22, 102
addr0x000008a4: sw x29, -289(x8)
addr0x000008a8: jal x11, addr0x0000059c
addr0x000008ac: sw x31, -86(x8)
addr0x000008b0: sw x14, 1374(x8)
addr0x000008b4: beq x22, x30, addr0x00000884
addr0x000008b8: mul x27, x16, x31
addr0x000008bc: lw x5, 1880(x8)
addr0x000008c0: addi x0, x25, -1636
addr0x000008c4: sw x27, 998(x8)
addr0x000008c8: jal x2, addr0x0000055c
addr0x000008cc: lw x21, -803(x8)
addr0x000008d0: jal x17, addr0x00000a14
addr0x000008d4: sw x18, 1044(x8)
addr0x000008d8: addi x31, x13, -1058
addr0x000008dc: blt x16, x15, addr0x00000b58
addr0x000008e0: add x13, x14, x15
addr0x000008e4: jal x18, addr0x000006fc
addr0x000008e8: bne x1, x19, addr0x000009f8
addr0x000008ec: lui x18, 305398
addr0x000008f0: slli x27, x21, 1
addr0x000008f4: srli x31, x0, 20
addr0x000008f8: bltu x5, x9, addr0x00000994
addr0x000008fc: sw x6, -1483(x8)
addr0x00000900: sw x23, -1715(x8)
addr0x00000904: lw x1, -1958(x8)
addr0x00000908: sw x25, 1360(x8)
addr0x0000090c: sub x29, x0, x10
addr0x00000910: add x27, x20, x13
addr0x00000914: jal x6, addr0x0000033c
addr0x00000918: bne x23, x25, addr0x00000798
addr0x0000091c: andi x11, x29, 1766
addr0x00000920: slli x9, x7, 6
addr0x00000924: lw x2, -1922(x8)
addr0x00000928: sw x27, 527(x8)
addr0x0000092c: lw x9, 277(x8)
addr0x00000930: lw x31, -336(x8)
addr0x00000934: bgeu x9, x23, addr0x0000099c
addr0x00000938: jal x20, addr0x00000174
addr0x0000093c: jal x12, addr0x000002b0
addr0x00000940: lw x2, -1204(x8)
addr0x00000944: jal x14, addr0x000002e4
addr0x00000948: addi x23, x26, -906
addr0x0000094c: sw x9, 1743(x8)
addr0x00000950: lw x4, 145(x8)
addr0x00000954: addi x13, x13, -1954
addr0x00000958: lw x2, 64(x8)
addr0x0000095c: lw x5, 341(x8)
addr0x00000960: lbu x7, 690(x8)
addr0x00000964: addi x14, x19, 1082
addr0x00000968: slli x11, x7, 20
addr0x0000096c: and x30, x12, x14
addr0x00000970: sh x25, 438(x8)
addr0x00000974: sb x7, 1618(x8)
addr0x00000978: sw x27, 673(x8)
addr0x0000097c: lw x18, -1802(x8)
addr0x00000980: addi x5, x13, 120
addr0x00000984: sw x13, 1435(x8)
addr0x00000988: sw x18, -1062(x8)
addr0x0000098c: jal x14, addr0x000007fc
addr0x00000990: lui x0, 1042536
addr0x00000994: addi x13, x27, -1549
addr0x00000998: jal x17, addr0x000004c4
addr0x0000099c: addi x18, x27, 30
addr0x000009a0: sw x12, 470(x8)
addr0x000009a4: sw x28, 962(x8)
addr0x000009a8: lw x24, 78(x8)
addr0x000009ac: sw x9, -1453(x8)
addr0x000009b0: addi x4, x10, 1364
addr0x000009b4: jal x7, addr0x00000b60
addr0x000009b8: lw x7, -1771(x8)
addr0x000009bc: blt x28, x12, addr0x00000550
addr0x000009c0: lw x13, -1337(x8)
addr0x000009c4: andi x31, x26, -873
addr0x000009c8: jal x2, addr0x00000268
addr0x000009cc: jal x6, addr0x000009f8
addr0x000009d0: bgeu x26, x7, addr0x00000644
addr0x000009d4: lw x21, -1646(x8)
addr0x000009d8: lw x21, 262(x8)
addr0x000009dc: sw x31, 70(x8)
addr0x000009e0: sw x25, -263(x8)
addr0x000009e4: lw x14, -1648(x8)
addr0x000009e8: add x27, x28, x31
addr0x000009ec: jal x10, addr0x00000550
addr0x000009f0: lw x6, -509(x8)
addr0x000009f4: sw x28, -997(x8)
addr0x000009f8: lw x14, -1016(x8)
addr0x000009fc: addi x19, x29, -1513
addr0x00000a00: jal x20, addr0x000007f0
addr0x00000a04: lw x16, 325(x8)
addr0x00000a08: lw x14, 1686(x8)
addr0x00000a0c: lw x30, -453(x8)
addr0x00000a10: bgeu x18, x19, addr0x00000358
addr0x00000a14: addi x29, x23, 1009
addr0x00000a18: jal x2, addr0x00000020
addr0x00000a1c: lw x18, -1445(x8)
addr0x00000a20: bne x13, x0, addr0x00000a90
addr0x00000a24: add x11, x7, x23
addr0x00000a28: sw x30, 1115(x8)
addr0x00000a2c: sw x7, 1914(x8)
addr0x00000a30: lw x15, -356(x8)
addr0x00000a34: addi x31, x9, 878
addr0x00000a38: jal x13, addr0x000002e8
addr0x00000a3c: lw x27, -1164(x8)
addr0x00000a40: add x23, x0, x5
addr0x00000a44: jal x7, addr0x000003dc
addr0x00000a48: lui x1, 360751
addr0x00000a4c: addi x11, x26, 1397
addr0x00000a50: jal x10, addr0x000002b0
addr0x00000a54: lw x4, -483(x8)
addr0x00000a58: addi x27, x0, -614
addr0x00000a5c: sw x18, 1781(x8)
addr0x00000a60: addi x27, x23, 1275
addr0x00000a64: lw x22, 52(x8)
addr0x00000a68: lw x29, 688(x8)
addr0x00000a6c: sw x26, 872(x8)
addr0x00000a70: sw x19, -439(x8)
addr0x00000a74: lw x21, -1785(x8)
addr0x00000a78: lw x23, 2037(x8)
addr0x00000a7c: lw x17, 540(x8)
addr0x00000a80: jal x26, addr0x000003a4
addr0x00000a84: lui x11, 58156
addr0x00000a88: or x29, x24, x30
addr0x00000a8c: sw x5, 1465(x8)
addr0x00000a90: sw x25, 787(x8)
addr0x00000a94: sw x29, -1507(x8)
addr0x00000a98: sw x30, -1878(x8)
addr0x00000a9c: addi x13, x17, -419
addr0x00000aa0: jal x7, addr0x0000045c
addr0x00000aa4: lw x20, 1833(x8)
addr0x00000aa8: jal x1, addr0x00000518
addr0x00000aac: lw x7, 1468(x8)
addr0x00000ab0: sw x7, 1582(x8)
addr0x00000ab4: sw x23, 1168(x8)
addr0x00000ab8: lw x21, 1662(x8)
addr0x00000abc: lw x31, -1234(x8)
addr0x00000ac0: sw x18, -1539(x8)
addr0x00000ac4: lw x27, 2039(x8)
addr0x00000ac8: beq x27, x14, addr0x00000640
addr0x00000acc: lw x21, 1222(x8)
addr0x00000ad0: lw x22, -680(x8)
addr0x00000ad4: addi x15, x16, -1205
addr0x00000ad8: sw x16, -180(x8)
addr0x00000adc: addi x12, x29, -815
addr0x00000ae0: addi x18, x20, 1912
addr0x00000ae4: jal x14, addr0x000008c4
addr0x00000ae8: lw x11, 151(x8)
addr0x00000aec: lw x14, 517(x8)
addr0x00000af0: addi x28, x24, 1256
addr0x00000af4: bltu x23, x31, addr0x000008cc
addr0x00000af8: slli x24, x31, 14
addr0x00000afc: addi x2, x31, -362
addr0x00000b00: bne x31, x31, addr0x00000b14
addr0x00000b04: addi x24, x28, -1848
addr0x00000b08: jal x15, addr0x0000040c
addr0x00000b0c: sw x1, 1060(x8)
addr0x00000b10: jal x11, addr0x00000784
addr0x00000b14: lui x7, 136705
addr0x00000b18: sw x10, -92(x8)
addr0x00000b1c: sw x31, 1095(x8)
addr0x00000b20: lw x2, -2039(x8)
addr0x00000b24: addi x19, x19, -176
addr0x00000b28: sw x27, -1675(x8)
addr0x00000b2c: jal x14, addr0x0000074c
addr0x00000b30: andi x19, x29, 1168
addr0x00000b34: lw x19, -80(x8)
addr0x00000b38: bgeu x5, x29, addr0x0000074c
addr0x00000b3c: lw x10, -1296(x8)
addr0x00000b40: lw x24, 883(x8)
addr0x00000b44: jal x17, addr0x00000184
addr0x00000b48: lw x22, -578(x8)
addr0x00000b4c: addi x6, x27, 1342
addr0x00000b50: sw x9, -1078(x8)
addr0x00000b54: addi x15, x6, 1960
addr0x00000b58: addi x10, x29, 1218
addr0x00000b5c: addi x31, x22, 1139
addr0x00000b60: slli x20, x13, 22
