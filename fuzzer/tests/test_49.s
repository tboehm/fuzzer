.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lui x20, 623414
addr0x0000001c: jal x24, addr0x00000198
addr0x00000020: slli x20, x20, 20
addr0x00000024: srli x21, x17, 2
addr0x00000028: lui x31, 204106
addr0x0000002c: addi x23, x0, 1640
addr0x00000030: beq x17, x26, addr0x00000540
addr0x00000034: jal x19, addr0x00000490
addr0x00000038: addi x22, x20, -1134
addr0x0000003c: jal x22, addr0x00000e90
addr0x00000040: sw x24, -1208(x8)
addr0x00000044: sw x1, -1658(x8)
addr0x00000048: jal x1, addr0x0000099c
addr0x0000004c: jal x24, addr0x000001b8
addr0x00000050: addi x19, x10, 1009
addr0x00000054: jal x27, addr0x00000418
addr0x00000058: jal x15, addr0x000006dc
addr0x0000005c: sw x22, -408(x8)
addr0x00000060: addi x14, x30, 1437
addr0x00000064: jal x15, addr0x00000b38
addr0x00000068: jal x13, addr0x00000abc
addr0x0000006c: lw x15, 1963(x8)
addr0x00000070: bgeu x20, x10, addr0x00000240
addr0x00000074: andi x0, x13, 1753
addr0x00000078: addi x23, x31, -327
addr0x0000007c: jal x15, addr0x000005f4
addr0x00000080: lw x17, 395(x8)
addr0x00000084: sw x23, 1270(x8)
addr0x00000088: sw x0, 1305(x8)
addr0x0000008c: addi x19, x21, 749
addr0x00000090: addi x30, x16, -761
addr0x00000094: jal x29, addr0x00000b38
addr0x00000098: lw x13, 1402(x8)
addr0x0000009c: sw x10, -642(x8)
addr0x000000a0: addi x22, x27, -1820
addr0x000000a4: jal x0, addr0x00000748
addr0x000000a8: lw x28, -259(x8)
addr0x000000ac: lw x14, 1148(x8)
addr0x000000b0: lw x28, -757(x8)
addr0x000000b4: lw x5, -502(x8)
addr0x000000b8: andi x0, x20, 1038
addr0x000000bc: bltu x31, x7, addr0x000005f8
addr0x000000c0: lbu x31, -1812(x8)
addr0x000000c4: xori x23, x20, 409
addr0x000000c8: andi x20, x9, -5
addr0x000000cc: addi x27, x29, -917
addr0x000000d0: bgeu x25, x30, addr0x00000288
addr0x000000d4: lui x10, 1042371
addr0x000000d8: addi x25, x30, 184
addr0x000000dc: add x31, x7, x21
addr0x000000e0: jal x2, addr0x000001ec
addr0x000000e4: jal x14, addr0x00000188
addr0x000000e8: lw x29, 393(x8)
addr0x000000ec: lw x1, -1376(x8)
addr0x000000f0: lw x4, 1783(x8)
addr0x000000f4: sw x17, -1553(x8)
addr0x000000f8: jal x4, addr0x00000eac
addr0x000000fc: jal x17, addr0x00000164
addr0x00000100: lw x0, -1972(x8)
addr0x00000104: addi x5, x0, -667
addr0x00000108: beq x23, x23, addr0x00000278
addr0x0000010c: addi x0, x12, 241
addr0x00000110: lw x6, -1376(x8)
addr0x00000114: lui x16, 157391
addr0x00000118: addi x13, x30, -995
addr0x0000011c: addi x31, x18, -1664
addr0x00000120: sw x19, -933(x8)
addr0x00000124: sw x2, 1716(x8)
addr0x00000128: sw x29, 1261(x8)
addr0x0000012c: jal x0, addr0x00000870
addr0x00000130: jal x12, addr0x00000134
addr0x00000134: sb x19, 1054(x8)
addr0x00000138: sb x13, -2045(x8)
addr0x0000013c: addi x11, x26, -1397
addr0x00000140: lw x10, -62(x8)
addr0x00000144: lw x30, 280(x8)
addr0x00000148: lw x25, 105(x8)
addr0x0000014c: sub x28, x29, x22
addr0x00000150: srai x13, x25, 5
addr0x00000154: addi x7, x30, 1647
addr0x00000158: addi x24, x25, 1917
addr0x0000015c: jal x31, addr0x0000034c
addr0x00000160: sw x19, -1504(x8)
addr0x00000164: lw x26, 866(x8)
addr0x00000168: lw x0, 1427(x8)
addr0x0000016c: addi x0, x31, -1645
addr0x00000170: bne x13, x21, addr0x00000644
addr0x00000174: bge x12, x29, addr0x00000180
addr0x00000178: addi x0, x27, 1448
addr0x0000017c: sw x22, 1872(x8)
addr0x00000180: sw x7, 364(x8)
addr0x00000184: addi x17, x17, 1546
addr0x00000188: jal x25, addr0x00000500
addr0x0000018c: lw x18, -110(x8)
addr0x00000190: slli x19, x28, 21
addr0x00000194: srli x5, x9, 25
addr0x00000198: lw x28, 1439(x8)
addr0x0000019c: addi x9, x0, -1644
addr0x000001a0: sw x21, -1396(x8)
addr0x000001a4: addi x19, x27, 1865
addr0x000001a8: lw x26, 1488(x8)
addr0x000001ac: lw x30, 1988(x8)
addr0x000001b0: lw x12, -1128(x8)
addr0x000001b4: lw x0, -1529(x8)
addr0x000001b8: addi x2, x19, 101
addr0x000001bc: jal x24, addr0x00000dec
addr0x000001c0: lui x18, 639556
addr0x000001c4: addi x10, x18, 513
addr0x000001c8: sw x19, -494(x8)
addr0x000001cc: sw x5, -2024(x8)
addr0x000001d0: lw x9, -92(x8)
addr0x000001d4: bne x31, x31, addr0x000002c4
addr0x000001d8: jal x18, addr0x00000150
addr0x000001dc: lw x0, 1859(x8)
addr0x000001e0: jal x23, addr0x00000e58
addr0x000001e4: lw x7, -291(x8)
addr0x000001e8: lw x11, 297(x8)
addr0x000001ec: addi x18, x0, 1178
addr0x000001f0: addi x23, x22, 259
addr0x000001f4: jal x21, addr0x00000194
addr0x000001f8: jal x2, addr0x00000b6c
addr0x000001fc: lw x13, -1091(x8)
addr0x00000200: lw x20, -1527(x8)
addr0x00000204: andi x20, x23, -499
addr0x00000208: or x29, x21, x7
addr0x0000020c: sw x7, -1912(x8)
addr0x00000210: addi x13, x7, 1329
addr0x00000214: sw x9, -1589(x8)
addr0x00000218: sw x19, -30(x8)
addr0x0000021c: sw x5, -829(x8)
addr0x00000220: jal x20, addr0x000002ec
addr0x00000224: jal x22, addr0x00000bb0
addr0x00000228: lui x29, 998171
addr0x0000022c: addi x13, x13, -1932
addr0x00000230: sw x23, 1476(x8)
addr0x00000234: lw x29, -526(x8)
addr0x00000238: lw x16, 1751(x8)
addr0x0000023c: addi x26, x21, 1839
addr0x00000240: bge x13, x13, addr0x00000470
addr0x00000244: andi x28, x9, 1769
addr0x00000248: lui x11, 331165
addr0x0000024c: addi x27, x17, -1331
addr0x00000250: lhu x27, 1649(x8)
addr0x00000254: lbu x18, 1083(x8)
addr0x00000258: sb x15, 1593(x8)
addr0x0000025c: addi x2, x29, 569
addr0x00000260: addi x16, x28, 818
addr0x00000264: sw x20, 0(x8)
addr0x00000268: jal x17, addr0x00000a64
addr0x0000026c: addi x23, x1, -1300
addr0x00000270: jal x29, addr0x000002b4
addr0x00000274: lui x20, 939397
addr0x00000278: lui x2, 991548
addr0x0000027c: sw x17, 131(x8)
addr0x00000280: sw x31, -1334(x8)
addr0x00000284: lw x31, 1083(x8)
addr0x00000288: addi x23, x17, 288
addr0x0000028c: sw x0, -1108(x8)
addr0x00000290: sw x21, 1134(x8)
addr0x00000294: sw x29, -431(x8)
addr0x00000298: beq x7, x22, addr0x00000518
addr0x0000029c: addi x17, x14, 1551
addr0x000002a0: lw x11, -288(x8)
addr0x000002a4: beq x18, x9, addr0x0000069c
addr0x000002a8: lui x27, 857389
addr0x000002ac: sw x26, -837(x8)
addr0x000002b0: lw x31, -195(x8)
addr0x000002b4: sw x11, 1593(x8)
addr0x000002b8: addi x18, x12, 1002
addr0x000002bc: addi x9, x15, -718
addr0x000002c0: sw x27, -656(x8)
addr0x000002c4: lw x17, -1209(x8)
addr0x000002c8: lw x20, -851(x8)
addr0x000002cc: sb x13, 1711(x8)
addr0x000002d0: addi x19, x17, 905
addr0x000002d4: lui x18, 901396
addr0x000002d8: addi x26, x23, -1601
addr0x000002dc: sw x6, -1672(x8)
addr0x000002e0: add x27, x23, x4
addr0x000002e4: jal x0, addr0x00000c78
addr0x000002e8: jal x5, addr0x000005f4
addr0x000002ec: lbu x28, -797(x8)
addr0x000002f0: sb x31, -1142(x8)
addr0x000002f4: sw x13, 1823(x8)
addr0x000002f8: addi x5, x27, -290
addr0x000002fc: addi x26, x22, 75
addr0x00000300: jal x14, addr0x0000052c
addr0x00000304: lw x31, 33(x8)
addr0x00000308: sb x7, -700(x8)
addr0x0000030c: lw x27, -24(x8)
addr0x00000310: addi x17, x16, -2002
addr0x00000314: jal x23, addr0x000008f4
addr0x00000318: addi x23, x20, 1567
addr0x0000031c: jal x12, addr0x00000e24
addr0x00000320: add x21, x17, x29
addr0x00000324: lw x31, -493(x8)
addr0x00000328: lw x22, 30(x8)
addr0x0000032c: lw x18, 1659(x8)
addr0x00000330: addi x20, x5, -580
addr0x00000334: lui x19, 718624
addr0x00000338: addi x0, x2, -1818
addr0x0000033c: lw x18, -416(x8)
addr0x00000340: lw x4, -60(x8)
addr0x00000344: jal x18, addr0x00000888
addr0x00000348: addi x17, x30, 98
addr0x0000034c: lw x7, -1744(x8)
addr0x00000350: bgeu x0, x25, addr0x00000950
addr0x00000354: slli x17, x19, 13
addr0x00000358: add x4, x28, x0
addr0x0000035c: bltu x23, x23, addr0x00000074
addr0x00000360: lui x11, 658161
addr0x00000364: addi x7, x29, -265
addr0x00000368: sw x4, -717(x8)
addr0x0000036c: sw x28, -109(x8)
addr0x00000370: addi x27, x29, 720
addr0x00000374: sw x15, -301(x8)
addr0x00000378: addi x19, x14, 1585
addr0x0000037c: addi x17, x26, 1568
addr0x00000380: andi x26, x13, -49
addr0x00000384: jal x0, addr0x00000d20
addr0x00000388: bne x18, x12, addr0x00000b10
addr0x0000038c: lw x19, -466(x8)
addr0x00000390: jal x20, addr0x00000884
addr0x00000394: lbu x27, 457(x8)
addr0x00000398: andi x6, x20, -153
addr0x0000039c: bne x27, x9, addr0x000008cc
addr0x000003a0: jal x22, addr0x000005e4
addr0x000003a4: slli x29, x21, 24
addr0x000003a8: addi x5, x19, -1111
addr0x000003ac: jal x17, addr0x000009a0
addr0x000003b0: jalr x0, 896(x3)
addr0x000003b4: lw x18, -48(x8)
addr0x000003b8: lui x17, 670217
addr0x000003bc: sw x6, 1615(x8)
addr0x000003c0: jal x19, addr0x00000a30
addr0x000003c4: sh x2, -706(x8)
addr0x000003c8: beq x29, x0, addr0x000000c0
addr0x000003cc: lw x18, 538(x8)
addr0x000003d0: sw x0, 1642(x8)
addr0x000003d4: sw x7, 594(x8)
addr0x000003d8: sw x28, 1511(x8)
addr0x000003dc: sw x23, -821(x8)
addr0x000003e0: jal x0, addr0x00000e94
addr0x000003e4: addi x29, x12, -1787
addr0x000003e8: sw x7, -1544(x8)
addr0x000003ec: sw x28, -568(x8)
addr0x000003f0: lw x12, 334(x8)
addr0x000003f4: lw x24, -1323(x8)
addr0x000003f8: sw x2, -1261(x8)
addr0x000003fc: lbu x13, 1288(x8)
addr0x00000400: xori x23, x13, 1139
addr0x00000404: andi x9, x30, 169
addr0x00000408: addi x16, x30, 1401
addr0x0000040c: sw x13, -412(x8)
addr0x00000410: bne x23, x29, addr0x000009f4
addr0x00000414: andi x13, x10, 1005
addr0x00000418: jal x13, addr0x00000a28
addr0x0000041c: lw x18, 175(x8)
addr0x00000420: sw x31, -814(x8)
addr0x00000424: lw x20, -878(x8)
addr0x00000428: add x14, x23, x7
addr0x0000042c: add x22, x11, x17
addr0x00000430: jal x7, addr0x00000370
addr0x00000434: lw x15, -823(x8)
addr0x00000438: addi x25, x27, -1933
addr0x0000043c: lui x23, 225466
addr0x00000440: addi x22, x19, -1073
addr0x00000444: sw x28, -474(x8)
addr0x00000448: srli x26, x27, 18
addr0x0000044c: beq x31, x24, addr0x00000bc0
addr0x00000450: bne x30, x17, addr0x00000b78
addr0x00000454: addi x12, x13, 318
addr0x00000458: add x9, x18, x7
addr0x0000045c: add x18, x19, x31
addr0x00000460: lw x23, -1979(x8)
addr0x00000464: sw x15, 1138(x8)
addr0x00000468: sw x31, 1716(x8)
addr0x0000046c: jal x31, addr0x00000130
addr0x00000470: addi x7, x6, -1802
addr0x00000474: lui x9, 965813
addr0x00000478: or x25, x9, x5
addr0x0000047c: lbu x5, 378(x8)
addr0x00000480: lw x19, -1080(x8)
addr0x00000484: lw x24, -540(x8)
addr0x00000488: lw x6, 1933(x8)
addr0x0000048c: lw x31, -794(x8)
addr0x00000490: jal x23, addr0x000008a0
addr0x00000494: beq x29, x4, addr0x00000bf4
addr0x00000498: slli x17, x30, 19
addr0x0000049c: lbu x1, 1637(x8)
addr0x000004a0: bgeu x20, x4, addr0x000008ec
addr0x000004a4: addi x14, x7, -1016
addr0x000004a8: jal x17, addr0x000000b8
addr0x000004ac: lw x26, 1468(x8)
addr0x000004b0: add x31, x23, x9
addr0x000004b4: addi x7, x5, -1676
addr0x000004b8: sw x11, -1717(x8)
addr0x000004bc: sw x20, 1622(x8)
addr0x000004c0: sw x1, 1444(x8)
addr0x000004c4: jal x5, addr0x00000828
addr0x000004c8: jal x26, addr0x00000d20
addr0x000004cc: jal x18, addr0x00000874
addr0x000004d0: lw x22, -1778(x8)
addr0x000004d4: lw x7, 2039(x8)
addr0x000004d8: lui x29, 662699
addr0x000004dc: jal x31, addr0x00000a50
addr0x000004e0: lw x29, -1746(x8)
addr0x000004e4: addi x23, x7, 1839
addr0x000004e8: add x10, x23, x27
addr0x000004ec: sw x20, -1247(x8)
addr0x000004f0: lw x13, 313(x8)
addr0x000004f4: lw x20, -474(x8)
addr0x000004f8: addi x23, x20, 1142
addr0x000004fc: lui x28, 890347
addr0x00000500: addi x28, x19, 1681
addr0x00000504: jal x22, addr0x000001f0
addr0x00000508: lw x0, 957(x8)
addr0x0000050c: lw x15, 1587(x8)
addr0x00000510: lw x7, -50(x8)
addr0x00000514: sw x19, -574(x8)
addr0x00000518: addi x1, x1, 740
addr0x0000051c: jal x30, addr0x00000704
addr0x00000520: addi x17, x30, -689
addr0x00000524: jal x18, addr0x00000878
addr0x00000528: sub x23, x28, x25
addr0x0000052c: sw x28, -466(x8)
addr0x00000530: jal x27, addr0x00000e48
addr0x00000534: sw x26, 1882(x8)
addr0x00000538: sw x23, -1321(x8)
addr0x0000053c: jal x25, addr0x00000328
addr0x00000540: beq x0, x15, addr0x00000c24
addr0x00000544: xori x17, x31, 268
addr0x00000548: andi x7, x29, -1524
addr0x0000054c: addi x5, x31, -1015
addr0x00000550: bge x9, x10, addr0x00000bd4
addr0x00000554: beq x29, x9, addr0x00000ce4
addr0x00000558: lw x14, -638(x8)
addr0x0000055c: lw x31, -1241(x8)
addr0x00000560: bgeu x11, x12, addr0x000000d8
addr0x00000564: addi x22, x23, 1209
addr0x00000568: jal x29, addr0x000007a0
addr0x0000056c: bne x29, x0, addr0x00000ae4
addr0x00000570: addi x14, x28, -861
addr0x00000574: addi x12, x0, -366
addr0x00000578: lw x20, -1587(x8)
addr0x0000057c: addi x22, x31, -669
addr0x00000580: addi x13, x31, -1620
addr0x00000584: addi x10, x19, -1121
addr0x00000588: sw x31, -1932(x8)
addr0x0000058c: sw x18, -1345(x8)
addr0x00000590: sw x5, 1430(x8)
addr0x00000594: addi x28, x26, -1628
addr0x00000598: sw x9, 391(x8)
addr0x0000059c: jal x7, addr0x00000030
addr0x000005a0: jal x29, addr0x00000220
addr0x000005a4: sw x23, -1582(x8)
addr0x000005a8: andi x24, x2, 1966
addr0x000005ac: lw x10, 2027(x8)
addr0x000005b0: addi x19, x10, -591
addr0x000005b4: addi x5, x2, 1988
addr0x000005b8: add x31, x13, x22
addr0x000005bc: lw x10, 1938(x8)
addr0x000005c0: andi x30, x17, -1942
addr0x000005c4: lw x12, 577(x8)
addr0x000005c8: addi x24, x9, 1245
addr0x000005cc: lw x29, -535(x8)
addr0x000005d0: bgeu x28, x22, addr0x00000148
addr0x000005d4: addi x0, x14, 1677
addr0x000005d8: jal x26, addr0x00000d6c
addr0x000005dc: auipc x28, 556079
addr0x000005e0: addi x31, x26, 504
addr0x000005e4: sw x31, -19(x8)
addr0x000005e8: sw x19, 1382(x8)
addr0x000005ec: sw x13, -1887(x8)
addr0x000005f0: sw x23, -1578(x8)
addr0x000005f4: sb x18, -706(x8)
addr0x000005f8: lw x14, 1322(x8)
addr0x000005fc: lw x2, 1453(x8)
addr0x00000600: sw x10, -804(x8)
addr0x00000604: sw x26, 901(x8)
addr0x00000608: or x12, x12, x29
addr0x0000060c: sh x13, -397(x8)
addr0x00000610: addi x13, x22, 263
addr0x00000614: sw x22, -1229(x8)
addr0x00000618: jal x25, addr0x00000ed4
addr0x0000061c: andi x27, x31, 1660
addr0x00000620: lui x20, 507327
addr0x00000624: addi x31, x20, -804
addr0x00000628: sw x7, 107(x8)
addr0x0000062c: lw x18, 653(x8)
addr0x00000630: addi x24, x30, 1849
addr0x00000634: addi x28, x20, 959
addr0x00000638: sw x9, -992(x8)
addr0x0000063c: addi x9, x10, 1379
addr0x00000640: jal x30, addr0x00000244
addr0x00000644: bltu x22, x23, addr0x00000590
addr0x00000648: sub x29, x13, x17
addr0x0000064c: jal x6, addr0x0000048c
addr0x00000650: addi x5, x17, -460
addr0x00000654: sw x5, -994(x8)
addr0x00000658: lw x7, 178(x8)
addr0x0000065c: lw x28, -989(x8)
addr0x00000660: lw x15, -1084(x8)
addr0x00000664: addi x5, x14, 1817
addr0x00000668: sw x19, 1616(x8)
addr0x0000066c: sw x31, 153(x8)
addr0x00000670: sw x10, 1947(x8)
addr0x00000674: jal x31, addr0x000002f4
addr0x00000678: lw x28, -1427(x8)
addr0x0000067c: addi x5, x29, -969
addr0x00000680: sh x13, 2001(x8)
addr0x00000684: jal x14, addr0x00000734
addr0x00000688: addi x12, x20, 668
addr0x0000068c: sw x16, 1861(x8)
addr0x00000690: jal x26, addr0x0000030c
addr0x00000694: lw x20, -1790(x8)
addr0x00000698: jal x18, addr0x000006a8
addr0x0000069c: sw x14, 1386(x8)
addr0x000006a0: jal x9, addr0x000001c4
addr0x000006a4: lw x13, 738(x8)
addr0x000006a8: addi x14, x9, 748
addr0x000006ac: lw x19, 675(x8)
addr0x000006b0: sw x27, -395(x8)
addr0x000006b4: lw x15, -1004(x8)
addr0x000006b8: add x27, x24, x30
addr0x000006bc: lbu x9, 737(x8)
addr0x000006c0: jal x13, addr0x000004d8
addr0x000006c4: lw x19, -881(x8)
addr0x000006c8: lw x22, -1996(x8)
addr0x000006cc: lbu x6, -860(x8)
addr0x000006d0: sb x27, 1408(x8)
addr0x000006d4: lbu x31, 279(x8)
addr0x000006d8: beq x4, x22, addr0x0000038c
addr0x000006dc: srai x9, x17, 29
addr0x000006e0: slli x11, x14, 24
addr0x000006e4: lw x0, -1293(x8)
addr0x000006e8: sw x17, -1354(x8)
addr0x000006ec: jalr x0, 288(x3)
addr0x000006f0: jal x19, addr0x00000538
addr0x000006f4: jal x13, addr0x00000388
addr0x000006f8: jal x14, addr0x00000c6c
addr0x000006fc: lui x9, 232273
addr0x00000700: addi x28, x13, -1245
addr0x00000704: sw x23, -389(x8)
addr0x00000708: lw x27, -2030(x8)
addr0x0000070c: lw x7, 1064(x8)
addr0x00000710: addi x7, x31, 163
addr0x00000714: lw x27, 701(x8)
addr0x00000718: lw x27, -1573(x8)
addr0x0000071c: addi x5, x7, 13
addr0x00000720: jal x31, addr0x000008fc
addr0x00000724: lw x27, -978(x8)
addr0x00000728: sub x29, x17, x6
addr0x0000072c: add x11, x23, x11
addr0x00000730: lui x29, 621545
addr0x00000734: sw x14, -1717(x8)
addr0x00000738: sw x9, 1637(x8)
addr0x0000073c: lw x21, -923(x8)
addr0x00000740: jal x24, addr0x00000798
addr0x00000744: addi x19, x11, -421
addr0x00000748: jal x18, addr0x000003e8
addr0x0000074c: jal x17, addr0x0000043c
addr0x00000750: lw x14, -846(x8)
addr0x00000754: jal x24, addr0x00000784
addr0x00000758: add x12, x23, x13
addr0x0000075c: jal x15, addr0x000002a8
addr0x00000760: lbu x14, 1816(x8)
addr0x00000764: sw x2, -1127(x8)
addr0x00000768: addi x27, x21, 758
addr0x0000076c: jal x9, addr0x00000e84
addr0x00000770: lw x29, 1888(x8)
addr0x00000774: sw x23, 64(x8)
addr0x00000778: sw x13, -469(x8)
addr0x0000077c: sw x25, 1743(x8)
addr0x00000780: jal x7, addr0x00000304
addr0x00000784: jal x16, addr0x000007f0
addr0x00000788: lw x16, -693(x8)
addr0x0000078c: addi x28, x25, -1312
addr0x00000790: addi x6, x23, 388
addr0x00000794: addi x23, x21, 1655
addr0x00000798: jal x28, addr0x00000eb4
addr0x0000079c: lw x18, 1667(x8)
addr0x000007a0: sub x31, x28, x22
addr0x000007a4: add x16, x14, x29
addr0x000007a8: jal x23, addr0x000006fc
addr0x000007ac: lw x18, -1460(x8)
addr0x000007b0: lw x19, 922(x8)
addr0x000007b4: jal x21, addr0x00000630
addr0x000007b8: lui x22, 94758
addr0x000007bc: sw x5, 309(x8)
addr0x000007c0: sw x9, -1043(x8)
addr0x000007c4: jal x5, addr0x00000d8c
addr0x000007c8: lw x7, -861(x8)
addr0x000007cc: lw x26, 1156(x8)
addr0x000007d0: lw x18, -776(x8)
addr0x000007d4: lw x17, -16(x8)
addr0x000007d8: addi x29, x14, 266
addr0x000007dc: lw x19, -1489(x8)
addr0x000007e0: addi x1, x30, -1449
addr0x000007e4: addi x22, x27, 334
addr0x000007e8: jal x9, addr0x00000bb0
addr0x000007ec: bgeu x28, x5, addr0x00000028
addr0x000007f0: lw x14, 689(x8)
addr0x000007f4: lw x18, -347(x8)
addr0x000007f8: addi x0, x4, 116
addr0x000007fc: jal x13, addr0x000006ec
addr0x00000800: lw x17, 1552(x8)
addr0x00000804: srli x23, x19, 28
addr0x00000808: lw x18, -314(x8)
addr0x0000080c: lw x14, 962(x8)
addr0x00000810: andi x27, x7, -708
addr0x00000814: sb x19, 1940(x8)
addr0x00000818: sb x23, 295(x8)
addr0x0000081c: sw x31, -1303(x8)
addr0x00000820: jal x22, addr0x000007e4
addr0x00000824: jal x20, addr0x00000d70
addr0x00000828: lw x27, -1093(x8)
addr0x0000082c: lw x25, 663(x8)
addr0x00000830: lw x4, 551(x8)
addr0x00000834: addi x0, x0, 244
addr0x00000838: lbu x18, 1552(x8)
addr0x0000083c: add x13, x29, x5
addr0x00000840: lbu x27, 1306(x8)
addr0x00000844: slli x28, x2, 12
addr0x00000848: add x31, x0, x5
addr0x0000084c: sw x26, 1162(x8)
addr0x00000850: addi x17, x2, -424
addr0x00000854: lw x27, -1888(x8)
addr0x00000858: addi x26, x23, -61
addr0x0000085c: add x18, x13, x23
addr0x00000860: jal x19, addr0x000007f8
addr0x00000864: xori x7, x10, -344
addr0x00000868: sh x4, 1920(x8)
addr0x0000086c: sh x24, 816(x8)
addr0x00000870: bne x25, x26, addr0x000006cc
addr0x00000874: lw x23, -610(x8)
addr0x00000878: addi x27, x18, -1198
addr0x0000087c: beq x2, x4, addr0x00000ec8
addr0x00000880: lw x26, 1930(x8)
addr0x00000884: jal x12, addr0x00000efc
addr0x00000888: lui x7, 913666
addr0x0000088c: addi x7, x13, -979
addr0x00000890: jal x2, addr0x00000d04
addr0x00000894: beq x4, x20, addr0x000002b4
addr0x00000898: lw x15, 570(x8)
addr0x0000089c: lw x9, -258(x8)
addr0x000008a0: lw x0, 746(x8)
addr0x000008a4: addi x31, x13, 626
addr0x000008a8: lw x18, -797(x8)
addr0x000008ac: addi x10, x27, -1238
addr0x000008b0: sw x9, 225(x8)
addr0x000008b4: jal x22, addr0x00000b88
addr0x000008b8: addi x31, x28, -2016
addr0x000008bc: sw x19, -1962(x8)
addr0x000008c0: lui x19, 420835
addr0x000008c4: bgeu x17, x17, addr0x000000cc
addr0x000008c8: lui x20, 629972
addr0x000008cc: addi x26, x18, -1989
addr0x000008d0: jal x4, addr0x0000039c
addr0x000008d4: lh x18, -625(x8)
addr0x000008d8: srai x14, x13, 22
addr0x000008dc: add x17, x29, x11
addr0x000008e0: add x14, x16, x7
addr0x000008e4: jal x18, addr0x00000e2c
addr0x000008e8: jal x7, addr0x000007c0
addr0x000008ec: lui x14, 213878
addr0x000008f0: addi x20, x6, -1687
addr0x000008f4: beq x18, x19, addr0x00000260
addr0x000008f8: addi x12, x6, 1686
addr0x000008fc: lui x24, 107530
addr0x00000900: addi x17, x14, -1392
addr0x00000904: addi x27, x7, 40
addr0x00000908: jal x20, addr0x00000604
addr0x0000090c: lw x27, -1260(x8)
addr0x00000910: srai x30, x0, 4
addr0x00000914: lw x25, -631(x8)
addr0x00000918: lw x23, -105(x8)
addr0x0000091c: jal x11, addr0x00000cdc
addr0x00000920: sw x0, -1832(x8)
addr0x00000924: lw x31, -1671(x8)
addr0x00000928: addi x15, x31, -1039
addr0x0000092c: bge x26, x17, addr0x000007a4
addr0x00000930: ori x9, x10, 100
addr0x00000934: lw x19, -1125(x8)
addr0x00000938: lui x1, 121303
addr0x0000093c: addi x10, x21, 1705
addr0x00000940: beq x6, x14, addr0x0000040c
addr0x00000944: andi x1, x18, 479
addr0x00000948: addi x23, x25, -1750
addr0x0000094c: sw x2, 1612(x8)
addr0x00000950: sw x9, -662(x8)
addr0x00000954: sw x20, -1444(x8)
addr0x00000958: sw x14, 979(x8)
addr0x0000095c: jal x0, addr0x000008ac
addr0x00000960: addi x30, x0, 1279
addr0x00000964: lw x16, -1567(x8)
addr0x00000968: ori x9, x0, 1376
addr0x0000096c: sw x19, 738(x8)
addr0x00000970: lw x23, 1747(x8)
addr0x00000974: add x16, x5, x29
addr0x00000978: jal x16, addr0x00000f14
addr0x0000097c: addi x14, x29, 1861
addr0x00000980: sub x19, x17, x23
addr0x00000984: lw x9, 1047(x8)
addr0x00000988: lw x16, 871(x8)
addr0x0000098c: lw x13, 1553(x8)
addr0x00000990: lw x23, -602(x8)
addr0x00000994: lw x4, -1623(x8)
addr0x00000998: addi x2, x19, -861
addr0x0000099c: bgeu x0, x27, addr0x00000850
addr0x000009a0: slli x12, x30, 17
addr0x000009a4: srli x20, x16, 7
addr0x000009a8: slli x7, x25, 5
addr0x000009ac: slli x7, x7, 13
addr0x000009b0: andi x7, x23, -1659
addr0x000009b4: add x30, x23, x23
addr0x000009b8: sw x22, 911(x8)
addr0x000009bc: jal x12, addr0x00000654
addr0x000009c0: lw x17, -1669(x8)
addr0x000009c4: lw x7, -383(x8)
addr0x000009c8: lw x17, 125(x8)
addr0x000009cc: sw x22, -269(x8)
addr0x000009d0: lw x5, 901(x8)
addr0x000009d4: lw x18, -357(x8)
addr0x000009d8: lbu x24, 1461(x8)
addr0x000009dc: andi x2, x0, -347
addr0x000009e0: lw x5, 622(x8)
addr0x000009e4: beq x9, x7, addr0x000007d8
addr0x000009e8: lbu x27, -1581(x8)
addr0x000009ec: lw x9, 18(x8)
addr0x000009f0: lw x11, -621(x8)
addr0x000009f4: addi x30, x13, 1051
addr0x000009f8: sw x30, -1210(x8)
addr0x000009fc: lw x30, -206(x8)
addr0x00000a00: bne x26, x18, addr0x00000298
addr0x00000a04: bne x30, x11, addr0x00000af4
addr0x00000a08: jal x26, addr0x000004fc
addr0x00000a0c: jal x12, addr0x000008d4
addr0x00000a10: lw x25, 1983(x8)
addr0x00000a14: jal x23, addr0x000000ac
addr0x00000a18: lui x27, 51861
addr0x00000a1c: lui x18, 907435
addr0x00000a20: addi x19, x20, 1289
addr0x00000a24: sw x15, -1172(x8)
addr0x00000a28: addi x29, x1, -832
addr0x00000a2c: sw x11, 737(x8)
addr0x00000a30: addi x20, x29, 1507
addr0x00000a34: sw x5, 739(x8)
addr0x00000a38: lw x0, 1767(x8)
addr0x00000a3c: sub x19, x28, x18
addr0x00000a40: addi x11, x28, -1981
addr0x00000a44: bne x0, x9, addr0x0000038c
addr0x00000a48: addi x1, x18, -1587
addr0x00000a4c: jal x5, addr0x00000ac0
addr0x00000a50: lw x18, 498(x8)
addr0x00000a54: lw x25, -704(x8)
addr0x00000a58: lw x14, 1718(x8)
addr0x00000a5c: sw x0, -1025(x8)
addr0x00000a60: jal x4, addr0x00000458
addr0x00000a64: addi x19, x4, -516
addr0x00000a68: jal x9, addr0x00000c7c
addr0x00000a6c: bgeu x30, x0, addr0x00000b68
addr0x00000a70: lw x7, -564(x8)
addr0x00000a74: jal x13, addr0x00000320
addr0x00000a78: bne x22, x24, addr0x0000064c
addr0x00000a7c: lw x29, 8(x8)
addr0x00000a80: sw x29, -1399(x8)
addr0x00000a84: sw x21, -809(x8)
addr0x00000a88: jal x18, addr0x0000019c
addr0x00000a8c: jal x13, addr0x00000b04
addr0x00000a90: addi x18, x10, -206
addr0x00000a94: bgeu x31, x0, addr0x0000080c
addr0x00000a98: lui x14, 897921
addr0x00000a9c: addi x24, x31, -24
addr0x00000aa0: addi x29, x9, 887
addr0x00000aa4: sw x21, -107(x8)
addr0x00000aa8: jal x22, addr0x000004d4
addr0x00000aac: bgeu x22, x20, addr0x000004f8
addr0x00000ab0: lw x12, 1139(x8)
addr0x00000ab4: xor x1, x7, x20
addr0x00000ab8: sw x17, -1253(x8)
addr0x00000abc: jal x2, addr0x000000f0
addr0x00000ac0: lw x18, -1248(x8)
addr0x00000ac4: lw x10, 1679(x8)
addr0x00000ac8: addi x4, x6, -233
addr0x00000acc: sw x0, 166(x8)
addr0x00000ad0: sw x23, -1854(x8)
addr0x00000ad4: sw x9, 617(x8)
addr0x00000ad8: sw x11, 1498(x8)
addr0x00000adc: sw x0, 1150(x8)
addr0x00000ae0: sw x22, -1836(x8)
addr0x00000ae4: jal x23, addr0x00000028
addr0x00000ae8: lw x19, -1428(x8)
addr0x00000aec: addi x31, x29, 1837
addr0x00000af0: jal x28, addr0x0000078c
addr0x00000af4: jal x13, addr0x00000e68
addr0x00000af8: sw x31, -1925(x8)
addr0x00000afc: sw x23, -1684(x8)
addr0x00000b00: sw x18, -584(x8)
addr0x00000b04: sw x1, 1984(x8)
addr0x00000b08: addi x29, x0, 937
addr0x00000b0c: beq x4, x9, addr0x00000cbc
addr0x00000b10: jal x26, addr0x00000a14
addr0x00000b14: sw x7, 272(x8)
addr0x00000b18: sw x28, 390(x8)
addr0x00000b1c: lw x27, -1483(x8)
addr0x00000b20: lw x14, 772(x8)
addr0x00000b24: addi x13, x31, -1927
addr0x00000b28: sw x19, -2041(x8)
addr0x00000b2c: sw x0, -1442(x8)
addr0x00000b30: lui x24, 757903
addr0x00000b34: addi x11, x20, 1126
addr0x00000b38: sw x18, -96(x8)
addr0x00000b3c: jal x23, addr0x000003d4
addr0x00000b40: sub x26, x13, x18
addr0x00000b44: lw x21, 1976(x8)
addr0x00000b48: add x31, x23, x1
addr0x00000b4c: addi x17, x14, 1545
addr0x00000b50: jal x28, addr0x00000a24
addr0x00000b54: lw x5, 738(x8)
addr0x00000b58: lw x17, -594(x8)
addr0x00000b5c: lw x20, 1000(x8)
addr0x00000b60: sb x31, -315(x8)
addr0x00000b64: sw x29, -1510(x8)
addr0x00000b68: auipc x11, 226712
addr0x00000b6c: addi x18, x23, 1783
addr0x00000b70: bne x22, x7, addr0x00000644
addr0x00000b74: sw x14, 1836(x8)
addr0x00000b78: sw x30, 172(x8)
addr0x00000b7c: lw x13, -1355(x8)
addr0x00000b80: addi x11, x18, 829
addr0x00000b84: slli x13, x23, 3
addr0x00000b88: sw x31, -1357(x8)
addr0x00000b8c: bgeu x13, x23, addr0x00000b2c
addr0x00000b90: lbu x7, 509(x8)
addr0x00000b94: beq x19, x18, addr0x000003b8
addr0x00000b98: lui x9, 694908
addr0x00000b9c: jal x6, addr0x000005b0
addr0x00000ba0: addi x7, x5, 1957
addr0x00000ba4: sw x0, -341(x8)
addr0x00000ba8: addi x31, x5, -589
addr0x00000bac: sw x13, -452(x8)
addr0x00000bb0: jal x24, addr0x000004e8
addr0x00000bb4: sw x27, 123(x8)
addr0x00000bb8: lw x2, -187(x8)
addr0x00000bbc: add x12, x30, x7
addr0x00000bc0: lbu x31, -253(x8)
addr0x00000bc4: beq x22, x7, addr0x00000628
addr0x00000bc8: jal x12, addr0x000000a4
addr0x00000bcc: jal x5, addr0x00000158
addr0x00000bd0: lw x27, -1967(x8)
addr0x00000bd4: sw x26, 484(x8)
addr0x00000bd8: beq x20, x16, addr0x00000c4c
addr0x00000bdc: jal x9, addr0x00000058
addr0x00000be0: lw x2, -266(x8)
addr0x00000be4: lw x23, -1510(x8)
addr0x00000be8: lui x23, 173574
addr0x00000bec: addi x15, x28, 1579
addr0x00000bf0: sw x19, 1799(x8)
addr0x00000bf4: sw x11, -1822(x8)
addr0x00000bf8: sw x30, 1806(x8)
addr0x00000bfc: sw x19, 154(x8)
addr0x00000c00: sw x5, -582(x8)
addr0x00000c04: addi x10, x27, 1427
addr0x00000c08: lw x9, 455(x8)
addr0x00000c0c: addi x28, x24, 1892
addr0x00000c10: sw x18, -1074(x8)
addr0x00000c14: sw x27, 596(x8)
addr0x00000c18: lw x1, 1555(x8)
addr0x00000c1c: lw x23, -1877(x8)
addr0x00000c20: addi x24, x21, 916
addr0x00000c24: lbu x17, 2031(x8)
addr0x00000c28: andi x20, x23, 642
addr0x00000c2c: andi x16, x1, -1654
addr0x00000c30: addi x19, x20, 810
addr0x00000c34: sw x29, 443(x8)
addr0x00000c38: sw x20, -1733(x8)
addr0x00000c3c: sw x19, 1325(x8)
addr0x00000c40: sw x29, 1803(x8)
addr0x00000c44: sw x20, 1032(x8)
addr0x00000c48: jal x9, addr0x000000c4
addr0x00000c4c: addi x23, x19, -1698
addr0x00000c50: lw x10, -34(x8)
addr0x00000c54: jal x26, addr0x0000002c
addr0x00000c58: lw x5, 654(x8)
addr0x00000c5c: lw x14, -1652(x8)
addr0x00000c60: lw x2, 1921(x8)
addr0x00000c64: lh x23, 1331(x8)
addr0x00000c68: lbu x27, -638(x8)
addr0x00000c6c: beq x1, x27, addr0x0000073c
addr0x00000c70: lw x23, 1330(x8)
addr0x00000c74: lw x14, -1183(x8)
addr0x00000c78: addi x27, x11, 1126
addr0x00000c7c: addi x22, x18, -1551
addr0x00000c80: sw x10, -1584(x8)
addr0x00000c84: sw x20, 78(x8)
addr0x00000c88: sw x27, 1207(x8)
addr0x00000c8c: sw x15, 1420(x8)
addr0x00000c90: sw x6, 1848(x8)
addr0x00000c94: mul x9, x0, x6
addr0x00000c98: lw x14, 1758(x8)
addr0x00000c9c: lw x11, 308(x8)
addr0x00000ca0: lw x30, 1243(x8)
addr0x00000ca4: lw x11, -1454(x8)
addr0x00000ca8: lw x23, -1027(x8)
addr0x00000cac: jal x27, addr0x00000d34
addr0x00000cb0: jal x0, addr0x00000690
addr0x00000cb4: jal x25, addr0x00000734
addr0x00000cb8: lw x4, 641(x8)
addr0x00000cbc: addi x31, x26, 62
addr0x00000cc0: sw x16, -277(x8)
addr0x00000cc4: lw x12, 114(x8)
addr0x00000cc8: lw x0, -846(x8)
addr0x00000ccc: jal x22, addr0x00000cac
addr0x00000cd0: jal x23, addr0x0000074c
addr0x00000cd4: lw x29, -643(x8)
addr0x00000cd8: lw x30, -119(x8)
addr0x00000cdc: lw x9, -509(x8)
addr0x00000ce0: lw x17, -2012(x8)
addr0x00000ce4: lw x0, -1810(x8)
addr0x00000ce8: sw x5, -1908(x8)
addr0x00000cec: sw x23, -1132(x8)
addr0x00000cf0: lui x17, 492928
addr0x00000cf4: addi x15, x22, 1920
addr0x00000cf8: lui x10, 262309
addr0x00000cfc: sh x15, -933(x8)
addr0x00000d00: sw x14, -2020(x8)
addr0x00000d04: sw x11, -715(x8)
addr0x00000d08: sw x19, -115(x8)
addr0x00000d0c: jal x13, addr0x00000aa0
addr0x00000d10: lw x6, 1399(x8)
addr0x00000d14: lw x27, 1365(x8)
addr0x00000d18: lw x0, 1939(x8)
addr0x00000d1c: slli x5, x5, 26
addr0x00000d20: add x23, x28, x17
addr0x00000d24: add x10, x9, x10
addr0x00000d28: addi x14, x5, 876
addr0x00000d2c: jal x0, addr0x00000bf4
addr0x00000d30: slli x26, x21, 6
addr0x00000d34: addi x31, x16, -1034
addr0x00000d38: lui x31, 728406
addr0x00000d3c: addi x22, x5, -342
addr0x00000d40: auipc x5, 1002479
addr0x00000d44: addi x22, x7, -298
addr0x00000d48: sw x23, 1092(x8)
addr0x00000d4c: jal x19, addr0x00000634
addr0x00000d50: lw x13, -272(x8)
addr0x00000d54: addi x17, x29, -98
addr0x00000d58: jal x14, addr0x00000644
addr0x00000d5c: jal x17, addr0x00000ac4
addr0x00000d60: addi x29, x31, 1681
addr0x00000d64: jal x0, addr0x000000a0
addr0x00000d68: lw x26, -127(x8)
addr0x00000d6c: add x13, x5, x7
addr0x00000d70: lbu x29, -1891(x8)
addr0x00000d74: addi x27, x11, -927
addr0x00000d78: lw x15, -1232(x8)
addr0x00000d7c: addi x20, x31, -1469
addr0x00000d80: srai x5, x10, 10
addr0x00000d84: sw x18, 1021(x8)
addr0x00000d88: lw x23, -289(x8)
addr0x00000d8c: lhu x18, -1038(x8)
addr0x00000d90: addi x27, x20, -1714
addr0x00000d94: sw x16, -318(x8)
addr0x00000d98: bne x22, x13, addr0x00000b5c
addr0x00000d9c: bltu x26, x29, addr0x000006bc
addr0x00000da0: lbu x10, 1535(x8)
addr0x00000da4: lw x28, -1598(x8)
addr0x00000da8: lw x4, -2027(x8)
addr0x00000dac: add x9, x20, x30
addr0x00000db0: bgeu x25, x25, addr0x0000080c
addr0x00000db4: lui x17, 340019
addr0x00000db8: lui x17, 541636
addr0x00000dbc: bge x0, x24, addr0x00000b90
addr0x00000dc0: addi x24, x13, -1045
addr0x00000dc4: lw x5, -1993(x8)
addr0x00000dc8: sw x17, -1543(x8)
addr0x00000dcc: lw x31, 311(x8)
addr0x00000dd0: addi x21, x5, 1857
addr0x00000dd4: addi x31, x2, -449
addr0x00000dd8: lw x11, -991(x8)
addr0x00000ddc: beq x5, x20, addr0x00000890
addr0x00000de0: addi x30, x29, 177
addr0x00000de4: add x7, x22, x6
addr0x00000de8: sw x13, -131(x8)
addr0x00000dec: jal x18, addr0x00000320
addr0x00000df0: sw x31, -1785(x8)
addr0x00000df4: sw x16, 1070(x8)
addr0x00000df8: jal x21, addr0x000005a8
addr0x00000dfc: addi x22, x22, -62
addr0x00000e00: sw x5, 396(x8)
addr0x00000e04: jal x29, addr0x00000ee8
addr0x00000e08: addi x10, x7, -2033
addr0x00000e0c: lw x23, 296(x8)
addr0x00000e10: lw x25, 1246(x8)
addr0x00000e14: addi x7, x26, 17
addr0x00000e18: beq x16, x15, addr0x00000d64
addr0x00000e1c: addi x9, x24, 1603
addr0x00000e20: add x22, x23, x18
addr0x00000e24: sw x14, -955(x8)
addr0x00000e28: sw x1, -1489(x8)
addr0x00000e2c: jal x17, addr0x00000e48
addr0x00000e30: lui x12, 459100
addr0x00000e34: lui x2, 836377
addr0x00000e38: sw x14, 29(x8)
addr0x00000e3c: sw x10, -817(x8)
addr0x00000e40: jal x22, addr0x00000e38
addr0x00000e44: bne x7, x27, addr0x000007f4
addr0x00000e48: lbu x30, -618(x8)
addr0x00000e4c: slli x28, x5, 5
addr0x00000e50: lw x28, 1167(x8)
addr0x00000e54: addi x28, x6, 419
addr0x00000e58: sh x5, -437(x8)
addr0x00000e5c: addi x16, x0, 2032
addr0x00000e60: jalr x0, 220(x3)
addr0x00000e64: sub x29, x27, x6
addr0x00000e68: lhu x14, 1034(x8)
addr0x00000e6c: addi x7, x23, -176
addr0x00000e70: lui x25, 807971
addr0x00000e74: addi x19, x18, -1868
addr0x00000e78: jalr x0, 308(x3)
addr0x00000e7c: blt x9, x22, addr0x00000870
addr0x00000e80: lw x19, -944(x8)
addr0x00000e84: lw x0, 846(x8)
addr0x00000e88: srai x26, x14, 8
addr0x00000e8c: bgeu x28, x20, addr0x00000758
addr0x00000e90: slli x9, x29, 26
addr0x00000e94: sw x25, 1000(x8)
addr0x00000e98: addi x27, x29, 421
addr0x00000e9c: sw x28, 1904(x8)
addr0x00000ea0: lui x7, 1032110
addr0x00000ea4: addi x21, x7, 2045
addr0x00000ea8: addi x15, x15, -12
addr0x00000eac: addi x23, x7, -779
addr0x00000eb0: jal x27, addr0x00000854
addr0x00000eb4: addi x6, x20, 989
addr0x00000eb8: sw x17, 1969(x8)
addr0x00000ebc: sw x9, -1254(x8)
addr0x00000ec0: sh x18, 1051(x8)
addr0x00000ec4: lw x25, -2043(x8)
addr0x00000ec8: slli x16, x29, 1
addr0x00000ecc: or x27, x31, x19
addr0x00000ed0: addi x18, x31, -1360
addr0x00000ed4: addi x21, x21, -514
addr0x00000ed8: lw x18, -1729(x8)
addr0x00000edc: lw x1, -490(x8)
addr0x00000ee0: sw x18, -1714(x8)
addr0x00000ee4: addi x7, x18, -1175
addr0x00000ee8: sw x13, 1657(x8)
addr0x00000eec: sw x14, -802(x8)
addr0x00000ef0: sb x14, 433(x8)
addr0x00000ef4: add x2, x5, x6
addr0x00000ef8: sw x13, -1614(x8)
addr0x00000efc: sw x30, -717(x8)
addr0x00000f00: lui x17, 360228
addr0x00000f04: addi x1, x29, -714
addr0x00000f08: addi x24, x19, -1393
addr0x00000f0c: addi x14, x28, -2024
addr0x00000f10: lw x2, -1716(x8)
addr0x00000f14: lw x31, -1091(x8)
addr0x00000f18: jal x31, addr0x0000075c
addr0x00000f1c: addi x30, x2, 1250
addr0x00000f20: lw x11, -1227(x8)
addr0x00000f24: lw x0, -1232(x8)
addr0x00000f28: jal x13, addr0x000001c4
addr0x00000f2c: lw x31, -74(x8)
addr0x00000f30: addi x19, x29, 1454
addr0x00000f34: sw x24, 920(x8)
addr0x00000f38: addi x6, x22, -1847
addr0x00000f3c: addi x10, x10, 1007
addr0x00000f40: jal x7, addr0x00000b0c
addr0x00000f44: bne x0, x5, addr0x00000a24
