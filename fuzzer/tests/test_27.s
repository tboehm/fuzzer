.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: addi x13, x23, -743
addr0x0000001c: jal x13, addr0x000002c0
addr0x00000020: lui x1, 309416
addr0x00000024: addi x19, x24, -547
addr0x00000028: addi x13, x25, 544
addr0x0000002c: addi x12, x29, -1729
addr0x00000030: addi x20, x0, 714
addr0x00000034: lw x19, -1025(x8)
addr0x00000038: lw x7, -1357(x8)
addr0x0000003c: lw x17, -722(x8)
addr0x00000040: bne x19, x27, addr0x00000438
addr0x00000044: lh x18, -321(x8)
addr0x00000048: slli x7, x4, 19
addr0x0000004c: or x13, x24, x2
addr0x00000050: add x18, x23, x22
addr0x00000054: lbu x15, -869(x8)
addr0x00000058: andi x9, x25, -773
addr0x0000005c: bltu x5, x27, addr0x0000024c
addr0x00000060: addi x29, x13, 674
addr0x00000064: lw x31, 1025(x8)
addr0x00000068: sw x18, 1394(x8)
addr0x0000006c: ori x17, x9, 299
addr0x00000070: sw x4, 216(x8)
addr0x00000074: sb x17, 1761(x8)
addr0x00000078: sb x26, -704(x8)
addr0x0000007c: sw x30, 520(x8)
addr0x00000080: sw x22, 163(x8)
addr0x00000084: addi x9, x31, -423
addr0x00000088: lw x28, 1225(x8)
addr0x0000008c: lw x22, -1025(x8)
addr0x00000090: jal x10, addr0x00000a5c
addr0x00000094: lw x5, 941(x8)
addr0x00000098: lw x29, -597(x8)
addr0x0000009c: jal x23, addr0x00000034
addr0x000000a0: lui x13, 593431
addr0x000000a4: sh x10, -150(x8)
addr0x000000a8: blt x0, x30, addr0x0000069c
addr0x000000ac: lw x19, 247(x8)
addr0x000000b0: sb x19, -765(x8)
addr0x000000b4: sw x15, 1304(x8)
addr0x000000b8: addi x15, x15, -133
addr0x000000bc: lw x31, -1026(x8)
addr0x000000c0: addi x17, x0, 1607
addr0x000000c4: slli x20, x15, 16
addr0x000000c8: lui x22, 459007
addr0x000000cc: addi x29, x18, 322
addr0x000000d0: lw x17, -1201(x8)
addr0x000000d4: jal x1, addr0x0000081c
addr0x000000d8: sw x17, 1226(x8)
addr0x000000dc: sw x27, 567(x8)
addr0x000000e0: bgeu x27, x5, addr0x00000134
addr0x000000e4: lw x29, 815(x8)
addr0x000000e8: addi x5, x22, -1186
addr0x000000ec: lw x20, 276(x8)
addr0x000000f0: jal x23, addr0x00000804
addr0x000000f4: jal x4, addr0x000003cc
addr0x000000f8: sw x31, 234(x8)
addr0x000000fc: lui x29, 6611
addr0x00000100: addi x31, x4, -170
addr0x00000104: jal x24, addr0x0000060c
addr0x00000108: addi x29, x31, 1990
addr0x0000010c: beq x22, x25, addr0x0000032c
addr0x00000110: bge x18, x1, addr0x0000027c
addr0x00000114: or x22, x18, x22
addr0x00000118: blt x2, x4, addr0x00000168
addr0x0000011c: beq x11, x2, addr0x00000828
addr0x00000120: sw x23, -861(x8)
addr0x00000124: lw x21, 980(x8)
addr0x00000128: addi x19, x7, -600
addr0x0000012c: sw x22, 955(x8)
addr0x00000130: jal x0, addr0x000001b4
addr0x00000134: lui x6, 980139
addr0x00000138: sw x10, 1939(x8)
addr0x0000013c: jal x0, addr0x000003dc
addr0x00000140: lbu x22, 1627(x8)
addr0x00000144: jal x25, addr0x000007b4
addr0x00000148: lw x18, 308(x8)
addr0x0000014c: sw x7, 1191(x8)
addr0x00000150: lw x25, 296(x8)
addr0x00000154: lw x22, 163(x8)
addr0x00000158: lw x13, 1574(x8)
addr0x0000015c: lbu x31, 136(x8)
addr0x00000160: beq x19, x6, addr0x00000470
addr0x00000164: sll x10, x12, x1
addr0x00000168: or x27, x22, x31
addr0x0000016c: lbu x1, 2008(x8)
addr0x00000170: sw x13, 1102(x8)
addr0x00000174: sw x1, 233(x8)
addr0x00000178: lw x7, -847(x8)
addr0x0000017c: lw x10, 261(x8)
addr0x00000180: lw x22, -1492(x8)
addr0x00000184: addi x27, x22, -533
addr0x00000188: sw x10, -179(x8)
addr0x0000018c: addi x22, x0, -845
addr0x00000190: addi x27, x18, -1556
addr0x00000194: jal x22, addr0x00000864
addr0x00000198: or x18, x14, x25
addr0x0000019c: bne x9, x23, addr0x00000704
addr0x000001a0: lw x9, -1776(x8)
addr0x000001a4: lw x19, 1532(x8)
addr0x000001a8: lw x0, -1999(x8)
addr0x000001ac: lw x24, -1910(x8)
addr0x000001b0: addi x2, x7, 1757
addr0x000001b4: lui x10, 927998
addr0x000001b8: addi x31, x28, 1956
addr0x000001bc: sw x20, -606(x8)
addr0x000001c0: jal x17, addr0x0000037c
addr0x000001c4: jal x19, addr0x00000574
addr0x000001c8: lw x6, 1901(x8)
addr0x000001cc: andi x29, x27, 2029
addr0x000001d0: addi x19, x22, 400
addr0x000001d4: andi x6, x31, -1826
addr0x000001d8: lui x29, 740453
addr0x000001dc: jal x23, addr0x00000960
addr0x000001e0: addi x1, x6, -952
addr0x000001e4: bne x17, x17, addr0x00000164
addr0x000001e8: lw x13, -577(x8)
addr0x000001ec: jal x10, addr0x00000514
addr0x000001f0: lw x16, 766(x8)
addr0x000001f4: lw x22, -424(x8)
addr0x000001f8: addi x28, x28, -76
addr0x000001fc: lui x7, 182860
addr0x00000200: lui x17, 856079
addr0x00000204: jal x22, addr0x000008e4
addr0x00000208: lw x7, 1590(x8)
addr0x0000020c: addi x5, x13, -742
addr0x00000210: jal x10, addr0x00000934
addr0x00000214: lw x23, -782(x8)
addr0x00000218: sw x2, 962(x8)
addr0x0000021c: addi x29, x24, 893
addr0x00000220: bne x7, x19, addr0x000008bc
addr0x00000224: lw x29, -1696(x8)
addr0x00000228: sw x7, -1452(x8)
addr0x0000022c: sw x29, 1201(x8)
addr0x00000230: sw x22, 1953(x8)
addr0x00000234: lw x18, -1161(x8)
addr0x00000238: lw x5, 867(x8)
addr0x0000023c: lw x7, 1512(x8)
addr0x00000240: lw x14, -1501(x8)
addr0x00000244: andi x29, x22, -1319
addr0x00000248: lw x20, -1273(x8)
addr0x0000024c: lui x28, 14522
addr0x00000250: sw x2, -1498(x8)
addr0x00000254: sw x13, -946(x8)
addr0x00000258: lw x0, -1096(x8)
addr0x0000025c: jal x6, addr0x00000138
addr0x00000260: or x7, x19, x18
addr0x00000264: addi x18, x1, -668
addr0x00000268: sw x17, 1608(x8)
addr0x0000026c: lw x4, 123(x8)
addr0x00000270: addi x10, x27, 1740
addr0x00000274: jal x22, addr0x00000838
addr0x00000278: lw x21, -1158(x8)
addr0x0000027c: addi x29, x5, 315
addr0x00000280: sub x2, x18, x13
addr0x00000284: lw x15, 659(x8)
addr0x00000288: addi x18, x10, -1343
addr0x0000028c: jal x13, addr0x00000414
addr0x00000290: lw x23, -977(x8)
addr0x00000294: jal x13, addr0x00000a64
addr0x00000298: bgeu x28, x28, addr0x00000618
addr0x0000029c: addi x5, x2, -1679
addr0x000002a0: sw x20, 32(x8)
addr0x000002a4: sw x31, -842(x8)
addr0x000002a8: addi x19, x13, 1934
addr0x000002ac: sw x27, 683(x8)
addr0x000002b0: addi x24, x14, 1166
addr0x000002b4: lw x4, -1493(x8)
addr0x000002b8: andi x6, x24, 1526
addr0x000002bc: lbu x26, 1810(x8)
addr0x000002c0: lui x27, 672677
addr0x000002c4: lui x20, 107340
addr0x000002c8: jal x21, addr0x000006f4
addr0x000002cc: andi x22, x27, 410
addr0x000002d0: bltu x30, x22, addr0x000007a0
addr0x000002d4: slli x21, x0, 26
addr0x000002d8: lhu x28, 1655(x8)
addr0x000002dc: addi x23, x17, 720
addr0x000002e0: lw x4, 761(x8)
addr0x000002e4: addi x1, x19, 1117
addr0x000002e8: srli x31, x17, 0
addr0x000002ec: bne x14, x25, addr0x0000009c
addr0x000002f0: lw x18, 1401(x8)
addr0x000002f4: jal x11, addr0x00000444
addr0x000002f8: addi x16, x5, 1641
addr0x000002fc: addi x19, x14, 1169
addr0x00000300: sw x19, -34(x8)
addr0x00000304: sw x14, 598(x8)
addr0x00000308: addi x29, x29, -79
addr0x0000030c: jal x6, addr0x000002a4
addr0x00000310: lw x19, 642(x8)
addr0x00000314: lw x7, -250(x8)
addr0x00000318: lw x12, -667(x8)
addr0x0000031c: sh x18, -329(x8)
addr0x00000320: sw x29, -1803(x8)
addr0x00000324: sw x7, -559(x8)
addr0x00000328: beq x18, x18, addr0x00000708
addr0x0000032c: lw x24, -1471(x8)
addr0x00000330: lw x31, -1324(x8)
addr0x00000334: lw x13, 304(x8)
addr0x00000338: lw x20, 1948(x8)
addr0x0000033c: addi x20, x9, -187
addr0x00000340: sw x14, -1752(x8)
addr0x00000344: addi x23, x25, 632
addr0x00000348: add x13, x29, x1
addr0x0000034c: addi x29, x17, -315
addr0x00000350: lw x30, 57(x8)
addr0x00000354: lw x29, 1745(x8)
addr0x00000358: sw x27, -872(x8)
addr0x0000035c: addi x24, x23, 102
addr0x00000360: jal x7, addr0x0000041c
addr0x00000364: sw x14, 1884(x8)
addr0x00000368: sw x11, 1057(x8)
addr0x0000036c: sw x7, 859(x8)
addr0x00000370: addi x7, x18, 1760
addr0x00000374: bne x6, x0, addr0x000008ac
addr0x00000378: addi x7, x14, 532
addr0x0000037c: sw x22, -921(x8)
addr0x00000380: bgeu x25, x24, addr0x000004f0
addr0x00000384: slli x20, x23, 2
addr0x00000388: add x23, x25, x14
addr0x0000038c: sw x20, 759(x8)
addr0x00000390: lw x13, 682(x8)
addr0x00000394: lw x9, 1326(x8)
addr0x00000398: lw x18, 453(x8)
addr0x0000039c: addi x28, x14, -102
addr0x000003a0: jal x0, addr0x000000e8
addr0x000003a4: bge x27, x20, addr0x00000470
addr0x000003a8: lui x10, 138172
addr0x000003ac: sw x31, 1749(x8)
addr0x000003b0: lw x30, -1671(x8)
addr0x000003b4: lw x4, -1304(x8)
addr0x000003b8: lw x11, -1373(x8)
addr0x000003bc: lw x22, -1645(x8)
addr0x000003c0: addi x22, x24, 743
addr0x000003c4: sw x29, 1081(x8)
addr0x000003c8: jal x19, addr0x0000090c
addr0x000003cc: lw x29, 815(x8)
addr0x000003d0: beq x23, x31, addr0x00000138
addr0x000003d4: lw x19, -2001(x8)
addr0x000003d8: addi x20, x13, 1503
addr0x000003dc: lhu x29, -1140(x8)
addr0x000003e0: lw x1, -412(x8)
addr0x000003e4: bne x21, x23, addr0x0000058c
addr0x000003e8: jal x18, addr0x000006b8
addr0x000003ec: addi x23, x30, -1439
addr0x000003f0: jal x21, addr0x0000074c
addr0x000003f4: lw x12, 970(x8)
addr0x000003f8: lw x6, -127(x8)
addr0x000003fc: addi x23, x0, 214
addr0x00000400: sw x21, -816(x8)
addr0x00000404: addi x7, x0, -1187
addr0x00000408: sw x31, 1011(x8)
addr0x0000040c: sw x23, 160(x8)
addr0x00000410: srl x17, x14, x19
addr0x00000414: lw x28, -1845(x8)
addr0x00000418: lui x19, 438814
addr0x0000041c: lw x19, 1150(x8)
addr0x00000420: lui x10, 187687
addr0x00000424: addi x2, x29, 1133
addr0x00000428: jal x5, addr0x000007b0
addr0x0000042c: lw x22, -271(x8)
addr0x00000430: lw x13, -496(x8)
addr0x00000434: add x6, x14, x19
addr0x00000438: jal x5, addr0x000004d4
addr0x0000043c: bne x23, x19, addr0x00000770
addr0x00000440: addi x29, x30, -1516
addr0x00000444: lw x1, -1902(x8)
addr0x00000448: add x10, x23, x18
addr0x0000044c: bltu x20, x14, addr0x00000310
addr0x00000450: addi x21, x19, -708
addr0x00000454: jal x10, addr0x000004cc
addr0x00000458: sw x29, -1145(x8)
addr0x0000045c: lw x0, 307(x8)
addr0x00000460: blt x31, x29, addr0x00000478
addr0x00000464: slli x13, x13, 19
addr0x00000468: sw x20, 1791(x8)
addr0x0000046c: lw x0, 1982(x8)
addr0x00000470: lw x0, 634(x8)
addr0x00000474: addi x30, x18, 1706
addr0x00000478: sw x15, 653(x8)
addr0x0000047c: addi x20, x19, 1918
addr0x00000480: jal x9, addr0x00000244
addr0x00000484: lw x13, 1126(x8)
addr0x00000488: lw x21, 1373(x8)
addr0x0000048c: jal x10, addr0x00000720
addr0x00000490: addi x19, x6, 1792
addr0x00000494: lw x26, -1382(x8)
addr0x00000498: lw x27, -991(x8)
addr0x0000049c: andi x23, x21, 1053
addr0x000004a0: bgeu x4, x19, addr0x0000029c
addr0x000004a4: addi x2, x23, -464
addr0x000004a8: lw x31, 387(x8)
addr0x000004ac: lw x17, 1283(x8)
addr0x000004b0: jal x0, addr0x00000808
addr0x000004b4: lui x7, 317928
addr0x000004b8: addi x31, x25, -1150
addr0x000004bc: sw x18, 913(x8)
addr0x000004c0: jal x20, addr0x00000740
addr0x000004c4: sw x23, -1511(x8)
addr0x000004c8: addi x0, x20, 362
addr0x000004cc: sw x30, 551(x8)
addr0x000004d0: lw x20, -1779(x8)
addr0x000004d4: sw x24, -1375(x8)
addr0x000004d8: andi x0, x19, 143
addr0x000004dc: jal x18, addr0x000002a8
addr0x000004e0: lui x27, 761202
addr0x000004e4: addi x11, x21, 490
addr0x000004e8: sw x13, 1274(x8)
addr0x000004ec: jal x30, addr0x000008f4
addr0x000004f0: sw x0, -259(x8)
addr0x000004f4: lw x14, 1915(x8)
addr0x000004f8: lw x20, -273(x8)
addr0x000004fc: addi x30, x10, 1170
addr0x00000500: lw x2, 1704(x8)
addr0x00000504: addi x19, x9, -375
addr0x00000508: addi x1, x22, -670
addr0x0000050c: lui x22, 741755
addr0x00000510: sw x27, 373(x8)
addr0x00000514: jal x5, addr0x00000110
addr0x00000518: lw x16, 1453(x8)
addr0x0000051c: jal x19, addr0x000002f4
addr0x00000520: lw x14, -1614(x8)
addr0x00000524: srli x24, x0, 31
addr0x00000528: add x29, x17, x23
addr0x0000052c: addi x19, x25, -578
addr0x00000530: sw x15, 1198(x8)
addr0x00000534: lw x25, 1408(x8)
addr0x00000538: addi x0, x20, -1359
addr0x0000053c: lbu x24, 1302(x8)
addr0x00000540: andi x14, x26, 246
addr0x00000544: bgeu x5, x10, addr0x000007fc
addr0x00000548: jal x21, addr0x000008f0
addr0x0000054c: andi x14, x21, -1694
addr0x00000550: sw x17, 1079(x8)
addr0x00000554: sw x22, -558(x8)
addr0x00000558: lui x20, 508586
addr0x0000055c: addi x1, x12, 634
addr0x00000560: addi x13, x20, -1274
addr0x00000564: addi x19, x0, -23
addr0x00000568: jal x18, addr0x000004f4
addr0x0000056c: lui x9, 358117
addr0x00000570: sw x20, -1137(x8)
addr0x00000574: addi x13, x5, 48
addr0x00000578: sw x1, 1789(x8)
addr0x0000057c: sw x13, -681(x8)
addr0x00000580: addi x27, x20, -1522
addr0x00000584: sw x6, -628(x8)
addr0x00000588: lw x20, -277(x8)
addr0x0000058c: lw x4, 1970(x8)
addr0x00000590: add x29, x17, x19
addr0x00000594: lw x31, 1085(x8)
addr0x00000598: lw x6, -800(x8)
addr0x0000059c: lw x19, 1295(x8)
addr0x000005a0: jal x9, addr0x000009e8
addr0x000005a4: jal x16, addr0x00000750
addr0x000005a8: jal x0, addr0x0000042c
addr0x000005ac: lw x4, -370(x8)
addr0x000005b0: addi x25, x27, 1464
addr0x000005b4: sw x20, -450(x8)
addr0x000005b8: sw x7, -113(x8)
addr0x000005bc: addi x24, x25, 1064
addr0x000005c0: addi x14, x6, -735
addr0x000005c4: beq x18, x10, addr0x000001f0
addr0x000005c8: lw x28, 1323(x8)
addr0x000005cc: lw x11, 1357(x8)
addr0x000005d0: lw x21, -1162(x8)
addr0x000005d4: bgeu x15, x5, addr0x00000154
addr0x000005d8: lui x18, 527108
addr0x000005dc: addi x11, x19, 1091
addr0x000005e0: lw x2, 614(x8)
addr0x000005e4: sw x25, 1027(x8)
addr0x000005e8: addi x4, x13, -826
addr0x000005ec: beq x20, x4, addr0x00000668
addr0x000005f0: jal x14, addr0x00000124
addr0x000005f4: jal x0, addr0x00000600
addr0x000005f8: lw x4, -451(x8)
addr0x000005fc: jal x11, addr0x00000350
addr0x00000600: sub x1, x13, x25
addr0x00000604: sw x7, 1129(x8)
addr0x00000608: sw x10, -687(x8)
addr0x0000060c: jal x21, addr0x00000128
addr0x00000610: jal x7, addr0x000001e0
addr0x00000614: sw x20, 653(x8)
addr0x00000618: sw x17, 627(x8)
addr0x0000061c: lw x4, -1675(x8)
addr0x00000620: bgeu x20, x19, addr0x000008b8
addr0x00000624: lw x14, 1679(x8)
addr0x00000628: jal x22, addr0x000006dc
addr0x0000062c: bne x0, x14, addr0x000005dc
addr0x00000630: lbu x17, -1269(x8)
addr0x00000634: andi x9, x5, -1961
addr0x00000638: lw x23, 510(x8)
addr0x0000063c: add x21, x17, x13
addr0x00000640: lh x14, 563(x8)
addr0x00000644: jal x14, addr0x000003b8
addr0x00000648: sw x30, -1517(x8)
addr0x0000064c: jal x23, addr0x00000a78
addr0x00000650: srai x28, x23, 30
addr0x00000654: andi x22, x19, -1024
addr0x00000658: jal x29, addr0x00000500
addr0x0000065c: lw x22, -749(x8)
addr0x00000660: lw x31, -1365(x8)
addr0x00000664: andi x22, x28, 551
addr0x00000668: addi x13, x9, 1981
addr0x0000066c: addi x23, x25, 1132
addr0x00000670: sw x16, 1565(x8)
addr0x00000674: sw x0, -1075(x8)
addr0x00000678: sh x4, -128(x8)
addr0x0000067c: lw x4, 323(x8)
addr0x00000680: sw x25, 857(x8)
addr0x00000684: lw x25, -775(x8)
addr0x00000688: addi x14, x7, 1146
addr0x0000068c: lhu x23, -1168(x8)
addr0x00000690: lui x13, 918902
addr0x00000694: addi x0, x27, -516
addr0x00000698: jal x21, addr0x00000010
addr0x0000069c: lw x19, -1930(x8)
addr0x000006a0: bne x15, x6, addr0x000002b0
addr0x000006a4: sh x27, 1590(x8)
addr0x000006a8: addi x13, x0, 697
addr0x000006ac: sw x0, -1900(x8)
addr0x000006b0: lui x13, 250033
addr0x000006b4: addi x30, x0, 1622
addr0x000006b8: jal x7, addr0x00000740
addr0x000006bc: sb x19, 822(x8)
addr0x000006c0: addi x15, x24, -1441
addr0x000006c4: jal x19, addr0x0000067c
addr0x000006c8: lw x29, 1866(x8)
addr0x000006cc: lw x30, 134(x8)
addr0x000006d0: addi x7, x22, -1532
addr0x000006d4: sh x22, 790(x8)
addr0x000006d8: bne x0, x23, addr0x000006e4
addr0x000006dc: addi x7, x18, -890
addr0x000006e0: sub x24, x9, x20
addr0x000006e4: sw x2, -526(x8)
addr0x000006e8: sw x15, 1338(x8)
addr0x000006ec: jal x1, addr0x000006f8
addr0x000006f0: lw x0, 1345(x8)
addr0x000006f4: addi x7, x31, 96
addr0x000006f8: lw x26, -1507(x8)
addr0x000006fc: addi x14, x23, -1550
addr0x00000700: sw x15, 910(x8)
addr0x00000704: jal x13, addr0x000004fc
addr0x00000708: lw x15, 1157(x8)
addr0x0000070c: lbu x25, 1009(x8)
addr0x00000710: addi x9, x10, 1433
addr0x00000714: lw x18, 1851(x8)
addr0x00000718: jal x5, addr0x00000734
addr0x0000071c: lw x6, -1140(x8)
addr0x00000720: lw x20, -1955(x8)
addr0x00000724: lw x9, 1051(x8)
addr0x00000728: lw x2, 1285(x8)
addr0x0000072c: lw x15, 326(x8)
addr0x00000730: lw x15, 23(x8)
addr0x00000734: jal x13, addr0x00000048
addr0x00000738: sw x0, -1308(x8)
addr0x0000073c: sw x25, 512(x8)
addr0x00000740: andi x18, x9, -489
addr0x00000744: jal x30, addr0x000007dc
addr0x00000748: lbu x15, 176(x8)
addr0x0000074c: beq x22, x5, addr0x00000384
addr0x00000750: addi x30, x24, 1671
addr0x00000754: sw x22, -476(x8)
addr0x00000758: addi x25, x12, 1208
addr0x0000075c: lw x31, 891(x8)
addr0x00000760: jal x19, addr0x00000840
addr0x00000764: addi x23, x9, 595
addr0x00000768: sw x0, -1078(x8)
addr0x0000076c: lw x24, 1450(x8)
addr0x00000770: lw x19, 1186(x8)
addr0x00000774: lw x23, 1739(x8)
addr0x00000778: lw x24, -207(x8)
addr0x0000077c: jal x22, addr0x0000077c
addr0x00000780: sw x30, 557(x8)
addr0x00000784: bltu x19, x25, addr0x00000220
addr0x00000788: lw x18, 1734(x8)
addr0x0000078c: addi x24, x7, -1891
addr0x00000790: add x15, x14, x5
addr0x00000794: sw x22, -1282(x8)
addr0x00000798: sw x18, 891(x8)
addr0x0000079c: beq x4, x26, addr0x000001d0
addr0x000007a0: sw x21, -1426(x8)
addr0x000007a4: lbu x5, -169(x8)
addr0x000007a8: slli x18, x19, 15
addr0x000007ac: add x13, x30, x20
addr0x000007b0: lw x6, -1769(x8)
addr0x000007b4: lw x29, -618(x8)
addr0x000007b8: jal x19, addr0x000004e0
addr0x000007bc: sb x22, -1682(x8)
addr0x000007c0: sb x17, -492(x8)
addr0x000007c4: sb x19, 1158(x8)
addr0x000007c8: lw x7, -699(x8)
addr0x000007cc: blt x29, x1, addr0x000002b4
addr0x000007d0: addi x24, x22, 1704
addr0x000007d4: jal x20, addr0x00000044
addr0x000007d8: lw x0, -245(x8)
addr0x000007dc: sw x7, 1183(x8)
addr0x000007e0: lui x20, 969659
addr0x000007e4: lui x17, 316860
addr0x000007e8: lw x31, 778(x8)
addr0x000007ec: andi x14, x24, -1206
addr0x000007f0: lbu x0, -597(x8)
addr0x000007f4: addi x9, x10, -2022
addr0x000007f8: bne x26, x0, addr0x00000064
addr0x000007fc: jal x20, addr0x0000009c
addr0x00000800: lw x17, -1255(x8)
addr0x00000804: bgeu x25, x23, addr0x000003b8
addr0x00000808: slli x9, x13, 14
addr0x0000080c: lw x31, -497(x8)
addr0x00000810: sw x9, 1814(x8)
addr0x00000814: sw x31, -559(x8)
addr0x00000818: lw x18, 1233(x8)
addr0x0000081c: sw x0, 243(x8)
addr0x00000820: lui x4, 292778
addr0x00000824: addi x4, x12, 1718
addr0x00000828: addi x20, x15, 1446
addr0x0000082c: lui x4, 52227
addr0x00000830: addi x29, x24, 1049
addr0x00000834: lui x0, 684187
addr0x00000838: lw x24, -1839(x8)
addr0x0000083c: addi x28, x20, -1351
addr0x00000840: sw x18, -1778(x8)
addr0x00000844: sw x21, -1413(x8)
addr0x00000848: sub x2, x0, x13
addr0x0000084c: lw x29, -834(x8)
addr0x00000850: bgeu x21, x17, addr0x0000060c
addr0x00000854: addi x31, x19, -255
addr0x00000858: lui x17, 758353
addr0x0000085c: sh x17, -401(x8)
addr0x00000860: bne x28, x18, addr0x00000650
addr0x00000864: sw x2, -1717(x8)
addr0x00000868: jal x14, addr0x000007bc
addr0x0000086c: jal x28, addr0x000008d4
addr0x00000870: lw x7, -991(x8)
addr0x00000874: slli x22, x27, 2
addr0x00000878: lui x18, 912142
addr0x0000087c: addi x30, x29, 280
addr0x00000880: sw x27, -203(x8)
addr0x00000884: sw x5, -1469(x8)
addr0x00000888: sw x25, -1167(x8)
addr0x0000088c: sw x13, 1248(x8)
addr0x00000890: lw x31, -1164(x8)
addr0x00000894: sw x22, 1372(x8)
addr0x00000898: sw x13, -2019(x8)
addr0x0000089c: jal x22, addr0x000008e4
addr0x000008a0: lw x0, -87(x8)
addr0x000008a4: lw x5, -60(x8)
addr0x000008a8: lw x12, 76(x8)
addr0x000008ac: lw x29, 151(x8)
addr0x000008b0: addi x6, x23, -90
addr0x000008b4: sw x0, -1840(x8)
addr0x000008b8: sw x31, -1797(x8)
addr0x000008bc: lw x17, 378(x8)
addr0x000008c0: addi x12, x31, 391
addr0x000008c4: addi x9, x2, -528
addr0x000008c8: sw x31, 1064(x8)
addr0x000008cc: addi x20, x4, 1190
addr0x000008d0: sb x0, 1956(x8)
addr0x000008d4: jal x13, addr0x000002a4
addr0x000008d8: lbu x13, -988(x8)
addr0x000008dc: lw x29, 349(x8)
addr0x000008e0: addi x17, x1, -1069
addr0x000008e4: lw x6, 1795(x8)
addr0x000008e8: lw x18, 367(x8)
addr0x000008ec: addi x9, x0, -1820
addr0x000008f0: lui x29, 117658
addr0x000008f4: lw x18, 723(x8)
addr0x000008f8: andi x17, x4, -568
addr0x000008fc: sb x13, 1799(x8)
addr0x00000900: sw x2, 1061(x8)
addr0x00000904: jal x23, addr0x00000758
addr0x00000908: addi x25, x22, 1713
addr0x0000090c: lw x22, -857(x8)
addr0x00000910: lw x29, -792(x8)
addr0x00000914: lw x25, 240(x8)
addr0x00000918: lw x13, -1015(x8)
addr0x0000091c: jal x6, addr0x00000220
addr0x00000920: lw x15, 1708(x8)
addr0x00000924: addi x13, x4, 232
addr0x00000928: lbu x30, -1816(x8)
addr0x0000092c: bne x25, x27, addr0x00000358
addr0x00000930: addi x4, x22, -468
addr0x00000934: add x27, x11, x29
addr0x00000938: lw x19, 1782(x8)
addr0x0000093c: lw x17, 1444(x8)
addr0x00000940: addi x1, x19, 1007
addr0x00000944: lhu x18, -1238(x8)
addr0x00000948: andi x31, x9, -461
addr0x0000094c: lw x14, -800(x8)
addr0x00000950: lw x24, 872(x8)
addr0x00000954: sw x13, 539(x8)
addr0x00000958: addi x21, x19, 559
addr0x0000095c: bge x29, x23, addr0x000006c8
addr0x00000960: sw x9, 869(x8)
addr0x00000964: sw x19, 1400(x8)
addr0x00000968: sw x19, 162(x8)
addr0x0000096c: sw x28, 239(x8)
addr0x00000970: addi x24, x28, -1189
addr0x00000974: jal x17, addr0x0000022c
addr0x00000978: lbu x29, 1361(x8)
addr0x0000097c: addi x22, x9, 1285
addr0x00000980: lw x14, 392(x8)
addr0x00000984: jal x23, addr0x00000468
addr0x00000988: jal x19, addr0x00000538
addr0x0000098c: jal x19, addr0x000002ec
addr0x00000990: lw x19, 2044(x8)
addr0x00000994: bgeu x30, x7, addr0x00000a10
addr0x00000998: sw x22, -2047(x8)
addr0x0000099c: sw x24, 6(x8)
addr0x000009a0: bge x31, x19, addr0x000008e0
addr0x000009a4: bne x12, x0, addr0x00000428
addr0x000009a8: beq x5, x2, addr0x00000260
addr0x000009ac: bne x10, x19, addr0x00000428
addr0x000009b0: lw x1, -1355(x8)
addr0x000009b4: sw x29, 1954(x8)
addr0x000009b8: sw x30, -212(x8)
addr0x000009bc: sw x9, 1752(x8)
addr0x000009c0: jal x9, addr0x00000884
addr0x000009c4: sb x13, -932(x8)
addr0x000009c8: addi x20, x23, 1790
addr0x000009cc: sw x15, -5(x8)
addr0x000009d0: jal x1, addr0x000003a0
addr0x000009d4: blt x4, x9, addr0x00000658
addr0x000009d8: addi x10, x19, -990
addr0x000009dc: lw x21, -527(x8)
addr0x000009e0: sw x23, 1465(x8)
addr0x000009e4: jal x7, addr0x00000328
addr0x000009e8: sw x23, 614(x8)
addr0x000009ec: lw x28, -2026(x8)
addr0x000009f0: lw x20, -1100(x8)
addr0x000009f4: sw x5, -1206(x8)
addr0x000009f8: jal x17, addr0x00000758
addr0x000009fc: sw x21, -367(x8)
addr0x00000a00: sw x22, -860(x8)
addr0x00000a04: sw x18, -1823(x8)
addr0x00000a08: sw x17, 1703(x8)
addr0x00000a0c: lbu x16, -1916(x8)
addr0x00000a10: addi x22, x19, 1896
addr0x00000a14: jal x13, addr0x000009c4
addr0x00000a18: addi x29, x18, 826
addr0x00000a1c: sw x5, 1865(x8)
addr0x00000a20: lw x23, -915(x8)
addr0x00000a24: bltu x11, x29, addr0x000003b0
addr0x00000a28: lw x6, 429(x8)
addr0x00000a2c: addi x19, x7, -1372
addr0x00000a30: sw x31, 1595(x8)
addr0x00000a34: jal x29, addr0x00000214
addr0x00000a38: srl x10, x4, x19
addr0x00000a3c: andi x17, x5, 1717
addr0x00000a40: lui x0, 906098
addr0x00000a44: addi x25, x31, 1039
addr0x00000a48: sw x12, -876(x8)
addr0x00000a4c: sw x0, -122(x8)
addr0x00000a50: addi x17, x7, 275
addr0x00000a54: sw x23, 1298(x8)
addr0x00000a58: lw x1, 324(x8)
addr0x00000a5c: jal x22, addr0x00000970
addr0x00000a60: lw x12, -356(x8)
addr0x00000a64: jal x5, addr0x00000320
addr0x00000a68: jal x17, addr0x00000458
addr0x00000a6c: jal x7, addr0x00000014
addr0x00000a70: bltu x17, x27, addr0x0000097c
addr0x00000a74: andi x19, x7, -1371
addr0x00000a78: bltu x23, x23, addr0x0000049c
