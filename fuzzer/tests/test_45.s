.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bge x29, x30, addr0x00000188
addr0x0000001c: lw x0, -1159(x8)
addr0x00000020: addi x31, x4, -147
addr0x00000024: lw x21, -1766(x8)
addr0x00000028: jal x29, addr0x00000700
addr0x0000002c: ori x7, x27, -396
addr0x00000030: sw x28, -911(x8)
addr0x00000034: jal x19, addr0x000004b4
addr0x00000038: addi x19, x27, 244
addr0x0000003c: jal x12, addr0x00000050
addr0x00000040: lbu x28, 1414(x8)
addr0x00000044: beq x17, x22, addr0x000000a8
addr0x00000048: lhu x9, -233(x8)
addr0x0000004c: lhu x13, -953(x8)
addr0x00000050: lui x7, 853604
addr0x00000054: addi x14, x12, -1337
addr0x00000058: lui x19, 314382
addr0x0000005c: addi x17, x22, -835
addr0x00000060: sw x20, -1845(x8)
addr0x00000064: jal x20, addr0x000002d0
addr0x00000068: addi x20, x23, 1624
addr0x0000006c: jal x29, addr0x000004b4
addr0x00000070: lw x20, 1996(x8)
addr0x00000074: lui x2, 588914
addr0x00000078: lw x22, 1867(x8)
addr0x0000007c: jal x9, addr0x000004ec
addr0x00000080: jal x4, addr0x00000390
addr0x00000084: addi x22, x27, 1416
addr0x00000088: jal x31, addr0x0000041c
addr0x0000008c: lw x15, 804(x8)
addr0x00000090: lw x23, -313(x8)
addr0x00000094: jal x18, addr0x00000008
addr0x00000098: lw x28, -905(x8)
addr0x0000009c: sw x22, -901(x8)
addr0x000000a0: bltu x16, x19, addr0x00000158
addr0x000000a4: andi x7, x4, 773
addr0x000000a8: lw x31, -1134(x8)
addr0x000000ac: slli x19, x24, 23
addr0x000000b0: or x14, x13, x9
addr0x000000b4: lbu x9, -1399(x8)
addr0x000000b8: jalr x0, 216(x3)
addr0x000000bc: jal x13, addr0x00000064
addr0x000000c0: jal x9, addr0x0000032c
addr0x000000c4: addi x6, x2, 1634
addr0x000000c8: add x2, x22, x18
addr0x000000cc: sw x31, 404(x8)
addr0x000000d0: sw x28, -886(x8)
addr0x000000d4: jal x31, addr0x00000350
addr0x000000d8: addi x13, x28, 483
addr0x000000dc: lui x22, 31383
addr0x000000e0: addi x12, x18, 1076
addr0x000000e4: lw x9, 4(x8)
addr0x000000e8: lw x19, -1552(x8)
addr0x000000ec: lw x22, -842(x8)
addr0x000000f0: addi x29, x23, 1118
addr0x000000f4: addi x11, x16, 1010
addr0x000000f8: jal x11, addr0x00000554
addr0x000000fc: sw x4, -1823(x8)
addr0x00000100: sw x22, 1338(x8)
addr0x00000104: jal x18, addr0x00000350
addr0x00000108: lw x22, -1999(x8)
addr0x0000010c: addi x25, x1, -1295
addr0x00000110: sw x2, 1053(x8)
addr0x00000114: beq x19, x4, addr0x000000f4
addr0x00000118: lbu x27, -721(x8)
addr0x0000011c: lw x28, -1308(x8)
addr0x00000120: sw x19, -418(x8)
addr0x00000124: sw x13, 1500(x8)
addr0x00000128: addi x15, x25, -872
addr0x0000012c: jal x11, addr0x00000074
addr0x00000130: sw x29, -78(x8)
addr0x00000134: lui x9, 865937
addr0x00000138: addi x19, x20, -1553
addr0x0000013c: jal x30, addr0x00000648
addr0x00000140: lw x4, -991(x8)
addr0x00000144: lbu x9, -2008(x8)
addr0x00000148: lw x13, -86(x8)
addr0x0000014c: sb x1, -1801(x8)
addr0x00000150: lbu x9, 1236(x8)
addr0x00000154: lw x31, -1325(x8)
addr0x00000158: slli x14, x26, 29
addr0x0000015c: add x18, x21, x4
addr0x00000160: bltu x13, x7, addr0x00000114
addr0x00000164: lw x18, 1015(x8)
addr0x00000168: lw x21, -470(x8)
addr0x0000016c: addi x7, x31, -444
addr0x00000170: lw x7, 361(x8)
addr0x00000174: lw x30, -893(x8)
addr0x00000178: sub x10, x16, x28
addr0x0000017c: lw x27, 1885(x8)
addr0x00000180: sw x16, -1810(x8)
addr0x00000184: sb x6, 1147(x8)
addr0x00000188: lw x2, 1373(x8)
addr0x0000018c: beq x11, x0, addr0x00000504
addr0x00000190: addi x5, x23, -2011
addr0x00000194: jal x19, addr0x00000144
addr0x00000198: lw x23, -653(x8)
addr0x0000019c: lw x1, -1642(x8)
addr0x000001a0: beq x22, x14, addr0x000000a8
addr0x000001a4: lw x27, -343(x8)
addr0x000001a8: lw x7, -1846(x8)
addr0x000001ac: addi x23, x17, 482
addr0x000001b0: lw x1, -423(x8)
addr0x000001b4: lw x4, -1876(x8)
addr0x000001b8: jal x18, addr0x00000314
addr0x000001bc: bne x6, x27, addr0x00000640
addr0x000001c0: jal x19, addr0x00000104
addr0x000001c4: jal x29, addr0x00000484
addr0x000001c8: lw x13, 13(x8)
addr0x000001cc: jal x22, addr0x00000004
addr0x000001d0: lw x9, 1450(x8)
addr0x000001d4: sw x7, 793(x8)
addr0x000001d8: sw x20, -1766(x8)
addr0x000001dc: addi x27, x19, 1515
addr0x000001e0: lw x11, -1341(x8)
addr0x000001e4: addi x14, x1, 1579
addr0x000001e8: jal x9, addr0x00000620
addr0x000001ec: jal x19, addr0x00000104
addr0x000001f0: sw x13, 1336(x8)
addr0x000001f4: lw x24, 506(x8)
addr0x000001f8: lw x27, 1724(x8)
addr0x000001fc: addi x16, x27, 395
addr0x00000200: jal x26, addr0x00000740
addr0x00000204: lw x28, -1377(x8)
addr0x00000208: sw x23, -761(x8)
addr0x0000020c: jal x14, addr0x00000560
addr0x00000210: jal x9, addr0x00000448
addr0x00000214: jal x19, addr0x000005e0
addr0x00000218: jal x7, addr0x00000498
addr0x0000021c: jal x19, addr0x000002f0
addr0x00000220: ori x30, x9, -1264
addr0x00000224: beq x0, x23, addr0x00000164
addr0x00000228: jal x10, addr0x000001b4
addr0x0000022c: addi x17, x31, 1394
addr0x00000230: sw x26, 102(x8)
addr0x00000234: sw x20, 1153(x8)
addr0x00000238: sw x2, 261(x8)
addr0x0000023c: jal x2, addr0x00000108
addr0x00000240: jal x26, addr0x000003cc
addr0x00000244: lw x27, 1704(x8)
addr0x00000248: lw x14, 760(x8)
addr0x0000024c: addi x28, x0, -1851
addr0x00000250: and x9, x23, x30
addr0x00000254: jal x26, addr0x000000f4
addr0x00000258: addi x30, x19, 898
addr0x0000025c: jal x25, addr0x000004bc
addr0x00000260: jal x13, addr0x00000768
addr0x00000264: lw x2, 1837(x8)
addr0x00000268: addi x18, x30, -682
addr0x0000026c: jal x15, addr0x0000056c
addr0x00000270: addi x30, x15, -908
addr0x00000274: lw x28, -531(x8)
addr0x00000278: lw x27, -447(x8)
addr0x0000027c: lw x9, -504(x8)
addr0x00000280: bne x4, x14, addr0x00000774
addr0x00000284: jal x7, addr0x000001d4
addr0x00000288: andi x19, x20, 36
addr0x0000028c: lw x15, -1752(x8)
addr0x00000290: lw x31, 1772(x8)
addr0x00000294: jal x10, addr0x00000120
addr0x00000298: jal x7, addr0x00000170
addr0x0000029c: lw x5, 673(x8)
addr0x000002a0: lw x19, 627(x8)
addr0x000002a4: addi x2, x22, 1883
addr0x000002a8: jal x26, addr0x00000184
addr0x000002ac: jal x9, addr0x00000458
addr0x000002b0: andi x17, x29, 1806
addr0x000002b4: andi x29, x16, 883
addr0x000002b8: andi x18, x25, 182
addr0x000002bc: jal x13, addr0x00000488
addr0x000002c0: auipc x4, 872012
addr0x000002c4: addi x12, x10, -1909
addr0x000002c8: sw x23, 1888(x8)
addr0x000002cc: sw x25, 477(x8)
addr0x000002d0: lw x4, 1820(x8)
addr0x000002d4: lw x19, -1195(x8)
addr0x000002d8: lw x22, -506(x8)
addr0x000002dc: bgeu x0, x7, addr0x000002dc
addr0x000002e0: srli x18, x20, 13
addr0x000002e4: and x20, x17, x29
addr0x000002e8: mul x19, x22, x20
addr0x000002ec: mul x20, x22, x16
addr0x000002f0: sub x19, x27, x27
addr0x000002f4: sw x31, -814(x8)
addr0x000002f8: bge x29, x14, addr0x00000570
addr0x000002fc: addi x22, x26, 896
addr0x00000300: sw x11, 1117(x8)
addr0x00000304: lw x14, 246(x8)
addr0x00000308: lw x22, 1176(x8)
addr0x0000030c: bge x18, x15, addr0x000001ac
addr0x00000310: addi x7, x29, -1414
addr0x00000314: beq x17, x20, addr0x000003f8
addr0x00000318: lw x1, 740(x8)
addr0x0000031c: lw x7, -559(x8)
addr0x00000320: lw x14, 1498(x8)
addr0x00000324: lw x14, 1175(x8)
addr0x00000328: jal x0, addr0x00000764
addr0x0000032c: lw x18, 1183(x8)
addr0x00000330: sh x27, -821(x8)
addr0x00000334: sh x4, -704(x8)
addr0x00000338: addi x28, x0, 683
addr0x0000033c: lbu x2, -117(x8)
addr0x00000340: lw x21, -1865(x8)
addr0x00000344: jal x20, addr0x0000057c
addr0x00000348: lw x9, -73(x8)
addr0x0000034c: lw x6, 1830(x8)
addr0x00000350: sw x22, -1679(x8)
addr0x00000354: sw x1, -357(x8)
addr0x00000358: sw x20, 18(x8)
addr0x0000035c: sw x0, 1409(x8)
addr0x00000360: lw x31, 1799(x8)
addr0x00000364: sw x14, 950(x8)
addr0x00000368: addi x2, x10, -190
addr0x0000036c: sw x21, 445(x8)
addr0x00000370: lui x30, 641752
addr0x00000374: addi x7, x9, 2030
addr0x00000378: lw x5, 1512(x8)
addr0x0000037c: lw x18, 1153(x8)
addr0x00000380: lw x26, 1705(x8)
addr0x00000384: sw x18, -149(x8)
addr0x00000388: lbu x20, -978(x8)
addr0x0000038c: bne x11, x10, addr0x00000700
addr0x00000390: bltu x29, x18, addr0x00000524
addr0x00000394: lw x4, 1069(x8)
addr0x00000398: sh x1, -479(x8)
addr0x0000039c: sw x9, -1355(x8)
addr0x000003a0: sb x22, 1185(x8)
addr0x000003a4: addi x7, x5, -239
addr0x000003a8: addi x12, x2, -1349
addr0x000003ac: sw x23, -1904(x8)
addr0x000003b0: sw x29, -220(x8)
addr0x000003b4: jal x30, addr0x000002c0
addr0x000003b8: lw x29, -685(x8)
addr0x000003bc: sw x30, -1806(x8)
addr0x000003c0: lbu x10, 935(x8)
addr0x000003c4: jal x23, addr0x000006e8
addr0x000003c8: lw x18, -370(x8)
addr0x000003cc: lw x20, 1892(x8)
addr0x000003d0: sw x18, 707(x8)
addr0x000003d4: sw x18, 1882(x8)
addr0x000003d8: sw x0, 188(x8)
addr0x000003dc: lw x22, 1234(x8)
addr0x000003e0: lw x4, 1595(x8)
addr0x000003e4: sw x22, -930(x8)
addr0x000003e8: sw x9, -801(x8)
addr0x000003ec: blt x27, x4, addr0x000000e4
addr0x000003f0: lw x27, -2037(x8)
addr0x000003f4: add x14, x14, x0
addr0x000003f8: addi x2, x20, -1465
addr0x000003fc: bltu x6, x0, addr0x000001a0
addr0x00000400: slli x20, x22, 7
addr0x00000404: lw x21, 734(x8)
addr0x00000408: add x30, x31, x19
addr0x0000040c: lw x13, -1216(x8)
addr0x00000410: lw x18, 428(x8)
addr0x00000414: lui x7, 8824
addr0x00000418: addi x22, x7, 816
addr0x0000041c: lw x18, 1712(x8)
addr0x00000420: lw x25, 105(x8)
addr0x00000424: lw x20, -360(x8)
addr0x00000428: addi x25, x18, 200
addr0x0000042c: bne x22, x9, addr0x0000009c
addr0x00000430: lw x25, -29(x8)
addr0x00000434: lw x5, -1369(x8)
addr0x00000438: lw x22, -406(x8)
addr0x0000043c: lw x18, -1694(x8)
addr0x00000440: sw x19, 1830(x8)
addr0x00000444: sw x23, 206(x8)
addr0x00000448: lui x14, 680323
addr0x0000044c: lui x30, 650046
addr0x00000450: addi x13, x18, 982
addr0x00000454: jal x2, addr0x000003a0
addr0x00000458: jal x2, addr0x000000f0
addr0x0000045c: bge x16, x22, addr0x0000075c
addr0x00000460: bgeu x9, x31, addr0x000002c8
addr0x00000464: lw x19, -123(x8)
addr0x00000468: addi x15, x11, 997
addr0x0000046c: addi x4, x21, 260
addr0x00000470: addi x27, x13, 580
addr0x00000474: jal x31, addr0x00000104
addr0x00000478: auipc x29, 83152
addr0x0000047c: addi x9, x11, -1508
addr0x00000480: sw x29, -620(x8)
addr0x00000484: sw x22, -2003(x8)
addr0x00000488: sw x13, -1439(x8)
addr0x0000048c: sw x28, -235(x8)
addr0x00000490: addi x15, x4, 715
addr0x00000494: lbu x28, 1359(x8)
addr0x00000498: add x22, x4, x19
addr0x0000049c: sw x9, 642(x8)
addr0x000004a0: addi x4, x27, 386
addr0x000004a4: add x11, x15, x5
addr0x000004a8: bgeu x17, x5, addr0x000006fc
addr0x000004ac: lw x23, -809(x8)
addr0x000004b0: lw x16, 1231(x8)
addr0x000004b4: jal x18, addr0x00000318
addr0x000004b8: lw x20, -442(x8)
addr0x000004bc: lw x30, -1851(x8)
addr0x000004c0: sb x18, 152(x8)
addr0x000004c4: lw x30, 655(x8)
addr0x000004c8: ori x27, x29, 1063
addr0x000004cc: lw x23, 517(x8)
addr0x000004d0: sw x26, -1576(x8)
addr0x000004d4: jal x27, addr0x00000060
addr0x000004d8: lw x22, -736(x8)
addr0x000004dc: addi x25, x7, 804
addr0x000004e0: add x9, x7, x23
addr0x000004e4: jal x26, addr0x00000528
addr0x000004e8: jal x10, addr0x0000055c
addr0x000004ec: addi x15, x14, -25
addr0x000004f0: addi x23, x26, 604
addr0x000004f4: sw x16, 1146(x8)
addr0x000004f8: sw x7, -756(x8)
addr0x000004fc: sw x31, 446(x8)
addr0x00000500: sw x4, -2037(x8)
addr0x00000504: jal x31, addr0x00000730
addr0x00000508: jal x4, addr0x00000478
addr0x0000050c: addi x22, x29, 1388
addr0x00000510: add x13, x4, x15
addr0x00000514: jal x7, addr0x0000037c
addr0x00000518: addi x31, x28, -265
addr0x0000051c: ori x9, x13, 1324
addr0x00000520: addi x12, x24, -1039
addr0x00000524: sw x0, 1673(x8)
addr0x00000528: sw x4, 1745(x8)
addr0x0000052c: jal x23, addr0x000002fc
addr0x00000530: sw x17, -1972(x8)
addr0x00000534: sw x23, -1428(x8)
addr0x00000538: jal x20, addr0x00000578
addr0x0000053c: jal x28, addr0x000002ec
addr0x00000540: lui x14, 277740
addr0x00000544: sw x6, 828(x8)
addr0x00000548: jal x7, addr0x00000260
addr0x0000054c: andi x21, x17, -497
addr0x00000550: sw x21, -53(x8)
addr0x00000554: sw x28, -290(x8)
addr0x00000558: addi x19, x19, 348
addr0x0000055c: sw x2, -874(x8)
addr0x00000560: sw x15, 6(x8)
addr0x00000564: jal x5, addr0x00000274
addr0x00000568: addi x29, x10, 1736
addr0x0000056c: addi x5, x7, -168
addr0x00000570: beq x1, x5, addr0x000003ac
addr0x00000574: jal x29, addr0x0000000c
addr0x00000578: ori x21, x13, 1777
addr0x0000057c: sw x0, -299(x8)
addr0x00000580: ori x29, x18, 1759
addr0x00000584: addi x14, x22, -2028
addr0x00000588: jal x7, addr0x000005e4
addr0x0000058c: addi x26, x19, 408
addr0x00000590: addi x10, x29, 1412
addr0x00000594: bne x4, x24, addr0x0000013c
addr0x00000598: addi x20, x9, -1058
addr0x0000059c: lw x27, -120(x8)
addr0x000005a0: lw x27, -139(x8)
addr0x000005a4: jal x20, addr0x000002fc
addr0x000005a8: beq x20, x5, addr0x00000650
addr0x000005ac: andi x19, x1, -1722
addr0x000005b0: addi x7, x24, 268
addr0x000005b4: jal x14, addr0x000001c8
addr0x000005b8: lw x25, -1400(x8)
addr0x000005bc: lw x5, -113(x8)
addr0x000005c0: jal x29, addr0x000002c0
addr0x000005c4: slli x21, x23, 16
addr0x000005c8: lw x31, -694(x8)
addr0x000005cc: sw x15, 1671(x8)
addr0x000005d0: lw x2, 794(x8)
addr0x000005d4: lw x23, -547(x8)
addr0x000005d8: add x13, x17, x31
addr0x000005dc: lw x23, 1595(x8)
addr0x000005e0: lw x18, -1643(x8)
addr0x000005e4: lw x14, 1812(x8)
addr0x000005e8: lw x1, 1895(x8)
addr0x000005ec: sw x23, -829(x8)
addr0x000005f0: lbu x9, -1189(x8)
addr0x000005f4: beq x23, x31, addr0x000000c4
addr0x000005f8: lw x27, -935(x8)
addr0x000005fc: addi x17, x22, 1688
addr0x00000600: sw x0, -648(x8)
addr0x00000604: sw x9, 317(x8)
addr0x00000608: sw x6, 1147(x8)
addr0x0000060c: sw x13, -1840(x8)
addr0x00000610: jal x13, addr0x000004e4
addr0x00000614: jal x19, addr0x0000018c
addr0x00000618: sw x5, -1707(x8)
addr0x0000061c: jal x22, addr0x000000d8
addr0x00000620: lbu x30, -952(x8)
addr0x00000624: sb x0, 1102(x8)
addr0x00000628: lw x31, 2046(x8)
addr0x0000062c: lw x24, 1347(x8)
addr0x00000630: bltu x0, x20, addr0x00000260
addr0x00000634: bne x1, x30, addr0x0000077c
addr0x00000638: lw x0, 2001(x8)
addr0x0000063c: sw x28, -1978(x8)
addr0x00000640: sw x20, -305(x8)
addr0x00000644: lui x26, 698870
addr0x00000648: beq x30, x23, addr0x00000668
addr0x0000064c: sw x28, 1028(x8)
addr0x00000650: jal x18, addr0x00000218
addr0x00000654: jal x19, addr0x00000338
addr0x00000658: lw x26, -861(x8)
addr0x0000065c: lw x14, 1292(x8)
addr0x00000660: sw x13, 1169(x8)
addr0x00000664: sw x25, -320(x8)
addr0x00000668: sw x14, 499(x8)
addr0x0000066c: lw x0, 481(x8)
addr0x00000670: beq x10, x7, addr0x00000554
addr0x00000674: addi x25, x13, 7
addr0x00000678: lui x14, 995451
addr0x0000067c: addi x1, x18, 429
addr0x00000680: sw x24, -133(x8)
addr0x00000684: addi x27, x5, 1743
addr0x00000688: sw x9, 204(x8)
addr0x0000068c: slli x17, x28, 24
addr0x00000690: add x20, x2, x16
addr0x00000694: beq x17, x9, addr0x00000400
addr0x00000698: bne x20, x9, addr0x0000044c
addr0x0000069c: lb x28, 1465(x8)
addr0x000006a0: jal x27, addr0x00000674
addr0x000006a4: lw x27, -89(x8)
addr0x000006a8: lw x9, -1197(x8)
addr0x000006ac: lw x18, 76(x8)
addr0x000006b0: addi x0, x18, 1392
addr0x000006b4: sw x20, -1521(x8)
addr0x000006b8: sw x7, -278(x8)
addr0x000006bc: lw x4, 18(x8)
addr0x000006c0: lw x22, 1673(x8)
addr0x000006c4: addi x9, x19, 551
addr0x000006c8: lw x29, -1842(x8)
addr0x000006cc: lbu x2, 991(x8)
addr0x000006d0: beq x29, x20, addr0x00000774
addr0x000006d4: addi x18, x27, 1192
addr0x000006d8: addi x11, x1, 1173
addr0x000006dc: jal x15, addr0x00000110
addr0x000006e0: lbu x12, -1355(x8)
addr0x000006e4: lui x22, 846221
addr0x000006e8: addi x22, x28, 106
addr0x000006ec: jal x10, addr0x000006d4
addr0x000006f0: lw x14, -357(x8)
addr0x000006f4: addi x7, x29, 1140
addr0x000006f8: sw x23, 450(x8)
addr0x000006fc: bgeu x12, x18, addr0x00000118
addr0x00000700: sw x29, 325(x8)
addr0x00000704: sb x17, -1714(x8)
addr0x00000708: bne x18, x13, addr0x000005d8
addr0x0000070c: addi x2, x21, -956
addr0x00000710: sw x16, -957(x8)
addr0x00000714: sw x9, 1381(x8)
addr0x00000718: lw x2, -1809(x8)
addr0x0000071c: bge x21, x31, addr0x0000004c
addr0x00000720: addi x13, x20, 1994
addr0x00000724: lw x9, -954(x8)
addr0x00000728: jal x19, addr0x00000550
addr0x0000072c: sw x7, 1730(x8)
addr0x00000730: jal x15, addr0x000001a8
addr0x00000734: sw x29, -1413(x8)
addr0x00000738: jal x30, addr0x00000714
addr0x0000073c: lw x29, -1403(x8)
addr0x00000740: andi x29, x25, 1642
addr0x00000744: sw x19, 965(x8)
addr0x00000748: sw x23, -1611(x8)
addr0x0000074c: sw x6, 965(x8)
addr0x00000750: sw x21, -186(x8)
addr0x00000754: lbu x5, 1547(x8)
addr0x00000758: lbu x17, -702(x8)
addr0x0000075c: lui x16, 434577
addr0x00000760: addi x31, x19, -276
addr0x00000764: lui x16, 772637
addr0x00000768: addi x27, x0, 75
addr0x0000076c: jal x20, addr0x00000410
addr0x00000770: lui x15, 199714
addr0x00000774: addi x23, x9, 1644
addr0x00000778: bne x23, x9, addr0x000002b0
addr0x0000077c: lw x18, 498(x8)
addr0x00000780: lw x19, 365(x8)
addr0x00000784: lbu x13, -1018(x8)
addr0x00000788: slli x0, x14, 18
addr0x0000078c: bltu x16, x13, addr0x0000004c
