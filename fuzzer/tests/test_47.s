.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: bne x28, x18, addr0x000006e8
addr0x0000001c: jal x4, addr0x00000358
addr0x00000020: sw x11, -1497(x8)
addr0x00000024: beq x31, x19, addr0x000002fc
addr0x00000028: lui x14, 41547
addr0x0000002c: addi x14, x23, 1103
addr0x00000030: addi x14, x26, -347
addr0x00000034: lw x31, -1687(x8)
addr0x00000038: slli x10, x19, 6
addr0x0000003c: or x24, x17, x7
addr0x00000040: srli x12, x7, 19
addr0x00000044: add x23, x23, x10
addr0x00000048: addi x14, x31, -365
addr0x0000004c: bne x11, x27, addr0x000005b8
addr0x00000050: sw x19, -422(x8)
addr0x00000054: bge x23, x28, addr0x000006cc
addr0x00000058: lw x12, 1796(x8)
addr0x0000005c: addi x19, x31, -735
addr0x00000060: lw x7, 759(x8)
addr0x00000064: sw x28, -2001(x8)
addr0x00000068: sw x23, -891(x8)
addr0x0000006c: sw x20, -144(x8)
addr0x00000070: lui x27, 832194
addr0x00000074: slli x29, x14, 2
addr0x00000078: lui x27, 172839
addr0x0000007c: addi x28, x18, -661
addr0x00000080: jal x20, addr0x00000430
addr0x00000084: jal x18, addr0x00000640
addr0x00000088: jal x21, addr0x000005f0
addr0x0000008c: lw x10, 25(x8)
addr0x00000090: beq x0, x2, addr0x0000005c
addr0x00000094: lbu x25, 426(x8)
addr0x00000098: sw x17, -701(x8)
addr0x0000009c: sw x26, -1914(x8)
addr0x000000a0: sb x19, 1373(x8)
addr0x000000a4: sw x23, 1803(x8)
addr0x000000a8: bge x9, x23, addr0x000004cc
addr0x000000ac: slli x11, x30, 21
addr0x000000b0: addi x16, x26, 1482
addr0x000000b4: lw x30, 1138(x8)
addr0x000000b8: lw x20, 1822(x8)
addr0x000000bc: jal x10, addr0x00000288
addr0x000000c0: sw x5, -772(x8)
addr0x000000c4: jal x7, addr0x0000020c
addr0x000000c8: bne x5, x27, addr0x0000069c
addr0x000000cc: lw x14, 660(x8)
addr0x000000d0: lbu x15, -1102(x8)
addr0x000000d4: lbu x2, 565(x8)
addr0x000000d8: sb x20, -1412(x8)
addr0x000000dc: andi x7, x29, -485
addr0x000000e0: lw x4, -1873(x8)
addr0x000000e4: lw x9, -1707(x8)
addr0x000000e8: addi x27, x0, -918
addr0x000000ec: jal x23, addr0x0000054c
addr0x000000f0: sh x16, 1733(x8)
addr0x000000f4: jal x17, addr0x00000494
addr0x000000f8: jal x20, addr0x000004c4
addr0x000000fc: jal x17, addr0x000006a8
addr0x00000100: lui x14, 421103
addr0x00000104: lui x9, 742787
addr0x00000108: jal x14, addr0x00000150
addr0x0000010c: addi x25, x13, 1660
addr0x00000110: lw x22, -6(x8)
addr0x00000114: lui x5, 133550
addr0x00000118: add x19, x9, x2
addr0x0000011c: lbu x28, 1978(x8)
addr0x00000120: lui x5, 509204
addr0x00000124: addi x19, x25, 474
addr0x00000128: add x30, x29, x10
addr0x0000012c: jal x11, addr0x0000010c
addr0x00000130: sw x23, 1942(x8)
addr0x00000134: lbu x2, 640(x8)
addr0x00000138: lw x5, -883(x8)
addr0x0000013c: addi x23, x30, 1684
addr0x00000140: addi x15, x19, -1345
addr0x00000144: jal x7, addr0x00000038
addr0x00000148: lw x17, 1568(x8)
addr0x0000014c: lw x9, -502(x8)
addr0x00000150: lw x16, -359(x8)
addr0x00000154: addi x1, x14, -410
addr0x00000158: bne x2, x30, addr0x00000298
addr0x0000015c: lw x27, -1932(x8)
addr0x00000160: jal x26, addr0x000004b4
addr0x00000164: lw x25, -1808(x8)
addr0x00000168: addi x19, x11, 84
addr0x0000016c: sw x0, -269(x8)
addr0x00000170: jal x18, addr0x00000710
addr0x00000174: addi x6, x29, 1558
addr0x00000178: sw x7, 31(x8)
addr0x0000017c: jal x22, addr0x000001cc
addr0x00000180: jal x31, addr0x00000128
addr0x00000184: lhu x6, 1090(x8)
addr0x00000188: lw x4, -1960(x8)
addr0x0000018c: sb x6, 1842(x8)
addr0x00000190: lw x13, -645(x8)
addr0x00000194: add x9, x28, x13
addr0x00000198: lhu x31, 188(x8)
addr0x0000019c: srli x22, x17, 8
addr0x000001a0: bge x26, x27, addr0x00000538
addr0x000001a4: lw x20, -975(x8)
addr0x000001a8: lw x20, 54(x8)
addr0x000001ac: lw x25, -461(x8)
addr0x000001b0: lbu x28, -1559(x8)
addr0x000001b4: ori x24, x23, 968
addr0x000001b8: lbu x15, -1699(x8)
addr0x000001bc: lui x23, 913714
addr0x000001c0: sw x20, 388(x8)
addr0x000001c4: lw x19, -1523(x8)
addr0x000001c8: jal x17, addr0x0000064c
addr0x000001cc: lui x22, 761375
addr0x000001d0: sw x22, -1400(x8)
addr0x000001d4: jal x18, addr0x000000c8
addr0x000001d8: jal x20, addr0x000005d4
addr0x000001dc: jal x6, addr0x0000034c
addr0x000001e0: addi x9, x5, -399
addr0x000001e4: add x31, x17, x17
addr0x000001e8: sw x25, 1449(x8)
addr0x000001ec: jal x4, addr0x0000033c
addr0x000001f0: lw x22, 624(x8)
addr0x000001f4: lw x5, -72(x8)
addr0x000001f8: addi x7, x31, 1013
addr0x000001fc: sw x26, 1745(x8)
addr0x00000200: addi x11, x7, -1885
addr0x00000204: sw x30, -383(x8)
addr0x00000208: addi x14, x31, 723
addr0x0000020c: addi x17, x22, 1366
addr0x00000210: sw x0, -295(x8)
addr0x00000214: jal x2, addr0x000002a8
addr0x00000218: lw x26, -664(x8)
addr0x0000021c: slli x9, x31, 12
addr0x00000220: lbu x26, 467(x8)
addr0x00000224: lbu x13, 1074(x8)
addr0x00000228: addi x26, x10, -1993
addr0x0000022c: lw x19, 610(x8)
addr0x00000230: lw x7, -123(x8)
addr0x00000234: lw x0, 1746(x8)
addr0x00000238: lw x30, 1570(x8)
addr0x0000023c: lw x27, 588(x8)
addr0x00000240: lw x22, 1266(x8)
addr0x00000244: lw x5, -1029(x8)
addr0x00000248: lw x27, 260(x8)
addr0x0000024c: lw x13, -596(x8)
addr0x00000250: addi x26, x21, -1412
addr0x00000254: sw x22, -172(x8)
addr0x00000258: lui x18, 654063
addr0x0000025c: addi x18, x0, 1310
addr0x00000260: jal x26, addr0x000000c8
addr0x00000264: addi x27, x6, 1953
addr0x00000268: jal x5, addr0x00000294
addr0x0000026c: addi x19, x14, -940
addr0x00000270: beq x17, x4, addr0x00000400
addr0x00000274: bne x14, x30, addr0x000005b4
addr0x00000278: beq x26, x30, addr0x0000041c
addr0x0000027c: add x2, x14, x13
addr0x00000280: beq x23, x4, addr0x00000420
addr0x00000284: jal x6, addr0x000001c0
addr0x00000288: jal x17, addr0x000006b4
addr0x0000028c: lw x5, 956(x8)
addr0x00000290: addi x10, x17, 1215
addr0x00000294: sw x14, -2034(x8)
addr0x00000298: addi x7, x26, -1521
addr0x0000029c: addi x31, x5, -1173
addr0x000002a0: addi x12, x29, -1199
addr0x000002a4: jal x4, addr0x00000570
addr0x000002a8: addi x27, x2, -765
addr0x000002ac: sw x13, 238(x8)
addr0x000002b0: sw x6, -60(x8)
addr0x000002b4: lw x2, 394(x8)
addr0x000002b8: andi x9, x2, -1329
addr0x000002bc: jal x23, addr0x00000368
addr0x000002c0: lw x1, 896(x8)
addr0x000002c4: lui x4, 108032
addr0x000002c8: addi x13, x19, 1018
addr0x000002cc: lw x18, -1779(x8)
addr0x000002d0: lw x4, -1828(x8)
addr0x000002d4: lw x19, -1113(x8)
addr0x000002d8: sw x13, 30(x8)
addr0x000002dc: bltu x19, x17, addr0x00000318
addr0x000002e0: lw x22, -1047(x8)
addr0x000002e4: lw x25, -415(x8)
addr0x000002e8: jalr x0, 448(x3)
addr0x000002ec: sw x2, -1970(x8)
addr0x000002f0: jal x21, addr0x000002e0
addr0x000002f4: addi x19, x5, 832
addr0x000002f8: lw x16, 1912(x8)
addr0x000002fc: sw x6, -1900(x8)
addr0x00000300: sw x20, -1178(x8)
addr0x00000304: addi x23, x22, -1105
addr0x00000308: jal x15, addr0x00000428
addr0x0000030c: bltu x29, x16, addr0x000003f4
addr0x00000310: sb x18, 1960(x8)
addr0x00000314: jal x19, addr0x000001e8
addr0x00000318: lw x19, 1471(x8)
addr0x0000031c: lw x27, -1927(x8)
addr0x00000320: lw x15, 1313(x8)
addr0x00000324: lw x17, 766(x8)
addr0x00000328: lw x19, -1763(x8)
addr0x0000032c: addi x19, x29, 50
addr0x00000330: addi x19, x0, 809
addr0x00000334: lw x23, -1284(x8)
addr0x00000338: and x15, x13, x0
addr0x0000033c: lh x23, -987(x8)
addr0x00000340: auipc x16, 279883
addr0x00000344: addi x20, x22, -1270
addr0x00000348: sw x23, 1779(x8)
addr0x0000034c: jal x13, addr0x0000062c
addr0x00000350: addi x10, x4, -911
addr0x00000354: sw x18, 1170(x8)
addr0x00000358: jal x14, addr0x000004a0
addr0x0000035c: lw x14, -1963(x8)
addr0x00000360: sw x27, 337(x8)
addr0x00000364: addi x23, x30, 1980
addr0x00000368: sw x0, -490(x8)
addr0x0000036c: sw x21, -1431(x8)
addr0x00000370: sw x0, -755(x8)
addr0x00000374: jal x26, addr0x0000033c
addr0x00000378: jal x5, addr0x00000554
addr0x0000037c: sw x18, 1543(x8)
addr0x00000380: lw x25, -1828(x8)
addr0x00000384: lw x27, -942(x8)
addr0x00000388: lw x7, -1361(x8)
addr0x0000038c: sw x13, 208(x8)
addr0x00000390: sw x31, 1968(x8)
addr0x00000394: blt x18, x18, addr0x00000708
addr0x00000398: beq x13, x13, addr0x00000678
addr0x0000039c: lw x23, -1058(x8)
addr0x000003a0: sw x18, -1927(x8)
addr0x000003a4: sw x23, 509(x8)
addr0x000003a8: addi x18, x17, 777
addr0x000003ac: sw x28, -645(x8)
addr0x000003b0: lw x9, -1440(x8)
addr0x000003b4: addi x9, x26, -1149
addr0x000003b8: sub x7, x9, x7
addr0x000003bc: beq x17, x26, addr0x00000280
addr0x000003c0: lbu x13, -70(x8)
addr0x000003c4: addi x18, x20, -1918
addr0x000003c8: sh x1, -1765(x8)
addr0x000003cc: lbu x9, 119(x8)
addr0x000003d0: add x22, x28, x5
addr0x000003d4: bgeu x21, x31, addr0x000004c4
addr0x000003d8: jal x9, addr0x00000068
addr0x000003dc: lw x5, -351(x8)
addr0x000003e0: sw x15, 294(x8)
addr0x000003e4: jal x22, addr0x000002e8
addr0x000003e8: lw x23, 1800(x8)
addr0x000003ec: addi x23, x13, 1425
addr0x000003f0: sw x19, -1977(x8)
addr0x000003f4: lw x9, 1522(x8)
addr0x000003f8: sw x19, 515(x8)
addr0x000003fc: jal x16, addr0x000001f4
addr0x00000400: lw x20, -1396(x8)
addr0x00000404: lw x7, -921(x8)
addr0x00000408: sw x24, 1213(x8)
addr0x0000040c: bltu x11, x23, addr0x00000200
addr0x00000410: addi x31, x7, 203
addr0x00000414: sw x9, 524(x8)
addr0x00000418: addi x27, x24, -1901
addr0x0000041c: sw x0, -196(x8)
addr0x00000420: sw x28, 399(x8)
addr0x00000424: sw x22, 1057(x8)
addr0x00000428: jal x27, addr0x00000244
addr0x0000042c: lw x20, 982(x8)
addr0x00000430: sw x13, -871(x8)
addr0x00000434: addi x0, x4, -1104
addr0x00000438: sw x19, -57(x8)
addr0x0000043c: jal x7, addr0x00000228
addr0x00000440: sw x7, 488(x8)
addr0x00000444: sw x27, -722(x8)
addr0x00000448: jal x19, addr0x0000065c
addr0x0000044c: lw x17, 1777(x8)
addr0x00000450: bltu x17, x28, addr0x0000015c
addr0x00000454: sb x29, -380(x8)
addr0x00000458: lbu x12, 989(x8)
addr0x0000045c: jal x22, addr0x00000504
addr0x00000460: lw x2, 1297(x8)
addr0x00000464: bge x25, x13, addr0x000000d8
addr0x00000468: andi x24, x10, -737
addr0x0000046c: or x0, x11, x28
addr0x00000470: lbu x20, 876(x8)
addr0x00000474: sw x19, 1736(x8)
addr0x00000478: sw x16, 1686(x8)
addr0x0000047c: lbu x22, -353(x8)
addr0x00000480: lw x22, 1814(x8)
addr0x00000484: jal x25, addr0x00000448
addr0x00000488: sw x6, 1083(x8)
addr0x0000048c: sw x27, -1632(x8)
addr0x00000490: bne x15, x12, addr0x00000260
addr0x00000494: lw x7, -836(x8)
addr0x00000498: addi x18, x1, 412
addr0x0000049c: lw x17, -168(x8)
addr0x000004a0: sw x30, 1586(x8)
addr0x000004a4: sw x13, -1264(x8)
addr0x000004a8: lw x30, -720(x8)
addr0x000004ac: addi x18, x7, 189
addr0x000004b0: jal x5, addr0x0000030c
addr0x000004b4: sw x20, 988(x8)
addr0x000004b8: sw x11, 830(x8)
addr0x000004bc: lw x20, -516(x8)
addr0x000004c0: lw x30, 1598(x8)
addr0x000004c4: addi x21, x9, 1032
addr0x000004c8: lhu x18, 1402(x8)
addr0x000004cc: lui x28, 383148
addr0x000004d0: addi x23, x29, -947
addr0x000004d4: lw x7, 1721(x8)
addr0x000004d8: lui x29, 457013
addr0x000004dc: addi x17, x21, 1968
addr0x000004e0: sw x0, 1467(x8)
addr0x000004e4: bgeu x7, x23, addr0x00000030
addr0x000004e8: lw x5, -1925(x8)
addr0x000004ec: add x15, x18, x1
addr0x000004f0: addi x22, x31, -1397
addr0x000004f4: addi x7, x14, -1921
addr0x000004f8: sw x22, 198(x8)
addr0x000004fc: sw x29, -753(x8)
addr0x00000500: lw x28, 597(x8)
addr0x00000504: beq x19, x19, addr0x00000238
addr0x00000508: lui x18, 849123
addr0x0000050c: addi x2, x6, -1404
addr0x00000510: add x26, x22, x25
addr0x00000514: jal x21, addr0x00000578
addr0x00000518: lw x0, -1169(x8)
addr0x0000051c: addi x22, x22, 1915
addr0x00000520: jal x30, addr0x00000300
addr0x00000524: lw x14, 1462(x8)
addr0x00000528: lw x15, -1262(x8)
addr0x0000052c: sw x24, 1938(x8)
addr0x00000530: sw x27, 1365(x8)
addr0x00000534: lw x31, -1632(x8)
addr0x00000538: bne x19, x31, addr0x00000224
addr0x0000053c: jal x23, addr0x000000ac
addr0x00000540: lw x29, -1904(x8)
addr0x00000544: sw x5, 1186(x8)
addr0x00000548: addi x18, x23, -1724
addr0x0000054c: sb x11, 717(x8)
addr0x00000550: sb x24, 1907(x8)
addr0x00000554: blt x29, x23, addr0x00000350
addr0x00000558: andi x1, x28, -229
addr0x0000055c: or x6, x5, x20
addr0x00000560: lw x4, 1590(x8)
addr0x00000564: bne x23, x0, addr0x00000144
addr0x00000568: sh x7, 1061(x8)
addr0x0000056c: bne x23, x13, addr0x0000074c
addr0x00000570: andi x16, x23, 787
addr0x00000574: addi x13, x29, 303
addr0x00000578: bge x2, x23, addr0x00000078
addr0x0000057c: lui x10, 420688
addr0x00000580: addi x9, x31, 572
addr0x00000584: sw x12, 1507(x8)
addr0x00000588: lbu x23, 1366(x8)
addr0x0000058c: ori x25, x29, 1619
addr0x00000590: lbu x5, 1405(x8)
addr0x00000594: jal x21, addr0x000005f8
addr0x00000598: sw x17, -424(x8)
addr0x0000059c: sw x14, -574(x8)
addr0x000005a0: jal x10, addr0x000000b8
addr0x000005a4: jal x9, addr0x000004ec
addr0x000005a8: lw x22, -1595(x8)
addr0x000005ac: addi x2, x29, -141
addr0x000005b0: add x27, x9, x2
addr0x000005b4: jal x21, addr0x000006bc
addr0x000005b8: add x31, x4, x0
addr0x000005bc: lh x14, -1093(x8)
addr0x000005c0: lh x7, -899(x8)
addr0x000005c4: lui x17, 679764
addr0x000005c8: addi x21, x15, 320
addr0x000005cc: sw x29, -1319(x8)
addr0x000005d0: bne x19, x29, addr0x00000294
addr0x000005d4: lw x19, -1267(x8)
addr0x000005d8: sw x12, -990(x8)
addr0x000005dc: sw x10, -1951(x8)
addr0x000005e0: sw x31, 949(x8)
addr0x000005e4: jal x28, addr0x000001e0
addr0x000005e8: lui x30, 335237
addr0x000005ec: sw x4, -794(x8)
addr0x000005f0: bne x12, x14, addr0x00000174
addr0x000005f4: bne x29, x5, addr0x000000d8
addr0x000005f8: divu x23, x27, x16
addr0x000005fc: blt x10, x18, addr0x000004b0
addr0x00000600: lw x5, 902(x8)
addr0x00000604: jal x24, addr0x000006d8
addr0x00000608: jal x12, addr0x0000032c
addr0x0000060c: lw x12, 829(x8)
addr0x00000610: jal x15, addr0x00000488
addr0x00000614: lw x11, -23(x8)
addr0x00000618: lw x17, -703(x8)
addr0x0000061c: lw x9, -1718(x8)
addr0x00000620: addi x0, x14, -1410
addr0x00000624: jal x5, addr0x00000620
addr0x00000628: jal x31, addr0x00000160
addr0x0000062c: lw x12, 1278(x8)
addr0x00000630: lw x12, -1490(x8)
addr0x00000634: bne x22, x31, addr0x000005c8
addr0x00000638: lw x22, 1240(x8)
addr0x0000063c: sw x17, 773(x8)
addr0x00000640: sh x23, 1691(x8)
addr0x00000644: bne x6, x25, addr0x00000444
addr0x00000648: lw x10, 1431(x8)
addr0x0000064c: jal x26, addr0x00000294
addr0x00000650: jal x24, addr0x00000314
addr0x00000654: addi x31, x25, -2006
addr0x00000658: sw x29, 527(x8)
addr0x0000065c: jal x20, addr0x0000050c
addr0x00000660: lw x0, -1437(x8)
addr0x00000664: lw x19, -1200(x8)
addr0x00000668: jal x23, addr0x0000043c
addr0x0000066c: lbu x13, -1081(x8)
addr0x00000670: ori x29, x19, 2019
addr0x00000674: sw x9, -1671(x8)
addr0x00000678: lui x20, 804327
addr0x0000067c: addi x15, x18, -1647
addr0x00000680: beq x29, x0, addr0x000002dc
addr0x00000684: sw x18, -1286(x8)
addr0x00000688: sw x22, 1309(x8)
addr0x0000068c: sw x18, 932(x8)
addr0x00000690: addi x29, x27, 271
addr0x00000694: jal x26, addr0x00000108
addr0x00000698: sw x23, -60(x8)
addr0x0000069c: sw x31, -1560(x8)
addr0x000006a0: sw x14, 1925(x8)
addr0x000006a4: sw x4, -1176(x8)
addr0x000006a8: addi x12, x14, 1338
addr0x000006ac: jal x12, addr0x000005d8
addr0x000006b0: lw x16, -1684(x8)
addr0x000006b4: addi x30, x31, 1309
addr0x000006b8: sw x1, 1957(x8)
addr0x000006bc: addi x29, x9, -474
addr0x000006c0: lui x19, 946819
addr0x000006c4: addi x18, x29, -1570
addr0x000006c8: sw x31, 603(x8)
addr0x000006cc: jal x24, addr0x000004d4
addr0x000006d0: lw x28, 1536(x8)
addr0x000006d4: jal x16, addr0x000001c4
addr0x000006d8: lw x9, -580(x8)
addr0x000006dc: lw x9, 3(x8)
addr0x000006e0: lw x14, 1483(x8)
addr0x000006e4: lw x22, -1228(x8)
addr0x000006e8: lw x5, 25(x8)
addr0x000006ec: add x1, x20, x23
addr0x000006f0: lw x29, 941(x8)
addr0x000006f4: lw x0, 1756(x8)
addr0x000006f8: lw x20, -1296(x8)
addr0x000006fc: lw x9, -699(x8)
addr0x00000700: addi x29, x18, -506
addr0x00000704: sw x18, -389(x8)
addr0x00000708: lw x9, -1445(x8)
addr0x0000070c: sw x29, 1340(x8)
addr0x00000710: sw x9, 237(x8)
addr0x00000714: sw x2, 1814(x8)
addr0x00000718: sw x7, -349(x8)
addr0x0000071c: addi x21, x5, 207
addr0x00000720: jal x13, addr0x00000474
addr0x00000724: jal x7, addr0x00000398
addr0x00000728: lui x31, 837776
addr0x0000072c: sw x0, 228(x8)
addr0x00000730: jal x30, addr0x000004b4
addr0x00000734: lbu x23, -395(x8)
addr0x00000738: andi x6, x0, 327
addr0x0000073c: addi x17, x1, 1611
addr0x00000740: addi x30, x18, -626
addr0x00000744: addi x28, x11, 1656
addr0x00000748: jal x1, addr0x000005dc
addr0x0000074c: jal x5, addr0x000001d8
addr0x00000750: lw x30, -41(x8)
