.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: rem x26, x4, x4
addr0x0000001c: lw x0, 1057(x8)
addr0x00000020: addi x25, x25, 2042
addr0x00000024: sw x20, 614(x8)
addr0x00000028: lw x18, 975(x8)
addr0x0000002c: lw x5, -1764(x8)
addr0x00000030: lbu x9, -1805(x8)
addr0x00000034: srli x5, x9, 16
addr0x00000038: andi x28, x19, 1368
addr0x0000003c: lw x29, -926(x8)
addr0x00000040: sw x21, -63(x8)
addr0x00000044: lui x24, 756266
addr0x00000048: lui x14, 533055
addr0x0000004c: addi x1, x23, 1653
addr0x00000050: andi x9, x13, -392
addr0x00000054: bne x13, x19, addr0x00000070
addr0x00000058: lw x30, 1782(x8)
addr0x0000005c: sw x22, -1841(x8)
addr0x00000060: lw x20, 1349(x8)
addr0x00000064: sw x7, -1308(x8)
addr0x00000068: sw x29, -1203(x8)
addr0x0000006c: sw x13, -888(x8)
addr0x00000070: beq x20, x5, addr0x00000090
addr0x00000074: sw x27, -627(x8)
addr0x00000078: jal x29, addr0x000000c8
addr0x0000007c: lw x6, 29(x8)
addr0x00000080: lw x22, -1371(x8)
addr0x00000084: addi x26, x13, 1711
addr0x00000088: addi x26, x1, -40
addr0x0000008c: jal x14, addr0x00000014
addr0x00000090: lw x21, 1601(x8)
addr0x00000094: lw x21, 811(x8)
addr0x00000098: lw x14, -8(x8)
addr0x0000009c: andi x0, x17, -1807
addr0x000000a0: bltu x2, x4, addr0x0000002c
addr0x000000a4: mul x7, x7, x21
addr0x000000a8: bltu x10, x23, addr0x0000006c
addr0x000000ac: lbu x1, 1876(x8)
addr0x000000b0: addi x5, x4, 1497
addr0x000000b4: sw x17, 2013(x8)
addr0x000000b8: sw x19, -20(x8)
addr0x000000bc: lw x0, -606(x8)
addr0x000000c0: add x7, x23, x19
addr0x000000c4: sub x23, x9, x19
addr0x000000c8: jal x11, addr0x00000090
addr0x000000cc: lw x11, 1062(x8)
addr0x000000d0: lw x22, -1(x8)
addr0x000000d4: lw x2, 276(x8)
addr0x000000d8: beq x6, x7, addr0x00000038
addr0x000000dc: addi x29, x27, -895
addr0x000000e0: bltu x18, x21, addr0x00000018
