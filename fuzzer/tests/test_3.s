.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: sw x7, -272(x8)
addr0x0000001c: jal x19, addr0x0000004c
addr0x00000020: lw x22, -1426(x8)
addr0x00000024: sw x19, -1140(x8)
addr0x00000028: lui x5, 229060
addr0x0000002c: lui x24, 942629
addr0x00000030: jal x29, addr0x00000ac8
addr0x00000034: lui x17, 154846
addr0x00000038: sw x29, 1143(x8)
addr0x0000003c: sw x5, 1884(x8)
addr0x00000040: sw x10, -1006(x8)
addr0x00000044: jal x30, addr0x00000230
addr0x00000048: lw x18, -212(x8)
addr0x0000004c: lw x9, -952(x8)
addr0x00000050: lw x29, -1096(x8)
addr0x00000054: jal x16, addr0x0000044c
addr0x00000058: jal x18, addr0x0000079c
addr0x0000005c: sw x4, 1764(x8)
addr0x00000060: sw x22, 451(x8)
addr0x00000064: addi x17, x0, 726
addr0x00000068: jal x29, addr0x00000b78
addr0x0000006c: sw x15, -533(x8)
addr0x00000070: jal x6, addr0x000005d0
addr0x00000074: sw x23, 297(x8)
addr0x00000078: sw x29, -1569(x8)
addr0x0000007c: lui x24, 710012
addr0x00000080: addi x19, x10, -782
addr0x00000084: sw x22, -1690(x8)
addr0x00000088: sw x24, -1550(x8)
addr0x0000008c: sw x22, 19(x8)
addr0x00000090: addi x10, x29, 1732
addr0x00000094: addi x18, x9, 440
addr0x00000098: addi x0, x10, 142
addr0x0000009c: sw x9, 1137(x8)
addr0x000000a0: lw x15, -1305(x8)
addr0x000000a4: sw x5, 966(x8)
addr0x000000a8: blt x30, x5, addr0x00000458
addr0x000000ac: andi x15, x0, 441
addr0x000000b0: slli x17, x1, 29
addr0x000000b4: sw x29, 728(x8)
addr0x000000b8: lbu x23, -206(x8)
addr0x000000bc: sb x5, 142(x8)
addr0x000000c0: jal x19, addr0x00000804
addr0x000000c4: lw x5, -1552(x8)
addr0x000000c8: lw x29, 1857(x8)
addr0x000000cc: sw x29, -2007(x8)
addr0x000000d0: lw x19, 845(x8)
addr0x000000d4: lw x23, -284(x8)
addr0x000000d8: sw x30, 1397(x8)
addr0x000000dc: sw x6, -1569(x8)
addr0x000000e0: addi x7, x5, 1789
addr0x000000e4: sw x21, 7(x8)
addr0x000000e8: jal x21, addr0x00000a28
addr0x000000ec: bltu x19, x9, addr0x00000118
addr0x000000f0: srli x4, x9, 14
addr0x000000f4: lw x9, 618(x8)
addr0x000000f8: jal x29, addr0x00000b6c
addr0x000000fc: bgeu x0, x19, addr0x00000288
addr0x00000100: jal x30, addr0x00000988
addr0x00000104: add x9, x29, x23
addr0x00000108: sw x18, 1551(x8)
addr0x0000010c: sw x29, -1882(x8)
addr0x00000110: beq x0, x19, addr0x00000200
addr0x00000114: sw x29, -1748(x8)
addr0x00000118: add x20, x7, x23
addr0x0000011c: sb x22, -864(x8)
addr0x00000120: sb x30, -408(x8)
addr0x00000124: sb x4, 9(x8)
addr0x00000128: lw x4, -737(x8)
addr0x0000012c: lw x7, 651(x8)
addr0x00000130: lw x6, -1595(x8)
addr0x00000134: lw x0, 539(x8)
addr0x00000138: slli x6, x29, 24
addr0x0000013c: lw x22, -620(x8)
addr0x00000140: add x17, x0, x30
addr0x00000144: lw x19, -728(x8)
addr0x00000148: jalr x0, 8(x3)
addr0x0000014c: sw x29, 261(x8)
addr0x00000150: jal x22, addr0x00000ba8
addr0x00000154: lw x22, -1912(x8)
addr0x00000158: bne x19, x22, addr0x00000330
addr0x0000015c: addi x0, x7, -56
addr0x00000160: lui x25, 477439
addr0x00000164: lui x22, 694042
addr0x00000168: addi x21, x4, 1504
addr0x0000016c: sw x29, 1290(x8)
addr0x00000170: sw x6, 1962(x8)
addr0x00000174: addi x19, x30, -1831
addr0x00000178: sw x29, 1713(x8)
addr0x0000017c: sw x19, 1084(x8)
addr0x00000180: sw x17, -1598(x8)
addr0x00000184: bgeu x30, x4, addr0x000001f4
addr0x00000188: addi x6, x15, -861
addr0x0000018c: lw x18, -824(x8)
addr0x00000190: addi x5, x19, 1616
addr0x00000194: jal x22, addr0x00000060
addr0x00000198: lw x22, 39(x8)
addr0x0000019c: slli x7, x23, 19
addr0x000001a0: srli x22, x6, 10
addr0x000001a4: divu x9, x22, x15
addr0x000001a8: slli x0, x30, 18
addr0x000001ac: srli x5, x23, 27
addr0x000001b0: divu x22, x17, x25
addr0x000001b4: sw x10, 1402(x8)
addr0x000001b8: sw x9, 1379(x8)
addr0x000001bc: sw x23, 1653(x8)
addr0x000001c0: sb x21, -1546(x8)
addr0x000001c4: lw x27, -68(x8)
addr0x000001c8: lw x15, 1414(x8)
addr0x000001cc: jal x19, addr0x0000017c
addr0x000001d0: lw x9, 1256(x8)
addr0x000001d4: addi x19, x19, -1314
addr0x000001d8: jal x23, addr0x00000358
addr0x000001dc: jal x5, addr0x00000980
addr0x000001e0: lbu x24, -638(x8)
addr0x000001e4: lw x7, 1552(x8)
addr0x000001e8: lw x22, -1936(x8)
addr0x000001ec: lw x21, -869(x8)
addr0x000001f0: lw x5, 1526(x8)
addr0x000001f4: lw x29, 864(x8)
addr0x000001f8: sw x4, 945(x8)
addr0x000001fc: sw x0, 1526(x8)
addr0x00000200: jal x29, addr0x000007b8
addr0x00000204: andi x16, x22, 702
addr0x00000208: addi x19, x22, 1459
addr0x0000020c: bgeu x18, x29, addr0x00000264
addr0x00000210: lw x2, -1924(x8)
addr0x00000214: sw x10, 584(x8)
addr0x00000218: sw x18, -817(x8)
addr0x0000021c: sw x7, -135(x8)
addr0x00000220: jal x2, addr0x000004e4
addr0x00000224: jal x18, addr0x00000424
addr0x00000228: addi x29, x5, -1190
addr0x0000022c: addi x18, x22, 73
addr0x00000230: jal x17, addr0x00000a94
addr0x00000234: bne x23, x15, addr0x000003d8
addr0x00000238: jal x23, addr0x00000474
addr0x0000023c: lw x30, -48(x8)
addr0x00000240: lw x29, 869(x8)
addr0x00000244: lw x4, 702(x8)
addr0x00000248: jal x22, addr0x00000184
addr0x0000024c: jal x29, addr0x00000844
addr0x00000250: slli x27, x20, 20
addr0x00000254: slli x20, x30, 25
addr0x00000258: lw x19, 88(x8)
addr0x0000025c: lw x19, -124(x8)
addr0x00000260: lw x22, -1200(x8)
addr0x00000264: addi x4, x17, 1030
addr0x00000268: bge x0, x9, addr0x00000008
addr0x0000026c: addi x29, x20, 359
addr0x00000270: addi x29, x30, -1245
addr0x00000274: sw x1, -352(x8)
addr0x00000278: addi x22, x23, -2030
addr0x0000027c: add x0, x23, x29
addr0x00000280: lbu x15, 714(x8)
addr0x00000284: sll x7, x30, x30
addr0x00000288: or x5, x29, x19
addr0x0000028c: slli x22, x9, 23
addr0x00000290: add x23, x22, x23
addr0x00000294: jal x7, addr0x00000478
addr0x00000298: beq x29, x2, addr0x000001d4
addr0x0000029c: lw x15, 2043(x8)
addr0x000002a0: jal x29, addr0x0000082c
addr0x000002a4: addi x22, x29, -1276
addr0x000002a8: lw x10, 101(x8)
addr0x000002ac: lw x25, 1156(x8)
addr0x000002b0: lw x7, -1208(x8)
addr0x000002b4: lw x19, -1661(x8)
addr0x000002b8: sw x17, 703(x8)
addr0x000002bc: sw x10, -272(x8)
addr0x000002c0: addi x19, x11, -1163
addr0x000002c4: jal x25, addr0x0000031c
addr0x000002c8: jal x30, addr0x00000350
addr0x000002cc: beq x5, x19, addr0x000007b8
addr0x000002d0: bltu x15, x9, addr0x00000908
addr0x000002d4: addi x18, x29, -1810
addr0x000002d8: sh x25, -10(x8)
addr0x000002dc: addi x15, x2, -1016
addr0x000002e0: lui x30, 451256
addr0x000002e4: addi x22, x6, -1282
addr0x000002e8: slli x23, x30, 29
addr0x000002ec: addi x25, x11, -155
addr0x000002f0: add x7, x25, x15
addr0x000002f4: lw x21, -329(x8)
addr0x000002f8: lw x21, -584(x8)
addr0x000002fc: bne x21, x7, addr0x000003f4
addr0x00000300: slli x4, x24, 29
addr0x00000304: add x21, x0, x5
addr0x00000308: bgeu x6, x23, addr0x00000810
addr0x0000030c: lw x20, -1837(x8)
addr0x00000310: lw x0, 645(x8)
addr0x00000314: lw x18, -1112(x8)
addr0x00000318: add x22, x24, x15
addr0x0000031c: lw x30, 223(x8)
addr0x00000320: jal x19, addr0x00000544
addr0x00000324: addi x0, x9, 139
addr0x00000328: lw x0, -701(x8)
addr0x0000032c: sw x7, 1459(x8)
addr0x00000330: addi x15, x22, 300
addr0x00000334: bne x16, x29, addr0x00000154
addr0x00000338: lbu x4, 208(x8)
addr0x0000033c: jal x0, addr0x000006b4
addr0x00000340: lw x19, 121(x8)
addr0x00000344: lw x9, 1655(x8)
addr0x00000348: jal x23, addr0x000006dc
addr0x0000034c: lw x9, 1742(x8)
addr0x00000350: sw x11, 309(x8)
addr0x00000354: bne x15, x19, addr0x000005a8
addr0x00000358: jal x23, addr0x00000b9c
addr0x0000035c: lw x9, 1191(x8)
addr0x00000360: lw x19, 1225(x8)
addr0x00000364: lw x4, 319(x8)
addr0x00000368: addi x23, x19, 2005
addr0x0000036c: sw x19, -482(x8)
addr0x00000370: sw x22, -1060(x8)
addr0x00000374: sw x5, 1098(x8)
addr0x00000378: lw x19, 1760(x8)
addr0x0000037c: jal x4, addr0x000000f0
addr0x00000380: lui x6, 311741
addr0x00000384: addi x4, x17, -1317
addr0x00000388: lw x5, 1862(x8)
addr0x0000038c: lw x21, -1500(x8)
addr0x00000390: sw x29, 1609(x8)
addr0x00000394: sw x1, -1381(x8)
addr0x00000398: sw x11, -1511(x8)
addr0x0000039c: sw x7, -1058(x8)
addr0x000003a0: add x19, x9, x29
addr0x000003a4: jal x29, addr0x000002c4
addr0x000003a8: lw x4, -1898(x8)
addr0x000003ac: lw x1, 49(x8)
addr0x000003b0: addi x29, x23, -1282
addr0x000003b4: slli x31, x4, 22
addr0x000003b8: add x23, x9, x19
addr0x000003bc: sw x21, 1709(x8)
addr0x000003c0: sw x4, 594(x8)
addr0x000003c4: jal x19, addr0x00000a78
addr0x000003c8: jal x24, addr0x000005b8
addr0x000003cc: addi x30, x9, -1150
addr0x000003d0: sw x2, 1810(x8)
addr0x000003d4: sw x29, -341(x8)
addr0x000003d8: sw x19, -996(x8)
addr0x000003dc: jal x0, addr0x0000061c
addr0x000003e0: lw x22, 1166(x8)
addr0x000003e4: lw x29, 1277(x8)
addr0x000003e8: lw x17, -1582(x8)
addr0x000003ec: slli x22, x22, 21
addr0x000003f0: srli x22, x23, 14
addr0x000003f4: lw x20, -1533(x8)
addr0x000003f8: lb x4, -1665(x8)
addr0x000003fc: sb x23, 1262(x8)
addr0x00000400: lw x18, -1453(x8)
addr0x00000404: lw x19, 846(x8)
addr0x00000408: lw x17, 861(x8)
addr0x0000040c: lw x29, -40(x8)
addr0x00000410: lw x29, 708(x8)
addr0x00000414: lw x22, 37(x8)
addr0x00000418: blt x18, x29, addr0x00000730
addr0x0000041c: addi x22, x23, 92
addr0x00000420: sw x18, 1072(x8)
addr0x00000424: addi x22, x18, -272
addr0x00000428: sw x9, -952(x8)
addr0x0000042c: sw x1, -1991(x8)
addr0x00000430: lw x9, 1960(x8)
addr0x00000434: lw x19, 843(x8)
addr0x00000438: lw x10, 804(x8)
addr0x0000043c: addi x30, x29, 1753
addr0x00000440: addi x29, x23, -1820
addr0x00000444: sw x23, -1294(x8)
addr0x00000448: sw x30, -1158(x8)
addr0x0000044c: addi x15, x21, -1793
addr0x00000450: jal x4, addr0x00000b44
addr0x00000454: jal x6, addr0x00000774
addr0x00000458: andi x2, x19, 705
addr0x0000045c: addi x16, x23, -1276
addr0x00000460: lw x24, -1598(x8)
addr0x00000464: lw x5, -1533(x8)
addr0x00000468: bgeu x29, x4, addr0x000001b0
addr0x0000046c: addi x29, x23, 460
addr0x00000470: sw x22, 1433(x8)
addr0x00000474: sw x30, 496(x8)
addr0x00000478: addi x25, x5, 1094
addr0x0000047c: blt x9, x2, addr0x000002dc
addr0x00000480: jal x22, addr0x0000052c
addr0x00000484: lw x19, 41(x8)
addr0x00000488: sub x22, x0, x24
addr0x0000048c: jal x21, addr0x00000a30
addr0x00000490: sw x10, 979(x8)
addr0x00000494: sw x27, 1810(x8)
addr0x00000498: addi x29, x22, 1760
addr0x0000049c: addi x15, x19, 342
addr0x000004a0: addi x4, x0, 1735
addr0x000004a4: sw x4, 2026(x8)
addr0x000004a8: jal x23, addr0x00000a54
addr0x000004ac: sw x22, -19(x8)
addr0x000004b0: addi x19, x9, -1065
addr0x000004b4: lw x4, 746(x8)
addr0x000004b8: lw x29, 12(x8)
addr0x000004bc: lw x7, -2028(x8)
addr0x000004c0: lw x23, -1305(x8)
addr0x000004c4: lw x23, 708(x8)
addr0x000004c8: lw x22, 1726(x8)
addr0x000004cc: lbu x22, -1931(x8)
addr0x000004d0: blt x22, x29, addr0x00000304
addr0x000004d4: lw x9, 1698(x8)
addr0x000004d8: addi x19, x5, -853
addr0x000004dc: sw x22, 79(x8)
addr0x000004e0: sw x0, 1806(x8)
addr0x000004e4: sw x29, -1956(x8)
addr0x000004e8: add x16, x21, x22
addr0x000004ec: jal x30, addr0x00000bd4
addr0x000004f0: lw x25, -1537(x8)
addr0x000004f4: auipc x31, 350082
addr0x000004f8: jalr x0, 548(x3)
addr0x000004fc: sw x20, -1056(x8)
addr0x00000500: sw x31, -1225(x8)
addr0x00000504: sw x22, -351(x8)
addr0x00000508: bne x23, x20, addr0x0000082c
addr0x0000050c: lh x22, -344(x8)
addr0x00000510: sw x22, -259(x8)
addr0x00000514: lw x31, -884(x8)
addr0x00000518: beq x21, x2, addr0x00000104
addr0x0000051c: lw x24, 27(x8)
addr0x00000520: lbu x7, -1645(x8)
addr0x00000524: addi x22, x0, 2035
addr0x00000528: sw x7, -757(x8)
addr0x0000052c: bne x30, x19, addr0x0000095c
addr0x00000530: jal x22, addr0x000005e0
addr0x00000534: lw x22, 1011(x8)
addr0x00000538: lw x23, 1753(x8)
addr0x0000053c: andi x18, x23, 1012
addr0x00000540: addi x7, x23, -1104
addr0x00000544: jal x29, addr0x00000530
addr0x00000548: addi x22, x21, 1225
addr0x0000054c: addi x4, x15, 617
addr0x00000550: jal x29, addr0x00000814
addr0x00000554: lw x9, 41(x8)
addr0x00000558: sw x29, 1574(x8)
addr0x0000055c: sw x23, -551(x8)
addr0x00000560: jal x19, addr0x000003a0
addr0x00000564: lw x23, 265(x8)
addr0x00000568: lw x5, 123(x8)
addr0x0000056c: lw x0, 1929(x8)
addr0x00000570: add x30, x30, x29
addr0x00000574: jal x23, addr0x00000130
addr0x00000578: lw x30, 699(x8)
addr0x0000057c: lw x19, -1188(x8)
addr0x00000580: lw x20, -946(x8)
addr0x00000584: jal x25, addr0x00000b88
addr0x00000588: lw x15, 1086(x8)
addr0x0000058c: sw x9, 191(x8)
addr0x00000590: add x9, x5, x4
addr0x00000594: addi x31, x2, -542
addr0x00000598: lui x25, 51217
addr0x0000059c: sw x11, -1474(x8)
addr0x000005a0: jal x21, addr0x00000910
addr0x000005a4: sub x22, x22, x2
addr0x000005a8: addi x27, x22, 2029
addr0x000005ac: lw x9, 1195(x8)
addr0x000005b0: lw x23, -1419(x8)
addr0x000005b4: lw x11, 731(x8)
addr0x000005b8: lw x1, 668(x8)
addr0x000005bc: bltu x7, x23, addr0x0000075c
addr0x000005c0: slli x25, x23, 7
addr0x000005c4: srli x4, x4, 21
addr0x000005c8: bne x24, x16, addr0x000000ac
addr0x000005cc: lui x29, 29905
addr0x000005d0: addi x29, x1, -919
addr0x000005d4: sb x9, -412(x8)
addr0x000005d8: bgeu x7, x15, addr0x0000066c
addr0x000005dc: lw x9, 918(x8)
addr0x000005e0: lw x23, 456(x8)
addr0x000005e4: lw x0, -518(x8)
addr0x000005e8: beq x16, x29, addr0x00000680
addr0x000005ec: beq x9, x16, addr0x00000448
addr0x000005f0: lw x9, -1382(x8)
addr0x000005f4: sub x20, x9, x18
addr0x000005f8: sub x10, x4, x2
addr0x000005fc: sub x17, x30, x21
addr0x00000600: andi x31, x22, -189
addr0x00000604: sw x1, 1131(x8)
addr0x00000608: addi x0, x7, -494
addr0x0000060c: addi x23, x4, -962
addr0x00000610: sw x7, -386(x8)
addr0x00000614: lw x17, 486(x8)
addr0x00000618: addi x1, x19, -1647
addr0x0000061c: add x27, x19, x17
addr0x00000620: sw x21, -1371(x8)
addr0x00000624: slli x29, x24, 16
addr0x00000628: sw x29, 1313(x8)
addr0x0000062c: jal x22, addr0x00000760
addr0x00000630: lw x29, 1636(x8)
addr0x00000634: addi x25, x30, 1635
addr0x00000638: sw x23, -1539(x8)
addr0x0000063c: lw x4, 870(x8)
addr0x00000640: add x22, x29, x9
addr0x00000644: lw x23, 588(x8)
addr0x00000648: lw x4, -1437(x8)
addr0x0000064c: sw x9, 1156(x8)
addr0x00000650: lw x19, 878(x8)
addr0x00000654: addi x15, x17, 396
addr0x00000658: andi x5, x29, -1542
addr0x0000065c: lw x20, 1568(x8)
addr0x00000660: lw x4, -419(x8)
addr0x00000664: addi x23, x18, 1725
addr0x00000668: add x1, x23, x0
addr0x0000066c: bgeu x5, x16, addr0x000007e4
addr0x00000670: addi x21, x18, 1713
addr0x00000674: addi x23, x23, 1419
addr0x00000678: addi x4, x23, -1729
addr0x0000067c: lw x4, 1539(x8)
addr0x00000680: addi x23, x19, -808
addr0x00000684: lw x18, -211(x8)
addr0x00000688: jal x4, addr0x000000ac
addr0x0000068c: addi x30, x24, -762
addr0x00000690: jal x29, addr0x00000028
addr0x00000694: lw x1, 1182(x8)
addr0x00000698: lw x25, 1941(x8)
addr0x0000069c: addi x0, x23, 66
addr0x000006a0: sw x4, -1109(x8)
addr0x000006a4: jal x19, addr0x000002cc
addr0x000006a8: lw x18, 666(x8)
addr0x000006ac: lw x2, 134(x8)
addr0x000006b0: sw x22, -1875(x8)
addr0x000006b4: sb x9, -579(x8)
addr0x000006b8: lw x19, 905(x8)
addr0x000006bc: lw x15, -1365(x8)
addr0x000006c0: addi x18, x4, -545
addr0x000006c4: lbu x23, 776(x8)
addr0x000006c8: jal x24, addr0x0000080c
addr0x000006cc: lw x29, 1327(x8)
addr0x000006d0: lw x18, -418(x8)
addr0x000006d4: jal x22, addr0x00000c04
addr0x000006d8: sw x20, -2018(x8)
addr0x000006dc: jal x29, addr0x0000087c
addr0x000006e0: addi x29, x21, -1260
addr0x000006e4: jal x17, addr0x00000448
addr0x000006e8: sw x17, -1824(x8)
addr0x000006ec: lw x30, -1636(x8)
addr0x000006f0: add x23, x15, x29
addr0x000006f4: lw x19, -1658(x8)
addr0x000006f8: lw x0, -1902(x8)
addr0x000006fc: lw x21, -559(x8)
addr0x00000700: lw x9, -940(x8)
addr0x00000704: lbu x24, 663(x8)
addr0x00000708: addi x17, x4, -1171
addr0x0000070c: addi x4, x25, 1836
addr0x00000710: lw x23, 1327(x8)
addr0x00000714: lhu x29, -125(x8)
addr0x00000718: lhu x0, 233(x8)
addr0x0000071c: addi x30, x23, 1690
addr0x00000720: jal x29, addr0x00000248
addr0x00000724: ori x30, x23, -253
addr0x00000728: sw x23, -37(x8)
addr0x0000072c: addi x29, x22, -1472
addr0x00000730: bne x9, x10, addr0x00000b04
addr0x00000734: addi x29, x19, -1317
addr0x00000738: jal x24, addr0x00000b48
addr0x0000073c: beq x24, x17, addr0x00000a28
addr0x00000740: lw x9, 1376(x8)
addr0x00000744: lh x1, 1393(x8)
addr0x00000748: bne x29, x30, addr0x000004ec
addr0x0000074c: or x22, x15, x20
addr0x00000750: addi x0, x19, -773
addr0x00000754: lw x23, 382(x8)
addr0x00000758: sw x23, -769(x8)
addr0x0000075c: sw x15, 1326(x8)
addr0x00000760: jal x9, addr0x000003e8
addr0x00000764: bge x9, x9, addr0x000007fc
addr0x00000768: slli x4, x4, 8
addr0x0000076c: srli x21, x0, 23
addr0x00000770: bltu x7, x0, addr0x00000244
addr0x00000774: slli x27, x20, 30
addr0x00000778: srli x30, x23, 10
addr0x0000077c: bgeu x20, x22, addr0x00000448
addr0x00000780: lh x9, 385(x8)
addr0x00000784: slli x20, x19, 10
addr0x00000788: sw x19, -1733(x8)
addr0x0000078c: sw x18, -584(x8)
addr0x00000790: lw x25, 259(x8)
addr0x00000794: jal x19, addr0x000009d8
addr0x00000798: lui x9, 676339
addr0x0000079c: sw x22, 1761(x8)
addr0x000007a0: lw x31, -1386(x8)
addr0x000007a4: addi x31, x29, 992
addr0x000007a8: lui x20, 534968
addr0x000007ac: lui x29, 491537
addr0x000007b0: addi x22, x22, -1831
addr0x000007b4: bne x18, x4, addr0x000004c4
addr0x000007b8: lbu x15, -1634(x8)
addr0x000007bc: lw x6, -1580(x8)
addr0x000007c0: lw x21, -1152(x8)
addr0x000007c4: lw x18, 1387(x8)
addr0x000007c8: sw x0, 1102(x8)
addr0x000007cc: sw x22, -1440(x8)
addr0x000007d0: sw x5, 4(x8)
addr0x000007d4: addi x22, x29, 1369
addr0x000007d8: jal x19, addr0x0000064c
addr0x000007dc: addi x24, x18, 1365
addr0x000007e0: lw x19, -84(x8)
addr0x000007e4: lw x1, 91(x8)
addr0x000007e8: ori x19, x23, 1551
addr0x000007ec: sw x29, -545(x8)
addr0x000007f0: lw x10, -808(x8)
addr0x000007f4: lw x4, 1193(x8)
addr0x000007f8: add x0, x20, x23
addr0x000007fc: lw x15, 468(x8)
addr0x00000800: lui x17, 594571
addr0x00000804: sw x9, -1053(x8)
addr0x00000808: sw x30, 1060(x8)
addr0x0000080c: bne x22, x19, addr0x000004f4
addr0x00000810: lw x12, 1087(x8)
addr0x00000814: lw x19, -146(x8)
addr0x00000818: lui x23, 953125
addr0x0000081c: lw x17, 720(x8)
addr0x00000820: sw x21, 909(x8)
addr0x00000824: addi x19, x15, -464
addr0x00000828: jal x19, addr0x00000530
addr0x0000082c: lw x9, 958(x8)
addr0x00000830: sw x4, 1726(x8)
addr0x00000834: jal x21, addr0x00000060
addr0x00000838: lw x24, 790(x8)
addr0x0000083c: sw x24, 134(x8)
addr0x00000840: lui x6, 846690
addr0x00000844: addi x5, x21, -862
addr0x00000848: addi x21, x22, 623
addr0x0000084c: sw x17, -13(x8)
addr0x00000850: addi x30, x29, -509
addr0x00000854: sw x4, 559(x8)
addr0x00000858: sw x9, 643(x8)
addr0x0000085c: addi x20, x20, 1236
addr0x00000860: lw x5, -1266(x8)
addr0x00000864: lw x12, 662(x8)
addr0x00000868: addi x22, x0, 624
addr0x0000086c: jal x15, addr0x000001d8
addr0x00000870: slli x18, x20, 11
addr0x00000874: or x22, x10, x19
addr0x00000878: lbu x9, -596(x8)
addr0x0000087c: lw x9, -513(x8)
addr0x00000880: jal x0, addr0x00000440
addr0x00000884: lw x23, -312(x8)
addr0x00000888: lw x21, 1215(x8)
addr0x0000088c: add x23, x17, x9
addr0x00000890: sw x20, -209(x8)
addr0x00000894: lw x24, 1204(x8)
addr0x00000898: add x9, x22, x22
addr0x0000089c: jal x19, addr0x00000904
addr0x000008a0: sw x23, -851(x8)
addr0x000008a4: sw x20, 299(x8)
addr0x000008a8: addi x4, x29, -1532
addr0x000008ac: sw x22, -1093(x8)
addr0x000008b0: sw x16, 176(x8)
addr0x000008b4: jal x27, addr0x00000368
addr0x000008b8: sb x6, 1440(x8)
addr0x000008bc: lw x19, 1000(x8)
addr0x000008c0: addi x0, x7, -1052
addr0x000008c4: sw x5, -1874(x8)
addr0x000008c8: jal x23, addr0x00000a1c
addr0x000008cc: andi x23, x4, 261
addr0x000008d0: blt x15, x15, addr0x000002a8
addr0x000008d4: sb x23, -1423(x8)
addr0x000008d8: lbu x24, 415(x8)
addr0x000008dc: addi x23, x2, 150
addr0x000008e0: sw x19, -256(x8)
addr0x000008e4: lbu x0, -1283(x8)
addr0x000008e8: sw x22, -559(x8)
addr0x000008ec: srli x4, x22, 14
addr0x000008f0: add x4, x27, x19
addr0x000008f4: sw x27, -293(x8)
addr0x000008f8: sw x4, -606(x8)
addr0x000008fc: addi x18, x19, 795
addr0x00000900: sw x24, -1998(x8)
addr0x00000904: sw x30, -1121(x8)
addr0x00000908: jal x20, addr0x00000430
addr0x0000090c: lw x15, -597(x8)
addr0x00000910: addi x30, x29, 1474
addr0x00000914: jal x19, addr0x00000610
addr0x00000918: addi x7, x22, -1277
addr0x0000091c: lw x16, 1534(x8)
addr0x00000920: addi x19, x23, -1367
addr0x00000924: addi x7, x23, 845
addr0x00000928: addi x1, x4, -399
addr0x0000092c: sw x29, -579(x8)
addr0x00000930: addi x30, x22, 1208
addr0x00000934: add x30, x22, x9
addr0x00000938: jal x7, addr0x00000898
addr0x0000093c: addi x20, x29, -262
addr0x00000940: sw x0, 359(x8)
addr0x00000944: lw x18, 1302(x8)
addr0x00000948: lw x20, -995(x8)
addr0x0000094c: lw x5, -297(x8)
addr0x00000950: sw x1, -950(x8)
addr0x00000954: sw x23, -1160(x8)
addr0x00000958: lw x4, -1988(x8)
addr0x0000095c: lw x20, -223(x8)
addr0x00000960: jal x4, addr0x0000081c
addr0x00000964: sw x22, 13(x8)
addr0x00000968: andi x19, x19, -1835
addr0x0000096c: beq x4, x20, addr0x00000bbc
addr0x00000970: lw x15, 1923(x8)
addr0x00000974: lw x20, 1847(x8)
addr0x00000978: sw x5, -934(x8)
addr0x0000097c: lw x30, -1131(x8)
addr0x00000980: lbu x17, 312(x8)
addr0x00000984: lui x29, 335028
addr0x00000988: lui x30, 653560
addr0x0000098c: addi x24, x22, 436
addr0x00000990: sb x22, 588(x8)
addr0x00000994: lw x4, 303(x8)
addr0x00000998: lw x9, 1202(x8)
addr0x0000099c: sw x11, -1605(x8)
addr0x000009a0: sw x29, -839(x8)
addr0x000009a4: sw x19, 1981(x8)
addr0x000009a8: addi x0, x5, 1456
addr0x000009ac: jal x27, addr0x00000018
addr0x000009b0: jal x4, addr0x000004f4
addr0x000009b4: beq x2, x18, addr0x000001d8
addr0x000009b8: bltu x29, x23, addr0x0000021c
addr0x000009bc: lui x29, 367374
addr0x000009c0: lui x19, 412760
addr0x000009c4: addi x7, x19, 1478
addr0x000009c8: jal x7, addr0x000000d4
addr0x000009cc: lw x19, 1281(x8)
addr0x000009d0: sw x17, -1054(x8)
addr0x000009d4: jal x23, addr0x00000770
addr0x000009d8: lui x22, 570617
addr0x000009dc: addi x7, x11, -154
addr0x000009e0: sw x27, -212(x8)
addr0x000009e4: sw x23, 1166(x8)
addr0x000009e8: sw x23, -1189(x8)
addr0x000009ec: sw x29, 1401(x8)
addr0x000009f0: sw x5, -749(x8)
addr0x000009f4: jal x11, addr0x0000010c
addr0x000009f8: lw x7, -1292(x8)
addr0x000009fc: lw x19, -1909(x8)
addr0x00000a00: addi x4, x19, 1644
addr0x00000a04: jal x5, addr0x00000850
addr0x00000a08: sw x2, -323(x8)
addr0x00000a0c: addi x23, x5, 1665
addr0x00000a10: sw x4, -824(x8)
addr0x00000a14: sw x19, -1526(x8)
addr0x00000a18: jal x21, addr0x00000044
addr0x00000a1c: beq x0, x23, addr0x000006ac
addr0x00000a20: lw x20, 1804(x8)
addr0x00000a24: jal x19, addr0x00000bd8
addr0x00000a28: lw x21, 1461(x8)
addr0x00000a2c: lw x0, 943(x8)
addr0x00000a30: lw x10, -1228(x8)
addr0x00000a34: sw x22, -1262(x8)
addr0x00000a38: sw x6, -1156(x8)
addr0x00000a3c: sw x1, 716(x8)
addr0x00000a40: lw x20, 1045(x8)
addr0x00000a44: addi x0, x30, -1599
addr0x00000a48: jal x23, addr0x000006e8
addr0x00000a4c: sw x9, -1156(x8)
addr0x00000a50: sw x5, -982(x8)
addr0x00000a54: sw x29, 70(x8)
addr0x00000a58: addi x30, x6, -1828
addr0x00000a5c: jal x9, addr0x00000ba8
addr0x00000a60: lbu x4, -981(x8)
addr0x00000a64: andi x16, x9, -1632
addr0x00000a68: addi x22, x2, 454
addr0x00000a6c: jal x25, addr0x000004e0
addr0x00000a70: jal x31, addr0x00000b7c
addr0x00000a74: lw x30, 89(x8)
addr0x00000a78: jal x18, addr0x00000208
addr0x00000a7c: lw x7, 1080(x8)
addr0x00000a80: lw x21, 1782(x8)
addr0x00000a84: sw x16, -1857(x8)
addr0x00000a88: sw x27, -1533(x8)
addr0x00000a8c: sw x2, -1574(x8)
addr0x00000a90: lw x29, 1562(x8)
addr0x00000a94: lw x9, 568(x8)
addr0x00000a98: lw x21, 985(x8)
addr0x00000a9c: lbu x20, 1484(x8)
addr0x00000aa0: andi x0, x0, 657
addr0x00000aa4: lw x2, 743(x8)
addr0x00000aa8: sw x2, 1309(x8)
addr0x00000aac: lw x18, 370(x8)
addr0x00000ab0: lw x22, 1621(x8)
addr0x00000ab4: lw x1, 700(x8)
addr0x00000ab8: addi x23, x4, -1967
addr0x00000abc: lbu x0, 1567(x8)
addr0x00000ac0: add x29, x5, x15
addr0x00000ac4: jal x22, addr0x00000250
addr0x00000ac8: lui x17, 720321
addr0x00000acc: addi x29, x4, -560
addr0x00000ad0: sw x29, 1403(x8)
addr0x00000ad4: lw x1, 216(x8)
addr0x00000ad8: addi x29, x23, 1943
addr0x00000adc: sub x29, x24, x23
addr0x00000ae0: sw x4, 1851(x8)
addr0x00000ae4: lw x2, -255(x8)
addr0x00000ae8: addi x9, x22, 64
addr0x00000aec: sw x17, -780(x8)
addr0x00000af0: bltu x16, x29, addr0x00000ac0
addr0x00000af4: addi x17, x10, -1293
addr0x00000af8: sw x21, 285(x8)
addr0x00000afc: addi x31, x21, 634
addr0x00000b00: sw x30, -950(x8)
addr0x00000b04: sw x2, 1525(x8)
addr0x00000b08: sw x17, -727(x8)
addr0x00000b0c: jal x18, addr0x00000a10
addr0x00000b10: lw x23, 852(x8)
addr0x00000b14: lw x29, 141(x8)
addr0x00000b18: sw x23, -1913(x8)
addr0x00000b1c: beq x18, x7, addr0x00000bf8
addr0x00000b20: addi x23, x17, 1414
addr0x00000b24: sh x29, -1356(x8)
addr0x00000b28: jal x29, addr0x00000a28
addr0x00000b2c: addi x23, x29, 1780
addr0x00000b30: jal x9, addr0x000005c4
addr0x00000b34: lw x29, 1003(x8)
addr0x00000b38: lbu x16, 1315(x8)
addr0x00000b3c: sb x4, -126(x8)
addr0x00000b40: addi x20, x23, 1223
addr0x00000b44: bne x4, x9, addr0x00000878
addr0x00000b48: lw x4, 1001(x8)
addr0x00000b4c: addi x23, x5, 1224
addr0x00000b50: bltu x22, x18, addr0x000006d8
addr0x00000b54: lui x10, 571528
addr0x00000b58: addi x23, x18, -1074
addr0x00000b5c: addi x18, x29, 1873
addr0x00000b60: lbu x23, -1054(x8)
addr0x00000b64: sb x1, 548(x8)
addr0x00000b68: lbu x20, 1420(x8)
addr0x00000b6c: bne x1, x23, addr0x000008d0
addr0x00000b70: lw x7, -1795(x8)
addr0x00000b74: addi x23, x0, 1362
addr0x00000b78: srli x4, x20, 8
addr0x00000b7c: bltu x15, x22, addr0x000005c0
addr0x00000b80: add x4, x7, x4
addr0x00000b84: jal x9, addr0x000006b8
addr0x00000b88: addi x0, x24, 1465
addr0x00000b8c: sw x25, 1091(x8)
addr0x00000b90: sw x29, -1622(x8)
addr0x00000b94: jal x9, addr0x00000234
addr0x00000b98: or x2, x22, x24
addr0x00000b9c: andi x29, x4, 1025
addr0x00000ba0: addi x23, x18, -645
addr0x00000ba4: addi x24, x4, -1728
addr0x00000ba8: jal x22, addr0x000008e8
addr0x00000bac: sw x18, -2011(x8)
addr0x00000bb0: sw x20, 1455(x8)
addr0x00000bb4: sw x20, -1364(x8)
addr0x00000bb8: sw x21, -929(x8)
addr0x00000bbc: sw x9, -448(x8)
addr0x00000bc0: addi x9, x29, 740
addr0x00000bc4: auipc x4, 855825
addr0x00000bc8: addi x9, x5, 1669
addr0x00000bcc: sh x23, 1987(x8)
addr0x00000bd0: slli x16, x9, 12
addr0x00000bd4: srli x23, x7, 31
addr0x00000bd8: bne x4, x29, addr0x000006f0
addr0x00000bdc: sw x29, 1528(x8)
addr0x00000be0: sw x24, 610(x8)
addr0x00000be4: sw x2, 1567(x8)
addr0x00000be8: sw x29, 1041(x8)
addr0x00000bec: jal x29, addr0x0000058c
addr0x00000bf0: bne x31, x12, addr0x000007dc
addr0x00000bf4: jal x4, addr0x000000d8
addr0x00000bf8: addi x19, x10, 77
addr0x00000bfc: sw x4, -1061(x8)
addr0x00000c00: lw x21, -714(x8)
addr0x00000c04: addi x15, x24, -1469
addr0x00000c08: sw x0, -1726(x8)
addr0x00000c0c: sw x7, -255(x8)
addr0x00000c10: lw x17, -1954(x8)
