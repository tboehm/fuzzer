.global _start
_start:
addr0x00000000: xor x8, x8, x8
addr0x00000004: lui x8, 1024
addr0x00000008: addi x8, x8, 0
addr0x0000000c: xor x3, x3, x3
addr0x00000010: lui x3, 0
addr0x00000014: addi x3, x3, 0
addr0x00000018: lw x30, 715(x8)
addr0x0000001c: ori x22, x1, 280
addr0x00000020: addi x25, x13, 1597
addr0x00000024: lw x0, -721(x8)
addr0x00000028: lui x14, 502561
addr0x0000002c: lui x22, 682540
addr0x00000030: addi x20, x23, -760
addr0x00000034: sub x25, x25, x15
addr0x00000038: beq x10, x17, addr0x0000054c
addr0x0000003c: addi x19, x26, 1098
addr0x00000040: jal x5, addr0x00000310
addr0x00000044: addi x30, x20, -1504
addr0x00000048: add x6, x14, x2
addr0x0000004c: lbu x23, -176(x8)
addr0x00000050: auipc x11, 1008936
addr0x00000054: jalr x0, 312(x3)
addr0x00000058: jal x22, addr0x00000954
addr0x0000005c: addi x17, x14, -1632
addr0x00000060: addi x20, x14, 175
addr0x00000064: sw x27, -163(x8)
addr0x00000068: bne x17, x25, addr0x00000734
addr0x0000006c: sh x13, 1621(x8)
addr0x00000070: bne x22, x17, addr0x0000031c
addr0x00000074: addi x2, x19, -719
addr0x00000078: lw x29, 1453(x8)
addr0x0000007c: lw x14, 1245(x8)
addr0x00000080: jal x18, addr0x00000764
addr0x00000084: bne x27, x31, addr0x0000025c
addr0x00000088: addi x14, x19, 1622
addr0x0000008c: lw x11, 747(x8)
addr0x00000090: lw x29, 788(x8)
addr0x00000094: bgeu x28, x7, addr0x00000464
addr0x00000098: slli x18, x14, 24
addr0x0000009c: lh x0, 31(x8)
addr0x000000a0: lui x22, 3915
addr0x000000a4: addi x29, x13, -247
addr0x000000a8: sw x7, -1369(x8)
addr0x000000ac: jal x19, addr0x00000544
addr0x000000b0: sw x12, -75(x8)
addr0x000000b4: sw x25, 1868(x8)
addr0x000000b8: lui x17, 1023421
addr0x000000bc: addi x19, x2, -1933
addr0x000000c0: add x17, x26, x7
addr0x000000c4: lw x1, 243(x8)
addr0x000000c8: lw x17, 1084(x8)
addr0x000000cc: lw x14, 118(x8)
addr0x000000d0: addi x20, x7, 1063
addr0x000000d4: addi x14, x25, -1781
addr0x000000d8: sw x21, 1525(x8)
addr0x000000dc: jal x24, addr0x00000878
addr0x000000e0: lw x18, 349(x8)
addr0x000000e4: sw x15, -1941(x8)
addr0x000000e8: sw x5, -1099(x8)
addr0x000000ec: bge x5, x10, addr0x000001e8
addr0x000000f0: lbu x27, 1009(x8)
addr0x000000f4: lbu x22, 630(x8)
addr0x000000f8: bge x21, x20, addr0x00000644
addr0x000000fc: beq x9, x26, addr0x00000258
addr0x00000100: lw x18, 1142(x8)
addr0x00000104: lw x22, 689(x8)
addr0x00000108: sw x14, -1940(x8)
addr0x0000010c: sw x25, 913(x8)
addr0x00000110: jal x19, addr0x00000300
addr0x00000114: lw x12, 534(x8)
addr0x00000118: lw x7, -334(x8)
addr0x0000011c: lw x17, -446(x8)
addr0x00000120: lw x9, 311(x8)
addr0x00000124: addi x23, x2, 763
addr0x00000128: ori x4, x14, 1036
addr0x0000012c: jal x19, addr0x000002f8
addr0x00000130: jal x7, addr0x00000438
addr0x00000134: lw x22, -1152(x8)
addr0x00000138: addi x20, x19, 1362
addr0x0000013c: ori x5, x19, -1212
addr0x00000140: lui x5, 175580
addr0x00000144: addi x10, x22, 862
addr0x00000148: jal x28, addr0x00000068
addr0x0000014c: addi x22, x30, -1026
addr0x00000150: srli x31, x18, 27
addr0x00000154: lbu x4, 1610(x8)
addr0x00000158: sw x14, -363(x8)
addr0x0000015c: lw x9, 1663(x8)
addr0x00000160: lw x22, 1076(x8)
addr0x00000164: lw x31, 146(x8)
addr0x00000168: jal x2, addr0x000007d0
addr0x0000016c: lw x28, 271(x8)
addr0x00000170: bltu x22, x25, addr0x000008fc
addr0x00000174: lui x19, 540035
addr0x00000178: addi x9, x20, 1664
addr0x0000017c: add x20, x22, x17
addr0x00000180: lw x15, 1930(x8)
addr0x00000184: addi x27, x9, -1825
addr0x00000188: lw x18, -1761(x8)
addr0x0000018c: lw x20, 696(x8)
addr0x00000190: lw x26, 13(x8)
addr0x00000194: addi x19, x22, 1638
addr0x00000198: addi x11, x31, -946
addr0x0000019c: andi x23, x15, -747
addr0x000001a0: add x14, x15, x4
addr0x000001a4: add x23, x29, x29
addr0x000001a8: jal x24, addr0x00000340
addr0x000001ac: sw x4, -1054(x8)
addr0x000001b0: lw x19, 1295(x8)
addr0x000001b4: lbu x19, 525(x8)
addr0x000001b8: jal x26, addr0x00000384
addr0x000001bc: addi x7, x17, 916
addr0x000001c0: bltu x30, x18, addr0x00000688
addr0x000001c4: addi x10, x19, -1429
addr0x000001c8: sw x19, -996(x8)
addr0x000001cc: lbu x17, -573(x8)
addr0x000001d0: lw x2, -1433(x8)
addr0x000001d4: bge x29, x22, addr0x00000350
addr0x000001d8: andi x27, x28, -1697
addr0x000001dc: addi x19, x20, -1179
addr0x000001e0: addi x22, x9, 1878
addr0x000001e4: lui x7, 1021599
addr0x000001e8: addi x10, x10, -2016
addr0x000001ec: addi x25, x20, -311
addr0x000001f0: addi x19, x21, -870
addr0x000001f4: lw x7, 1793(x8)
addr0x000001f8: sw x22, -1402(x8)
addr0x000001fc: lw x22, 578(x8)
addr0x00000200: lw x7, -336(x8)
addr0x00000204: bne x5, x22, addr0x00000020
addr0x00000208: sw x5, -1381(x8)
addr0x0000020c: sw x18, 2017(x8)
addr0x00000210: slli x2, x17, 31
addr0x00000214: srli x24, x1, 17
addr0x00000218: add x6, x15, x7
addr0x0000021c: addi x0, x29, -950
addr0x00000220: bne x14, x1, addr0x0000063c
addr0x00000224: jal x18, addr0x00000688
addr0x00000228: bne x19, x0, addr0x000002e0
addr0x0000022c: jal x9, addr0x00000764
addr0x00000230: beq x20, x14, addr0x000002bc
addr0x00000234: add x5, x19, x11
addr0x00000238: lbu x29, -154(x8)
addr0x0000023c: bne x13, x6, addr0x000006d4
addr0x00000240: lbu x17, 1490(x8)
addr0x00000244: lbu x29, -322(x8)
addr0x00000248: jal x19, addr0x00000320
addr0x0000024c: jal x12, addr0x000005c4
addr0x00000250: lw x21, -71(x8)
addr0x00000254: jal x9, addr0x0000034c
addr0x00000258: jal x22, addr0x00000370
addr0x0000025c: lw x0, -1851(x8)
addr0x00000260: sb x30, 888(x8)
addr0x00000264: sb x7, 380(x8)
addr0x00000268: lw x6, 1822(x8)
addr0x0000026c: lw x13, -1490(x8)
addr0x00000270: lw x22, 1200(x8)
addr0x00000274: addi x12, x0, -1498
addr0x00000278: sw x24, 1997(x8)
addr0x0000027c: sw x5, -1434(x8)
addr0x00000280: sw x19, -584(x8)
addr0x00000284: sw x10, 906(x8)
addr0x00000288: sub x7, x23, x26
addr0x0000028c: srai x17, x4, 18
addr0x00000290: lui x17, 493704
addr0x00000294: sw x17, 680(x8)
addr0x00000298: addi x25, x20, -1829
addr0x0000029c: jal x18, addr0x00000998
addr0x000002a0: beq x18, x29, addr0x000007e8
addr0x000002a4: lw x30, 936(x8)
addr0x000002a8: lui x22, 8993
addr0x000002ac: addi x23, x2, 1457
addr0x000002b0: add x10, x9, x29
addr0x000002b4: sw x27, 1718(x8)
addr0x000002b8: lui x30, 990461
addr0x000002bc: sw x9, 1155(x8)
addr0x000002c0: lw x15, 1586(x8)
addr0x000002c4: lw x11, -598(x8)
addr0x000002c8: add x23, x21, x31
addr0x000002cc: sw x18, -362(x8)
addr0x000002d0: jal x21, addr0x0000030c
addr0x000002d4: add x19, x29, x29
addr0x000002d8: jal x10, addr0x000007d4
addr0x000002dc: lui x5, 193469
addr0x000002e0: addi x18, x4, 1179
addr0x000002e4: jal x22, addr0x00000838
addr0x000002e8: lw x22, -241(x8)
addr0x000002ec: andi x25, x22, -1542
addr0x000002f0: beq x31, x4, addr0x000003f8
addr0x000002f4: addi x28, x0, 1336
addr0x000002f8: jal x23, addr0x00000738
addr0x000002fc: addi x17, x6, 759
addr0x00000300: addi x27, x0, 344
addr0x00000304: sw x20, -1758(x8)
addr0x00000308: lw x13, 7(x8)
addr0x0000030c: addi x28, x30, 629
addr0x00000310: sw x19, 1954(x8)
addr0x00000314: jal x28, addr0x000008d8
addr0x00000318: addi x17, x9, 186
addr0x0000031c: sw x14, -912(x8)
addr0x00000320: lw x28, -625(x8)
addr0x00000324: lw x30, -590(x8)
addr0x00000328: add x24, x29, x29
addr0x0000032c: bne x30, x23, addr0x00000210
addr0x00000330: jal x27, addr0x00000a64
addr0x00000334: lw x5, 1571(x8)
addr0x00000338: addi x22, x9, 246
addr0x0000033c: lw x14, -969(x8)
addr0x00000340: sw x22, -1408(x8)
addr0x00000344: addi x10, x29, 893
addr0x00000348: lbu x9, -1759(x8)
addr0x0000034c: sb x27, -9(x8)
addr0x00000350: bne x22, x18, addr0x00000574
addr0x00000354: lw x31, -1648(x8)
addr0x00000358: lw x30, 753(x8)
addr0x0000035c: lbu x31, 1243(x8)
addr0x00000360: sw x11, -1953(x8)
addr0x00000364: jal x18, addr0x000005c4
addr0x00000368: lui x18, 485841
addr0x0000036c: addi x17, x9, -1001
addr0x00000370: jal x5, addr0x00000610
addr0x00000374: lui x23, 320717
addr0x00000378: addi x7, x12, 1053
addr0x0000037c: lw x23, -89(x8)
addr0x00000380: lw x4, -1351(x8)
addr0x00000384: lw x16, 685(x8)
addr0x00000388: lw x22, 1680(x8)
addr0x0000038c: bne x7, x1, addr0x0000059c
addr0x00000390: addi x17, x4, 1783
addr0x00000394: addi x0, x25, 231
addr0x00000398: sw x17, 174(x8)
addr0x0000039c: sw x17, -1152(x8)
addr0x000003a0: sw x23, -503(x8)
addr0x000003a4: sw x13, -393(x8)
addr0x000003a8: jal x30, addr0x000009f4
addr0x000003ac: sw x23, -1719(x8)
addr0x000003b0: jal x21, addr0x000002dc
addr0x000003b4: andi x22, x29, 319
addr0x000003b8: andi x22, x31, 977
addr0x000003bc: sw x6, -959(x8)
addr0x000003c0: jal x27, addr0x000003b8
addr0x000003c4: lw x30, 1875(x8)
addr0x000003c8: lw x30, 899(x8)
addr0x000003cc: slli x7, x30, 30
addr0x000003d0: jal x7, addr0x00000180
addr0x000003d4: sw x20, -90(x8)
addr0x000003d8: jal x14, addr0x000001e8
addr0x000003dc: addi x31, x23, 839
addr0x000003e0: jal x0, addr0x000006f8
addr0x000003e4: addi x27, x17, -423
addr0x000003e8: add x9, x24, x25
addr0x000003ec: addi x7, x20, 1772
addr0x000003f0: addi x15, x30, 1861
addr0x000003f4: jal x11, addr0x00000638
addr0x000003f8: sw x4, 1284(x8)
addr0x000003fc: addi x27, x7, -282
addr0x00000400: jal x22, addr0x00000a14
addr0x00000404: sb x24, 1809(x8)
addr0x00000408: sb x25, -1121(x8)
addr0x0000040c: sw x28, -1158(x8)
addr0x00000410: sw x13, 861(x8)
addr0x00000414: jal x22, addr0x00000618
addr0x00000418: sw x5, 1357(x8)
addr0x0000041c: slli x20, x17, 14
addr0x00000420: lw x20, 841(x8)
addr0x00000424: jal x12, addr0x0000077c
addr0x00000428: lw x2, 879(x8)
addr0x0000042c: addi x29, x5, 1284
addr0x00000430: jal x17, addr0x00000354
addr0x00000434: lw x29, 1793(x8)
addr0x00000438: lw x17, -368(x8)
addr0x0000043c: addi x7, x2, 1669
addr0x00000440: sw x30, -794(x8)
addr0x00000444: sw x29, -376(x8)
addr0x00000448: sh x23, -1572(x8)
addr0x0000044c: sh x31, -317(x8)
addr0x00000450: lw x20, -628(x8)
addr0x00000454: jal x24, addr0x000008fc
addr0x00000458: lw x20, -1823(x8)
addr0x0000045c: addi x27, x0, -614
addr0x00000460: sw x30, 210(x8)
addr0x00000464: sw x29, -130(x8)
addr0x00000468: lw x0, -1033(x8)
addr0x0000046c: lw x27, 1653(x8)
addr0x00000470: jal x5, addr0x0000067c
addr0x00000474: sb x13, -1189(x8)
addr0x00000478: lw x0, -1904(x8)
addr0x0000047c: sltu x10, x23, x7
addr0x00000480: add x20, x27, x18
addr0x00000484: addi x0, x31, -361
addr0x00000488: addi x20, x25, -945
addr0x0000048c: lw x19, 821(x8)
addr0x00000490: lw x20, -47(x8)
addr0x00000494: sw x22, -1163(x8)
addr0x00000498: addi x7, x23, 784
addr0x0000049c: addi x27, x10, -1972
addr0x000004a0: lw x24, 536(x8)
addr0x000004a4: sub x26, x17, x30
addr0x000004a8: addi x5, x20, 870
addr0x000004ac: sw x29, 1402(x8)
addr0x000004b0: lw x4, 1555(x8)
addr0x000004b4: addi x2, x23, 350
addr0x000004b8: add x13, x20, x18
addr0x000004bc: bne x26, x23, addr0x00000764
addr0x000004c0: addi x23, x18, -1514
addr0x000004c4: jal x27, addr0x000000a4
addr0x000004c8: bge x22, x30, addr0x00000298
addr0x000004cc: addi x0, x22, -1146
addr0x000004d0: sw x17, 1800(x8)
addr0x000004d4: addi x30, x21, 907
addr0x000004d8: addi x17, x4, 97
addr0x000004dc: sw x13, -1337(x8)
addr0x000004e0: lw x28, -813(x8)
addr0x000004e4: sw x31, 405(x8)
addr0x000004e8: sw x24, -1261(x8)
addr0x000004ec: sw x21, -1666(x8)
addr0x000004f0: add x9, x31, x2
addr0x000004f4: divu x18, x17, x23
addr0x000004f8: bgeu x19, x0, addr0x00000468
addr0x000004fc: blt x23, x9, addr0x000000c4
addr0x00000500: beq x24, x18, addr0x00000860
addr0x00000504: lw x30, -672(x8)
addr0x00000508: beq x18, x31, addr0x00000364
addr0x0000050c: bltu x30, x19, addr0x00000614
addr0x00000510: sub x30, x0, x6
addr0x00000514: addi x7, x9, 1917
addr0x00000518: sw x0, -1496(x8)
addr0x0000051c: lw x0, 1437(x8)
addr0x00000520: addi x17, x22, 1099
addr0x00000524: sh x13, -748(x8)
addr0x00000528: slli x21, x24, 23
addr0x0000052c: auipc x29, 148233
addr0x00000530: addi x18, x22, -1521
addr0x00000534: bltu x22, x16, addr0x00000858
addr0x00000538: slli x4, x26, 3
addr0x0000053c: srli x6, x10, 4
addr0x00000540: bne x13, x0, addr0x000008c0
addr0x00000544: lw x5, 1497(x8)
addr0x00000548: jal x27, addr0x000000d4
addr0x0000054c: sb x0, -712(x8)
addr0x00000550: lw x12, -9(x8)
addr0x00000554: jalr x0, 436(x3)
addr0x00000558: jal x7, addr0x000009d4
addr0x0000055c: lw x23, -151(x8)
addr0x00000560: lbu x4, 546(x8)
addr0x00000564: jal x23, addr0x000006c8
addr0x00000568: lw x22, -1471(x8)
addr0x0000056c: sw x6, -26(x8)
addr0x00000570: sw x20, 23(x8)
addr0x00000574: sub x29, x19, x14
addr0x00000578: add x2, x20, x20
addr0x0000057c: sw x0, 1060(x8)
addr0x00000580: add x28, x7, x22
addr0x00000584: lw x20, -1920(x8)
addr0x00000588: lw x11, -904(x8)
addr0x0000058c: lw x31, 309(x8)
addr0x00000590: bne x24, x17, addr0x000005dc
addr0x00000594: lw x22, -946(x8)
addr0x00000598: sw x22, 1496(x8)
addr0x0000059c: addi x5, x14, -50
addr0x000005a0: jal x19, addr0x00000888
addr0x000005a4: sw x7, -1540(x8)
addr0x000005a8: blt x24, x19, addr0x00000304
addr0x000005ac: lw x0, 1451(x8)
addr0x000005b0: andi x27, x29, 751
addr0x000005b4: lw x7, 1075(x8)
addr0x000005b8: andi x23, x5, 1580
addr0x000005bc: addi x7, x10, 586
addr0x000005c0: lw x19, -1507(x8)
addr0x000005c4: lw x31, 1860(x8)
addr0x000005c8: lw x27, 308(x8)
addr0x000005cc: addi x10, x19, -861
addr0x000005d0: sw x28, 1677(x8)
addr0x000005d4: sw x22, -168(x8)
addr0x000005d8: lw x17, -1800(x8)
addr0x000005dc: addi x29, x17, -1686
addr0x000005e0: sw x25, -1123(x8)
addr0x000005e4: sw x13, -552(x8)
addr0x000005e8: jal x27, addr0x000008c0
addr0x000005ec: lw x30, -1577(x8)
addr0x000005f0: sub x13, x10, x4
addr0x000005f4: beq x20, x7, addr0x000002cc
addr0x000005f8: srl x28, x20, x24
addr0x000005fc: sll x19, x31, x7
addr0x00000600: srl x29, x29, x22
addr0x00000604: sll x30, x7, x20
addr0x00000608: or x0, x13, x18
addr0x0000060c: addi x2, x31, 1866
addr0x00000610: sw x7, 475(x8)
addr0x00000614: jal x23, addr0x00000a44
addr0x00000618: lw x0, 1595(x8)
addr0x0000061c: bgeu x26, x20, addr0x0000091c
addr0x00000620: lw x23, 871(x8)
addr0x00000624: bne x27, x12, addr0x000008a4
addr0x00000628: addi x23, x19, 441
addr0x0000062c: sltu x20, x4, x4
addr0x00000630: sw x7, 1523(x8)
addr0x00000634: jal x19, addr0x00000394
addr0x00000638: jal x28, addr0x000000e8
addr0x0000063c: lw x17, 314(x8)
addr0x00000640: sw x25, 774(x8)
addr0x00000644: sb x11, -187(x8)
addr0x00000648: lw x5, 188(x8)
addr0x0000064c: addi x4, x1, -974
addr0x00000650: sw x19, -1632(x8)
addr0x00000654: jal x30, addr0x000003fc
addr0x00000658: addi x14, x10, -2016
addr0x0000065c: beq x30, x2, addr0x000004b8
addr0x00000660: lw x25, 532(x8)
addr0x00000664: jal x27, addr0x000008d0
addr0x00000668: lw x23, -1549(x8)
addr0x0000066c: addi x0, x0, 883
addr0x00000670: lw x20, 1701(x8)
addr0x00000674: addi x31, x22, 489
addr0x00000678: jal x5, addr0x000007c8
addr0x0000067c: lw x17, 1495(x8)
addr0x00000680: slli x23, x0, 10
addr0x00000684: lw x31, 437(x8)
addr0x00000688: addi x18, x5, 1952
addr0x0000068c: addi x14, x21, 1191
addr0x00000690: sw x20, 1259(x8)
addr0x00000694: lw x22, -1795(x8)
addr0x00000698: sw x28, 1143(x8)
addr0x0000069c: lw x28, 633(x8)
addr0x000006a0: beq x13, x1, addr0x00000738
addr0x000006a4: addi x0, x7, -1591
addr0x000006a8: sw x18, 202(x8)
addr0x000006ac: jal x11, addr0x000006b8
addr0x000006b0: lw x7, -2027(x8)
addr0x000006b4: add x17, x17, x0
addr0x000006b8: lw x17, 1762(x8)
addr0x000006bc: lw x5, 593(x8)
addr0x000006c0: bgeu x22, x28, addr0x00000054
addr0x000006c4: bltu x10, x18, addr0x0000074c
addr0x000006c8: srai x7, x31, 17
addr0x000006cc: ori x6, x6, 1492
addr0x000006d0: sw x7, -563(x8)
addr0x000006d4: lw x0, -1943(x8)
addr0x000006d8: addi x27, x24, 1486
addr0x000006dc: jal x7, addr0x000008ec
addr0x000006e0: lw x27, 53(x8)
addr0x000006e4: lw x25, -874(x8)
addr0x000006e8: lbu x0, -1203(x8)
addr0x000006ec: beq x4, x13, addr0x00000120
addr0x000006f0: addi x12, x0, -385
addr0x000006f4: sub x9, x16, x9
addr0x000006f8: sw x6, -960(x8)
addr0x000006fc: lw x18, 451(x8)
addr0x00000700: lw x0, -1475(x8)
addr0x00000704: lw x15, -233(x8)
addr0x00000708: lw x29, -639(x8)
addr0x0000070c: sw x17, 376(x8)
addr0x00000710: jal x17, addr0x00000440
addr0x00000714: lui x4, 292067
addr0x00000718: addi x14, x27, -481
addr0x0000071c: jal x27, addr0x00000390
addr0x00000720: lw x19, 275(x8)
addr0x00000724: lw x29, -721(x8)
addr0x00000728: sw x30, 1236(x8)
addr0x0000072c: sw x17, 2(x8)
addr0x00000730: lbu x20, 670(x8)
addr0x00000734: lw x23, 836(x8)
addr0x00000738: addi x4, x7, -1234
addr0x0000073c: jal x9, addr0x000003f8
addr0x00000740: addi x19, x7, -1355
addr0x00000744: sw x9, 508(x8)
addr0x00000748: jal x26, addr0x00000080
addr0x0000074c: sb x14, -12(x8)
addr0x00000750: slli x13, x9, 18
addr0x00000754: lw x5, 128(x8)
addr0x00000758: lw x20, 1961(x8)
addr0x0000075c: lw x23, 670(x8)
addr0x00000760: sw x19, -162(x8)
addr0x00000764: sw x21, 615(x8)
addr0x00000768: sw x1, -1768(x8)
addr0x0000076c: sw x0, 1027(x8)
addr0x00000770: lbu x0, -1354(x8)
addr0x00000774: sb x19, 72(x8)
addr0x00000778: addi x19, x18, -1026
addr0x0000077c: jal x29, addr0x00000490
addr0x00000780: addi x23, x2, -680
addr0x00000784: jal x31, addr0x000009e4
addr0x00000788: lw x31, -1771(x8)
addr0x0000078c: andi x19, x14, -977
addr0x00000790: jal x2, addr0x000000c0
addr0x00000794: bge x9, x19, addr0x00000548
addr0x00000798: addi x13, x5, -719
addr0x0000079c: slli x31, x27, 18
addr0x000007a0: jal x5, addr0x000007f8
addr0x000007a4: lw x27, 1591(x8)
addr0x000007a8: addi x27, x5, -1355
addr0x000007ac: sw x22, -1148(x8)
addr0x000007b0: lw x7, 661(x8)
addr0x000007b4: bne x17, x0, addr0x0000099c
addr0x000007b8: lw x2, 1983(x8)
addr0x000007bc: sb x24, 138(x8)
addr0x000007c0: sw x31, 639(x8)
addr0x000007c4: jal x6, addr0x00000140
addr0x000007c8: lw x2, -1562(x8)
addr0x000007cc: lw x17, 1257(x8)
addr0x000007d0: lw x26, -380(x8)
addr0x000007d4: lw x24, -480(x8)
addr0x000007d8: addi x24, x11, -1749
addr0x000007dc: jal x24, addr0x00000484
addr0x000007e0: lw x4, -1359(x8)
addr0x000007e4: lw x13, 1505(x8)
addr0x000007e8: lw x9, 919(x8)
addr0x000007ec: addi x23, x19, -1390
addr0x000007f0: sw x0, -706(x8)
addr0x000007f4: jal x4, addr0x00000714
addr0x000007f8: addi x27, x23, -775
addr0x000007fc: sw x14, 849(x8)
addr0x00000800: lw x25, -310(x8)
addr0x00000804: beq x29, x0, addr0x00000060
addr0x00000808: sb x12, 1350(x8)
addr0x0000080c: jal x25, addr0x00000a78
addr0x00000810: lw x29, -1214(x8)
addr0x00000814: jal x9, addr0x000003d0
addr0x00000818: sb x11, -1685(x8)
addr0x0000081c: lw x11, -871(x8)
addr0x00000820: sw x20, 69(x8)
addr0x00000824: bne x9, x18, addr0x00000918
addr0x00000828: lw x1, 1130(x8)
addr0x0000082c: bgeu x22, x6, addr0x00000800
addr0x00000830: lw x18, 818(x8)
addr0x00000834: lw x28, 495(x8)
addr0x00000838: lw x0, -1887(x8)
addr0x0000083c: lbu x27, -1308(x8)
addr0x00000840: bne x23, x19, addr0x000009e8
addr0x00000844: lbu x24, 37(x8)
addr0x00000848: sll x19, x18, x17
addr0x0000084c: lw x30, -1829(x8)
addr0x00000850: addi x16, x9, -410
addr0x00000854: sub x19, x9, x25
addr0x00000858: addi x27, x19, 243
addr0x0000085c: lw x30, -1418(x8)
addr0x00000860: sw x10, -935(x8)
addr0x00000864: addi x14, x9, 1578
addr0x00000868: lw x23, -1158(x8)
addr0x0000086c: addi x0, x22, 1468
addr0x00000870: lw x23, 1144(x8)
addr0x00000874: sw x31, -764(x8)
addr0x00000878: sw x20, 898(x8)
addr0x0000087c: sw x7, 384(x8)
addr0x00000880: lw x7, -1146(x8)
addr0x00000884: sw x19, 841(x8)
addr0x00000888: beq x10, x19, addr0x00000208
addr0x0000088c: sw x2, 1410(x8)
addr0x00000890: lui x0, 884779
addr0x00000894: addi x9, x20, -1982
addr0x00000898: sw x27, 1463(x8)
addr0x0000089c: sw x21, -1579(x8)
addr0x000008a0: sw x24, 186(x8)
addr0x000008a4: sw x20, 536(x8)
addr0x000008a8: jal x20, addr0x0000030c
addr0x000008ac: addi x13, x13, -1740
addr0x000008b0: lw x31, -859(x8)
addr0x000008b4: jal x4, addr0x0000036c
addr0x000008b8: lw x27, 1767(x8)
addr0x000008bc: andi x9, x17, -1856
addr0x000008c0: andi x18, x7, -1460
addr0x000008c4: lw x0, -1123(x8)
addr0x000008c8: bltu x19, x17, addr0x00000908
addr0x000008cc: bne x28, x17, addr0x00000380
addr0x000008d0: lw x23, 1727(x8)
addr0x000008d4: lw x13, 412(x8)
addr0x000008d8: lw x16, -1760(x8)
addr0x000008dc: lw x30, 42(x8)
addr0x000008e0: sw x18, -1077(x8)
addr0x000008e4: lw x19, 1553(x8)
addr0x000008e8: bge x10, x17, addr0x00000434
addr0x000008ec: bltu x12, x6, addr0x000004cc
addr0x000008f0: slli x7, x7, 6
addr0x000008f4: jal x13, addr0x000004e4
addr0x000008f8: jal x2, addr0x0000033c
addr0x000008fc: lw x13, 1755(x8)
addr0x00000900: beq x19, x17, addr0x00000758
addr0x00000904: lh x4, 1093(x8)
addr0x00000908: jal x7, addr0x00000580
addr0x0000090c: lw x1, 1884(x8)
addr0x00000910: sw x1, 2003(x8)
addr0x00000914: sw x2, -997(x8)
addr0x00000918: sw x12, 1130(x8)
addr0x0000091c: addi x21, x2, -379
addr0x00000920: sw x2, -1920(x8)
addr0x00000924: sw x22, -1846(x8)
addr0x00000928: addi x22, x18, 1125
addr0x0000092c: jal x31, addr0x0000029c
addr0x00000930: addi x7, x21, 863
addr0x00000934: lbu x7, 1084(x8)
addr0x00000938: lw x16, 729(x8)
addr0x0000093c: addi x23, x1, -1436
addr0x00000940: addi x10, x13, 1970
addr0x00000944: jal x22, addr0x0000038c
addr0x00000948: lw x1, 509(x8)
addr0x0000094c: add x11, x27, x31
addr0x00000950: sltu x27, x6, x9
addr0x00000954: or x22, x0, x31
addr0x00000958: addi x17, x5, 408
addr0x0000095c: lw x31, 1086(x8)
addr0x00000960: addi x31, x30, -522
addr0x00000964: addi x13, x29, 1210
addr0x00000968: lui x9, 863792
addr0x0000096c: lw x25, -743(x8)
addr0x00000970: lw x0, 93(x8)
addr0x00000974: lw x20, 1168(x8)
addr0x00000978: sub x0, x31, x23
addr0x0000097c: jal x21, addr0x0000038c
addr0x00000980: add x21, x13, x23
addr0x00000984: jal x6, addr0x00000890
addr0x00000988: jal x15, addr0x00000580
addr0x0000098c: lw x29, -1984(x8)
addr0x00000990: lw x2, -1180(x8)
addr0x00000994: lbu x19, 852(x8)
addr0x00000998: sb x25, 1295(x8)
addr0x0000099c: lw x19, -1063(x8)
addr0x000009a0: lw x20, 1768(x8)
addr0x000009a4: addi x7, x29, -1738
addr0x000009a8: jal x31, addr0x0000057c
addr0x000009ac: addi x24, x25, -130
addr0x000009b0: addi x5, x19, -1596
addr0x000009b4: jal x30, addr0x00000984
addr0x000009b8: addi x4, x12, -14
addr0x000009bc: sw x19, -1923(x8)
addr0x000009c0: addi x22, x4, -89
addr0x000009c4: sw x25, 1383(x8)
addr0x000009c8: sw x7, 1541(x8)
addr0x000009cc: jal x6, addr0x0000024c
addr0x000009d0: lw x6, -2010(x8)
addr0x000009d4: jal x13, addr0x00000868
addr0x000009d8: beq x14, x31, addr0x00000478
addr0x000009dc: beq x24, x23, addr0x00000320
addr0x000009e0: lbu x4, -1876(x8)
addr0x000009e4: sw x18, -1577(x8)
addr0x000009e8: andi x31, x27, -456
addr0x000009ec: sw x27, -1783(x8)
addr0x000009f0: lw x29, -1816(x8)
addr0x000009f4: lw x22, 56(x8)
addr0x000009f8: lw x18, 1293(x8)
addr0x000009fc: lw x0, 452(x8)
addr0x00000a00: lw x25, -97(x8)
addr0x00000a04: or x17, x28, x7
addr0x00000a08: lbu x4, -1026(x8)
addr0x00000a0c: sb x29, -1032(x8)
addr0x00000a10: lw x1, -264(x8)
addr0x00000a14: addi x25, x17, -1921
addr0x00000a18: lbu x23, 1664(x8)
addr0x00000a1c: lw x14, -1915(x8)
addr0x00000a20: beq x7, x7, addr0x00000778
addr0x00000a24: lw x7, 404(x8)
addr0x00000a28: beq x0, x6, addr0x000006f8
addr0x00000a2c: addi x29, x20, -1979
addr0x00000a30: lw x7, -1345(x8)
addr0x00000a34: sw x5, -699(x8)
addr0x00000a38: ori x29, x10, 640
addr0x00000a3c: slli x11, x29, 11
addr0x00000a40: add x14, x28, x4
addr0x00000a44: beq x19, x25, addr0x00000534
addr0x00000a48: sw x7, -456(x8)
addr0x00000a4c: lw x11, 702(x8)
addr0x00000a50: lui x11, 693317
addr0x00000a54: jal x25, addr0x0000093c
addr0x00000a58: addi x1, x27, 1301
addr0x00000a5c: jal x7, addr0x00000038
addr0x00000a60: lw x31, 251(x8)
addr0x00000a64: lw x18, 1088(x8)
addr0x00000a68: addi x22, x13, 801
addr0x00000a6c: jal x24, addr0x00000a38
addr0x00000a70: lui x11, 938694
addr0x00000a74: lui x4, 906001
addr0x00000a78: sw x17, 1229(x8)
addr0x00000a7c: sw x27, -1326(x8)
addr0x00000a80: lbu x23, 1665(x8)
addr0x00000a84: add x14, x17, x22
addr0x00000a88: addi x0, x23, 373
addr0x00000a8c: addi x22, x15, -1059
addr0x00000a90: slli x7, x14, 19
addr0x00000a94: mul x23, x17, x29
addr0x00000a98: mul x27, x17, x27
addr0x00000a9c: add x17, x30, x22
