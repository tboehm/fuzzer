#!/usr/bin/env python3

# analyze.py: Process object dumps
#
# Copyright (c) 2020 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import subprocess as sp

from instructions import opcodes, asm_terminus

# Object dump command and flags
objdump_cmd = 'riscv64-unknown-elf-objdump -d -z -M numeric'

# {symbol: address}
# TODO: this will look like a standard symbol table based on the input
symbol_table = dict()

# {symbol: frequency}
# TODO: count how many times we see each symbol in the input
address_pool = dict()


class Dump:
    '''
    Parsed object dump from a RISC-V ELF
    '''
    def __init__(self, filename):
        self.filename = filename
        if filename.endswith('.dump'):
            with open(filename, 'r') as fp:
                self.raw_lines = fp.readlines()
        else:
            self.raw_lines = self.dump()
        self.lines = [Line(raw_line) for raw_line in self.raw_lines]
        self.code = self.get_code()
        self._frequencies = None

    def dump(self):
        '''
        Run the object dump and return the raw output.
        '''
        command = ' '.join([objdump_cmd, self.filename])
        raw_output = sp.check_output(command.split())
        lines = [line.strip() for line in raw_output.decode().splitlines()]
        return lines

    def get_code(self):
        '''
        Extract the lines that contain assembly instructions from the dump.
        '''
        code = dict()
        for nr, line in enumerate(self.lines):
            if line.is_code:
                code[line.addr] = line
        return code

    @property
    def frequencies(self):
        '''
        Frequencies for opcode 2-sequences.
        '''
        if self._frequencies is None:
            self._frequencies = self.analyze()
        return self._frequencies

    def print_frequencies(self):
        '''
        Display the opcode frequencies.
        '''
        for opcode in self.frequencies:
            print(opcode, self.frequencies[opcode])

    def analyze(self):
        '''
        Generate the opcode frequency dictionary.
        '''
        # opcode: {op1: num_occurrences, op2: num_occurences, ...}
        # where `op1`, `op2`, ... are opcodes that follow `opcode`
        opcode_frequencies = dict()

        for addr, line in self.code.items():
            # The opcode in question
            opcode = line.opcode

            # Frequencies for opcodes that follow this opcode
            frequencies = opcode_frequencies.setdefault(opcode, {})

            # There may be compressed instructions, so check +2 and +4 bytes
            next_half = addr + 2
            next_word = addr + 4
            next_line = self.code.get(next_word) or self.code.get(next_half)

            # Update the frequencies dict for this opcode
            if next_line is not None:
                next_opcode = next_line.opcode
            else:
                next_opcode = asm_terminus
            freq = frequencies.get(next_opcode, 0)
            freq += 1
            frequencies[next_opcode] = freq

        return opcode_frequencies


class Line:
    '''
    A single line from the object dump.

    Do not use the `get_` functions from this class directly. Instead, use the
    attributes that we set up during initialization.
    '''
    def __init__(self, line):
        self.line = line
        self.tokens = line.split()
        self.addr = self.get_addr()
        self.data = self.get_data()
        self.opcode = self.get_opcode()
        self.operands = self.get_operands()
        self.is_code = self.get_is_code()

    def get_addr(self):
        '''
        Extract the address where a line is located.

        Returns None on failure.
        '''
        try:
            # Shouldn't be more than 8 nibbles
            addr_part = self.tokens[0][:8]
            colon_index = addr_part.index(':')
            addr = addr_part[:colon_index].strip()
            return int(addr, 16)
        except (IndexError, ValueError):
            # Line/token too short or unable to decode from hex
            return None

    def get_data(self):
        '''
        Extract the assembled data from a line.

        Returns None on failure.
        '''
        try:
            data = self.tokens[1][:8]
            return int(data, 16)
        except (IndexError, ValueError):
            # Line/token too short or unable to decode from hex
            return None

    def get_opcode(self):
        '''
        Extract the opcode from a line.

        Returns None if there is no opcode.
        '''
        try:
            opcode = self.tokens[2]
            if opcode in opcodes:
                return opcode
            else:
                return None
        except IndexError:
            # Line too short
            return None

    def get_operands(self):
        '''
        Extract operands from a line.

        Returns None if there are no operands.
        '''
        if self.opcode is None:
            # No opcode -> no operands
            return None
        try:
            operands = self.tokens[3].split(',')
            return operands
        except IndexError:
            # Line too short
            return None

    def get_is_code(self):
        '''
        Code lines look like this:

        addr:   data    opcode operands

        Where addr and data are numeric, opcode is alphabetic, and operands may
        be a mix.
        '''
        if self.addr is None or self.data is None or self.opcode is None:
            return False
        else:
            return True

    def __str__(self):
        return self.line
