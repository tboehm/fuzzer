#!/usr/bin/env python3

import argparse
from multiprocessing import Process, Value
import numpy as np
import os
import shutil
import subprocess as sp
import time

from fuzzer import generate_chain, generate_test
from markov import MarkovChain

MAX_MISMATCH = 1
MAX_BUGS = 30

# root = os.path.realpath(os.pardir)
root = os.getcwd()
ref_dir = os.path.join(root, "swerv/tools")
ref_design = os.path.join(root, "swerv/design")
bug_dir = os.path.join(root, "buggy-swerv/tools")
bug_suite = os.path.join(root, "buggy-swerv/bugged_files")
bug_design = os.path.join(root, "buggy-swerv/design")
test_dir = os.path.join(root, "fuzzer/tests")
res_dir = os.path.join(root, "results")

bug_dict = {
    "dec_decode_ctl.sv": "dec/dec_decode_ctl.sv",
    "dec_decode_ctl_1.sv": "dec/dec_decode_ctl.sv",
    "dec_decode_ctl_2.sv": "dec/dec_decode_ctl.sv",
    "dec_decode_ctl_3.sv": "dec/dec_decode_ctl.sv",
    "dec_decode_ctl_4.sv": "dec/dec_decode_ctl.sv",
    "dec_decode_ctl_5.sv": "dec/dec_decode_ctl.sv",
    "exu.sv": "exu/exu.sv",
    "exu_1.sv": "exu/exu.sv",
    "exu_alu_ctl.sv": "exu/exu_alu_ctl.sv",
    "exu_div_ctl.sv": "exu/exu_div_ctl.sv",
    "exu_mul_ctl.sv": "exu/exu_mul_ctl.sv",
    "ifu_ifc_ctl.sv": "ifu/ifu_ifc_ctl.sv",
}

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--make', action='store_true',
                    help='Remake SweRV')
parser.add_argument('-n', '--num-tests', type=int, default=1000,
                    help='Maximum number of tests to run')
parser.add_argument('-f', '--files', nargs='+', default=None,
                    help='Input files (.s or .dump)')


def test_process(test, num_pass, bug):
    ref_log = os.path.join(ref_dir, 'exec.log')
    bug_log = os.path.join(bug_dir, 'exec.log')
    with open(ref_log, "w") as ref, open(bug_log, "w") as buggy:
        ref.truncate(0)
        buggy.truncate(0)
    print("Running Test: " + test[: -len(".hex")])
    test_proc = Process(target=run_test, args=(test, ref_dir, bug_dir))
    test_proc.start()
    test_proc.join()
    found_bug = compare(ref_dir, bug_dir, test, bug)
    if found_bug:
        # Save the test that generated the bug
        test_base = test.strip('.hex')
        saved_name = f"reproduce_{bug.strip('.sv')}_{test_base}"
        orig_test = os.path.join(test_dir, test_base)
        saved_test = os.path.join(test_dir, saved_name)
        shutil.copy(orig_test + '.hex', saved_test + '.hex')
        shutil.copy(orig_test + '.s', saved_test + '.s')
    else:
        log = "NO BUG FOUND IN TEST: {}\n".format(test[: -len(".hex")])
        print(log)
        with open(os.path.join(res_dir, f"{bug}.log"), "a") as summary:
            summary.write(log)
        num_pass.value += 1


def run_test(test, dir1, dir2):
    shutil.copy(os.path.join(test_dir, test), os.path.join(dir1, "fuzzed.hex"))
    shutil.copy(os.path.join(test_dir, test), os.path.join(dir2, "fuzzed.hex"))
    ref_proc = Process(target=tester, args=(test, dir1))
    bug_proc = Process(target=tester, args=(test, dir2))
    ref_proc.start()
    bug_proc.start()
    ref_proc.join()
    bug_proc.join()


def tester(test, dir):
    shutil.copy(os.path.join(test_dir, test), os.path.join(dir, "fuzzed.hex"))
    os.chdir(dir)
    os.system("./obj_dir/Vtb_top > out.log")


def compare(dir1, dir2, test, bug):
    # Compare Outputs
    ref_prev = ""
    bug_prev = ""
    ref_log = os.path.join(dir1, 'exec.log')
    bug_log = os.path.join(dir2, 'exec.log')
    with open(ref_log, "r") as ref, open(bug_log, "r") as buggy:
        reflines = ref.readlines()
        for ref_line in reflines:
            bug_line = buggy.readline()
            if ref_line != bug_line:
                test_name = test.strip('.hex')
                print_err(ref_line, bug_line, test_name, ref_prev, bug_prev, bug)
                return True
            ref_prev = ref_line
            bug_prev = bug_line
    return False


def print_err(ref_line, bug_line, test, ref_prev, bug_prev, bug_name):
    ref = parse(ref_line.strip(), test)
    bug = parse(bug_line.strip(), test)
    table = [["", "Cycle", "PC", "Instruction",
              "LSU_ADDR", "LSU_DATA", "REG", "REGVALUE"]]
    if ref_prev:
        ref_p = parse(ref_prev.strip(), test)
        bug_p = parse(bug_prev.strip(), test)
        table.append(["SweRV", ref_p["Cycle"], ref_p["PC"], ref_p["instr"],
                      ref_p["addr"], ref_p["data"], ref_p["reg"],
                      ref_p["regvalue"]])
        table.append(["Buggy-SweRV", bug_p["Cycle"], bug_p["PC"],
                      bug_p["instr"], bug_p["addr"], bug_p["data"],
                      bug_p["reg"], bug_p["regvalue"]])
        table.append(["", "", "", "", "", "", "", "", ])

    table.append(["SweRV", ref["Cycle"], ref["PC"], ref["instr"], ref["addr"],
                  ref["data"], ref["reg"], ref["regvalue"]])
    table.append(["Buggy-SweRV", bug["Cycle"], bug["PC"], bug["instr"],
                  bug["addr"], bug["data"], bug["reg"], bug["regvalue"]])

    log = ("BUG FOUND IN TEST {}\n".format(test))
    for cat, cyc, pc, ins, adr, data, reg, val in table:
        log += "{:<12} {:<5} {:<10} {:<30} {:<10} {:<10} {:<3} {:<10}\n".format(
            cat, cyc, pc, ins, adr, data, reg, val)

    print(log)
    with open(os.path.join(res_dir, f"{bug_name}.log"), "a") as summary:
        summary.write(log)


def instr(pc, test):
    addr = f'addr0x{pc}'
    test_asm = os.path.join(test_dir, f'{test}.s')
    with open(test_asm, 'r') as read:
        f = read.readlines()
    for line in f:
        if addr in line:
            tokens = line.split(":")
            return tokens[1][1:].strip()
    # TODO: Sometimes we hit this line -- not sure why
    print(f"FAILED TO FIND ADDRESS IN ASM: {addr}")


def parse(line, test):
    data = (" ".join(line.split())).split(" ")
    if len(data) < 6:
        print(f'Malformed line: {line}')
        return
    op = instr(data[2], test)
    if len(data) == 7:
        reg_info = data[6].split("=")
        reg = reg_info[0]
        regvalue = reg_info[1]
    else:
        reg = ""
        regvalue = ""
    data_dict = {"Cycle": data[1], "PC": data[2], "instr": op, "addr": data[4],
                 "data": data[5], "reg": reg, "regvalue": regvalue}
    return data_dict


def make():
    os.environ["RV_ROOT"] = os.path.join(root, "buggy-swerv")
    os.chdir(os.path.join(os.environ["RV_ROOT"], "tools"))
    print("Building Buggy-SweRV")
    rc = sp.call("make clean".split(), stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    if rc:
        print(f'make clean failed for buggy swerv with code {rc}')
        exit(1)
    rc = sp.call("make -j4 verilator-build".split(), stdout=sp.DEVNULL,
                 stderr=sp.DEVNULL)
    if rc:
        print(f'make verilator-build failed for buggy swerv with code {rc}')
        exit(1)
    print("Finished Building Buggy-SweRV")
    os.chdir(root)


def init():
    print(os.getcwd())
    shutil.copy("tb_top.sv", os.path.join(root, "swerv/testbench"))
    shutil.copy("tb_top.sv", os.path.join(root, "buggy-swerv/testbench"))
    os.environ["RV_ROOT"] = os.path.join(root, "swerv")
    tools = os.path.join(os.environ["RV_ROOT"], "tools")
    os.chdir(tools)
    print(os.getcwd())
    print("Building SweRV")
    rc = sp.call("make -j4 verilator-build".split(), stdout=sp.DEVNULL,
                 stderr=sp.DEVNULL)
    if rc:
        print(f'make verilator-build failed for swerv with code {rc}')
        exit(1)
    print("Finished Building SweRV")
    os.chdir(root)


def test(bug, max_tests: int = 0, chain: MarkovChain = None):
    '''
    1. Run Test on Reference and Bugged cores in parallel.
    2. Comparison results in [bug].log.
    '''
    num_pass = Value("i", 0)
    num_tests = 0
    if chain is not None:
        for test_num in range(max_tests):
            test_path = os.path.join(test_dir, f'test_{test_num}')
            start_gen = time.monotonic()
            hexfile = os.path.basename(generate_test(chain, test_path))
            gen_time = time.monotonic() - start_gen
            print(f'---> Generated test in {gen_time:.2f} sec')
            start_run = time.monotonic()
            proc = Process(target=test_process, args=(hexfile, num_pass, bug))
            proc.start()
            proc.join()
            end_run = time.monotonic() - start_run
            print(f'---> Ran test in {end_run:.2f} sec')
            if num_pass.value <= test_num + 1 - MAX_BUGS:
                # Found MAX_BUGS bugs -- stop
                break
    else:
        hex_tests = sorted([f for f in os.listdir(test_dir)
                            if f.endswith('.hex')])
        for test_num, test in enumerate(hex_tests):
            proc = Process(target=test_process, args=(test, num_pass, bug))
            proc.start()
            proc.join()
            if num_pass.value <= num_tests + 1 - MAX_BUGS:
                # Found MAX_BUGS bugs -- stop
                break
    num_tests = test_num + 1
    log = ("Running tests for bug {}\n".format(bug))
    log += "Number of tests run: {}\n".format(num_tests)
    log += "Found bug in {} tests\n".format(num_tests - num_pass.value)
    print(log)
    with open(os.path.join(res_dir, f"{bug}.log"), "a") as logger, \
            open(os.path.join(res_dir, "summary.log"), "a") as summary:
        summary.write(log)
        logger.write(log)


def insert_bug(bug):
    '''
    Inserts bug into Buggy-SweRV. Return the path of the file that was changed.
    '''
    shutil.copy(os.path.join(bug_suite, bug),
                os.path.join(bug_design, bug_dict[bug]))
    return bug_dict[bug]


def revert_bug(file):
    shutil.copy(os.path.join(ref_design, file), os.path.join(bug_design, file))


def main(make_swerv: bool, files: list, max_tests: int):
    '''
    1. Copy over testbench files to Buggy-SweRV and SweRV
    2. Make SweRV
    3. For each bug, copy bug into Buggy-SweRV
    4. Make Buggy-SweRV
    5. Run Test Suite
    6. Compare Results
    7. Revert Buggy-SweRV Design
    8. Loop back to step 3
    '''
    generate_new_tests = files is not None

    if make_swerv:
        init()

    with open(os.path.join(res_dir, "summary.log"), "w") as summary:
        summary.truncate(0)

    if generate_new_tests:
        seed = np.random.randint(0, int(2**32 - 1))
        print(f'Random seed: {seed}')
        start_chain = time.monotonic()
        chain = generate_chain(files, seed)
        chain_time = time.monotonic() - start_chain
        print(f'--> Generated Markov chain in {chain_time:.2f} sec')
    else:
        chain = None

    num_bugs = len(bug_dict)
    for bugnum, bug in enumerate(bug_dict.keys()):
        print(f"Testing bug {bugnum + 1}/{num_bugs}: {bug.strip('.sv')}")
        changed_file = insert_bug(bug)
        start_make = time.monotonic()
        make()
        make_time = time.monotonic() - start_make
        print(f'--> Built swerv in {make_time:.2f} sec')
        test(bug.strip('.sv'), max_tests, chain)
        revert_bug(changed_file)

    if generate_new_tests:
        print(f'Random seed for this run was {seed}')


if __name__ == "__main__":
    args = parser.parse_args()
    main(args.make, args.files, args.num_tests)
