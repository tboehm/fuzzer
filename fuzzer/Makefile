# Copyright (c) 2020 Trey Boehm
#
# Makefile for generating RISC-V dumps
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Additions from https://tech.davis-hansson.com/p/make
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c  # "strict" bash mode
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables

RV=riscv64-unknown-elf
CC=$(RV)-gcc
CXX=$(RV)-g++
OBJDUMP=$(RV)-objdump
CXXVERSIONFLAGS=-Wno-c++11-extensions -std=c++11
# Don't generate compressed instructions. Unfortunately, I compiled my standard
# library with -march=rv64gc, so there are going to be unusable instructions in
# the dump. Also, specify position-independent code/executable for the code
# generation and linker. This makes it easier to parse the ELF later on.
CFLAGS=-march=rv64imafd -mabi=lp64d -fpic -fpie -static-pie -MMD -MP
CXXFLAGS=$(CFLAGS) $(CXXVERSIONFLAGS)

# Generate an executable called `rv-elf`. This Makefile assumes all source is
# located in a flat directory called "src". It will put the object files in a
# directory called "build", also at the top level.
BIN=rv-elf
SRC=src
BUILD=build
SRC_FILES = $(shell ls $(SRC)/*.cpp 2>/dev/null)
HDR_FILES = $(shell ls $(SRC)/*.h 2>/dev/null)
OBJ_FILES = $(subst $(SRC),$(BUILD),$(SRC_FILES:.cpp=.o))
DEP_FILES = $(OBJ_FILES:.o=.d)

# Options for creating an assembly test for the swerv core
SWERV_OPTS= \
	-march=rv32im \
	-mabi=ilp32 \
	-nostdlib \
	-static \
	-mcmodel=medany \
	-fvisibility=hidden \
	-nostartfiles \
	-T tests/simple.ld \

-include $(DEPS)

.DEFAULT_GOAL: all
all: $(BIN)

$(BIN): $(OBJ_FILES)
	@printf "  LD   $@\n"
	@$(CXX) $(CXXFLAGS) $^ -o $@

$(BUILD)/%.o: $(SRC)/%.c
	@printf "  CC   $<\n"
	@mkdir -p `dirname $@`
	@$(CC) -c $(CFLAGS) $< -o $@

$(BUILD)/%.o: $(SRC)/%.cpp
	@printf "  CXX  $<\n"
	@mkdir -p `dirname $@`
	@$(CXX) -c $(CXXFLAGS) $< -o $@

# Create the elf binary from the assembly file.
define assemble_elf
	@printf "  AS   $<\n"
	@$(CC) $(SWERV_OPTS) $< -o $@
endef
%.elf: %.s ; $(assemble_elf)
%.elf: %.S ; $(assemble_elf)

# 1) printf: load in at the entry point
# 2) grep: isolate data from applicable lines
# 3) cut: strip whitespace
# 4) sed: separate into bytes
# 5) awk: reverse endianness
%.hex: %.elf
	@printf "  OBJDUMP  $<\n"
	@printf "@00000000\n" > $@
	@$(OBJDUMP) -d $< \
		| grep -Eo '\t[0-9a-f]{8} ' \
		| cut -b 2-9 \
		| sed 's/.\{2\}/& /g' \
		| awk '{print $$4 " " $$3 " " $$2 " " $$1;}' \
		>> $@

.PHONY: clean
clean:
	@rm -f $(BIN)
	@rm -rf $(BUILD)
