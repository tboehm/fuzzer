RISC-V Fuzzer
=============
Term project for Verification of Digital Systems (EE 382M-11) with [Dr. Jacob
Abraham.](https://www.cerc.utexas.edu/~jaa).

Setup
-----
Pull in the required Python dependencies for the fuzzer (requires Python 3.6 or
higher):
```bash
pip3 install -r fuzzer/requirements.txt
```

Before you commit any changes to this file, please run the following command
from within the `fuzzer` directory to make sure there are no PEP-8 style
infractions. This should help keep our code relatively clean:
```bash
flake8 .
```

Fuzzer
------

Generate a Markov chain of opcodes from a RISC-V ELF or ELFs, create an opcode
sequence, randomize the operands, assemble, and dump hex. See the output of
`./fuzzer.py --help` for available options. Note: you must have the [GNU
toolchain](https://github.com/riscv/riscv-gnu-toolchain)
for RISC-V set up on your machine so you can use objdump and gcc.

Finding Bugs
------------

The `bug_suite.py` script runs fuzzed tests through the buggy DUT for each
available bug until it finds a mismatch with the reference model. You can
optionally use the `--files` to specify a list of ELF/dump files for generating
new tests. In this case, you will also want to use `--num-tests` to put a limit
on how many tests the tool will run through before giving up. If you do not
specify `--files`, the tool will use all available tests (hex files) in the
`fuzzer/tests` directory.

The script works as follows:
1. Copy an RTL file with one bug from `buggy-swerv/bugged_files` into the
   appropriate location in the design.
2. Rebuild the buggy DUT.
3. Run tests through the buggy DUT and reference model for some number of
   cycles.
4. Compare the resulting `exec.log` files to see whether they diverge at any
   point.
5. If there are differences, save the test file that found the bug. Go to step
   1 until there are no more bugged files.
6. If we have run all of the tests (or reached the specified limit), go back to
   step 1 until there are no more bugged files.

If this is your first time running the script, use the below command to build
the processors and copy over the updated testbench.
```bash
./fuzzer/bug_suite.py --make
```
